package com.avanza.pims.web;


public class MessageConstants {
	
	public interface InvestmentOpportunity
	{
		public static final String OPPORTUNITY_TITLE_IS_REQUIRED = "opportunity.save.titleRequired";
		public static final String OPPORTUNITY_OWNER_IS_REQUIRED = "opportunity.save.ownerRequired";
		public static final String OPPORTUNITY_TYPE_IS_REQUIRED = "opportunity.save.typeRequired";
		public static final String OPPORTUNITY_SUBJECT_IS_REQUIRED = "opportunity.save.subjectRequired";
		public static final String OPPORTUNITY_OPPORTUNITY_DATE_IS_REQUIRED = "opportunity.save.dateRequired";
		public static final String OPPORTUNITY_DESCRIPTION_IS_REQUIRED = "opportunity.save.descriptionRequired";
		public static final String OPPORTUNITY_PORTFOLIO_IS_REQUIRED = "opportunity.save.portfolioRequired";
		public static final String OPPORTUNITY_EVALUATION_IS_REQUIRED = "opportunity.save.evaluationRequired";
		
		
		
		public static final String OPPORTUNITY_SAVE_SUCCESS_ADD = "opportunity.save.success.add";
		public static final String OPPORTUNITY_SAVE_SUCCESS_EDIT = "opportunity.save.success.edit";
		public static final String OPPORTUNITY_EVALUATION_SAVE_SUCCESS = "opportunity.evaluation.save.success";
		
		public static final String EVALUATION_REQUEST_SENT_SUCCESS = "opportunity.evalution.request.success";
		public static final String APPROVAL_REQUEST_SENT_SUCCESS = "opportunity.approval.request.sent.success";
		public static final String APPROVAL_REQUEST_APPROVE_SUCCESS = "opportunity.approval.request.approve.success";
		public static final String APPROVAL_REQUEST_REJECT_SUCCESS = "opportunity.approval.request.reject.success";
		public static final String OPP_SAVED = "opportunity.saved";
		public static final String OPP_UPDATED = "opportunity.updated";
		public static final String OPP_APPROVED = "opportunity.approved";
		public static final String OPP_SENT_FOR_APPROVAL = "opportunity.sentApproval";
		public static final String OPP_SENT_FOR_EVALUATION = "opportunity.sentEvaluation";
		public static final String OPP_EVALUATION_SAVED = "opportunity.evaluationSaved";
		public static final String OPP_EVALUATION_UPDATED = "opportunity.evaluationUpdated";
		public static final String OPP_REJECTED = "opportunity.rejected";
		public static final String EVALUATION_REQUIRED = "evaluation.selectEvaluation";
		public static final String EVALUATION_RECOMMENDATION_REQUIRED = "evaluation.recommendationRequired";
		public static final String EVALUATION_DETAILS_REQUIRED = "evaluation.detailsRequired";
		
	}
	
	
	public interface Portfolio
	{
		public static final String PORTFOLIO_SAVE_SUCCESS = "portfolio.save.success";
		public static final String PORTFOLIO_SAVE_ERROR = "portfolio.save.error";
		public static final String PORTFOLIO_DELETE_SUCCESS = "portfolio.delete.success";
		public static final String PORTFOLIO_DELETE_ERROR = "portfolio.delete.error";
		
		public static final String NO_PORTFOLIO_SELECTED_ERROR = "no.portfolio.selected.error";
		
		public static final String PORTFOLIO_REQUEST_SENT_SUCCESS = "portfolio.request.sent.success";
		public static final String PORTFOLIO_REQUEST_APPROVE_SUCCESS = "portfolio.request.approve.success";
		public static final String PORTFOLIO_REQUEST_REJECT_SUCCESS = "portfolio.request.reject.success";
		public static final String PORTFOLIO_REQUEST_CLOSE_SUCCESS = "portfolio.request.close.success";
	}
	
	
	public interface Order
	{
		public static final String ORDER_SAVE_SUCCESS = "order.save.success";
		public static final String ORDER_SAVE_ERROR = "order.save.error";
		public static final String ORDER_DELETE_SUCCESS = "order.delete.success";
		public static final String ORDER_DELETE_ERROR = "order.delete.error";
		public static final String ORDER_REQUEST_SENT_SUCCESS = "order.request.sent.success";
		public static final String ORDER_REQUEST_APPROVE_SUCCESS = "order.request.approve.success";
		public static final String ORDER_REQUEST_REJECT_SUCCESS = "order.request.reject.success";
		 

	}
	
	public class Study {
		
		public static final String STUDY_CANCEL_SUCCESS = "study.cancel.success";
		public static final String STUDY_CANCEL_ERROR = "study.cancel.error";
		public static final String STUDY_DELETE_SUCCESS = "study.delete.success";
		public static final String STUDY_DELETE_ERROR = "study.delete.error";
		public static final String STUDY_ADD_SUCCESS = "study.add.success";
		public static final String STUDY_UPDATE_SUCCESS = "study.update.success";
		public static final String STUDY_SAVE_ERROR = "study.save.error";
		public static final String STUDY_SAVE_SUCCESS = "study.save.success";		
		public static final String NO_STUDY_SELECTED_ERROR = "study.select.zero.error";
		public static final String STUDY_REQUEST_SENT_SUCCESS = "study.request.sent";
		public static final String STUDY_REQUEST_APPROVE_SUCCESS = "study.request.approved";
		public static final String STUDY_REQUEST_REJECT_SUCCESS = "study.request.rejected";
		public static final String STUDY_REQUEST_COMPLETE_SUCCESS = "study.request.completed";
		public static final String LBL_STUDY_DATE = "study.startDate";
		public static final String LBL_COMPLETION_DATE = "study.endDate";
		public static final String LBL_STUDY_TYPE = "study.type";
		public static final String LBL_STUDY_SUBJECT = "study.subject";
		public static final String LBL_STUDY_TITLE = "study.title";
		public static final String LBL_STUDY_PURPOSE = "study.purpose";
		public static final String LBL_REQUESTED_BY = "study.requestedBy";
		public static final String LBL_DESCRIPTION = "study.description";
		
	}
	
	
	
public class FinancialConfiguration{
	public static final String MSG_CONFIGURATION_SAVED_SUCCESSFULLY = "financialAccountConfiguration.messages.savedSuccessfully";
	public static final String MSG_CONFIGURATION_ALRTEADY_EXIST= "financialAccountConfiguration.messages.alreadyExist";
	public static final String MSG_ERROR_ACTION_NO_IS_INCORRECT = "financialAccountConfiguration.messages.incorrectNo";
	public static final String MSG_ERROR_OWNERSHIP_TYPE_IS_MANDATORY = "financialAccountConfiguration.messages.ownershipTypeIsMandatory";
	public static final String MSG_ERROR_TRX_TYPE_REQ = "addFinancialConfiguration.msg.TrxTypeRequired";
	public static final String MSG_ERROR_PAYMENT_METHOD_REQ = "financialAccConfiguration.paymentMethodRequired";
	public static final String MSG_ERROR_BANK_ACC_NAME_REQ = "financialAccConfiguration.remitBankAccNameReq";

}

public class GRP{
	public static final String MSG_TRANSACTIONS_POSTED_SUCCESSFULLY = "grp.messages.selectedTransactionsPosted";
	public static final String GRP_TRANSACTION_STATUS = "GRP_TRANSACTION_STATUS";
	public static final String GRP_POSTED = "GRP_POSTED";
}
	public class Attachment {
		public static final String MSG_UPLOAD_FAILURE = "attachment.messages.uploadFailure";
		public static final String MSG_DOWNLOAD_FAILURE = "attachment.messages.downloadFailure";
		public static final String MSG_UPLOAD_SUCCESS = "attachment.messages.uploadSuccess";
		public static final String MSG_DOWNLOAD_SUCCESS = "attachment.messages.downloadSuccess";
		public static final String MSG_NO_FILE_SELECTED = "attachment.messages.noFileSelected";
		public static final String MSG_UPLOAD_PROC_DOC_FILE = "attachment.messages.uploadProcDocFile";
		public static final String MSG_UPLOAD_OTHER_DOC_FILE = "attachment.messages.uploadOtherDocFile";
		public static final String MSG_DOC_DELETE_SUCCESS = "attachment.messages.docDeleteSuccess";
		public static final String MSG_DOC_DELETE_FAILURE = "attachment.messages.docDeleteFailure";
		public static final String MSG_MANDATORY_DOCS = "attachment.messages.mandatoryDocs";
		public static final String MSG_FILE_SIZE_TOO_LARGE = "attachment.messages.fileSizeTooLarge";
		public static final String MSG_FILE_SIZE_TOO_SMALL = "attachment.messages.fileSizeTooSmall";
		public static final String MSG_FILE_DOES_NOT_EXIST = "attachment.messages.fileNotFound";
		public static final String MSG_UPLOAD_FAIL = "attachment.messages.uploadFail";
	}

	public class Notes{
		public static final String MSG_NO_COMMENTS="notes.messages.noComments";
		
	}

	public class ReplaceCheque {
		public static final String MSG_SEND_FOR_ACCEPTANCE = "replaceCheque.msg.sentForActionByBank";
		public static final String MSG_APPROVED_SUCCESSFULLY = "replaceCheque.msg.approvedByBank";
		public static final String INVALID_REQUEST = "replaceCheque.msg.invalidRequest";
		public static final String CHEQUE_CAN_NOT_BE_REPLACED = "replaceCheque.msg.chequeCanNotBeReplaced";
		public static final String MSG_CHEQUE_REPLACED_SUCCESSFULLY = "replaceCheque.msg.chequeReplaced";
		public static final String MSG_PAYMENT_RECEIVED_SUCCESSFULLY = "replaceCheque.msg.paymentReceived";
		public static final String MSG_REQUEST_COMPLETED_SUCCESSFULLY = "replaceCheque.msg.requestCompleted";
	}

	public class ReceiveProperty {

		public static final String MSG_PROPERTY_INVALID_UNIT_START_NUMBER ="receiveProperty.msg.invalidUnitStartNumber";
		public static final String MSG_PROPERTY_INVALID_UNIT_END_NUMBER="receiveProperty.msg.invalidUnitEndNumber";
		public static final String MSG_PROPERTY_INVALID_FLOOR_START_NUMBER ="receiveProperty.msg.invalidFloorStartNumber";
		public static final String MSG_PROPERTY_INVALID_FLOOR_END_NUMBER="receiveProperty.msg.invalidFloorEndNumber";
		public static final String MSG_PROPERTY_UNIT_RANGE_REQUIRED="receiveProperty.msg.unitRangeRequired";
		public static final String MSG_PROPERTY_FLOOR_RANGE_REQUIRED="receiveProperty.msg.floorRangeRequired";
		public static final String MSG_PROPERTY_DIRECTION_REQUIRED="receiveProperty.msg.directionRequired";
		public static final String MSG_PROPERTY_UNIT_RANGE_INVLAID="receiveProperty.msg.invalidUnitRange";
		public static final String MSG_PROPERTY_INVALID_FLOOR_NUMBER_FROM="receiveProperty.msg.invalidFloorNumberFrom";
		public static final String MSG_PROPERTY_INVALID_FLOOR_NUMBER_TO="receiveProperty.msg.invalidFloorNumberTo";
		public static final String MSG_PROPERTY_FLOOR_NO_TO_REQUIRED="receiveProperty.msg.floorNumberToRequired";
		public static final String MSG_PROPERTY_FLOOR_NO_FROM_REQUIRED="receiveProperty.msg.floorNumberFromRequired";
		public static final String MSG_PROPERTY_COMMERCIAL_NAME_REQUIRED="receiveProperty.msg.commercialNameRequired";
		public static final String MSG_PROPERTY_ADDRESS_REQUIRED="receiveProperty.msg.addressRequired";
		public static final String MSG_PROPERTY_LAND_NUMBER_REQUIRED="receiveProperty.msg.landNumberRequired";
		public static final String MSG_PROPERTY_INVALID_EW_UTILITY_NUMBER="receiveProperty.msg.invalidEWUtilityNumber";
		public static final String MSG_PROPERTY_FLOOR_REQUIRED="receiveProperty.msg.floorRequired";
		public static final String MSG_PROPERTY_UNIT_NO_REQUIRED="receiveProperty.msg.unitNoRequired";
		public static final String MSG_PROPERTY_INVALID_UNIT_NO="receiveProperty.msg.invalidUnitNo";
		public static final String MSG_PROPERTY_UNIT_TYPE_REQUIRED="receiveProperty.msg.unitTypeRequired";
		public static final String MSG_PROPERTY_UNIT_SIDE_REQUIRED="receiveProperty.msg.unitSideRequired";
		public static final String MSG_PROPERTY_INVALID_UNIT_SIDE="receiveProperty.msg.invalidUnitSide";
		public static final String MSG_PROPERTY_UNIT_USAGE_TYPE_REQUIRED="receiveProperty.msg.unitUsageTypeRequired";
		public static final String MSG_PROPERTY_UNIT_INVESTMENT_TYPE_REQUIRED="receiveProperty.msg.unitInvestmentTypeRequired";
		public static final String MSG_PROPERTY_UNIT_ACCOUNT_NUMBER_REQUIRED="receiveProperty.msg.unitAccountNumberRequired";
		public static final String MSG_PROPERTY_AREA_REQUIRED="receiveProperty.msg.areaInSquareRequired";
		public static final String MSG_PROPERTY_INVALID_AREA="receiveProperty.msg.invalidAreaInSquare";
		public static final String MSG_PROPERTY_BEDROOMS_REQUIRED="receiveProperty.msg.bedroomRequired";
		public static final String MSG_PROPERTY_INVALID_BEDROOMS="receiveProperty.msg.invalidBedrooms";
		public static final String MSG_PROPERTY_LIVINGROOMS_REQUIRED="receiveProperty.msg.livingRoomsRequired";
		public static final String MSG_PROPERTY_INVALID_LIVINGROOMS="receiveProperty.msg.invalidLivingRooms";
		public static final String MSG_PROPERTY_BATHROOMS_REQUIRED="receiveProperty.msg.bathroomRequired";
		public static final String MSG_PROPERTY_INVALID_BATHROOMS="receiveProperty.msg.invalidBathrooms";
		public static final String MSG_PROPERTY_RENT_REQUIRED="receiveProperty.msg.rentRequired";
		public static final String MSG_PROPERTY_INVALID_RENT="receiveProperty.msg.invalidRent";
		public static final String MSG_REVIEWED_SUCCESSFULLY="receiveProperty.msg.reviewedSuccessfully";
		public static final String THERE_MUST_BE_ONE_UNIT= "receiveProperty.msg.oneUnitShouldBeAvailable";
		public static final String FLOORS_MISSING_IN_DEFINITE_RANGE= "receiveProperty.msg.floorsMissingInDefiniteRange";
		public static final String RECEIVING_TEAM_MUST_BE_MADE= "receiveProperty.msg.receiveTeamShouldBeAvailable";
		public static final String EVALUATION_TEAM_MUST_BE_MADE= "receiveProperty.msg.evaluationTeamShouldBeAvailable";
		public static final String MSG_PROPERTY_REFERENCE_NUMBER_REQUIRED = "receiveProperty.msg.refNoRequired";
		public static final String MSG_UNIT_CANNOT_BE_DELETED  = "receiveProperty.msg.unitAssociated";
		public static final String MSG_PROPERTY_TYPE_REQUIRED = "receiveProperty.msg.propertyTypeRequired";
		public static final String MSG_PROPERTY_INVALID_LAND_AREA = "receiveProperty.msg.invalidLandArea";
		public static final String MSG_PROPERTY_INVALID_TOTAL_RENT="receiveProperty.msg.invalidTotalRent";
		public static final String MSG_PROPERTY_INVALID_BUILT_IN_AREA = "receiveProperty.msg.invalidBuiltInArea";
		public static final String MSG_PROPERTY_INVALID_NO_OF_LIFTS = "receiveProperty.msg.invalidNoOfLifts";
		public static final String MSG_PROPERTY_INVALID_SHARE_PERCENT = "receiveProperty.msg.invalidPercentage";
		public static final String MSG_OWNER_REQUIRED = "receiveProperty.msg.ownerRequired";
		public static final String MSG_REPRESENTATIVE_REQUIRED = "receiveProperty.msg.representativeRequired";
		public static final String MSG_PERCENTAGE_REQUIRED = "receiveProperty.msg.percentageRequired";
		public static final String MSG_PROPERTY_OWNERSHIP_TYPE_REQUIRED = "receiveProperty.msg.ownershipTypeRequired";
		public static final String MSG_PROPERTY_USAGE_REQUIRED = "receiveProperty.msg.usageRequired";
		public static final String MSG_PROPERTY_INVESTMENT_PURPOSE_REQUIRED = "receiveProperty.msg.investmentPurposeRequired";

		public static final String MSG_PERCENTAGE_INCORRECT = "receiveProperty.msg.percentageIncorrect";
		public static final String MSG_OWNER_ALREADY_ADDED = "receiveProperty.msg.ownerExist";
		public static final String MSG_REPRESENTATIVE_AND_OWNER_CANT_SAME = "receiveProperty.msg.commonRepresentativeAndOwner";
		public static final String MSG_CHARGES_REQUIRED = "receiveProperty.msg.chargesRequired";
		public static final String MSG_INVALID_CHARGES = "receiveProperty.msg.invalidCharges";
		public static final String MSG_FACILITY_REQUIRED = "receiveProperty.msg.facilityRequired";
		public static final String MSG_FACILITY_TYPE_REQUIRED = "receiveProperty.msg.facilityTypeRequired";

		public static final String MSG_ATTACHMENT_SAVED_SUCCESSFULLY = "receiveProperty.msg.attachmentsSaved";
		public static final String MSG_COMMENTS_SAVED_SUCCESSFULLY = "receiveProperty.msg.commentsSaved";
		public static final String MSG_OWNER_SAVED_SUCCESSFULLY = "receiveProperty.msg.ownerSaved";
		public static final String MSG_RECEIVING_TEAM_SAVED_SUCCESSFULLY = "receiveProperty.msg.receivingTeamSaved";
		public static final String MSG_EVALUATION_TEAM_SAVED_SUCCESSFULLY = "receiveProperty.msg.evaluationTeamSaved";
		public static final String MSG_PROPERTY_SAVED_SUCCESSFULLY = "receiveProperty.msg.propertySaved";
		public static final String MSG_LOCATION_SAVED_SUCCESSFULLY = "receiveProperty.msg.locationSaved";
		public static final String MSG_FACILITY_SAVED_SUCCESSFULLY = "receiveProperty.msg.facilitySaved";
		public static final String MSG_RENT_FOR_PROPERTY_SAVED_SUCCESSFULLY = "receiveProperty.msg.rentForPropertyUpdated";
		public static final String MSG_RENT_FOR_FLOOR_SAVED_SUCCESSFULLY = "receiveProperty.msg.rentForFloorUpdated";
		public static final String MSG_RENT_FOR_UNIT_SAVED_SUCCESSFULLY = "receiveProperty.msg.rentForUnitUpdated";
		public static final String MSG_SEND_FOR_EVALUATION_SUCCESSFULLY = "receiveProperty.msg.sendForEvaluation";
		public static final String MSG_SEND_FOR_APPROVAL_SUCCESSFULLY = "receiveProperty.msg.sendForApproval";
		public static final String MSG_EVALUATION_APPROVED_SUCCESSFULLY = "receiveProperty.msg.evaluationApproved";
		public static final String MSG_EVALUATION_REJECTED_SUCCESSFULLY = "receiveProperty.msg.evaluationRejected";
		public static final String MSG_APPROVED_AND_UPDATED_SUCCESSFULLY = "receiveProperty.msg.approvedAndUpdated";
		public static final String MSG_PROPERTY_UPDATED_SUCCESSFULLY = "receiveProperty.msg.propertyUpdated";
        public static final String MSG_PROPERTY_IS_NOT_AVAILABLE = "receiveProperty.msg.propertyIsNotAvailable";
        public static final String FLOORS_FROM_SMALLER_FLOOR_TO="generateFloors.messages.floorFromSmallerorEqualThanFloorTo";
	}

	public class ReceivePaymentScreen {
		public static final String paymentScreenAmountValidationError = "PaymentScreen.error.AmountValidation";

		public static final String chkDateInvalid = "collectPayment.msg.ChqDateNotValid";
		public static final String chkAmountInvalid = "collectPayment.msg.ChqAmountsNotValid";
		public static final String bankTransferAmountInvalid = "collectPayment.msg.BankTransferAmountsNotValid";
		public static final String totalAmountInvalid = "collectPayment.msg.TotalAmountsNotEqual";
		public static final String bank_required = "collectPayment.requiredBank";
		public static final String ChqNo_required = "collectPayment.requiredChqNo";
		public static final String AccNo_required = "collectPayment.requiredAccNo";
	}

	public class NOL {
		public static final String MSG_CONTRACT_HAS_EXPIRED = "nol.message.contractHasExpired";
		public static final String MSG_NOL_ERROR_OCCURRED = "nol.message.errorOccurred";
		public static final String MSG_CONTRACT_HAS_CREATED_SUCCESSFULLY = "nol.message.contractHasCreated";
		public static final String MSG_CONTRACT_HAS_APPROVED_SUCCESSFULLY = "nol.message.contractHasApproved";
		public static final String MSG_TECHNICAL_COMMENT_HAS_SUBMITTED_SUCCESSFULLY = "nol.message.commentsUpdated";
		public static final String MSG_CONTRACT_HAS_REJECTED_SUCCESSFULLY = "nol.message.contractRejected";
		public static final String MSG_COMPLETED_SUCCESSFULLY = "nol.message.completed";
		public static final String MSG_SUCCESS_PAID = "nol.message.paid";
		public static final String MSG_REQUEST_COMLETE_FAILURE = "noObjectionLetter.message.completeFailure";
		public static final String MSG_REQUEST_COMPLETE_SUCCESS = "noObjectionLetter.message.completeSuccess";
		public static final String MSG_REQUEST_CANCEL_FAILURE = "noObjectionLetter.message.cancelFailure";
		public static final String MSG_REQUEST_CANCEL_SUCCESS = "noObjectionLetter.message.cancelSuccess";

		public static final String MSG_CONFIRM_SEND = "noObjectionLetter.message.confirmSend";
		public static final String MSG_REQUEST_SENT_FAILURE = "noObjectionLetter.message.requestSentFailure";
		public static final String MSG_REQUEST_APPROVE_FAILURE = "noObjectionLetter.message.approveFailure";
		public static final String MSG_REQUEST_APPROVE_SUCCESS = "noObjectionLetter.message.approveSuccess";
		public static final String MSG_REQUEST_REJECT_FAILURE = "noObjectionLetter.message.rejectFailure";
		public static final String MSG_REQUEST_REJECT_SUCCESS = "noObjectionLetter.message.rejectSuccess";
		public static final String MSG_CONFIRM_APPROVE = "noObjectionLetter.message.confirmApprove";
		public static final String MSG_CONFIRM_REJECT = "noObjectionLetter.message.confirmReject";
		public static final String MSG_CONFIRM_TECHINCAL = "noObjectionLetter.message.confirmTechnical";
		public static final String MSG_REQUEST_TECHNICAL_FAILURE =  "noObjectionLetter.message.technicalFailure";
		

	}

	public class AuctionRefund {
		public static final String MSG_CONFISCATION_SAVE_SUCCESS = "auction.refund.messages.confiscatedAmountSaved";
		public static final String MSG_CONFISCATION_SAVE_FAILURE = "auction.refund.messages.confiscatedAmountSaveFailure";
		public static final String MSG_REQ_CONFISCATED_AMOUNT = "auction.refund.messages.confiscatedAmountRequired";
		public static final String MSG_REFUND_APPROVE_SUCCESS = "auction.refund.messages.refundApproveSuccess";
		public static final String MSG_REFUND_APPROVE_FAILURE = "auction.refund.messages.refundApproveFailure";
		public static final String MSG_REFUND_REJECT_SUCCESS = "auction.refund.messages.refundRejectSuccess";
		public static final String MSG_REFUND_REJECT_FAILURE = "auction.refund.messages.refundRejectFailure";
		public static final String MSG_CONFISCATION_VALIDATION = "auction.refund.messages.confiscationValidation";
	}

	public class UserGroup {
		public static final String PRI_NAME_REQUIRED = "group.message.groupPriRequired";
		public static final String SEC_NAME_REQUIRED = "group.message.groupSecRequired";
		public static final String GROUP_UPDATED = "group.message.groupUpdated";
		public static final String GROUP_INSERTED = "group.message.groupInserted";
		public static final String GROUP_CANNOT_BE_DELETED = "group.message.cannot.delete";
		public static final String GROUP_DELETION_NOT_ALLOWED = "group.message.delete.notAllowed";
		public static final String GROUP_DELETED = "group.message.delete.success";
		
		public static final String GROUP_PERMISSIONS_SAVED_SUCCESS= "group.permissions.message.save.success";
		public static final String GROUP_PERMISSIONS_SAVED_FAILURE = "group.permissions.message.save.failure";
	}

	public class User {
		public static final String USER_INSERTED = "user.message.userInserted";
		public static final String USER_UPDATED = "user.message.userUpdated";
		public static final String LOGIN_ID_REQUIRED = "user.message.loginIdRequired";
		public static final String USER_FULLNAME_REQUIRED = "user.message.fullNameRequired";
		public static final String USER_FIRSTNAME_REQUIRED = "user.message.firstNameRequired";
	}

	public class AuctionUnitRefund {
		public static final String SUCCESS_REFUND = "RefundAuction.RefundSuccess";
		public static final String FAILURE_REFUND = "RefundAuction.RefundFail";
		public static final String SUCCESS_PRINT = "RefundAuction.PrintSuccess";
	}

	public class ContractAdd {

		public static final String MSG_REQ_TENANT_NUM = "contract.message.requiredTenantNumber";
		public static final String MSG_REQ_UNIT_NUM = "contract.message.requiredUnitNumber";
		public static final String MSG_REQ_START_DATE = "contract.message.requiredStartDate";
		public static final String MSG_REQ_EXPIRY_DATE = "contract.message.requiredExpiryDate";
		public static final String MSG_REQ_RENT_VALUE = "contract.message.requiredRentValue";
		public static final String MSG_INVALID_RENT_VALUE = "contract.message.invalidRentValue";
		public static final String MSG_SEL_REQ_STATUS = "contract.message.selectContractStatus";
		public static final String MSG_CONTRACT_CREATED_SUCCESSFULLY = "contract.message.contractCreatedSuccessfully";
		public static final String MSG_CONTRACT_UPDATED_SUCCESSFULLY = "contract.message.contractUpdatedSuccessfully";
		public static final String MSG_CONTRACT_UNITS_INVALID = "contract.message.invalidUnitStatus";
		public static final String MSG_CONTRACT_ADD = "contract.message.contractAdded";
		public static final String MSG_CONTRACT_UPDATED = "contract.message.contractUpdated";
		public static final String MSG_REQ_SPONSOR_INFO = "contract.message.requiredSponsorInfo";
		public static final String MSG_CON_START_SMALL_EXP = "contract.message.contractStartDateSmallThanEndDate";
		public static final String MSG_COMPANY_TENANT_RESIDENTIAL_UNIT = "contract.message.CompanyTenantCannotLeaseResidentialUnit";
		public static final String MSG_INVALID_CASH = "renewContract.invalidCash";
		public static final String MSG_CON_START_SMALL_TODAY = "contract.message.contractStartDateLessThanToday";
		public static final String MSG_CON_START_END_EMPTY = "contract.message.contractStartEndDateEmpty";
		public static final String MSG_REQ_COMMERCIALACTIVITY = "contract.message.requiredCommerciialUnitActivity";
		public static final String MSG_CONTRACT_APPROVED_SUCCESSFULLY = "contract.message.contractApprovedSuccessfully";
		public static final String MSG_CONTRACT_REJECED_SUCCESSFULLY = "contract.message.contractRejectedSuccessfully";
		public static final String MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE = "contract.message.totalRentAmountNotEqualToTotalCotractValue";
		public static final String MSG_AMEND_CONTRACT_REQ_COMPLETED = "contract.msg.amendrequest.completed";
		public static final String MSG_AMEND_CONTRACT_REQ_APPROVED = "contract.msg.amendrequest.approved";
		public static final String MSG_AMEND_CONTRACT_REQ_REJECTED = "contract.msg.amendrequest.rejected";
		public static final String MSG_AMEND_CONTRACT_REQ_SAVED = "contract.msg.amendrequest.saved";
		public static final String TITLE_AMEND_CONTRACT = "contract.AmendLeaseContract";
		public static final String TITLE_NEW_LEASE_CONTRACT = "contract.LeaseContract";
		public static final String MSG_REQ_CONTRACT_TYPE = "contract.message.ContractTypeRequired";
		public static final String MSG_REQ_SENT_FOR_APPROVAL = "contract.sendForApproval";
		public static final String MSG_CONTRACT_CANCELLED_SUCCESSFULLY = "contract.message.contractCancelledSuccessfully";
	}

	public class PaymentSchedule {

		public static final String MSG_REQ_PAYMENT_TYPE = "paymentSchedule.msg.reqPaymentType";
		public static final String MSG_REQ_PAYMENT_MODE = "paymentSchedule.msg.reqPaymentMode";
		public static final String MSG_REQ_PAYMENT_DUEON = "paymentSchedule.msg.reqPaymentDueOn";
		public static final String MSG_INVALID_DATE = "paymentSchedule.msg.invalidDate";
		public static final String MSG_REQ_PAYMENT_AMOUNT = "paymentSchedule.msg.reqPaymentAmount";
		public static final String MSG_PAYMENT_MODE_REF_NUM = "paymentSchedule.msg.reqPaymentModeRefNum";
		public static final String MSG_REQ_BANK_NUM = "paymentSchedule.msg.reqBankNum";
		public static final String MSG_INVALID_RENT_VALUE = "paymentSchedule.msg.invalidRentValue";
		public static final String MSG_REQ_ONE_PAYMENT_DUEON = "paymentSchedule.ReqPaymentDueMode";
		public static final String MSG_REQ_MILESTONE = "paymentSchedule.reqMileStone";
		public static final String MSG_REQ_PERCENTAGE = "paymentSchedule.reqCompletionPercent";
		public static final String MSG_REQ_DESCRIPTION = "paymentSchedule.reqPaymentDescription"; 
		public static final String MSG_PAYMENT_ACCEPT_SUCCESS = "paymentSchedule.acceptSuccess";
		public static final String MSG_PAYMENT_ACCEPT_FAILURE = "paymentSchedule.acceptFailure";
		

	}

	public class ViolationDetails {

		public static final String MSG_DATE_FORMAT = "violationDetails.msg.dateFormat";
		public static final String MSG_DESC_SIZE = "violationDetails.msg.descriptionSize";
		public static final String MSG_DAMAGE_FORMAT = "violationDetails.msg.doubleFormat";
		public static final String MSG_DESC_NULL = "violationDetails.msg.descriptionNull";
		public static final String MSG_SELECT_CATEGORY = "violationDetails.msg.plsSelctCategory";
		public static final String MSG_SELECT_TYPE = "violationDetails.msg.plsSelectType";
		public static final String MSG_SELECT_UNIT = "violationDetails.msg.plsSelectUnit";
		public static final String MSG_ADD_VIOLATION = "violationDetails.msg.AddViolation";
		public static final String MSG_VIOLATION_TYPE_ADDED = "violationDetails.msg.ViolationTypeAdded";
		public static final String MSG_VIOLATION_DESC_REQ="violationDetails.msg.descriptionRequired";
		public static final String MSG_VIOLATION_DESC_EN_REQ="violationDetails.msg.descriptionRequiredEn";
		public static final String MSG_VIOLATION_DESC_AR_REQ="violationDetails.msg.descriptionRequiredAr";
		public static final String MSG_BLACKLIST_REASON_REQUIRED="violationDetails.msg.BlacklistReasonRequired";		
		public static final String MSG_BLACKLIST_CHECK="violationDetails.msg.BlacklistCheckRequired";
		public static final String MSG_BLACKLIST_CHECK_DESC="violationDetails.msg.BlacklistDescCheckRequired";
	}

	public class ViolationActions {

		public static final String MSG_ACTION = "violationAction.msg.plsSelectAction";
		public static final String MSG_ENTER_FINE_AMOUNT = "violationAction.msg.plsEnterFineAmount";
		public static final String MSG_FINE_ACTION = "violationAction.msg.plsSelectFineAction";
		public static final String MSG_FINE_FORMAT = "violationAction.msg.invalidFineAmount";
		public static final String MSG_REASON = "violationAction.msg.plsEnterReason";
		public static final String MSG_ALREADY_BLACKLISTED = "violationAction.msg.alreadyBlackListed";

	}

	public class ClearanceLetter {
		public static final String MSG_CONFIRM_SEND = "clearanceLetter.message.confirmSend";
		public static final String MSG_REQUEST_SENT = "clearanceLetter.message.requestSent";
		public static final String MSG_REQUEST_SENT_FAILURE = "clearanceLetter.message.requestSentFailure";
		public static final String MSG_REQUEST_APPROVE_FAILURE = "clearanceLetter.message.approveFailure";
		public static final String MSG_REQUEST_APPROVE_SUCCESS = "clearanceLetter.message.approveSuccess";
		public static final String MSG_REQUEST_REJECT_FAILURE = "clearanceLetter.message.rejectFailure";
		public static final String MSG_REQUEST_REJECT_SUCCESS = "clearanceLetter.message.rejectSuccess";
		public static final String MSG_CONFIRM_APPROVE = "clearanceLetter.message.confirmApprove";
		public static final String MSG_CONFIRM_REJECT = "clearanceLetter.message.confirmReject";
		public static final String MSG_CONTRACT_NOT_EXPIRED = "clearanceLetter.message.contractNotExpired";
		public static final String MSG_LETTER_IS_PRINTED = "clearanceLetter.message.printed";
		public static final String MSG_LETTER_IS_RE_PRINTED = "clearanceLetter.message.rePrinted";
		public static final String MSG_NO_CONTRACT_SELECTED = "clearanceLetter.message.noContractSelected";
		public static final String MSG_NO_CONTRACT_FOUND = "clearanceLetter.message.noContractFound";
		public static final String MSG_REQUEST_COMLETE_FAILURE = "clearanceLetter.message.completeFailure";
		public static final String MSG_REQUEST_COMPLETE_SUCCESS = "clearanceLetter.message.completeSuccess";
		public static final String MSG_REQUEST_CANCEL_FAILURE = "clearanceLetter.message.cancelFailure";
		public static final String MSG_REQUEST_CANCEL_SUCCESS = "clearanceLetter.message.cancelSuccess";
	}

	public class Tender {
		public static final String MARK_AS_WINNER="tenderManagement.messages.markedAsWinner";
		public static final String MSG_INVALID_DATE="tenderManagement.messages.invalidDate";
		public static final String MSG_EXPECTED_START_DATE_REQUIRED ="tenderManagement.messages.startDateRequired";
		public static final String MSG_EXPECTED_END_DATE_REQUIRED ="tenderManagement.messages.endDateRequired";
		public static final String MSG_CONTRACTORS_LIST = "tenderManagement.messages.contractorRequired";
		public static final String MSG_SCOPE_OF_WORK_LIST="tenderManagement.messages.scopeOfWorkRequired";
		public static final String MSG_UNIT_REQUIRED="tenderManagement.messages.unitRequired";
		public static final String MSG_TENDER_TYPE_REQUIRED="tenderManagement.messages.tenderTypeRequired";
		public static final String MSG_EXPECTED_COST_REQUIRED="tenderManagement.messages.expectedCostRequired";
		public static final String MSG_TENDER_PRICE_REQUIRED="tenderManagement.messages.tenderPriceRequired";
		public static final String MSG_PROPERTY_INVALID_TENDER_PRICE="tenderManagement.messages.invalidTenderPriceRequired";
		public static final String MSG_PROPERTY_INVALID_EXPECTED_COST="tenderManagement.messages.invalidExpectedCostRequired";
		
		
		
		public static final String MSG_CONTRACTOR_ALREADY_SELECTED="tenderManagement.messages.contractorAlreadySelected";
		public static final String MSG_SUCCESS_REJECTED="tenderManagement.messages.tenderRejectedSuccess";
		public static final String MSG_SUCCESS_APPROVED="tenderManagement.messages.tenderApprovedSuccess";
		public static final String MSG_SEND_FOR_EVALUATION_SUCCESSFULLY="tenderManagement.msg.sendForEvaluation";
		public static final String MSG_SAVED_SUCCESSFULLY="tenderManagement.messages.savedSuccess";
		public static final String MSG_UPDATED_SUCCESSFULLY="tenderManagement.messages.updatedSuccess";
		public static final String MSG_TENDER_CANCEL_SUCCESS = "tenderManagement.messages.tenderCancelSuccess";
    	public static final String MSG_TENDER_CANCEL_FAILURE = "tenderManagement.messages.tenderCancelFailure";
    	public static final String MSG_TENDER_REOPEN_SUCCESS = "tenderManagement.messages.tenderReopenSuccess";
    	public static final String MSG_TENDER_REOPEN_FAILURE = "tenderManagement.messages.tenderReopenFailure";
    	public static final String MSG_TENDER_DELETE_SUCCESS = "tenderManagement.messages.tenderDeleteSuccess";
    	public static final String MSG_TENDER_DELETE_FAILURE = "tenderManagement.messages.tenderDeleteFailure";
    	public static final String PROPERTY_ALREADY_EXIST = "tenderManagement.messages.propertyAlreadyExist";
    	public static final String CONTRACTOR_ALREADY_EXIST = "tenderManagement.messages.contractorAlreadyExist";
    	public static final String MULTIPLE_CONTRACTOR_ALREADY_EXIST = "tenderManagement.messages.multipleContractorAlreadyExist";

	}
	
	public interface ReceiveTenderProposals{
		
    	public static final String MSG_PROPOSAL_SAVE_SUCCESS = "receiveTenderProposals.messages.saveSuccess";
    	public static final String MSG_PROPOSAL_SAVE_FAILURE = "receiveTenderProposals.messages.saveFailure";

	}
	public interface PurchaseTenderDocs{
		
    	public static final String MSG_TENDER_PURCHASED_SUCCESSFULLY = "purchaseTenderDocs.messages.saveSuccess";
    	

	}
	
	public class ExtendApplication{
    	public static final String MSG_SELECT_CONTRACTOR = "extendApplication.details.messages.selectContractor";
    	public static final String MSG_SELECT_TENDER = "extendApplication.details.messages.selectTender";
    	public static final String MSG_REQUEST_CREATE_SUCCESS = "extendApplication.details.messages.requestCreateSuccess";
    	public static final String MSG_REQUEST_SAVE_FAILURE = "extendApplication.details.messages.requestSaveFaliure";
    	public static final String MSG_REQUEST_SAVE_SUCCESS = "extendApplication.details.messages.requestSaveSuccess";
    	public static final String MSG_REQUEST_SUBMIT_SUCCESS = "extendApplication.details.messages.requestSubmitSuccess";
    	public static final String MSG_REQUEST_SUBMIT_FAILURE = "extendApplication.details.messages.requestSubmitFailure";
    	public static final String MSG_REQUEST_DELETE_SUCCESS = "extendApplication.search.messages.requestDeleteSuccess";
    	public static final String MSG_REQUEST_DELETE_FAILURE = "extendApplication.search.messages.requestDeleteFailure";
    	public static final String MSG_REQUEST_APPROVE_SUCCESS = "extendApplication.details.messages.requestApproveSuccess";
    	public static final String MSG_REQUEST_APPROVE_FAILURE = "extendApplication.details.messages.requestApproveFailure";
    	public static final String MSG_REQUEST_REJECT_SUCCESS = "extendApplication.details.messages.requestRejectSuccess";
    	public static final String MSG_REQUEST_REJECT_FAILURE = "extendApplication.details.messages.requestRejectFailure";
    	public static final String MSG_REQUEST_PUBLISH_SUCCESS = "extendApplication.details.messages.requestPublishSuccess";
    	public static final String MSG_REQUEST_PUBLISH_FAILURE = "extendApplication.details.messages.requestPublishFailure";
    	public static final String MSG_REQUEST_FOLLOW_UP_SUCCESS = "extendApplication.details.messages.requestFollowUpSuccess";
    	public static final String MSG_REQUEST_FOLLOW_UP_FAILURE = "extendApplication.details.messages.requestFollowUpFailure";
    	public static final String MSG_NEW_CLOSURE_DATE_INVALID = "extendApplication.details.messages.newClosureDateInvalid";
    	public static final String MSG_REQUEST_CANCEL_SUCCESS = "extendApplication.details.messages.requestCancelSuccess";
    	public static final String MSG_REQUEST_CANCEL_FAILURE = "extendApplication.details.messages.requestCancelFailure";
	}
	 
	public class EvaluationApplication{
    	public static final String MSG_SELECT_PROPERTY = "evaluationApplication.details.messages.selectProperty";
    	public static final String MSG_REQUEST_CANCEL_SUCCESS = "evaluationApplication.search.messages.requestCancelSuccess";
    	public static final String MSG_REQUEST_REJECTED_SUCCESS = "evaluationApplication.search.messages.requestRejectSuccess";
    	public static final String MSG_REQUEST_CANCEL_FAILURE = "evaluationApplication.search.messages.requestCancelFailure";
    	public static final String MSG_REQUEST_DELETE_SUCCESS = "evaluationApplication.search.messages.requestDeleteSuccess";
    	public static final String MSG_REQUEST_DELETE_FAILURE = "evaluationApplication.search.messages.requestDeleteFailure";
    	public static final String MSG_REQUEST_SAVE_FAILURE = "evaluationApplication.details.messages.requestSaveFaliure";
    	public static final String MSG_REQUEST_SAVE_SUCCESS = "evaluationApplication.details.messages.requestSaveSuccess";
    	public static final String MSG_EVALUATE_SUCCESS = "evaluationApplication.details.messages.evaluateSuccess";
    	public static final String MSG_EVALUATE_FAILURE = "evaluationApplication.details.messages.evaluateFailure";
    	public static final String MSG_SITE_VISIT_SUCCESS = "evaluationApplication.details.messages.siteVisitSuccess";
    	public static final String MSG_SITE_VISIT_FAILURE = "evaluationApplication.details.messages.siteVisitFailure";
    	public static final String MSG_REQUEST_CREATE_SUCCESS = "evaluationApplication.details.messages.requestCreateSuccess";
    	public static final String MSG_REQUEST_SUBMIT_FAILURE = "evaluationApplication.details.messages.requestSubmitFailure";
    	public static final String MSG_REQUEST_ON_SITE_VISIT_SUCCESS = "evaluationApplication.details.messages.onSiteVisitSuccess";
    	public static final String MSG_REQUEST_ON_SITE_VISIT_FAILURE = "evaluationApplication.details.messages.onSiteVisitFailure";
    	public static final String MSG_REQUEST_SENT_TO_HOS= "propertyEvaluation.infoMessage.sentToHOS";
    	
	}
	
	public class AuctionRegistration{
    	public static final String MSG_COMPLETE_PAYMENT_RECEIVED = "auction.registration.paidEqualAmount";
    	public static final String MSG_PAYMENT_NOT_RECEIVED = "auction.registration.paymentNotReceived";
    	public static final String MSG_INCOMPLETE_PAYMENT_RECEIVED = "auction.registration.paidLessAmount";
    	public static final String MSG_REQUEST_SAVED = "auction.registration.requestSaved";
    	public static final String MSG_REQUEST_NOT_SAVED = "auction.registration.requestNotSaved";
    	public static final String MSG_RECEIPT_REPRINTED = "auction.registration.receiptReprint";    	
    	public static final String MSG_OPERATION_NOT_ALLOWED = "auction.registration.operationNotAllowed";
    	public static final String MSG_SELECT_BIDDER = "auction.registration.selectBidder";
    	public static final String MSG_SELECT_AUCTION = "auction.registration.selectAuction";
    	public static final String MSG_SELECT_UNIT_TO_PAY = "auction.registration.selectUnitToPay";
    	public static final String MSG_NO_UNITS_TO_REGISTER = "auction.registration.noUnitsToRegister";
    	public static final String MSG_DEADLINE_CROSSED = "auction.registration.registrationDeadlineCrossed";
    	public static final String MSG_REGISTRATION_NOT_YET_STARTED = "auction.registration.registrationNotYetStarted";
    	public static final String MSG_GENERATE_PAYMENT_SCHEDULE_FAILED = "auction.registration.paymentScheduleGenerationFailure";
    	public static final String MSG_CREATE_REQUEST_FAILED = "auction.registration.requestCreationFailure";
    	public static final String MSG_UPDATE_REQUEST_FAILED = "auction.registration.requestUpdateFailure";
    	public static final String MSG_AUCTION_REGISTER = "auction.register";
	}

	public class InspectionDetails {

		public static final String MSG_REQ_DATE = "inspection.messages.dateRequired";
		public static final String MSG_REQ_TYPE = "inspection.messages.typeRequired";
		public static final String MSG_REQ_STATUS = "inspection.messages.statusRequired";
		public static final String MSG_REQ_UNIT = "inspection.messages.unitRequired";
		public static final String MSG_REQ_TEAM = "inspection.messages.teamRequired";
		public static final String MSG_REQ_TEAM_MEMBER = "inspection.messages.teamMemberRequired";
		public static final String MSG_REQ_TEAM_NAME = "inspection.messages.teamNameRequired";
		public static final String MSG_SUCCESS_ADD = "inspection.added.successfully";
		public static final String MSG_SUCCESS_UPDATED = "inspection.updated.successfully";
		public static final String MSG_UNITS_NOT_AVAILABLE_INSPECTION = "inspection.messages.unitsAvailableForInspection";
		public static final String MSG_UNITS_NOT_SELECTED = "inspection.messages.addViolationValidation";
		

	}

	public class RenewContract {
		public static final String MSG_CONTRACT_VALUE = "renewContract.MSG.contractValueNotCorrect";
		public static final String MSG_MNT_NOT_FOUND = "renewContract.MSG.maintanceNotFound";
		public static final String MSG_REQUEST_UPDATED = "renewContract.message.requestUpdated";
		public static final String MSG_TASK_SENT = "renew.msg.TaskSent";
		public static final String MSG_TASK_APPROVED = "renew.msg.TaskApproved";
		public static final String MSG_TASK_REJECTED = "renew.msg.TaskRejected";
		public static final String MSG_CONTRACT_SAVED = "renew.msg.ContractSaved";
		public static final String MSG_PAYMENT_RECIVED = "commons.msg.paymentRecivedSuccessfully";
		public static final String MSG_CONTRACT_NOT_SAVED = "renewContract.MSG.notSavedAsDraft";
		public static final String MSG_REQ_COMPLETED = "replaceCheque.msg.requestCompleted";
		public static final String MSG_APPLICANT_NOT_FOUND = "renew.msg.applicantNotFound";
		public static final String MSG_PAY_FEES_FIRST = "renew.msg.firstPayTheFees";
		public static final String MSG_PAY_OLD_PAYENTS = "renew.msg.oldPaymentsExists";
		public static final String MSG_PAY_EXISTING_PAYENTS = "renew.msg.paymentsExists";
		public static final String MSG_OLD_CONTRACT_NOT_TERMINATED = "renew.msg.oldContractNotTerminated";
		public static final String MSG_COLLECT_ALL_PAYMENTS="renew.msg.collectAllPayments";
		public static final String MSG_INVALID_UNIT_RENT="renewContract.MSG.invalidUnitRentValue";
		public static final String MSG_REQUIRED_UNIT_RENT="renewContract.MSG.reqUnitRentValue";
		

	}

	public class GeneratePayments {
		public static final String MSG_CASH = "generatePayments.msg.invalidCashValue";
		public static final String MSG_CHQ = "generatePayments.msg.invalidChqValue";
		public static final String MSG_DATE = "generatePayments.msg.date";
		public static final String RENT_DESCRIPTION = "generatePayments.RentDescription";
		public static final String MSG_PAYMENTSTARTDATE_LESSTHAN_CONTRACTSTARTDATE ="generateServiceContractPayment.msg.paymentStarLessThanContractStart";
		public static final String MSG_PAYMENTSTARTDATE_GREATERTHAN_CONTRACTENDDATE="generateServiceContractPayment.msg.paymentStarGreatThanContractStart";
		public static final String MSG_CASH_CHQ_EQUAL_INSTALLMENTS = "generatePayments.ChqCashEqualNoOfInstallments";
	}

	public class ConductAuction {
		public static final String MSG_INCOMPLETE_INFO = "auction.conductAuction.msgs.incompleteInfo";
		public static final String MSG_BIDDER_NOT_SELECTED = "auction.conductAuction.msgs.bidderNotSelected";
		public static final String MSG_INVALID_BID_PRICE = "auction.conductAuction.msgs.invalidBidPrice";
		public static final String ATTENDANCE_SAVED = "auction.conductAuction.msgs.attendanceSaved";
		public static final String UNIT_EXCLUSION_SAVED = "auction.conductAuction.msgs.unitStatusSaved";
		public static final String WINNER_SAVED = "auction.conductAuction.msgs.winnerMarked";
		public static final String AUCTION_COMPLETE_FAILURE = "auction.conductAuction.msgs.auctionCompletedFailure";
		public static final String AUCTION_COMPLETE_SUCCESS = "auction.conductAuction.msgs.auctionCompletedSuccess";
		public static final String AUCTION_UPDATE_SUCCESS = "auction.conductAuction.msgs.auctionUpdateSuccess";
		public static final String BIDDERS_ABSENT_MARKED = "auction.conductAuction.msgs.markBiddersAbsent";
		public static final String BIDDERS_PRESENT_MARKED = "auction.conductAuction.msgs.markBiddersPresent";
		public static final String MULTI_BIDDERS_ERROR = "auction.conductAuction.msgs.multiBidderError";
		public static final String NO_BIDDERS_SELECTED = "auction.conductAuction.msgs.noBiddersSelected";
		public static final String BIDDER_WINNER_MARKED = "auction.conductAuction.msgs.bidderWinnerMarked";
		public static final String MOVE_TO_CONTRACT_SUCCESS = "auction.conductAuction.msgs.moveToContractSuccess";
		public static final String MOVE_TO_CONTRACT_FAILURE = "auction.conductAuction.msgs.moveToContractFailure";
		public static final String CONTRACT_NOT_SIGNED_SUCCESS = "auction.conductAuction.msgs.contractNotSignedSuccess";
		public static final String CONTRACT_NOT_SIGNED_FAILURE = "auction.conductAuction.msgs.contractNotSignedFailure";
		public static final String MARKED_AS_REFUNDED_SUCCESS = "auction.conductAuction.msgs.markAsRefundedSuccess";
		public static final String MARKED_AS_REFUNDED_FAILURE = "auction.conductAuction.msgs.markAsRefundedFailure";
		public static final String CANNOT_COMPLETE_AUCTION = "auction.conductAuction.msgs.cannotCompleteAuction";
		public static final String ALL_WINNERS_NOT_SELECTED = "auction.conductAuction.msgs.allWinnersNotSelected";
	}

	public class ChangeTenantName {
		public static final String MSG_SUCCESS_ADD = "changeTenantName.msg.savedsuccessfully";
		public static final String MSG_SUCCESS_UPDATED = "changeTenantName.msg.updatedsuccessfully";
		public static final String MSG_BASIC_INFO_PERSON_NOT_ADDED ="changeTenantName.msg.personNotAdded";
		public static final String MSG_SAME_CURRENTANDNEWTENANT = "changeTenantName.msg.newtenantCurrenttenantsame";
		public static final String MSG_REQ_NEW_TENANT = "changeTenantName.msg.newTenantRequired";
		public static final String MSG_SUCCESS_APPROVED = "changeTenantName.msg.approved";
		public static final String MSG_SUCCESS_REJECTED = "changeTenantName.msg.rejected";
		public static final String MSG_SUCCESS_COMPLETED = "changeTenantName.msg.completed";
		public static final String MSG_SUCCESS_PAY = "changeTenantName.msg.Paymentsuccessfully";
	}

	public interface SearchAuction {
		public static final String MSG_START_DATE_FROM_TO_INVALID = "auction.messages.requestStartDateFromToInvalid";
		public static final String MSG_END_DATE_FROM_TO_INVALID = "auction.messages.requestEndDateFromToInvalid";
		public static final String MSG_CANCEL_AUCTION_SUCCESS = "auction.messages.cancelSuccess";
		public static final String MSG_CANCEL_AUCTION_FAILURE = "auction.messages.cancelFailure";
		
	}
	
	public class PrepareAuction {

		public static final String MSG_AUCTION_APPROVE_SUCCESS = "auction.messages.approveSuccess";
		public static final String MSG_AUCTION_APPROVE_FAILURE = "auction.messages.approveFailure";
		public static final String MSG_AUCTION_REJECT_SUCCESS = "auction.messages.rejectSuccess";
		public static final String MSG_AUCTION_REJECT_FAILURE = "auction.messages.rejectFailure";
		public static final String MSG_AUCTION_PUBLISH_SUCCESS = "auction.messages.publishSuccess";
		public static final String MSG_AUCTION_PUBLISH_FAILURE = "auction.messages.publishFailure";
		public static final String MSG_NOTFICATION_FAILURE = "auction.messages.notificationFailure";
		public static final String NO_UNIT_SELECTED_TO_APPLY_DEPOSIT = "auction.messages.noUnitSelectedToApplyDeposit";
		public static final String NO_UNIT_SELECTED_TO_APPLY_OPENING_PRICE = "auction.messages.noUnitSelectedToApplyOpeningPrice";
		public static final String AUCTION_UNIT_AMOUNTS_NOT_ENTERED = "auction.messages.amountsNotEntered";
		public static final String BASED_ON_NOT_SELECTED = "auction.messages.basedOnNotSelected";
		public static final String MSG_CONFISCATION_AMOUNT_INVALID = "auction.messages.confiscationInvalid";
		public static final String MSG_AUCTION_DATE_INVALID = "auction.messages.auctionDateInvalid";
		public static final String MSG_REQUEST_END_DATE_INVALID = "auction.messages.requestEndDateInvalid";
		public static final String MSG_REQUEST_START_DATE_INVALID = "auction.messages.requestStartDateInvalid";
		public static final String MSG_OUTBIDDING_VALUE_INVALID = "auction.messages.outbiddingValueInvalid";
		public static final String MSG_OPENING_PRICE_INVALID = "auction.messages.openingPriceInvalid";
	}

	public class TransferLeaseContract {
		public static final String MSG_REQ_SAVE = "transferContract.messages.reqSaved";
		public static final String MSG_REQ_UPDATE = "transferContract.messages.reqUpdated";
		public static final String MSG_REQ_SENT_FOR_APPROVAL = "transferContract.messages.reqSentForApproval";
		public static final String MSG_DATE_LT_EVA_DATE = "transferContract.messages.newDateLtEvacuationDate";
		public static final String MSG_DATE_LT_START_DATE = "transferContract.messages.newDateLtStartDate";
		public static final String MSG_TRANSFER_CONTRACT_VERIFIED="transferContract.messages.contractHasVerified";
		public static final String 	MSG_INCORRECT_START_DATE="transferContract.messages.incorrectStartDate";
		public static final String 	MSG_INCORRECT_END_DATE="transferContract.messages.incorrectEndDate";
		public static final String MSG_INCORRECT_END_DATE_GT_START_DATE = "transferContract.messages.endDateGTStartDate";
		public static final String MSG_INCORRECT_NEW_RENT_AMOUNT ="transferContract.messages.incorrectNewRentAmount";
		public static final String MSG_TOTAL_PAYMENTS = "transferContract.messages.totalPayments";
		public static final String MSG_NOTFICATION_FAILURE = "transferContract.messages.notificationFailure";
		public static final String MSG_SUCCESS_CONTRACT_ISSUE_REJECTED = "transferContract.messages.issuedContractRejected";
		public static final String MSG_SUCCESS_CONTRACT_ISSUE_APPROVED = "transferContract.messages.issuedContractApproved";
		public static final String MSG_SUCCESS_CONTRACT_ISSUE = "transferContract.messages.issueContractSuccess";
		public static final String MSG_VALIDATION_FAILED_NO_OF_PAYMENTS = "transferContract.messages.validationFailedNoOfPayments";
		public static final String MSG_SUBMIT_FAILURE = "transferContract.messages.requestCreationException";
		public static final String SETTLEMENT_DATE_INVALID = "transferContract.messages.settlementDateInvalid";
		public static final String MSG_SUCCESS_APPROVED = "transferContract.messages.approvedSuccess";
		public static final String MSG_SUCCESS_REJECTED = "transferContract.messages.rejectSuccess";
		public static final String MSG_FAILURE_APPROVED = "transferContract.messages.approvedFailure";
		public static final String MSG_FAILURE_REJECTED = "transferContract.messages.rejectFailure";
		public static final String MSG_FAILURE_ISSUE_CONTRACT = "transferContract.messages.issueContractFailure";
		public static final String NEW_RENT_SUCCESS = "transferContract.messages.newRentSuccess";
		public static final String NEW_RENT_FAILURE = "transferContract.messages.newRentFailure";
		public static final String MSG_INCORRECT_PAYMENT_RECEIVED = "transferContract.messages.incorrectAmtReceived";
		public static final String MSG_TRANSFER_CONTRACT_FEE_COLLECTED = "transferContract.messages.feeCollected";
		public static final String MSG_SETTLEMENT_TASK_ADDED = "transferContract.messages.settlementTaskAdded";
		public static final String MSG_TRANSFER_CONTRACT_SETTLED = "transferContract.messages.settled";
		public static final String MSG_TRANSFER_CONTRACT_REQUEST_ALREADY_EXISTS = "transferContract.messages.requestExists";
		public static final String MSG_TRANSFER_CONTRACT_CLEARANCE_LETTER_PRINTED = "clearanceLetter.message.printed";
		public static final String MSG_EVACUATION_DATE_MUST_BE_SAVED_B4_SENDING ="transferContract.evacuationBeforeSendingMsg";
		public static final String MSG_RENT_VALUE_VERIFIED  = "requestHistory.msg.rentValueVerfied";
		public static final String MSG_OLD_PAYMENT_COLLECTED="requestHistory.msg.oldPaymentsCollected";
		public static final String MSG_NEW_PAYMENT_COLLECTED="requestHistory.msg.newPaymentsCollected";
	}

	public class CancelContract {
		public static final String NOL_PRINTED = "cancelContract.message.nolIssued";
		public static final String NOL_NOT_PRINTED = "cancelContract.message.nolNotIssued";
		public static final String SETTLEMENT_PRINTED = "settlement.message.settlementPrinted";
		public static final String RECEIPT_PRINTED = "settlement.message.receiptPrinted";
		public static final String PAY_FIRST = "cancelContract.message.payFirst";
		public static final String SELECT_AT_LEAST_ONE_PAYMENT = "cancelContract.message.selectAtLeastOnePayment";
		public static final String SENT_FOR_FOLLOW_UP = "cancelContract.message.sentForFollowUp";
		public static final String SENT_FOR_COLLECT_PAYMENT = "cancelContract.message.sentForCollectPayment";
		public static final String LEGAL_DEPT_REVIEW_DONE = "cancelContract.message.legalReviewDone";
	}

	public class CommonMessage {
		public static final String DOCUMENTS_SAVED = "common.messages.documentSaved";
		public static final String NOTES_SAVED = "common.messages.noteSaved";
		public static final String RECORD_UPDATED="common.messages.Updated";
		public static final String RECORD_SAVED="common.messages.Save";
		public static final String BLACKLIST_DESC_TOO_LONG="common.msg.toLongDescription";
	}

	public class CommonErrorMessage {

		public static final String MSG_COMMON_ERROR = "commons.ErrorMessage";
		public static final String MSG_REQUIRED_REMARKS = "commons.RequiredRemarks";
		public static final String ERROR_MSG_CONTRACT_REQUIERD="contractor.message.contactReferencesRequired";
		public static final String ERROR_MSG_TOTAL_VALUE_NOT_PRESENT="common.message.requiredFieldsNotPresentForPayments";
	}

	public class Person {
		public static final String MSG_LAST_NAME = "person.message.lastName";
		public static final String MSG_FIRST_NAME = "person.message.firstName";
		public static final String MSG_GENDER = "person.message.gender";
		public static final String MSG_TITLE = "person.message.title";
		public static final String MSG_DATE_OF_BIRTH = "person.Msg.DateOfBirth";
		public static final String MSG_NATIONALITY_REQ = "person.Msg.NationalityRequired";
		public static final String MSG_COMPANY_INFO_REQ = "person.Msg.CompanyInfoRequired";
		public static final String MSG_COMPANY_LICENSE_ISSUE_PLACE_REQ = "person.Msg.CompanyLicenseIssuePlaceRequired";
		public static final String MSG_ADDED_SUCESS = "person.Msg.addedSuccessfully";
		public static final String MSG_CONTACT_INFO_REQ = "person.Msg.ContactInfoRequired";
		public static final String MSG_ROLE_INFO_REQ = "person.Msg.RoleInfoRequird";
		public static final String MSG_INVALID_EMAIL = "person.Msg.InvalidEmailFormat";
		public static final String MSG_ADDRESS_INFO_REQ = "person.Msg.AddressInfoRequired";
		public static final String MSG_CONTACT_REF_FIRST_NAME = "person.Msg.contactReference.firstName";
		public static final String MSG_CONTACT_REF_LAST_NAME = "person.Msg.contactReference.lastName";
		public static final String MSG_CONTACT_REF_INVALID_EMAIL = "person.Msg.contactReference.InvalidEmailFormat";
		public static final String MSG_CONTACT_REF_ADDRESS_INFO_REQ = "person.Msg.contactReference.AddressInfoRequired";
		public static final String MSG_CONTACT_REF_TITLE = "person.Msg.contactReference.title";
		public static final String MSG_EMAIL_REQUIRED="person.Msg.emailRequired";
		public static final String MSG_ID_GENERATED="person.Msg.idGenerated";
		public static final String MSG_EMAIL_MANDATORY_FOR_ACTIVATION = "person.Msg.Email.Activation.Required";
		public static final String MSG_GRP_ALREADY_EXISTS = "person.message.GRPAlreadyExist";
		public static final String MSG_GRP_CUSTOMER_NUMBER_REQ = "person.message.GRPCustomerNumberRequired";
	}

	public class General {
		public static final String ATTACHMENT_FAILURE = "attachment.messages.failure";
		public static final String COMMENTS_FAILURE = "notes.messages.failure";
		public static final String OPERATION_FAILURE = "commons.operation.failed";
	}

	public class Authentication {
		public static final String AUTHENTICATION_FALIURE = "authentication.failed";

	}

	public class RequestEvents {
		public static final String REQUEST_REVIEWED= "request.event.RequestReviewed";
		public static final String REQUEST_CREATED = "request.event.RequestCreated";
		public static final String REQUEST_RESUBMITTED = "request.event.RequestResubmitted";
		public static final String REQUEST_APPROVED = "request.event.RequestApproved";
		public static final String REQUEST_REJECTED = "request.event.RequestRejected";
		public static final String REQUEST_TECHNICAL_COMMENT_ADDED = "request.event.TechnicalCommentAdded";
		// for cancel contract
		public static final String REQUEST_NOL_PRINTED = "request.event.nolIssued";
		public static final String REQUEST_INSPECTION_DONE = "request.event.inspectionDone";
		public static final String REQUEST_SETTLEMENT_DONE = "request.event.settlementDone";
		public static final String REQUEST_NOTIFICATION_DONE = "request.event.notificationDone";
		public static final String REQUEST_CL_ISSUED = "request.event.CLIssued";
		public static final String REQUEST_PAYMENT_COLLECTED_ONLY = "request.event.RequestPaymentCollectedOnly";
		//
		public static final String AUCTION_CREATED = "auction.event.created";
		public static final String AUCTION_SAVED = "auction.event.saved";
		public static final String AUCTION_SENT_FOR_APPROVAL = "auction.event.sentForApproval";
		public static final String AUCTION_APPROVED = "auction.event.approved";
		public static final String AUCTION_REJECTED = "auction.event.rejected";
		public static final String AUCTION_PUBLISHED = "auction.event.published";

		public static final String REQUEST_COMPLETED = "request.event.RequestCompleted";
		public static final String REQUEST_CANCELED = "request.event.RequestCanceled";
		public static final String REQUEST_PUBLISHED = "request.event.RequestPublished";
		public static final String REQUEST_FOLLOW_UP_DONE = "request.event.FollowUpDone";
		
		public static final String REQUEST_PROPERTY_EVALUATED = "request.event.propertyEvaluated";
		public static final String REQUEST_SITE_VISIT_DONE = "request.event.siteVisitDone";
		public static final String REQUEST_SENT_FOR_EVALUATION = "request.event.sentForEvaluation";
		public static final String REQUEST_ON_SITE_VISIT = "request.event.onSiteVisit";
		public static final String REQUEST_SAVED_SITE_VISIT = "request.event.savedSiteVisit";
		public static final String REQUEST_SAVED_FOLLOWUP = "request.event.savedFollowUp";
		public static final String REQUEST_SENT_FOR_INSPECTOR_RECOMM = "request.event.sentForInspectorRecommendation";
		public static final String REQUEST_SENT_FOR_FOLLOWUP = "request.event.sentForFollowUp";
		public static final String REQUEST_LEGAL_REVIEW_DONE = "request.event.legalReviewDone";
		public static final String REQUEST_SAVED = "request.event.requestSaved";
		public static final String REQUEST_SENT_FOR_APPROVAL = "request.event.sentForApproval";
		public static final String REQUEST_UPDATED = "request.event.requestUpdated";
		public static final String REQUEST_SITE_VISIT_REQ= "request.event.requestSiteVisitReq";
		public static final String REQUEST_COLLECT = "request.event.RequestCollect";
		public static final String REQUEST_PAYMENT_COLLECTED = "request.event.RequestPaymentCollect";
		public static final String REQUEST_RETURN = "request.event.RequestReturn";
		public static final String INSPECTION_REQUIRED = "request.event.InspectionRequired";
		public static final String EVALUATION_HOS_RECOMMENDATION_DONE = "propertyEvaluation.hosRecommendationDone";
		public static final String EVALUATION_PROP_MGT_EXEC_RECOMMENDATION_DONE = "propertyEvaluation.propMgtExecRecommendationDone";
		public static final String REQUEST_SENT_FOR_COLLECT_PAYMENT= "request.event.requestSentForCollectPayment";
		public static final String REQUEST_INITIAL_LIMITATION_DONE = "request.event.initialLimDone";
		public static final String REQUEST_FINAL_LIMITATION_DONE = "request.event.finalLimDone";
		public static final String REQUEST_INITIAL_LIMITATION_REJECTED= "request.event.initLimRejected";
		public static final String REQUEST_PAYMENT_SENT_FOR_JUDGE = "request.sent.judge";
		public static final String REQUEST_PAYMENT_JUDGE_DONE = "request.judge.done";
		public static final String REQUEST_SENT_FOR_DISBURSEMENT = "request.event.disbursement";
		public static final String REQUEST_COMPLETED_AND_DISBURSEMENT_CREATED = "successMsg.requestCompletedAndDisCreated";
		public static final String REQUEST_DISTRIBUTION_DONE = "request.event.distributionDone";
		public static final String FILE_BLOCKED = "systemNotes.fileBlocked";
		public static final String FILE_UNBLOCKED= "systemNotes.fileUnBlocked";
		public static final String REQUEST_SENT_TO_JUDGE = "request.event.sentToJudge";
		public static final String REQUEST_JUDGE_DONE = "request.event.judgeDone";
		public static final String REQUEST_SENT_FOR_REVIEW = "request.event.RequestSentForReview";
		public static final String REQUEST_SUBMIT = "request.event.requestSubmmited";
		public static final String REQUEST_REGISTERED = "request.event.registered";
		public static final String REQUEST_SENTBACK = "request.event.Sentback";
		public static final String REQUEST_REJECTED_FINANCE = "request.event.RequestRejectedFinance";
		public static final String REQUEST_RESEARCHER_ACK = "request.event.researcherAcknowledged";
		public static final String REQUEST_RESENT_TO_CONTRACTOR = "request.event.ResentToContractor";
		public static final String REQUEST_CUSTOMER_REJECTED = "request.event.customerRejected";
		public static final String REQUEST_SENT_TO_FM = "request.event.sentToFM";
		public static final String REQUEST_SENT_TO_PM = "request.event.sentToPM";
		public static final String REQUEST_DONE_BY_FM = "request.event.doneByFM";
		public static final String REQUEST_DONE_BY_PM = "request.event.doneByPM";
                public static final String REQUEST_SENT_FOR_COMPLETION = "request.event.CompletionRequired";
		
	}

	public class ContractEvents {

		public static final String CONTRACT_CREATED = "contract.event.ContractCreated";
		public static final String CONTRACT_APPROVED = "contract.event.ContractApproved";
		public static final String CONTRACT_SENT_FOR_APPROVAL = "contract.event.ContractSendForApproval";
		public static final String CONTRACT_REJECTED = "contract.event.ContractRejected";
		public static final String CONTRACT_UPDATED = "contract.event.ContractUpdated";
		public static final String CONTRACT_PAYMENT_COLLECTED = "contract.event.ContractPaymentCollected";
		public static final String APROVAL_REQUIRED = "contract.event.ContractApprovalRequired";
		public static final String REVIEW_REQUIRED_LEGAL = "contract.event.ContractReviewRequiredLegal";
		public static final String REVIEWED_LEGAL = "contract.event.ContractReviewedLegal";
		public static final String CONTRACT_ACTIVATED = "contract.event.ContractActivated";
		public static final String CONTRACT_PRINTED = "contract.event.ContractPrinted";
		public static final String DISCLOSURE_PRINTED = "commons.disclosurePrinted";
		public static final String CONNECT_ELECTRICAL_SERVICES_PRINTED = "contract.event.connectElectricServicesPrinted";
		public static final String CONNECT_WATER_SERVICES_PRINTED = "contract.event.connectWaterServicesPrinted";
		public static final String ACTIVITY_PRACTICING_LETTER_PRINTED = "contract.event.activityPracticingLetterPrinted";
		public static final String CONTRACT_CANCELLED = "contract.event.ContractCancelled";
		public static final String CONTRACT_START_DATE_CHANGED = "contract.event.contractStartDateChangedAfterActive";
		public static final String CONTRACT_END_DATE_CHANGED = "contract.event.contractEndDateChangedAfterActive";
	}

	public  class CommonsMessages {
		public static final String MSG_ZERO_PAYMENTS_COLLECTED ="commons.msg.nothingToCollectTaskMoved";
		public static final String MSG_OLD_CONTRACT_PAYMENTS_COLLECTED="commons.msg.oldContractPaymentsCollectedSuccessfully";
		public static final String NO_FILTER_ADDED = "commons.msg.noFilterAdded";
		public static final String REQUEST_CANCEL_SUCCESS = "commons.msg.requestCancelledSuccessfully";
		public static final String REQUEST_COMPLETE_SUCCESS = "commons.msg.requestCompletedSuccessfully";
		public static final String MSG_PAYMENT_RECIVED = "commons.msg.paymentRecivedSuccessfully";
		public static final String MSG_APPLICANT_REQUIRED = "commons.msg.applicantRequired";
		public static final String MSG_PAYMENT_RECIVED_FAILED = "auction.registration.paymentReceivedFailed";
		public static final String MSG_APPLICANT_NOT_SELECTED = "commons.messages.applicantNotSelected";

	}

	public  class ReplaceChequeNew {

		public static final String REQUEST_CREATED = "replace.message.requestCreated";
		public static final String REQUEST_Approved = "replace.message.requestApproved";
		public static final String REQUEST_Reject = "replace.message.requestRejected";
		public static final String REQUEST_Complete = "replace.message.requestCompleted";
		public static final String REQUEST_Cancel = "replace.message.requestCancel";

		public static final String REQUEST_FAILED_CREATED = "replace.message.Fail.requestCreated";
		public static final String REQUEST_FAILED_APPROVED = "replace.message.Fail.requestApproved";
		public static final String REQUEST_FAILED_REJECT = "replace.message.Fail.requestRejected";
		public static final String REQUEST_FAILED_COMPLETE = "replace.message.Fail.requestCompleted";
		public static final String REQUEST_FAILED_CANCEL = "replace.message.Fail.requestCancel";

		public static final String REQUEST_REPLACED = "replace.message.requestReplaced";
		public static final String REQUEST_RETURNED = "replace.message.requestReturned";
		public static final String REQUEST_FAILED_REPLACED = "replace.message.Fail.requestReplaced";
		public static final String REQUEST_FAILED_RETURNED = "replace.message.Fail.requestReturned";
		public static final String REQUEST_REPLACED_NEW = "replace.message.requestReplacedNew";
		public static final String REQUEST_REPLACED_NOT_RETURN = "replace.message.requestNotReturn";

	}

	public class ServiceContract {

		public static final String MSG_REQ_START_DATE = "serviceContract.message.requiredStartDate";
		public static final String MSG_REQ_EXPIRY_DATE = "serviceContract.message.requiredExpiryDate";
		public static final String MSG_INVALID_CONTRACT_VALUE = "serviceContract.message.invalidContractValue";
		public static final String MSG_CONTRACT_CREATED_SUCCESSFULLY = "serviceContract.message.contractCreatedSuccessfully";
		public static final String MSG_CONTRACT_UPDATED_SUCCESSFULLY = "serviceContract.message.contractUpdatedSuccessfully";
		public static final String MSG_CONTRACT_ADD = "serviceContract.message.contractAdded";
		public static final String MSG_CONTRACT_UPDATED = "serviceContract.message.contractUpdated";
		public static final String MSG_CON_START_SMALL_EXP = "serviceContract.message.contractStartDateSmallThanEndDate";
		public static final String MSG_CON_START_SMALL_TODAY = "serviceContract.message.contractStartDateLessThanToday";
		public static final String MSG_CONTRACT_APPROVED_SUCCESSFULLY = "serviceContract.message.contractApprovedSuccessfully";
		public static final String MSG_CONTRACT_REJECED_SUCCESSFULLY = "serviceContract.message.contractRejectedSuccessfully";
		public static final String TITLE_MAINTAINENCE_CONTRACT = "serviceContract.title.maintainenceContract";
		public static final String MSG_REQ_SENT_FOR_APPROVAL = "serviceContract.message.sendForApproval";
		public static final String MSG_REQ_CONTRACTOR = "serviceContract.message.requiredContractor";
		public static final String MSG_REQ_BANK = "serviceContract.message.requiredBank";
		public static final String MSG_REQ_CONTRACT_AMOUNT = "serviceContract.message.requiredContractAmount";
		public static final String MSG_REQ_SCOPE_OF_WORK = "serviceContract.message.requiredScopeOfWork";
		public static final String MSG_REQ_GUARANTEE_PERCENTAGE = "serviceContract.message.requiredBankGuaranteePercentage";
		public static final String MSG_INVALID_GUARANTEE_PERCENTAGE = "serviceContract.message.invalidBankGuaranteePercentage";
		public static final String MSG_REQ_PAY_SCH = "serviceContract.message.requiredPaymentSchedule";
		public static final String MSG_CONTRACT_COMPLETED_SUCCESSFULLY = "serviceContract.message.completed";
		public static final String MSG_CONTRACT_PRINTED_SUCCESSFULLY = "serviceContract.message.printed";
		public static final String MSG_CONTRACT_REVIEWED_SUCCESSFULLY = "serviceContract.message.reviewed";
		public static final String MSG_CONTRACT_SENT_FOR_REVIEW = "serviceContract.message.sentForReview";
		public static final String TITLE_INSURANCE_CONTRACT = "serviceContract.title.insuranceContract";
		public static final String TITLE_SECURITY_CONTRACT = "serviceContract.title.securityContract";
		public static final String MSG_REQ_PROPERTY = "serviceContract.message.requiredPropertyUnit";
		public static final String MSG_REQ_CONTRACT_DATE = "contract.message.requiredContractDate";
		public static final String MSG_CON_START_SMALL_CONTRACT_DATE= "contract.message.contractStartDateLessThanContractDate";
		public static final String MSG_REQ_CONTRACT_PERIOD = "contract.message.requiredContractPeriod";
		public static final String MSG_INVALID_CONTRACT_PERIOD = "contract.message.invalidContractPeriod";
		public static final String MSG_INVALID_GRACE_PERIOD = "contract.message.invalidGracePeriod";
	}

	/**
	 * @author Tanveer ul Islam
	 */
	public class Contractor {
		public static final String MSG_CONTRACTOR_NAME_EN_REQ = "contractor.message.nameEnRequired";
		public static final String MSG_CONTRACTOR_NAME_AR_REQ = "contractor.message.nameArRequired";
		public static final String MSG_COMMERCIAL_NAME_REQ = "contractor.message.commercialNameRequired";
		public static final String MSG_CONTRACTOR_TYPE_REQ = "contractor.message.contractorTypeRequired";
		public static final String MSG_LICENSE_NUMBER_REQ = "contractor.message.licenseNumberRequired";
		public static final String MSG_LICENSE_SOURCE_REQ = "contractor.message.licenseSourceRequired";
		public static final String MSG_ISSUE_DATE_REQ = "contractor.message.issueDateRequired";
		public static final String MSG_EXPIRY_DATE_REQ = "contractor.message.expiryDateRequired";
		public static final String MSG_BUSINESS_ACTIVITY_REQ = "contractor.message.businessActivityRequired";
		public static final String MSG_SERVICE_TYPE_REQ = "contractor.message.serviceTypeRequired";
		public static final String MSG_CONTRACTOR_STATUS_REQ = "contractor.message.contractorStatusRequired";
		public static final String MSG_ADDED_SUCESS = "contractor.message.addedSuccessfully";
		public static final String MSG_CONTRACTOR_ADDRESS_DETAILS_REQ = "contractor.message.addressDetailsRequired";
		public static final String MSG_CONTACT_REFERENCES_REQ = "contractor.message.contactReferencesRequired";
		public static final String MSG_UPDATED_SUCESS="contractor.message.updatedSuccessfully";
	}
	public class ConstructionContract
	{
		public static final String TITLE_CONSTRUCTION_CONTRACT = "constructionContract.title";
		public static final String MSG_REQ_PROJECT = "constructionContract.requiredProject";
	}
	
	public interface ProjectMilestone{
		public static final String MSG_PERCENT_OUT_OF_RANGE = "projectMilestone.messages.completionPercentInvalid";
		public static final String MSG_PERCENT_OUT_OF_LIMIT = "projectMilestone.messages.completionPercentLimitOut";
		public static final String MSG_MILESTONE_DELETE_SUCCESS = "projectMilestone.messages.deleteSuccess";
		public static final String MSG_MILESTONE_DELETE_FAILURE = "projectMilestone.messages.deleteFailure";
		public static final String MSG_MILESTONE_EDIT_SUCCESS = "projectMilestone.messages.editSuccess";
		public static final String MSG_MILESTONE_EDIT_FAILURE = "projectMilestone.messages.editFailure";
		public static final String MSG_MILESTONE_ADD_SUCCESS = "projectMilestone.messages.addSuccess";
		public static final String MSG_MILESTONE_ADD_FAILURE = "projectMilestone.messages.addFailure";
		public static final String MSG_MILESTONE_DATE_PERCENT_INVALID = "projectMilestone.messages.datePercentInvalid";
	}
	
	public interface ProjectFollowUp{
		public static final String MSG_PROJECT_DELETE_SUCCESS = "project.search.messages.deleteSuccess";
		public static final String MSG_PROJECT_DELETE_FAILURE = "project.search.messages.deleteFailure";
		public static final String MSG_PROJECT_CANCEL_SUCCESS = "project.search.messages.cancelSuccess";
		public static final String MSG_PROJECT_CANCEL_FAILURE = "project.search.messages.cancelFailure";
		public static final String MSG_HAND_OVER_FAILURE = "project.details.messages.handOverFailure";
		public static final String MSG_HAND_OVER_SUCCESS = "project.details.messages.handOverSuccess";
		public static final String MSG_PROJECT_CREATE_SUCCESS = "project.details.messages.projectCreateSuccess";
		public static final String MSG_PROJECT_SAVE_SUCCESS = "project.details.messages.projectSaveSuccess";
		public static final String MSG_PROJECT_SAVE_FAILURE = "project.details.messages.projectSaveFailure";
		public static final String MSG_FOLLOW_UP_SUCCESS = "project.details.messages.followUpSuccess";
		public static final String MSG_FOLLOW_UP_FAILURE = "project.details.messages.followUpFailure";
		public static final String MSG_FOLLOW_UP_COMPLETE_SUCCESS = "project.details.messages.followUpCompleteSuccess";
		public static final String MSG_FOLLOW_UP_COMPLETE_FAILURE = "project.details.messages.followUpCompleteFailure";
		public static final String MSG_FINAL_RECEIVING_SUCCESS = "project.details.messages.finalReceivingSuccess";
		public static final String MSG_FINAL_RECEIVING_FAILURE = "project.details.messages.finalReceivingFailure";
		public static final String MSG_INITIAL_RECEIVING_SUCCESS = "project.details.messages.initialReceivingSuccess";
		public static final String MSG_INITIAL_RECEIVING_FAILURE = "project.details.messages.initialReceivingFailure";
		public static final String MSG_ATLEAST_ONE_CONTRACT_ACTIVE = "project.details.messages.atleastOneContractActive";
		public static final String MSG_ALL_CONTRACTS_SHUD_BE_TERMINATED = "project.details.messages.allContractsTerminated";
		public static final String MSG_CONTRACTS_WITH_PENDING_PAYMENTS = "project.details.messages.contractsWithPendingPayments";
		
		public static final String MSG_MILESTONE_REQUIRED = "project.details.messages.projectMilestoneRequired";
		public static final String MSG_MILESTONE_DATE_INVALID = "project.details.messages.projectMilestoneDateInvalid";
		public static final String MSG_MILESTONE_DATE_INVALID_2 = "project.details.messages.projectMilestoneDateInvalid2";
		public static final String MSG_PARAM_DATE_BEFORE_START_DATE = "project.details.messages.dateAfterStartDate";
		
		public interface HistoryComments{

			public static final String PROJECT_CREATED = "project.event.projectCreated";
			public static final String PROJECT_SUBMITTED = "project.event.projectSubmitted";
			public static final String PROJECT_DELETED = "project.event.projectDeleted";
			public static final String PROJECT_CANCELLED = "project.event.projectCancelled";
			public static final String DESIGN_APPROVED = "project.event.designApproved";
			public static final String HANDOVER_DONE = "project.event.handOverDone";
			public static final String FOLLOWUP_PERFORMED = "project.event.followUpPerformed";
			public static final String FOLLOWUP_DONE = "project.event.followUpDone";
			public static final String INITIAL_ACCEPTANCE_DONE = "project.event.initialAcceptanceDone";
			public static final String FINAL_ACCEPTANCE_DONE = "project.event.finalAcceptanceDone";
		}

		public interface ApproveDesign{

			public static final String MSG_DESIGN_SAVE_SUCCESS = "approveProjectDesign.messages.saveSuccess";
			public static final String MSG_DESIGN_SAVE_FAILURE = "approveProjectDesign.messages.saveFailure";
			public static final String MSG_DESIGN_DELETE_SUCCESS = "approveProjectDesign.messages.deleteSuccess";
			public static final String MSG_DESIGN_DELETE_FAILURE = "approveProjectDesign.messages.deleteFailure";
			public static final String MSG_DESIGN_APPROVE_SUCCESS = "approveProjectDesign.messages.designApproveSuccess";
			public static final String MSG_DESIGN_APPROVE_FAILURE = "approveProjectDesign.messages.designApproveFailure";
			public static final String MSG_NO_CONSULTANT_CONTRACT = "approveProjectDesign.messages.noConsultantContract";
			public static final String MSG_NO_APPROVED_DESIGN = "approveProjectDesign.messages.noApprovedDesign";
		}
	}
	
	public class ConsultantContract
	{
		public static final String TITLE_CONSULTANT_CONTRACT = "consultantContract.title";
		
	}

	public class BOTConstructionContract
	{
		public static final String TITLE_CONSTRUCTION_CONTRACT = "constructionContractBOT.title";
		public static final String MSG_REQ_PROJECT = "constructionContract.requiredProject";
		public static final String MSG_REQ_HEJREE_DATE = "botContract.msg.requiredHejreeDate";
		public static final String MSG_REQ_CONTRACT_PERIOD = "botContract.msg.requiredContractPeriod";
		public static final String MSG_REQ_GRACE_PERIOD = "botContract.msg.requiredGracePeriod";
		public static final String MSG_INVALID_CONTRACT_PERIOD = "botContract.msg.invalidContractPeriod";
		public static final String MSG_INVALID_GRACE_PERIOD = "botContract.msg.invalidGracePeriod";
		public static final String MSG_REQ_INVESTOR = "botContract.msg.requiredInvestor";
		
		
		
	}
	
	public interface RegionDetails{
   		public static final String REGION_DELETE_SUCCESS = "regionDetails.messages.deleteSuccess";
   		public static final String REGION_DELETE_FAILURE = "regionDetails.messages.deleteFailure";
   		public static final String REGION_SAVE_SUCCESS = "regionDetails.messages.saveSuccess";
   		public static final String REGION_SAVE_FAILURE = "regionDetails.messages.saveFailure";
   		public static final String ENTER_DETAILS = "regionDetails.messages.enterDetails";
   }
	
	public class PersonList
	{
	public static final String NO_PERSON_SELECTED = "personList.noPersonSelected";
	public static final String NOTIFICATION_SEND = "personList.notificationSend";
	}
	
	public static final String NOT_PROCEED_FURTHER = "Commons.msg.NotProceed";

	public static final String UNITS_GENERATED_SUCCESSFULLY = "generateUnits.messages.UnitsGeneratedSuccess";
	public static final String UNITS_UPDATED_SUCCESSFULLY = "generateUnits.messages.UnitsUpdatedSuccess";
	public static final String UNITS_GENERATED_GENERAL_ERROR = "generateUnits.messages.generalError";
	public static final String UNITS_UPDATE_GENERAL_ERROR = "generateUnits.messages.generalErrorUpdate";
	public static final String UNITS_ACCOUNT_NUM_ALREADY_EXISTS = "generateUnits.messages.unitAccountNumberAlreadyExists";
	
	public interface MaintenanceRequest{
   		public static final String UNIT_OR_PROPERTY_REQ = "maintenanceRequest.messages.UnitOrPropertyInfoRequired";
   		public static final String CONTRACT_OR_CONTACTOR_REQ = "maintenanceRequest.messages.ContractOrContractorInfoRequired";
   		public static final String ENGINEER_NAME_REQ = "maintenanceRequest.messages.engineerRequired";
   		public static final String VISIT_DATE_REQ = "maintenanceRequest.messages.visitDateRequired";
   		public static final String ESTIMATED_COST_REQ = "maintenanceRequest.messages.estimatedCostRequired";
   		public static final String ACTUAL_COST_REQ = "maintenanceRequest.messages.actualCostRequired";
   		public static final String INVALID_ESTIMATED_COST = "maintenanceRequest.messages.invalidEstimatedCost";
   		public static final String INVALID_ACTUAL_COST = "maintenanceRequest.messages.invalidActualCost";
   		public static final String SITE_VISIT_SAVED_SUCCESSFULLY = "maintenanceRequest.messages.SiteVisitSavedSuccessFully";
   		public static final String SITE_VISIT_COMPLETED= "maintenanceRequest.messages.SiteVisitCompleted";
   		public static final String FOLLOWUP_SAVED_SUCCESSFULLY = "maintenanceRequest.messages.followUpSavedSuccessFully";
   		public static final String MAINTENANCE_REQUEST_REJECTED = "maintenanceRequest.messages.maintenanceRequestRejectedSuccessFully";
   		public static final String MAINTENANCE_REQUEST_COMPLETED  = "maintenanceRequest.messages.maintenanceRequestCompletedSuccessFully";
   		public static final String MAINTENANCE_REQUEST_APPROVED = "maintenanceRequest.messages.maintenanceRequestApprovedSuccessFully";
   		public static final String MAINTENANCE_SENT_FOR_SITE_VISIT = "maintenanceRequest.messages.maintenanceRequestSiteVisitRequired";
   		public static final String PAGE_TITLE_ADD_MODE ="maintenanceRequest.pageTitle.newMaintenanceApp";
   		public static final String PAGE_TITLE_EDIT_MODE ="maintenanceRequest.pageTitle.newMaintenanceApp";
   		public static final String PAGE_TITLE_APPROVAL_REQUIRED_MODE ="maintenanceRequest.pageTitle.approveMaintenanceApp";
   		public static final String PAGE_TITLE_SITE_VISIT_MODE ="maintenanceRequest.pageTitle.siteVisitMaintenanceApp";
   		public static final String PAGE_TITLE_FOLLOWUP_MODE ="maintenanceRequest.pageTitle.followUpMaintenanceApp";
   		public static final String PAGE_TITLE_VIEW_MODE ="maintenanceRequest.pageTitle.viewMaintenanceApplication";
   		public static final String MAINTENANCE_REQUEST_ADDED="maintenanceRequest.messages.requestAddedSuccessFully";
   		public static final String MAINTENANCE_REQUEST_UPDATED="maintenanceRequest.messages.requestUpdatedSuccessFully";
   		
   		
	}
	public interface SetUpNumWorkers
	{
		public static final String INVALID_NUM_WORKERS="setUpWorkers.messages.invalidNumWorkers";
		public static final String REQ_NUM_WORKERS="setUpWorkers.messages.numWorkersReq";
		
	}
	
	public class LeaseContract
	{
		public static final String Contract_Number="commons.Contract.Number";
		public static final String Property_Name="contract.property.Name";
		public static final String Contract_issueDate="contract.issueDate";
		public static final String Contract_EndDate="contract.endDate";
		public static final String Occupant_Name="commons.report.occupantName";
		public static final String Tenant_Name="chequeList.tenantName";
		public static final String Status="commons.status";
		public static final String Property_rentUnitNumber="receiveProperty.rentUnitNumber";
		public static final String Unit_Description="unit.unitDescription";
		public static final String Unit_Area="unit.unitArea";
		public static final String Floor_Name="report.floor.Name";
		public static final String Floor_No="floor.floorNo";
		public static final String Rent_Value="cancelContract.tab.unit.rent";
		public static final String Payment_Number="bouncedChequesList.paymentNumberCol";
		public static final String Payment_DueOn="paymentSchedule.paymentDueOn";
		public static final String Payment_Type="cancelContract.payment.paymenttype";
		public static final String Payment_Method="cancelContract.payment.paymentmethod";
		public static final String Cheque_No="chequeList.chequeNo";
		public static final String Bank_Name="chequeList.bankNameCol";
		public static final String Payment_Amount="chequeList.paymentAmountCol";
		public static final String Payment_Status="chequeList.message.paymentStatus";
		public static final String Receipt_No="settlement.payment.receiptno";
		public static final String Report_Header="commons.report.pims.header";
		public static final String Report_Name="commons.report.name.leaseContractsDetail.rpt";
	
	}
	public class IstisnaaContract
	{
		public static final String TITLE_CONSTRUCTION_CONTRACT = "istisnaaContract.title";
		public static final String MSG_REQ_PROJECT_OR_PROPERTY = "istisnaaContract.msg.requiredProjectorProperty";
		public static final String MSG_REQ_OWNER = "istisnaaContract.msg.requiredOwner";
		public static final String MSG_REQ_SECOND_PARTY_SHARE="istisnaaContract.msg.requiredSecPartyShare";
		public static final String MSG_REQ_AMAF_OR_SEC_PARTY_SHARE="istisnaaContract.msg.requiredAMAForSecPartyShare";
	}

	public class PaymentReceiptReportProcedureName
	{
		public static final String NEW_LEASE_CONTRACT    = "paymentReceiptReport.procedureName.newLeaseContract";
		public static final String ISSUE_NOL             = "paymentReceiptReport.procedureName.issueNol";
		public static final String CHANGE_TENANT_NAME    = "paymentReceiptReport.procedureName.changeTenantName";
		public static final String CLEARENCE_LETTER      = "paymentReceiptReport.procedureName.clearenceLetter";
		public static final String BOT_CONTRACT          = "paymentReceiptReport.procedureName.botContract";
		public static final String SETTLEMENT            = "paymentReceiptReport.procedureName.settlement";
		public static final String TENDER_PURCHASE_DOC   = "paymentReceiptReport.procedureName.tenderPurchaseDocument";
		public static final String AUCTION_REQ_REG       = "paymentReceiptReport.procedureName.auctionRequestRegistration";
		public static final String RENEW_CONTRACT        = "paymentReceiptReport.procedureName.renewContract";
		public static final String REPLACE_CHEQUE        = "paymentReceiptReport.procedureName.replaceCheque";
		public static final String TRANSFER_REQUEST      = "paymentReceiptReport.procedureName.transferRequest";
		public static final String AMEND_CONTRACT        = "paymentReceiptReport.procedureName.amendContract";
		public static final String CANCEL_CONTRACT       = "paymentReceiptReport.procedureName.cancelContract";
		
	}

	
	public static final String ACTIVATION_LINK="commons.activation.link";
	
	public class RequestTypes
	{
		public static final String REPLACE_CHEQUE ="chequeList.replaceCheque";
	}

	public class Tenant
	{
		public static final String MSG_FIELD_REQUIRED="tenant.errorMsg";
	}
	
	public interface InheritedAssetShares {					 		
		public static final String MSG_BENF_REQUIRED = "benef.asset.messages.beneficiaryIsMandatory";
		public static final String MSG_ASSET_REQUIRED = "benef.asset.messages.inheritanceAssetIsMandatory";
		public static final String MSG_PCT_REQUIRED = "benef.asset.messages.pctweightIsMandatory";
		public static final String MSG_INVALID_PCT_WT = "benef.asset.messages.invalidPctWeight";
		public static final String MSG_INVALID_PCT_SUM = "benef.asset.messages.invalidPctSum";
		public static final String MSG_BENEF_ALREADY_EXISTS = "benef.asset.messages.alreadyExists";
	}
	public interface DisbursementSource {
		public static final String MSG_SRC_EN_REQUIRED = "disuburse.messages.sourceNameEnIsMandatory";
		public static final String MSG_SRC_AR_REQUIRED = "disuburse.messages.sourceNameArIsMandatory";
		public static final String MSG_SRC_GRP_REQUIRED = "disuburse.messages.grpNumIsMandatory";				
	}
	
	/**
	 * 
	 * @author Farhan Muhammad Amin
	 */
	
	public static class MemsNOL {
		
		private MemsNOL() {
			
		}
		public static final String MSG_NOL_UPDATE_SUCCESS = "mems.nol.message.updateSuccess";
		public static final String MSG_NOL_SAVE_SUCCESS = "mems.nol.message.saveSuccess";
		public static final String MSG_NOL_SUBMIT_QUERY_SUCCESS = "mems.nol.message.submitSuccess";
		
		public static final String MSG_NOL_SENT_FOR_REVIEW_SUCCESS = "mems.nol.message.reviewSuccess";
		public static final String MSG_NOL_APPROVE_SUCCESS = "mems.nol.message.approveSuccess";
		public static final String MSG_NOL_REJECT_SUCCESS = "mems.nol.message.rejectSuccess";
		
		public static final String MSG_NOL_REVIWED_SUCCESS = "mems.nol.message.reviwedSuccess";
		public static final String MSG_NOL_COMPLETE_SUCCESS = "mems.nol.message.completeSuccess";
		
		public static final String MSG_CONFIRM_APPROVE = "mems.nol.message.confirmApprove";
		public static final String MSG_CONFIRM_REJECT = "mems.nol.message.confirmReject";
		public static final String MSG_CONFIRM_TECHINCAL = "mems.nol.message.confirmTechnical";
		
		public static final String MSG_REQ_SENT_FOR_REVIEW="mems.nol.message.activity.reqSentForReview";
		public static final String MSG_REQ_REVIWED="mems.nol.message.activity.reqReviwed";
		
		public static final String ERR_GRP_REQUIRED	= "mems.nol.messages.grpRequired";
		public static final String ERR_NO_GRP_ASSOCIATED = "mems.nol.message.nolGroupAssociate";
	}
	
	public static class MemsPeriodicDisbursements {
		private MemsPeriodicDisbursements() {
			
		}
		public static final String ERR_RENEWAL_MANDATORY="mems.perdc.disb.renewalMandatory";
		public static final String ERR_NO_RECORD_IS_SELECTED="mems.perdc.disb.noSelection";
		public static final String ERR_CURRENT_DATE_GREATER="mems.perdc.disb.currentDateGreater";
		public static final String ERR_NO_ALL_RCRDS="mems.perdc.disb.noAllRecords";
		public static final String SUCCESS_RCRD_UPDATED="mems.perdc.disb.recordUpdated";
	}
	
	public static final String RENT_PAYMENTS = "settlement.lbl.rentPaymentsDetails";
	public static final String OTHER_PAYMENTS = "settlement.lbl.otherPayments";
	public static final String RETURNED_PAYMENTS = "settlement.lbl.returnCheques";
	public static final String WITH_DRAWL_LETTER_PRINTED = "withdrawlLetter.printed";
	public static final String WITH_DRAWL_LETTER_PRINTED_FOR_CHEQUES = "withdrawlLetter.letterPrintedForCheques";
	public static final String UNIT_STATUS_CHANGED_LOGG = "changeUnitStatus.loggMsg.unitStatusChanged";
	public static final String CHEQUE_RECEIVED_FROM_BANK = "systemComments.ChequeReceivedFromBank";
	public static class MemsPaymentDisbursementMsgs {
		private MemsPaymentDisbursementMsgs() {
			
		}
		public static final String ERR_BENF_REQ				=	"mems.payment.err.msgs.benefReq";
		public static final String ERR_AMNT_REQ				=	"mems.payment.err.msgs.amntReq";
		public static final String ERR_DESC_REQ				=	"mems.payment.err.msgs.descReq";
		public static final String ERR_AMNT_NUM				=	"mems.payment.err.msgs.amntMustNumeric";
		public static final String ERR_ONE_BENEF_REQ		=	"mems.payment.err.msg.oneBenefReq";
		public static final String ERR_BENF_ALREADY_EXISTS	=	"mems.payment.err.msg.benefAlreayExists";
		public static final String ERR_BENF_DELETE_SUCC		=	"mems.payment.err.msg.benefDeleteSuc";
		public static final String ERR_SEL_GROU				=	"mems.payment.err.msg.selGroup";
		public static final String ERR_COMMENTS_REQ			=	"mems.payment.err.msg.comments.req";
		public static final String ERR_EXTERNAL_NAME_REQ	=	"mems.payment.err.msg.externalIdReq";		
		public static final String ERR_PAYMENT_DT_REQ		=	"mems.payment.err.msg.approvePaymentDetailsReq";
		public static final String ERR_SEL_BENEFICIARY	 	= 	"mems.payment.err.msg.selectBeneficiaryToEdit";
		public static final String ERR_PAYMENT_SRC_REQ		= 	"mems.payment.err.msg.paymentSrcReq";
		public static final String SUCC_JUDGE_REVIEW 		=   "mems.payment.succ.msg.judgReviewSent";
		public static final String REVIEW_REQUIRED 			=   "mems.payment.succ.msg.reviewRequired";
		public static final String REVIEW_DONE 				=   "mems.payment.succ.msg.reviewDone";
		public static final String SUCC_JUDGE_REVIEW_DONE	=   "mems.payment.succ.msg.judgeReviewDone";
		public static final String ERR_AMNT_GREATER			= 	"mems.payment.err.msg.amntGreater";
		
	}
	
	public static class MemsCommonRequestMsgs {
		private MemsCommonRequestMsgs() {}
		public static final String SUCC_BENF_ADD_SUCC		=	"mems.req.common.suc.msgs.benefSuccess";
		public static final String SUCC_BENF_UPDATE_SUCC	=	"mems.req.common.suc.msgs.benefUpdate";
		public static final String SUCC_REQ_SAVE			=	"mems.req.common.suc.msgs.rqSaved";
		public static final String SUCC_REQ_UPDATE			=	"mems.req.common.suc.msgs.rqUpdated";
		public static final String SUCC_REQ_SUBMIT			=	"mems.req.common.suc.msgs.submitQuery";
		public static final String SUCC_REQ_APPROVE			=   "mems.req.common.suc.msgs.approve";
		public static final String SUCC_REQ_REJECT			=  	"mems.req.common.suc.msgs.reject";
		public static final String SUCC_REQ_REVIEW			= 	"mems.req.common.suc.msgs.review";
		public static final String SUCC_REQ_REVIEW_DONE		= 	"mems.req.common.suc.msgs.reviewDone";
		public static final String SUCC_REQ_DISBURSE		= 	"mems.req.common.suc.msgs.disburse";
		public static final String SUCC_REQ_COMPLETE		=	"mems.req.common.suc.msgs.completed";
		public static final String ERR_COMPLETE				= 	"mems.req.common.err.msgs.complete";
		public static final String SUCC_PAYMENT_UPDATED		=	"mems.req.common.suc.msgs.updated";
		public static final String SUCC_REQ_SENT_FOR_DISB	= 	"mems.req.common.suc.msgs.sentForDisb";
	}
	
	public static class MemsPaymentPopupMsgs {
		private MemsPaymentPopupMsgs() {}
		public static final String ERR_PAYMENT_METHOD_REQ 	= "mems.payment.popup.err.msg.paymentMethodReq";
		public static final String ERR_ACCT_NO_REQ			= "mems.payment.popup.err.msg.accoutNoReq";
		public static final String ERR_REF_DATE_REQ			= "mems.payment.popup.err.msg.refDateReq";
		public static final String ERR_REF_NO_REQ			= "mems.payment.popup.err.msg.referenceNoReq";
		public static final String ERR_BANK_ID_REQ			= "mems.payment.popup.err.msg.bankId";				
		public static final String ERR_NO_BENEFICIARY_NAME	= "mems.payment.popup.err.msg.noBeneficiaryName";
	}
	
	public static class MemsInvestmentRequestMsgs {
		private MemsInvestmentRequestMsgs() {}					
		public static final String ERR_INV_TYPE_REQ 		= "mems.investment.err.msg.investmentTypeReq"; 
		public static final String ERR_AMNT_NUMERIC			= "mems.investment.err.msg.amountNumeric";
		public static final String ERR_INV_DESC_REQ			= "mems.investment.err.msg.investmentDetailsReq";
		public static final String ERR_AMNT_REQ				= "mems.investment.err.msg.amoundReq";
		public static final String ERR_BENF_REQ				= "mems.investment.err.msg.beneficiaryReq";
	}
	
	public static class MemsNormalDisbursementMsgs {
		private MemsNormalDisbursementMsgs() {} 
		public static final String ERR_PAYMENT_TYPE_REQ		= "mems.normaldisb.err.msg.paymentTypeReq";
		public static final String ERR_AMNT_REQ				= "mems.normaldisb.err.msg.amountReq";
		public static final String ERR_AMNT_NUMERIC			= "mems.normaldisb.err.msg.amountNumeric";
		public static final String ERR_DESC_REQ				= "mems.normaldisb.err.msg.descReq";
		public static final String ERR_ITEM_ELEC_REQ		= "mems.normaldisb.err.msg.itemReq";
		public static final String ERR_BENEFF_MAND			= "mems.normaldisb.err.msg.benefMandatory";
		public static final String ERR_BENEF_REQ			= "mems.normaldisb.err.msg.benefReq";
		public static final String ERR_BENEF_ALREADY		= "mems.normaldisb.err.msg.benefAlready";
		public static final String ERR_AMNT_EXCEEDS			= "mems.normaldisb.err.msg.amountExceeds";
		public static final String ERR_AMNT_EQ				= "mems.normaldisb.err.msg.amountNotEqual";
		public static final String ERR_ACCCEPT_ERR			= "mems.normaldisb.err.msg.acceptErr";
		public static final String ERR_APPROVE				= "mems.normaldisb.err.msg.approveErr";
		public static final String ERR_REVIEW_COMMT			= "mems.normaldisb.err.msg.reviewCmtErr";
		public static final String ERR_REVIEW_GRP			= "mems.normaldisb.err.msg.reviewGrp";
		public static final String ERR_SEL_BENEF			= "mems.normaldisb.err.msg.selBeneficiary";
		public static final String ERR_DISB_SRC_REQ			= "mems.normaldisb.err.msg.disbSrcReq";
		public static final String ERR_VENDOR_REQ			= "mems.normaldisb.err.msg.vendorReq";
		public static final String ERR_MARK_DONE			= "mems.normaldisb.err.msg.markDone";
		public static final String ERR_ONE_BENEF_REQ		= "mems.normaldisb.err.msg.benefReq";
		public static final String SUCC_MSG_ACCEPT			= "mems.normaldisb.suc.msg.accept";
		public static final String SUCC_MSG_REJECT			= "mems.normaldisb.suc.msg.reject";
		public static final String ERR_INST_REQ				= "mems.normaldisb.err.msg.instalReq";
		public static final String ERR_INST_FREQ			= "mems.normaldisb.err.msg.disbFreqReq";
		public static final String ERR_FIRST_DATE			= "mems.normaldisb.err.msg.firstDisbDate";
		public static final String ERR_INSTALLMENT			= "mems.normaldisb.err.msg.installments";
		public static final String SUCC_MSG_SCHEDULE		= "mems.normaldisb.suc.msg.scheduleUpdate";
		public static final String SUCC_MSG_SCH_ADD			= "mems.normaldisb.suc.msg.scheduleAdd";
		public static final String SUCC_BENEF_UPDATE		= "mems.normaldisb.succ.msg.benefUpdated";
		public static final String SUCC_BENEF_ADD			= "mems.normaldisb.succ.msg.benefAdd";
		public static final String ERR_ADD_PAYMENT			= "mems.normaldisb.err.msg.addPayment";
		public static final String ERR_INSTALLMENT_NUM		= "mems.normaldisb.err.msg.installmentsNum";
		public static final String ERR_NO_ROW				= "mems.normaldisb.err.msg.noRowSelected";
		public static final String ERR_NO_BENEFICIARY		= "mems.normaldisb.err.msg.noBeneficiary";
				
	}
	
	public static class MemsFinanceMsgs {
		private MemsFinanceMsgs() {}
		public static final String SUCC_DISB 				= "mems.finance.succ.msgs";
		public static final String ERR_SELECT_ONE		 	= "mems.finance.err.msg"; 
	}
	
	public class Report {
		public static final String ERROR_NO_RECORD_FOUND="report.error.noRecordFound";
	}
	public class InheritanceFileMessage {
		public static final String SEND_TO_RESEARCHER="logMsg.assignToResearcher";
		public static final String FOLLOW_UP_RECOMM_SENT_FOR_APPROVAL="logMsg.followRecommSendForApproval";
		public static final String SENT_TO_SOCIAL_RESEARCH="logMsg.sentToSocialRes";
		public static final String FOLLOW_UP_TASK_ACKNOWLEGDE = "logMsg.followupTaskCompleted";
		
	}
	
	public class MemsPayVendorMsgs {
		private MemsPayVendorMsgs() {}
		public static final String ERR_NO_RECORD_SELECTED 	= "mems.payvendor.err.noRecord";
		public static final String ERR_SELECT_VENDOR		= "mems.payvendor.err.selectVendor";
		
	}
	public class ManageAssetEvent {
		public static final String MANAGER_UPDATED= "manageAsset.successMsg.managerSave";
		public static final String UNIT_UPDATED= "thirdPartyUnit.systemNotes.unitUpdated";
		public static final String UNIT_ADDED= "thirdPartyUnit.systemNotes.unitadded";
		public static final String UNIT_DELETED= "thirdPartyUnit.systemNotes.unitdeleted";
		
		}
	public class MasrafEvent {
		public static final String UPDATED= "masraf.label.successMsg.masrafUpdate";
		public static final String ADDED= "masraf.label.successMsg.masrafAdded";
		}
	
	public class FileNetConfiguration {
		public static final String ERR_PROCEDURE_TYPE = "fileNetConfiguration.errMsg.procedureType";
		public static final String ERR_DOC_CLASS = "fileNetConfiguration.errMsg.docClass";
		public static final String ERR_META_COLUMNS = "fileNetConfiguration.errMsg.metaColumns";
		public static final String ERR_META_COLUMNS_VALUES = "fileNetConfiguration.errMsg.metaColumnsValues";
		public static final String ERR_OBJECT_STORENAME = "fileNetConfiguration.errMsg.objectStoreName";
		public static final String ERR_DOC_SEARCH_QUERY = "fileNetConfiguration.errMsg.docSearchQuery";
		public static final String ERR_DOC_SEARCH_QUERY_WHERE = "fileNetConfiguration.errMsg.docSearchQueryWhere";
		public static final String ERR_FOLDER_NAME = "fileNetConfiguration.errMsg.folderName";
		public static final String ERR_META_QUERY = "fileNetConfiguration.errMsg.metaQuery";
		}
 
}