package com.avanza.pims.web.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class AttachmentServlet extends HttpServlet {

	private static final long serialVersionUID = -1014353379849907100L;

	@SuppressWarnings("unchecked")
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processUploadRequest(request, response);
	}

	@SuppressWarnings("unchecked")
	private void processUploadRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String entityId = (String) request.getAttribute("entityId");
		if( entityId==null ) {
			entityId = request.getParameter("entityId");
		}
		String entityOwnerId = (String) request.getAttribute("entityOwnerId");
		if( entityOwnerId==null ) {
			entityOwnerId = request.getParameter("entityOwnerId");
		}
		
		boolean isMultiPart = ServletFileUpload.isMultipartContent(request);
		if(isMultiPart) {
			try {
				FileItemFactory itemFactory = new DiskFileItemFactory(); //new DiskFileItemFactory(0, new File("e:\\tmpUpload"));
				ServletFileUpload servlet = new ServletFileUpload(itemFactory);
				// Problem in below line
				List<FileItem> list = (List<FileItem>) servlet.parseRequest(request);
				for (FileItem fileItem : list) {
					System.out.println(fileItem.getName());
				}
				for (FileItem fileItem : list) {
					fileItem.delete();
				}
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//response.sendRedirect(request.getSession(false).getServletContext().get .getContextPath()+"/Attachment.jsp");
	}
	
}
