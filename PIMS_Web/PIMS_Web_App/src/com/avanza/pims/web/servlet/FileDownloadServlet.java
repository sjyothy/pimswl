package com.avanza.pims.web.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Blob;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.Hibernate;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DocumentView;
import com.emitac.pims.filenet.viewdocproxy.types.beans.ebla.DocumentBean;


public class FileDownloadServlet extends HttpServlet {
    
    private static final int DEFAULT_BUFFER_SIZE = 15360;
    static Logger logger = Logger.getLogger(FileDownloadServlet.class);    
    public FileDownloadServlet() {
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
    {
        
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        byte[] buffer = null;
        byte[] base64EncodedStringByteArray = null;
        byte[] decodedFileByteArray = null;
        Blob file = null;
        String base64EncodedString ="";
        try 
        {           	
        	UtilityService utilityService = new UtilityService();
        	Long documentId  = new Long(48);
        	String docName = "";
        	
        	DocumentView documentView =	 null;
        	String fileNetDocId = null;
        	if(request.getSession().getAttribute("documentId") != null)
        	{
        		documentId = Long.parseLong(request.getSession().getAttribute("documentId").toString());
        		request.getSession().removeAttribute("documentId");
        		documentView =	utilityService.getDocument(documentId);
        	}
        	else if(request.getSession().getAttribute("fileNetDocumentId") != null)
        	{
        		fileNetDocId =  request.getSession().getAttribute("fileNetDocumentId").toString();
        		request.getSession().removeAttribute("fileNetDocumentId");
        		documentView = new DocumentView();
        		documentView.setFileNetDocId(fileNetDocId);
        	}
        	 
        	
        	//if file net id is not present then try to retrieve from pims repository.
        	if(documentView.getFileNetDocId() == null || documentView.getFileNetDocId().trim().length() <= 0 )
        	{
				base64EncodedString =  utilityService.getDocumentContent(documentId);
	            docName = documentView.getDocumentFileName() ;
        	}
        	else
        	{
//        		DocumentBean documentBean =	utilityService.getFileNetDocument( documentView );
        		com.emitac.pims.filenet.viewdocproxy.ViewDocumentMTOMClient myPort = new com.emitac.pims.filenet.viewdocproxy.ViewDocumentMTOMClient();
//        		
        		myPort.setEndpoint("http://bkd-ae-01:9080/AmafCEWS/services/ViewDocumentMTOM");
        		logger.logInfo("FileNetDownload:"+myPort.getEndpoint());
        	    DocumentBean documentBean =  myPort.viewdocumentMTOM(
        	                                                             "p8admin",
        	                                                             "ECM_AD1_Pass",
        	                                                             "AMAF2OS",
        	                                                             documentView.getFileNetDocId()
        	                                                           );
        		base64EncodedString =  new String(
        											Base64.encodeBase64( documentBean.getDocBytesArr() )
        										 );
                docName = documentBean.getRetrievalName() ;
        	
                
        	}
        	
        	base64EncodedStringByteArray =  base64EncodedString.getBytes();
        	decodedFileByteArray = Base64.decodeBase64(base64EncodedStringByteArray);
        	file = Hibernate.createBlob(decodedFileByteArray);
            input = new BufferedInputStream( file.getBinaryStream(), DEFAULT_BUFFER_SIZE );
        	response.setHeader( "Content-disposition", "attachment; filename=\"" + docName+ "\"" );
        	output = new BufferedOutputStream( response.getOutputStream(), DEFAULT_BUFFER_SIZE );            
            buffer = new byte[DEFAULT_BUFFER_SIZE];
            for (int length; (length = input.read(buffer)) != -1;) 
            {
                output.write(buffer, 0, length);
            }
            output.flush();                
        	
        }
        catch (PimsBusinessException e) 
        {
        	logger.LogException( "Error Occured:",e);
        }
        catch (Exception e) 
        {
        	logger.LogException( "Error Occured:",e);
        }
        finally 
        {
            close(output);
            close(input);
            buffer = null;
            base64EncodedStringByteArray =null;
            decodedFileByteArray = null;
            base64EncodedString= null;
            file= null;
        }
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    {
        
        BufferedInputStream input = null;
        String base64EncodedString = null;
        BufferedOutputStream output = null;
        byte[] decodedFileByteArray = null;
        byte[] base64EncodedStringByteArray =  null;
        Blob file =null;
        byte[] buffer =  null;
        try 
        {
        	Long documentId = (Long)request.getAttribute("documentId");
        	
        	UtilityService utilityService = new UtilityService();
        	DocumentView documentView =	utilityService.getDocument(documentId);
			base64EncodedString =  utilityService.getDocumentContent(documentId);
			base64EncodedStringByteArray =  base64EncodedString.getBytes();
			decodedFileByteArray = Base64.decodeBase64(base64EncodedStringByteArray); 	
            file = Hibernate.createBlob(decodedFileByteArray);
            
            
            
            input = new BufferedInputStream( file.getBinaryStream(), DEFAULT_BUFFER_SIZE );
            
            response.reset();
            response.setContentType(documentView.getContentType());
            response.setContentLength( input.available() );
            response.setHeader( "Content-disposition", "attachment; filename=\"" + documentView.getDocumentFileName() + "\"" );
            
            output = new BufferedOutputStream( response.getOutputStream(), DEFAULT_BUFFER_SIZE );            
            
            buffer = new byte[DEFAULT_BUFFER_SIZE];
            for (int length; (length = input.read(buffer)) != -1;) 
            {
                output.write(buffer, 0, length);
            }
            
            output.flush();            
        }
        catch (PimsBusinessException e) 
        {
        	logger.LogException( "Error Occured:",e);   
        }
        catch (Exception e) 
        {
            logger.LogException( "Error Occured:",e);
        }
        finally 
        {
            close(output);
            close(input);
            buffer = null;
            base64EncodedStringByteArray =null;
            decodedFileByteArray = null;
            base64EncodedString= null;
            file= null;
        }
        
    }
    
    private static void close(Closeable resource) {
        if (resource != null) 
        {
            try 
            {
                resource.close();
            } 
            catch (IOException e) 
            {       
            	logger.LogException( "Error Occured:",e);
            }
        }
    }

}
