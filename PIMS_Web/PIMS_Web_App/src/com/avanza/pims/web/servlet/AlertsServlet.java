package com.avanza.pims.web.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.ws.utility.UtilityService;


public class AlertsServlet extends HttpServlet {
    
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static Logger logger = Logger.getLogger(AlertsServlet.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
    {
    	try
    	{
    		UserDbImpl loggedInUser =(UserDbImpl) request.getSession().getAttribute(WebConstants.USER_IN_SESSION) ;
    		response.setContentType("application/json");
    		PrintWriter out = response.getWriter();
    		if(loggedInUser != null && loggedInUser.getLoginId()!=null)
    		{
    			logger.logDebug("Alert Requested For:%s",loggedInUser.getLoginId());
    		}
    		//Get Counts of New Online Requests
    		if(	request.getRequestURI().toLowerCase().contains("getnewonlinerequests"))
    		{
	    		 int count = getNewOnlineRequestCounts(loggedInUser.getLoginId());	
	    		
	    		 
	    		 out.print("{\"count\":\""+count+"\"}");
    		}
    		else
    		{
    			 out.print("{\"key1\":\"Test\",\"loginId\":\""+loggedInUser.getLoginId()+"\"}");
    		}
    		out.flush();
    	}
    	catch(Exception e)
    	{
    		logger.LogException("Error", e);
    	}
    	finally
    	{}
       
    }
    
    public static int getNewOnlineRequestCounts(String loginId) throws Exception
    {
    	int count= 0;
    	try{
    			count= UtilityService.getNewOnlineRequestCount(loginId);
    			 logger.logDebug("getNewOnlineRequestCounts|loggedInUser:%s|Count:%s",loginId,count);
    	}
    	catch(Exception e)
    	{
    		logger.LogException("Error", e);
    		throw e;
    	}
    	return count;
    }
    
    

}
