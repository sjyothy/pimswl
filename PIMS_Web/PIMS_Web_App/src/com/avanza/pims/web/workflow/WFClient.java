package com.avanza.pims.web.workflow;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.CallWebservice;

public class WFClient {

	private Logger logger = Logger.getLogger(this.getClass());

	public String initiateProcess(WorkFlowVO workFlowVO) {

		StringBuffer result = new StringBuffer();
		String json = workFlowVO.toJson();
		try {
			result.append(CallWebservice.POST(SystemParameters.getInstance().getParameter(WebConstants.WorkFlowEndPoints.INITIATEWORKFLOW), "",json));
			logger.logInfo("initiateProcess|result:%s" , result);
		} catch (Exception ex) {
			logger.logError("error", ex);
		}

		return result.toString();
	}

	public String claimTask(String taskId, String userId) {

		StringBuffer result = new StringBuffer();
		try {
			String json = "{\"taskId\":\"" + taskId + "\",\"userId\":\"" + userId + "\"}";
			result.append(CallWebservice.POST(SystemParameters.getInstance().getParameter(WebConstants.WorkFlowEndPoints.CLAIMTASK), "", json));
		} catch (RuntimeException ex) {
			logger.LogException("error", ex);
		} catch (Exception ex) {
			logger.LogException("error", ex);
		}

		return result.toString();
	}

	public String releaseTask(String taskId, String userId)throws Exception {

		StringBuffer result = new StringBuffer();
		try {
			String json = "{\"taskId\":\"" + taskId + "\",\"userId\":\"" + userId + "\"}";
			result.append(CallWebservice.POST(SystemParameters.getInstance().getParameter(WebConstants.WorkFlowEndPoints.RELEASETASK), "", json));
		}
		catch (Exception ex) {
			logger.LogException("error", ex);
			throw ex;
		}

		return result.toString();
	}

	public String completeTask(WorkFlowVO workFlowVO) {
		StringBuffer result = new StringBuffer();
		try {
			String json = workFlowVO.toJson();
			result.append(CallWebservice.POST(SystemParameters.getInstance().getParameter(WebConstants.WorkFlowEndPoints.COMPLETETASK), "",json));
		} catch (RuntimeException ex) {
			logger.logError("error", ex);
		} catch (Exception ex) {
			logger.logError("error", ex);
		}

		return result.toString();

	}

}
