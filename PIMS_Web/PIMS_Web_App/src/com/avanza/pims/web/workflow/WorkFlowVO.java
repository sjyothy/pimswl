package com.avanza.pims.web.workflow;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.web.WebConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WorkFlowVO {
	
	private Long requestId;
	private String userId;
	private Long procedureTypeId;
	private String assignedGroups;
	private String assignedUser;
	private Long inheritanceFileId;
	private Long auctionId;
	private Long contractId;
	private Long propertyId;
	private Long endowmentId;
	private String taskAction;
	private String taskId;
	private String procedureTaskId;
	private String humanTaskDataValue;
	


    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setProcedureTypeId(String procedureTypeId) throws Exception {
    	this.procedureTypeId = UtilityManager.getProcedureTypeId(procedureTypeId);
    }

    public Long getProcedureTypeId() throws PimsBusinessException {
    	
    	return this.procedureTypeId;

    }

    public void setAssignedGroups(String assignedGroups) {
        this.assignedGroups = assignedGroups;
    }

    public String getAssignedGroups() {
        return assignedGroups;
    }

    public void setAssignedUser(String assignedUser) {
        this.assignedUser = assignedUser;
    }

    public String getAssignedUser() {
        return assignedUser;
    }

    public void setInheritanceFileId(Long inheritanceFileId) {
        this.inheritanceFileId = inheritanceFileId;
    }

    public Long getInheritanceFileId() {
        return inheritanceFileId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setEndowmentId(Long endowmentId) {
        this.endowmentId = endowmentId;
    }

    public Long getEndowmentId() {
        return endowmentId;
    }
    
    String toJson(){
    	
    	return new Gson().toJson(this);
    	
    }

    public void setTaskAction(String taskAction) {
        this.taskAction = taskAction;
    }

    public String getTaskAction() {
        return taskAction;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setProcedureTaskId(String procedureTaskId) {
        this.procedureTaskId = procedureTaskId;
    }

    public String getProcedureTaskId() {
        return procedureTaskId;
    }

	public String getHumanTaskDataValue() {
		return humanTaskDataValue;
	}

	public void setHumanTaskDataValue(String humanTaskDataValue) {
		this.humanTaskDataValue = humanTaskDataValue;
	}

	public void setProcedureTypeId(Long procedureTypeId) {
		this.procedureTypeId = procedureTypeId;
	}
}
