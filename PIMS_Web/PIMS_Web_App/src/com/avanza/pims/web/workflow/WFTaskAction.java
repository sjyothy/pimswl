package com.avanza.pims.web.workflow;

public enum WFTaskAction {
	
	REVIEW("REVIEW"),
	POSTBACK("POSTBACK"),
	POSTBACKRESUBMIT("POSTBACK_RESUBMIT"),
	APPROVED("APPROVED"),
	RESUBMIT("RESUBMIT"),
	REJECT("REJECT"),
	OK("OK")
	;

    private final String text;

    /**
     * @param text
     */
    private WFTaskAction(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
	
}
