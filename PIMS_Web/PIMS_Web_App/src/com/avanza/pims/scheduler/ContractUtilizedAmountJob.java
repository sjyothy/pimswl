package com.avanza.pims.scheduler;

import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.ContractDistributionInfo;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.ws.ContractDistributionInfoService;
public class ContractUtilizedAmountJob extends AbstractJob 
{
	private ContractDistributionInfoService service;
	
	
	private static final String userId ;
	private static final String notesOwner ;
	private static final String notesType ;
	List<ContractDistributionInfo> list=null;
	
	static
	{
	  userId     ="pims_admin";
	  notesOwner = WebConstants.ContractDistributionInfo.NOTES_OWNER;
	  notesType  = "revenueFollowup.event.followupCloseTaskCreated";
	}

	public ContractUtilizedAmountJob(){
		
		service = new ContractDistributionInfoService(); 
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		try
		{
			execute();
		}
		catch(Exception e)
		{
			logger.LogException("executeJob|Error occured :", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void execute() throws Exception 
	{
		if( list != null && list.size() > 0 )
		{
			logger.logInfo("execute| List for contract distribution is not null,%s items are present.",list.size());
			return;
		}
		else
		{
			service = new ContractDistributionInfoService();
			list= service.getJobData();
			for ( ContractDistributionInfo cdi: list) 
			{
				processContractDistributionInfo(cdi);
			}
			list = null;
		}
	}

	@SuppressWarnings("unchecked")
	private void processContractDistributionInfo( ContractDistributionInfo cdi )  
	{
		Contract contract  = null;
		try 
		{
			contract  = cdi.getContract()  ;
			ApplicationContext.getContext().getTxnContext().beginTransaction();

			double utilizedAmount = service.calculateUtilizedAmount( cdi.getContract()  );
			cdi.setUtilizedAmount(utilizedAmount);
			cdi.setUpdatedOn( new Date() );
			EntityManager.getBroker().update(cdi);
			
//	    	NotesController.saveSystemNotesForRequest( notesOwner, notesType, contract.getContractId() );						
			
	    	ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception e)
		{
			logger.LogException("processContractDistributionInfo|Error occured for ContractDistributionInfoId:"+ cdi.getContractDistributionInfoId(), e);
			ApplicationContext.getContext().getTxnContext().rollback();
//			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
			contract = null;
		}	
	}

}
