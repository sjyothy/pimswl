package com.avanza.pims.scheduler;

import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;

public abstract class AbstractJob implements Job 
{
	protected Logger logger;
	
	public AbstractJob() 
	{
		logger = Logger.getLogger( this.getClass() );
	}
	
	public final void execute(JobExecutionContext jobExecutionContext)  
	{	
		final String METHOD_NAME = "execute()";
		
		try 
		{				
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			executeJob(jobExecutionContext);
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		} 
		catch (Exception exception) 
		{	
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
	}
	
	public abstract void executeJob(JobExecutionContext jobExecutionContext) throws Exception;
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		final String METHOD_NAME = "generateNotification()";
		boolean success = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
		
		return success;
	}
	
}
