package com.avanza.pims.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;

import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notificationservice.NotificationType;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.ws.vo.FollowUpProgressView;

public class FollowUpReminderNotificationJob extends AbstractJob 
{
	private FollowUpServiceAgent followUpServiceAgent;
	
	public FollowUpReminderNotificationJob() 
	{
		followUpServiceAgent = new FollowUpServiceAgent();
	}

	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{	
		List<FollowUpProgressView> followUpProgressViewList = getJobData();
		
		for ( FollowUpProgressView followUpProgressView : followUpProgressViewList )
		{
			List<ContactInfo> recipientList = getRecipientList( followUpProgressView );
			
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			
			if ( generateNotification( WebConstants.Notification_MetaEvents.Event_FollowUp_Reminder , recipientList, eventAttributesValueMap, NotificationType.Email ) )
			{
				// Update the FollowUpProgressView by setting the reminder sent date
				followUpProgressView.setReminderSent( new Date() );
				
				followUpServiceAgent.updateFollowUpProgress( followUpProgressView );
			}
		}
	}
	
	private List<FollowUpProgressView> getJobData() throws PimsBusinessException
	{
		FollowUpProgressView followUpProgressView = new FollowUpProgressView();
		followUpProgressView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
		followUpProgressView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
		followUpProgressView.setReminderScheduled( new Date() );
		followUpProgressView.setReminderSent( new Date() );
		
		return followUpServiceAgent.getFollowUpProgressList( followUpProgressView );
	}
	
	private List<ContactInfo> getRecipientList(FollowUpProgressView followUpProgressView)
	{
		List<ContactInfo> recipientList = new ArrayList<ContactInfo>(0);
		
		String loginId = followUpProgressView.getCreatedBy();
		User user = SecurityManager.getUser( loginId );
		user.getEmailUrl();
		recipientList.add( new ContactInfo( loginId, null, null, user.getEmailUrl() ) );
		
		return recipientList;
	}

}
