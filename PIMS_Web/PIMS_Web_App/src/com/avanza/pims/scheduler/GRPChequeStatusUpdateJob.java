package com.avanza.pims.scheduler;


import org.quartz.JobExecutionContext;

import com.avanza.pims.ws.GRP.ReadChequeStatusService;

public class GRPChequeStatusUpdateJob extends AbstractJob 
{
	@Override
	@SuppressWarnings("unchecked")
	public synchronized void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
			ReadChequeStatusService service = ReadChequeStatusService.getInstance();
			service.executeReadChequeStatusService();	
	}
	
		
}
