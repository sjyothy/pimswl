package com.avanza.pims.scheduler;


import java.util.List;

import org.quartz.JobExecutionContext;

import com.avanza.pims.dao.RequestManager;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.RequestDetail;
import com.avanza.pims.ws.GRP.GRPCustomersService;
import com.avanza.pims.ws.property.PropertyService;

public class BidderUnitAddToGRPCustomerInterfaceJob extends AbstractJob 
{
	@Override
	@SuppressWarnings("unchecked")
	public synchronized void executeJob(JobExecutionContext jobExecutionContext)
	{
			
			String methodName = "BidderUnitAddToGRPCustomerInterfaceJob|executeJob|";
			logger.logInfo(methodName+"Start");
			populateInterfaceFromRequestdetail();
			populateInterfaceFromRequest();
			
			logger.logInfo(methodName+"Finish");
	
	}
	private void populateInterfaceFromRequestdetail() 
	{
		String methodName= "populateInterfaceFromRequest ";
		RequestManager rm = new RequestManager();
		GRPCustomersService service = new GRPCustomersService();
		try
		{
		List<RequestDetail>  rdlist= rm.getRequestDetailForAuctionRegistrationCollectedPayment();
		
			for (RequestDetail requestDetail : rdlist) 
			{
				logger.logInfo( methodName+"requestDetailId:%s",requestDetail.getRequestDetailId() );
	             Request request  = requestDetail.getRequest();
	             Long personId = request.getBidder().getPersonId();
	             Long unitId = requestDetail.getUnit().getUnitId();
	             service.sendPersonLocationToGRP(null, personId, unitId, requestDetail);
			}
		}
		
		catch (Exception e) 
		{
			logger.LogException(methodName + "crashed... ",e);
		}
		
	}
	public void populateInterfaceFromRequest() 
	{
		String methodName= "getFlagFromRequestDetail ";
		RequestManager rm = new RequestManager();
		GRPCustomersService service = new GRPCustomersService();
		try
		{
		List<Request>  rlist= rm.getRequestForContractCollectedPayment();
		
			for (Request request : rlist) 
			{
				logger.logInfo( methodName+"requestId:%s",request.getRequestId() );
	             Long personId = request.getContract().getTenant().getPersonId();
	             Long unitId = new PropertyService().getUnitIdByRequestId(request.getRequestId());
	             service.sendPersonLocationToGRP(request.getRequestId(), personId, unitId, new RequestDetail());
			}
		}
		
		catch (Exception e) 
		{
			logger.LogException(methodName + "crashed... ",e);
		}
	}
	
		
}
