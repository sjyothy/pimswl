package com.avanza.pims.scheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.quartz.JobExecutionContext;

import com.avanza.core.data.ApplicationContext;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.RequestTasks;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.ws.utility.UtilityService;

public class TaskNotificationJob extends AbstractJob 
{
	private static final String userId ;
    public transient static BPMWorklistClient bpmWorkListClient;
	private static List<RequestTasks> tasksToNotify; 
	private static HashMap<Long,DomainData> mapDomainData;
	static List<Long> statusToAvoid;
	static DateFormat df;
	static Date lowerBoundDate;
//	private static Map<String,String> mapUsersExcludedFromNotification;
	static
	{
	  userId     ="pims_admin";
	  tasksToNotify =null;
	  if(bpmWorkListClient == null)
	  {   
  		try
  		{                        
  			  String soaConfigPath = (String)ApplicationContext.getContext().get("SOA_CONFIG_PATH");
              if(soaConfigPath!=null && !soaConfigPath.equals(""))
              {
                  bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
              }
              mapDomainData  =  new UtilityManager().getDomainDataMap();
              statusToAvoid= new ArrayList<Long>();
              statusToAvoid.add(9003l );
              statusToAvoid.add(9005l );
              df = new SimpleDateFormat("dd/MM/yyyy");
              lowerBoundDate=  df.parse("31/12/2014");
              
  		}
  		catch(Exception exp)
  		{
  			System.out.println("Failed to intialize bpmWorkListClient:"+ exp);
  		}
	  }
	}
	public TaskNotificationJob(){}

	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		try
		{
			execute();
		}
		catch(Exception e)
		{
			logger.LogException("executeJob|Error occured:", e);
		}
		
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void execute() throws Exception 
	{

		if( tasksToNotify != null )
		{
			logger.logInfo("execute|tasksToNotify queue is not null,%s tasks are present in the queue",tasksToNotify.size());
		}
		else
		{
			tasksToNotify = getTasksToNotify();
			for (RequestTasks task : tasksToNotify) 
			{
				processRequestTask(task);
			}
			tasksToNotify = null;
		}

	}

	/**
	 * @param task
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void processRequestTask(RequestTasks task) throws Exception 
	{
		logger.logDebug( "Processing request task id:%s", task.getRequestTasksId() );
		task.setIsNotified( Constant.RequestTasksNotificationStatus.IN_PROCESS );
		updateRequestTasks(task);
		List<ContactInfo>  particpants = null;
		Request request  = null;
		UserTask userTask = null;
		try 
		{
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			task.setIsNotified( null );
			userTask = getUserTask(task);
			
			particpants = getNotificationParticipants(task,userTask );
//			request  = task.getRequest();
//			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
//			setEventAttributes(task, userTask , request, eventAttributesValueMap);
//			
//			boolean isfired = generateNotification("Event.RequestTaskNotifications.General", particpants, eventAttributesValueMap,null);			
//			if(  isfired )
//			{
//				logger.logDebug( "Notification fired for task id:%s", task.getRequestTasksId() );
				task.setIsNotified( Constant.RequestTasksNotificationStatus.NOTIFIED );
//			}
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Throwable e)
		{
			logger.LogException("processRequestTask|Error occured:", e);
			ApplicationContext.getContext().getTxnContext().rollback();

		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
			updateRequestTasks(task);
			request=null;
			particpants  = null;
			userTask = null;
			
		}
		logger.logDebug( "Finish Processing request task id:%s", task.getRequestTasksId() );
	}

	/**
	 * @param task
	 * @param request
	 * @param eventAttributesValueMap
	 */
	@SuppressWarnings("unchecked")
	private void setEventAttributes(RequestTasks task, UserTask userTask , Request request,Map<String, Object> eventAttributesValueMap) throws Exception 
	{

		String relatedRefNumber = getRelatedReferenceNumber(request);
		if(relatedRefNumber != null && relatedRefNumber.length() > 0)
		{
			eventAttributesValueMap.put("REQUEST_NUMBER", request.getRequestNumber() + " / " + relatedRefNumber );
		}
		else
		{
			eventAttributesValueMap.put("REQUEST_NUMBER", request.getRequestNumber() );
		}
		eventAttributesValueMap.put("REQUEST_TYPE",   request.getRequestType().getRequestTypeEn() +" - "+ request.getRequestType().getRequestTypeAr() );
		if( task.getRequestStatusId() != null && mapDomainData.containsKey( task.getRequestStatusId() ) )
		{
			DomainData dd = mapDomainData.get(task.getRequestStatusId()); 
			eventAttributesValueMap.put("REQUEST_STATUS", dd.getDataDescEn() +" - "+ dd.getDataDescAr() );
			dd=null;		
		}
		eventAttributesValueMap.put("TASK_DETAIL",   userTask.getTaskNumber()+"("+ task.getRequestTasksId() +"/"+ task.getTaskId() +")");
	}

	private String getRelatedReferenceNumber(Request request)throws Exception
	{
		if( request.getContract() != null && request.getContract().getContractId() != null )
		{
			if( request.getContract().getContractNumber() == null )
			{
				request.setContract( EntityManager.getBroker().findById(Contract.class, request.getContract().getContractId() ) );
			}
		     return request.getContract().getContractNumber();
			 
		}
		else if ( request.getInheritanceFile() != null && request.getInheritanceFile().getInheritanceFileId() != null )
		{
			if( request.getInheritanceFile().getFileNumber() == null )
			{
				request.setInheritanceFile( EntityManager.getBroker().findById(InheritanceFile.class, request.getInheritanceFile().getInheritanceFileId() ) );
			}
		     return request.getInheritanceFile().getFileNumber();
			 
		}
		else if ( request.getEndowmentFile() != null && request.getEndowmentFile().getFileId() != null )
		{
			if( request.getEndowmentFile().getFileNumber() == null )
			{
				request.setEndowmentFile( EntityManager.getBroker().findById(EndowmentFile.class, request.getEndowmentFile().getFileId() ) );
			}
		     return request.getEndowmentFile().getFileNumber();
			 
		}
		else if ( request.getEndowment() != null && request.getEndowment().getEndowmentId()  != null )
		{
			if( request.getEndowment().getEndowmentNum() == null )
			{
				request.setEndowment( EntityManager.getBroker().findById(Endowment.class, request.getEndowment().getEndowmentId() ) );
			}
		     return request.getEndowment().getEndowmentNum();
			 
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	private List<ContactInfo>  getNotificationParticipants(RequestTasks task,UserTask userTask ) throws Exception 
	{
		List<ContactInfo> listParticipants = null;
		String taskGroups = null;
		
		if(task.getTaskGroups() == null )
		{
			logger.logDebug( "Request Task with id:%s doesnt have group so fetching from bpel task", task.getRequestTasksId() );
			
			taskGroups = userTask.getTaskAssigneeGroups();
			task.setTaskGroups( taskGroups );
			updateRequestTasks(task);
		}
		else
		{
			logger.logDebug( "Request Task with id:%s has group.", task.getRequestTasksId() );
			taskGroups = task.getTaskGroups();
		}
		if( taskGroups == null )
		{
			return null; 
		}
		listParticipants = UtilityService.getContactInfosFromGroupsName( taskGroups );
		return listParticipants ;
	}

	@SuppressWarnings("unchecked")
	private void updateRequestTasks(RequestTasks task) throws Exception 
	{
		task.setUpdatedBy(userId);
		task.setUpdatedOn(new Date() );
		task.setNotifyUpdatedOn(new Date() );
		EntityManager.getBroker().update(task);
	}
	
	/**
	 * @param task
	 * @return
	 * @throws PIMSWorkListException
	 */
	private UserTask getUserTask(RequestTasks task)throws PIMSWorkListException 
	{
		logger.logDebug("getUserTask|Geting the user task for task id:%s ",task.getTaskId() );
		
		return bpmWorkListClient.getTaskForWFAdmin(task.getTaskId() );
	}
	
	@SuppressWarnings("unchecked")
	private List<RequestTasks> getTasksToNotify()throws Exception
	{
		List<RequestTasks> list = null;
		
		Session session = (Session)EntityManager.getBroker().getSession();
		Criteria criteria = session.createCriteria(RequestTasks.class);
		Criteria requestCriteria = criteria.createCriteria("request");
		criteria.add( Restrictions.isNull( "isNotified" ) );
		criteria.add( Restrictions.eq("isDeleted", 0L) );
		criteria.add( Restrictions.eq("hasCompleted", "N") );
		requestCriteria.add(Restrictions.eq("isDeleted", 0L) );
		requestCriteria.add(Restrictions.not( Restrictions.in("statusId", statusToAvoid) ) );
		
		requestCriteria.add(Restrictions.ge("createdOn", lowerBoundDate) );
		requestCriteria.addOrder(org.hibernate.criterion.Order.desc("requestId"));
		
		list =  criteria.list();
		return list;
	}
	
	

}
