package com.avanza.pims.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;

import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;

public class ContractExpiryNotificationJob extends AbstractJob 
{
	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		List<ContractView> contractViewList = getContracts();		 
		
		for ( ContractView contractView : contractViewList )
		{
			List<ContactInfo> contactInfoList = getContactInfoList( contractView.getTenantView() );
			
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			eventAttributesValueMap.put("TENANT_NAME", contractView.getTenantView().getPersonFullName());
			eventAttributesValueMap.put("CONTRACT_NO", contractView.getContractNumber());
			
			generateNotification(WebConstants.Notification_MetaEvents.Event_Contract_Expired, contactInfoList, eventAttributesValueMap,null);
		}		
	}
	
	@SuppressWarnings("unchecked")
	private List<ContractView> getContracts() throws Exception
	{	
		ContractView contractView = new ContractView();
		
		DomainData contractStatus = new UtilityManager().getDomainDataByValue(WebConstants.CONTRACT_STATUS_EXPIRED);
		contractView.setStatus(contractStatus.getDomainDataId());
		
		HashMap contractHashMap = new HashMap();
		contractHashMap.put("contractView", contractView);
		
		PropertyService propertyService = new PropertyService();
		return propertyService.getContractsList(contractHashMap);
	}
	
	@SuppressWarnings("unchecked")
	private List<ContactInfo> getContactInfoList(PersonView personView) 
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(personView.getPersonId().toString(), personView.getCellNumber(), null, ciView.getEmail()));    			
            }
    	}
    	return emailList;
	}
	
}
