package com.avanza.pims.scheduler;

import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndowmentProgram;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.bpel.proxy.MEMSEndowmentProgramCloseBPELPortClient;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.endowment.EndowmentProgramService;
public class EndowmentProgramCloseReminderJob extends AbstractJob 
{
	private EndowmentProgramService service;
	private static String endPoint ="";
	private static SystemParameters parameters;
	private static Long status;
	private static final String userId ;
	private static final String notesOwner ;
	private static final String notesType ;
	static
	{
	  userId     ="pims_admin";
	  parameters = SystemParameters.getInstance();
	  notesOwner =  Constant.EndowmentProgram.NOTES_OWNER;
	  notesType  = "endowmentProgram.event.closeTaskCreated";
	  endPoint   = parameters.getParameter( "MEMSEndowmentProgramsCloseBPEL" );
	  status     = Constant.EndowmentProgramStatus.NOTIFIED_ID;
	}
	public EndowmentProgramCloseReminderJob() 
	{
		service = new EndowmentProgramService(); 
	}

	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		try
		{

				execute();
		}
		catch(Exception e)
		{
			logger.LogException("executeJob|Error occured :", e);
		}
		
	}

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception 
	{
		List<EndowmentProgram> list= service.getJobData();
		MEMSEndowmentProgramCloseBPELPortClient port = new MEMSEndowmentProgramCloseBPELPortClient();
		port.setEndpoint(endPoint);

		for (EndowmentProgram program: list) 
		{
			try 
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				
				program.setStatusId(status);
				program.setUpdatedOn(new Date());
				program.setUpdatedBy( userId );
				program.setHasCloseTaskCreated("1");
				EntityManager.getBroker().update(program);
				
				port.initiate(
								 "pims_admin", 
							     program.getEndowmentProgramId().toString(), 
							     program.getRefNum(), 
							     null, 
							     null
						     );
				
		    	  //NotesController.saveSystemNotesForRequest( notesOwner, notesType, program.getEndowmentProgramId() );
//		    	  NotesController.saveSystemNotesForRequest( notesOwner,
//		    			                                     "Endowment program close task created",
//		    			                                     "Endowment program close task created",
//		    			                                     program.getEndowmentProgramId());
		    	  
				
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch(Exception e)
			{
				logger.LogException("executeJob|Error occured while generating close task for Program id:"+ program.getEndowmentProgramId().toString(), e);
				ApplicationContext.getContext().getTxnContext().rollback();
				throw e;
			}
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
		}
	}

}
