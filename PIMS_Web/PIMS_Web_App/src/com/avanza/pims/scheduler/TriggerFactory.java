package com.avanza.pims.scheduler;

import java.util.Date;

import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.helpers.TriggerUtils;

import com.avanza.pims.entity.JobSchedule;

public class TriggerFactory {
	
	private static Trigger trigger;
	
	private static interface TriggerType 
	{	
		public static final String SECONDLY = "SECONDLY";
		public static final String MINUTELY = "MINUTELY";
		public static final String HOURLY = "HOURLY";
		public static final String DAILY = "DAILY";
		public static final String WEEKLY = "WEEKLY";
		public static final String MONTHLY = "MONTHLY";
	}	
	
	static {
		trigger = TriggerUtils.makeHourlyTrigger();
	}
	
	private TriggerFactory() {
	
	}
	
	public static Trigger getTrigger(JobSchedule jobSchedule)
	{	
		String triggerType = jobSchedule.getMetaTrigger().getMetaTriggerType();
		
		if ( triggerType.equalsIgnoreCase( TriggerType.SECONDLY ) )
			trigger = getSecondlyTrigger( jobSchedule );
		
		else if ( triggerType.equalsIgnoreCase( TriggerType.MINUTELY ) )
			trigger = getMinutelyTrigger( jobSchedule );
		
		else if ( triggerType.equalsIgnoreCase( TriggerType.HOURLY ) )
			trigger = getHourlyTrigger( jobSchedule );
		
		else if ( triggerType.equalsIgnoreCase( TriggerType.DAILY ) )
			trigger = getDailyTrigger( jobSchedule );
		
		else if ( triggerType.equalsIgnoreCase( TriggerType.WEEKLY ) )
			trigger = getWeeklyTrigger( jobSchedule );
		
		else if ( triggerType.equalsIgnoreCase( TriggerType.MONTHLY ) )
			trigger = getMonthlyTrigger( jobSchedule );
		
		trigger.setStartTime( new Date() );
		trigger.setName( String.valueOf( jobSchedule.getJobScheduleId() ) );		
		trigger.setGroup( Scheduler.DEFAULT_GROUP );
		
		return trigger;
	}
	
	private static Trigger getSecondlyTrigger( JobSchedule jobSchedule )
	{
		Integer executionIntervalInSeconds = ( jobSchedule.getExecutionInterval() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionInterval() ) ) : null;
		Integer executionRepeatCount = ( jobSchedule.getExecutionRepeatCount() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionRepeatCount() ) ) : null;		
		
		if ( executionIntervalInSeconds != null && executionRepeatCount != null )
			return TriggerUtils.makeSecondlyTrigger( executionIntervalInSeconds,  executionRepeatCount );
		else if ( executionIntervalInSeconds != null  )
			return TriggerUtils.makeSecondlyTrigger( executionIntervalInSeconds );
		else if ( executionRepeatCount != null )
			return TriggerUtils.makeSecondlyTrigger( 1,  executionRepeatCount );
		else
			return TriggerUtils.makeSecondlyTrigger();
	}
	
	private static Trigger getMinutelyTrigger( JobSchedule jobSchedule )
	{
		Integer executionIntervalInMinutes = ( jobSchedule.getExecutionInterval() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionInterval() ) ) : null;
		Integer executionRepeatCount = ( jobSchedule.getExecutionRepeatCount() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionRepeatCount() ) ) : null;		
		
		if ( executionIntervalInMinutes != null && executionRepeatCount != null )
			return TriggerUtils.makeMinutelyTrigger( executionIntervalInMinutes,  executionRepeatCount );
		else if ( executionIntervalInMinutes != null  )
			return TriggerUtils.makeMinutelyTrigger( executionIntervalInMinutes );
		else if ( executionRepeatCount != null )
			return TriggerUtils.makeMinutelyTrigger( 1,  executionRepeatCount );
		else
			return TriggerUtils.makeMinutelyTrigger();
	}
	
	private static Trigger getHourlyTrigger( JobSchedule jobSchedule )
	{
		Integer executionIntervalInHours = ( jobSchedule.getExecutionInterval() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionInterval() ) ) : null;
		Integer executionRepeatCount = ( jobSchedule.getExecutionRepeatCount() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionRepeatCount() ) ) : null;		
		
		if ( executionIntervalInHours != null && executionRepeatCount != null )
			return TriggerUtils.makeHourlyTrigger( executionIntervalInHours,  executionRepeatCount );
		else if ( executionIntervalInHours != null  )
			return TriggerUtils.makeHourlyTrigger( executionIntervalInHours );
		else if ( executionRepeatCount != null )
			return TriggerUtils.makeHourlyTrigger( 1,  executionRepeatCount );
		else
			return TriggerUtils.makeHourlyTrigger();
	}
	
	private static Trigger getDailyTrigger( JobSchedule jobSchedule )
	{
		Integer executionHour = ( jobSchedule.getExecutionHour() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionHour() ) ) : null;
		Integer executionMinute = ( jobSchedule.getExecutionMinute() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionMinute() ) ) : null;
		
		if ( executionHour != null && executionMinute != null )
			return TriggerUtils.makeDailyTrigger( executionHour, executionMinute );
		else
			return trigger;
	}
	
	private static Trigger getWeeklyTrigger( JobSchedule jobSchedule )
	{
		Integer executionDay = ( jobSchedule.getExecutionDay() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionDay() ) ) : null;
		Integer executionHour = ( jobSchedule.getExecutionHour() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionHour() ) ) : null;
		Integer executionMinute = ( jobSchedule.getExecutionMinute() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionMinute() ) ) : null;
		
		if ( executionDay != null && executionHour != null && executionMinute != null )
			return TriggerUtils.makeWeeklyTrigger( executionDay, executionHour, executionMinute );
		else
			return trigger;
	}
	
	private static Trigger getMonthlyTrigger( JobSchedule jobSchedule )
	{
		Integer executionDay = ( jobSchedule.getExecutionDay() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionDay() ) ) : null;
		Integer executionHour = ( jobSchedule.getExecutionHour() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionHour() ) ) : null;
		Integer executionMinute = ( jobSchedule.getExecutionMinute() != null ) ? Integer.parseInt( String.valueOf( jobSchedule.getExecutionMinute() ) ) : null;
		
		if ( executionDay != null && executionHour != null && executionMinute != null )
			return TriggerUtils.makeMonthlyTrigger( executionDay, executionHour, executionMinute );
		else
			return trigger;
	}

}
