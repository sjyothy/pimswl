package com.avanza.pims.scheduler;

import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.bpel.proxy.MEMSRevenueFollowupPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.NextRevFollowup;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.RevenueFollowupService;
public class MEMSRevenueFollowupReminderJob extends AbstractJob 
{
	private RevenueFollowupService followUpService;
	private static String endPoint ="";
	private static SystemParameters parameters;
	private static DomainData status;
	private static final String userId ;
	private static final String notesOwner ;
	private static final String notesType ;
	static
	{
	  userId     ="pims_admin";
	  parameters = SystemParameters.getInstance();
	  notesOwner = WebConstants.RevenueFollowup.NOTES_OWNER;
	  notesType  = "revenueFollowup.event.followupCloseTaskCreated";
	  endPoint   = parameters.getParameter( "MEMSRevenueFollowupBPEL" );
	  status     = new DomainData();
	  status.setDomainDataId(Constant.RevenueFollowupStatus.Notified_Id);
	}
	public MEMSRevenueFollowupReminderJob() 
	{
		followUpService = new RevenueFollowupService();
	}

	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		try
		{
				execute();
		}
		catch(Exception e)
		{
			logger.LogException("executeJob|Error occured :", e);
		}
		
	}

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception 
	{
		List<NextRevFollowup> followUpList= followUpService.getJobData();
		MEMSRevenueFollowupPortClient port = new MEMSRevenueFollowupPortClient();
		port.setEndpoint(endPoint);

		for (NextRevFollowup followup : followUpList) 
		{
			try 
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				
				followup.setStatus(status);
				followup.setUpdatedOn(new Date());
				followup.setUpdatedBy( userId );
				EntityManager.getBroker().update(followup);
				
				port.initiate(
								 "pims_admin", 
							     Integer.valueOf( followup.getPrimaryId().toString()), 
							     followup.getRefNum(), 
							     null, 
							     null
						     );
				
		    	  NotesController.saveSystemNotesForRequest(  notesOwner, notesType, followup.getPrimaryId() );						
				
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch(Exception e)
			{
				logger.LogException("executeJob|Error occured while generating task for followup id:"+ followup.getPrimaryId(), e);
				ApplicationContext.getContext().getTxnContext().rollback();
				throw e;
			}
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
		}
	}

}
