package com.avanza.pims.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.PersonView;
import com.ibm.icu.text.DateFormat;
import org.quartz.JobExecutionContext;

public class RenewalNotificationJob extends AbstractJob {

	@Override
	public void executeJob(JobExecutionContext jobExecutionContext)	throws Exception 
	{
		List<ContractView> contractList = new ArrayList<ContractView>();
		contractList = getContracts();
		
		for ( ContractView contractView : contractList )
		{
			if(contractView.getTenantView() != null)
			{
				List<ContactInfo> contactInfoList = getContactInfoList( contractView.getTenantView() );
						
				Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
				eventAttributesValueMap.put("TENANT_NAME", contractView.getTenantView().getPersonFullName());
				eventAttributesValueMap.put("CONTRACT_NO", contractView.getContractNumber());
				eventAttributesValueMap.put("EXPIRY_DATE", contractView.getEndDateString());
				
				generateNotification(WebConstants.Notification_MetaEvents.Event_Contract_Renewal, contactInfoList, eventAttributesValueMap,null);
			}
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	private List<ContractView> getContracts()throws Exception
	{
		List<ContractView> contracts = new ArrayList<ContractView>();
		
		ContractView contract = new ContractView();
		
		Date currentDate = new Date();
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(currentDate);
		cal.add(cal.MONTH, 3);
		Date expiryDate = cal.getTime();
		
		contract.setEndDate(expiryDate);
		contract.setStatus( 310010L );
		HashMap<String,ContractView> contractHashMap = new HashMap<String,ContractView>();
		contractHashMap.put("contractView", contract);
		
		PropertyService propertyService = new PropertyService();
		contracts = propertyService.getContractsList(contractHashMap);
		return contracts;
	}
	
	
	private List<ContactInfo> getContactInfoList(PersonView personView) 
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(personView.getPersonId().toString(), personView.getCellNumber(), null, ciView.getEmail()));    			
            }
    	}
    	return emailList;
	}

}
