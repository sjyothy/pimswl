package com.avanza.pims.scheduler;

import org.quartz.JobDetail;
import org.quartz.Scheduler;

import com.avanza.pims.entity.JobSchedule;

public class JobDetailFactory {
	
	private static JobDetail jobDetail;
	
	static {
		jobDetail = new JobDetail();
	}
	
	private JobDetailFactory() {
		
	}
	
	@SuppressWarnings("unchecked")
	public static JobDetail getJobDetail(JobSchedule jobSchedule) throws Exception
	{
		String jobId = String.valueOf( jobSchedule.getJobScheduleId() );
		String jobGroup = Scheduler.DEFAULT_GROUP;
		Class jobClass = Class.forName( jobSchedule.getMetaJob().getMetaJobClass() );		
		jobDetail = new JobDetail( jobId, jobGroup, jobClass );		
		return jobDetail;
	}

}
