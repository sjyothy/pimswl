package com.avanza.pims.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.dao.JobScheduleManager;
import com.avanza.pims.entity.JobSchedule;
import com.avanza.pims.web.WebConstants;

public class SchedulerManager 
{
	private static Logger logger;
	private static Scheduler scheduler;
	private static JobScheduleManager jobScheduleManager;
	private static List<JobSchedule> jobScheduleList;
	
	static 
	{
		try 
		{
			logger = Logger.getLogger( SchedulerManager.class );
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			jobScheduleManager = new JobScheduleManager();
			jobScheduleList = new ArrayList<JobSchedule>(0);
		} 
		catch (SchedulerException e) 
		{	
			logger.LogException(" Exception occurred while loading SchedulerManager ", e);
		}
	}
	
	private SchedulerManager() 
	{
		
	}
	
	public static void startScheduler() throws Exception 
	{
		if ( scheduler != null )
		{
			scheduler.start();
		}
	}
	
	public static void stopScheduler() throws Exception
	{
		if ( scheduler != null )
		{
			scheduler.shutdown();
		}
	}
	
	public static void loadJobSchedule() throws Exception
	{
		if ( jobScheduleManager != null )
		{
			JobSchedule jobSchedule = new JobSchedule();
			jobSchedule.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			jobSchedule.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			jobScheduleList = jobScheduleManager.findEntity( jobSchedule );
		}
	}
	
	public static void scheduleJobs() throws Exception
	{		
		String methodName = "scheduleJobs|";
		logger.logInfo(methodName + "Start");
		for ( JobSchedule jobSchedule : jobScheduleList )
		{
			logger.logInfo(methodName + "Job Id:%s, Meta Job Id:%s, Meta Job Name:%s", jobSchedule.getJobScheduleId(),
					                      jobSchedule.getMetaJob().getMetaJobId(), 
					                      jobSchedule.getMetaJob().getMetaJobName());
			JobDetail jobDetail = JobDetailFactory.getJobDetail( jobSchedule );
			Trigger trigger = TriggerFactory.getTrigger( jobSchedule ) ;
			scheduler.scheduleJob( jobDetail , trigger );			
		}
		logger.logInfo(methodName + "Finish");
	}
}
