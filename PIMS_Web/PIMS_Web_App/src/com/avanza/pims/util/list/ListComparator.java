package com.avanza.pims.util.list;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;

import com.avanza.pims.ws.vo.BeneficiaryDetailsView;
/*
 * Author: Wasif A Kirmani
 * Dated: 06-10-2008
 */
public class ListComparator implements Comparator<Object> {

    // Init --------------------------------------------------------------------------------------

    private List<String> getters;
    private boolean ascending;

    // Constructor -------------------------------------------------------------------------------

    /**
     * @param getter The name of the getter of the field to sort on.
     * @param ascending The sort order: true = ascending, false = descending.
     */
    public ListComparator(String getter, boolean ascending) {
        this.getters = new ArrayList<String>();
        for (String name : getter.split(",")) {
            if (!name.startsWith("get")) {
                name = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
            }
            this.getters.add(name);
        }
        this.ascending = ascending;
    }

    /**
     * @param getter The name of the getter of the field to sort on.
     */
    public ListComparator(String getter) {
        this(getter, true);
    }

    // Actions -----------------------------------------------------------------------------------

    /**
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    public int compare(Object o1, Object o2) {
        try {
            Iterator<String> iter = getters.iterator();
            
            while (o1 != null && o2 != null && iter.hasNext()) {
                String getter = iter.next();
                o1 = o1.getClass().getMethod(getter, new Class[0]).invoke(o1, new Object[0]);
                o2 = o2.getClass().getMethod(getter, new Class[0]).invoke(o2, new Object[0]);
            }
        } catch (Exception e) {

            throw new RuntimeException("Cannot compare " + o1 + " with " + o2 + " on " + getters, e);
        }

        if (ascending) {
            return (o1 == null) ? -1 : ((o2 == null) ? 1 : ((Comparable<Object>) o1).compareTo(o2));
        } else {
            return (o1 == null) ? 1 : ((o2 == null) ? -1 : ((Comparable<Object>) o1).compareTo(o2));
        }
    }
    /*Prepare Sort*/
    public static final Comparator LIST_COMPARE = new Comparator() {
    private String label1;
    private String label2;

    public int compare(Object object1, Object object2) {
    SelectItem file1 = (SelectItem)object1;
    SelectItem file2 = (SelectItem)object2;
    label1 = file1.getLabel();
    label2 = file2.getLabel();

    return label1.compareTo(label2); 
    }
   };
   public static final Comparator PERSON_NAME_COMPARATOR = new Comparator() {

	    public int compare(Object object1, Object object2) {
	    	BeneficiaryDetailsView detail1 = (BeneficiaryDetailsView)object1;
	    	BeneficiaryDetailsView detail2 = (BeneficiaryDetailsView)object2;

	    return detail1.getBeneficiaryName().compareTo(detail2.getBeneficiaryName()); 
	    }
	   };


   
}