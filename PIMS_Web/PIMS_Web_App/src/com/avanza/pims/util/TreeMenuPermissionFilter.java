//package com.avanza.pims.util;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//
//import com.avanza.core.security.PermissionBinding;
//import com.avanza.core.security.SecurityManager;
//import com.avanza.core.security.db.UserDbImpl;
//import com.avanza.core.util.StringHelper;
//import com.avanza.entity.TreeMenuItemComparator;
//import com.avanza.entity.TreeMenuItems;
//import com.avanza.entity.Item;
//
///**
// * 
// * @author Nauman Bashir
// *
// */
//public class TreeMenuPermissionFilter {
//
//	public List<? extends Item> getFilteredListForUser(
//			List<? extends Item> itemList, String userId) throws Exception {
//		boolean result;
//		UserDbImpl user = new UserDbImpl();// (UserDbImpl) SecurityManager.getUser(userId);
//
//		for (Item item : itemList) {
//			TreeMenuItems treeItem = (TreeMenuItems) item;
//			// if the user is a system user. set the rendered property for all
//			// itmes to be true
//			treverseItemsForPermissions(user, treeItem);
//		}
//		return itemList;
//	}
//
//	/**
//	 * @param user
//	 * @param treeItem
//	 */
//	private void treverseItemsForPermissions(UserDbImpl user,
//			TreeMenuItems treeItem) {
//		boolean result;
//		//if (!user.isSystemUser()) {
//
//			// if action is not specified. check permission
//			// if action is specified check the action property
//			/*if (StringHelper.isNotEmpty(treeItem.getPermissionAction())) {
//
//				PermissionBinding permissionBinding = user
//						.getPermission(treeItem.getItemPermission());
//				if (permissionBinding != null) {
//					String type = treeItem.getPermissionAction();
//					if (type.equalsIgnoreCase("create"))
//						result = permissionBinding.getCanCreate();
//					else if (type.equalsIgnoreCase("delete"))
//						result = permissionBinding.getCanDelete();
//					else if (type.equalsIgnoreCase("update"))
//						result = permissionBinding.getCanUpdate();
//					else
//						result = false;
//				} else
//					result = false;
//
//			} else {
//				result = checkPermission(user, treeItem.getItemPermission());
//			}*/
//		/*remove the line */	//result = true;
//	//	} else
//			result = true;
//			
//		Collection<TreeMenuItems> treeChildItems = treeItem.getChildMenus()
//				.values();
//		ArrayList<TreeMenuItems> lst = new ArrayList<TreeMenuItems>(
//				treeChildItems);
//		Collections.sort(lst, new TreeMenuItemComparator());
//
//		for (TreeMenuItems treeMenuItems2 : lst) {
//			treverseItemsForPermissions(user, treeMenuItems2);
//		}
//
//		treeItem.setRendered(result);
//	}
//
//	private boolean checkPermission(UserDbImpl user, String permissionId) {
//
//		if (user.isSystemUser())
//			return true;
//		return user.hasPermission(permissionId);
//
//	}
//}
