package com.avanza.pims.exporter.reportdetail.criteria;

import java.util.Date;


public class TenancyContractTemplateReportDetailCriteria extends AbstractReportDetailCriteria {
	
	//CONTRACT
	private String txtContractValueFrom;
	private String txtContractValueTo;
	
	private String cboContractType;
	private String cboContractStatus;
	
	private Date clndrContractDateFrom;
	private Date clndrContractDateTo;
	
	private Date clndrContractExpiryDateFrom;
	private Date clndrContractExpiryDateTo;
		
	//OWNER
	private String txtOwnerName;
	private String txtOwnerNo;
	
	private String cboOwnerNationality;
	private String txtOwnerNidNo;
	
	private String txtOwnerPassportNo;
	private String txtOwnerVisaNo;
	
	private String txtOwnerTradeLNo;
	private String txtOwnerPhoneNo;
	
	//UNIT
	private String txtUnitNumber;
	private String txtUnitCostCenter;
	
	private String cboUnittype;
	private String cboUnitStatus;
	
	private String unitRentAmountFrom;
	private String unitRentAmountTo;
	
	private String cboUnitSubType;
	private String cboUnitUsage;
	
	private String txtUnitAreaSqFtFrom;
	private String txtUnitAreaSqFtTo;
	
	//PROPERTY
	private String txtPropertyName;
	private String txtPropertyNumber;
	
	private String cboPropertyType;
	private String cboPropertyStatus;
	
	private String cboPropertyOwnershipType;
	private String cboPropertyUsage;
	
	private String txtPropertyDmNo;
	private String txtLandNumber;
	
	private Date clndrPropertyReceivingDateFrom;
	private Date clndrPropertyReceivingDateTo;
	
	private String cboPropertyCommunity;
	private String txtPropertyAddress;
	
	private String txtNoOfUnitsFrom;
	private String txtNoOfUnitsTo;
	
	private String txtNoOfFloorsFrom;
	private String txtNoOfFloorsTo;
	
	private String txtNoOfParkingFrom;
	private String txtNoOfParkingTo;
	
	private String txtPropertyBuiltAreaSqFtFrom;
	private String txtPropertyBuiltAreaSqFtTo;
	
	private String txtCommercialAreaFrom;
	private String txtCommercialAreaTo;
	
	private String txtPropertyDewaAccNo;
	private String txtContractNumber;
	
	// TENANT
	private String txtTenantName;	
	private Date clndrTenantDateOfBirthFrom;	
	private Date clndrTenantDateOfBirthTo;
	
	private String cboTenantNationality;
	private String txtTenantNidNo;
	
	private String txtTenantPassportNo;
	private String txtTenantVisaNo;
		
	private String txtTenantTradeLNo;
	private String txtTenantPhoneNo;
	
	public TenancyContractTemplateReportDetailCriteria() {
	}

	public String getTxtPropertyNumber() {
		return txtPropertyNumber;
	}

	public void setTxtPropertyNumber(String txtPropertyNumber) {
		this.txtPropertyNumber = txtPropertyNumber;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getTxtPropertyDmNo() {
		return txtPropertyDmNo;
	}

	public void setTxtPropertyDmNo(String txtPropertyDmNo) {
		this.txtPropertyDmNo = txtPropertyDmNo;
	}

	public String getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public String getCboPropertyCommunity() {
		return cboPropertyCommunity;
	}

	public void setCboPropertyCommunity(String cboPropertyCommunity) {
		this.cboPropertyCommunity = cboPropertyCommunity;
	}

	public String getTxtPropertyAddress() {
		return txtPropertyAddress;
	}

	public void setTxtPropertyAddress(String txtPropertyAddress) {
		this.txtPropertyAddress = txtPropertyAddress;
	}

	public String getTxtNoOfUnitsFrom() {
		return txtNoOfUnitsFrom;
	}

	public void setTxtNoOfUnitsFrom(String txtNoOfUnitsFrom) {
		this.txtNoOfUnitsFrom = txtNoOfUnitsFrom;
	}

	public String getTxtNoOfUnitsTo() {
		return txtNoOfUnitsTo;
	}

	public void setTxtNoOfUnitsTo(String txtNoOfUnitsTo) {
		this.txtNoOfUnitsTo = txtNoOfUnitsTo;
	}

	public String getTxtNoOfFloorsFrom() {
		return txtNoOfFloorsFrom;
	}

	public void setTxtNoOfFloorsFrom(String txtNoOfFloorsFrom) {
		this.txtNoOfFloorsFrom = txtNoOfFloorsFrom;
	}

	public String getTxtNoOfFloorsTo() {
		return txtNoOfFloorsTo;
	}

	public void setTxtNoOfFloorsTo(String txtNoOfFloorsTo) {
		this.txtNoOfFloorsTo = txtNoOfFloorsTo;
	}

	public String getTxtNoOfParkingFrom() {
		return txtNoOfParkingFrom;
	}

	public void setTxtNoOfParkingFrom(String txtNoOfParkingFrom) {
		this.txtNoOfParkingFrom = txtNoOfParkingFrom;
	}

	public String getTxtNoOfParkingTo() {
		return txtNoOfParkingTo;
	}

	public void setTxtNoOfParkingTo(String txtNoOfParkingTo) {
		this.txtNoOfParkingTo = txtNoOfParkingTo;
	}

	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getCboPropertyOwnershipType() {
		return cboPropertyOwnershipType;
	}

	public void setCboPropertyOwnershipType(String cboPropertyOwnershipType) {
		this.cboPropertyOwnershipType = cboPropertyOwnershipType;
	}

	public String getCboPropertyStatus() {
		return cboPropertyStatus;
	}

	public void setCboPropertyStatus(String cboPropertyStatus) {
		this.cboPropertyStatus = cboPropertyStatus;
	}

	public String getTxtPropertyBuiltAreaSqFtFrom() {
		return txtPropertyBuiltAreaSqFtFrom;
	}

	public void setTxtPropertyBuiltAreaSqFtFrom(String txtPropertyBuiltAreaSqFtFrom) {
		this.txtPropertyBuiltAreaSqFtFrom = txtPropertyBuiltAreaSqFtFrom;
	}

	public String getTxtPropertyBuiltAreaSqFtTo() {
		return txtPropertyBuiltAreaSqFtTo;
	}

	public void setTxtPropertyBuiltAreaSqFtTo(String txtPropertyBuiltAreaSqFtTo) {
		this.txtPropertyBuiltAreaSqFtTo = txtPropertyBuiltAreaSqFtTo;
	}

	public String getTxtCommercialAreaFrom() {
		return txtCommercialAreaFrom;
	}

	public void setTxtCommercialAreaFrom(String txtCommercialAreaFrom) {
		this.txtCommercialAreaFrom = txtCommercialAreaFrom;
	}

	public String getTxtCommercialAreaTo() {
		return txtCommercialAreaTo;
	}

	public void setTxtCommercialAreaTo(String txtCommercialAreaTo) {
		this.txtCommercialAreaTo = txtCommercialAreaTo;
	}

	public String getTxtPropertyDewaAccNo() {
		return txtPropertyDewaAccNo;
	}

	public void setTxtPropertyDewaAccNo(String txtPropertyDewaAccNo) {
		this.txtPropertyDewaAccNo = txtPropertyDewaAccNo;
	}

	public Date getClndrPropertyReceivingDateFrom() {
		return clndrPropertyReceivingDateFrom;
	}

	public void setClndrPropertyReceivingDateFrom(
			Date clndrPropertyReceivingDateFrom) {
		this.clndrPropertyReceivingDateFrom = clndrPropertyReceivingDateFrom;
	}

	public Date getClndrPropertyReceivingDateTo() {
		return clndrPropertyReceivingDateTo;
	}

	public void setClndrPropertyReceivingDateTo(Date clndrPropertyReceivingDateTo) {
		this.clndrPropertyReceivingDateTo = clndrPropertyReceivingDateTo;
	}

	public String getTxtContractValueFrom() {
		return txtContractValueFrom;
	}

	public void setTxtContractValueFrom(String txtContractValueFrom) {
		this.txtContractValueFrom = txtContractValueFrom;
	}

	public String getTxtContractValueTo() {
		return txtContractValueTo;
	}

	public void setTxtContractValueTo(String txtContractValueTo) {
		this.txtContractValueTo = txtContractValueTo;
	}

	public Date getClndrContractDateFrom() {
		return clndrContractDateFrom;
	}

	public void setClndrContractDateFrom(Date clndrContractDateFrom) {
		this.clndrContractDateFrom = clndrContractDateFrom;
	}

	public Date getClndrContractDateTo() {
		return clndrContractDateTo;
	}

	public void setClndrContractDateTo(Date clndrContractDateTo) {
		this.clndrContractDateTo = clndrContractDateTo;
	}

	public Date getClndrContractExpiryDateFrom() {
		return clndrContractExpiryDateFrom;
	}

	public void setClndrContractExpiryDateFrom(Date clndrContractExpiryDateFrom) {
		this.clndrContractExpiryDateFrom = clndrContractExpiryDateFrom;
	}

	public Date getClndrContractExpiryDateTo() {
		return clndrContractExpiryDateTo;
	}

	public void setClndrContractExpiryDateTo(Date clndrContractExpiryDateTo) {
		this.clndrContractExpiryDateTo = clndrContractExpiryDateTo;
	}

	public String getCboContractType() {
		return cboContractType;
	}

	public void setCboContractType(String cboContractType) {
		this.cboContractType = cboContractType;
	}

	public String getTxtOwnerName() {
		return txtOwnerName;
	}

	public void setTxtOwnerName(String txtOwnerName) {
		this.txtOwnerName = txtOwnerName;
	}

	public String getTxtOwnerNo() {
		return txtOwnerNo;
	}

	public void setTxtOwnerNo(String txtOwnerNo) {
		this.txtOwnerNo = txtOwnerNo;
	}

	public String getCboOwnerNationality() {
		return cboOwnerNationality;
	}

	public void setCboOwnerNationality(String cboOwnerNationality) {
		this.cboOwnerNationality = cboOwnerNationality;
	}

	public String getTxtOwnerNidNo() {
		return txtOwnerNidNo;
	}

	public void setTxtOwnerNidNo(String txtOwnerNidNo) {
		this.txtOwnerNidNo = txtOwnerNidNo;
	}

	public String getTxtOwnerPassportNo() {
		return txtOwnerPassportNo;
	}

	public void setTxtOwnerPassportNo(String txtOwnerPassportNo) {
		this.txtOwnerPassportNo = txtOwnerPassportNo;
	}

	public String getTxtOwnerVisaNo() {
		return txtOwnerVisaNo;
	}

	public void setTxtOwnerVisaNo(String txtOwnerVisaNo) {
		this.txtOwnerVisaNo = txtOwnerVisaNo;
	}

	public String getTxtOwnerTradeLNo() {
		return txtOwnerTradeLNo;
	}

	public void setTxtOwnerTradeLNo(String txtOwnerTradeLNo) {
		this.txtOwnerTradeLNo = txtOwnerTradeLNo;
	}

	public String getTxtOwnerPhoneNo() {
		return txtOwnerPhoneNo;
	}

	public void setTxtOwnerPhoneNo(String txtOwnerPhoneNo) {
		this.txtOwnerPhoneNo = txtOwnerPhoneNo;
	}

	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public String getTxtUnitCostCenter() {
		return txtUnitCostCenter;
	}

	public void setTxtUnitCostCenter(String txtUnitCostCenter) {
		this.txtUnitCostCenter = txtUnitCostCenter;
	}

	public String getCboUnittype() {
		return cboUnittype;
	}

	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}

	public String getCboUnitStatus() {
		return cboUnitStatus;
	}

	public void setCboUnitStatus(String cboUnitStatus) {
		this.cboUnitStatus = cboUnitStatus;
	}

	public String getUnitRentAmountFrom() {
		return unitRentAmountFrom;
	}

	public void setUnitRentAmountFrom(String unitRentAmountFrom) {
		this.unitRentAmountFrom = unitRentAmountFrom;
	}

	public String getUnitRentAmountTo() {
		return unitRentAmountTo;
	}

	public void setUnitRentAmountTo(String unitRentAmountTo) {
		this.unitRentAmountTo = unitRentAmountTo;
	}

	public String getCboUnitSubType() {
		return cboUnitSubType;
	}

	public void setCboUnitSubType(String cboUnitSubType) {
		this.cboUnitSubType = cboUnitSubType;
	}

	public String getCboUnitUsage() {
		return cboUnitUsage;
	}

	public void setCboUnitUsage(String cboUnitUsage) {
		this.cboUnitUsage = cboUnitUsage;
	}

	public String getTxtUnitAreaSqFtFrom() {
		return txtUnitAreaSqFtFrom;
	}

	public void setTxtUnitAreaSqFtFrom(String txtUnitAreaSqFtFrom) {
		this.txtUnitAreaSqFtFrom = txtUnitAreaSqFtFrom;
	}

	public String getTxtUnitAreaSqFtTo() {
		return txtUnitAreaSqFtTo;
	}

	public void setTxtUnitAreaSqFtTo(String txtUnitAreaSqFtTo) {
		this.txtUnitAreaSqFtTo = txtUnitAreaSqFtTo;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}

	public String getCboContractStatus() {
		return cboContractStatus;
	}

	public void setCboContractStatus(String cboContractStatus) {
		this.cboContractStatus = cboContractStatus;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public Date getClndrTenantDateOfBirthFrom() {
		return clndrTenantDateOfBirthFrom;
	}

	public void setClndrTenantDateOfBirthFrom(Date clndrTenantDateOfBirthFrom) {
		this.clndrTenantDateOfBirthFrom = clndrTenantDateOfBirthFrom;
	}

	public Date getClndrTenantDateOfBirthTo() {
		return clndrTenantDateOfBirthTo;
	}

	public void setClndrTenantDateOfBirthTo(Date clndrTenantDateOfBirthTo) {
		this.clndrTenantDateOfBirthTo = clndrTenantDateOfBirthTo;
	}

	public String getCboTenantNationality() {
		return cboTenantNationality;
	}

	public void setCboTenantNationality(String cboTenantNationality) {
		this.cboTenantNationality = cboTenantNationality;
	}

	public String getTxtTenantNidNo() {
		return txtTenantNidNo;
	}

	public void setTxtTenantNidNo(String txtTenantNidNo) {
		this.txtTenantNidNo = txtTenantNidNo;
	}

	public String getTxtTenantPassportNo() {
		return txtTenantPassportNo;
	}

	public void setTxtTenantPassportNo(String txtTenantPassportNo) {
		this.txtTenantPassportNo = txtTenantPassportNo;
	}

	public String getTxtTenantVisaNo() {
		return txtTenantVisaNo;
	}

	public void setTxtTenantVisaNo(String txtTenantVisaNo) {
		this.txtTenantVisaNo = txtTenantVisaNo;
	}

	public String getTxtTenantTradeLNo() {
		return txtTenantTradeLNo;
	}

	public void setTxtTenantTradeLNo(String txtTenantTradeLNo) {
		this.txtTenantTradeLNo = txtTenantTradeLNo;
	}

	public String getTxtTenantPhoneNo() {
		return txtTenantPhoneNo;
	}

	public void setTxtTenantPhoneNo(String txtTenantPhoneNo) {
		this.txtTenantPhoneNo = txtTenantPhoneNo;
	}
	
	
}
