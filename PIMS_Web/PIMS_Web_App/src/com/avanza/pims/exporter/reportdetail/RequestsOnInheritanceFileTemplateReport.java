package com.avanza.pims.exporter.reportdetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.pims.entity.Request;
import com.avanza.pims.exporter.reportdetail.criteria.ReportDetailCriteria;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.InheritanceFileView;

/**
 * 
 * @author Majid Hameed
 *
 */
public class RequestsOnInheritanceFileTemplateReport implements ReportDetail {

	private InheritanceFileView inheritanceFileView;
	public RequestsOnInheritanceFileTemplateReport() {
	

	}
	

	public RequestsOnInheritanceFileTemplateReport( InheritanceFileView inheritanceFileView ) 
	{
	 this.inheritanceFileView = inheritanceFileView;
	 

	}



	private List<String> getColumnHeaders() {

		List<String> columnHeaders = new ArrayList<String>(0);

		columnHeaders.add(CommonUtil.getBundleMessage("commons.requestNumber"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.requestStatus"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.typeCol"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.createdOn"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.createdBy"));
		
		return columnHeaders;
	}


	@SuppressWarnings("unchecked")
	private List<Object[]>  getDataList ()
	{
		List<Object[]> newList = null;
		HashMap searchCriteriaMap = new HashMap();
		try
		{
			Request request = new Request();
			searchCriteriaMap.put( "request", request );
			searchCriteriaMap.put("inheritanceFileNumber",inheritanceFileView.getFileNumber() );
			newList = new InheritanceFileService().getRequestsOnInheritanceFile(searchCriteriaMap);
			
		}
		catch( Exception e )
		{
			
		}
		return  newList;
	}

	public Map<?, ?> getReportDetailMap(ReportDetailCriteria criteria) {
		Map<String, Object> reportDetailMap = new HashMap<String, Object>(0);

		reportDetailMap.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getColumnHeaders());
		reportDetailMap.put(ReportConstant.Keys.REPORT_DATA_LIST, getDataList() );

		return reportDetailMap;
	}




}
