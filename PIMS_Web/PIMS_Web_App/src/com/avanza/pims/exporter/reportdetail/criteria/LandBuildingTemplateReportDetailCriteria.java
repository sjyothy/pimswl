package com.avanza.pims.exporter.reportdetail.criteria;

import java.util.Date;

/**
 * 
 * @author Majid Hameed
 *
 */
public class LandBuildingTemplateReportDetailCriteria extends AbstractReportDetailCriteria {
	
	
	//OWNER INFO
	private String txtOwnerNo;
	private String txtOwnerName; 
	private String cboOwnerNationality;
	private String txtOwnerPassportNo;
	private String txtOwnerVisaNo;
	private String txtOwnerNidNo;
	private String txtOwnerTradeLNo;
	private String txtOwnerPhoneNo;
	
	//PROPERTY INFO
	private String txtPropertyNumber;
	private String txtPropertyName;
	private String txtPropertyDmNo;
	private String txtLandNumber;
	private String cboPropertyCommunity;
	private String txtNoOfUnitsFrom;
	private String txtNoOfUnitsTo;
	private String txtNoOfFloorsFrom;
	private String txtNoOfFloorsTo;
	private String txtNoOfParkingFrom;
	private String txtNoOfParkingTo;
	private String cboPropertyType;
	private String cboPropertyUsage;
	private String cboPropertyStatus;
	private String txtPropertyBuiltAreaSqFtFrom;
	private String txtPropertyBuiltAreaSqFtTo;
	private String txtPropertyDewaAccNo;
	private String txtCommercialAreaFrom;
	private String txtCommercialAreaTo;
	private String txtPropertyAddress;
	
	private String cboPropertyOwnershipType;
	private Date clndrFromDate;
	private Date cldrToDate;
	
	//CONTRACT INFO
	private String contractRentAmountFrom;
	private String contractRentAmountTo;
	private String txtContractNumber;

	
	
	public LandBuildingTemplateReportDetailCriteria() {
	}


	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public Date getClndrFromDate() {
		return clndrFromDate;
	}

	public void setClndrFromDate(Date clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}

	public Date getCldrToDate() {
		return cldrToDate;
	}

	public void setCldrToDate(Date cldrToDate) {
		this.cldrToDate = cldrToDate;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}

	public String getContractRentAmountFrom() {
		return contractRentAmountFrom;
	}

	public void setContractRentAmountFrom(String contractRentAmountFrom) {
		this.contractRentAmountFrom = contractRentAmountFrom;
	}

	public String getContractRentAmountTo() {
		return contractRentAmountTo;
	}

	public void setContractRentAmountTo(String contractRentAmountTo) {
		this.contractRentAmountTo = contractRentAmountTo;
	}

	public String getCboPropertyCommunity() {
		return cboPropertyCommunity;
	}

	public void setCboPropertyCommunity(String cboPropertyCommunity) {
		this.cboPropertyCommunity = cboPropertyCommunity;
	}

	public String getCboOwnerNationality() {
		return cboOwnerNationality;
	}

	public void setCboOwnerNationality(String cboOwnerNationality) {
		this.cboOwnerNationality = cboOwnerNationality;
	}

	public String getTxtOwnerName() {
		return txtOwnerName;
	}

	public void setTxtOwnerName(String txtOwnerName) {
		this.txtOwnerName = txtOwnerName;
	}

	public String getTxtOwnerVisaNo() {
		return txtOwnerVisaNo;
	}

	public void setTxtOwnerVisaNo(String txtOwnerVisaNo) {
		this.txtOwnerVisaNo = txtOwnerVisaNo;
	}

	public String getTxtOwnerTradeLNo() {
		return txtOwnerTradeLNo;
	}

	public void setTxtOwnerTradeLNo(String txtOwnerTradeLNo) {
		this.txtOwnerTradeLNo = txtOwnerTradeLNo;
	}

	public String getTxtOwnerPhoneNo() {
		return txtOwnerPhoneNo;
	}

	public void setTxtOwnerPhoneNo(String txtOwnerPhoneNo) {
		this.txtOwnerPhoneNo = txtOwnerPhoneNo;
	}

	public String getTxtOwnerNo() {
		return txtOwnerNo;
	}

	public void setTxtOwnerNo(String txtOwnerNo) {
		this.txtOwnerNo = txtOwnerNo;
	}

	public String getTxtOwnerNidNo() {
		return txtOwnerNidNo;
	}

	public void setTxtOwnerNidNo(String txtOwnerNidNo) {
		this.txtOwnerNidNo = txtOwnerNidNo;
	}

	public String getTxtPropertyDmNo() {
		return txtPropertyDmNo;
	}

	public void setTxtPropertyDmNo(String txtPropertyDmNo) {
		this.txtPropertyDmNo = txtPropertyDmNo;
	}

	public String getTxtPropertyDewaAccNo() {
		return txtPropertyDewaAccNo;
	}

	public void setTxtPropertyDewaAccNo(String txtPropertyDewaAccNo) {
		this.txtPropertyDewaAccNo = txtPropertyDewaAccNo;
	}

	public String getCboPropertyOwnershipType() {
		return cboPropertyOwnershipType;
	}

	public void setCboPropertyOwnershipType(String cboPropertyOwnershipType) {
		this.cboPropertyOwnershipType = cboPropertyOwnershipType;
	}


	public String getTxtOwnerPassportNo() {
		return txtOwnerPassportNo;
	}


	public void setTxtOwnerPassportNo(String txtOwnerPassportNo) {
		this.txtOwnerPassportNo = txtOwnerPassportNo;
	}


	public String getTxtPropertyNumber() {
		return txtPropertyNumber;
	}


	public void setTxtPropertyNumber(String txtPropertyNumber) {
		this.txtPropertyNumber = txtPropertyNumber;
	}


	public String getTxtNoOfFloorsFrom() {
		return txtNoOfFloorsFrom;
	}


	public void setTxtNoOfFloorsFrom(String txtNoOfFloorsFrom) {
		this.txtNoOfFloorsFrom = txtNoOfFloorsFrom;
	}


	public String getTxtNoOfFloorsTo() {
		return txtNoOfFloorsTo;
	}

	public void setTxtNoOfFloorsTo(String txtNoOfFloorsTo) {
		this.txtNoOfFloorsTo = txtNoOfFloorsTo;
	}

	public String getTxtNoOfParkingFrom() {
		return txtNoOfParkingFrom;
	}

	public void setTxtNoOfParkingFrom(String txtNoOfParkingFrom) {
		this.txtNoOfParkingFrom = txtNoOfParkingFrom;
	}

	public String getTxtNoOfParkingTo() {
		return txtNoOfParkingTo;
	}

	public void setTxtNoOfParkingTo(String txtNoOfParkingTo) {
		this.txtNoOfParkingTo = txtNoOfParkingTo;
	}

	public String getTxtPropertyBuiltAreaSqFtFrom() {
		return txtPropertyBuiltAreaSqFtFrom;
	}

	public void setTxtPropertyBuiltAreaSqFtFrom(String txtPropertyBuiltAreaSqFtFrom) {
		this.txtPropertyBuiltAreaSqFtFrom = txtPropertyBuiltAreaSqFtFrom;
	}

	public String getTxtPropertyBuiltAreaSqFtTo() {
		return txtPropertyBuiltAreaSqFtTo;
	}

	public void setTxtPropertyBuiltAreaSqFtTo(String txtPropertyBuiltAreaSqFtTo) {
		this.txtPropertyBuiltAreaSqFtTo = txtPropertyBuiltAreaSqFtTo;
	}


	public String getTxtPropertyAddress() {
		return txtPropertyAddress;
	}


	public void setTxtPropertyAddress(String txtPropertyAddress) {
		this.txtPropertyAddress = txtPropertyAddress;
	}


	public String getCboPropertyStatus() {
		return cboPropertyStatus;
	}


	public void setCboPropertyStatus(String cboPropertyStatus) {
		this.cboPropertyStatus = cboPropertyStatus;
	}


	public String getTxtNoOfUnitsFrom() {
		return txtNoOfUnitsFrom;
	}


	public void setTxtNoOfUnitsFrom(String txtNoOfUnitsFrom) {
		this.txtNoOfUnitsFrom = txtNoOfUnitsFrom;
	}


	public String getTxtNoOfUnitsTo() {
		return txtNoOfUnitsTo;
	}

	public void setTxtNoOfUnitsTo(String txtNoOfUnitsTo) {
		this.txtNoOfUnitsTo = txtNoOfUnitsTo;
	}

	public String getTxtCommercialAreaFrom() {
		return txtCommercialAreaFrom;
	}


	public void setTxtCommercialAreaFrom(String txtCommercialAreaFrom) {
		this.txtCommercialAreaFrom = txtCommercialAreaFrom;
	}


	public String getTxtCommercialAreaTo() {
		return txtCommercialAreaTo;
	}


	public void setTxtCommercialAreaTo(String txtCommercialAreaTo) {
		this.txtCommercialAreaTo = txtCommercialAreaTo;
	}
	
	
}
