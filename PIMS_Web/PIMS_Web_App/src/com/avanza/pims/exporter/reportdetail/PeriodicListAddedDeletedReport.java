package com.avanza.pims.exporter.reportdetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.pims.exporter.reportdetail.criteria.ReportDetailCriteria;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.RequestView;

/**
 * 
 * @author Majid Hameed
 *
 */
public class PeriodicListAddedDeletedReport implements ReportDetail {

	private RequestView requestView;
	private String listTypeId;
	public PeriodicListAddedDeletedReport() {}
	

	public PeriodicListAddedDeletedReport( RequestView requestView,String listTypeId ) 
	{
	 this.requestView  = requestView ;
	 this.listTypeId   = listTypeId;
	}



	private List<String> getColumnHeaders() {

		List<String> columnHeaders = new ArrayList<String>(0);

		columnHeaders.add(CommonUtil.getBundleMessage("disList.disbursementListType"));
		columnHeaders.add(CommonUtil.getBundleMessage("receiptDetail.PaidBy"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.status"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.amount"));
		columnHeaders.add(CommonUtil.getBundleMessage("paymentDetailsPopUp.paidTo"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.startDate"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.endDate"));
		
		columnHeaders.add(CommonUtil.getBundleMessage("financialAccConfiguration.bankRemitanceAccountName"));
		columnHeaders.add(CommonUtil.getBundleMessage("mems.inheritanceFile.fieldLabel.bank"));
		
		return columnHeaders;
	}
	@SuppressWarnings("unchecked")
	private String getTotalBeneficiaryListForListTypeIdQuery()
	{
		String sqlQuery = "";
		try
		{
			
			sqlQuery =" SELECT " +
			" \"List Name\",\"Paid By\",\"Status\",\"Amount\",\"Paid to\",\"Start date\",\"End date\",\"Bank Account #\",\"Bank\" " +			
			" FROM ALL_PERIODIC_DISBURSEMENTS WhERE 1=1 AND type_id = " +this.listTypeId;
			
		}
		catch( Exception e )
		{
			
		}
		return  sqlQuery;
	}

	@SuppressWarnings("unchecked")
	private String getBeneficiaryInListButNotInRequest()
	{
		String sqlQuery = "";
		try
		{
			
			sqlQuery =" SELECT " +
			" \"List Name\",\"Paid By\",\"Status\",\"Amount\",\"Paid to\",\"Start date\",\"End date\",\"Bank Account #\",\"Bank\" " +			
			" FROM ALL_PERIODIC_DISBURSEMENTS WhERE 1=1 AND type_id = " +this.listTypeId +
			" AND PRDC_DIS_LIST_ID NOT IN " + 
			" ( " +
			"   select  DD.PRDC_DIS_LIST_ID from DISBURSEMENT_DETAILS dd inner join request r on dd.request_id=r.request_id where r.request_id in ( "+requestView.getRequestId()+ ") " +
			"			and dd.is_deleted=0 and dd.status not in (165003)"+
			" )";
			
		}
		catch( Exception e )
		{
			
		}
		return  sqlQuery;
	}



	private List<String> getAddedBenColumnHeaders() {

		List<String> columnHeaders = new ArrayList<String>(0);

		
		columnHeaders.add(CommonUtil.getBundleMessage("receiptDetail.PaidBy"));
		columnHeaders.add(CommonUtil.getBundleMessage("commons.amount"));
		columnHeaders.add(CommonUtil.getBundleMessage("paymentDetailsPopUp.paidTo"));
		columnHeaders.add(CommonUtil.getBundleMessage("financialAccConfiguration.bankRemitanceAccountName"));
		columnHeaders.add(CommonUtil.getBundleMessage("mems.inheritanceFile.fieldLabel.bank"));
		
		return columnHeaders;
	}
	@SuppressWarnings("unchecked")
	private String getReqBenInListQuery()
	{
		StringBuilder sqlQuery = new StringBuilder(" ");
		try
		{

			sqlQuery.append( " select passport_name, amount, paid_to_name, account_number, paid_to_Bank from  " ) 
				    .append( "( select pdl.prdc_dis_list_id, p.person_id,dd.amount,p.passport_name,pdl.account_number,pdl.paid_to_Bank,pdl.paid_to_name from request r ")
			        .append( " inner join  disbursement_details dd on r.request_id = dd.request_id ")
			        .append( " inner join  periodic_disbursement_list pdl on dd.prdc_dis_list_id = pdl.prdc_dis_list_id ")
			        .append( " inner join  dis_req_beneficiary drb on dd.dis_det_id = drb.dis_det_id" )
			        .append( " inner join  asso_dis_ben_inf_ben adb on adb.dis_req_beneficiary = drb.dis_req_beneficiary") 
			        .append( " inner join  person p on  drb.person_id = p.person_id ")           
			        .append( " where  r.request_id in ( "+requestView.getRequestId()+ ") and    dd.is_deleted=0  and dd.status not in (165003) )");
		}
		catch( Exception e )
		{
			
		}
		return  sqlQuery.toString();
	}

	
	@SuppressWarnings("unchecked")
	private String getAddedBenInListQuery()
	{
		StringBuilder sqlQuery = new StringBuilder(" ");
		try
		{
			sqlQuery.append( " select passport_name, amount, paid_to_name, account_number, paid_to_Bank from  " ) 
		   .append( "( select pdl.prdc_dis_list_id, p.person_id,dd.amount,p.passport_name,pdl.account_number,pdl.paid_to_Bank,pdl.paid_to_name from request r ")
           .append( " inner join  disbursement_details dd on r.request_id = dd.request_id ")
           .append( " inner join  periodic_disbursement_list pdl on dd.prdc_dis_list_id = pdl.prdc_dis_list_id ")
           .append( " inner join  dis_req_beneficiary drb on dd.dis_det_id = drb.dis_det_id" )
           .append( " inner join  asso_dis_ben_inf_ben adb on adb.dis_req_beneficiary = drb.dis_req_beneficiary") 
           .append( " inner join  person p on  drb.person_id = p.person_id ")           
           .append( " where  r.request_id in ( "+requestView.getRequestId()+ ") and    dd.is_deleted=0  and dd.status not in (165003) ")
           .append( " MINUS   ")
           //.append( " --Beneficiaries in Last Request ")
		   .append( " select ")
		   .append( " pdl.prdc_dis_list_id,p.person_id,dd.amount,p.passport_name,pdl.account_number ,pdl.paid_to_Bank,pdl.paid_to_name from request r ")
		   .append( " inner join disbursement_details dd on r.request_id = dd.request_id ")
		   .append( " inner join  periodic_disbursement_list pdl on dd.prdc_dis_list_id = pdl.prdc_dis_list_id ")
		   .append( " inner join  dis_req_beneficiary drb on dd.dis_det_id = drb.dis_det_id ")
		   .append( " inner join  asso_dis_ben_inf_ben adb on adb.dis_req_beneficiary = drb.dis_req_beneficiary ")
		   .append( " inner join  person p on  drb.person_id = p.person_id ")           
		   .append( " where  r.request_id in (select max(request_id) from request where  disbursement_list_type_id= " )
		   .append( " 																					( select disbursement_list_type_id from request where request_id=").append( requestView.getRequestId() +")")
		   .append( "                           									 and request_id<").append(requestView.getRequestId() )
		   .append( "                               									 and is_deleted=0 and status_id not in(9003,90018) " )
		   .append( " 						 )  and  dd.is_deleted=0  and dd.status not in (165003) " )
		   .append( " ) sq order by sq.passport_name,sq.amount");
			
		}
		catch( Exception e )
		{
			
		}
		return  sqlQuery.toString();
	}
	
	@SuppressWarnings("unchecked")
	private String getDeletedBenFromListQuery()
	{
		StringBuilder sqlQuery = new StringBuilder(" ");
		try
		{
			   sqlQuery.append( " select passport_name, amount, paid_to_name, account_number, paid_to_Bank from  " )
			   .append( "( select pdl.prdc_dis_list_id,p.person_id,dd.amount,p.passport_name,pdl.account_number ,pdl.paid_to_Bank,pdl.paid_to_name from request r ")
			   .append( " inner join disbursement_details dd on r.request_id = dd.request_id ")
			   .append( " inner join  periodic_disbursement_list pdl on dd.prdc_dis_list_id = pdl.prdc_dis_list_id ")
			   .append( " inner join  dis_req_beneficiary drb on dd.dis_det_id = drb.dis_det_id ")
			   .append( " inner join  asso_dis_ben_inf_ben adb on adb.dis_req_beneficiary = drb.dis_req_beneficiary ")
			   .append( " inner join  person p on  drb.person_id = p.person_id ")           
			   .append( " where  r.request_id in ( select max(request_id) from request where  disbursement_list_type_id= " )
			   .append( " 																					( select disbursement_list_type_id from request where request_id=").append( requestView.getRequestId() +")")
			   .append( "                           									 and request_id<").append(requestView.getRequestId() )
			   .append( "                               									 and is_deleted=0 and status_id not in(9003,90018) " )
			   .append( " 						 )  and  dd.is_deleted=0  and dd.status not in (165003) " )
			   .append( " MINUS   ")
			   .append( " select pdl.prdc_dis_list_id, p.person_id,dd.amount,p.passport_name,pdl.account_number,pdl.paid_to_Bank,pdl.paid_to_name from request r ")
	           .append( " inner join  disbursement_details dd on r.request_id = dd.request_id ")
	           .append( " inner join  periodic_disbursement_list pdl on dd.prdc_dis_list_id = pdl.prdc_dis_list_id ")
	           .append( " inner join  dis_req_beneficiary drb on dd.dis_det_id = drb.dis_det_id" )
	           .append( " inner join  asso_dis_ben_inf_ben adb on adb.dis_req_beneficiary = drb.dis_req_beneficiary") 
	           .append( " inner join  person p on  drb.person_id = p.person_id ")           
	           .append( " where  r.request_id in ( "+requestView.getRequestId()+ ") and    dd.is_deleted=0  and dd.status not in (165003) ")
	           .append( " ) sq order by sq.passport_name,sq.amount");	
			
		}
		catch( Exception e )
		{
			
		}
		return  sqlQuery.toString();
	}
	
	public Map<?, ?> getReportDetailMap(ReportDetailCriteria criteria) {
		
		Map<Integer,Map<?,?>> 	sheetInfoMap = new HashMap<Integer, Map<?,?>>();

		Map<String, Object> dataHeaderMap_3 = new HashMap<String, Object>(0);
		dataHeaderMap_3.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getAddedBenColumnHeaders());
		dataHeaderMap_3 .put(ReportConstant.Keys.REPORT_SQL_QUERY, getReqBenInListQuery() );
		dataHeaderMap_3 .put(ReportConstant.Keys.SHEET_NAME,"Request List");
		sheetInfoMap.put(0, dataHeaderMap_3);
		
		Map<String, Object> dataHeaderMap_0 = new HashMap<String, Object>(0);
		dataHeaderMap_0.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getColumnHeaders());
		dataHeaderMap_0 .put(ReportConstant.Keys.REPORT_SQL_QUERY, getTotalBeneficiaryListForListTypeIdQuery() );
		dataHeaderMap_0 .put(ReportConstant.Keys.SHEET_NAME,"Total Ben. in List");
		sheetInfoMap.put(1, dataHeaderMap_0); 
		
		Map<String, Object> dataHeaderMap_4 = new HashMap<String, Object>(0);
		dataHeaderMap_4.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getColumnHeaders());
		dataHeaderMap_4 .put(ReportConstant.Keys.REPORT_SQL_QUERY, getBeneficiaryInListButNotInRequest() );
		dataHeaderMap_4 .put(ReportConstant.Keys.SHEET_NAME,"Ben. in List Not In Request");
		sheetInfoMap.put(2, dataHeaderMap_4); 
		
		Map<String, Object> dataHeaderMap = new HashMap<String, Object>(0);
		dataHeaderMap.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getAddedBenColumnHeaders());
		dataHeaderMap .put(ReportConstant.Keys.REPORT_SQL_QUERY, getAddedBenInListQuery() );
		dataHeaderMap .put(ReportConstant.Keys.SHEET_NAME,"Added Beneficiaries");
		sheetInfoMap.put(3, dataHeaderMap);
		
		Map<String, Object> dataHeaderMap_2 = new HashMap<String, Object>(0);
		dataHeaderMap_2.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getAddedBenColumnHeaders());
		dataHeaderMap_2 .put(ReportConstant.Keys.REPORT_SQL_QUERY, getDeletedBenFromListQuery() );
		dataHeaderMap_2 .put(ReportConstant.Keys.SHEET_NAME,"Deleted Beneficiaries");
		sheetInfoMap.put(4, dataHeaderMap_2);
//		
		Map<String, Object> reportDetailMap = new HashMap<String, Object>(0);
		reportDetailMap.put(ReportConstant.Keys.REPORT_MULTIPLE_SHEETS_MAP, sheetInfoMap );
		reportDetailMap.put(ReportConstant.Keys.EXCEL_SHEETS_COUNT, sheetInfoMap.size() );
		return reportDetailMap;
	}




}
