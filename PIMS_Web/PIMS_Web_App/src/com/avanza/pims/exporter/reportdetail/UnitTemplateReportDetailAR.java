package com.avanza.pims.exporter.reportdetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.exporter.reportdetail.criteria.ReportDetailCriteria;
import com.avanza.pims.exporter.reportdetail.criteria.UnitTemplateReportDetailCriteria;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;

/**
 * 
 * @author Majid Hameed
 *
 */
public class UnitTemplateReportDetailAR implements ReportDetail {

	public UnitTemplateReportDetailAR() {
		if( ddBlockedUnitId == null )
		{
			
			ddBlockedUnitId  = CommonUtil.getDomainDataId(WebConstants.UNIT_STATUS, "Blocked");
		}
		if( ddDeletedPropertyId == null )
		{
			
			ddDeletedPropertyId  = CommonUtil.getDomainDataId(WebConstants.PROPERTY_STATUS, "PROPERTY DELETED");
		}

	}
	public static Long ddBlockedUnitId = null;
	public static Long ddDeletedPropertyId = null;


	private List<String> getColumnHeaders() {

		List<String> columnHeaders = new ArrayList<String>(0);

		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.dewaAccNo"));		
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.unitAreaSqFt"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.unitUsage"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.unitSubType"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.unitType"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.unitNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.buildingName"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.community"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.landNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.dmNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.phoneNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.tradeLNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.nidNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.visaNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.nationality"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.ownerName"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.unitTemplate.ownerNo"));
		columnHeaders.add(CommonUtil.getBundleMessage("listReport.sNo"));
		
		return columnHeaders;
	}

	private String getSqlQuery(ReportDetailCriteria reportCriteria) {


		UnitTemplateReportDetailCriteria criteria = (UnitTemplateReportDetailCriteria) reportCriteria;

		
		String sqlQuery = 
			"SELECT " +

			" CASE  WHEN  U.UNIT_EW_UTILITY_NO IS NOT NULL THEN  CAST( to_char(U.UNIT_EW_UTILITY_NO) AS VARCHAR(100) ) ELSE U.DEWA_NUMBER END dewaAccNo, "+
			"U.UNIT_AREA unitAreaSqFt, " +
			"DD_UUS.DATA_DESC_AR unitUsage, " +
			"DD_US.DATA_DESC_AR  unitSubType, " +
			"DD_UT.DATA_DESC_AR unitType, " +
			"U.UNIT_NUMBER unitNo, " +
			"PROP.COMMERCIAL_NAME buildingName, " +
			"RGN_AREA_C.DESCRIPTION_AR community, " +
			"PROP.LAND_NUMBER landNo, " +
			"PROP.MUNICIPALITY_PLAN_NO dmNo, " +
			"PRSN.CELL_NUMBER phoneNo, " +
			"PRSN.LICENSE_NUMBER tradeLNo, " +
			"'' nidNo, " + // LEAVE IT BLANK
			"PRSN.RESIDENSE_VISA_NUMBER visaNo, " +
			"RGN_AREA_N.DESCRIPTION_AR nationality, " +
			"CASE \n"+ 
		    "WHEN PRSN.IS_COMPANY =0 THEN \n"+
		       "PRSN.FIRST_NAME||' '||PRSN.MIDDLE_NAME||' '||PRSN.LAST_NAME \n"+ 
		       "ELSE \n"+
		       "PRSN.COMPANY_NAME END  \"ownerName\" ,\n"+
			"PRSN.GRP_CUSTOMER_NO ownerNo, " +
			"rowNum sNo " + //-- NON DB --

			" FROM UNIT U " +
			
			" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
			
			" INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = F.PROPERTY_ID" +
			
			// PERSON - PROPERY OWNER
			" LEFT JOIN PROPERTY_OWNER PO ON PO.PROPERTY_ID = PROP.PROPERTY_ID" +
			" LEFT JOIN PERSON PRSN ON PRSN.PERSON_ID = PO.OWNER_ID" +
			
			//" LEFT JOIN PERSON_CONTACT_INFO PCI ON PCI.PERSON_ID = PRSN.PERSON_ID" +
			" LEFT JOIN CONTACT_INFO CI ON  CI.CONTACT_INFO_ID = PROP.CONTACT_INFO_ID" +

			// CONTRACT
	        " LEFT JOIN CONTRACT CNTRT ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID " +
	        
	        // UNIT
	        " LEFT JOIN DOMAIN_DATA DD_UT ON DD_UT.DOMAIN_DATA_ID = U.UNIT_TYPE_ID " +
	        " LEFT JOIN DOMAIN_DATA DD_US ON DD_US.DOMAIN_DATA_ID = U.UNIT_SIDE_TYPE_ID " +
	        " LEFT JOIN DOMAIN_DATA DD_UUS ON DD_UUS.DOMAIN_DATA_ID = U.USAGE_TYPE_ID " +
	        " LEFT JOIN DOMAIN_DATA DD_USTATUS ON DD_USTATUS.DOMAIN_DATA_ID = U.STATUS_ID " +
	        
	        // COMMUNITY
	        " LEFT JOIN REGION RGN_AREA_C ON RGN_AREA_C.REGION_ID = CI.CITY_ID " +
	        
	        // NATIONALITY
			" LEFT JOIN REGION RGN_AREA_N ON RGN_AREA_N.REGION_ID = PRSN.NATIONALITY_ID" ;
		
		String sqlQueryCondition = "";

		// Contract No.
		if(criteria.getTxtContractNumber().length() >0) {
			sqlQueryCondition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getTxtContractNumber() +"%')";
		}	

		// Owner Name
		if(criteria.getTxtOwnerName().length() >0) {
			sqlQueryCondition+= " AND (PRSN.FIRST_NAME like '%" + criteria.getTxtOwnerName() +"%'" +
			" OR PRSN.MIDDLE_NAME like  '%" + criteria.getTxtOwnerName() +"%'"+
			" OR PRSN.LAST_NAME like  '%" + criteria.getTxtOwnerName() +"%'"+
			" ) " ;
		}	
		
		// Property Name
		if(criteria.getTxtPropertyName()!=null && criteria.getTxtPropertyName().length()>0) {
			sqlQueryCondition+=" AND PROP.COMMERCIAL_NAME like  '%" + criteria.getTxtPropertyName() +"%'";
		}	
		
		// Unit No.
		if(criteria.getTxtUnitNumber()!=null && criteria.getTxtUnitNumber().length() >0) {
			sqlQueryCondition+=" AND lower (U.UNIT_NUMBER)  like lower( '%" + criteria.getTxtUnitNumber() +"%')";
		}	

		// Cost Center
		if(criteria.getTxtCostCenter()!=null && criteria.getTxtCostCenter().length() >0) {
			sqlQueryCondition+=" AND lower (U.ACCOUNT_NUMBER)  like lower( '%" + criteria.getTxtCostCenter() +"%')";
		}	
		
		// Property Receiving Date From
		if( criteria.getClndrFromDate()!=null ) {		 						
			sqlQueryCondition += " AND PROP.RECEIVING_DATE  >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrFromDate() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		// Property Receiving Date To		
		if( criteria.getCldrToDate()!=null ) {								
			sqlQueryCondition += " AND  PROP.RECEIVING_DATE  <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getCldrToDate() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}
		
		// Property Type
		if(!criteria.getCboPropertyType().equals("-1")) {
			sqlQueryCondition+= " AND PROP.TYPE_ID = '" + criteria.getCboPropertyType() +"'"	;
		}	
		
		//Property OwnerShipType
		if(!criteria.getCboPropertyOwnershipType().equals("-1")) {
			sqlQueryCondition+=" AND PROP.OWNERSHIP_TYPE_ID='"+criteria.getCboPropertyOwnershipType()+"'";
		}
		
		//Property Usage
		if(!criteria.getCboPropertyUsage().equals("-1")) {
			sqlQueryCondition+=" AND PROP.USAGE_TYPE_ID='"+criteria.getCboPropertyUsage()+"'";
		}
		
		//Unit Status
		if(!criteria.getCboUnitStatus().equals("-1")) {
			sqlQueryCondition+= " AND U.STATUS_ID='" + criteria.getCboUnitStatus()+"'";
		}	
		
		//Unit Type
		if(!criteria.getCboUnittype().equals("-1")) {
			sqlQueryCondition+= " AND U.UNIT_TYPE_ID='" + criteria.getCboUnittype()+"'";
		}
		
		//Unit SubType
		if(!criteria.getCboUnitSubType().equals("-1")) {
			sqlQueryCondition+= " AND U.UNIT_SIDE_TYPE_ID='" + criteria.getCboUnitSubType()+"'";
		}
		
		//Unit Usage
		if(!criteria.getCboUnitUsage().equals("-1")) {
			sqlQueryCondition+= " AND U.USAGE_TYPE_ID='" + criteria.getCboUnitUsage()+"'";
		}	
		
		//Unit Area SqFt From
		if( criteria.getTxtUnitAreaSqFtFrom()!=null && criteria.getTxtUnitAreaSqFtFrom().length()>0)  {
			sqlQueryCondition+=" AND U.UNIT_AREA >=  " + criteria.getTxtUnitAreaSqFtFrom().trim();
		}
		
		//Unit Area SqFt To
		if( criteria.getTxtUnitAreaSqFtTo()!=null && criteria.getTxtUnitAreaSqFtTo().length()>0)  {
			sqlQueryCondition+=" AND U.UNIT_AREA <=  " + criteria.getTxtUnitAreaSqFtTo().trim();
		}
		
		// Community 
		if(!criteria.getCboPropertyCommunity().equals("-1")) {
			sqlQueryCondition+=" AND RGN_AREA_C.REGION_ID= '"+criteria.getCboPropertyCommunity()+"'";
		}
		
		//Owner Nationality ID
		if(!criteria.getCboOwnerNationality().equals("-1")) {
			sqlQueryCondition+=" AND RGN_AREA_N.REGION_ID= '"+criteria.getCboOwnerNationality()+"'";
		}
		
		// Owner Visa No.
		if(criteria.getTxtOwnerVisaNo().length() >0) {
			sqlQueryCondition+=" AND PRSN.RESIDENSE_VISA_NUMBER like  '%" + criteria.getTxtOwnerVisaNo() +"%'";
		}
		
		// Owner Trade L.No.
		if(criteria.getTxtOwnerTradeLNo().length() >0) {
			sqlQueryCondition+=" AND PRSN.LICENSE_NUMBER like  '%" + criteria.getTxtOwnerTradeLNo() +"%'";
		}
		
		// Owner Phone No.
		if(criteria.getTxtOwnerPhoneNo().length() >0) {
			sqlQueryCondition+=" AND PRSN.CELL_NUMBER like  '%" + criteria.getTxtOwnerPhoneNo() +"%'";
		}
		
		// Property DM. No.
		if(criteria.getTxtPropertyDmNo().length() >0) {
			sqlQueryCondition+=" AND PROP.MUNICIPALITY_PLAN_NO like  '%" + criteria.getTxtPropertyDmNo() +"%'";
		}
		
		// Property Dewa Acc No.
		if(criteria.getTxtPropertyDewaAccNo().length() >0) {
			sqlQueryCondition+=" AND( LOWER(U.DEWA_NUMBER    )    like '%" + criteria.getTxtPropertyDewaAccNo().toLowerCase() +"%' OR " +
            " U.UNIT_EW_UTILITY_NO =  "+criteria.getTxtPropertyDewaAccNo()+" )";
		}

		// Owner No.
		if(criteria.getTxtOwnerNo().length() >0) {
			sqlQueryCondition+=" AND PRSN.GRP_CUSTOMER_NO like  '%" + criteria.getTxtOwnerNo() +"%'";
		}
		
		//LEAVE IT BLANK
		/*
		if(criteria.getTxtOwnerNidNo().length() >0) {
			sqlQueryCondition+=" AND PRSN.CELL_NUMBER like  '%" + criteria.getTxtOwnerNidNo() +"%'";
		}
		*/
		
		// Floor No.
		if(criteria.getTxtFloorNumber().length() >0) {
			sqlQueryCondition+=" AND lower(F.FLOOR_NUMBER) like lower('%" + criteria.getTxtFloorNumber()+"%')" ;
		}	

		// Property Land No.
		if(criteria.getTxtLandNumber().length()>0) {
			sqlQueryCondition+=" AND PROP.LAND_NUMBER like '%"+criteria.getTxtLandNumber()+"%'";
		}	
		
		// Property No.
		if(criteria.getTxtPropertyNumber().length()>0) {
			sqlQueryCondition+=" AND lower(PROP.PROPERTY_NUMBER) like lower('%"+criteria.getTxtPropertyNumber()+"%')";
		}	

		// Unit Rent Amount From
		if( criteria.getUnitRentAmountFrom() != null &&  criteria.getUnitRentAmountFrom().length() > 0 ) {
			sqlQueryCondition+=" AND U.RENT_VALUE >= "+criteria.getUnitRentAmountFrom().trim();
		}	
		
		// Unit Rent Amount To
		if( criteria.getUnitRentAmountTo() != null &&  criteria.getUnitRentAmountTo().length() > 0 ) {
			sqlQueryCondition+=" AND U.RENT_VALUE <= "+criteria.getUnitRentAmountTo().trim();
		}	

		// Contract Rent Amount From
		if( criteria.getContractRentAmountFrom() != null &&  criteria.getContractRentAmountFrom().length() > 0 ) {
			sqlQueryCondition+=" AND CNTRT.RENT_AMOUNT >= "+criteria.getContractRentAmountFrom().trim();
		}	

		// Contract Rent Amount To
		if( criteria.getContractRentAmountTo() != null &&  criteria.getContractRentAmountTo().length() > 0 ) {
			sqlQueryCondition+=" AND CNTRT.RENT_AMOUNT <= "+criteria.getContractRentAmountTo().trim();
		}	
		sqlQuery += " WHERE 1=1 AND PROP.IS_DELETED = 0 AND U.IS_DELETED = 0  AND U.STATUS_ID <> "+ ddBlockedUnitId.toString()+"  ";
		if(sqlQueryCondition.length() > 0) { // add query condition
			sqlQuery += sqlQueryCondition;
		}
		
		
		String sqlQuerySorting = ""; //" ORDER BY F.floor_sequence, f.type_id,u.unit_number";

		sqlQuery+=sqlQuerySorting; // add query sorting

		return sqlQuery;
	}

	public Map<?, ?> getReportDetailMap(ReportDetailCriteria criteria) {
		Map<String, Object> reportDetailMap = new HashMap<String, Object>(0);

		reportDetailMap.put(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST, getColumnHeaders());
		reportDetailMap.put(ReportConstant.Keys.REPORT_SQL_QUERY, getSqlQuery(criteria));

		return reportDetailMap;
	}




}
