package com.avanza.pims.exporter.reportdetail.criteria;

import java.util.Date;

/**
 * 
 * @author Majid Hameed
 *
 */
public class UnitTemplateReportDetailCriteria extends AbstractReportDetailCriteria {
	
	private String txtPropertyNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String txtLandNumber;
	private String cboPropertyCommunity;
	private String txtUnitNumber;
	private String cboUnittype;
	private String txtFloorNumber;
	private String txtOwnerName; 
	private Date clndrFromDate;
	private Date cldrToDate;
	private String cboUnitStatus;
	private String txtCostCenter;
	private String unitRentAmountFrom;
	private String unitRentAmountTo;
	private String contractRentAmountFrom;
	private String contractRentAmountTo;
	private String txtContractNumber;
	
	// new fields
	private String cboUnitSubType;
	private String cboUnitUsage;
	
	private String cboOwnerNationality;
	private String txtOwnerVisaNo;
	
	private String txtOwnerTradeLNo;
	private String txtOwnerPhoneNo;
	
	private String txtOwnerNo;
	private String txtOwnerNidNo;
	
	private String txtPropertyDmNo;
	private String txtPropertyDewaAccNo;
	
	private String cboPropertyOwnershipType;
	
	private String txtUnitAreaSqFtFrom;
	private String txtUnitAreaSqFtTo;
	
	
	public UnitTemplateReportDetailCriteria() {
	}

	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}


	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public String getCboUnittype() {
		return cboUnittype;
	}

	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}

	public String getTxtFloorNumber() {
		return txtFloorNumber;
	}

	public void setTxtFloorNumber(String txtFloorNumber) {
		this.txtFloorNumber = txtFloorNumber;
	}

	public Date getClndrFromDate() {
		return clndrFromDate;
	}

	public void setClndrFromDate(Date clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}

	public Date getCldrToDate() {
		return cldrToDate;
	}

	public void setCldrToDate(Date cldrToDate) {
		this.cldrToDate = cldrToDate;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}

	public String getCboUnitStatus() {
		return cboUnitStatus;
	}

	public void setCboUnitStatus(String cboUnitStatus) {
		this.cboUnitStatus = cboUnitStatus;
	}

	public String getTxtCostCenter() {
		return txtCostCenter;
	}

	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}

	public String getUnitRentAmountFrom() {
		return unitRentAmountFrom;
	}

	public void setUnitRentAmountFrom(String unitRentAmountFrom) {
		this.unitRentAmountFrom = unitRentAmountFrom;
	}

	public String getUnitRentAmountTo() {
		return unitRentAmountTo;
	}

	public void setUnitRentAmountTo(String unitRentAmountTo) {
		this.unitRentAmountTo = unitRentAmountTo;
	}

	public String getContractRentAmountFrom() {
		return contractRentAmountFrom;
	}

	public void setContractRentAmountFrom(String contractRentAmountFrom) {
		this.contractRentAmountFrom = contractRentAmountFrom;
	}

	public String getContractRentAmountTo() {
		return contractRentAmountTo;
	}

	public void setContractRentAmountTo(String contractRentAmountTo) {
		this.contractRentAmountTo = contractRentAmountTo;
	}

	public String getCboPropertyCommunity() {
		return cboPropertyCommunity;
	}

	public void setCboPropertyCommunity(String cboPropertyCommunity) {
		this.cboPropertyCommunity = cboPropertyCommunity;
	}

	public String getCboOwnerNationality() {
		return cboOwnerNationality;
	}

	public void setCboOwnerNationality(String cboOwnerNationality) {
		this.cboOwnerNationality = cboOwnerNationality;
	}

	public String getTxtOwnerName() {
		return txtOwnerName;
	}

	public void setTxtOwnerName(String txtOwnerName) {
		this.txtOwnerName = txtOwnerName;
	}

	public String getCboUnitSubType() {
		return cboUnitSubType;
	}

	public void setCboUnitSubType(String cboUnitSubType) {
		this.cboUnitSubType = cboUnitSubType;
	}

	public String getCboUnitUsage() {
		return cboUnitUsage;
	}

	public void setCboUnitUsage(String cboUnitUsage) {
		this.cboUnitUsage = cboUnitUsage;
	}

	public String getTxtOwnerVisaNo() {
		return txtOwnerVisaNo;
	}

	public void setTxtOwnerVisaNo(String txtOwnerVisaNo) {
		this.txtOwnerVisaNo = txtOwnerVisaNo;
	}

	public String getTxtOwnerTradeLNo() {
		return txtOwnerTradeLNo;
	}

	public void setTxtOwnerTradeLNo(String txtOwnerTradeLNo) {
		this.txtOwnerTradeLNo = txtOwnerTradeLNo;
	}

	public String getTxtOwnerPhoneNo() {
		return txtOwnerPhoneNo;
	}

	public void setTxtOwnerPhoneNo(String txtOwnerPhoneNo) {
		this.txtOwnerPhoneNo = txtOwnerPhoneNo;
	}

	public String getTxtOwnerNo() {
		return txtOwnerNo;
	}

	public void setTxtOwnerNo(String txtOwnerNo) {
		this.txtOwnerNo = txtOwnerNo;
	}

	public String getTxtOwnerNidNo() {
		return txtOwnerNidNo;
	}

	public void setTxtOwnerNidNo(String txtOwnerNidNo) {
		this.txtOwnerNidNo = txtOwnerNidNo;
	}

	public String getTxtPropertyDmNo() {
		return txtPropertyDmNo;
	}

	public void setTxtPropertyDmNo(String txtPropertyDmNo) {
		this.txtPropertyDmNo = txtPropertyDmNo;
	}

	public String getTxtPropertyDewaAccNo() {
		return txtPropertyDewaAccNo;
	}

	public void setTxtPropertyDewaAccNo(String txtPropertyDewaAccNo) {
		this.txtPropertyDewaAccNo = txtPropertyDewaAccNo;
	}

	public String getCboPropertyOwnershipType() {
		return cboPropertyOwnershipType;
	}

	public void setCboPropertyOwnershipType(String cboPropertyOwnershipType) {
		this.cboPropertyOwnershipType = cboPropertyOwnershipType;
	}

	public String getTxtUnitAreaSqFtFrom() {
		return txtUnitAreaSqFtFrom;
	}

	public void setTxtUnitAreaSqFtFrom(String txtUnitAreaSqFtFrom) {
		this.txtUnitAreaSqFtFrom = txtUnitAreaSqFtFrom;
	}

	public String getTxtUnitAreaSqFtTo() {
		return txtUnitAreaSqFtTo;
	}

	public void setTxtUnitAreaSqFtTo(String txtUnitAreaSqFtTo) {
		this.txtUnitAreaSqFtTo = txtUnitAreaSqFtTo;
	}

	public String getTxtPropertyNumber() {
		return txtPropertyNumber;
	}

	public void setTxtPropertyNumber(String txtPropertyNumber) {
		this.txtPropertyNumber = txtPropertyNumber;
	}



	
	
}
