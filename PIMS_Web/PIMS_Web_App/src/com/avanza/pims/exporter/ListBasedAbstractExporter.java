package com.avanza.pims.exporter;

/**
 * 
 * @author Majid Hameed
 *
 */
public abstract class ListBasedAbstractExporter implements IExporter {
	
	protected ListBasedExporterMeta exporterMeta;
	
	public ListBasedAbstractExporter(ListBasedExporterMeta exporterMeta) {		
		this.exporterMeta = exporterMeta;		
	}
}
