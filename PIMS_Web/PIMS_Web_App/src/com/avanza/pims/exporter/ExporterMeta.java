package com.avanza.pims.exporter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIData;
import javax.faces.context.FacesContext;

/**
 * 
 * @author averani
 *
 */

public class ExporterMeta {
	
	public static enum ExporterType {ExcelExporter, PDFExporter}	
	
	private ExporterType exporterType;
	private FacesContext facesContext;
	private UIData uiData;
	private String exportFileName;
	private List<Integer> excludeColumnIndex;
	private List<Integer> excludeRowIndex;
	
	public ExporterMeta() {
		this.exporterType = ExporterMeta.ExporterType.ExcelExporter;
		this.facesContext = FacesContext.getCurrentInstance();
		this.uiData = new UIData();
		this.exportFileName = "export";
		this.excludeColumnIndex = new ArrayList<Integer>(0);
		this.excludeRowIndex = new ArrayList<Integer>(0);
	}

	public ExporterType getExporterType() {
		return exporterType;
	}

	public void setExporterType(ExporterType exporterType) {
		this.exporterType = exporterType;
	}

	public FacesContext getFacesContext() {
		return facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public UIData getUiData() {
		return uiData;
	}

	public void setUiData(UIData uiData) {
		this.uiData = uiData;
	}

	public String getExportFileName() {
		return exportFileName;
	}

	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}

	public List<Integer> getExcludeColumnIndex() {
		return excludeColumnIndex;
	}

	public void setExcludeColumnIndex(List<Integer> excludeColumnIndex) {
		this.excludeColumnIndex = excludeColumnIndex;
	}

	public List<Integer> getExcludeRowIndex() {
		return excludeRowIndex;
	}

	public void setExcludeRowIndex(List<Integer> excludeRowIndex) {
		this.excludeRowIndex = excludeRowIndex;
	}
	
}
