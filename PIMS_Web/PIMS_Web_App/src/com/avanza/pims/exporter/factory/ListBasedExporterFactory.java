package com.avanza.pims.exporter.factory;

import com.avanza.pims.exporter.IExporter;
import com.avanza.pims.exporter.ListBasedExcelExporter;
import com.avanza.pims.exporter.ListBasedExporterMeta;


/**
 * 
 * @author Majid Hameed
 *
 */
public abstract class ListBasedExporterFactory {
	
	public static IExporter getExporter(ListBasedExporterMeta exporterMeta) {
		
		IExporter exporter = null;
		
		if ( exporterMeta.getExporterType().equals(ListBasedExporterMeta.ExporterType.ExcelExporter ) ) {
			exporter = new ListBasedExcelExporter(exporterMeta);
		}	
		
		
		return exporter;
		
	}
	
}
