package com.avanza.pims.exporter;

import javax.faces.context.FacesContext;

import com.avanza.pims.exporter.reportdetail.ReportDetail;
import com.avanza.pims.exporter.reportdetail.criteria.ReportDetailCriteria;

/**
 * ExporterMeta for the list based reports
 * @author Majid Hameed
 *
 */
public class ListBasedExporterMeta {
	
	public static enum ExporterType{ExcelExporter};	
	
	private ExporterType exporterType;
	private FacesContext facesContext;
	private String exportFileName;
	private ReportDetail reportDetail;
	private ReportDetailCriteria reportCriteria;
	
	public ListBasedExporterMeta(){
	}
	
	public ListBasedExporterMeta(ReportDetailCriteria reportCriteria) {
		this.exporterType = ListBasedExporterMeta.ExporterType.ExcelExporter; // default value
		this.facesContext = FacesContext.getCurrentInstance();
		this.exportFileName = "export"; // defaultValue
		this.reportCriteria = reportCriteria;
	}

	public ExporterType getExporterType() {
		return exporterType;
	}

	public void setExporterType(ExporterType exporterType) {
		this.exporterType = exporterType;
	}

	public FacesContext getFacesContext() {
		return facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getExportFileName() {
		return exportFileName;
	}

	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}

	public ReportDetail getReportDetail() {
		return reportDetail;
	}

	public void setReportDetail(ReportDetail reportDetail) {
		this.reportDetail = reportDetail;
	}

	public ReportDetailCriteria getReportCriteria() {
		return reportCriteria;
	}

	public void setReportCriteria(ReportDetailCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}


	
}
