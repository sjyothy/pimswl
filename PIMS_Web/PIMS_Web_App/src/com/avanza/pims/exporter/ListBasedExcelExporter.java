package com.avanza.pims.exporter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.exporter.reportdetail.ReportDetail;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.Service;

/**
 * ListBasedExcelExporter - Exports the XLS sheet for the given SQL query
 * @author Majid Hameed
 *
 */
public class ListBasedExcelExporter extends ListBasedAbstractExporter 
{
	
	Service  service;
	public ListBasedExcelExporter(ListBasedExporterMeta exporterMeta) 
	{
		super(exporterMeta);		
	}

	@SuppressWarnings(  "unchecked" )
	public void export() throws Exception 
	{	
		service = new Service();
		HSSFWorkbook wb = new HSSFWorkbook();
		ReportDetail reportDetail = exporterMeta.getReportDetail();
		Map<Integer,Map<?,?>> 	sheetInfoMap = new HashMap<Integer, Map<?,?>>();
		Map<?, ?> reportMap = reportDetail.getReportDetailMap(exporterMeta.getReportCriteria());
		int noOfSheets= 1;
		if(reportMap.containsKey( ReportConstant.Keys.EXCEL_SHEETS_COUNT ) )
		{
			noOfSheets = new Integer( reportMap.remove( ReportConstant.Keys.EXCEL_SHEETS_COUNT ).toString() );
			sheetInfoMap = (Map<Integer,Map<?,?>> )reportMap.remove( ReportConstant.Keys.REPORT_MULTIPLE_SHEETS_MAP );
		}
		else
		{
			sheetInfoMap.put( 0, reportMap ); 
		}
		for ( int i = 0; i < noOfSheets; i++)
		{
			String sheetName="Records";
			Map<?, ?> reportDetailsMap = sheetInfoMap.get(i); 
			List<?> list = getDataList( reportDetailsMap );
			if(  reportDetailsMap.containsKey(ReportConstant.Keys.SHEET_NAME) )
			{
				sheetName = reportDetailsMap.remove(ReportConstant.Keys.SHEET_NAME).toString();
			}
			List<?> columnHeaderList = (List<?>) reportDetailsMap.get(ReportConstant.Keys.REPORT_COLUMN_HEADERS_LIST);
			createSheetInWorkBook( 
									wb, 
									list, 
									columnHeaderList,
									sheetName
								 );
		}
		writeExcelToResponse(
        					    ( HttpServletResponse ) this.exporterMeta.getFacesContext().getExternalContext().getResponse(), 
        					    wb,
        					    this.exporterMeta.getExportFileName() 
        					);
	}

	/**
	 * @param wb
	 * @throws PimsBusinessException
	 */
	private void createSheetInWorkBook(
										HSSFWorkbook wb,
										List<?> list,
										List<?> columnHeaderList,
										String sheetName
									  )throws PimsBusinessException 
	{
		HSSFSheet sheet = wb.createSheet(sheetName);
 
		sheet.setDefaultColumnWidth( new Short("20") );
		addColumnHeaders(wb,sheet, columnHeaderList);

		int excelRowIndex = 1; // as we already added headers at row=0
		for (int rowIndex = 0; rowIndex < list.size(); rowIndex++) 
		{ 
			// for each Row
			Object[] listRow = (Object[]) list.get(rowIndex);
			HSSFRow row = sheet.createRow(excelRowIndex++);
			for (int columnIndex = 0; columnIndex< listRow.length; columnIndex++) 
			{ 
				// for each Column
				addColumnValue(row, listRow[columnIndex], columnIndex);
			}
		}

		
		list = null; 
        columnHeaderList = null;
	}

	/**
	 * @param reportDetailsMap
	 * @param list
	 * @return
	 * @throws PimsBusinessException
	 */
	private List<?> getDataList(Map<?, ?> reportDetailsMap) throws PimsBusinessException 
	{
		List<?> list = null;
		if( reportDetailsMap.get(ReportConstant.Keys.REPORT_SQL_QUERY) != null )
		{
			String sqlQuery = (String) reportDetailsMap.get(ReportConstant.Keys.REPORT_SQL_QUERY);
			list = service.createSqlQuery(sqlQuery);
		}
		else if( reportDetailsMap.get( ReportConstant.Keys.REPORT_DATA_LIST ) != null  )
		{
			list = (List<?>)reportDetailsMap.get( ReportConstant.Keys.REPORT_DATA_LIST );
		}
//		if (list==null || list.size()==0) 
//		{
//			throw new PimsBusinessException(
//											ExceptionCodes.REPORT_NO_RECORDS_FOUND,
//											CommonUtil.getBundleMessage( MessageConstants.Report.ERROR_NO_RECORD_FOUND )
//											);
//		}
		return list;
	}
	
	private void addColumnHeaders(HSSFWorkbook workBook,HSSFSheet sheet, List<?> columnList) 
	{
		
		HSSFRow rowHeader = sheet.createRow(0);
		
		HSSFCellStyle style = addDefaultHeaderSytle(workBook);
		
		
		for ( int index = 0; index < columnList.size(); index++ ) 
		{
			HSSFCell cell = rowHeader.createCell((short)index);
			cell.setCellStyle(style); // add style
			cell.setEncoding(HSSFCell.ENCODING_UTF_16);
			String columnValue =  (String)columnList.get(index);
			
			if (columnValue != null) {			
				cell.setCellValue(columnValue.toString());
			}	
			else {		
				cell.setCellValue("");
			}	

		}
    }

	private HSSFCellStyle addDefaultHeaderSytle(HSSFWorkbook workBook) {
		HSSFCellStyle style = workBook.createCellStyle();
		style.setFillBackgroundColor(HSSFColor.BLACK.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		HSSFFont font = workBook.createFont();
		font.setColor(HSSFColor.WHITE.index);
		
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private void addColumnValue(HSSFRow rowHeader, Object columnValue, int index) {
		
		HSSFCell cell = rowHeader.createCell((short)index);
		
		cell.setEncoding(HSSFCell.ENCODING_UTF_16);		
		
		if (columnValue != null) {			
			cell.setCellValue(columnValue.toString());
		}	
		else {		
			cell.setCellValue("");
		}	
		
	}
	
	private void writeExcelToResponse(HttpServletResponse response, HSSFWorkbook generatedExcel, String filename) throws IOException {
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".xls").toString());
        generatedExcel.write(response.getOutputStream());
	}
}
