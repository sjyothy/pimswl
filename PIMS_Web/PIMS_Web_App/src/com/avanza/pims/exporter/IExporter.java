package com.avanza.pims.exporter;

/**
 * 
 * @author averani
 *
 */

public interface IExporter {
	
	public abstract void export() throws Exception;
}
