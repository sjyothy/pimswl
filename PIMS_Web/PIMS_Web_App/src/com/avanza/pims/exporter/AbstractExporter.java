package com.avanza.pims.exporter;

/**
 * 
 * @author averani
 *
 */

public abstract class AbstractExporter implements IExporter {
	
	protected ExporterMeta exporterMeta;
	
	public AbstractExporter(ExporterMeta exporterMeta) {		
		this.exporterMeta = exporterMeta;		
	}
}
