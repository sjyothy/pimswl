package com.avanza.pims.exporter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.custom.sortheader.HtmlCommandSortHeader;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 
 * @author averani
 *
 */

public class ExcelExporter extends AbstractExporter {
	
	public ExcelExporter(ExporterMeta exporterMeta) {
		super(exporterMeta);		
	}

	public void export() throws Exception 
	{		
		List<UIColumn> uiColumnList = getUIColumnList();
		int numberOfColumns = uiColumnList.size();
		int numberOfRows = this.exporterMeta.getUiData().getRowCount();
		int rowIndex = this.exporterMeta.getUiData().getRowIndex();
		int excelRowIndex = 1;
		
		HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        
        addColumnHeaders(sheet, uiColumnList);
        
        for ( int currentRowIndex = 0; currentRowIndex < numberOfRows; currentRowIndex++ )
        {
        	if ( ! this.exporterMeta.getExcludeRowIndex().contains( currentRowIndex ) )
        	{
	        	this.exporterMeta.getUiData().setRowIndex(currentRowIndex);
	        	
	        	HSSFRow row = sheet.createRow(excelRowIndex++);
	        	
	        	for ( int currentColumnIndex = 0; currentColumnIndex < numberOfColumns; currentColumnIndex++ )
	        	{
	        		UIColumn uiColumn = uiColumnList.get(currentColumnIndex);
	        		addColumnValue(row, (UIComponent) uiColumn.getChildren().get(0), currentColumnIndex);
	        	}
        	}
        }
        
        this.exporterMeta.getUiData().setRowIndex(rowIndex);
        
        writeExcelToResponse( (HttpServletResponse) this.exporterMeta.getFacesContext().getExternalContext().getResponse(), wb, this.exporterMeta.getExportFileName() );
	}
	
	private List<UIColumn> getUIColumnList()
	{
		List<UIColumn> uiColumnList = new ArrayList<UIColumn>(0);
		
		for ( int index = 0; index < this.exporterMeta.getUiData().getChildCount(); index++ )
		{
			if ( (! this.exporterMeta.getExcludeColumnIndex().contains( index )) && (this.exporterMeta.getUiData().getChildren().get(index) instanceof UIColumn) )
			{
				uiColumnList.add( (UIColumn) this.exporterMeta.getUiData().getChildren().get(index) );
			}
		}
		
		return uiColumnList;		
	}
	
	private void addColumnHeaders(HSSFSheet sheet, List<UIColumn> uiColumnList)	
    {
		HSSFRow rowHeader = sheet.createRow(0);
						
		for ( int index = 0; index < uiColumnList.size(); index++ )
		{
			addColumnValue(rowHeader, uiColumnList.get(index).getHeader(), index);			
		}
    }
	
	private void addColumnValue(HSSFRow rowHeader, UIComponent component, int index)
	{
		HSSFCell cell = rowHeader.createCell((short)index);
//		cell.setEncoding( HSSFCell.ENCODING_UTF_16 );		
		
		Object value = null;
		
		if ( component instanceof ValueHolder )
			value = ((ValueHolder) component).getValue();
		else if ( component instanceof HtmlCommandSortHeader )
			value = ((HtmlCommandSortHeader) component).getPropertyName();
		
		
		if ( value != null )			
			cell.setCellValue( value.toString() );		
		else		
			cell.setCellValue("");		
		
	}
	
	private void writeExcelToResponse(HttpServletResponse response, HSSFWorkbook generatedExcel, String filename) throws IOException
	{
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-Type", "charset=UTF-8");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".xls").toString());
        generatedExcel.write(response.getOutputStream());
	}
}
