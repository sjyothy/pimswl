package com.avanza.pims.exporter;

/**
 * 
 * @author averani
 *
 */

public abstract class ExporterFactory {
	
	public static IExporter getExporter(ExporterMeta exporterMeta) {
		
		IExporter exporter = null;
		
		if ( exporterMeta.getExporterType().equals( ExporterMeta.ExporterType.ExcelExporter ) )
			exporter = new ExcelExporter(exporterMeta);
		
		else if ( exporterMeta.getExporterType().equals( ExporterMeta.ExporterType.PDFExporter ) )
			exporter = new PDFExporter(exporterMeta);
		
		return exporter;
		
	}
	
}
