package com.avanza.pims.activedirectory;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

public class LDAPEntryParser {
	public static final String LDAP_USERNAME_ATTR_NAME = "sAMAccountName";
	public static final String LDAP_ENGLISH_NAME_ATTR_NAME = "name";
	public static final String LDAP_ARABIC_NAME_ATTR_NAME = "streetAddress";
	public static final String LDAP_ENGLISH_TITLE_ATTR_NAME = "title";
	public static final String LDAP_ARABIC_TITLE_ATTR_NAME = "st";
	public static final String LDAP_EMPLOYEE_NUMBER_ATTR_NAME = "employeeID";
	public static final String LDAP_ARABIC_SECTION_ATTR_NAME = "postalCode";
	public static final String LDAP_ENGLISH_SECTION_ATTR_NAME = "description";
	public static final String LDAP_DEPARTMENT_ATTR_NAME = "department";
	public static final String LDAP_TELEPHONE_NUMBER_ATTR_NAME = "telephoneNumber";
	public static final String LDAP_MAIL_ATTR_NAME = "mail";
	public static final String LDAP_MANAGER_ATTR_NAME = "manager";
	//public static final String LDAP_MOBILE_ATTR_NAME = "mobile";
	public static final String LDAP_MOBILE_ATTR_NAME = "ipphone";
		
	public static LDAPEntry parse(Attributes attrs) {
		LDAPEntry entry = new LDAPEntry();
		if (attrs == null) {
			return null;
		}
		
		try {
			entry.setUsername(getAttributeValue(attrs, LDAP_USERNAME_ATTR_NAME));
			if (entry.getUsername() != null) {
				entry.setUsername(entry.getUsername().toLowerCase());
			}
			entry.setEnglishName(getAttributeValue(attrs, LDAP_ENGLISH_NAME_ATTR_NAME));
			entry.setArabicName(getAttributeValue(attrs, LDAP_ARABIC_NAME_ATTR_NAME));
			entry.setEnglishTitle(getAttributeValue(attrs, LDAP_ENGLISH_TITLE_ATTR_NAME));
			entry.setArabicTitle(getAttributeValue(attrs, LDAP_ARABIC_TITLE_ATTR_NAME));
			entry.setEmployeeNumber(getAttributeValue(attrs, LDAP_EMPLOYEE_NUMBER_ATTR_NAME));
			entry.setArabicSection(getAttributeValue(attrs, LDAP_ARABIC_SECTION_ATTR_NAME));
			entry.setEnglishSection(getAttributeValue(attrs, LDAP_ENGLISH_SECTION_ATTR_NAME));
			entry.setDepartment(getAttributeValue(attrs, LDAP_DEPARTMENT_ATTR_NAME));
			entry.setTelephoneNumber(getAttributeValue(attrs, LDAP_TELEPHONE_NUMBER_ATTR_NAME));
			entry.setEmail(getAttributeValue(attrs, LDAP_MAIL_ATTR_NAME));
			entry.setMobile(getAttributeValue(attrs, LDAP_MOBILE_ATTR_NAME));
			
			//Load Manager Username
//			entry.setManager(LDAPController.searchMananger(getAttributeValue(attrs, LDAP_MANAGER_ATTR_NAME)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return entry;		
	}
	
	private static String getAttributeValue(Attributes attrs, String attributeName) throws NamingException {
		Attribute value = attrs.get(attributeName);
		if (value == null) {
			return null;
		} 
		
		return ((String) value.get());
	}
}
