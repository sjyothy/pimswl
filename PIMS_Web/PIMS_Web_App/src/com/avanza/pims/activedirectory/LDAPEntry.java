package com.avanza.pims.activedirectory;

public class LDAPEntry {

	private String username;
	private String englishName;
	private String arabicName;
	private String englishTitle;
	private String arabicTitle;
	private String employeeNumber;
	private String arabicSection;
	private String englishSection;
	private String department;
	private String telephoneNumber;
	private String email;
	private String manager;
	private String mobile;
	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public String getArabicTitle() {
		return arabicTitle;
	}

	public void setArabicTitle(String arabicTitle) {
		this.arabicTitle = arabicTitle;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getArabicSection() {
		return arabicSection;
	}

	public void setArabicSection(String arabicSection) {
		this.arabicSection = arabicSection;
	}

	public String getEnglishSection() {
		return englishSection;
	}

	public void setEnglishSection(String englishSection) {
		this.englishSection = englishSection;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setUsername(String username) {
		this.username = username;
		if (username != null) {
			username = username.toLowerCase();
		}
	}
	
	public String getUsername() {
		return username;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}	
}
