package com.avanza.pims.activedirectory;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.AuthenStatus;
import com.avanza.core.security.AuthenticationProvider;
import com.avanza.core.security.AuthenticationStatus;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LDAPController implements AuthenticationProvider{
	
    private static final Logger logger = Logger.getLogger(LDAPController.class);
        public static final String LDAP_SERVER = "192.168.2.29";
    
        public static final String LDAP_SERVER_PORT = "389";
        public static final String LDAP_AD_PRINCIPAL_SUFFIX = "@amaf.ae";
        public static final String LDAP_PRINCIPAL = "amafapps" + LDAP_AD_PRINCIPAL_SUFFIX;
        public static final String LDAP_CREDENTIAL = "a123456_";
        public static final String USERS_CONTAINER = "ou=AMAF,dc=amaf,dc=ae";
    
    
       private static final String LDAP_CONTEXT_FACTORY = "contextFactory";
       private static final String LDAP_PROVIDER_URL = "providerURL";
       private static final String LDAP_SECURITY_PRINCIPAL = "userName";
       private static final String LDAP_SECURITY_CREDENTIALS = "password";
       private static final String SSO_USER_BASE_PATH = "userBase";
       private static final String OBJECT_CLASS = "objectClass";
       private static final String LOGIN_FILTER_NAME = "loginFilter";
       private static final String GROUP_CLASS = "groupClass";
       private static final String SSO_GROUP_BASE_PATH = "groupBase";
       private static final String GROUP_FILTER_NAME = "groupFilter";
       private static final String USER_LOGIN_ID_LDAP_ATTRIBUTE = "uid";
       private static final String USER_PASSWORD_LDAP_ATTRIBUTE = "userPassword";
       private static final String SN_LDAP_ATTRIBUTE = "sn";
       private static final String CN_LDAP_ATTRIBUTE = "cn";
       private static final String OBJECT_CLASSES_FOR_NEW_USER = "objectClassesForNewUser";
       
       
       
    private String initialContextFactory;
        private String providerURL;
        private String securityPrincipal;
        private String securityCredentials;
        private String userBasePath;
        private String objectClass;
        private String loginFilterName;
        private String groupBasePath;
        private String groupClass;
        private String groupFilterName;
        private String[] objectClassesForNewUser;
        
        private Hashtable env;
        private Map<String, String> attributeMap = new HashMap<String, String>(0);



    public void load(ConfigSection section) {
           if (section == null) {
               throw new SecurityException("LDAP Configuration not found");
           }
           else {
               initialContextFactory = section.getValue(LDAP_CONTEXT_FACTORY, StringHelper.EMPTY);
               providerURL = section.getValue(LDAP_PROVIDER_URL, StringHelper.EMPTY);
               securityPrincipal = section.getValue(LDAP_SECURITY_PRINCIPAL, StringHelper.EMPTY);
               securityCredentials = section.getValue(LDAP_SECURITY_CREDENTIALS, StringHelper.EMPTY);
               userBasePath = section.getValue(SSO_USER_BASE_PATH, StringHelper.EMPTY);
               objectClass = section.getValue(OBJECT_CLASS, StringHelper.EMPTY);
               loginFilterName = section.getValue(LOGIN_FILTER_NAME, StringHelper.EMPTY);
               groupBasePath = section.getValue(SSO_GROUP_BASE_PATH, StringHelper.EMPTY);
               groupClass = section.getValue(GROUP_CLASS, StringHelper.EMPTY);
               groupFilterName = section.getValue(GROUP_FILTER_NAME, StringHelper.EMPTY);   
               
               String objectClassesForNewUser = section.getValue(OBJECT_CLASSES_FOR_NEW_USER, StringHelper.EMPTY);             
               if (StringHelper.isNotEmpty(objectClassesForNewUser))               
               {
                   this.objectClassesForNewUser =  objectClassesForNewUser.split(",");
               }
               
               env = null;

               //preparing the attriibute map.
               List<ConfigSection> childs = section.getChildSections("attribute");
               for(ConfigSection cfgSection : childs) 
               {  
                   attributeMap.put(cfgSection.getTextValue("ldap-attribute"), cfgSection.getTextValue("user-attribute"));
               }
    //            for(String s : attributeMap.keySet() )
    //            {
    //              System.out.println("Key: " + s + " Value "+ attributeMap.get(s) );
    //            }
               
               try {
                   getDefaultDirContext();
                   System.out.println("authenticate user: "+authenticateUser("pims_admin", "P123456_"));
    //                getGroups(null);
               } catch (NamingException ex) {
                   throw new com.avanza.core.security.SecurityException(ex, ex.getMessage());
               }
           }
       }

    private void getDefaultDirContext() throws NamingException {
            env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
            env.put(Context.PROVIDER_URL, providerURL);
            env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
            env.put(Context.SECURITY_CREDENTIALS, securityCredentials);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
        }
	
	
	
	
	public AuthenticationStatus authenticateUser(String username, String password) {
		
                AuthenticationStatus authenticationStatus = new AuthenticationStatus();
                boolean authenticated = false;
		DirContext ctx = null;
		try {	
		        logger.logDebug("Authenticating user: " + username);
			getDefaultDirContext();
			
			env.put(Context.SECURITY_PRINCIPAL, username + LDAP_AD_PRINCIPAL_SUFFIX);
			env.put(Context.SECURITY_CREDENTIALS, password);
		        
			ctx = new InitialDirContext(env);
			authenticated = true;
		        logger.logDebug("Authentication successful. User:" + username);
                        authenticationStatus.setAuthenStatus(AuthenStatus.SUCCESS);
			
		    } catch (AuthenticationException authEx){
		               logger.logError("Admin user, authentication failed", authEx);
		               logger.logError("LDAP connection failed with admin user.");
		               authenticationStatus.setAuthenStatus(AuthenStatus.PROVIDER_ERROR);          
		           } catch (NameAlreadyBoundException nabe) {
		               logger.logError("Username is already in use. Please choose another.", nabe);
		               logger.logError("LDAP connection failed with admin user.");
		               authenticationStatus.setAuthenStatus(AuthenStatus.PROVIDER_ERROR);
		           } catch (NamingException ne) {
		               logger.logError("NamingException: ", ne);
		               logger.logError("LDAP connection failed with admin user.");
		               authenticationStatus.setAuthenStatus(AuthenStatus.PROVIDER_ERROR);
		           } catch (Exception e) {
		               logger.logError("User account was not created.", e);
		               logger.logError("LDAP connection failed with admin user.");
		               authenticationStatus.setAuthenStatus(AuthenStatus.PROVIDER_ERROR);
		           }
                finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
				    logger.logError("Unable to close ldap context.", e);
				}
			}
		}
		return authenticationStatus;
	}
	
	private NamingEnumeration<SearchResult> searchLDAP(String name, String filter) {
		
		NamingEnumeration<SearchResult> results = null;
		DirContext ctx = null;		
		try {
			ctx = new InitialDirContext(env);
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            results = ctx.search( name, filter, controls );     
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
				}
			}
		}
		
		return results;
	}
	
	public LDAPEntry searchLDAPUserEntry(String username) {
		System.out.println("Reading Directory Entry for User : "+username);
    	NamingEnumeration<SearchResult> results = searchLDAP(USERS_CONTAINER, "(&(objectclass=user)(!(objectclass=computer))(samaccountname="+ username +"))");
    	
    	LDAPEntry entry = null;
    	try  {
	    	while (results.hasMore()) {
			    SearchResult result = (SearchResult) results.next();			    
			    Attributes attributes = result.getAttributes();
			    entry = parseLDAPEntryAttributes(attributes);
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return entry;
	}
	
	public Map<String, LDAPEntry> loadLDAPUserEntryMap() {
		System.out.println("Loading Directory Entries for All Users");
		Map<String, LDAPEntry> ldapEntries = new HashMap<String, LDAPEntry>();
		NamingEnumeration<SearchResult> results = null;
		try {
			results = searchLDAP(USERS_CONTAINER, "(&(objectclass=user)(!(objectclass=computer)))");
			if (results != null) {
				while (results.hasMore()) {
					SearchResult searchResult = (SearchResult) results.next();			    
				    Attributes attributes = searchResult.getAttributes();
				    LDAPEntry entry = parseLDAPEntryAttributes(attributes);
				    if (entry != null) {
				    	ldapEntries.put(entry.getUsername().toLowerCase(), entry);
				    }		
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (Exception e) {
				}
			}
		}		
		return ldapEntries;
	}
	
	private LDAPEntry parseLDAPEntryAttributes(Attributes attributes) {
		return LDAPEntryParser.parse(attributes);
	}
	
	public  List<String> listLDAPUsernames() {
		System.out.println("Loading All Usernames for Directory");
		List<String> usernamesList = new ArrayList<String>();
		NamingEnumeration<SearchResult> results = null;
		try {
			results = searchLDAP(USERS_CONTAINER, "(&(objectclass=user)(!(objectclass=computer)))");
			if (results != null) {
				while (results.hasMore()) {
				    SearchResult searchResult = (SearchResult) results.next();			    
				    Attributes attributes = searchResult.getAttributes();
				    Attribute attr = attributes.get("samaccountname");
				    String uid = (String) attr.get();		      
				    if (uid != null) {
				    	usernamesList.add(uid.toLowerCase());
				    }
				}	    
			}		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (Exception e) {
				}
			}
		}
		
		return usernamesList;
	
	}	
	
	public String searchMananger(String managerCN) {
    	//System.out.println("Loading Manager Entry : "+managerCN);
    	NamingEnumeration<SearchResult> managerResults = searchLDAP(USERS_CONTAINER, "(&(objectclass=user)(!(objectclass=computer))(distinguishedName="+ managerCN +"))");
    	
    	String managerName = null;
    	try  {
	    	while (managerResults.hasMore()) {
			    SearchResult mResult = (SearchResult) managerResults.next();			    
			    Attributes mAttributes = mResult.getAttributes();
			    Attribute mAttr = mAttributes.get("samaccountname");
			    if (mAttr != null) {
			    	managerName = (String) mAttr.get();
			    	managerName = managerName.toLowerCase();
			    }
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return managerName;

	}
	public static void main (String [] args) {		
		//System.out.println("List ; "+LDAPAuthenticator.listLDapUsersUID());
		LDAPController con = new LDAPController();
		System.out.println("List ; " + con.listLDAPUsernames());		
		Map<String, LDAPEntry> entries = con.loadLDAPUserEntryMap();
		System.out.println("Size of the Map : " + entries.size());
		
		System.out.println("resutl: "+ con.authenticateUser("pims_admin", "P123456_"));;
		
	}

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public AuthenticationStatus authenticate(String username, String password) {
        return authenticateUser(username, password);
    }

    @Override
    public AuthenticationStatus changePassword(User user, String string, String string2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<User> getUsers(Search search) {
        // TODO Implement this method
        return Collections.emptyList();
    }

    @Override
    public List<UserGroup> getGroups(Search search) {
        // TODO Implement this method
        return Collections.emptyList();
    }

    @Override
    public boolean userExists(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
