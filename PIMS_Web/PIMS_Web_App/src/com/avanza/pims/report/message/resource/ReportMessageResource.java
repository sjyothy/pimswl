package com.avanza.pims.report.message.resource;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.util.SystemParameters;

import com.avanza.util.ResourceWrapper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import java.util.ResourceBundle;

import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ReportMessageResource {
    private static Logger logger = Logger.getLogger(ReportMessageResource.class);
    private static FileConfiguration messageResourceEn;
    private static FileConfiguration messageResourceAr;
    
    private static Map<String, ResourceBundle> resourceBundles;

    public static String property = "com.avanza.pims.web.messageresource.Messages";
    private static Locale localeEn;
    private static Locale localeAr;
    private static Properties propertiesEn;
    private static Properties propertiesAr;

    static {
        resourceBundles = new HashMap<String, ResourceBundle>(); 
        localeEn = new Locale("en");
        localeAr = new Locale("ar");
//        messageResourceEn = new PropertiesConfiguration();
//        messageResourceEn.setEncoding("UTF-8");
//
//        messageResourceAr = new PropertiesConfiguration();
//        messageResourceAr.setEncoding("UTF-8");
//
//        propertiesEn = new Properties();
//        propertiesAr = new Properties();
    }

    public static void load(String messageResourceEnFilePath, String messageResourceArFilePath) throws Exception {

    
//        messageResourceEn.load(new BufferedInputStream(new FileInputStream(messageResourceEnFilePath)));
//        messageResourceEn.setFileName(messageResourceEnFilePath);
//
//        messageResourceAr.load(new BufferedInputStream(new FileInputStream(messageResourceArFilePath)));
//        messageResourceAr.setFileName(messageResourceArFilePath);
//
//        propertiesAr.load(new BufferedInputStream(new FileInputStream(messageResourceArFilePath)));
//        propertiesEn.load(new BufferedInputStream(new FileInputStream(messageResourceEnFilePath)));

    }
    private static ResourceBundle getResourceBundle(String bundleName, Locale locale) {
        ResourceBundle bundle = resourceBundles.get(bundleName + locale.toString());
        
        if (bundle == null) {
            bundle = ResourceBundle.getBundle(bundleName, locale);
            resourceBundles.put((bundleName + locale.toString()), bundle);
        }
        return bundle;
    }

    public static String getPropertyEn(String key) {

        
        String keyValue = getResourceBundle(property, localeEn).getString(key);
//        String keyValue = messageResourceEn.getString(key);
//        logger.logInfo("getPropertyEn|key:%s|keyValue:%s", key, keyValue);
//        if (keyValue != null && !keyValue.equalsIgnoreCase(""))
//            keyValue = keyValue.trim();
//        else
//            keyValue = "";

        return keyValue;
    }

    public static String getPropertyAr(String key) {
        String keyValue = getResourceBundle(property, localeAr).getString(key);
//        String keyValue = messageResourceAr.getString(key);
//        logger.logInfo("getPropertyAr|key:%s|keyValue:%s", key, keyValue);
//        if (keyValue != null && !keyValue.equalsIgnoreCase(""))
//            keyValue = keyValue.trim();
//        else
//            keyValue = "";

        return keyValue;
    }

    public static FileConfiguration getMessageResourceEn() {
        return messageResourceEn;
    }

    public static void setMessageResourceEn(FileConfiguration messageResourceEn) {
        ReportMessageResource.messageResourceEn = messageResourceEn;
    }

    public static FileConfiguration getMessageResourceAr() {
        return messageResourceAr;
    }

    public static void setMessageResourceAr(FileConfiguration messageResourceAr) {
        ReportMessageResource.messageResourceAr = messageResourceAr;
    }

    public static Properties getPropertiesEn() {
        return propertiesEn;
    }

    public static void setPropertiesEn(Properties propertiesEn) {
        ReportMessageResource.propertiesEn = propertiesEn;
    }

    public static Properties getPropertiesAr() {
        return propertiesAr;
    }

    public static void setPropertiesAr(Properties propertiesAr) {
        ReportMessageResource.propertiesAr = propertiesAr;
    }
}
