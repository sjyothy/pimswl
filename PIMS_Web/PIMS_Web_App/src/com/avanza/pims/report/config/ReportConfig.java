package com.avanza.pims.report.config;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;

public class ReportConfig {
	
	private static Properties reportConfig;
	
	static {
		reportConfig = new Properties();
	}
	
	public static void load( String configFilePath ) throws Exception {
		reportConfig.load( new BufferedInputStream( new FileInputStream( configFilePath ) ) );
	}
	
	public static String getConfig( String configName ) {		 
		
		String configValue = reportConfig.getProperty( configName );
		
		if ( configValue != null && !configValue.equalsIgnoreCase("") )
			configValue = configValue.trim();
		else
			configValue = "";
		
		return configValue;
	}

	public static Properties getReportConfig() {
		return reportConfig;
	}

	public static void setReportConfig(Properties reportConfig) {
		ReportConfig.reportConfig = reportConfig;
	}	
}
