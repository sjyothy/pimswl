package com.avanza.pims.report.criteria;

public class VendorsReportCriteria extends AbstractReportCriteria{

	public VendorsReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}
	private String venderGRPNumber;
	private String vendorStatus;
	//private String vendorArabicName
	private String vendorName;
	private String vendorType;
	private String emirate; 
	private String city;
	private String area;
	private String telephoneNumber;
	private String faxNumber;
	private String contactPerson;
	private String email;
	private String licenseNumber;
	private String licenseSource;
	public String getVenderGRPNumber() {
		return venderGRPNumber;
	}
	public void setVenderGRPNumber(String venderGRPNumber) {
		this.venderGRPNumber = venderGRPNumber;
	}
	public String getVendorStatus() {
		return vendorStatus;
	}
	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getLicenseSource() {
		return licenseSource;
	}
	public void setLicenseSource(String licenseSource) {
		this.licenseSource = licenseSource;
	}

	
	
	


}
