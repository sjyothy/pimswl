package com.avanza.pims.report.criteria;

import java.util.Date;

public class ApplicationDetailsReportCriteria extends AbstractReportCriteria 
{
	private String applicationNumber;
	private String applicationStatus;
	private String applicantName;
	private String applicationType;
	private Date applicationDateFrom;
	private Date applicationDateTo;
	private String propertyName;
	private String propertyOwnershipType;
	private String unitNumber;
	private String unitType;
	private String contractNumber;
	private String contractStatus;
	private String tenantName;
	private String tenantType;
	
	
	public ApplicationDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
		
	}


	public String getApplicationNumber() {
		return applicationNumber;
	}


	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}


	public String getApplicationStatus() {
		return applicationStatus;
	}


	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}


	public String getApplicantName() {
		return applicantName;
	}


	public void setApplicantName(String applicationName) {
		this.applicantName = applicationName;
	}


	public String getApplicationType() {
		return applicationType;
	}


	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}


	

	public Date getApplicationDateFrom() {
		return applicationDateFrom;
	}


	public void setApplicationDateFrom(Date applicationDateFrom) {
		this.applicationDateFrom = applicationDateFrom;
	}


	public Date getApplicationDateTo() {
		return applicationDateTo;
	}


	public void setApplicationDateTo(Date applicationDateTo) {
		this.applicationDateTo = applicationDateTo;
	}


	public String getPropertyName() {
		return propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}


	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}


	public String getUnitNumber() {
		return unitNumber;
	}


	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}


	public String getUnitType() {
		return unitType;
	}


	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getContractStatus() {
		return contractStatus;
	}


	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}


	public String getTenantName() {
		return tenantName;
	}


	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}


	public String getTenantType() {
		return tenantType;
	}


	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

}
