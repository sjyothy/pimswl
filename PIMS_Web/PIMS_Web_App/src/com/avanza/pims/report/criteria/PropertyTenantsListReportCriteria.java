package com.avanza.pims.report.criteria;

import java.util.Date;

public class PropertyTenantsListReportCriteria extends AbstractReportCriteria  
{	
	private String txtPropertNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String cboEmirate;
	private String cboUnittype;
	private String txtTenantName; 
	private String txtContractNumber;
	private Date clndrContractStartDateFrom;
	private Date clndrContractStartDateTo;
	private Date clndrContractEndDateFrom;
	private Date clndrContractEndDateTo;
	private String cboCommunity;
	private String txtCostCenter;
	private String txtUnitNumber;
	private String cboContractStatus;
	
	public String getCboCommunity() {
		return cboCommunity;
	}

	public void setCboCommunity(String cboCommunity) {
		this.cboCommunity = cboCommunity;
	}

	public String getTxtCostCenter() {
		return txtCostCenter;
	}

	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}

	public PropertyTenantsListReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getTxtPropertNumber() {
		return txtPropertNumber;
	}

	public void setTxtPropertNumber(String txtPropertNumber) {
		this.txtPropertNumber = txtPropertNumber;
	}

	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getCboEmirate() {
		return cboEmirate;
	}

	public void setCboEmirate(String cboEmirate) {
		this.cboEmirate = cboEmirate;
	}

	public String getCboUnittype() {
		return cboUnittype;
	}

	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}

	public Date getClndrContractStartDateFrom() {
		return clndrContractStartDateFrom;
	}

	public void setClndrContractStartDateFrom(Date clndrContractStartDateFrom) {
		this.clndrContractStartDateFrom = clndrContractStartDateFrom;
	}

	public Date getClndrContractStartDateTo() {
		return clndrContractStartDateTo;
	}

	public void setClndrContractStartDateTo(Date clndrContractStartDateTo) {
		this.clndrContractStartDateTo = clndrContractStartDateTo;
	}

	

	public Date getClndrContractEndDateFrom() {
		return clndrContractEndDateFrom;
	}

	public void setClndrContractEndDateFrom(Date clndrContractEndDateFrom) {
		this.clndrContractEndDateFrom = clndrContractEndDateFrom;
	}

	public Date getClndrContractEndDateTo() {
		return clndrContractEndDateTo;
	}

	public void setClndrContractEndDateTo(Date clndrContractEndDateTo) {
		this.clndrContractEndDateTo = clndrContractEndDateTo;
	}

	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public String getCboContractStatus() {
		return cboContractStatus;
	}

	public void setCboContractStatus(String cboContractStatus) {
		this.cboContractStatus = cboContractStatus;
	}

	}
