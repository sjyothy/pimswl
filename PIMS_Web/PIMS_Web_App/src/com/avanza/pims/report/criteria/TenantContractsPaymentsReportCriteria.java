package com.avanza.pims.report.criteria;

import java.util.Date;



public class TenantContractsPaymentsReportCriteria extends AbstractReportCriteria
{
	
	private String tenantId;
	private String tenantName;
	private String tenantType;
	private String propertyName;
	private String ownershipType;
	private String unitNumber;
	private String contractNumber;
	private String contractStatus;
	private Date contractStartDateFrom;
	private Date contractStartDateTo;
	private Date contactExpiryDateFrom;
	private Date contactExpiryDateTo;
	private String txtCostCenter;
	
	public TenantContractsPaymentsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public Date getContractStartDateFrom() {
		return contractStartDateFrom;
	}
	public void setContractStartDateFrom(Date contractStartDateFrom) {
		this.contractStartDateFrom = contractStartDateFrom;
	}
	public Date getContractStartDateTo() {
		return contractStartDateTo;
	}
	public void setContractStartDateTo(Date contractStartDateTo) {
		this.contractStartDateTo = contractStartDateTo;
	}
	public Date getContactExpiryDateFrom() {
		return contactExpiryDateFrom;
	}
	public void setContactExpiryDateFrom(Date contactExpiryDateFrom) {
		this.contactExpiryDateFrom = contactExpiryDateFrom;
	}
	public Date getContactExpiryDateTo() {
		return contactExpiryDateTo;
	}
	public void setContactExpiryDateTo(Date contactExpiryDateTo) {
		this.contactExpiryDateTo = contactExpiryDateTo;
	}
	public String getTenantType() {
		return tenantType;
	}
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	public String getTxtCostCenter() {
		return txtCostCenter;
	}
	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}
	

}
