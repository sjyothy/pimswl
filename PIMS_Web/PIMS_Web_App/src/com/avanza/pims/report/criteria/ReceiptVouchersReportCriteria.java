package com.avanza.pims.report.criteria;

import java.util.Date;

public class ReceiptVouchersReportCriteria extends AbstractReportCriteria 
{

	private String tenantName;
	private String tenantId;
	private String tenantType;
	private String unitNumber;
	private String contractNumber;
	private Date contractStartDateFrom;
	private Date contractStartDateTo;
	private Date expiryDateFrom;
	private Date expiryDateTo;
	private String propertyName;
	private String propertyOwnershipType;
	private String paymentNumber;
	private String paymentType;
	private Date fromPaymentDueDate;
	private Date toPaymentDueDate;
	private String paymentMethod;
	private String receiptNumber;
	private Date receiptDateFrom;
	private Date receiptDateTo;
	private String chequeNumber;
	private String chequeAmount;
	private String chequeType;
	private String bankName;
	private String chequeOwnerName;
	
	public ReceiptVouchersReportCriteria(String reportFileName,	String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}

	public void setPropertyOwnershipType(String propertyOnershipType) {
		this.propertyOwnershipType = propertyOnershipType;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	

	public Date getContractStartDateFrom() {
		return contractStartDateFrom;
	}

	public void setContractStartDateFrom(Date contractStartDateFrom) {
		this.contractStartDateFrom = contractStartDateFrom;
	}

	public Date getContractStartDateTo() {
		return contractStartDateTo;
	}

	public void setContractStartDateTo(Date contractStartDateTo) {
		this.contractStartDateTo = contractStartDateTo;
	}

	public Date getExpiryDateFrom() {
		return expiryDateFrom;
	}

	public void setExpiryDateFrom(Date expiryDateFrom) {
		this.expiryDateFrom = expiryDateFrom;
	}

	public Date getExpiryDateTo() {
		return expiryDateTo;
	}

	public void setExpiryDateTo(Date expiryDateTo) {
		this.expiryDateTo = expiryDateTo;
	}

	public Date getFromPaymentDueDate() {
		return fromPaymentDueDate;
	}

	public void setFromPaymentDueDate(Date fromPaymentDueDate) {
		this.fromPaymentDueDate = fromPaymentDueDate;
	}

	public Date getToPaymentDueDate() {
		return toPaymentDueDate;
	}

	public void setToPaymentDueDate(Date toPaymentDueDate) {
		this.toPaymentDueDate = toPaymentDueDate;
	}

	public Date getReceiptDateFrom() {
		return receiptDateFrom;
	}

	public void setReceiptDateFrom(Date receiptDateFrom) {
		this.receiptDateFrom = receiptDateFrom;
	}

	public Date getReceiptDateTo() {
		return receiptDateTo;
	}

	public void setReceiptDateTo(Date receiptDateTo) {
		this.receiptDateTo = receiptDateTo;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public String getChequeType() {
		return chequeType;
	}

	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeOwnerName() {
		return chequeOwnerName;
	}

	public void setChequeOwnerName(String chequeOwnerName) {
		this.chequeOwnerName = chequeOwnerName;
	}
	

}
