package com.avanza.pims.report.criteria;

public class ContractDisclosureReportCriteria extends AbstractReportCriteria {

	private String contractId;
	
	public ContractDisclosureReportCriteria(String reportFileName, String reportProcessorClassName) {
		super(reportFileName, reportProcessorClassName);
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	
}
