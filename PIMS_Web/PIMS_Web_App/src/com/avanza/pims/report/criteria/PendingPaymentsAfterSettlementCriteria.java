package com.avanza.pims.report.criteria;

import java.util.Date;

public class PendingPaymentsAfterSettlementCriteria extends AbstractReportCriteria {
	
	private String CONTRACT_ID;
	private String CONTRACT_NUMBER;
	private String SETTLEMENT_DATE;
	private String CONTRACT_START_DATE;
	private String CONTRACT_END_DATE;

	private String PAYMENT_TYPE;
	private String PAYMENT_TYPE_ID;

	private String PAYMENT_SCHEDULE_ID;
	private String PAYMENT_NUMBER;
	private String AMOUNT;
	private String AMOUNT_FROM;
	private String AMOUNT_TO;
	
	private String RENT_EXT_DUR;
	private String PAYMENT_DUE_ON;

	private String REQUEST_ID;
	private String REQUEST_DATE;
	private String REQUEST_NUMBER;

	private String REQUEST_TYPE;

	private String GRP_CUSTOMER_NO;

	private String TENANT_NAME;

	private String OWNERSHIP_TYPE_ID;
	private String OWNERSHIP_TYPE;
	private String PROPERTY_NAME;

	private String UNIT_ACC_NO;
	private String UNIT_DESC;
	
	private Date SETTLEMENT_DATE_FROM;
	private Date SETTLEMENT_DATE_TO;
	
	private Date CONTRACT_START_DATE_FROM;
	private Date CONTRACT_START_DATE_TO;
	
	private Date CONTRACT_END_DATE_FROM;
	private Date CONTRACT_END_DATE_TO;
	
	private Date PAYMENT_DUE_ON_FROM;
	private Date PAYMENT_DUE_ON_TO;
	
	private Date REQUEST_DATE_FROM;
	private Date REQUEST_DATE_TO;


	public PendingPaymentsAfterSettlementCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}


	public String getCONTRACT_ID() {
		return CONTRACT_ID;
	}


	public void setCONTRACT_ID(String contract_id) {
		CONTRACT_ID = contract_id;
	}


	public String getCONTRACT_NUMBER() {
		return CONTRACT_NUMBER;
	}


	public void setCONTRACT_NUMBER(String contract_number) {
		CONTRACT_NUMBER = contract_number;
	}


	public String getSETTLEMENT_DATE() {
		return SETTLEMENT_DATE;
	}


	public void setSETTLEMENT_DATE(String settlement_date) {
		SETTLEMENT_DATE = settlement_date;
	}


	public String getCONTRACT_START_DATE() {
		return CONTRACT_START_DATE;
	}


	public void setCONTRACT_START_DATE(String contract_start_date) {
		CONTRACT_START_DATE = contract_start_date;
	}


	public String getCONTRACT_END_DATE() {
		return CONTRACT_END_DATE;
	}


	public void setCONTRACT_END_DATE(String contract_end_date) {
		CONTRACT_END_DATE = contract_end_date;
	}


	public String getPAYMENT_TYPE() {
		return PAYMENT_TYPE;
	}


	public void setPAYMENT_TYPE(String payment_type) {
		PAYMENT_TYPE = payment_type;
	}


	public String getPAYMENT_SCHEDULE_ID() {
		return PAYMENT_SCHEDULE_ID;
	}


	public void setPAYMENT_SCHEDULE_ID(String payment_schedule_id) {
		PAYMENT_SCHEDULE_ID = payment_schedule_id;
	}


	public String getPAYMENT_NUMBER() {
		return PAYMENT_NUMBER;
	}


	public void setPAYMENT_NUMBER(String payment_number) {
		PAYMENT_NUMBER = payment_number;
	}


	public String getAMOUNT() {
		return AMOUNT;
	}


	public void setAMOUNT(String amount) {
		AMOUNT = amount;
	}


	public String getRENT_EXT_DUR() {
		return RENT_EXT_DUR;
	}


	public void setRENT_EXT_DUR(String rent_ext_dur) {
		RENT_EXT_DUR = rent_ext_dur;
	}


	public String getPAYMENT_DUE_ON() {
		return PAYMENT_DUE_ON;
	}


	public void setPAYMENT_DUE_ON(String payment_due_on) {
		PAYMENT_DUE_ON = payment_due_on;
	}


	public String getREQUEST_ID() {
		return REQUEST_ID;
	}


	public void setREQUEST_ID(String request_id) {
		REQUEST_ID = request_id;
	}


	public String getREQUEST_DATE() {
		return REQUEST_DATE;
	}


	public void setREQUEST_DATE(String request_date) {
		REQUEST_DATE = request_date;
	}


	public String getREQUEST_NUMBER() {
		return REQUEST_NUMBER;
	}


	public void setREQUEST_NUMBER(String request_number) {
		REQUEST_NUMBER = request_number;
	}


	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}


	public void setREQUEST_TYPE(String request_type) {
		REQUEST_TYPE = request_type;
	}


	public String getGRP_CUSTOMER_NO() {
		return GRP_CUSTOMER_NO;
	}


	public void setGRP_CUSTOMER_NO(String grp_customer_no) {
		GRP_CUSTOMER_NO = grp_customer_no;
	}


	public String getTENANT_NAME() {
		return TENANT_NAME;
	}


	public void setTENANT_NAME(String tenant_name) {
		TENANT_NAME = tenant_name;
	}


	public String getOWNERSHIP_TYPE_ID() {
		return OWNERSHIP_TYPE_ID;
	}


	public void setOWNERSHIP_TYPE_ID(String ownership_type_id) {
		OWNERSHIP_TYPE_ID = ownership_type_id;
	}


	public String getOWNERSHIP_TYPE() {
		return OWNERSHIP_TYPE;
	}


	public void setOWNERSHIP_TYPE(String ownership_type) {
		OWNERSHIP_TYPE = ownership_type;
	}


	public String getPROPERTY_NAME() {
		return PROPERTY_NAME;
	}


	public void setPROPERTY_NAME(String property_name) {
		PROPERTY_NAME = property_name;
	}


	public String getUNIT_ACC_NO() {
		return UNIT_ACC_NO;
	}


	public void setUNIT_ACC_NO(String unit_acc_no) {
		UNIT_ACC_NO = unit_acc_no;
	}


	public String getUNIT_DESC() {
		return UNIT_DESC;
	}


	public void setUNIT_DESC(String unit_desc) {
		UNIT_DESC = unit_desc;
	}


	public Date getSETTLEMENT_DATE_FROM() {
		return SETTLEMENT_DATE_FROM;
	}


	public void setSETTLEMENT_DATE_FROM(Date settlement_date_from) {
		SETTLEMENT_DATE_FROM = settlement_date_from;
	}


	public Date getSETTLEMENT_DATE_TO() {
		return SETTLEMENT_DATE_TO;
	}


	public void setSETTLEMENT_DATE_TO(Date settlement_date_to) {
		SETTLEMENT_DATE_TO = settlement_date_to;
	}


	public Date getCONTRACT_START_DATE_FROM() {
		return CONTRACT_START_DATE_FROM;
	}


	public void setCONTRACT_START_DATE_FROM(Date contract_start_date_from) {
		CONTRACT_START_DATE_FROM = contract_start_date_from;
	}


	public Date getCONTRACT_START_DATE_TO() {
		return CONTRACT_START_DATE_TO;
	}


	public void setCONTRACT_START_DATE_TO(Date contract_start_date_to) {
		CONTRACT_START_DATE_TO = contract_start_date_to;
	}


	public Date getCONTRACT_END_DATE_FROM() {
		return CONTRACT_END_DATE_FROM;
	}


	public void setCONTRACT_END_DATE_FROM(Date contract_end_date_from) {
		CONTRACT_END_DATE_FROM = contract_end_date_from;
	}


	public Date getCONTRACT_END_DATE_TO() {
		return CONTRACT_END_DATE_TO;
	}


	public void setCONTRACT_END_DATE_TO(Date contract_end_date_to) {
		CONTRACT_END_DATE_TO = contract_end_date_to;
	}


	public Date getPAYMENT_DUE_ON_FROM() {
		return PAYMENT_DUE_ON_FROM;
	}


	public void setPAYMENT_DUE_ON_FROM(Date payment_due_on_from) {
		PAYMENT_DUE_ON_FROM = payment_due_on_from;
	}


	public Date getPAYMENT_DUE_ON_TO() {
		return PAYMENT_DUE_ON_TO;
	}


	public void setPAYMENT_DUE_ON_TO(Date payment_due_on_to) {
		PAYMENT_DUE_ON_TO = payment_due_on_to;
	}


	public Date getREQUEST_DATE_FROM() {
		return REQUEST_DATE_FROM;
	}


	public void setREQUEST_DATE_FROM(Date request_date_from) {
		REQUEST_DATE_FROM = request_date_from;
	}


	public Date getREQUEST_DATE_TO() {
		return REQUEST_DATE_TO;
	}


	public void setREQUEST_DATE_TO(Date request_date_to) {
		REQUEST_DATE_TO = request_date_to;
	}


	public String getPAYMENT_TYPE_ID() {
		return PAYMENT_TYPE_ID;
	}


	public void setPAYMENT_TYPE_ID(String payment_type_id) {
		PAYMENT_TYPE_ID = payment_type_id;
	}


	public String getAMOUNT_FROM() {
		return AMOUNT_FROM;
	}


	public void setAMOUNT_FROM(String amount_from) {
		AMOUNT_FROM = amount_from;
	}


	public String getAMOUNT_TO() {
		return AMOUNT_TO;
	}


	public void setAMOUNT_TO(String amount_to) {
		AMOUNT_TO = amount_to;
	}



}
