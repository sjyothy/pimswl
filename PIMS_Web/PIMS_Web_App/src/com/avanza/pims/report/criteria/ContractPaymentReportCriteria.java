package com.avanza.pims.report.criteria;

public class ContractPaymentReportCriteria  extends AbstractReportCriteria {

	public ContractPaymentReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractId;
	private String paymentScheduleId;
	private String receiptNumber;
	private String reportProcedureNameEn;
	private String reportProcedureNameAr;
	private String requestId;
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReportProcedureNameEn() {
		return reportProcedureNameEn;
	}
	public void setReportProcedureNameEn(String reportProcedureNameEn) {
		this.reportProcedureNameEn = reportProcedureNameEn;
	}
	public String getReportProcedureNameAr() {
		return reportProcedureNameAr;
	}
	public void setReportProcedureNameAr(String reportProcedureNameAr) {
		this.reportProcedureNameAr = reportProcedureNameAr;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	
}
