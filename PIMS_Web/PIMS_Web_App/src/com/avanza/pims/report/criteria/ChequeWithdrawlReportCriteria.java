package com.avanza.pims.report.criteria;

public class ChequeWithdrawlReportCriteria  extends AbstractReportCriteria {

	public ChequeWithdrawlReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String paymentScheduleIds;
	private String inCriteria;
	

	public String getPaymentScheduleIds() {
		return paymentScheduleIds;
	}
	public void setPaymentScheduleIds(String paymentScheduleIds) {
		this.paymentScheduleIds = paymentScheduleIds;
	}
	public String getInCriteria() {
		return inCriteria;
	}
	public void setInCriteria(String inCriteria) {
		this.inCriteria = inCriteria;
	}
}
