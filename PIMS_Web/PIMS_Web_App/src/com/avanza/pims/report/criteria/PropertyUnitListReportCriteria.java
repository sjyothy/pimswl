package com.avanza.pims.report.criteria;

import java.util.Date;

public class PropertyUnitListReportCriteria extends AbstractReportCriteria  
{	
	private String txtPropertNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String txtLandNumber;
	private String cboEmirate;
	private String txtUnitNumber;
	private String cboUnittype;
	private String txtFloorNumber;
	private String txtTenantName; 
	private Date clndrFromDate;
	private Date cldrToDate;
	private String cboUnitStatus;
	private String txtCostCenter;
	private String unitRentAmountFrom;
	private String unitRentAmountTo;
	private String contractRentAmountFrom;
	private String contractRentAmountTo;
	private String txtContractNumber;
	private String txtOwnerShip;
	private String cboCommunity;
	
	public PropertyUnitListReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getTxtPropertNumber() {
		return txtPropertNumber;
	}

	public void setTxtPropertNumber(String txtPropertNumber) {
		this.txtPropertNumber = txtPropertNumber;
	}

	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public String getCboEmirate() {
		return cboEmirate;
	}

	public void setCboEmirate(String cboEmirate) {
		this.cboEmirate = cboEmirate;
	}

	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public String getCboUnittype() {
		return cboUnittype;
	}

	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}

	public String getTxtFloorNumber() {
		return txtFloorNumber;
	}

	public void setTxtFloorNumber(String txtFloorNumber) {
		this.txtFloorNumber = txtFloorNumber;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public Date getClndrFromDate() {
		return clndrFromDate;
	}

	public void setClndrFromDate(Date clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}

	public Date getCldrToDate() {
		return cldrToDate;
	}

	public void setCldrToDate(Date cldrToDate) {
		this.cldrToDate = cldrToDate;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}

	public String getCboUnitStatus() {
		return cboUnitStatus;
	}

	public void setCboUnitStatus(String cboUnitStatus) {
		this.cboUnitStatus = cboUnitStatus;
	}

	public String getTxtCostCenter() {
		return txtCostCenter;
	}

	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}

	public String getUnitRentAmountFrom() {
		return unitRentAmountFrom;
	}

	public void setUnitRentAmountFrom(String unitRentAmountFrom) {
		this.unitRentAmountFrom = unitRentAmountFrom;
	}

	public String getUnitRentAmountTo() {
		return unitRentAmountTo;
	}

	public void setUnitRentAmountTo(String unitRentAmountTo) {
		this.unitRentAmountTo = unitRentAmountTo;
	}

	public String getContractRentAmountFrom() {
		return contractRentAmountFrom;
	}

	public void setContractRentAmountFrom(String contractRentAmountFrom) {
		this.contractRentAmountFrom = contractRentAmountFrom;
	}

	public String getContractRentAmountTo() {
		return contractRentAmountTo;
	}

	public void setContractRentAmountTo(String contractRentAmountTo) {
		this.contractRentAmountTo = contractRentAmountTo;
	}

	public String getTxtOwnerShip() {
		return txtOwnerShip;
	}

	public void setTxtOwnerShip(String txtOwnerShip) {
		this.txtOwnerShip = txtOwnerShip;
	}

	public String getCboCommunity() {
		return cboCommunity;
	}

	public void setCboCommunity(String cboCommunity) {
		this.cboCommunity = cboCommunity;
	}
	
}
