package com.avanza.pims.report.criteria;

import java.util.Date;

public class TenantDetailsReportCriteria extends AbstractReportCriteria
{
	private String tenantId;
	private String tenantName;
	private String tenantNo;
	private String nationality;
	private String tenantStatus;
	private String maritalStatus;
	private String tenantType;
	private String nationalId;
	private String passportNumber;
	private String visaNumber;
	private String licenseNumber;
	private String licenseSource;
	private String mobileNumber;
	private String phoneNumber;
	private String propertyName;
	private String propertyOwnershipType;
	private Date contractStartDateFrom;
	private Date contractStartDateTo;
	private Date expiryDateFrom;
	private Date expiryDateTo;
	private String contractNumber;
	public TenantDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getTenantStatus() {
		return tenantStatus;
	}
	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}
	public String getTenantType() {
		return tenantType;
	}
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getLicenseSource() {
		return licenseSource;
	}
	public void setLicenseSource(String licenseSource) {
		this.licenseSource = licenseSource;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}
	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public Date getContractStartDateFrom() {
		return contractStartDateFrom;
	}
	public void setContractStartDateFrom(Date contractStartDateFrom) {
		this.contractStartDateFrom = contractStartDateFrom;
	}
	public Date getContractStartDateTo() {
		return contractStartDateTo;
	}
	public void setContractStartDateTo(Date contractStartDateTo) {
		this.contractStartDateTo = contractStartDateTo;
	}
	public Date getExpiryDateFrom() {
		return expiryDateFrom;
	}
	public void setExpiryDateFrom(Date expiryDateFrom) {
		this.expiryDateFrom = expiryDateFrom;
	}
	public Date getExpiryDateTo() {
		return expiryDateTo;
	}
	public void setExpiryDateTo(Date expiryDateTo) {
		this.expiryDateTo = expiryDateTo;
	}
	public String getTenantNo() {
		return tenantNo;
	}
	public void setTenantNo(String tenantNo) {
		this.tenantNo = tenantNo;
	}
	

}
