package com.avanza.pims.report.criteria;


public class ClearenceLetterCriteria extends AbstractReportCriteria
{
	private String requestId;
	public ClearenceLetterCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	
	
	
}
