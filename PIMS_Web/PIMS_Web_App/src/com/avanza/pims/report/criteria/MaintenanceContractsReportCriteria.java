package com.avanza.pims.report.criteria;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

public class MaintenanceContractsReportCriteria  extends AbstractReportCriteria {

	public MaintenanceContractsReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractNumber;
	private String workType ;
	private Date fromContractDateRich;
	private Date toContractDateRich;
	private String propertyName;
	private String contractorName;
	private String contractStatus;
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public Date getFromContractDateRich() {
		return fromContractDateRich;
	}
	public void setFromContractDateRich(Date fromContractDateRich) {
		this.fromContractDateRich = fromContractDateRich;
	}
	public Date getToContractDateRich() {
		return toContractDateRich;
	}
	public void setToContractDateRich(Date toContractDateRich) {
		this.toContractDateRich = toContractDateRich;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getContractorName() {
		return contractorName;
	}
	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	
	

}
