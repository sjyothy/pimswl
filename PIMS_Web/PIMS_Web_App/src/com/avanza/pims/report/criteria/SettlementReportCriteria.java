 package com.avanza.pims.report.criteria;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.avanza.pims.ws.vo.PaymentBean;
import com.avanza.pims.ws.vo.PaymentScheduleView;

public class SettlementReportCriteria extends AbstractReportCriteria
{
	private String contractStartDate;
	private String contractEndDate;
	private Date evacuationDate;
	private String evacuationDateString;
	private Integer months;
	private Integer days;
	private Double contractRentalAmount;
	private Double actualRentalAmount;
	private Double totalDepositAmount;
	private Double totalRealizedAmount;
	private Double totalPaidAmount;
	private Double totalUnrealizedAmount;
	private Double totalEvacFines;
	private Double otherUnpaidfines;
	private Double totalUnpaidAmount;
	private Double totalRefundAmount;
	private Double totalDueAmount;
	private Double creditMemo;
	private Long requestId;
	private List<PaymentBean> rentPayments = new ArrayList<PaymentBean>();
	private List<PaymentBean> otherPayments = new ArrayList<PaymentBean>();
	private List<PaymentBean> chequesToBeReturned = new ArrayList<PaymentBean>();
	private List<PaymentBean> allPayments = new ArrayList<PaymentBean>();
	
	private String contractNumber;
	private String tenantName;
	private String costCenter;
	private String grpCustomerNumber;
	
	
	
	
	
	public SettlementReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}


	public Date getEvacuationDate() {
		return evacuationDate;
	}





	public void setEvacuationDate(Date evacuationDate) {
		this.evacuationDate = evacuationDate;
	}





	public String getEvacuationDateString() {
		return evacuationDateString;
	}





	public void setEvacuationDateString(String evacuationDateString) {
		this.evacuationDateString = evacuationDateString;
	}





	public Integer getMonths() {
		return months;
	}





	public void setMonths(Integer months) {
		this.months = months;
	}





	public Integer getDays() {
		return days;
	}





	public void setDays(Integer days) {
		this.days = days;
	}





	public Double getActualRentalAmount() {
		return actualRentalAmount;
	}





	public void setActualRentalAmount(Double actualRentalAmount) {
		this.actualRentalAmount = actualRentalAmount;
	}





	public Double getTotalDepositAmount() {
		return totalDepositAmount;
	}





	public void setTotalDepositAmount(Double totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}





	public Double getTotalRealizedAmount() {
		return totalRealizedAmount;
	}





	public void setTotalRealizedAmount(Double totalRealizedAmount) {
		this.totalRealizedAmount = totalRealizedAmount;
	}





	public Double getTotalPaidAmount() {
		return totalPaidAmount;
	}





	public void setTotalPaidAmount(Double totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}





	public Double getTotalUnrealizedAmount() {
		return totalUnrealizedAmount;
	}





	public void setTotalUnrealizedAmount(Double totalUnrealizedAmount) {
		this.totalUnrealizedAmount = totalUnrealizedAmount;
	}





	public Double getTotalEvacFines() {
		return totalEvacFines;
	}





	public void setTotalEvacFines(Double totalEvacFines) {
		this.totalEvacFines = totalEvacFines;
	}





	public Double getOtherUnpaidfines() {
		return otherUnpaidfines;
	}





	public void setOtherUnpaidfines(Double otherUnpaidfines) {
		this.otherUnpaidfines = otherUnpaidfines;
	}





	public Double getTotalUnpaidAmount() {
		return totalUnpaidAmount;
	}





	public void setTotalUnpaidAmount(Double totalUnpaidAmount) {
		this.totalUnpaidAmount = totalUnpaidAmount;
	}




	public Double getTotalRefundAmount() {
		return totalRefundAmount;
	}





	public void setTotalRefundAmount(Double totalRefundAmount) {
		this.totalRefundAmount = totalRefundAmount;
	}





	public Double getTotalDueAmount() {
		return totalDueAmount;
	}





	public void setTotalDueAmount(Double totalDueAmount) {
		this.totalDueAmount = totalDueAmount;
	}


	public Double getContractRentalAmount() {
		return contractRentalAmount;
	}


	public void setContractRentalAmount(Double contractRentalAmount) {
		this.contractRentalAmount = contractRentalAmount;
	}


	public String getContractStartDate() {
		return contractStartDate;
	}


	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}


	public String getContractEndDate() {
		return contractEndDate;
	}


	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}


	public List<PaymentBean> getRentPayments() {
		return rentPayments;
	}


	public void setRentPayments(List<PaymentBean> rentPayments) {
		this.rentPayments = rentPayments;
	}


	public List<PaymentBean> getOtherPayments() {
		return otherPayments;
	}


	public void setOtherPayments(List<PaymentBean> otherPayments) {
		this.otherPayments = otherPayments;
	}


	public List<PaymentBean> getChequesToBeReturned() {
		return chequesToBeReturned;
	}


	public void setChequesToBeReturned(List<PaymentBean> chequesToBeReturned) {
		this.chequesToBeReturned = chequesToBeReturned;
	}


	public List<PaymentBean> getAllPayments() {
		return allPayments;
	}


	public void setAllPayments(List<PaymentBean> allPayments) {
		this.allPayments = allPayments;
	}


	public Long getRequestId() {
		return requestId;
	}


	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getTenantName() {
		return tenantName;
	}


	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}


	public String getCostCenter() {
		return costCenter;
	}


	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}


	public String getGrpCustomerNumber() {
		return grpCustomerNumber;
	}


	public void setGrpCustomerNumber(String grpCustomerNumber) {
		this.grpCustomerNumber = grpCustomerNumber;
	}


	public Double getCreditMemo() {
		return creditMemo;
	}


	public void setCreditMemo(Double creditMemo) {
		this.creditMemo = creditMemo;
	}
}
