package com.avanza.pims.report.criteria;

import java.util.Date;



public class ChequeDetailsListReportCriteria extends AbstractReportCriteria  
{	
	
	private String tenantId;
	private String tenantName;
	private String tenantStatus;
	private String tenantType;
	private String propertyName;
	private String propertyOwnershipType;
	private String unitNumber;
	private Date contractStartDateFrom;
	private Date contractStartDateTo;
	private Date expiryDateFrom;
	private Date expiryDateTo;
	private String contractNumber;
	private String paymentType;
	private String paymentNumber;
	private String receiptNumber;
	private Date receiptDate;
	private String chequeNumber;
	private Date dueDate;
	private Date dueDateTo;
	private String chequeAmount;
	private String bankName;
	private String chequeType;
	private String chequeOwnerName;
	private String paymentStatus;
	
	public ChequeDetailsListReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantStatus() {
		return tenantStatus;
	}

	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}

	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}


	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	

	public String getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeType() {
		return chequeType;
	}

	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}

	public String getChequeOwnerName() {
		return chequeOwnerName;
	}

	public void setChequeOwnerName(String chequeOwnerName) {
		this.chequeOwnerName = chequeOwnerName;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getContractStartDateFrom() {
		return contractStartDateFrom;
	}

	public void setContractStartDateFrom(Date contractStartDateFrom) {
		this.contractStartDateFrom = contractStartDateFrom;
	}

	public Date getContractStartDateTo() {
		return contractStartDateTo;
	}

	public void setContractStartDateTo(Date contractStartDateTo) {
		this.contractStartDateTo = contractStartDateTo;
	}

	public Date getExpiryDateFrom() {
		return expiryDateFrom;
	}

	public void setExpiryDateFrom(Date expiryDateFrom) {
		this.expiryDateFrom = expiryDateFrom;
	}

	public Date getExpiryDateTo() {
		return expiryDateTo;
	}

	public void setExpiryDateTo(Date expiryDateTo) {
		this.expiryDateTo = expiryDateTo;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getDueDateTo() {
		return dueDateTo;
	}

	public void setDueDateTo(Date dueDateTo) {
		this.dueDateTo = dueDateTo;
	}
	
}
