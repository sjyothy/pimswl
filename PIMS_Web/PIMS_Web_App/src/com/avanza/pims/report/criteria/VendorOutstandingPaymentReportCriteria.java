package com.avanza.pims.report.criteria;

import java.util.Date;

public class VendorOutstandingPaymentReportCriteria  extends AbstractReportCriteria {

	public VendorOutstandingPaymentReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}
	private String paymentNumber;
	private String paymentStatus;
	private String paymentType;
	private String paymentAmount;
	private Date paymentDateFrom;
	private Date paymentDateTo;
	private String vendorNumber;
	private String vendorName;
	private String contractNumber; 
	private String contractType;
	
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public Date getPaymentDateFrom() {
		return paymentDateFrom;
	}
	public void setPaymentDateFrom(Date paymentDateFrom) {
		this.paymentDateFrom = paymentDateFrom;
	}
	public Date getPaymentDateTo() {
		return paymentDateTo;
	}
	public void setPaymentDateTo(Date paymentDateTo) {
		this.paymentDateTo = paymentDateTo;
	}
	public String getVendorNumber() {
		return vendorNumber;
	}
	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	

	

}
