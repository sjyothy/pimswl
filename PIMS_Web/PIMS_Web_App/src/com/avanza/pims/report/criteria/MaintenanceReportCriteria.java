package com.avanza.pims.report.criteria;

import java.util.Date;

public class MaintenanceReportCriteria extends AbstractReportCriteria {

	private String contractId;
	private String contractNumber;
	private Date createdOnFrom;
	private Date createdOnTo;
	private String requestNumber;
	private String serviceProvider;
	private String unitNumber;
	private String propertyNumber;
	private String propertyName;
	private String unitType;
	private String contractorName;
	private String contractStatus;
    private String workTypeItems;
    
	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getWorkTypeItems() {
		return workTypeItems;
	}

	public void setWorkTypeItems(String workTypeItems) {
		this.workTypeItems = workTypeItems;
	}

	public String getContractorName() {
		return contractorName;
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}

	public MaintenanceReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreatedOnFrom() {
		return createdOnFrom;
	}

	public void setCreatedOnFrom(Date createdOnFrom) {
		this.createdOnFrom = createdOnFrom;
	}

	public Date getCreatedOnTo() {
		return createdOnTo;
	}

	public void setCreatedOnTo(Date createdOnTo) {
		this.createdOnTo = createdOnTo;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getPropertyNumber() {
		return propertyNumber;
	}

	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public MaintenanceReportCriteria(String reportFileName, String reportProcessorClassName) {
		super(reportFileName, reportProcessorClassName);
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	
}
