package com.avanza.pims.report.criteria;

import java.util.Date;


public class RequestTaskActivityCriteria extends AbstractReportCriteria
{
	
	private String requestNumber;
	private String requestTypeId;
	private Date fromDate;
	private Date toDate;
	
	public RequestTaskActivityCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public void setReportName(String reportFileName) 
	{
		this.setReportFileName(reportFileName);
	}
	
	public String getRequestNumber() 
	{
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


}
