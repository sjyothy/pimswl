package com.avanza.pims.report.criteria;

import java.util.Date;

public class LeaseContractsSummaryReportCriteria extends AbstractReportCriteria{

	public LeaseContractsSummaryReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractNumber;
	private String isBlackList;
	private String tenantName;
	private String propertyComercialName;
	private String unitNum;
	private Date fromContractDateRich;
	private Date toContractDateRich;
	private Date fromContractExpDateRich;
	private Date toContractExpDateRich;	
	private String contractType;
	private String contractStatus;
	private String unitRentAmountFrom;
	private String unitRentAmountTo;
	private String contractRentAmountFrom;
	private String contractRentAmountTo;
	public String costCenter;
	public String propertyNumber;
	
	public String country;
	public String emirate;
	public String city;
	
	public String community;
	
	private String contractId;
	private String propertyId;
	private String tenantId;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getPropertyComercialName() {
		return propertyComercialName;
	}
	public void setPropertyComercialName(String propertyComercialName) {
		this.propertyComercialName = propertyComercialName;
	}
	public String getUnitNum() {
		return unitNum;
	}
	public void setUnitNum(String unitNum) {
		this.unitNum = unitNum;
	}
	public Date getFromContractDateRich() {
		return fromContractDateRich;
	}
	public void setFromContractDateRich(Date fromContractDateRich) {
		this.fromContractDateRich = fromContractDateRich;
	}
	public Date getToContractDateRich() {
		return toContractDateRich;
	}
	public void setToContractDateRich(Date toContractDateRich) {
		this.toContractDateRich = toContractDateRich;
	}
	public Date getFromContractExpDateRich() {
		return fromContractExpDateRich;
	}
	public void setFromContractExpDateRich(Date fromContractExpDateRich) {
		this.fromContractExpDateRich = fromContractExpDateRich;
	}
	public Date getToContractExpDateRich() {
		return toContractExpDateRich;
	}
	public void setToContractExpDateRich(Date toContractExpDateRich) {
		this.toContractExpDateRich = toContractExpDateRich;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getUnitRentAmountFrom() {
		return unitRentAmountFrom;
	}
	public void setUnitRentAmountFrom(String unitRentAmountFrom) {
		this.unitRentAmountFrom = unitRentAmountFrom;
	}
	public String getUnitRentAmountTo() {
		return unitRentAmountTo;
	}
	public void setUnitRentAmountTo(String unitRentAmountTo) {
		this.unitRentAmountTo = unitRentAmountTo;
	}
	public String getContractRentAmountFrom() {
		return contractRentAmountFrom;
	}
	public void setContractRentAmountFrom(String contractRentAmountFrom) {
		this.contractRentAmountFrom = contractRentAmountFrom;
	}
	public String getContractRentAmountTo() {
		return contractRentAmountTo;
	}
	public void setContractRentAmountTo(String contractRentAmountTo) {
		this.contractRentAmountTo = contractRentAmountTo;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getIsBlackList() {
		return isBlackList;
	}
	public void setIsBlackList(String isBlackList) {
		this.isBlackList = isBlackList;
	}
	
}	
