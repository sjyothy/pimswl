package com.avanza.pims.report.criteria;

import java.util.Date;



public class ContractPaymentDetailReportCriteria extends AbstractReportCriteria {

	public ContractPaymentDetailReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractNumber; 
	private String tenantName;
	private String propertyComercialName;
	private String unitNum;
	private String selectOnePaymentMethod;
	private String chequeNum;
	private String selectOnePaymentStatus;
	private String bankName;
	private Date fromContractDateRich;
	private Date toContractDateRich;
	private Date fromContractExpDateRich;
	private Date toContractExpDateRich;
	private String receiptNumber;
	private String costCenter;
	private String contractStatus;
	
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getPropertyComercialName() {
		return propertyComercialName;
	}
	public void setPropertyComercialName(String propertyComercialName) {
		this.propertyComercialName = propertyComercialName;
	}
	public String getUnitNum() {
		return unitNum;
	}
	public void setUnitNum(String unitNum) {
		this.unitNum = unitNum;
	}
	public String getSelectOnePaymentMethod() {
		return selectOnePaymentMethod;
	}
	public void setSelectOnePaymentMethod(String selectOnePaymentMethod) {
		this.selectOnePaymentMethod = selectOnePaymentMethod;
	}
	public String getChequeNum() {
		return chequeNum;
	}
	public void setChequeNum(String chequeNum) {
		this.chequeNum = chequeNum;
	}
	public String getSelectOnePaymentStatus() {
		return selectOnePaymentStatus;
	}
	public void setSelectOnePaymentStatus(String selectOnePaymentStatus) {
		this.selectOnePaymentStatus = selectOnePaymentStatus;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Date getFromContractDateRich() {
		return fromContractDateRich;
	}
	public void setFromContractDateRich(Date fromContractDateRich) {
		this.fromContractDateRich = fromContractDateRich;
	}
	public Date getToContractDateRich() {
		return toContractDateRich;
	}
	public void setToContractDateRich(Date toContractDateRich) {
		this.toContractDateRich = toContractDateRich;
	}
	public Date getFromContractExpDateRich() {
		return fromContractExpDateRich;
	}
	public void setFromContractExpDateRich(Date fromContractExpDateRich) {
		this.fromContractExpDateRich = fromContractExpDateRich;
	}
	public Date getToContractExpDateRich() {
		return toContractExpDateRich;
	}
	public void setToContractExpDateRich(Date toContractExpDateRich) {
		this.toContractExpDateRich = toContractExpDateRich;
	}
	
	

}
