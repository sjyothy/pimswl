 package com.avanza.pims.report.criteria;

import java.util.Date;

public class PropertyDetailReportCriteria extends AbstractReportCriteria
{
	private String txtPropertyNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String txtLandNumber;
	private String cboEmirate;
	private String txtUnitNumber;
	private String cboUnittype;
	private String txtFloorNumber;
	private String txtTenantName; 
	private Date clndrFromDate;
	private Date cldrToDate;
	private String txtContractNumber;
	private String unitUsage;
	private String cboCommunity;
	private String txtCostCenter;
	private String cboOwnerShipType;
	
	public String getCboOwnerShipType() {
		return cboOwnerShipType;
	}
	public void setCboOwnerShipType(String cboOwnerShipType) {
		this.cboOwnerShipType = cboOwnerShipType;
	}
	public PropertyDetailReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}
	public String getTxtPropertyNumber() {
		return txtPropertyNumber;
	}
	public void setTxtPropertyNumber(String txtPropertNumber) {
		this.txtPropertyNumber = txtPropertNumber;
	}
	public String getCboPropertyType() {
		return cboPropertyType;
	}
	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}
	public String getTxtPropertyName() {
		return txtPropertyName;
	}
	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}
	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}
	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}
	public String getTxtLandNumber() {
		return txtLandNumber;
	}
	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}
	public String getCboEmirate() {
		return cboEmirate;
	}
	public void setCboEmirate(String cboEmirate) {
		this.cboEmirate = cboEmirate;
	}
	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}
	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}
	public String getCboUnittype() {
		return cboUnittype;
	}
	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}
	public String getTxtFloorNumber() {
		return txtFloorNumber;
	}
	public void setTxtFloorNumber(String txtFloorNumber) {
		this.txtFloorNumber = txtFloorNumber;
	}
	public String getTxtTenantName() {
		return txtTenantName;
	}
	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}
	public Date getClndrFromDate() {
		return clndrFromDate;
	}
	public void setClndrFromDate(Date clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}
	public Date getCldrToDate() {
		return cldrToDate;
	}
	public void setCldrToDate(Date cldrToDate) {
		this.cldrToDate = cldrToDate;
	}
	public String getTxtContractNumber() {
		return txtContractNumber;
	}
	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}
	public String getUnitUsage() {
		return unitUsage;
	}
	public void setUnitUsage(String unitUsage) {
		this.unitUsage = unitUsage;
	}
	public String getCboCommunity() {
		return cboCommunity;
	}
	public void setCboCommunity(String cboCommunity) {
		this.cboCommunity = cboCommunity;
	}
	public String getTxtCostCenter() {
		return txtCostCenter;
	}
	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}
}
