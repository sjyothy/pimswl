package com.avanza.pims.report.criteria;

import java.util.Date;

public class FollowupReportCriteria extends AbstractReportCriteria
{

	private String followupNumber;
	private String action;
	private Date followupFrom;
	private Date followupTo;
	private String chequeNumber;
	private String bank;
	private Date paymentDateFrom;
	private Date paymentDateTo;
	private String propertyName;
	private String unitNumber;
	private String contractNumber;
	private String tenantName;
	private String status;
	
	
	public FollowupReportCriteria(String reportFileName,String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}


	public String getFollowupNumber() {
		return followupNumber;
	}


	public void setFollowupNumber(String followupNumber) {
		this.followupNumber = followupNumber;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}



	public String getChequeNumber() {
		return chequeNumber;
	}


	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}


	public String getBank() {
		return bank;
	}


	public void setBank(String bank) {
		this.bank = bank;
	}


	public String getPropertyName() {
		return propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public String getUnitNumber() {
		return unitNumber;
	}


	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getTenantName() {
		return tenantName;
	}


	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getPaymentDateFrom() {
		return paymentDateFrom;
	}


	public void setPaymentDateFrom(Date paymentDateFrom) {
		this.paymentDateFrom = paymentDateFrom;
	}


	public Date getPaymentDateTo() {
		return paymentDateTo;
	}


	public void setPaymentDateTo(Date paymentDateTo) {
		this.paymentDateTo = paymentDateTo;
	}


	public Date getFollowupFrom() {
		return followupFrom;
	}


	public void setFollowupFrom(Date followupFrom) {
		this.followupFrom = followupFrom;
	}


	public Date getFollowupTo() {
		return followupTo;
	}


	public void setFollowupTo(Date followupTo) {
		this.followupTo = followupTo;
	} 

}
