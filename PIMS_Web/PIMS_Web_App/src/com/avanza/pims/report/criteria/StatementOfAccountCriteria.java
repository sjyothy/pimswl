package com.avanza.pims.report.criteria;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class StatementOfAccountCriteria extends AbstractReportCriteria  implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String masrafBeneficiaryName;
	private Long personId;
	private Long inheritanceBeneficiaryId;
	private Long inheritancefileId;
	private Long endowmentId;
	private Long masrafId;
	private Date fromDate;
	private Date toDate;
	private String forYear;
	private String previousYear;
	private String loggedInUser;
	private String formattedFromDate;
	private String formattedToDate;
	private Date fromDatePreviousYear;
	private Date toDatePreviousYear;
	private String formattedFromDatePreviousYear;
	private String formattedToDatePreviousYear;
	private String showSubMasrafTrx;
	private String masrafName;
	private String parentMasrafName;
	private String waqifName;
	private String waqifGRPNumber;
	private String endowmentFileNumber;
	private String endowmentFileName;
	private String endowmentFileId;
	private List<String> costCenters = new ArrayList<String>();
	DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat formatterReportDisplay = new SimpleDateFormat("MMM, dd yyyy");

	public void setFormatterReportDisplay(DateFormat formatterReportDisplay) {
		this.formatterReportDisplay = formatterReportDisplay;
	}

	public String getMasrafBeneficiaryName() {
		return masrafBeneficiaryName;
	}

	public void setMasrafBeneficiaryName(String masrafBeneficiaryName) {
		this.masrafBeneficiaryName = masrafBeneficiaryName;
	}

	public String getWaqifGRPNumber() {
		return waqifGRPNumber;
	}

	public void setWaqifGRPNumber(String waqifGRPNumber) {
		this.waqifGRPNumber = waqifGRPNumber;
	}

	public String getWaqifName() {
		return waqifName;
	}

	public void setWaqifName(String waqifName) {
		this.waqifName = waqifName;
	}

	public String getEndowmentFileNumber() {
		return endowmentFileNumber;
	}

	public void setEndowmentFileNumber(String endowmentFileNumber) {
		this.endowmentFileNumber = endowmentFileNumber;
	}

	public String getEndowmentFileName() {
		return endowmentFileName;
	}

	public void setEndowmentFileName(String endowmentFileName) {
		this.endowmentFileName = endowmentFileName;
	}

	public String getEndowmentFileId() {
		return endowmentFileId;
	}

	public void setEndowmentFileId(String endowmentFileId) {
		this.endowmentFileId = endowmentFileId;
	}

	public String getForYear() {
		return forYear;
	}

	public void setForYear(String forYear) {
		this.forYear = forYear;
	}

	public String getPreviousYear() {
		return previousYear;
	}

	public void setPreviousYear(String previousYear) {
		this.previousYear = previousYear;
	}

	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public DateFormat getFormatterWithOutTime() {
		return formatterWithOutTime;
	}

	public DateFormat getFormatterReportDisplay() {
		return formatterReportDisplay;
	}

	public Date getFromDatePreviousYear() {
		return fromDatePreviousYear;
	}

	public void setFromDatePreviousYear(Date fromDatePreviousYear) {
		this.fromDatePreviousYear = fromDatePreviousYear;
		if( this.fromDatePreviousYear != null )
		{
			formattedFromDatePreviousYear = formatterWithOutTime.format(this.fromDatePreviousYear)+" 00:00:00";
		}
	}

	public Date getToDatePreviousYear() {
		return toDatePreviousYear;
	}

	public void setToDatePreviousYear(Date toDatePreviousYear) {
		this.toDatePreviousYear = toDatePreviousYear;
		if( this.toDatePreviousYear != null )
		{
			formattedToDatePreviousYear = formatterWithOutTime.format(this.toDatePreviousYear)+" 23:59:59";
		}
	}

	public String getFormattedFromDatePreviousYear() {
		return formattedFromDatePreviousYear;
	}

	public String getFormattedToDatePreviousYear() {
		return formattedToDatePreviousYear;
	}

	public String getMasrafName() {
		return masrafName;
	}

	public void setMasrafName(String masrafName) {
		this.masrafName = masrafName;
	}

	public String getParentMasrafName() {
		return parentMasrafName;
	}

	public void setParentMasrafName(String parentMasrafName) {
		this.parentMasrafName = parentMasrafName;
	}

	//For mobile webservice statement of account
	public StatementOfAccountCriteria()
	{


	}

	public StatementOfAccountCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getInheritanceBeneficiaryId() {
		return inheritanceBeneficiaryId;
	}

	public void setInheritanceBeneficiaryId(Long inheritanceBeneficiaryId) {
		this.inheritanceBeneficiaryId = inheritanceBeneficiaryId;
	}

	public Long getInheritancefileId() {
		return inheritancefileId;
	}

	public void setInheritancefileId(Long inheritancefileId) {
		this.inheritancefileId = inheritancefileId;
	}

	public List<String> getCostCenters() {
		return costCenters;
	}

	public void setCostCenters(List<String> costCenters) {
		this.costCenters = costCenters;
	}

	public Long getEndowmentId() {
		return endowmentId;
	}

	public void setEndowmentId(Long endowmentId) {
		this.endowmentId = endowmentId;
	}

	public Long getMasrafId() {
		return masrafId;
	}

	public void setMasrafId(Long masrafId) {
		this.masrafId = masrafId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		
		this.fromDate = fromDate;
		if( this.fromDate != null )
		{
			formattedFromDate = formatterWithOutTime.format(this.fromDate)+" 00:00:00";
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
		if( this.toDate  != null )
		{
			formattedToDate = formatterWithOutTime.format(this.toDate )+" 23:59:59";
		}
	}

	public String getFormattedFromDateForReportDisplay() {
		if(this.fromDate != null )
		{
			java.util.Calendar cal  = GregorianCalendar.getInstance();
			cal.setTime(this.fromDate );
			cal.add(java.util.Calendar.DATE, -1);
		return formatterReportDisplay.format(  cal.getTime() );
		}
		return "";
	}

	public String getFormattedToDateForReportDisplay() {
		return formatterReportDisplay.format(this.toDate );
	}
	public String getFormattedFromDate() {
		return formattedFromDate;
	}

	public String getFormattedToDate() {
		return formattedToDate;
	}

	public String getShowSubMasrafTrx() {
		return showSubMasrafTrx;
	}

	public void setShowSubMasrafTrx(String showSubMasrafTrx) {
		this.showSubMasrafTrx = showSubMasrafTrx;
	}
	
	
}

