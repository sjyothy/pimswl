package com.avanza.pims.report.criteria;

public interface IReportCriteria {
	
	public String getReportFileName();
	public void setReportFileName(String reportFileName);
	public String getReportProcessorClassName();
	public void setReportProcessorClassName(String reportProcessorClassName);	
}
