package com.avanza.pims.report.criteria;

public class PropertyOccupancyDetailsReportCriteria extends AbstractReportCriteria
{
	private String unitUsage;
	private String propertyOwnerShip;
	private String buildingName;
	
	public PropertyOccupancyDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}


	public String getPropertyOwnerShip() {
		return propertyOwnerShip;
	}

	public void setPropertyOwnerShip(String propertyOwnerShip) {
		this.propertyOwnerShip = propertyOwnerShip;
	}


	public String getUnitUsage() {
		return unitUsage;
	}


	public void setUnitUsage(String unitUsage) {
		this.unitUsage = unitUsage;
	}


	public String getBuildingName() {
		return buildingName;
	}


	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

}
