package com.avanza.pims.report.criteria;


public class AuctionReportCriteria extends AbstractReportCriteria
{ 

	String auctionId;
	public AuctionReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}
	public String getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}

}
