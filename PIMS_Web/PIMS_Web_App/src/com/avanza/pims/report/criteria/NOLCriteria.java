package com.avanza.pims.report.criteria;

import java.util.ArrayList;
import java.util.List;

import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.CommercialActivityView;
import com.avanza.pims.ws.vo.ContractActivityView;


public class NOLCriteria extends AbstractReportCriteria
{
	private String requestId;
	private String commercialActivityListString="";
	private List<ContractActivityView> contractActivityList=new ArrayList<ContractActivityView>(0);
	private List<CommercialActivityView> commActivityList=new ArrayList<CommercialActivityView>(0);
	public NOLCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getCommercialActivityListString() 
	{
		try {
			RequestService rs = new RequestService();
			PropertyService ps=new PropertyService();
			int len=0;
			if (requestId != null)
			{
				contractActivityList = rs	.getContractActivitiesByRequestId(Long.parseLong(requestId));
				commActivityList=ps.getAllCommercialActivities(null);
				for(CommercialActivityView cmav:commActivityList)
				{
					for(ContractActivityView cnav:contractActivityList)
					{
						if(cmav.getCommercialActivityId().compareTo(cnav.getCommercialActivityId())==0)
							{
								++len;
								commercialActivityListString+=cmav.getCommercialActivityDescAr();
								if(len<contractActivityList.size())
									commercialActivityListString+=" ,";
							}
					}
				}
			}
		}
		catch (Exception e) 
		{
			System.out.println("getCommercialActivityListString Crashed ");
			e.printStackTrace();
		}
		return commercialActivityListString;
	}
	public void setCommercialActivityListString(String commercialActivityListString) {
		this.commercialActivityListString = commercialActivityListString;
	}
	
	
	
	
}
