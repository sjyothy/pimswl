package com.avanza.pims.report.criteria;

import java.util.Date;

public class TenantContractsListCriteria extends AbstractReportCriteria {

	public TenantContractsListCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}
	
	private String tenantName;
	private String tenantStatus;
	private String tenantType;
	private String tenantId;
	private String contractNumber; 
	private String propertyName;
	private String propertyOwnershipType;
	private Date fromContractDate;
	private Date toContractDate;
	private Date fromContractExpDate;
	private Date toContractExpDate;
	private String community;
	private String unitNumber;
	private String costCenter;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantStatus() {
		return tenantStatus;
	}
	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}
	public String getTenantType() {
		return tenantType;
	}
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}
	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}
	public Date getFromContractDate() {
		return fromContractDate;
	}
	public void setFromContractDate(Date fromContractDate) {
		this.fromContractDate = fromContractDate;
	}
	public Date getToContractDate() {
		return toContractDate;
	}
	public void setToContractDate(Date toContractDate) {
		this.toContractDate = toContractDate;
	}
	public Date getFromContractExpDate() {
		return fromContractExpDate;
	}
	public void setFromContractExpDate(Date fromContractExpDate) {
		this.fromContractExpDate = fromContractExpDate;
	}
	public Date getToContractExpDate() {
		return toContractExpDate;
	}
	public void setToContractExpDate(Date toContractExpDate) {
		this.toContractExpDate = toContractExpDate;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	
	
}