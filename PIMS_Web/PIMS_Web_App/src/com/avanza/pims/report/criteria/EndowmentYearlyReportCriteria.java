package com.avanza.pims.report.criteria;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class EndowmentYearlyReportCriteria extends AbstractReportCriteria
{
	private String forYear;
	private String endowmentName;
	private String endowmentNumber;
	private String endowmentFileName;
	private String endowmentFileNumber;
	private String endowmentPropertyName;
	private String waaqifName;
	private String year;
	private Date fromDate;
	private Date toDate;
	DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat formatterReportDisplay = new SimpleDateFormat("MMM, dd yyyy");


	public String getForYear() {
		return forYear;
	}

	public void setForYear(String forYear) {
		this.forYear = forYear;
	}

	public EndowmentYearlyReportCriteria(
										 String reportFileName,
										 String reportProcessorClassName,
										 String reportGeneratedBy
										)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}
	
	public DateFormat getFormatterWithOutTime() {
		return formatterWithOutTime;
	}

	public String getEndowmentName() {
		return endowmentName;
	}
	public void setEndowmentName(String endowmentName) {
		this.endowmentName = endowmentName;
	}
	public String getEndowmentNumber() {
		return endowmentNumber;
	}
	public void setEndowmentNumber(String endowmentNumber) {
		this.endowmentNumber = endowmentNumber;
	}
	public String getEndowmentFileName() {
		return endowmentFileName;
	}
	public void setEndowmentFileName(String endowmentFileName) {
		this.endowmentFileName = endowmentFileName;
	}
	public String getEndowmentFileNumber() {
		return endowmentFileNumber;
	}
	public void setEndowmentFileNumber(String endowmentFileNumber) {
		this.endowmentFileNumber = endowmentFileNumber;
	}
	public String getEndowmentPropertyName() {
		return endowmentPropertyName;
	}
	public void setEndowmentPropertyName(String endowmentPropertyName) {
		this.endowmentPropertyName = endowmentPropertyName;
	}
	public String getWaaqifName() {
		return waaqifName;
	}
	public void setWaaqifName(String waaqifName) {
		this.waaqifName = waaqifName;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
