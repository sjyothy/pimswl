package com.avanza.pims.report.criteria;

import java.util.ArrayList;
import java.util.List;


public class RenewContractsReminderLetterCriteria extends AbstractReportCriteria{

	public RenewContractsReminderLetterCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}
	
	
	private Long contractId;
	private String contractNumber;
	private String rentValue;
	private String contractIdsCSV ;
	private String contractRentQuery;


	public Long getContractId() {
		return contractId;
	}


	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getRentValue() {
		return rentValue;
	}


	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}


	public String getContractIdsCSV() {
		return contractIdsCSV;
	}


	public void setContractIdsCSV(String contractIdsCSV) {
		this.contractIdsCSV = contractIdsCSV;
	}


	public String getContractRentQuery() {
		return contractRentQuery;
	}


	public void setContractRentQuery(String contractRentQuery) {
		this.contractRentQuery = contractRentQuery;
	}

	
}	
