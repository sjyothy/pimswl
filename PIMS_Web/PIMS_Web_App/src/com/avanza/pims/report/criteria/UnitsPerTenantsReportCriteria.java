 package com.avanza.pims.report.criteria;



public class UnitsPerTenantsReportCriteria extends AbstractReportCriteria
{
	private String txtPropertyNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String txtTenantName;
	private String cboEmirate;
	private String cboUnittype;
	private String txtContractNumber;
	private String cboCommunity;
	private String txtCostCenter;
	private String txtUnitNumber;
	
	public UnitsPerTenantsReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}
	public String getTxtPropertyNumber() {
		return txtPropertyNumber;
	}
	public void setTxtPropertyNumber(String txtPropertNumber) {
		this.txtPropertyNumber = txtPropertNumber;
	}
	public String getCboPropertyType() {
		return cboPropertyType;
	}
	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}
	public String getTxtPropertyName() {
		return txtPropertyName;
	}
	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}
	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}
	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}
	
	public String getCboEmirate() {
		return cboEmirate;
	}
	public void setCboEmirate(String cboEmirate) {
		this.cboEmirate = cboEmirate;
	}
	
	public String getCboUnittype() {
		return cboUnittype;
	}
	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}
	
	public String getTxtContractNumber() {
		return txtContractNumber;
	}
	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}
	public String getTxtTenantName() {
		return txtTenantName;
	}
	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}
	public String getCboCommunity() {
		return cboCommunity;
	}
	public void setCboCommunity(String cboCommunity) {
		this.cboCommunity = cboCommunity;
	}
	public String getTxtCostCenter() {
		return txtCostCenter;
	}
	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}
	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}
	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}
}
