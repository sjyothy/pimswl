package com.avanza.pims.report.criteria;


public class FileDetailsReportCriteria extends AbstractReportCriteria
{
	private Long fileId;
	
	public FileDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}


}
