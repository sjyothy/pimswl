package com.avanza.pims.report.criteria;

import java.util.Date;

public class ServiceContractsReportCriteria extends AbstractReportCriteria{
	public ServiceContractsReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractNumber;
	private String Status;
	private String contractType ;
	private String contractorName;
	private Date fromContractDate;
	private Date toContractDate;
	private String propertyName;
	private String propertyOwnershipType;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getContractorName() {
		return contractorName;
	}
	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}
	public Date getFromContractDate() {
		return fromContractDate;
	}
	public void setFromContractDate(Date fromContractDate) {
		this.fromContractDate = fromContractDate;
	}
	public Date getToContractDate() {
		return toContractDate;
	}
	public void setToContractDate(Date toContractDate) {
		this.toContractDate = toContractDate;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}
	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}
	
}
