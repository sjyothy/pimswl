package com.avanza.pims.report.criteria;

import java.util.Date;

public class PaymentSummaryReportCriteria extends AbstractReportCriteria{
	
	public PaymentSummaryReportCriteria (String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String propertyName;
	private String contractType;
	private String propertyOwnershipType;
	private String paymentType;
	private Date paymentDueDateFrom;
	private Date paymentDueDateTo;
	private Date receiptDateFrom;
	private Date receiptDateTo;
	private String receiptNo;
	private String txtTenantName;
	private String txtContractNumber;
	private String txtCostCenter;
	
	public String getTxtTenantName() {
		return txtTenantName;
	}
	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}
	public String getTxtContractNumber() {
		return txtContractNumber;
	}
	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}
	public String getTxtCostCenter() {
		return txtCostCenter;
	}
	public void setTxtCostCenter(String txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}
	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Date getPaymentDueDateFrom() {
		return paymentDueDateFrom;
	}
	public void setPaymentDueDateFrom(Date paymentDueDateFrom) {
		this.paymentDueDateFrom = paymentDueDateFrom;
	}
	public Date getPaymentDueDateTo() {
		return paymentDueDateTo;
	}
	public void setPaymentDueDateTo(Date paymentDueDateTo) {
		this.paymentDueDateTo = paymentDueDateTo;
	}
	public Date getReceiptDateFrom() {
		return receiptDateFrom;
	}
	public void setReceiptDateFrom(Date receiptDateFrom) {
		this.receiptDateFrom = receiptDateFrom;
	}
	public Date getReceiptDateTo() {
		return receiptDateTo;
	}
	public void setReceiptDateTo(Date receiptDateTo) {
		this.receiptDateTo = receiptDateTo;
	}
	
	
	
	
}
