 package com.avanza.pims.report.criteria;

import java.util.Date;

public class MaintenanceRequestsReportCriteria extends AbstractReportCriteria
{
	private String maintenanceRequestNo;
	private String contractorName;
	private Date  maintenanceRequestDateFrom;
	private Date  maintenanceRequestDateTo;
	private String  propertyName;
	private String  ownershipType;
	private String  requestSource;
	private String maintenanceWorkType;
	private String maintenanceType;
	private String underContract;
	private String maintenancePriority;
	private String requestStatus; 
	
	public MaintenanceRequestsReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getMaintenanceRequestNo() {
		return maintenanceRequestNo;
	}

	public void setMaintenanceRequestNo(String maintenanceRequestNo) {
		this.maintenanceRequestNo = maintenanceRequestNo;
	}

	public String getContractorName() {
		return contractorName;
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}

	public Date getMaintenanceRequestDateFrom() {
		return maintenanceRequestDateFrom;
	}

	public void setMaintenanceRequestDateFrom(Date maintenanceRequestDateFrom) {
		this.maintenanceRequestDateFrom = maintenanceRequestDateFrom;
	}

	public Date getMaintenanceRequestDateTo() {
		return maintenanceRequestDateTo;
	}

	public void setMaintenanceRequestDateTo(Date maintenanceRequestDateTo) {
		this.maintenanceRequestDateTo = maintenanceRequestDateTo;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public String getRequestSource() {
		return requestSource;
	}

	public void setRequestSource(String requestSource) {
		this.requestSource = requestSource;
	}

	public String getMaintenanceWorkType() {
		return maintenanceWorkType;
	}

	public void setMaintenanceWorkType(String maintenanceWorkType) {
		this.maintenanceWorkType = maintenanceWorkType;
	}

	public String getMaintenanceType() {
		return maintenanceType;
	}

	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}

	public String getUnderContract() {
		return underContract;
	}

	public void setUnderContract(String underContract) {
		this.underContract = underContract;
	}

	

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getMaintenancePriority() {
		return maintenancePriority;
	}

	public void setMaintenancePriority(String maintenancePriority) {
		this.maintenancePriority = maintenancePriority;
	}
	
}
