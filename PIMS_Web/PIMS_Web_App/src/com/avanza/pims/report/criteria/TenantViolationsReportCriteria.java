package com.avanza.pims.report.criteria;

import java.util.Date;

public class TenantViolationsReportCriteria extends AbstractReportCriteria {

	public TenantViolationsReportCriteria (String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String tenantId;
	private String tenantName;
	private String propertyName;
	private String propertyOwnershipType;
	private String unitNumber;
	private String contractNumber;
	private Date violationDate;
	private String violationCategory;
	private String violationType;
	private String violationStatus;
	private String violationAction;

	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyOwnershipType() {
		return propertyOwnershipType;
	}
	public void setPropertyOwnershipType(String propertyOwnershipType) {
		this.propertyOwnershipType = propertyOwnershipType;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public Date getViolationDate() {
		return violationDate;
	}
	public void setViolationDate(Date violationDate) {
		this.violationDate = violationDate;
	}
	public String getViolationCategory() {
		return violationCategory;
	}
	public void setViolationCategory(String violationCategory) {
		this.violationCategory = violationCategory;
	}
	public String getViolationType() {
		return violationType;
	}
	public void setViolationType(String violationType) {
		this.violationType = violationType;
	}
	public String getViolationStatus() {
		return violationStatus;
	}
	public void setViolationStatus(String violationStatus) {
		this.violationStatus = violationStatus;
	}
	public String getViolationAction() {
		return violationAction;
	}
	public void setViolationAction(String violationAction) {
		this.violationAction = violationAction;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	
	

	
	
}
