package com.avanza.pims.report.criteria;

import java.util.Date;

public class TenantsByContractsReportCriteria extends AbstractReportCriteria  
{	
	 
	private String propertyType;
	private String propertyName;
	private String propertyUsage;
	private String emirate;
	private String unitNumber;
	private String unittype;
	private String tenantName; 
	private String contractNumber;
	private Date contractPeriodFrom;
	private Date contractPeriodTo;
	private String community;
	private String costCenter;

	
	public String getCommunity() {
		return community;
	}


	public void setCommunity(String community) {
		this.community = community;
	}


	public String getCostCenter() {
		return costCenter;
	}


	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}


	public TenantsByContractsReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}


	public String getPropertyType() {
		return propertyType;
	}


	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}


	public String getPropertyName() {
		return propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public String getPropertyUsage() {
		return propertyUsage;
	}


	public void setPropertyUsage(String propertyUsage) {
		this.propertyUsage = propertyUsage;
	}


	public String getEmirate() {
		return emirate;
	}


	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}


	public String getUnitNumber() {
		return unitNumber;
	}


	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}


	public String getUnittype() {
		return unittype;
	}


	public void setUnittype(String unittype) {
		this.unittype = unittype;
	}


	public String getTenantName() {
		return tenantName;
	}


	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public Date getContractPeriodFrom() {
		return contractPeriodFrom;
	}


	public void setContractPeriodFrom(Date contractPeriodFrom) {
		this.contractPeriodFrom = contractPeriodFrom;
	}


	public Date getContractPeriodTo() {
		return contractPeriodTo;
	}


	public void setContractPeriodTo(Date contractPeriodTo) {
		this.contractPeriodTo = contractPeriodTo;
	}


	}
