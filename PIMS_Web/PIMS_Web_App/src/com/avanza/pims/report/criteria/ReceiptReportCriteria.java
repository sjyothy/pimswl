package com.avanza.pims.report.criteria;

import java.util.Date;

public class ReceiptReportCriteria extends AbstractReportCriteria{ 

	private String tenantName;
	private String propertyName;
	private String unitNumber;
	private String contractNumber;
	private String paymentScheduleId;
	private String paymentType;
	private String paymentMethod;
	private String receiptNumber;
	private Date receiptDate;
	private Date receiptDateTo;
	private String receiptBy;
	private String paidBy;
	private String bankName;
	private String chequeType;
	private String transactionType;
	private String requestNumber;
	private String criteriaReceiptBy;
	private boolean cancelledReceipt;
	private String ownerShipTypes;
	
	public String getCriteriaReceiptBy() {
		return criteriaReceiptBy;
	}

	public void setCriteriaReceiptBy(String criteriaReceiptBy) {
		this.criteriaReceiptBy = criteriaReceiptBy;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public ReceiptReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}


	public String getReceiptBy() {
		return receiptBy;
	}

	public void setReceiptBy(String receiptBy) {
		this.receiptBy = receiptBy;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeType() {
		return chequeType;
	}

	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}

	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public Date getReceiptDateTo() {
		return receiptDateTo;
	}

	public void setReceiptDateTo(Date receiptDateTo) {
		this.receiptDateTo = receiptDateTo;
	}

	public boolean isCancelledReceipt() {
		return cancelledReceipt;
	}

	public void setCancelledReceipt(boolean cancelledReceipt) {
		this.cancelledReceipt = cancelledReceipt;
	}

	public String getOwnerShipTypes() {
		return ownerShipTypes;
	}

	public void setOwnerShipTypes(String ownerShipTypes) {
		this.ownerShipTypes = ownerShipTypes;
	}
}
