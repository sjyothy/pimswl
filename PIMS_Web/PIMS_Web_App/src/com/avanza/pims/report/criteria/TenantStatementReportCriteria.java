package com.avanza.pims.report.criteria;

public class TenantStatementReportCriteria extends AbstractReportCriteria 
{	
	private String tenantName;
	private String tenantType;
	private String contractNumber;
	private String propertyName;
	private String unitNumber;
	
	
	
	public TenantStatementReportCriteria(String reportFileName,	String reportProcessorClassName, String reportGeneratedBy){ 
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}



	public String getTenantName() {
		return tenantName;
	}



	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}



	public String getTenantType() {
		return tenantType;
	}



	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}



	public String getContractNumber() {
		return contractNumber;
	}



	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}



	public String getPropertyName() {
		return propertyName;
	}



	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}



	public String getUnitNumber() {
		return unitNumber;
	}



	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}


}
