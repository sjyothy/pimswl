package com.avanza.pims.report.criteria;



public class PortFolioDetailsCriteria extends AbstractReportCriteria 
{
	private String accountType;
	private String assetCategory;
	private String assetName;
	private String portfoliName;
	private String portfolioNumber;
	private String year;
	private String quarter;
	
	public PortFolioDetailsCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}
	
	
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAssetCategory() {
		return assetCategory;
	}
	public void setAssetCategory(String assetCategory) {
		this.assetCategory = assetCategory;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getPortfoliName() {
		return portfoliName;
	}
	public void setPortfoliName(String portfoliName) {
		this.portfoliName = portfoliName;
	}
	public String getPortfolioNumber() {
		return portfolioNumber;
	}
	public void setPortfolioNumber(String portfolioNumber) {
		this.portfolioNumber = portfolioNumber;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public String getQuarter() {
		return quarter;
	}


	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	

}
