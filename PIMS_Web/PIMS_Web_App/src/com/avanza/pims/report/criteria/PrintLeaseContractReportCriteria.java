package com.avanza.pims.report.criteria;

public class PrintLeaseContractReportCriteria extends AbstractReportCriteria 
{

	private String contractId;
	private String statusId;
	public String getStatusId()
	{
		return statusId;
	}
	public void setStatusId(String statusId) 
	{
		this.statusId = statusId;
	}
	public PrintLeaseContractReportCriteria(String reportFileName, String reportProcessorClassName) 
	{
		super(reportFileName, reportProcessorClassName);
	}
	public String getContractId() 
	{
		return contractId;
	}
	public void setContractId(String contractId) 
	{
		this.contractId = contractId;
	}
	
}
