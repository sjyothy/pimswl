package com.avanza.pims.report.criteria;

public class MEMSPaymentReceiptReportCriteria  extends AbstractReportCriteria {

	public MEMSPaymentReceiptReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String collectionTransactionId;
    private String requestId;
    private String paymentScheduleId;
	public String getCollectionTransactionId() {
		return collectionTransactionId;
	}

	public void setCollectionTransactionId(String collectionTransactionId) {
		this.collectionTransactionId = collectionTransactionId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}

	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}	
	
}
