package com.avanza.pims.report.criteria;

public class PaymentReceiptReportCriteria  extends AbstractReportCriteria {

	public PaymentReceiptReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String contractId;
	private String paymentScheduleId;
	private String receiptNumber;
	private String reportProcedureNameEn;
	private String reportProcedureNameAr;
	
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReportProcedureNameEn() {
		return reportProcedureNameEn;
	}
	public void setReportProcedureNameEn(String reportProcedureNameEn) {
		this.reportProcedureNameEn = reportProcedureNameEn;
	}
	public String getReportProcedureNameAr() {
		return reportProcedureNameAr;
	}
	public void setReportProcedureNameAr(String reportProcedureNameAr) {
		this.reportProcedureNameAr = reportProcedureNameAr;
	}
	
	
}
