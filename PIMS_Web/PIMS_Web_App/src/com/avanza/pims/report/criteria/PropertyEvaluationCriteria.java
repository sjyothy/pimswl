package com.avanza.pims.report.criteria;

public class PropertyEvaluationCriteria extends AbstractReportCriteria {

	public PropertyEvaluationCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private Long requestId;
	private boolean wholeProperty;
	

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public boolean isWholeProperty() {
		return wholeProperty;
	}

	public void setWholeProperty(boolean wholeProperty) {
		this.wholeProperty = wholeProperty;
	}

}
