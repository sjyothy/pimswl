package com.avanza.pims.report.criteria;

import java.util.Date;

public class RequestDetailsReportCriteria extends AbstractReportCriteria
{

	private String requestId;

	public RequestDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}	

}
