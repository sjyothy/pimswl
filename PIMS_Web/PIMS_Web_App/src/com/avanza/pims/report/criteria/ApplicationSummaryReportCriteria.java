package com.avanza.pims.report.criteria;

import java.util.Date;

public class ApplicationSummaryReportCriteria extends AbstractReportCriteria 
{
	private String applicationNumber;
	private String applicationStatus;
	private String applicantName;
	private String applicationType;
	private Date applicationDateFrom;
	private Date applicationDateTo;
	private String createdBy;
	
		
	
	public ApplicationSummaryReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
		
	}


	public String getApplicationNumber() {
		return applicationNumber;
	}


	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}


	public String getApplicationStatus() {
		return applicationStatus;
	}


	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}


	public String getApplicantName() {
		return applicantName;
	}


	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}


	public String getApplicationType() {
		return applicationType;
	}


	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}


	public Date getApplicationDateFrom() {
		return applicationDateFrom;
	}


	public void setApplicationDateFrom(Date applicationDateFrom) {
		this.applicationDateFrom = applicationDateFrom;
	}


	
	public Date getApplicationDateTo() {
		return applicationDateTo;
	}


	public void setApplicationDateTo(Date applicationDateTo) {
		this.applicationDateTo = applicationDateTo;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	

}
