package com.avanza.pims.report.criteria;

import java.util.Date;

public class UnitInquiryReportCriteria extends AbstractReportCriteria  
{	
	private String txtPropertNumber; 
	private String cboPropertyType;
	private String txtPropertyName;
	private String cboPropertyUsage;
	private String txtLandNumber;
	private String cboEmirate;
	private String txtUnitNumber;
	private String cboUnittype;
	private String txtFloorNumber;
	private String txtTenantName; 
	private Date clndrFromDate;
	private Date cldrToDate;
	private String txtContractNumber;
	
	public UnitInquiryReportCriteria(String reportFileName, String reportProcessorClassName,String reportGeneratedBy) 
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);
	}

	public String getTxtPropertNumber() {
		return txtPropertNumber;
	}

	public void setTxtPropertNumber(String txtPropertNumber) {
		this.txtPropertNumber = txtPropertNumber;
	}

	public String getCboPropertyType() {
		return cboPropertyType;
	}

	public void setCboPropertyType(String cboPropertyType) {
		this.cboPropertyType = cboPropertyType;
	}

	public String getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(String txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public String getCboPropertyUsage() {
		return cboPropertyUsage;
	}

	public void setCboPropertyUsage(String cboPropertyUsage) {
		this.cboPropertyUsage = cboPropertyUsage;
	}

	public String getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(String txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public String getCboEmirate() {
		return cboEmirate;
	}

	public void setCboEmirate(String cboEmirate) {
		this.cboEmirate = cboEmirate;
	}

	public String getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(String txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public String getCboUnittype() {
		return cboUnittype;
	}

	public void setCboUnittype(String cboUnittype) {
		this.cboUnittype = cboUnittype;
	}

	public String getTxtFloorNumber() {
		return txtFloorNumber;
	}

	public void setTxtFloorNumber(String txtFloorNumber) {
		this.txtFloorNumber = txtFloorNumber;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public Date getClndrFromDate() {
		return clndrFromDate;
	}

	public void setClndrFromDate(Date clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}

	public Date getCldrToDate() {
		return cldrToDate;
	}

	public void setCldrToDate(Date cldrToDate) {
		this.cldrToDate = cldrToDate;
	}

	public String getTxtContractNumber() {
		return txtContractNumber;
	}

	public void setTxtContractNumber(String txtContractNumber) {
		this.txtContractNumber = txtContractNumber;
	}	
}
