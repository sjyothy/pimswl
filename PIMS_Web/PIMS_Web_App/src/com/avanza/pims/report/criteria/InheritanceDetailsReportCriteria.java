package com.avanza.pims.report.criteria;


public class InheritanceDetailsReportCriteria extends AbstractReportCriteria
{
	private Long fileId;
	
	public InheritanceDetailsReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}


}
