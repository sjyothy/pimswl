package com.avanza.pims.report.criteria;

public class ConstructionProjectsReportCriteria extends AbstractReportCriteria{
	
	public ConstructionProjectsReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		super(reportFileName, reportProcessorClassName, reportGeneratedBy);
	}

	private String projectNumber;
	private String projectStatus;
	private String projectName;
	private String projectType;
	@Override
	public String getDateFormat() {
		
		return super.getDateFormat();
	}
	
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	

}
