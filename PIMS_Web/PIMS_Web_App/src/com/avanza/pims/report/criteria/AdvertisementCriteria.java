package com.avanza.pims.report.criteria;


public class AdvertisementCriteria extends AbstractReportCriteria
{
	private Long auctionId;
	public AdvertisementCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}
	public Long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(Long auctionId) {
		this.auctionId = auctionId;
	}
	
}
