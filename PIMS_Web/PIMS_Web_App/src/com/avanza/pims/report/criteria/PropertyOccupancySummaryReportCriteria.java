package com.avanza.pims.report.criteria;

public class PropertyOccupancySummaryReportCriteria extends AbstractReportCriteria
{
	private String unitUsage;
	private String propertyOwnerShip;
	
	public PropertyOccupancySummaryReportCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}


	public String getPropertyOwnerShip() {
		return propertyOwnerShip;
	}

	public void setPropertyOwnerShip(String propertyOwnerShip) {
		this.propertyOwnerShip = propertyOwnerShip;
	}


	public String getUnitUsage() {
		return unitUsage;
	}


	public void setUnitUsage(String unitUsage) {
		this.unitUsage = unitUsage;
	}

}
