package com.avanza.pims.report.manager;

import java.util.Map;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.processor.IReportProcessor;
import com.crystaldecisions.reports.sdk.IReportClientDocument;
import com.crystaldecisions.sdk.occa.report.reportsource.IReportSource;

public interface IReportManager {
	
	public IReportCriteria getReportCriteria();
	public void setReportCriteria(IReportCriteria reportCriteria);
	public IReportProcessor getReportProcessor();
	public void setReportProcessor(IReportProcessor reportProcessor);
	public IReportClientDocument getReportClientDocument();
	public void setReportClientDocument(IReportClientDocument reportClientDocument);
	public String getReportBaseFolderName();
	public void setReportBaseFolderName(String reportBaseFolderName);
	public Map<String, String> getReportQueryMap();
	public void setReportQueryMap(Map<String, String> reportQueryMap);
	public IReportSource getReportSource() throws Exception;
	
}
