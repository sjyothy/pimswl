package com.avanza.pims.report.manager;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.processor.IReportProcessor;
import com.crystaldecisions.reports.sdk.IReportClientDocument;
import com.crystaldecisions.reports.sdk.ReportClientDocument;

public abstract class AbstractReportManager implements IReportManager {

	private IReportCriteria reportCriteria;
	private IReportProcessor reportProcessor;
	private IReportClientDocument reportClientDocument;
	public static String reportBaseFolderName;
	private Map<String, String> reportQueryMap;
	private Map<String, String> reportParametersMap;
	
	public AbstractReportManager(IReportCriteria reportCriteria) throws Exception {
		this.reportQueryMap = new HashMap<String, String>(0);
		this.reportParametersMap = new HashMap<String, String>(0);
		this.reportBaseFolderName = ReportConstant.BASE_REPORT_FOLDER_NAME;
		this.reportClientDocument = new ReportClientDocument();
		this.reportCriteria = reportCriteria;
		this.reportProcessor = (IReportProcessor) Class.forName( this.reportCriteria.getReportProcessorClassName() ).newInstance();  
	}

	public IReportCriteria getReportCriteria() {
		return reportCriteria;
	}

	public void setReportCriteria(IReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}

	public IReportProcessor getReportProcessor() {
		return reportProcessor;
	}

	public void setReportProcessor(IReportProcessor reportProcessor) {
		this.reportProcessor = reportProcessor;
	}

	public IReportClientDocument getReportClientDocument() {
		return reportClientDocument;
	}

	public void setReportClientDocument(IReportClientDocument reportClientDocument) {
		this.reportClientDocument = reportClientDocument;
	}

	public String getReportBaseFolderName() {
		return reportBaseFolderName;
	}

	public void setReportBaseFolderName(String reportBaseFolderName) {
//		this.reportBaseFolderName = reportBaseFolderName;
	}

	public Map<String, String> getReportQueryMap() {
		return reportQueryMap;
	}

	public void setReportQueryMap(Map<String, String> reportQueryMap) {
		this.reportQueryMap = reportQueryMap;
	}

	public Map<String, String> getReportParametersMap() {
		return reportParametersMap;
	}

	public void setReportParametersMap(Map<String, String> reportParametersMap) {
		this.reportParametersMap = reportParametersMap;
	}
	
}
