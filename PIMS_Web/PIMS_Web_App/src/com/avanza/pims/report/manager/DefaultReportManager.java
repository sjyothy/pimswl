package com.avanza.pims.report.manager;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.report.config.ReportConfig;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.util.JRCHelperSample;
import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.reportsource.IReportSource;

public class DefaultReportManager extends AbstractReportManager {
	
	private transient Logger logger = Logger.getLogger(DefaultReportManager.class);

	private Connection connection;
	private Statement statement;	
	private String connectionString;
	private String username;
	private String password;	

	public DefaultReportManager(IReportCriteria reportCriteria) throws Exception {		
		super(reportCriteria);
                logger.logInfo("=========In constructor of DefaultReportManager=========");
		connectionString = ReportConfig.getConfig( ReportConstant.Keys.CONNECTION_STRING );
		username = ReportConfig.getConfig( ReportConstant.Keys.USERNAME );
		password = ReportConfig.getConfig( ReportConstant.Keys.PASSWORD );
	}
	
	public IReportSource getReportSource() throws Exception {
		logger.logInfo("=========In method getReportSource=========");
		ReportClientDocument reportClientDocument = (ReportClientDocument) getReportClientDocument();
		logger.logInfo("=========got report client document =========");
		logger.logInfo("========File path is========="+
                               getReportBaseFolderName() + 
                               File.separator + 
                               getReportCriteria().getReportFileName()
                               );
		reportClientDocument.open(getReportBaseFolderName() + File.separator + getReportCriteria().getReportFileName(), OpenReportOptions._openAsReadOnly);		
		logger.logInfo("========Opened the file=========");
		setReportQueryMap( getReportProcessor().getReportQueryMap( getReportCriteria() ) );
		setReportParametersMap( getReportProcessor().getReportParametersMap( getReportCriteria() ) );
		logger.logInfo("========set report criteria=========");
		iterateReportQueryMap( getReportQueryMap(), reportClientDocument );
		logger.logInfo("========set report parameters=========");
		iterateReportParametersMap( getReportParametersMap(), reportClientDocument );		
		logger.logInfo("========iterating in report query map========");
		return reportClientDocument.getReportSource();
	}
	
	private void iterateReportQueryMap( Map<String, String> reportQueryMap, ReportClientDocument reportClientDocument ) throws Exception 
	{
		for ( String dataDef : reportQueryMap.keySet() ) {
			
			String query = reportQueryMap.get(dataDef) .toString() ;
			
			String subReportName = "";
			
			if (dataDef.contains("#")) { // SubReport Scenario
				String[] temp = dataDef.split("#");
				dataDef = temp[0]; // dataDef is 1st element
				subReportName = temp[1];
			}
			
			ResultSet resultSet = fetchResultSet( query );
			JRCHelperSample.passResultSet(reportClientDocument, resultSet, dataDef, subReportName );
			
		}
	}
	private void iterateReportParametersMap( Map<String, String> reportParametersMap, ReportClientDocument reportClientDocument ) throws Exception 
	{
		for ( String parameterName : reportParametersMap.keySet() ) {
			String parameterValue = reportParametersMap.get(parameterName) .toString() ;
			JRCHelperSample.addDiscreteParameterValue(reportClientDocument, parameterName, "", parameterValue);
		}
	}
	
	private ResultSet fetchResultSet( String query ) throws Exception {		
		logger.logInfo("Query::"+query);
		if (connection == null) 
		{		
			Class.forName("oracle.jdbc.OracleDriver");
//			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection(connectionString, username, password);			
		}
		statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		
		return statement.executeQuery( query );
	}

}
