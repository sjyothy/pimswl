package com.avanza.pims.report.constant;

public class ReportConstant {
	
	public static final String LOAD_REPORT_JSP = "loadReport.jsf";
	public static final String LOAD_POJO_REPORT_JSP = "loadPOJOReport.jsf";
	public static final String EXPORT_LIST_BASED_EXCEL_REPORT = "exportListBasedExcelReport.jsf";
	public static final String BASE_REPORT_FOLDER_NAME = "reports";
	
	public class Keys {
		public static final String CONNECTION_STRING = "connectionString";
		public static final String USERNAME = "username";
		public static final String PASSWORD = "password";
		public static final String REPORT_CRITERIA_KEY = "REPORT_CRITERIA_KEY";
		public static final String POJO_REPORT_DATASOURCE_KEY = "POJO_REPORT_DATASOURCE_KEY";
		public static final String REPORT_COLUMN_HEADERS_LIST = "REPORT_COLUMN_HEADERS_LIST";
		public static final String REPORT_SQL_QUERY = "REPORT_SQL_QUERY";
		public static final String REPORT_DATA_LIST = "REPORT_DATA_LIST";
		public static final String REPORT_EXPORTER_META = "REPORT_EXPORTER_META";
		public static final String REPORT_MULTIPLE_SHEETS_MAP = "REPORT_MULTIPLE_SHEETS_MAP";
		public static final String EXCEL_SHEETS_COUNT = "EXCEL_SHEETS_COUNT";
		public static final String SHEET_NAME = "SHEET_NAME";
	}
	
	public class Report {
		
		
		public static final String CONTRACT_PAYMENTS = "ContractPaymentsReport_EnAr.rpt";
		public static final String STATEMENT_OF_ACCOUNT = "StatementOfAccount_En.rpt";
		public static final String STATEMENT_OF_ACCOUNT_NEW = "StatementOfAccountNew.rpt";
		public static final String STATEMENT_OF_ACCOUNT_ENDOWMENT = "StatementOfAccountEndowment.rpt";
		public static final String STATEMENT_OF_ACCOUNT_MASRAF = "StatementOfAccountMasraf.rpt";
		public static final String STATEMENT_OF_ACCOUNT_MASRAF_BENEFICIARY = "StatementOfAccountMasrafBeneficiary.rpt";
		public static final String ALL_MASRAF_BALANCE_SUMMARY = "AllMasrafBalanceSummary.rpt";
		public static final String ENDOWMENT_YEARLY_REPORT = "EndowmentYearlyReport.rpt";
		public static final String WAQIF_BALANCE_DETAILS = "WaqifBalanceDetails.rpt";
		public static final String AUCTION_BIDDER_REPORT="AuctionReport.rpt";
		public static final String CHEQUE_WITHDRAWL_REPORT="ChequeWithdrawlReport_EnAr.rpt";
		public static final String COMMERCIAL_DISCLOSURE = "CommercialDisclosure.rpt";
		public static final String RESIDENTIAL_DISCLOSURE = "ResidentialDisclosure.rpt";
		public static final String SAMPLE_PROPERTY_DETAIL_EN = "samplePropertyDetail_En.rpt";
		public static final String ADVERTISEMENT_REPORT_EN = "Advertisement_En.rpt";
		public static final String ADVERTISEMENT_REPORT_AR = "Advertisement_Ar.rpt";
		public static final String PRINT_LEASE_CONTRACT = "printLeaseContract.rpt";
		public static final String CLEARANCE_LETTER_REPORT = "ClearanceLetter.rpt";

		public static final String PROPERTY_DETAIL = "propertyDetail.rpt";
		public static final String PROPERTY_UNIT_LIST = "propertyUnitList.rpt";

		public static final String CHEQUE_DETAILS_LIST = "chequeDetailsList.rpt";
		public static final String RECEIPT_VOUCHERS_REPORT="receiptVouchersReport.rpt";
		public static final String APPLICATION_DETAILS_REPORT="applicationDetailsReport.rpt"; 
		public static final String TENANT_DETAILS_REPORT="tenantDetailsReport.rpt";
		
		public static final String PROPERTY_DETAIL_EN = "propertyDetail_En.rpt";
		public static final String PROPERTY_DETAIL_AR = "propertyDetail_Ar.rpt";
		public static final String CHEQUE_DETAILS_LIST_EN = "chequeDetailsList_En.rpt";
		public static final String CHEQUE_DETAILS_LIST_AR = "chequeDetailsList_Ar.rpt";
		public static final String PROPERTY_TENANTS_LIST_EN = "propertyTenantsList_En.rpt";
		public static final String PROPERTY_TENANTS_LIST_AR = "propertyTenantsList_Ar.rpt";
		public static final String PROPERTY_UNIT_LIST_EN = "propertyUnitList_En.rpt";
		public static final String RECEIPT_VOUCHERS_REPORT_EN="receiptVouchersReport_En.rpt";
		public static final String APPLICATION_DETAILS_REPORT_EN="applicationDetailsReport_En.rpt"; 
		public static final String APPLICATION_SUMMARY_REPORT_EN="applicationSummaryReport_En.rpt"; 
		public static final String TENANT_DETAILS_REPORT_EN="tenantDetailsReport_En.rpt";
		public static final String PROPERTY_UNIT_LIST_AR = "propertyUnitList_Ar.rpt";
		public static final String RECEIPT_VOUCHERS_REPORT_AR="receiptVouchersReport_Ar.rpt";
		public static final String APPLICATION_DETAILS_REPORT_AR="applicationDetailsReport_Ar.rpt"; 
		public static final String TENANT_DETAILS_REPORT_AR="tenantDetailsReport_Ar.rpt";
		public static final String APPLICATION_SUMMARY_REPORT_AR="applicationSummaryReport_Ar.rpt"; 
		public static final String PAYMENTS_DUE_DATES_AR="paymentsDueDatesReport_Ar.rpt";
		public static final String UNIT_EVALUATION_REPORT_EN="applicationSummaryReport_En.rpt";
		public static final String PROPERTY_EVALUATION_REPORT_EN="PropertyEvaluation.rpt";
		public static final String FILE_DETAILS_AR="FileDetailsReport_Ar.rpt";
		public static final String REQUEST_DETAILS_MEMS = "MemsRequestDetailReport.rpt";
		public static final String REQUEST_DETAILS_MASRAF_DISBURSEMENT = "MasrafDisbursementRequestDetailReport.rpt";
		public static final String REQUEST_DETAILS_ENDOWMENT_DISBURSEMENT = "EndowmentDisbursementRequestDetailReport.rpt";


		

        
		/*******************************                           English Report Names             *********************************/

		public static final String CONTRACT_PAYMENT_DETAIL_EN= "ContractPaymentDetailReport_En.rpt";
		public static final String CONTRACT_RENEWAL_HISTORY_EN = "ContractRenewalHistoryReport_En.rpt";
		public static final String MAINTENANCE_CONTRACTS_EN = "MaintenanceContractsReport_En.rpt";    
		public static final String LEASE_CONTRACTS_SUMMARY_EN = "LeaseContractsSummaryReport_En.rpt";
		public static final String LEASE_CONTRACTS_DETAIL_EN = "LeaseContractsDetailReport_En.rpt";
		public static final String TENANT_VIOLATION_EN = "TenantViolationsReport_En.rpt";
		public static final String TENANT_STATEMENT_REPORT_EN="TenantStatementReport_En.rpt";
		public static final String COLLECTION_REPORT_EN="CollectionReport_En.rpt";
		public static final String PAYMENT_RECEIPT_REPORT_ENAR="PaymentReceiptReport_EnAr.rpt";
		public static final String BOUNCED_CHECKS_DETAILS_EN ="BouncedChecksDetailsReport_En.rpt";
		public static final String PAYMENT_SUMMARY_EN ="PaymentSummaryReport_En.rpt";
		public static final String PAYMENTS_DUE_DATES_EN="paymentsDueDatesReport_En.rpt"; 
		public static final String PAYMENT_HISTORY_EN="PaymentHistoryReport_En.rpt";
		public static final String MEMS_PAYMENT_RECEIPT_REPORT_ENAR="MEMSPaymentReceiptReport_EnAr.rpt";
		public static final String MISC_PAYMENT_RECEIPT_REPORT_ENAR="MiscellaneousPaymentReceiptReport_EnAr.rpt";
		public static final String UNITS_PER_TENANTS_EN="unitsPerTenantsReport_En.rpt"; 
		public static final String DUE_PAYMENTS_PER_TENANT_EN="duePaymentsPerTenantReport_En.rpt";
		public static final String TENANT_CONTRACTS_LIST_EN = "TenantContractsListReport_En.rpt";
		public static final String SERVICE_CONTRACTS_EN = "ServiceContractsReport_En.rpt";
		public static final String PROPERTIES_LIST_EN = "propertiesListReport_En.rpt";
		public static final String TENANTS_BY_CONTRACTS_EN = "tenantsByContractsReport_En.rpt";
		public static final String CONSTRUCTION_PROJECTS_SUMMARY_EN = "ConstructionProjectsSummaryReport_En.rpt";
		public static final String CONSTRUCTION_PROJECTS_DETAIL_EN = "ConstructionProjectsDetailReport_En.rpt";
		public static final String PROJECT_MILESTONES_EN = "ProjectMilestonesReport_En.rpt";
		public static final String TENANT_CONTRACTS_PAYMENTS_EN= "tenantContractsPaymentsReport_En.rpt";
		public static final String VENDORS_REPORT_EN= "VendorsReport_En.rpt";
		public static final String MAINTENANCE_REQUESTS_REPORT_EN="maintenanceRequestsReport_En.rpt";
		public static final String VENDOR_OUTSTANDING_PAYMENT_REPORT_EN= "VendorOutstandingPaymentReport_En.rpt";
		public static final String ONLINE_PAYMENT_TRANSACTION_REPORT_EN= "onlinePaymentTransaction_En.rpt";
		public static final String SETTLEMENT_REPORT_EN="Settlement_En.rpt";
		public static final String REQUEST_DETAILS_REPORT_EN="RequestDetailReport_En.rpt";
		public static final String BANK_REMMITANCE_REPORT_EN="BankRemittanceReport _En.rpt";
		public static final String FOLLOWUP_REPORT_EN="FollowupReport_En.rpt";
		public static final String FILE_DETAILS_EN="FileDetailsReport_En.rpt";
		

		/*******************************                           Arabic Report Names             *********************************/
		public static final String REQUEST_DETAILS_REPORT_AR="RequestDetailReport_AR.rpt";
		public static final String CONTRACT_PAYMENT_DETAIL_AR= "ContractPaymentDetailReport_Ar.rpt";
		public static final String CONTRACT_RENEWAL_HISTORY_AR = "ContractRenewalHistoryReport_Ar.rpt";
		public static final String LEASE_CONTRACTS_SUMMARY_AR = "LeaseContractsSummaryReport_Ar.rpt";
		public static final String MAINTENANCE_CONTRACTS_AR = "MaintenanceContractsReport_Ar.rpt";    
		public static final String LEASE_CONTRACTS_DETAIL_AR = "LeaseContractsDetailReport_Ar.rpt";
		public static final String TENANT_VIOLATION_AR = "TenantViolationsReport_Ar.rpt";
		public static final String TENANT_STATEMENT_REPORT_AR="TenantStatementReport_Ar.rpt";
		public static final String COLLECTION_REPORT_AR="CollectionReport_Ar.rpt";
		public static final String BOUNCED_CHECKS_DETAILS_AR ="BouncedChecksDetailsReport_Ar.rpt";
		public static final String PAYMENT_SUMMARY_AR ="PaymentSummaryReport_Ar.rpt";
		public static final String PAYMENT_HISTORY_AR="PaymentHistoryReport_Ar.rpt";
		public static final String UNITS_PER_TENANTS_AR="unitsPerTenantsReport_Ar.rpt"; 
		//public static final String UNITS_PER_TENANTS_AR="unitsPerTenantsReport_Ar.rpt"; 
		public static final String DUE_PAYMENTS_PER_TENANT_AR="duePaymentsPerTenantReport_Ar.rpt";
		public static final String TENANT_CONTRACTS_LIST_AR = "TenantContractsListReport_Ar.rpt";
		public static final String SERVICE_CONTRACTS_AR = "ServiceContractsReport_Ar.rpt";
		public static final String PROPERTIES_LIST_AR = "propertiesListReport_Ar.rpt";
		public static final String TENANTS_BY_CONTRACTS_AR = "tenantsByContractsReport_Ar.rpt";
		public static final String CONSTRUCTION_PROJECTS_SUMMARY_AR = "ConstructionProjectsSummaryReport_Ar.rpt";
		public static final String CONSTRUCTION_PROJECTS_DETAIL_AR = "ConstructionProjectsDetailReport_Ar.rpt";
		public static final String PROJECT_MILESTONES_AR = "ProjectMilestonesReport_Ar.rpt";
		public static final String TENANT_CONTRACTS_PAYMENTS_AR= "tenantContractsPaymentsReport_Ar.rpt";
		public static final String VENDORS_REPORT_AR= "VendorsReport_Ar.rpt";
		public static final String MAINTENANCE_REQUESTS_REPORT_AR="maintenanceRequestsReport_Ar.rpt";
		public static final String VENDOR_OUTSTANDING_PAYMENT_REPORT_AR= "VendorOutstandingPaymentReport_Ar.rpt";
		public static final String ONLINE_PAYMENT_TRANSACTION_REPORT_AR= "onlinePaymentTransaction_Ar.rpt";
		public static final String SETTLEMENT_REPORT_AR="Settlement_Ar.rpt";
		public static final String BANK_REMMITANCE_REPORT_AR="BankRemittanceReport_Ar.rpt";
		

		public static final String RENEW_CONTRACT_REMINDER_LETTER="RenewContractReminderLetter.rpt";
		public static final String FOLLOWUP_REPORT_AR="FollowupReport_Ar.rpt";

		public static final String PORTFOLIO_DETAILS_REPORT ="PortfolioDetails.rpt";
		public static final String DETAILED_INVESTMENT_EVALUATION_REPORT_AR ="DetailedInvestmentEvaluationReport_Ar.rpt";
		public static final String SUMMARY_INVESTMENT_EVALUATION_REPORT_AR ="SummaryInvestmentEvaluationReport_Ar.rpt";
		
		public static final String PROPERTY_OCCUPANCY_SUMMARY_REPORT_EN="PropertyOccupancySummaryReport_En.rpt";
		public static final String PROPERTY_OCCUPANCY_SUMMARY_REPORT_AR="PropertyOccupancySummaryReport_Ar.rpt";
		public static final String PROPERTY_OCCUPANCY_DETAILS_REPORT_EN="PropertyOccupancyDetailsReport_En.rpt";
		public static final String PROPERTY_OCCUPANCY_DETAILS_REPORT_AR="PropertyOccupancyDetailsReport_Ar.rpt";
		
		//nol practicing activity
		public static final String PRACTICING_ACTIVITY="PracticingActivity.rpt";
		public static final String RECEIPT_REPORT="ReceiptReport.rpt";
		
		public static final String PROPERTY_EVALUATION_REPORT_AR="PropertyEvaluationReport_Ar.rpt";
		public static final String UNIT_EVALUATION_REPORT_AR="UnitEvaluationReport_Ar.rpt";
		public static final String INHERITANCE_DETAILS_AR="InheritanceDetailsReport_Ar.rpt";
		
		public static final String PENDING_PAYMENTS_AFTER_SETTLEMENT = "PendingPaymentsAfterSettlement_Ar.rpt";
	}
	
	public class Processor {
		public static final String AUCTION_BIDDER_REPORT="com.avanza.pims.report.processor.AuctionReportProcessor";
		public static final String CHEQUE_WITHDRAWL_REPORT="com.avanza.pims.report.processor.ChequeWithdrawlReportProcessor";
		public static final String REQUEST_DETAILS_REPORT_PROCESSOR = "com.avanza.pims.report.processor.RequestDetailsReportProcessor";
		
		public static final String CONTRACT_DISCLOSURE_PROCESSOR = "com.avanza.pims.report.processor.ContractDisclosureReportProcessor";
		public static final String PRINT_LEASE_CONTRACT = "com.avanza.pims.report.processor.PrintLeaseContractReportProcessor";
		public static final String ADVERTISEMENT_REPORT= "com.avanza.pims.report.processor.AdvertisementProcessor";

		public static final String  PROPERTY_DETAIL= "com.avanza.pims.report.processor.PropertyDetailReportProcessor";
		public static final String  PROPERTY_UNIT_LIST= "com.avanza.pims.report.processor.PropertyUnitListReportProcessor";
		public static final String  PROPERTY_TENANTS_LIST= "com.avanza.pims.report.processor.PropertyTenantsListReportProcessor";
		public static final String  CHEQUE_DETAILS_LIST= "com.avanza.pims.report.processor.ChequeDetailsListReportProcessor";
		public static final String COLLECTION_REPORT="com.avanza.pims.report.processor.CollectionReportProcessor";
		public static final String RECEIPT_VOUCHERS_REPORT="com.avanza.pims.report.processor.ReceiptVouchersReportProcessor";
		public static final String APPLICATION_DETAILS_REPORT="com.avanza.pims.report.processor.ApplicationDetailsReportProcessor";
		public static final String TENANT_DETAILS_REPORT="com.avanza.pims.report.processor.TenantDetailsReportProcessor";

		public static final String CONTRACT_PAYMENT_DETAIL = "com.avanza.pims.report.processor.ContractPaymentDetailReportProcessor";
		public static final String CONTRACT_RENEWAL_HISTORY = "com.avanza.pims.report.processor.ContractRenewalHistoryReportProcessor";
		public static final String MAINTENANCE_CONTRACTS = "com.avanza.pims.report.processor.MaintenanceContractsReportProcessor";
		public static final String LEASE_CONTRACTS_SUMMARY = "com.avanza.pims.report.processor.LeaseContractsSummaryReportProcessor";
		public static final String LEASE_CONTRACTS_DETAIL = "com.avanza.pims.report.processor.LeaseContractDetailReportProcessor";
		public static final String TENANT_VIOLATION = "com.avanza.pims.report.processor.TenantViolationsReportProcessor";
		public static final String TENANT_STATEMENT="com.avanza.pims.report.processor.TenantStatementReportProcessor";
		public static final String PAYMENT_RECEIPT_REPORT="com.avanza.pims.report.processor.PaymentReceiptReportProcessor";
		public static final String MEMS_PAYMENT_RECEIPT_REPORT="com.avanza.pims.report.processor.MEMSPaymentReceiptReportProcessor";
		public static final String MISC_PAYMENT_RECEIPT_REPORT="com.avanza.pims.report.processor.MiscellaneousPaymentReceiptReportProcessor";
		public static final String BOUNCED_CHECKS_DETAILS ="com.avanza.pims.report.processor.BouncedChecksDetailsReportProcessor";
		public static final String APPLICATION_SUMMARY_REPORT="com.avanza.pims.report.processor.ApplicationSummaryReportProcessor";
		public static final String PAYMENT_SUMMARY="com.avanza.pims.report.processor.PaymentSummaryReportProcessor";
		public static final String PAYMENTS_DUE_DATES="com.avanza.pims.report.processor.PaymentsDueDatesReportProcessor";
		public static final String PAYMENT_HISTORY="com.avanza.pims.report.processor.PaymentHistoryReportProcessor";
		public static final String UNITS_PER_TENANTS="com.avanza.pims.report.processor.UnitsPerTenantsReportProcessor";
		public static final String DUE_PAYMENTS_PER_TENANT="com.avanza.pims.report.processor.DuePaymentsPerTenantReportProcessor";
		public static final String TENANT_CONTRACTS_LIST="com.avanza.pims.report.processor.TenantContractsListReportProcessor";
		public static final String SERVICE_CONTRACTS="com.avanza.pims.report.processor.ServiceContractsReportProcessor";
		public static final String PROPERTIES_LIST="com.avanza.pims.report.processor.PropertiesListReportProcessor";
		public static final String TENANTS_BY_CONTRACTS="com.avanza.pims.report.processor.TenantsByContractsReportProcessor";
		public static final String CONSTRUCTION_PROJECTS_SUMMARY="com.avanza.pims.report.processor.ConstructionProjectsSummaryReportProcessor";
		public static final String CONSTRUCTION_PROJECTS_DETAIL="com.avanza.pims.report.processor.ConstructionProjectsDetailReportProcessor";
		public static final String PROJECT_MILESTONES="com.avanza.pims.report.processor.ProjectMilestonesReportProcessor";
		public static final String TENANT_CONTRACTS_PAYMENTS="com.avanza.pims.report.processor.TenantContractsPaymentsReportProcessor";
		public static final String VENDORS_REPORT= "com.avanza.pims.report.processor.VendorsReportProcessor";
		public static final String MAINTENANCE_REQUESTS_REPORT="com.avanza.pims.report.processor.MaintenanceRequestsReportProcessor";
		public static final String VENDOR_OUTSTANDING_PAYMENT_REPORT="com.avanza.pims.report.processor.VendorOutstandingPaymentReportProcessor";
		public static final String ONLINE_PAYMENT_TRANSACTION_REPORT="com.avanza.pims.report.processor.OnlinePaymentTransactionReportProcessor";
		public static final String NOL_REPORT="com.avanza.pims.report.processor.NOLProcessor";
		public static final String CLEARANCE_LETTER_REPORT="com.avanza.pims.report.processor.ClearenceLetterProcessor";
		public static final String SETTLEMENT_REPORT= "com.avanza.pims.report.processor.SettlementReportProcessor";
		public static final String RENEW_CONTRACT_REMINDER_LETTER = "com.avanza.pims.report.processor.RenewContractReminderLetterProcessor";
		public static final String BANK_REMMITANCE_REPORT="com.avanza.pims.report.processor.BankRemittanceReportProcessor";
		public static final String RECEIPT_REPORT="com.avanza.pims.report.processor.ReceiptReportProcessor";
		public static final String FOLLOWUP_REPORT="com.avanza.pims.report.processor.FollowupReportProcessor";
		public static final String DETAILED_INVESTMENT_EVALUATION_REPORT= "com.avanza.pims.report.processor.DetailedInvestmentEvaluationProcessor";
		public static final String PORTFOLIO_DETAILS_REPORT = "com.avanza.pims.report.processor.PortfolioDetailsProcessor";
		public static final String SUMMARY_INVESTMENT_EVALUATION_REPORT= "com.avanza.pims.report.processor.SummaryInvestmentEvaluationProcessor";
		public static final String PROPERTY_OCCUPANCY_SUMMARY_REPORT ="com.avanza.pims.report.processor.PropertyOccupancySummaryProcessor";
		public static final String PROPERTY_OCCUPANCY_DETAILS_REPORT ="com.avanza.pims.report.processor.PropertyOccupancyDetailsProcessor";
		public static final String PROPERTY_EVALUATION_REPORT = "com.avanza.pims.report.processor.PropertyEvaluationProcessor";
		public static final String UNIT_EVALUATION_REPORT = "com.avanza.pims.report.processor.UnitEvaluationProcessor";
		public static final String STATEMENT_OF_ACCOUNT = "com.avanza.pims.report.processor.StatementOfAccountProcessor";
		public static final String STATEMENT_OF_ACCOUNT_NEW = "com.avanza.pims.report.processor.StatementOfAccountNewProcessor";
		public static final String STATEMENT_OF_ACCOUNT_ENDOWMENT = "com.avanza.pims.report.processor.StatementOfAccountEndowmentProcessor";
		public static final String STATEMENT_OF_ACCOUNT_MASRAF = "com.avanza.pims.report.processor.StatementOfAccountMasrafProcessor";
		public static final String ALL_MASRAF_BALANCE_SUMMARY = "com.avanza.pims.report.processor.AllMasrafBalanceSummaryProcessor";
		public static final String STATMENT_OF_ACCOUNT_MASRAF_BENEFICIARY = "com.avanza.pims.report.processor.StatementOfAccountMasrafBeneficiaryProcessor";
		
		public static final String WAQIF_BALANCE_DETAILS = "com.avanza.pims.report.processor.StatementOfAccountWaqifProcessor";
		public static final String ENDOWMENT_YEARLY_REPORT = "com.avanza.pims.report.processor.EndowmentYearlyReportProcessor";
		
		public static final String FILE_DEETAILS = "com.avanza.pims.report.processor.FileDetailsReportProcessor";
		public static final String INHERITANCE_DETAILS = "com.avanza.pims.report.processor.InheritanceDetailsReportProcessor";
		public static final String CONTRACT_PAYMENTS = "com.avanza.pims.report.processor.ContractPaymentReportProcessor";
		public static final String PENDING_PAYMENTS_AFTER_SETTLEMENT_PROCESSOR = "com.avanza.pims.report.processor.PendingPaymentsAfterSettlementProcessor";		

	}
	
	public class POJOReportsDataSourceConstants
	{
		public static final String  SAMPLE_PROPERTY_DETAIL_ALIAS_PROPERTY = "PropertyView";
		public static final String  SAMPLE_PROPERTY_DETAIL_QUALIFIED_NAME_PROPERTY = "PropertyView";
		public static final String  SAMPLE_PROPERTY_DETAIL_ALIAS_FLOOR = "FloorView";
		public static final String  SAMPLE_PROPERTY_DETAIL_QUALIFIED_NAME_FLOOR = "FloorView";
	}
	
	public class DataDef {
		
		public static final String AUCTION_BIDDER_REPORT="AuctionReportDataDef";
		public static final String CHEQUE_WITHDRAWL_REPORT="ChequeWithdrawlReportDataDef";
		public static final String CHEQUE_WITHDRAWL_REPORT_LABEL="ChequeWithdrawlReportLabelDataDef";
		public static final String REQUEST_DETAILS_REPORT="RequesttDetailsReportDataDef";
		public static final String PRINT_LEASE_CONTRACT = "PrintLeaseContractReportDataDef";
		public static final String CONTRACT_DISCLOSURE = "ContractDisclosureReportDataDef";
		public static final String CONTRACT_DISCLOSURE_LABEL = "ContractDisclosureReportLabelDataDef";
		public static final String PRINT_LEASE_CONTRACT_PAYMENT_SCHEDULE = "PrintLeaseContractPaymentScheduleDataDef";
		public static final String PRINT_LEASE_CONTRACT_LABEL = "PrintLeaseContractReportLabelDataDef";
		public static final String PRINT_LEASE_CONTRACT_PARAMETER = "PrintLeaseContractParameterDataDef";

		/*******************************                           Data Defs             *********************************/
		
		public static final String  UNIT_INQUIRY_REPORT_DATADEF = "UnitInquiryReportDataDef";
		public static final String  RENEW_CONTRACTS_SUMMARY = "RenewContractSummaryReportDataDef";
		public static final String  PROPERTY_DETAIL= "PropertyDetailReportDataDef";
		public static final String  ADVERTISEMENT_REPORT= "AdvertisementDataDef";
		public static final String  PROPERTY_UNIT_LIST= "PropertyUnitListReportDataDef";
		public static final String  PROPERTY_TENANTS_LIST= "PropertyTenantsListReportDataDef";
		public static final String CHEQUE_DETAILS_LIST= "ChequeDetailsListReportDataDef";
		public static final String COLLECTION_REPORT="CollectionReportDataDef";
		public static final String RECEIPT_VOUCHERS_REPORT="ReceiptVouchersReportDataDef";
		public static final String APPLICATION_DETAILS_REPORT="ApplicationDetailsReportDateDef"; 
		public static final String TENANT_STATEMENT_REPORT="TenantStatementReportDataDef";
		public static final String TENANT_VIOLATIONS_REPORT="TenantViolationsReportDataDef";
		public static final String TENANT_DETAILS_REPORT="TenantDetailsReportDataDef";
		public static final String TENANT_DETAILS_REPORT_EXT="TenantDetailsReportExtDataDef";
		public static final String CONTRACT_PAYMENT_DETAIL = "ContractPaymentDetailReportDataDef";
		public static final String CONTRACT_RENEWAL_HISTORY = "ContractRenewalHistoryReportDataDef";
		public static final String CONTRACT_RENEWAL_HISTORY_SUBREPORT = "ContractRenewalHistorySubReportDataDef";		
		public static final String MAINTENANCE_CONTRACTS = "MaintenanceContractsReportDataDef";
		public static final String LEASE_CONTRACTS_SUMMARY = "LeaseContractsSummaryReportDataDef";
		public static final String LEASE_CONTRACTS_DETAIL = "LeaseContractsDetailReportDataDef";
		public static final String TENANT_VIOLATION = "TenantViolationsReportDataDef";
		public static final String PAYMENT_RECEIPT_REPORT="PaymentReceiptReportDataDef";
		public static final String MEMS_PAYMENT_RECEIPT_REPORT="MEMSPaymentReceiptReportDataDef";
		public static final String TENANT_STATEMENT="TenantStatementReportDataDef";
		public static final String APPLICATION_SUMMARY_REPORT="ApplicationSummaryReportDateDef"; 
		public static final String BOUNCED_CHECKS_DETAILS ="BouncedChecksDetailsReportDataDef";
		public static final String PAYMENT_SUMMARY="PaymentSummaryReportDataDef";
		public static final String PAYMENTS_DUE_DATES="PaymentsDueDatesReportDataDef";
		public static final String PAYMENT_HISTORY="PaymentHistoryReportDataDef";
		public static final String UNITS_PER_TENANTS="UnitsPerTenantsReportDataDef";
		public static final String DUE_PAYMENTS_PER_TENANT="DuePaymentsPerTenantReportDataDef";
		public static final String TENANT_CONTRACTS_LIST="TenantContractsListReportDataDef";
		public static final String SERVICE_CONTRACTS="ServiceContractsReportDataDef";
		public static final String PROPERTIES_LIST="PropertiesListReportDataDef";
		public static final String TENANTS_BY_CONTRACTS="TenantsByContractsReportDataDef";
		public static final String CONSTRUCTION_PROJECTS_SUMMARY="ConstructionProjectsSummaryReportDataDef";
		public static final String CONSTRUCTION_PROJECTS_DETAIL="ConstructionProjectsDetailReportDataDef";
		public static final String PROJECT_MILESTONES="ProjectsMileStonesReportDataDef";
		public static final String TENANT_CONTRACTS_PAYMENTS="TenantContractsPaymentsReportDataDef";
		public static final String VENDORS_REPORT= "VendorsReportDataDef";
		public static final String MAINTENANCE_REQUESTS_REPORT="MaintenanceRequestReportDataDef";
		public static final String VENDOR_OUTSTANDING_PAYMENT_REPORT= "VendorOutstandingPaymentReportDataDef";
		public static final String ONLINE_PAYMENT_TRANSACTION_REPORT="OnlinePaymentTransactionReportDataDef";
		public static final String SETTLEMENT_REPORT="SettlementReportDataDef";
		public static final String RENEW_CONTRACT_REMINDER_LETTER="RenewContractReminderLetterDataDef";
		public static final String BANK_REMMITANCE_REPORT="BankRemittanceReportDataDef";
		public static final String RECEIPT_REPORT="ReceiptReportDataDef";
		public static final String FOLLOWUP_REPORT="FollowupReportDataDef";
		public static final String PORTFOLIO_DETAILS="PortfolioDetailsDataDef";
		public static final String PROPERTY_OCCUPANCY_SUMMARY="PropertyOccupancySummaryDataDef";
		public static final String PROPERTY_OCCUPANCY_DETAILS="PropertyOccupancyDetailsDataDef";
		public static final String PROPERTY_EVALUATION_REPORT="PropertyEvaluationDataDef";
		public static final String PROPERTY_EVALUATION_UNIT_DETAIL_REPORT="PropertyEvaluationUnitDetailDataDef";
		public static final String UNIT_EVALUATION_REPORT="UnitEvaluationDataDef";
		
		/*******************************                           NOL Reports Data Defs             *********************************/
		public static final String NOL_REPORT="NOLDataDef";
		public static final String NOL_COMM_ACT_REPORT="NOLCommActDataDef";
		public static final String CLEARENCE_LETTER_REPORT="ClearenceLetterDataDef";
		public static final String ADDING_WATER_METER_NOL="AddingWaterMeterNOLDataDef";
		/*******************************                           Label Data Defs             *********************************/
		public static final String AUCTION_BIDDER_LABEL_REPORT="AuctionReportLabelDataDef";
		public static final String PROPERTY_DETAIL_LABEL = "PropertyDetailReportLabelDataDef";
		public static final String PROPERTY_UNIT_LIST_LABEL = "PropertyUnitListReportLabelDataDef";
		public static final String  PROPERTY_TENANTS_LIST_LABEL= "PropertyTenantsListReportLabelDataDef";
		public static final String CHEQUE_DETAILS_LIST_LABEL = "ChequeDetailsListReportLabelDataDef";
		public static final String COLLECTION_REPORT_LABEL="CollectionReportLabelDataDef";
		public static final String RECEIPT_VOUCHERS_REPORT_LABEL="ReceiptVouchersReportLabelDataDef";
		public static final String TENANT_STATEMENT_REPORT_LABEL="TenantStatementReportLabelDataDef";
		public static final String APPLICATION_DETAILS_REPORT_LABEL="ApplicationDetailsReportLabelDataDef"; 
		public static final String TENANT_VIOLATIONS_REPORT_LABEL="TenantViolationsReportLabelDataDef";
		public static final String TENANT_DETAILS_REPORT_LABEL="TenantDetailsReportLabelDataDef";
		public static final String CONTRACT_PAYMENT_DETAIL_LABEL = "ContractPaymentDetailReportLabelDataDef";
		public static final String CONTRACT_RENEWAL_HISTORY_LABEL  = "ContractRenewalHistoryReportLabelDataDef";
		public static final String MAINTENANCE_CONTRACTS_LABEL  = "MaintenanceContractsReportLabelDataDef";
		public static final String LEASE_CONTRACTS_SUMMARY_LABEL  = "LeaseContractsSummaryReportLabelDataDef";
		public static final String LEASE_CONTRACTS_DETAIL_LABEL  = "LeaseContractsDetailReportLabelDataDef";
		public static final String TENANT_VIOLATION_LABEL  = "TenantViolationsReportLabelDataDef";
		public static final String TENANT_STATEMENT_LABEL="TenantStatementReportLabelDataDef";
		public static final String PAYMENT_RECEIPT_REPORT_LABEL="PaymentReceiptReportLabelDataDef";
		public static final String MEMS_PAYMENT_RECEIPT_REPORT_LABEL="MEMSPaymentReceiptReportLabelDataDef";
		public static final String APPLICATION_SUMMARY_REPORT_LABEL="ApplicationSummaryReportLabelDataDef"; 
		public static final String BOUNCED_CHECKS_DETAILS_LABEL ="BouncedChecksDetailsReportLabelDataDef";
		public static final String PAYMENT_SUMMARY_LABEL ="PaymentSummaryReportLabelDataDef";
		public static final String PAYMENTS_DUE_DATES_LABEL="PaymentsDueDatesReportLabelDataDef";
		public static final String PAYMENT_HISTORY_LABEL="PaymentHistoryReportLabelDataDef";
		public static final String UNITS_PER_TENANTS_LABEL="UnitsPerTenantsReportLabelDataDef";
		public static final String DUE_PAYMENTS_PER_TENANT_LABEL="DuePaymentsPerTenantReportLabelDataDef";
		public static final String TENANT_CONTRACTS_LIST_LABEL="TenantContractsListReportLabelDataDef";
		public static final String SERVICE_CONTRACTS_LABEL="ServiceContractsReportLabelDataDef";
		public static final String PROPERTIES_LIST_LABEL="PropertiesListReportLabelDataDef";
		public static final String TENANTS_BY_CONTRACTS_LABEL="TenantsByContractsReportLabelDataDef";
		public static final String CONSTRUCTION_PROJECTS_SUMMARY_LABEL="ConstructionProjectsSummaryReportLabelDataDef";
		public static final String CONSTRUCTION_PROJECTS_DETAIL_LABEL="ConstructionProjectsDetailReportLabelDataDef";
		public static final String PROJECT_MILESTONES_LABEL="ProjectsMileStonesReportLabelDataDef";
		public static final String TENANT_CONTRACTS_PAYMENTS_LABEL="TenantContractsPaymentsReportLabelDataDef";
		public static final String VENDORS_REPORT_LABEL= "VendorsReportLabelDataDef";
		public static final String MAINTENANCE_REQUESTS_REPORT_LABEL="MaintenanceRequestReportLabelDataDef";		
		public static final String VENDOR_OUTSTANDING_PAYMENT_REPORT_LABEL= "VendorOutstandingPaymentReportLabelDataDef";
		public static final String ONLINE_PAYMENT_TRANSACTION_REPORT_LABEL="OnlinePaymentTransactionReportLabelDataDef";
		public static final String SETTLEMENT_REPORT_LABEL="SettlementReportLabelDataDef";
		public static final String SETTLEMENT_REPORT_CALCULATIONS="SettlementReportCalculationsDataDef";
		public static final String SETTLEMENT_REPORT_RENT_TABLE="SettlementReportRentTableDataDef";
		public static final String SETTLEMENT_REPORT_OTHER_TABLE="SettlementReportOtherTableDataDef";
		public static final String SETTLEMENT_REPORT_CHEQUES_TABLE="SettlementReportChequesTableaDef";	
		public static final String SETTLEMENT_REPORT_PAYMENTS_TABLE="SettlementReportPaymentsDataDef";
		public static final String BANK_REMMITANCE_REPORT_LABEL="BankRemittanceReportLabelDataDef";
		public static final String RECEIPT_REPORT_LABEL="ReceiptReportLabelDataDef";
		public static final String FOLLOWUP_REPORT_LABEL="FollowupReportLabelDataDef";
		public static final String PORTFOLIO_REPORT_LABEL="PortfolioReportLabelDataDef";
		public static final String PROPERTY_OCCUPANCY_SUMMARY_LABEL="PropertyOccupancySummaryLabelDataDef";
		public static final String PROPERTY_OCCUPANCY_DETAILS_LABEL="PropertyOccupancyDetailsLabelDataDef";
		public static final String STATEMENT_OF_ACCOUNT="StatementOfAccountDataDef";
		public static final String STATEMENT_OF_ACCOUNT_LABEL="StatementOfAccountLabelDataDef";
		public static final String FILE_DETAILS="FileDetailsReportDataDef";
		public static final String FILE_DETAILS_BENEFICIARY="FileDetailsReportBeneficiaryDataDef";
		public static final String FILE_DETAILS_ASSET="FileDetailsReportAssetDataDef";
		public static final String FILE_DETAILS_SHARES="FileDetailsReportShareDataDef";
		public static final String FILE_DETAILS_LABEL="FileDetailsReportLabelDataDef";
		public static final String INHERITANCE_DETAILS="InheritanceDetailsDataDef";
		public static final String LICENSE_DETAILS="InheritanceDetailsLicenceDataDef";
		public static final String BANK_TRANSFER_DETAILS="InheritanceDetailsBankTrDataDef";
		public static final String LAND_AND_PROPERTIES_DETAILS="InheritanceDetailsLandDataDef";
		public static final String PENSION_DETAILS="InheritanceDetailsPensionDataDef";
		public static final String ANIMAL_DETAILS="InheritanceDetailsAnimalDataDef";
		public static final String TRANSPORTATION_DETAILS="InheritanceDetailsTransportationDataDef";
		public static final String JEWELRY_DETAILS="InheritanceDetailsJewelryDataDef";
		public static final String CASH_DETAILS="InheritanceDetailsCashDataDef";
		public static final String CHEQUE_DETAILS="InheritanceDetailsChequeDataDef";
		public static final String VEHICLE_DETAILS="InheritanceDetailsVehicleDataDef";
		public static final String STOCK_SHARE_DETAILS="InheritanceDetailsStockShareDataDef";
		public static final String LOAN_AND_LIABILITIES_DETAILS="InheritanceDetailsLoanLiabilitiesDataDef";
		public static final String INHERITANCE_DETAILS_LABEL="InheritanceDetailsReportLabelDataDef";
		public static final String INHERITANCE_DETAILS_ASSET_LABEL="InheritanceDetailsAssetsLabelDataDef";
		public static final String INHERITANCE_DETAILS_LAND_LABEL="InheritanceDetailsLandAndPropertiesLabelDataDef";
		public static final String INHERITANCE_DETAILS_BANK_TRANSFER_LABEL="InheritanceDetailsBankTransferLabelDataDef";
		public static final String INHERITANCE_DETAILS_LICENSES_LABEL="InheritanceDetailsLicensesLabelDataDef";
		public static final String INHERITANCE_DETAILS_ANIMAL_LABEL="InheritanceDetailsAnimalLabelDataDef";
		public static final String INHERITANCE_DETAILS_CASH_LABEL="InheritanceDetailsCashLabelDataDef";
		public static final String INHERITANCE_DETAILS_CHEQUE_LABEL="InheritanceDetailsChequeLabelDataDef";
		public static final String INHERITANCE_DETAILS_LOAN_LIABILITY_LABEL="InheritanceDetailsLoanLiabilityLabelDataDef";
		public static final String INHERITANCE_DETAILS_STOCK_SHARE_LABEL="InheritanceDetailsStockShareLabelDataDef";
		public static final String INHERITANCE_DETAILS_PENSION_LABEL="InheritanceDetailsPensionLabelDataDef";
		public static final String INHERITANCE_DETAILS_VEHICLE_LABEL="InheritanceDetailsVehicleLabelDataDef";
		public static final String INHERITANCE_DETAILS_JEWELRY_LABEL="InheritanceDetailsJewelryLabelDataDef";
		public static final String INHERITANCE_DETAILS_TRANSPORTATION_LABEL="InheritanceDetailsTransportationLabelDataDef";
		public static final String HOS_RECOMMENDATION_SUBREPORT = "HosRecommendationSubReportDataDef";
		public static final String RENT_COMMITTE_RECOMMENDATION_SUBREPORT = "RentCommitteRecommendationSubReportDataDef";
		public static final String PENDING_PAYMENTS_AFTER_SETTLEMENT = "PendingPaymentsAfterSettlementDataDef";
		public static final String PENDING_PAYMENTS_AFTER_SETTLEMENT_LABEL = "PendingPaymentsAfterSettlementLabelDataDef";
		public static final String UNIT_INQUIRY_REPORT_LABEL = "UnitInquiryReportLabelDataDef";
		public static final String RENEW_CONTRACTS_SUMMARY_LABEL = "RenewContractSummaryReportLabelDataDef";
		public static final String RequestTaskActivityDataDef ="RequestTaskActivityDataDef";
	}
	

}
