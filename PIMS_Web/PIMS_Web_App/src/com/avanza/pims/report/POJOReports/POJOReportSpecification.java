package com.avanza.pims.report.POJOReports;

import java.util.ArrayList;
import java.util.List;


public class POJOReportSpecification 
{
	private String reportFileName;
	private String reportGeneratedBy;
	private List<POJOReportDataSource> pojoReportDataSourceList = new ArrayList<POJOReportDataSource>(0) ;
	
	public POJOReportSpecification(String reportFileName,String reportGeneratedBy)
	{
		this.reportFileName = reportFileName;
		this.reportGeneratedBy = reportGeneratedBy;
	}
	public String getReportFileName() {
		return reportFileName;
	}
	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}
	public String getReportGeneratedBy() {
		return reportGeneratedBy;
	}
	public void setReportGeneratedBy(String reportGeneratedBy) {
		this.reportGeneratedBy = reportGeneratedBy;
	}	
	public List<POJOReportDataSource> getPojoReportDataSourceList() {
		return pojoReportDataSourceList;
	}
	public void addPojoReportDataSource(POJOReportDataSource pojoReportDataSource) 
	{
		this.pojoReportDataSourceList.add(pojoReportDataSource);
	}

}
