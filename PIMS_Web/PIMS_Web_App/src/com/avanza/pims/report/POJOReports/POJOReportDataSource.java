package com.avanza.pims.report.POJOReports;
import java.util.Collection;

public class POJOReportDataSource 
{
	private String dataSourceAlias;
	private String dataSourceQualifiedName;
	private Class dataSourceType;
	private Collection data;
	
	public POJOReportDataSource(String dataSourceAlias,String dataSourceQualifiedName,Class dataSourceType,Collection data)
	{
		this.dataSourceAlias = dataSourceAlias;
		this.dataSourceQualifiedName =dataSourceQualifiedName;
		this.dataSourceType = dataSourceType;
		this.data = data;
	}
	public String getDataSourceAlias() {
		return dataSourceAlias;
	}
	public void setDataSourceAlias(String dataSourceAlias) {
		this.dataSourceAlias = dataSourceAlias;
	}
	public String getDataSourceQualifiedName() {
		return dataSourceQualifiedName;
	}
	public void setDataSourceQualifiedName(String dataSourceQualifiedName) {
		this.dataSourceQualifiedName = dataSourceQualifiedName;
	}
	public Class getDataSourceType() {
		return dataSourceType;
	}
	public void setDataSourceType(Class dataSourceType) {
		this.dataSourceType = dataSourceType;
	}
	public Collection getData() {
		return data;
	}
	public void setData(Collection data) {
		this.data = data;
	}
	
	
}
