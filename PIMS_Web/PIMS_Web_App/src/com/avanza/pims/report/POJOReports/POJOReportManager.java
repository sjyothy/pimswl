package com.avanza.pims.report.POJOReports;

import java.io.File;
import java.util.List;

import com.avanza.pims.report.constant.ReportConstant;
import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;
import com.crystaldecisions.sdk.occa.report.reportsource.IReportSource;
import com.crystaldecisions.reports.sdk.DatabaseController;

public class POJOReportManager 
{
	private ReportClientDocument reportClientDocument;
	private String reportBaseFolderName;
	private POJOReportSpecification pojoReportSpecification;
	
	public POJOReportManager(POJOReportSpecification pojoReportSpecification) throws Exception 
	{		
		this.pojoReportSpecification = pojoReportSpecification;
		this.reportClientDocument = new ReportClientDocument();
		this.reportBaseFolderName = ReportConstant.BASE_REPORT_FOLDER_NAME;
	}	
	
	public IReportSource getReportSource() throws Exception 
	{		
		reportClientDocument.open(this.reportBaseFolderName + File.separator + this.pojoReportSpecification.getReportFileName(), OpenReportOptions._openAsReadOnly);
		assignReportDataSource();
		return reportClientDocument.getReportSource();
	}
	
	private void assignReportDataSource() throws ReportSDKException
	{
		List<POJOReportDataSource> pojoReportDataSourceList  = pojoReportSpecification.getPojoReportDataSourceList();
		DatabaseController databaseController = reportClientDocument.getDatabaseController();
		for(POJOReportDataSource pojoReportDataSource : pojoReportDataSourceList )
		{
			databaseController.setDataSource(pojoReportDataSource.getData(), pojoReportDataSource.getDataSourceType(), pojoReportDataSource.getDataSourceAlias(), pojoReportDataSource.getDataSourceAlias());
		}
		
	}

}
