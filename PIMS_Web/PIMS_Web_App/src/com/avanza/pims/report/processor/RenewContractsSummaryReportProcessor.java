package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class RenewContractsSummaryReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		LeaseContractsSummaryReportCriteria criteria = (LeaseContractsSummaryReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = "SELECT DISTINCT " +
							" CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
							"CAST( to_char(CNTRT.CONTRACT_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractDate\" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							"U.UNIT_NUMBER \"unitNo\" , " +
							"PRSN.FIRST_NAME \"tenantFirstName\" , " +
							"PRSN.MIDDLE_NAME \"tenantMiddleName\" , " +
							"PRSN.LAST_NAME \"tenantLastName\" , " +
							"PRSN.COMPANY_NAME," +
							"PRSN2.FIRST_NAME \"occupantFirstName\" , " +
							"PRSN2.LAST_NAME \"occupantLastName\" , " +
							"DD2.DATA_DESC_EN  \"statusEn\" , " +
							"DD2.DATA_DESC_AR  \"statusAr\" " +
							
							
							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
					         " INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
					         " INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
					         " INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
					         " INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
					         " INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
					         " INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
					         " INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
					         " INNER JOIN DOMAIN_DATA DD2 ON CNTRT.STATUS_ID = DD2.DOMAIN_DATA_ID " +
					         " LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
					         " ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
					         " LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD " +
					         " ON PRCD.PAYMENT_RECEIPT_DETAIL_ID = PRC.PAYMENT_RECEIPT_DETAIL_ID " +
					         " LEFT OUTER JOIN PAYMENT_METHOD PMT " +
					         " ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
					         " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
					         " LEFT JOIN CONTRACT_UNIT_OCCUPIER CUO " +
					         " ON CUO.CONTRACT_UNIT_ID = CU.CONTRACT_UNIT_ID " +
					         " LEFT OUTER JOIN PERSON PRSN2 ON CUO.PERSON_ID = PRSN2.PERSON_ID " +
					         " LEFT OUTER JOIN PAYMENT_RECEIPT PRCT " +
					         " ON PRCT.PAYMENT_RECEIPT_ID = PRCD.PAYMENT_RECEIPT_DETAIL_ID " +
					         " INNER JOIN DOMAIN_DATA DD_PS ON PS.STATUS_ID = DD_PS.DOMAIN_DATA_ID " ;	
		
					          
				String condition="";
				
							if(criteria.getContractNumber()!= null && criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
					
							if(criteria.getContractId()!= null && criteria.getContractId().length() >0)
								condition+=" AND (CNTRT.CONTRACT_ID) = " + criteria.getContractId();
							
							if(criteria.getTenantId()!= null && criteria.getTenantId().length() >0)
								condition+=" AND (CNTRT.TENANT_ID) = " + criteria.getTenantId();
							
							if(criteria.getPropertyId()!= null && criteria.getPropertyId().length() >0)
								condition+=" AND (CNTRT.PROPERTY_ID) = " + criteria.getPropertyId();							
							
							
							/*		if(criteria.getTenantName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
								" ) " ;
							if(criteria.getPropertyComercialName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyComercialName() +"%')";
							if(criteria.getUnitNum().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNum() +"%')";
							if(criteria.getFromContractDateRich()!=null  && criteria.getToContractDateRich()!=null)								
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() )+"', 'dd/MM/yyyy')";
							if(criteria.getFromContractExpDateRich()!=null  && criteria.getToContractExpDateRich()!=null)
								condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() )+"', 'dd/MM/yyyy')";
							if(!criteria.getContractType().equals("-1"))
								condition+= " AND CNTRT.CONTRACT_TYPE_ID  =  '" + criteria.getContractType() +"'"	;
							if(!criteria.getContractStatus().equals("-1"))
								condition+= " AND CNTRT.STATUS_ID   =  " + criteria.getContractStatus();*/
					
							if(condition.length() > 0)
					        	query+="where 1=1 \n"+ condition;

				String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.RENEW_CONTRACTS_SUMMARY;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractDate") + "' \"lblContractDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.occupantName") + "' \"lblOccupantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblStatusEn\",\n" + 
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractDate") + "' \"lblContractDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.occupantName") + "' \"lblOccupantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblStatusAr\",\n" + 

		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.renewContractsSummary.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.renewContractsSummary.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.RENEW_CONTRACTS_SUMMARY_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
