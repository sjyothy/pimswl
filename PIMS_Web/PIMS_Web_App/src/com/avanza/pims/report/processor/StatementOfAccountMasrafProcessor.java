package com.avanza.pims.report.processor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.avanza.core.util.Logger;
import com.avanza.pims.dao.MasarifManager;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Masraf;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class StatementOfAccountMasrafProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{
			String masarifIdCSV = "";
			if( 
				criteria.getShowSubMasrafTrx() != null &&  
				criteria.getShowSubMasrafTrx().equals("1") 
			  )
			{
				masarifIdCSV = getSubMasarifIds(  criteria.getMasrafId() );
			}
			Double openingBalance  = executeMasrafDetailsQuery(criteria,reportQueryMap,masarifIdCSV );
			executeTransactionQuery(criteria,reportQueryMap,masarifIdCSV ,openingBalance  );
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}
	
	/**
	 * @param criteria
	 * @return
	 */
	private String getSubMasarifIds( Long masrafId )throws Exception
	{
		MasarifManager mgr = new MasarifManager();
		StringBuilder csv = new StringBuilder("");
		List<Masraf> list =  mgr.getAllChildMasarif( masrafId );
		for (Masraf obj: list) 
		{
			csv.append( obj.getMasrafId() ).append( "," );
		}
		String cc=csv.toString();
		if(cc.length() > 0 && cc.contains(","))
		{
			cc= cc.substring(0, (cc.length()-1) );
		}
		return cc;
	}
	
	
	@SuppressWarnings( "unchecked" )
	private Double executeMasrafDetailsQuery(
											StatementOfAccountCriteria criteria, 
											Map<String, String> reportQueryMap,
											String masarifIdCSV  
										  )throws Exception 
	{
		Masraf masraf = EntityManager.getBroker().findById(Masraf.class, criteria.getMasrafId()  );
		
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		Double openingBalance    = 0.00d;
		Double closingBalance    = 0.00d;
		Double closingBalanceMasraf    = 0.00d;
		Double openingBalanceMasraf    = 0.00d;
		Double openingBalanceSubMasarif    = 0.00d;
		Double closingBalanceSubMasarif    = 0.00d;
		
		Double finalBalance = 0.00d;
		
		Session session = (Session)EntityManager.getBroker().getSession();
		Query query = session.getNamedQuery("com.avanza.pims.entity.Masraf.spgetmasrafbalancesummary");
		query.setParameter("fromdate", criteria.getFormattedFromDate());
		query.setParameter("todate", criteria.getFormattedToDate() );
		query.setParameter("masrafId", criteria.getMasrafId());
		query.setParameter("showsubmasraftrx",criteria.getShowSubMasrafTrx());
		if( masarifIdCSV != null )
		{
			query.setParameter("masrafIds", masarifIdCSV);
			
		}
		
		List  list = query.list();
		if( list != null && list.size() > 0 )
		{
			Object[] obj =  (Object[])list.get(0);
			if( obj[0] != null )
			{
				openingBalance = Double.valueOf( obj[0].toString() ) ;
			}
			if( obj[1] != null )
			{
				closingBalance = Double.valueOf( obj[1].toString() ) ;
			}
			if( obj[2] != null )
			{
				openingBalanceMasraf = Double.valueOf( obj[2].toString() ) ;
			}
			if( obj[3] != null )
			{
				closingBalanceMasraf = Double.valueOf( obj[3].toString() ) ;
			}
			if( obj[4] != null )
			{
				openingBalanceSubMasarif = Double.valueOf( obj[4].toString() ) ;
			}
			if( obj[5] != null )
			{
				closingBalanceSubMasarif = Double.valueOf( obj[5].toString() ) ;
			}
		}
		
		String queryStr  = " select '"+ 
		   							  criteria.getShowSubMasrafTrx()+ "' as \"field1\",'"+
									   criteria.getFormattedFromDateForReportDisplay() + "' as \"fromDate\",'"+
									   criteria.getFormattedToDateForReportDisplay() + "' as \"toDate\",'"+
									   criteria.getMasrafId().toString()+"' as \"masrafId\",'"+
									   masraf.getMasrafName()+"' as \"name\", cast('" +
									   
									   oneDForm.format( closingBalance )+     "'      as DECIMAL(20,2) ) as \"balance\",cast('" +
									   oneDForm.format( openingBalance )+	  "'  	  as DECIMAL(20,2) ) as \"openingbalance\",cast('" +
									   
									   oneDForm.format( openingBalanceMasraf )+"'     as DECIMAL(20,2) ) as \"openingBalanceMasraf\",      cast('" +
									   oneDForm.format( closingBalanceMasraf )+"'     as DECIMAL(20,2) ) as \"closingBalanceMasraf\",      cast('" +

									   oneDForm.format( openingBalanceSubMasarif )+"' as DECIMAL(20,2) ) as \"openingBalanceSubMasarif\", cast('" +
									   oneDForm.format( closingBalanceSubMasarif )+"' as DECIMAL(20,2) ) as \"closingBalanceSubMasarif\", cast('" +
									   
									   oneDForm.format( finalBalance )+            "' as DECIMAL(20,2) ) as \"finalBalance\" " +
						   " from DUAL ";

		System.out.print("-------------" + queryStr);
		String dataDef = "MasrafDetailsDataDef";        
		reportQueryMap.put(dataDef, queryStr+"");
		return openingBalance ;
	}

	private void executeTransactionQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String subMasrafCsv,Double openingBalance  )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		query += " SELECT   "+criteria.getMasrafId() +"\"criteriaMasrafId\"," +
							  openingBalance  +"\"openingBalanceFromMainReport\"," +
				"\"transId\",        \"TRANS_DATE\",     \"trxDate\", \"createdOn\" ,        \"masrafId\"," +
				 "		    \"masrafName\",     \"fromEndowmentId\",\"END_NUM\",  \"fromEndowmentName\"," +
				 "          ( \"END_NUM\" ||' - '||  \"fromEndowmentName\") as \"field1\","+
				 "          \"paidTo\" as \"field2\","+
                 " 			\"fromCostCenter\", \"amount\",         \"credit\",   \"debit\",            \"descriptionAr\","+
                 " 			\"descriptionEn\",  \"trxTypeEn\",       \"trxTypeAr\" " +
                 " FROM 	STATEMENT_OF_ACCOUNT_MASRAF  WHERE 1=1 ";

		if(subMasrafCsv != null && subMasrafCsv.length() > 0 )
		{
			condition.append(" and \"masrafId\" in (").append(  subMasrafCsv).append(",").append(criteria.getMasrafId()  ).append(")");
		}
		else 
		{
//			condition.append(" and \"person_Id\" is null  and \"masrafId\" = ").append(  criteria.getMasrafId() );
			condition.append(" and \"masrafId\" = ").append(  criteria.getMasrafId() );
		}
		
		
		if( criteria.getFromDate() != null )
		{
			condition.append( " and trans_date >= to_date('"+criteria.getFormattedFromDate()+"' , 'dd/MM/yyyy hh24:MI:ss')");
		}
		if( criteria.getToDate() != null )
		{
			condition.append( " and trans_date <= to_date('"+ criteria.getFormattedToDate()+"' , 'dd/MM/yyyy hh24:MI:ss')");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "MasrafTransDetailsDataDef#MasrafTransDetails";        
		reportQueryMap.put(dataDef, query+"");
	
	}
		
	}
