package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.TenantContractsPaymentsReportCriteria;
import com.avanza.pims.report.criteria.TenantDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantContractsPaymentsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		TenantContractsPaymentsReportCriteria criteria = (TenantContractsPaymentsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		//////////////tenant details//////////////////
		String query=   " SELECT "+
						" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME ||' ' ||PRSN.LAST_NAME \"tenantName\",\n"+
				        " PRSN.CELL_NUMBER \"cellNumber\" ,\n"+
				        " CNTRT.CONTRACT_NUMBER  \"contractNumber\",\n" +
				        " CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
						" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
						" CNTRT.RENT_AMOUNT  \"contractRentValue\",\n" +
						" DD_CT.DATA_DESC_EN  \"contractTypeEn\" , \n" +
						" DD_CT.DATA_DESC_AR  \"contractTypeAr\", \n" +
						" DD_CS.DATA_DESC_EN  \"statusEn\" , \n" +
						" DD_CS.DATA_DESC_AR  \"statusAr\" ,\n" +
						" PROP.COMMERCIAL_NAME \"propertyName\",\n"+
						" nvl(U.UNIT_NUMBER, '') || ' | ' || nvl(U.ACCOUNT_NUMBER,'') \"unitNumber\" , " +
						" PS.PAYMENT_NUMBER \"paymentNumber\",\n"+
						" PYT.DESCRIPTION_EN \"paymentTypeEn\",\n"+
						" PYT.DESCRIPTION_AR \"paymentTypeAr\",\n"+
						" PRSN.SOCIAL_SEC_NUMBER \"tenantNumber\", \n"+
						 " PRSN.IS_COMPANY \"tenantTypeEn\",\n"+
					     " PRSN.IS_COMPANY \"tenantTypeAr\",\n"+
					     " PRSN.PERSON_ID \"tenantId\",\n"+
					     
				       " PRSN.PERSON_TYPE \n,"+
						
						
						" CAST(to_char(PS.PAYMENT_DUE_ON,'dd/mm/yyyy')As VARCHAR(20)) \"paymentDate\",\n"+
						" PYTMTHD.DESCRIPTION \"paymentMethod\",\n"+
						" PS.AMOUNT  \"paymentAmount\",\n"+
						" PRCT.RECEIPT_NUMBER \"receiptNumber\",\n"+
						" CAST(to_char(PRCT.TRANSACTION_DATE,'dd/mm/yyyy')As VARCHAR(20)) \"receiptDate\",\n"+
						" DD_RS.DATA_DESC_EN  \"receiptStatusEn\" , " +   // this field related to Payment Status(receipt status treat it here as paymentStatus) 
						" DD_RS.DATA_DESC_AR  \"receiptStatusAr\", " +	
						" PRCD.METHOD_REF_NO \"chequeNumber\",\n"+
	                    " CAST(to_char(PRCD.METHOD_REF_DATE,'dd/mm/yyyy')As VARCHAR(20)) \"chequeDate\",\n"+
	                    " PRCD.AMOUNT \"chequeAmount\",\n"+
	                    " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_EN end \"BankNameEn\",\n"+
                        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_AR end \"BankNameAr\",\n"+
                        " PRCD.CHEQUE_OWNER \"owner\"\n"+
                        
                        ///FROM Clause///////////
                        
                        " FROM  CONTRACT CNTRT  "+
                        " INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID \n"+
                        " INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID \n"+
                        " INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID \n" +
                        " INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID \n"+
                        " INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID \n"+
                        " INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID \n"+
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID  \n"+
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID \n"+
                        " LEFT JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID \n"+
                        " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID \n"+
                        " INNER JOIN PAYMENT_RECEIPT PRCT  ON PRCD.PAYMENT_RECEIPT_ID = PRCT.PAYMENT_RECEIPT_ID \n"+ 
//						" INNER JOIN DOMAIN_DATA DD_POT ON PROP.OWNERSHIP_TYPE_ID = DD_POT.DOMAIN_DATA_ID \n" +
                        " INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID \n"+
						" INNER JOIN DOMAIN_DATA DD_RS ON PS.STATUS_ID=DD_RS.DOMAIN_DATA_ID \n" +
						" LEFT JOIN PAYMENT_METHOD PYTMTHD ON PRCD.PAYMENT_METHOD_ID=PYTMTHD.PAYMENT_METHOD_ID \n" +
						" INNER JOIN DOMAIN_DATA DD_CS ON CNTRT.STATUS_ID=DD_CS.DOMAIN_DATA_ID \n" +
						" INNER JOIN DOMAIN_DATA DD_CT ON CNTRT.CONTRACT_TYPE_ID=DD_CT.DOMAIN_DATA_ID \n" +
						" LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;
						
						
                        
                   
       	String condition="";
		if(criteria.getContractNumber().length() >0)
			condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
		
		if(criteria.getTenantName() .length() >0)
			condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
		/*
			condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
							" ) " ;*/
		if(criteria.getPropertyName().length() >0)
			condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
		if(criteria.getContractStartDateFrom()!=null  && criteria.getContractStartDateTo()!=null)								
			condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractStartDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getContractStartDateTo())+"', 'dd/MM/yyyy')";
		if(criteria.getContactExpiryDateFrom()!=null  && criteria.getContactExpiryDateTo()!=null)
			condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContactExpiryDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getContactExpiryDateTo())+"', 'dd/MM/yyyy')";
//		if(criteria.getTenantId().length()>0)
//			condition+= " AND PRSN.PERSONAL_SEC_CARD_NO like '%" + criteria.getTenantId() +"%'"	;
		if(!criteria.getOwnershipType().equals("-1"))
			condition+= " AND PROP.OWNERSHIP_TYPE_ID ='" + criteria.getOwnershipType()+"'"	;
		if(criteria.getUnitNumber().length() >0)
			condition+=" AND lower(U.UNIT_NUMBER)  like  lower('%" + criteria.getUnitNumber() +"%')";
		if(!criteria.getContractStatus().equals("-1"))
			condition+= " AND CNTRT.STATUS_ID  =  " + criteria.getContractStatus();
		if(criteria.getTenantId().length() >0)
			condition+=" AND PRSN.SOCIAL_SEC_NUMBER = '" + criteria.getTenantId()+"'" ;
		if(!criteria.getTenantType().equals("-1"))
		{
			String tenantTypeCriteria;
			if(criteria.getTenantType().equals("26002"))
				tenantTypeCriteria="0";
			else 
				tenantTypeCriteria="1";
			condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
		}	
		if(criteria.getTxtCostCenter().length() >0)
			condition+=" AND lower(U.ACCOUNT_NUMBER)  like  lower('%" + criteria.getTxtCostCenter() +"%')";
				
        
       if(condition.length() > 0)
		      	query+="WHERE 1=1 \n"+ condition;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.TENANT_CONTRACTS_PAYMENTS;        
		reportQueryMap.put(dataDef, query+"");
		
		  // /////////////LABELS////////////////////////////////
        
		
		query = "SELECT \n" + 
               
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantNo") + "' \"lblTenantNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.type") + "' \"lblTenantTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.mobileNumber") + "' \"lblCellNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.contractNo") + "' \"lblContractNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.startDate") + "' \"lblContractStartDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.tab.unit.rent") + "' \"lblContractRentValueEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractType") + "' \"lblContractTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractStatus") + "' \"lblStatusEn\",\n" +	
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.unitNo") + "' \"lblUnitNumberEn\",\n" +
  		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentNumber") + "' \"lblPaymentNumberEn\",\n" +	
  		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" +  		        
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.message.paymentAmount") + "' \"lblPaymentAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.ReceiptNumber") + "' \"lblReceiptNumberEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.amount") + "' \"lblReceiptAmountEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.status") + "' \"lblReceiptStatusEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.chequeNo") + "' \"lblChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("payment.amount") + "' \"lblChequeAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeType") + "' \"lblChequeTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.company") + "' \"lblCompanyEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.individual") + "' \"lblIndividualEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.bank") + "' \"lblBankNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("person.owner") + "' \"lblOwnerEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.total") + "' \"lblTotalEn\",\n" +
		        

		     		  
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantNo") + "' \"lblTenantNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.type") + "' \"lblTenantTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.mobileNumber") + "' \"lblCellNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.contractNo") + "' \"lblContractNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.startDate") + "' \"lblContractStartDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractType") + "' \"lblContractTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractStatus") + "' \"lblStatusAr\",\n" +	
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.tab.unit.rent") + "' \"lblContractRentValueAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.unitNo") + "' \"lblUnitNumberAr\",\n" +
  		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentNumber") + "' \"lblPaymentNumberAr\",\n" +	
  		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" +  		        
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.message.paymentAmount") + "' \"lblPaymentAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.ReceiptNumber") + "' \"lblReceiptNumberAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.amount") + "' \"lblReceiptAmountAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.status") + "' \"lblReceiptStatusAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.chequeNo") + "' \"lblChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("payment.amount") + "' \"lblChequeAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.company") + "' \"lblCompanyAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.individual") + "' \"lblIndividualAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeType") + "' \"lblChequeTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.bank") + "' \"lblBankNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("person.owner") + "' \"lblOwnerAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.total") + "' \"lblTotalAr\",\n" +

		     
		       

		        

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantContractsPaymentsReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantContractsPaymentsReport.rpt") + "' \"lblReportNameAr\" ,\n" + 
		         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.TENANT_CONTRACTS_PAYMENTS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
