package com.avanza.pims.report.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.ReceiptReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.collaxa.cube.xml.xpath.functions.xml.GetChildElementFunction;

public class ReceiptReportProcessor extends AbstractReportProcessor {
@SuppressWarnings( "unchecked" )
		List<DomainDataView> ddvChequeTypeList=new ArrayList<DomainDataView>();
		List<PaymentMethodView> paymentMethodItems=new ArrayList<PaymentMethodView>();
		List<SelectItem> paymentTypeItems=new ArrayList<SelectItem>();
		List<SelectItem> userNameList=new ArrayList<SelectItem>();
		long cancelledReceitps = 0;
		public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		ReceiptReportCriteria criteria = (ReceiptReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);

		try
		{
		ddvChequeTypeList=CommonUtil.getDomainDataListForDomainType(WebConstants.CHEQUE_TYPE);
		paymentMethodItems =	new UtilityServiceAgent().getAllPaymentMethods();
		paymentTypeItems =	new ApplicationBean().getPaymentTypeList();
		userNameList= new ApplicationBean().getUserNameList();		
		}
		catch( Exception e)
		{
			
		}
		////////////////////////////////
		String receiptStatusCondition = "";
		
		if(criteria.isCancelledReceipt()){
			receiptStatusCondition = "and ps.status_id  =  36005";
			cancelledReceitps = 1;
		}
		else{
			receiptStatusCondition = "and ps.status_id  <>  36005";
		}
		
		String query ="SELECT " +
		" ABC.total  \"totalAmount\", \n" +
		" ABC.noOfReceipt ,\n" +
		" PROP.COMMERCIAL_NAME  \"propertyName\" ,\n" +
		" PSUP.UNIT_COST_CENTER  \"unitNumber\",\n" +
		" CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) )  ,\n" +
		" CNTRT.CONTRACT_NUMBER   \"contractNumber\",\n" +
		" CNTRT.CONTRACT_ID  \"contractId\",\n" +
		" REQ.REQUEST_ID   \"requestId\",\n" + 
		" REQ.REQUEST_NUMBER \"requestNumber\",\n" + 
		" PS.STATUS_ID \"psStatusId\",\n"+ 
		" CT_TYPE.DATA_DESC_EN \"contarctTypeEn\",\n"+
		" CT_TYPE.DATA_DESC_AR \"contarctTypeAr\", \n"+
		" RQ_TYPE.REQUEST_TYPE_EN \"requestTypeEn\",\n"+
		" RQ_TYPE.REQUEST_TYPE_AR \"requestTypeAr\",\n"+
		" Req.REQUEST_TYPE_ID \"requestTypeId\",\n"+
		" ps.payment_schedule_id \"paymentScheduleId\",\n" +
		" abc.PAYMENT_RECEIPT_ID ,\n" +
		" abc.total \n," +
		" ps.PAYMENT_NUMBER ,\n" +
		" LTRIM ( PRSN.FIRST_NAME ||' ' || NVL(PRSN.MIDDLE_NAME,'')  || ' '  || PRSN.LAST_NAME  || PRSN.COMPANY_NAME  ) \"paidBy\",\n" +  
		" PRSN.MIDDLE_NAME  ,\n" +  
		" PRSN.LAST_NAME   ,\n"+  
		" PRSN.COMPANY_NAME  ,\n"+ 
		" sUser.FULL_NAME  \"receiptBy\" ,\n " +
		" PRCT.RECEIPT_NUMBER \"receiptNumber\",\n"+  
		" CAST(to_char(PRCT.CREATED_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"receiptDate\",\n" +
	 	" prct.PAYMENT_RECEIPT_ID \"receiptId\",\n"+ 
	 	" DD_CS.DATA_DESC_EN  \"receiptStatusEn\",\n" +  
	 	" DD_CS.DATA_DESC_AR  \"receiptStatusAr\",\n" +  
		" DD_CT.DATA_DESC_EN  \"chequeTypeEn\",\n" +  
		" DD_CT.DATA_DESC_AR  \"chequeTypeAr\",\n" + 
		" PMT.PAYMENT_METHOD_ID  \"paymentMethodEn\",\n" +  
		" PMT.PAYMENT_METHOD_ID  \"paymentMethodAr\",\n" +  
		" PS.AMOUNT   \"receiptAmount\",\n" +  
		" PT.DESCRIPTION_EN \"paymentTypeEn\",\n" + 
		" PT.DESCRIPTION_Ar \"paymentTypeAr\",\n" + 
		" PT.PAYMENT_TYPE_ID \"paymentTypeId\",\n" + 
		" prd.METHOD_REF_NO  \"chequeNumber\",\n" +  
		" prd.METHOD_REF_DATE  \"chequeDate\",\n" +
		" prd.METHOD_REF_DATE  \"chequeDate\",\n" +
		" prd.AMOUNT  ,\n" +  
		" prd.PAYMENT_RECEIPT_DETAIL_ID \"paymentReceiptDetailId\",\n" + 
		" CASE \n " + 
		" WHEN prd.MIGRATED IS NOT NULL AND prd.MIGRATED =1 THEN prd.BANK_NAME \n" + 
		"	 else BANK.BANK_EN  end \"bankNameEn\",\n" +  
		" CASE \n " + 
		" WHEN prd.MIGRATED IS NOT NULL AND prd.MIGRATED =1 THEN prd.BANK_NAME \n" + 
		"	 else BANK.BANK_AR  end \"bankNameAr\" \n"+ 
		"From (" +
		" select rcpt.PAYMENT_RECEIPT_ID,rcpt.RECEIPT_NUMBER ,\n" +
		" sum(ps.AMOUNT) total, count (prd.PAYMENT_RECEIPT_DETAIL_id) noOfReceipt \n"+
		" from PAYMENT_RECEIPT rcpt inner join  PAYMENT_RECEIPT_DETAIL prd on rcpt.payment_receipt_id = prd.payment_receipt_id \n"+
		" inner join  PAYMENT_RECEIPT_TERMS prt on prd.payment_receipt_detail_id = prt.payment_receipt_detail_id \n" +
		" inner join  PAYMENT_SCHEDULE ps on prt.payment_schedule_id = ps.payment_schedule_id \n" +
		" where 1 = 1"+ receiptStatusCondition+
		"  and  PS.MIGRATED = 0 "+ 
		" AND PRD.IS_DELETED=0 AND PRD.RECORD_STATUS=1 AND rcpt.IS_DELETED=0 AND rcpt.RECORD_STATUS=1 "+ 

		" group by rcpt.PAYMENT_RECEIPT_ID,rcpt.RECEIPT_NUMBER \n "+
		" ) ABC \n" +
	    " inner join  PAYMENT_RECEIPT_DETAIL prd on abc.payment_receipt_id = prd.payment_receipt_id \n" +
		" inner join  PAYMENT_RECEIPT_TERMS prt on prd.payment_receipt_detail_id = prt.payment_receipt_detail_id \n" +
		" inner join  PAYMENT_SCHEDULE ps on prt.payment_schedule_id = ps.payment_schedule_id \n"+
		" LEFT JOIN  contract cntrt ON ps.contract_id = cntrt.contract_id \n"+  
		" LEFT JOIN PAYMENT_SCH_UNIT_PROP PSUP ON PS.PAYMENT_SCHEDULE_ID = PSUP.PAYMENT_SCHEDULE_ID \n"+  
		" LEFT JOIN PAYMENT_TYPE PT ON PS.PAYMENT_TYPE_ID = PT.PAYMENT_TYPE_ID \n"+  
		" LEFT JOIN PROPERTY PROP ON PROP.PROPERTY_ID = PSUP.PROPERTY_ID \n"+
		" LEFT JOIN REQUEST REQ ON PS.REQUEST_ID = REQ.REQUEST_ID \n" +  
		" LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID \n"+  
		" LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+  
		" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID \n" +  
		" LEFT JOIN DOMAIN_DATA DD_CS ON PS.STATUS_ID = DD_CS.DOMAIN_DATA_ID \n" +  
		" INNER JOIN PAYMENT_METHOD PMT ON prd.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID\n " +  
		" LEFT JOIN BANK ON BANK.BANK_ID = prd.BANK_ID \n" +  
		" LEFT JOIN DOMAIN_DATA DD_CT ON prd.CHEQUE_TYPE = DD_CT.DOMAIN_DATA_ID \n" +  
		" LEFT OUTER JOIN  payment_receipt prct ON prd.PAYMENT_RECEIPT_ID = prct.PAYMENT_RECEIPT_ID \n" +  
		" LEFT JOIN person prsn ON prct.paid_by = prsn.person_id \n" + 
		" LEFT JOIN SEC_USER sUser on prct.created_by = sUser.login_id \n"+
		" LEFT JOIN REQUEST_TYPE RQ_TYPE ON RQ_TYPE.REQUEST_TYPE_ID = REQ.REQUEST_TYPE_ID \n"+
		" LEFT JOIN DOMAIN_DATA CT_TYPE	ON CT_TYPE.DOMAIN_DATA_ID = CNTRT.CONTRACT_TYPE_ID \n";
				
		String condition="";
							
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
			
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getOwnerShipTypes() != null && criteria.getOwnerShipTypes().length() >0)
								condition+=" AND  ( " +
										"			PROP.OWNERSHIP_TYPE_ID IN (" + criteria.getOwnerShipTypes() +") OR"+
										"        	PS.OWNERSHIP_TYPE_ID IN (" + criteria.getOwnerShipTypes() +")" +
												  ")";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(PSUP.UNIT_NUMBER)  like  lower('%" + criteria.getUnitNumber() +"%')";
							if(criteria.getPaidBy() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getPaidBy() +"%')" +
													" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getPaidBy() +"%')" +
													" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getPaidBy() +"%')" +
													" ) " ;
							if(criteria.getRequestNumber()!=null && criteria.getRequestNumber().trim().length()>0)
								condition += " ANd lower (REQ.REQUEST_NUMBER) LIKE LOWER('%" +criteria.getRequestNumber() + "%')";
								
							if(criteria.getReceiptNumber().length() >0)
								condition+=" AND lower(PRCT.RECEIPT_NUMBER) like  lower('%" + criteria.getReceiptNumber() +"%')";
							
							if(criteria.getReceiptDate() !=null  )
							{
							  condition += " AND PRCT.CREATED_ON >=" + "to_date ('"+  DateUtil.convertToDisplayDate( criteria.getReceiptDate() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  " ;
							
							}
							if( criteria.getReceiptDateTo() != null )
							{
								
								condition += " AND PRCT.CREATED_ON <=" + "to_date ('"+  DateUtil.convertToDisplayDate( criteria.getReceiptDateTo()  ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss')  " ;
							}
							if(criteria.getReceiptBy().length() >0 && ! criteria.getReceiptBy().equals( "-1" ) )
								condition+=" AND PRCT.CREATED_BY like  '%" + criteria.getReceiptBy() +"%'";
							
							if(criteria.getPaymentType() != null &&  ! criteria.getPaymentType().equals( "-1" ) )
								condition+=" AND PS.PAYMENT_TYPE_ID  =" + criteria.getPaymentType() ;
				
							
							List<DomainDataView> psList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
							DomainDataView ddv = CommonUtil.getIdFromType( psList, WebConstants.CANCELED);
					            condition+= " AND PS.MIGRATED = 0 AND prd.IS_DELETED=0 AND prd.RECORD_STATUS=1 AND PRCT.IS_DELETED=0 AND PRCT.RECORD_STATUS=1 " +receiptStatusCondition;
					            	
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY PRCT.PAYMENT_RECEIPT_ID, PRD.PAYMENT_METHOD_ID ";
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.RECEIPT_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		
		String selectedChequeType="";
		String selectPaymentMethod="";
		String selectedPaymentType="";
		String selectedReceiptBy="";
        
        
        /*******************************                           English Labels             *********************************/
//				if(criteria.getChequeType()!=null && !criteria.getChequeType().equals("-1"))
//				{
//					for(DomainDataView ddvCheque: ddvChequeTypeList)
//					{
//						if(ddvCheque.getDomainDataId().toString().compareTo(criteria.getChequeType())==0)
//						{
//							if(CommonUtil.getIsEnglishLocale())
//								selectedChequeType=ddvCheque.getDataDescEn();
//							else
//								selectedChequeType=ddvCheque.getDataDescAr();
//							break;
//						}
//					}
//				}
//				if(criteria.getPaymentMethod()!=null && !criteria.getPaymentMethod().equals("-1"))
//				{
//					for(PaymentMethodView method: paymentMethodItems)
//					{
//						if(method.getPaymentMethodId().longValue() ==  Long.parseLong( criteria.getPaymentMethod() ) )
//						{
//							if(CommonUtil.getIsEnglishLocale())
//								selectPaymentMethod=method.getDescription();
//							else
//								selectPaymentMethod=method.getDescriptionAr();
//							break;
//						}
//					}
//				}
//				if(criteria.getPaymentType()!=null && !criteria.getPaymentType().equals("-1"))
//				{
//					for(SelectItem type: paymentTypeItems)
//					{
//						if(type.getValue().toString().compareTo(criteria.getPaymentType())==0)
//						{
//							selectedPaymentType=type.getLabel();
//							break;
//						}
//					}
//				}
				if(criteria.getReceiptBy().length() >0 && ! criteria.getReceiptBy().equals( "-1" ) )
				{
					for(SelectItem user: userNameList)
					{
						if(user.getValue().toString().compareTo(criteria.getReceiptBy())==0)
						{
							selectedReceiptBy=user.getLabel();
							break;
						}
					}
				}
				
				
				query = "SELECT \n" ;
        		
        		if(criteria.getContractNumber()!=null && criteria.getContractNumber().toString().trim().length()>0)
        			query += "  '" + criteria.getContractNumber() + "' \"contractNumber\", \n" ;
    			else
    				query += "  ' ' \"contractNumber\", \n" ;
        		
        		if(criteria.getPropertyName()!=null && criteria.getPropertyName().toString().trim().length()>0)
        			query += "  '" + criteria.getPropertyName() + "' \"propertyName\", \n" ;
    			else
    				query += "  ' ' \"propertyName\", \n" ;
        		
        		if(criteria.getUnitNumber()!=null && criteria.getUnitNumber().toString().trim().length()>0)
        			query += "  '" + criteria.getUnitNumber() + "' \"unitNumber\", \n" ;
    			else
    				query += "  ' ' \"unitNumber\", \n" ;
        		
        		if(criteria.getReceiptBy().length() >0 && ! criteria.getReceiptBy().equals( "-1" ) )
        			query += "  '" + selectedReceiptBy + "' \"receiptBy\", \n" ;
    			else
    				query += "  ' ' \"receiptBy\", \n" ;
        		
//        		if(criteria.getPaymentType().length() >0 && ! criteria.getPaymentType().equals( "-1" ) )
//        			query += "  '" + selectedPaymentType + "' \"paymentType\", \n" ;
//    			else
//    				query += "  ' ' \"paymentType\", \n" ;
        		
        		if(criteria.getReceiptNumber()!=null && criteria.getReceiptNumber().toString().trim().length()>0)
        			query += "  '" + criteria.getReceiptNumber() + "' \"receiptNumber\", \n" ;
    			else
    				query += "  ' ' \"receiptNumber\", \n" ;
        		
//        		if(criteria.getChequeType().length() >0 && ! criteria.getChequeType().equals( "-1" ) )
//        			query += "  '" + selectedChequeType + "' \"chequeType\", \n" ;
//    			else
//    				query += "  ' ' \"chequeType\", \n" ;
        		
        		if(criteria.getReceiptDate()!=null)
        			{
        				String date="";
        				SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        				date=dateFormat.format(criteria.getReceiptDate());
        			   query += "  '" + date + "' \"receiptDate\", \n" ;
        			}
    			else
    				query += "  ' ' \"receiptDate\", \n" ;
        		
//        		if(criteria.getPaymentMethod().length() >0 && ! criteria.getPaymentMethod().equals( "-1" ) )
//        			query += "  '" + selectPaymentMethod + "' \"paymentMethod\", \n" ;
//    			else
//    				query += "  ' ' \"paymentMethod\", \n" ;
        		
        		if(criteria.getRequestNumber()!=null && criteria.getRequestNumber().toString().trim().length()>0)
        			query += "  '" + criteria.getRequestNumber() + "' \"requestNumber\", \n" ;
    			else
    				query += "  ' ' \"requestNumber\", \n" ;
        		
        		if(criteria.getPaidBy()!=null && criteria.getPaidBy().toString().trim().length()>0)
        			query += "  '" + criteria.getPaidBy() + "' \"paidBy\", \n" ;
    			else
    				query += "  ' ' \"paidBy\", \n" ;
        		
				
        		query += "  '"+cancelledReceitps+"' \"cancelledFlag\",\n"+ 
        		"  '" + ReportMessageResource.getPropertyEn("report.summaryField.totalReceipts") + "' \"lblTotalReceiptsEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiptReport.cancellationReason.dueToCancelContract") + "' \"lblCancelledReasonContractEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiptReport.cancellationReason.dueToCancelRequest") + "' \"lblCancelledReasonRequestEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("grp.paymentType") + "' \"lblPaymentTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblSumOfAmountEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblReceiptStatusEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receipt.receiptBy") + "' \"lblReceiptByEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiptDetail.PaidBy") + "' \"lblPaidByEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.receiptAmount") + "' \"lblReceiptAmountEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("report.columnName.serialNumber") + "' \"lblSerialNumbereN\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.amountCol") + "' \"lblChequeAmountEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" +

        		"  '" + ReportMessageResource.getPropertyEn("chequeList.chequeType") + "' \"lblChequeTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.chequeDate") + "' \"lblChequeDateEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("collection.transactionType") + "' \"lblTransactionTypeEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.cash") + "' \"lblPMCashEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("payment.draft") + "' \"lblPMDraftEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.cheque") + "' \"lblChequeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiptReport.cancellationReason") + "' \"lblCancellationReasonEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberEn\",\n" +
        		
        		
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("report.summaryField.totalReceipts") + "' \"lblTotalReceiptsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiptReport.cancellationReason.dueToCancelContract") + "' \"lblCancelledReasonContractAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiptReport.cancellationReason.dueToCancelRequest") + "' \"lblCancelledReasonRequestAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("grp.paymentType") + "' \"lblPaymentTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblSumOfAmountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblReceiptStatusAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receipt.receiptBy") + "' \"lblReceiptByAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiptDetail.PaidBy") + "' \"lblPaidByAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.receiptAmount") + "' \"lblReceiptAmountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("report.columnName.serialNumber") + "' \"lblSerialNumberAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"lblChequeAmountAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.chequeType") + "' \"lblChequeTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.chequeDate") + "' \"lblChequeDateAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("collection.transactionType") + "' \"lblTransactionTypeAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.cash") + "' \"lblPMCashAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("payment.draft") + "' \"lblPMDraftAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiptReport.cancellationReason") + "' \"lblCancellationReasonAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.cheque") + "' \"lblChequeAr\",\n" +
        		
		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.title.receiptReport") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.title.receiptReport") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.RECEIPT_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
	public List<DomainDataView> getDdvChequeTypeList() {
		return ddvChequeTypeList;
	}
	public void setDdvChequeTypeList(List<DomainDataView> ddvChequeTypeList) {
		this.ddvChequeTypeList = ddvChequeTypeList;
	}
			

}

