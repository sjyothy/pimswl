package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;


import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.report.criteria.IReportCriteria;

import com.avanza.pims.report.criteria.UnitsPerTenantsReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class UnitsPerTenantsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		UnitsPerTenantsReportCriteria criteria = (UnitsPerTenantsReportCriteria) reportCriteria;
        
        String query =  " SELECT PROP.PROPERTY_NUMBER \"propertyNumber\",\n"+
			            " PROP.COMMERCIAL_NAME \"propertyName\",\n"+
// -- Property Type Left
			            " PRSN.PERSON_ID  \"tenantId\",\n"+
			            " DD_PT.DATA_DESC_EN \"propertyTypeEn\",\n"+
			            " DD_PT.DATA_DESC_AR \"propertyTypeAr\",\n"+
			            " CNTRT.CONTRACT_NUMBER \"contractNumber\",\n"+
			            "CAST( to_char(CNTRT.START_DATE,'dd/mm/yyyy')AS VARCHAR(10) ) \"fromDate\",\n"+
        				"CAST( to_char(CNTRT.END_DATE ,'dd/mm/yyyy')AS VARCHAR(10) ) \"toDate\" ,\n"+
			            " SUM(PMT_S.AMOUNT) \"rentalIncome\" ,"+
            
// -- Addresss
			            " CI.ADDRESS1 ||' '|| CI.ADDRESS2 \"address\",\n"+
           	            " RGN_AREA.DESCRIPTION_AR \"emirates\",\n"+
           	            " U.UNIT_NUMBER \"unitNumber\",\n"+
           	            " U.ACCOUNT_NUMBER \"costCenter\",\n"+
           	            " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME ||' ' ||PRSN.LAST_NAME \"tenantName\"\n"+
          
           

            
// -- TOTAL for Property - SUM of Count (DISTINCT U.UNIT_ID)
//        
// -- GRAND TOTAL for ALL Property - SUM ( TOTAL for Property )
            
 			           "FROM  PROPERTY PROP INNER JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID \n"+
		            	" INNER JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID \n"+
		            	//" LEFT JOIN CONTRACT CNTRT ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID \n"+
		            	"  LEFT JOIN CONTRACT_UNIT CU ON CU.UNIT_ID = U.UNIT_ID LEFT JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
		            	" inner JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID \n"+
		            	" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+
		            	" LEFT JOIN PAYMENT_SCHEDULE PMT_S ON CNTRT.CONTRACT_ID=PMT_S.CONTRACT_ID "+
						" LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID \n"+
					    " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID \n";

        	String condition="";
			        	if(!criteria.getCboPropertyType().equals("-1"))
			        	condition+=" AND PROP.TYPE_ID="+criteria.getCboPropertyType();
			         	if(criteria.getTxtPropertyName().length()>0)
			        		condition+=" AND lower(PROP.COMMERCIAL_NAME) like lower('%"+criteria.getTxtPropertyName()+"%')";
			           	if(!criteria.getCboPropertyUsage().equals("-1"))
			        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getCboPropertyUsage();
			        	if(criteria.getTxtTenantName() .length() >0)
			        		condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTxtTenantName().trim() +"%')";
/*							condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTxtTenantName() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +			
											" ) " ;*/
			        	if(criteria.getTxtContractNumber().length()>0)
			        		condition+="AND lower(CNTRT.CONTRACT_NUMBER) like lower('%"+criteria.getTxtContractNumber()+"%')";
			        	if(!criteria.getCboEmirate().equals("-1"))
			        		condition+=" AND RGN_AREA.REGION_ID= '"+criteria.getCboEmirate()+"'";
			        	if(!criteria.getCboUnittype().equals("-1"))
							condition+= " AND U.UNIT_TYPE_ID  =  " + criteria.getCboUnittype();
			        	
						if(criteria.getTxtCostCenter().length()>0)
			        		condition+=" AND U.ACCOUNT_NUMBER like '%"+criteria.getTxtCostCenter()+"%'";
						if(criteria.getTxtUnitNumber().length()>0)
			        		condition+=" AND U.UNIT_NUMBER like '%"+criteria.getTxtUnitNumber()+"%'";
						if(!criteria.getCboCommunity().equals("-1"))
							condition+=" AND CI.CITY_ID = '"+criteria.getCboCommunity()+"'";
     			
     			query+=" WHERE 1=1 \n"+condition+
		     			"GROUP BY "+
						" CNTRT.contract_ID, "+
						" U.UNIT_TYPE_ID ,"+
						" PROP.PROPERTY_NUMBER  , "+
						" PROP.COMMERCIAL_NAME   , "+
						" CNTRT.START_DATE  ,"+
				        " CNTRT.END_DATE  ,\n"+
				        " PRSN.PERSON_ID ,"+
						" DD_PT.DATA_DESC_EN   , "+
						" DD_PT.DATA_DESC_AR  , "+
						" CI.ADDRESS1 || ' ' || CI.ADDRESS2  , "+
						" RGN_AREA.DESCRIPTION_AR  , " +
						" U.UNIT_NUMBER," +
						" U.ACCOUNT_NUMBER,"+
					    " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  ,"+
						" CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) , "+
						" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , "+
						" CNTRT.CONTRACT_NUMBER  ," +
						" CNTRT.RENT_AMOUNT "; 

        	   			
        String dataDef = ReportConstant.DataDef.UNITS_PER_TENANTS;        
        reportQueryMap.put(dataDef, query+"");
        
        // /////////////LABELS////////////////////////////////
        query = "SELECT \n" + 

        /**
-		 * ***************************** English Labels
		 * ********************************
		 */
		
        		 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.propertyNumber") + "' \"lblPropertyNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.address") + "' \"lblAddressEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.emirates") + "' \"lblEmiratesEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("tenants.name") + "' \"lblTenantNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.contractNo") + "' \"lblContractNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.fromDate") + "' \"lblFromDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.toDate") + "' \"lblToDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.rentalIncome") + "' \"lblRentalIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.total") + "' \"lblTotalRentalIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.unitsPerTenantsReport.rpt") + "' \"lblReportNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
		       
		        
		        
		        /**
				 * ***************************** Arabic Labels
				 * ********************************
				 */
		             
		        
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.propertyNumber") + "' \"lblPropertyNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.address") + "' \"lblAddressAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.emirates") + "' \"lblEmiratesAr\",\n" +
		       
		        "  '" + ReportMessageResource.getPropertyAr("tenants.name") + "' \"lblTenantNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.contractNo") + "' \"lblContractNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.fromDate") + "' \"lblFromDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.toDate") + "' \"lblToDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.rentalIncome") + "' \"lblRentalIncomeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.total") + "' \"lblTotalRentalIncomeAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.unitsPerTenantsReport.rpt") + "' \"lblReportNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" ,\n" + 
		       
		        " '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" +

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.UNITS_PER_TENANTS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;

      
	}

}
