package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.Utils.DateUtil;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.MaintenanceContractsReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class MaintenanceContractsReportProcessor extends AbstractReportProcessor{

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		MaintenanceContractsReportCriteria criteria = (MaintenanceContractsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);

		String query = "SELECT DISTINCT " +
							" CNTRT.CONTRACT_ID  ," +
							"CAST( to_char(CNTRT.CONTRACT_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"issueDate\" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , " +
							"CNTRT.RENT_AMOUNT ," +
						//	"PROP.PROPERTY_NAME ," +
							" NVL(PROP.COMMERCIAL_NAME, '---') , " +
							"PRSN.FIRST_NAME \"contractorFirstName\" , " +
							"PRSN.MIDDLE_NAME \"contractorMiddleName\" , " +
							"PRSN.LAST_NAME \"contractorLastName\" , " +
							"PRSN.COMPANY_NAME," +
							"TWS.DESCRIPTION \"workScope\"," + // add it to report
							"WT.DESCRIPTION_EN \"maintenanceworkTypeEn\" , " +
							"WT.DESCRIPTION_AR \"maintenanceworkTypeAr\" , " +
							"DD.DATA_DESC_EN  \"contractStatusEn\" , " +
							"DD.DATA_DESC_AR  \"contractStatusAr\" " +
							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID  = PRSN.PERSON_ID " +
						//	" INNER JOIN PROPERTY PROP ON CNTRT.PROPERTY_ID  = PROP.PROPERTY_ID " +
							" LEFT JOIN DOMAIN_DATA DD ON CNTRT.STATUS_ID = DD.DOMAIN_DATA_ID " +
							" INNER JOIN TENDER_WORK_SCOPE TWS ON CNTRT.CONTRACT_ID = TWS.CONTRACT_ID "+
                            " INNER JOIN WORK_TYPE WT ON WT.WORK_TYPE_ID = TWS.WORK_TYPE_ID " +
                            " LEFT JOIN PROPERTY PROP ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID " ;

					String condition="";
					
					if(criteria.getContractNumber().length() >0)
						condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
					if(criteria.getContractorName() .length() >0)
						condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +
										" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getContractorName() +"%')" +
										" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +			
										" ) " ;
						
					if(criteria.getPropertyName().length() >0)
						condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
					if(criteria.getFromContractDateRich()!=null  && criteria.getToContractDateRich()!=null)								
						condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() )+"', 'dd/MM/yyyy')";
					if(!criteria.getWorkType().equals("-1"))
						condition+= " AND WT.WORK_TYPE_ID  =  '" + criteria.getWorkType() +"'"	;
					if(!criteria.getContractStatus().equals("-1"))
						condition+= " AND CNTRT.STATUS_ID   =  " + criteria.getContractStatus();
			
					if(condition.length() > 0)
			        	query+=" where 1=1  and CNTRT.CONTRACT_TYPE_ID=32003 \n"+ condition;
					else
						query+=" where 1=1  and CNTRT.CONTRACT_TYPE_ID=32003 \n";
			
			String sorting =" ORDER BY CNTRT.CONTRACT_ID";
					
			query+=sorting;
			
			System.out.print("-------------" + query);
			String dataDef = ReportConstant.DataDef.MAINTENANCE_CONTRACTS;        
			reportQueryMap.put(dataDef, query+"");

	        query = "SELECT \n" + 

	        /*******************************                           English Labels             *********************************/
			
	        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
			        "  '" + ReportMessageResource.getPropertyEn("scopeOfWork.workType") + "' \"lblMaintenanceworkTypeEn\",\n" + 
			        "  '" + ReportMessageResource.getPropertyEn("contract.issueDate") + "' \"lblIssueDateEn\",\n" + 
			        "  '" + ReportMessageResource.getPropertyEn("contractor.name") + "' \"lblContractorNameEn\",\n" + 
			        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblContractStatusEn\",\n" +
			        "  '" + ReportMessageResource.getPropertyEn("commons.tab.scopeOfWork") + "' \"lblWorkScopeEn\",\n" +
			        
			        
			        
			        /*******************************                           Arabic Labels             *********************************/
			        
	        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("scopeOfWork.workType") + "' \"lblMmaintenanceworkTypeAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("contract.issueDate") + "' \"lblIssueDateAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("contractor.name") + "' \"lblContractorNameAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblContractStatusAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("commons.tab.scopeOfWork") + "' \"lblWorkScopeAr\",\n" +

			        /*******************************                           Log in User  -- Mandatory Lables           *********************************/	        
			        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
			        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
			        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.maintenanceContracts.rpt") + "' \"lblReportNameEn\",\n" + 
			        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.maintenanceContracts.rpt") + "' \"lblReportNameAr\",\n" + 
			        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

			        "FROM DUAL";
			dataDef = ReportConstant.DataDef.MAINTENANCE_CONTRACTS_LABEL;        
			reportQueryMap.put(dataDef, query);         
	        
	        return reportQueryMap;
		}
			
	
}
