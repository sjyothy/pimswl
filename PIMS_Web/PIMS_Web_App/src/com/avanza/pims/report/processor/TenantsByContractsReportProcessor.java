package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.TenantsByContractsReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantsByContractsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		TenantsByContractsReportCriteria criteria = (TenantsByContractsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		///////////////////////////
		
		String query = "SELECT " +
		" CNTRT.CONTRACT_ID ,"+
		" U.UNIT_TYPE_ID ,"+
		" PROP.PROPERTY_NUMBER \"propertyNumber\" , " +
        " PROP.COMMERCIAL_NAME  \"propertyName\" , " +
        
//        -- Property Type 
        
        " DD_PT.DATA_DESC_EN  \"propertyTypeEn\" , " +
        " DD_PT.DATA_DESC_AR  \"propertyTypeAr\" , " +
        
//        -- Addresss
        " CI.ADDRESS1 || ' ' || CI.ADDRESS2 \"address\" , " +
        " RGN_AREA.DESCRIPTION_AR \"emirate\" , " +
        " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  \"tenantName\", " +
        " CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"issueDate\" , " +
		" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"expiryDate\" , " +
		" CNTRT.CONTRACT_NUMBER  \"contractNumber\", " +
		" SUM(PMT_S.AMOUNT) \"rentalIncome\" , "+
		" CNTRT.RENT_AMOUNT \"rentAnnual\" , "+
		" RGN_COM.DESCRIPTION_AR \"community\" , " + 
		" U.ACCOUNT_NUMBER \"costCenter\" , " +
		" U.UNIT_NUMBER \"unitNumber\" " +

        
		" FROM  " +

		" PROPERTY PROP " +
        " INNER JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
        //" INNER JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID  " +
        " INNER JOIN ((UNIT U LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID) INNER JOIN CONTRACT_UNIT CUNIT ON U.UNIT_ID = CUNIT.UNIT_ID) ON U.FLOOR_ID = F.FLOOR_ID " +
        //" LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID " +
        " LEFT JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = CUNIT.CONTRACT_ID " +
        " INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
        " INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
        " LEFT JOIN PAYMENT_SCHEDULE PMT_S ON CNTRT.CONTRACT_ID=PMT_S.CONTRACT_ID "+
        " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID " +         
        " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID " + 
        " LEFT JOIN REGION RGN_COM ON RGN_COM.REGION_ID = CI.CITY_ID  " ;
        
                        

				String condition = "";
				
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
							
							if(criteria.getTenantName().length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower('%" + criteria.getTenantName().trim() +"%')" +
													" OR lower(PRSN.MIDDLE_NAME) like  lower('%" + criteria.getTenantName().trim() +"%')"+
													" OR lower(PRSN.LAST_NAME) like  lower('%" + criteria.getTenantName().trim() +"%')"+
													" ) " ;
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";						
							
							if(criteria.getContractPeriodFrom() !=null  && criteria.getContractPeriodTo()!=null)
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractPeriodFrom()) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getContractPeriodTo() )+"','dd/MM/yyyy')";
							if(!criteria.getPropertyType().equals("-1"))
								condition+= " AND PROP.TYPE_ID = '" + criteria.getPropertyType() +"'"	;
							if(!criteria.getPropertyUsage().equals("-1"))
				        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getPropertyUsage();
							if(!criteria.getUnittype().equals("-1"))
								condition+= " AND U.UNIT_TYPE_ID  =  " + criteria.getUnittype();
							if(!criteria.getEmirate().equals("-1"))
				        		condition+=" AND RGN_AREA.REGION_ID= '"+criteria.getEmirate()+"'";
							if(criteria.getUnitNumber().length()>0)
				        		condition+=" AND lower(U.UNIT_NUMBER) like lower('%"+criteria.getUnitNumber()+"%')";
							
							if(!criteria.getCommunity().equals("-1"))
				        		condition+=" AND RGN_COM.REGION_ID= '"+criteria.getCommunity()+"'";
							if(criteria.getCostCenter().length()>0)
				        		condition+=" AND U.ACCOUNT_NUMBER like '%"+criteria.getCostCenter()+"%'";
							
							
					
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				query+=" GROUP BY "+
						" CNTRT.contract_ID, "+
						" U.UNIT_TYPE_ID ,"+
						" PROP.PROPERTY_NUMBER  , "+
						" PROP.COMMERCIAL_NAME   , "+
						" DD_PT.DATA_DESC_EN   , "+
						" DD_PT.DATA_DESC_AR  , "+
						" CI.ADDRESS1 || ' ' || CI.ADDRESS2  , "+
						" RGN_AREA.DESCRIPTION_AR  , "+
					    " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  ,"+
						" CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) , "+
						" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , "+
						" CNTRT.CONTRACT_NUMBER  ," +
						" CNTRT.RENT_AMOUNT , " + 
						"RGN_COM.DESCRIPTION_AR, U.ACCOUNT_NUMBER, U.UNIT_NUMBER "; 
				String sorting =" ORDER BY PROP.PROPERTY_NUMBER , PROP.COMMERCIAL_NAME ";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.TENANTS_BY_CONTRACTS;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("property.number") + "' \"lblPropertyNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.Emirate") + "' \"lblEmirateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblAddressEn\",\n" +		        		        

		        "  '" + ReportMessageResource.getPropertyEn("tenants.name") + "' \"lblTenantNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.issueDate") + "' \"lblIssueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.date.expiry") + "' \"lblExpiryDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractNumber") + "' \"lblContractNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.TotalContractValue") + "' \"lblContractValueEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.rentalIncome") + "' \"lblRentalIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.unit.annualRent") + "' \"lblRentAnnualEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.total") + "' \"lblTotalRentalIncomeEn\",\n" +
		       

		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("property.number") + "' \"lblPropertyNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.Emirate") + "' \"lblEmirateAr\",\n" +
		   		"  '" + ReportMessageResource.getPropertyAr("tenants.name") + "' \"lblTenantNameAr\",\n" +
		   		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblAddressAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.issueDate") + "' \"lblIssueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.date.expiry") + "' \"lblExpiryDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractNumber") + "' \"lblContractNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.TotalContractValue") + "' \"lblContractValueAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.rentalIncome") + "' \"lblRentalIncomeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.unit.annualRent") + "' \"lblRentAnnualAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.total") + "' \"lblTotalRentalIncomeAr\",\n" +
		        
		       
		               

		     

		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantsByContractsReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantsByContractsReport.rpt") + "' \"lblReportNameAr\" , \n" + 
	             "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
        System.out.print("222-------------" + query);
		dataDef = ReportConstant.DataDef.TENANTS_BY_CONTRACTS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
