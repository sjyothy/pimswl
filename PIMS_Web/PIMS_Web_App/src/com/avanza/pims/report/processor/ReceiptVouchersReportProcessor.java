package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.ReceiptVouchersReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ReceiptVouchersReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		// TODO : Change criteria object for this one
		
		ReceiptVouchersReportCriteria criteria = (ReceiptVouchersReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		
		///////////////////////////////
		
		String query = "SELECT " +
//        -- Tenant Name
		" PRSN.PERSON_ID \"tenantId\", " +
        " PRSN.FIRST_NAME ||' '||PRSN.MIDDLE_NAME || ' '|| PRSN.LAST_NAME \"tenantName\", " +
        " PRSN.MIDDLE_NAME , " +
		" PRSN.LAST_NAME , " +
		" PRSN.COMPANY_NAME, " +
		" PRSN.PERSON_ID \"tenantId\",\n"+
        " PRSN.IS_COMPANY \"tenantTypeEn\",\n"+
        " PRSN.IS_COMPANY \"tenantTypeAr\",\n"+
		        
		" U.UNIT_NUMBER \"unitNumber\", " +
        
        " CNTRT.CONTRACT_NUMBER \"contractNumber\", " +
        " CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) , " +
        " CAST(to_char(CNTRT.START_DATE ,'dd/mm/yyyy') AS VARCHAR(10) )  \"contractStartDate\" , " +
        " CAST(to_char(CNTRT.END_DATE ,'dd/mm/yyyy') AS VARCHAR(10) )  \"expiryDate\" , " +
                
        
        " PROP.COMMERCIAL_NAME \"propertyName\", " +
//        -- Ownership Type
        " DD_POT.DATA_DESC_EN \"propertyOwnershipTypeEn\", " +
        " DD_POT.DATA_DESC_AR \"propertyOwnershipTypeAr\", " +
        
//        -- Payment Info 
        " PS.PAYMENT_NUMBER \"paymentNumber\", " +
        " CAST(to_char(PS.PAYMENT_DUE_ON ,'dd/mm/yyyy') AS VARCHAR(10) )  \"PaymentDueDate\" , " +
        
        
//        -- Paymt Type
        " PYT.DESCRIPTION_EN  \"paymentTypeEn\", " +
        " PYT.DESCRIPTION_AR  \"paymentTypeAr\", " +
        
//        -- Method
        " PMT.DESCRIPTION \"paymentMethodEn\", " +
        " PMT.DESCRIPTION \"paymentMethodAr\", " +
        
        " PRCT.RECEIPT_NUMBER \"receiptNumber\", " +
        " CAST(to_char(PRCT.CREATED_ON ,'dd/mm/yyyy') AS VARCHAR(10) )  \"receiptDate\" , " +
        
        
//        -- Cheque Number
        " PRCD.METHOD_REF_NO \"chequeNumber\", " +
        " PRCD.METHOD_REF_DATE, " +
        " DD_CT.DATA_DESC_EN  \"chequeTypeEn\" , " +
        " DD_CT.DATA_DESC_AR  \"chequeTypeAr\" , " +  
        " PRCD.AMOUNT \"chequeAmount\", " +
        
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_EN end\"bankNameEn\", " +
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_AR end\"bankNameAr\", " +
        
        " PRCD.CHEQUE_TYPE \"chequeType\", " +
        " PRCD.CHEQUE_OWNER \"chequeOwnerName\", " +
        
        " PRCD.UPDATED_BY , " +
        
//        -- Receipt Status
        " DD_CS.DATA_DESC_EN , " +
        " DD_CS.DATA_DESC_AR  ," +
        " DD_TT.DATA_DESC_EN \"tenantTypeEn\",\n"+
        " DD_TT.DATA_DESC_AR \"tenantTypeAr\",\n"+
        " PRSN.PERSON_TYPE \n"+
        
//        -- CHQUE TYPE LEFT HERE

" FROM  " +
" CONTRACT CNTRT " + 
" INNER JOIN PERSON PRSN ON " + 

" CNTRT.TENANT_ID = PRSN.PERSON_ID " +
" LEFT JOIN CONTRACT_UNIT CU ON  " +

" CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
" LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " + 
" LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
" LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " + 

" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +

" INNER  JOIN PAYMENT_RECEIPT_TERMS PRC ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " + 
                    
" INNER  JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
" INNER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
        
" INNER JOIN PAYMENT_RECEIPT PRCT ON PRCD.PAYMENT_RECEIPT_ID = PRCT.PAYMENT_RECEIPT_ID " +
" LEFT JOIN DOMAIN_DATA DD_CT ON PRCD.CHEQUE_TYPE = DD_CT.DOMAIN_DATA_ID " +
" LEFT JOIN DOMAIN_DATA DD_POT ON PROP.OWNERSHIP_TYPE_ID = DD_POT.DOMAIN_DATA_ID " +
" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
" LEFT JOIN DOMAIN_DATA DD_CS ON PS.STATUS_ID = DD_CS.DOMAIN_DATA_ID " +
" LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;

        
       
        
		                
				String condition="";
				
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
												" ) " ;
							if(!criteria.getChequeType().equals("-1"))
								condition+= " AND PRCD.CHEQUE_TYPE  =  " + criteria.getChequeType();
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
							
							
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like lower( '%" + criteria.getUnitNumber() +"%')";
							
							if(criteria.getPaymentNumber().length() >0)
								condition+=" AND lower(PS.PAYMENT_NUMBER) like  lower('%" + criteria.getPaymentNumber().toLowerCase() +"%')";

							if(criteria.getReceiptNumber().length() >0)
								condition+=" AND Lower( PRCT.RECEIPT_NUMBER ) like  '%" + criteria.getReceiptNumber().toLowerCase() +"%'";
							
							if(criteria.getChequeNumber().length() >0)
								condition+=" AND lower(PRCD.METHOD_REF_NO) like  lower('%" + criteria.getChequeNumber().toLowerCase() +"%')";
							
							if(criteria.getChequeAmount().length() >0)
								condition+=" AND PRCD.AMOUNT =  " + criteria.getChequeAmount() ;
							
							
							if(criteria.getChequeOwnerName().length() >0)
								condition+=" AND lower(PRCD.CHEQUE_OWNER) like lower('% " + criteria.getChequeOwnerName().toLowerCase() +"%')";
							
							
							// Change the Type in JSP and Criteria Object
						    if(criteria.getContractStartDateFrom()!=null  )								
								condition += " AND CNTRT.START_DATE >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractStartDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
							
						    if(criteria.getContractStartDateTo()!=null  )								
								condition += " AND CNTRT.START_DATE <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractStartDateTo() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
							
						    
						    if(criteria.getExpiryDateFrom()!=null  )
								condition += " AND CNTRT.END_DATE >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getExpiryDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  ";
							
						    if(criteria.getExpiryDateTo() !=null  )
								condition += " AND CNTRT.END_DATE <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getExpiryDateTo() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss')  ";
							
							
							if(criteria.getReceiptDateFrom()!=null  )
							condition += " AND PRCT.CREATED_ON >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getReceiptDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";

							if(criteria.getReceiptDateTo() !=null  )
								condition += " AND PRCT.CREATED_ON <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getReceiptDateTo() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";

							if(criteria.getFromPaymentDueDate()!=null  )
							condition += " AND PS.PAYMENT_DUE_ON >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromPaymentDueDate() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')";

							if(criteria.getToPaymentDueDate() !=null  )
								condition += " AND PS.PAYMENT_DUE_ON <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getToPaymentDueDate() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";

							if(!criteria.getPropertyOwnershipType().equals("-1"))
								condition+= " AND PROP.OWNERSHIP_TYPE_ID = " + criteria.getPropertyOwnershipType() 	;
							
							if(!criteria.getPaymentType().equals("-1"))
								condition+= " AND PS.PAYMENT_TYPE_ID = " + criteria.getPaymentType() ;
							if(!criteria.getBankName().equals("-1"))
								condition+= " AND BANK.BANK_ID  =  " + criteria.getBankName();
																		
							
							if(!criteria.getPaymentMethod().equals("-1"))
								condition+= " AND PMT.DESCRIPTION  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getPaymentMethod() +"')"	;
							if(criteria.getTenantId().length() >0)
								condition+=" AND PRSN.PERSON_ID = " + criteria.getTenantId() ;
//							if(!criteria.getTenantType().equals("-1"))
//								condition+=" AND PRSN.IS_COMPANY = '" + criteria.getTenantType()+"'";
							if(!criteria.getTenantType().equals("-1"))
							{
								String tenantTypeCriteria;
								if(criteria.getTenantType().equals("26002"))
									tenantTypeCriteria="0";
								else 
									tenantTypeCriteria="1";
								condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
							}	
					
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY CNTRT.CONTRACT_ID , PS.PAYMENT_DUE_ON";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.RECEIPT_VOUCHERS_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("tenants.id") + "' \"lblTenantIdEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("contract.tenanttype") + "' \"lblTenantTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("settlement.label.startdate") + "' \"lblContractStartDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.enddate") + "' \"lblExpiryDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("applicationDetails.ownershipType") + "' \"lblPropertyOwnershipTypeEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentAmountCol") + "' \"lblChequeAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeType") + "' \"lblChequeTypeEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cheuqe.owner") + "' \"lblChequeOwnerNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.company") + "' \"lblCompanyEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.individual") + "' \"lblIndividualEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("tenants.id") + "' \"lblTenantIdAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("contract.tenanttype") + "' \"lblTenantTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("settlement.label.startdate") + "' \"lblContractStartDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.enddate") + "' \"lblExpiryDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("applicationDetails.ownershipType") + "' \"lblPropertyOwnershipTypeAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeType") + "' \"lblChequeTypeAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentAmountCol") + "' \"lblChequeAmountAr\",\n" + 
		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cheuqe.owner") + "' \"lblChequeOwnerNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.company") + "' \"lblCompanyAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.individual") + "' \"lblIndividualAr\",\n" +
		        
	        /*******************************                           User Info and Mandatory Info             *********************************/	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.receiptVouchersReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.receiptVouchersReport.rpt") + "' \"lblReportNameAr\" , \n" + 
		  "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.RECEIPT_VOUCHERS_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
