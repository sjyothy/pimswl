package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ApplicationDetailsReportCriteria;
import com.avanza.pims.report.criteria.ApplicationSummaryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ApplicationSummaryReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		ApplicationSummaryReportCriteria criteria = (ApplicationSummaryReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////
		String query = "SELECT  " +
		" REQST.REQUEST_ID, \n" +
        " REQST.REQUEST_NUMBER \"applicationNumber\" , \n" +
        
        //-- Request Status
        " DD.DATA_DESC_EN  \"applicationStatusEn\" , \n" +
        " DD.DATA_DESC_AR  \"applicationStatusAr\" , \n" +
        
//        -- APPLICATION NAME 
                
        " PRSN_APP.FIRST_NAME ||' ' || PRSN_APP.MIDDLE_NAME || ' '  || PRSN_APP.LAST_NAME   \"applicantName\" , \n" +
          
		" CAST(to_char(REQST.REQUEST_DATE ,'dd/mm/yyyy') AS VARCHAR(10) )  \"applicationDateFrom\" , \n" +
        
//        -- Application Type
        " RTP.REQUEST_TYPE_EN \"applicationTypeEn\" , \n" +
        " RTP.REQUEST_TYPE_AR \"applicationTypeAr\" , \n" +
        
        " REQST.DESCRIPTION \"applicationDescription\", \n " +
        " (select su.full_name_sec from sec_user su where su.login_id = REQST.created_by) \"createdBy\" \n " +
          
        " FROM  " +
		" REQUEST REQST " +
        " LEFT JOIN PERSON PRSN_APP ON REQST.APPLICANT_ID = PRSN_APP.PERSON_ID " +
       	" INNER JOIN DOMAIN_DATA DD ON REQST.STATUS_ID = DD.DOMAIN_DATA_ID " +
		" INNER JOIN REQUEST_TYPE RTP ON REQST.REQUEST_TYPE_ID = RTP.REQUEST_TYPE_ID " ;
       	                    
				String condition="";
				
							if(criteria.getApplicationNumber().length() >0)
								condition+=" AND lower(REQST.REQUEST_NUMBER) like lower('%" + criteria.getApplicationNumber() +"%')";
														
							if(criteria.getApplicantName() .length() >0)
								condition+= " AND (lower(PRSN_APP.FIRST_NAME) like lower( '%" + criteria.getApplicantName() +"%')" +
												" OR lower(PRSN_APP.MIDDLE_NAME) like  lower( '%" + criteria.getApplicantName() +"%')" +
												" OR lower(PRSN_APP.LAST_NAME) like lower( '%" + criteria.getApplicantName() +"%')" +			
												" ) " ;
							if(!criteria.getApplicationType().equals("-1"))
								condition+= " AND REQST.REQUEST_TYPE_ID  =  '" + criteria.getApplicationType() +"'"	;														
												
							if(criteria.getApplicationDateFrom() !=null  )								
								condition += " AND REQST.REQUEST_DATE >=to_date ('"+ DateUtil.convertToDisplayDate( criteria.getApplicationDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')";
							
							if( criteria.getApplicationDateTo()!=null )
								condition += " AND REQST.REQUEST_DATE <= to_date ('" + DateUtil.convertToDisplayDate( criteria.getApplicationDateTo() )+" 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";
							if(!criteria.getApplicationStatus().equals("-1"))
								condition+= " AND REQST.STATUS_ID   =  '" + criteria.getApplicationStatus() +"'"	;
							
							if(!criteria.getCreatedBy().equals("-1"))
								condition+= " AND REQST.created_by   =  '" + criteria.getCreatedBy() +"'"	;

							
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY REQST.created_by, REQST.REQUEST_TYPE_ID,REQST.REQUEST_DATE";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.APPLICATION_SUMMARY_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("inquiryApplication.applicationNumber") + "' \"lblApplicationNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblApplicationStatusEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.applicantName") + "' \"lblApplicantNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.date") + "' \"lblApplicationDateFromEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("application.type") + "' \"lblApplicationTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.dsescription") + "' \"lblApplicationDescriptionEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.report.name.applicationSummary.rpt") + "' \"lblReportNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\" ,\n" +
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("inquiryApplication.applicationNumber") + "' \"lblApplicationNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblApplicationStatusAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.applicantName") + "' \"lblApplicantNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.date") + "' \"lblApplicationDateFromAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("application.type") + "' \"lblApplicationTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.dsescription") + "' \"lblApplicationDescriptionAr\",\n" +
        	   "  '" + ReportMessageResource.getPropertyAr("commons.report.name.applicationSummary.rpt") + "' \"lblReportNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" , \n" +
		        
		        
	           "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.APPLICATION_SUMMARY_REPORT_LABEL;  
		
		System.out.print("-------------" + query);
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
