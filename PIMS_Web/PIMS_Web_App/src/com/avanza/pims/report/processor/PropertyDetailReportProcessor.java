package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PropertyDetailReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PropertyDetailReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		PropertyDetailReportCriteria criteria = (PropertyDetailReportCriteria) reportCriteria;
        String newQuery = " SELECT prop.property_number \"propertyNumber\", prop.commercial_name \"propetyName\", dd_pt.data_desc_ar \"propertyTypeAr\", (ci.address1 || ' ' || ci.address2) \"Address\", rgn_area.DESCRIPTION_AR \"Emirate\", rgn_city.DESCRIPTION_AR \"ef2\", dd_ot.data_desc_ar \"ef4\", prop.account_number \"ef3\", prop.created_on \"ef1\", prop.receiving_date \"ef10\", " +
        				  " f.floor_id, f.floor_number \"floorNumber\", f.floor_name \"floorName\", f.type_id, dd_ft.data_desc_ar \"floorTypeAr\", " +
        				  " u.unit_type_id \"ef9\", dd_ut.data_desc_ar \"ef7\", COUNT(u.unit_id) \"ef6\"" +
        				  " FROM property prop LEFT JOIN FLOOR f ON f.property_id = prop.property_id" +
        				  " inner JOIN UNIT U ON F.FLOOR_ID = U.FLOOR_ID " +
        				  " LEFT JOIN DOMAIN_DATA DD_FT ON F.TYPE_ID = DD_FT.DOMAIN_DATA_ID " +
        				  " LEFT JOIN DOMAIN_DATA DD_UT ON u.unit_type_id = DD_UT.DOMAIN_DATA_ID " +
        				  " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID"  +
        				  " LEFT JOIN DOMAIN_DATA DD_OT ON PROP.OWNERSHIP_TYPE_ID = DD_OT.DOMAIN_DATA_ID " +
        				  " LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
        				  " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID " +
        				  " LEFT JOIN REGION RGN_CITY ON RGN_CITY.REGION_ID = CI.CITY_ID ";
        
        	String condition="";
			        	if(criteria.getTxtPropertyNumber().length()>0)
			        	condition+=" AND lower(PROP.PROPERTY_NUMBER) like lower('%"+criteria.getTxtPropertyNumber()+"%')";
			        	if(!criteria.getCboPropertyType().equals("-1"))
			        	condition+=" AND PROP.TYPE_ID="+criteria.getCboPropertyType();
			         	if(criteria.getTxtPropertyName().length()>0)
			        		condition+=" AND lower(PROP.COMMERCIAL_NAME) like lower('%"+criteria.getTxtPropertyName()+"%')";
			           	if(!criteria.getCboPropertyUsage().equals("-1"))
			        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getCboPropertyUsage();
			        	if(criteria.getTxtLandNumber().length()>0)
			        		condition+=" AND PROP.LAND_NUMBER like '%"+criteria.getTxtLandNumber()+"%'";
			        	if(!criteria.getCboEmirate().equals("-1"))
			        		condition+=" AND RGN_AREA.REGION_ID = '"+criteria.getCboEmirate()+"'";
			        	if(!criteria.getCboCommunity().equals("-1"))
			        		condition+=" AND RGN_CITY.REGION_ID = '"+criteria.getCboCommunity()+"'";
			        	//if( criteria.getClndrFromDate() !=null  )								
						//	condition += " AND (  (MIGRATED = 1 AND PROP.STATUS_ID = 10017 AND PROP.CREATED_ON >= to_date('"+DateUtil.convertToDisplayDate( criteria.getClndrFromDate())+" 00:00:00','dd/MM/yyyy  hh24:MI:ss' )) OR  ( (MIGRATED = 0 OR MIGRATED IS NULL) AND PROP.STATUS_ID = 10017 AND PROP.UPDATED_ON >= to_date('"+DateUtil.convertToDisplayDate( criteria.getClndrFromDate())+" 00:00:00','dd/MM/yyyy  hh24:MI:ss' )) )";
					    //if( criteria.getCldrToDate() !=null  )
					    //	condition += " AND ( (MIGRATED = 1  AND PROP.STATUS_ID = 10017 AND PROP.CREATED_ON <= to_date('"+DateUtil.convertToDisplayDate( criteria.getCldrToDate())+" 23:59:59','dd/MM/yyyy  hh24:MI:ss' )) OR ((MIGRATED = 0 OR MIGRATED IS NULL) AND PROP.STATUS_ID = 10017 AND  PROP.UPDATED_ON <= to_date('"+DateUtil.convertToDisplayDate( criteria.getCldrToDate())+" 23:59:59','dd/MM/yyyy  hh24:MI:ss' )) )";
			        	if(criteria.getClndrFromDate()!=null  && criteria.getCldrToDate()!=null)								
							condition += " AND PROP.RECEIVING_DATE between to_date  ('"+ DateUtil.convertToDisplayDate( criteria.getClndrFromDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getCldrToDate() )+"', 'dd/MM/yyyy')";
			        	if(criteria.getTxtUnitNumber().length()>0)
			        		condition+=" AND lower(U.UNIT_NUMBER) like lower('%"+criteria.getTxtUnitNumber()+"%')";
			        	if(!criteria.getCboUnittype().equals("-1"))
			        		condition+=" AND U.UNIT_TYPE_ID="+criteria.getCboUnittype();
			        	if(criteria.getTxtFloorNumber().length()>0)
			        		condition+=" AND lower(F.FLOOR_NUMBER) like lower('%"+criteria.getTxtFloorNumber()+"%')";
			        	if(criteria.getTxtCostCenter().length()>0)
			        		condition+=" AND lower(PROP.ACCOUNT_NUMBER) like lower('%"+criteria.getTxtCostCenter()+"%')";
			        	if(!criteria.getCboOwnerShipType().equals("-1"))
			        		condition+=" AND PROP.OWNERSHIP_TYPE_ID = " + criteria.getCboOwnerShipType();
//			        	if(criteria.getTxtTenantName() .length() >0)
//							condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +
//											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTxtTenantName() +"%')" +
//											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +			
//											" ) " ;
//			        	if(criteria.getTxtContractNumber().length()>0)
//			        		condition+="AND lower(CNTRT.CONTRACT_NUMBER) like lower('%"+criteria.getTxtContractNumber()+"%')";
			        	if(StringHelper.isNotEmpty(criteria.getUnitUsage()) && criteria.getUnitUsage().compareTo("-1") != 0){
			        		condition+="AND  U.USAGE_TYPE_ID = "+criteria.getUnitUsage();
			        	}

						//PROP.property_number like '%PROP-10-0000115%'
     			
              newQuery += " WHERE PROP.is_deleted = 0 and PROP.STATUS_ID <> 1009 " + condition;
                
          	  newQuery += " GROUP BY " +
          				  "    prop.property_number, prop.commercial_name, dd_pt.data_desc_ar, (ci.address1 || ' ' || ci.address2), rgn_area.DESCRIPTION_AR, rgn_city.DESCRIPTION_AR, dd_ot.data_desc_ar, prop.account_number, prop.created_on, prop.receiving_date, " +
          				  "    f.floor_id, f.floor_number, f.floor_name, f.type_id, dd_ft.data_desc_ar, " +
          				  "    u.unit_type_id, dd_ut.data_desc_ar " +
          				  "  ORDER BY " +
          				  "    COUNT(u.unit_id)";
        	   			
        String dataDef = ReportConstant.DataDef.PROPERTY_DETAIL;        
        reportQueryMap.put(dataDef, newQuery+"");
        
        // /////////////LABELS////////////////////////////////
        String query = "SELECT \n" + 

        /**
-		 * ***************************** English Labels
		 * ********************************
		 */
		
        		 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.propertyNumber") + "' \"lblPropertyNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.address") + "' \"lblAddressEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.emirates") + "' \"lblEmirateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.type") + "' \"lblUnitTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalUnits") + "' \"lblTotalUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.floorNumber") + "' \"lblFloorNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.floor.Name") + "' \"lblFloorNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("generateUnits.NoOfUnits") + "' \"lblNumberOfUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.floorType") + "' \"lblFloorTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("Common.grandTotal") + "' \"lblTotalSumEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.propertyDetailReport.rpt") + "' \"lblPropertyDetailReportEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
		       
		        
		        
		        /**
				 * ***************************** Arabic Labels
				 * ********************************
				 */
		             
		        
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.propertyNumber") + "' \"lblPropertyNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.address") + "' \"lblAddressAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.emirates") + "' \"lblEmirateAn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.type") + "' \"lblUnitTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalUnits") + "' \"lblTotalUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.floorNumber") + "' \"lblFloorNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.floor.Name") + "' \"lblFloorNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("generateFloors.FloorType") + "' \"lblFloorTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("Common.grandTotal") + "' \"lblTotalSumAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("generateUnits.NoOfUnits") + "' \"lblNumberOfUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.propertyDetailReport.rpt") + "' \"lblPropertyDetailReportAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" ,\n" + 
		       
		        " '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" +

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PROPERTY_DETAIL_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;

      
	}

}
