package com.avanza.pims.report.processor;

import java.util.Map;

import com.avanza.pims.report.criteria.IReportCriteria;

public interface IReportProcessor {
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria);
	public Map<String, String> getReportParametersMap(IReportCriteria reportCriteria);
}
