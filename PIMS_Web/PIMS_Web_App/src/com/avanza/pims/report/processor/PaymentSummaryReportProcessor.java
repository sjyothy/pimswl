package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PaymentSummaryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PaymentSummaryReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		PaymentSummaryReportCriteria criteria = (PaymentSummaryReportCriteria) reportCriteria;
		
		String query = " SELECT  NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							" PROP.PROPERTY_NUMBER \"propertyNumber\" , " +		
  	                        "CAST(to_char(PRCT.CREATED_ON,'dd/mm/yyyy')As VARCHAR(20)) || ' | ' || PRCT.RECEIPT_NUMBER || ' | ' || PRSN.FIRST_NAME || ' ' ||  PRSN.MIDDLE_NAME  || ' ' || PRSN.LAST_NAME \"receiptDate\",\n"+
							"CAST( to_char(PS.PAYMENT_DUE_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDueDate\" , " +
							"PS.PAYMENT_TYPE_ID," +
							"NVL(PYT.DESCRIPTION_EN, '---')  \"paymentTypeEn\" , " +
							"NVL(PYT.DESCRIPTION_AR, '---')  \"paymentTypeAr\" , " +
							"PS.AMOUNT \"paymentAmount\"," +
							"DD2.DATA_DESC_EN  \"paymentStatusEn\" , " +   // this field related to Payment Status(receipt status treat it here as paymentStatus) 
							"DD2.DATA_DESC_AR  \"paymentStatusAr\", " +
							"DD.DATA_DESC_EN  \"propertyOwnershipTypeEn\" , " +
							"DD.DATA_DESC_AR  \"propertyOwnershipTypeAr\" ," +
							"PRCT.RECEIPT_NUMBER \"receiptNo\" "+
							
							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							
							" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON PS.STATUS_ID = DD2.DOMAIN_DATA_ID " +
							" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
							" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON " +
							" PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
							" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
						    "LEFT OUTER JOIN PAYMENT_RECEIPT PRCT\n"+ 
						    " ON PRCT.PAYMENT_Receipt_id = PRCD.PAYMENT_Receipt_id  \n";


	    		String condition="";
							if(!criteria.getPropertyOwnershipType().equals("-1"))
								condition+= " AND DD.DOMAIN_DATA_ID = " + criteria.getPropertyOwnershipType(); 
							
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getPaymentDueDateFrom()!=null  && criteria.getPaymentDueDateTo()!=null)								
								condition += " AND PS.PAYMENT_DUE_ON between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getPaymentDueDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getPaymentDueDateTo() )+"', 'dd/MM/yyyy')";
							if(criteria.getReceiptDateFrom()!=null  && criteria.getReceiptDateTo()!=null)
								condition += " AND PRCT.CREATED_ON between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getReceiptDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getReceiptDateTo() )+"', 'dd/MM/yyyy')";
							if(!criteria.getPaymentType().equals("-1"))
					        	condition+="AND PS.PAYMENT_TYPE_ID="  +criteria.getPaymentType();
							if(!criteria.getContractType().equals("-1"))
								condition+= " AND CNTRT.CONTRACT_TYPE_ID  =  '" + criteria.getContractType() +"'"	;
							if(criteria.getReceiptNo().length() >0)
								condition+= " AND lower(PRCT.RECEIPT_NUMBER)  = lower( '" + criteria.getReceiptNo() +"')"	;

							if(criteria.getTxtTenantName() != null && criteria.getTxtTenantName().length() > 0)
								condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTxtTenantName().trim() +"%')";
							
							if(criteria.getTxtContractNumber() != null && criteria.getTxtContractNumber().length() > 0)
								condition+=" AND CNTRT.CONTRACT_NUMBER like '%"+criteria.getTxtContractNumber()+"%'";
							
							if(criteria.getTxtCostCenter() != null && criteria.getTxtCostCenter().length() > 0)
								condition+=" AND lower (U.ACCOUNT_NUMBER)  like lower( '%" + criteria.getTxtCostCenter().trim() +"%')";
							
							if(condition.length() > 0)
					        	query+="where 1=1 \n"+ condition;
		

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.PAYMENT_SUMMARY;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
    	
    		
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentAmountCol") + "' \"lblPaymentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.status") + "' \"lblPaymentStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.ownershipType") + "' \"lblPropertyOwnershipTypeEn\",\n" + 
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
	    		
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentAmountCol") + "' \"lblPaymentAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.status") + "' \"lblPaymentStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.ownershipType") + "' \"lblPropertyOwnershipTypeAr\",\n" + 

		        /*******************************                           Summary Lables           *********************************/	        

		        "  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblTotalAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblTotalAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.contractPaymentDetail.totalPaymentAmount") + "' \"lblTotalPaymentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.contractPaymentDetail.totalPaymentAmount") + "' \"lblTotalPaymentAmountAr\",\n" + // , placed here if login name will be uncommnet


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentSummary.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentSummary.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PAYMENT_SUMMARY_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}

}
