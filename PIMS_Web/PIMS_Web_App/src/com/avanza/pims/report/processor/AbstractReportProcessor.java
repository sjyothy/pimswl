package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.criteria.IReportCriteria;


public abstract class AbstractReportProcessor implements IReportProcessor {
	 public  String getDateFormat()
		{
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
			
		}
	 
	 public Map<String, String> getReportParametersMap(IReportCriteria reportCriteria){
		 return new  HashMap<String, String>();
	 }
	
}
