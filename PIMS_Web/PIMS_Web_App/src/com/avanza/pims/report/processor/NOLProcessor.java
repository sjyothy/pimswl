package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.NOLCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
public class NOLProcessor extends AbstractReportProcessor
{
	public NOLProcessor()
	{
		
	}
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria)
	{
		
		NOLCriteria criteria = (NOLCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		String condition="";
		String query=  "SELECT distinct REQST.REQUEST_NUMBER,\n" +
						   "REQST.REQUEST_ID ,\n"+
					       "C_UNIT.CONTRACT_ID ,\n"+
					       "CNTRCT.CONTRACT_NUMBER ,\n"+

					       "UNT.UNIT_NUMBER \"unitNumber\" ,\n"+
					       "UNT.UNIT_NUMBER \"unitDescription\" ,\n"+
					     
					       "NT.TEXT_AR \"field1\","+
					       "PROP.PROPERTY_NAME \"propertyName\" ,\n"+
					       "coalesce(cast(PROP.PROPERTY_NAME_EN as nvarchar2(200)),NVL(PROP.COMMERCIAL_NAME,'' )) \"propertyNameEn\" ,\n"+
					       "PROP.PROPERTY_NUMBER ,\n"+
					       "NVL(PROP.COMMERCIAL_NAME,'' )\"propCommercialName\", \n"+
					       "NVL(PROP.LAND_NUMBER,'--') \"landNumber\",\n"+
					       "case \n"+ 
					       "when PRSN.IS_COMPANY =0 THEN \n"+
					       "PRSN.FIRST_NAME||' '||MIDDLE_NAME||' '||LAST_NAME \n"+ 
					       "ELSE \n"+
					       "PRSN.company_name end  \"tenantName\" ,\n"+
					       
					       " Coalesce("+
					       " Cast (Prsn.Full_Name_En As Nvarchar2(200)  ),"+
					       " cast( (Case  When Prsn.Is_Company =0 Then Prsn.First_Name||' '||Middle_Name||' '||Last_Name Else Prsn.Company_Name End) as nvarchar2(200))"+
					       " ) \"tenantNameEn\" ,\n" +
					       
					       "NVL(RGN.DESCRIPTION_EN,'--') \"regionNameEn\" ,\n"+
					       "NVL(RGN.DESCRIPTION_AR,'--') \"regionNameAr\" ,\n"+
					       " COM_ACT.COMMERCIAL_ACTIVITY_DESC_AR \"commActivityName\" ,\n"+
					       "CI.CONTACT_INFO_ID\n"+ 
						   "FROM  REQUEST REQST \n"+
						   " inner Join Request_Field_Detail  Rfd On Rfd.Request_Id=REQST.Request_Id "+
						   " inner Join Request_Key Rf On  Rfd.Request_Key_Id =Rf.Request_Key_Id And Rf.Key_Name='NOL_TYPE'"+
						   " Inner join NOL_TYPE NT on nt.nol_type_id=rfd.request_key_value  "+
						   "INNER JOIN CONTRACT CNTRCT ON CNTRCT.CONTRACT_ID = REQST.CONTRACT_ID \n"+ 
						   "INNER JOIN CONTRACT_UNIT C_UNIT ON C_UNIT.CONTRACT_ID = CNTRCT.CONTRACT_ID \n"+
						   "INNER JOIN UNIT UNT ON UNT.UNIT_ID = C_UNIT.UNIT_ID \n"+
						   "inner join FLOOR FLR ON UNT.FLOOR_ID=FLR.FLOOR_ID \n"+
						   "INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = FLR.PROPERTY_ID \n"+ 
						   "INNER JOIN PERSON PRSN ON CNTRCT.TENANT_ID = PRSN.PERSON_ID \n"+ 
						   "LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+ 
						   "LEFT JOIN REGION RGN ON RGN.REGION_ID = CI.CITY_ID \n"+ 
						   "LEFT JOIN CONTRACT_ACTIVITY CNRCT_ACT ON REQST.REQUEST_ID = CNRCT_ACT.REQUEST_ID \n"+ 
						   "LEFT JOIN COMMERCIAL_ACTIVITY COM_ACT ON CNRCT_ACT.COMMERCIAL_ACTIVITY_ID = CNRCT_ACT.COMMERCIAL_ACTIVITY_ID \n"+
						   "WHERE 1=1 \n"; 
		if(criteria.getRequestId().length() >0)
			condition+=" AND lower(REQST.REQUEST_ID) like lower('%" + criteria.getRequestId()+"%')";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.NOL_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		query="";
		if(criteria.getReportFileName().compareTo(ReportConstant.Report.PRACTICING_ACTIVITY)==0)
		{
			query+= " Select \n"+
			"  '" +criteria.getCommercialActivityListString() + "' \"commercialActivity\"\n" +
			" FROM DUAL";
			dataDef = ReportConstant.DataDef.NOL_COMM_ACT_REPORT;
			reportQueryMap.put(dataDef, query+"");
		}
		
		
		return reportQueryMap;
	}
			

}
