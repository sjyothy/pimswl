package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class StatementOfAccountMasrafBeneficiaryProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{
			executeTransactionQuery(criteria,reportQueryMap);
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}
	

	private void executeTransactionQuery(
											StatementOfAccountCriteria criteria, 
											Map<String, String> reportQueryMap
										)throws Exception  
	{
		String query = "";
		
		query += " select '" +
		  criteria.getLoggedInUser() + "' as \"field1\",'"+
		  criteria.getFormatterReportDisplay().format(criteria.getFromDate())+ "' as \"field2\",'"+
		  criteria.getFormattedToDateForReportDisplay() + "' as \"field3\","+
		"        pd.other \"beneficiaryName\"," +
		"  		cast(to_char( pat.trans_date ,'Mon,dd yyyy') as varchar(11) ) \"trxDate\", " +
		" 		m.masraf_name  \"masrafName\"," +
		"		m.masraf_id \"masrafId\" ," +
		"		sum(pat.amount*-1) \"amount\",  " +
		"		r.request_number \"requestNumber\" , " +
		"		r.request_id \"requestId\"	" +
		" from	" +
		 
		"   personal_account_trans pat " +  
		"   inner join personal_account pa on pa.personal_account_id=pat.personal_account_id " +
		"   inner join MASRAF M on M.MASRAF_ID=PA.MASRAF_ID " +  
		"   left join request r on r.request_id=pat.request_id " +  
		"   left join domain_data dd_tt on dd_tt.domain_data_id = pat.trans_type " +      
		"   left  join distribution_details_end_rev dder on dder.primary_id=pat.end_rev_dist_detail_id " +  
		"   left  join end_file_ben efb on pat.end_file_ben_id = efb.end_file_ben_id " +  
		"   left  join end_file_asso efa on efa.end_file_asso_id =efb.end_file_asso_id " +  
		"   LEFT  JOIN endowment e ON e.endowment_id=efa.endowment_id " +  
		"   left join DISBURSEMENT_DETAILS DISDET on PAT.DISBURSEMENT_DET_ID=DISDET.DIS_DET_ID " +  
		"   left join PAYMENT_DETAILS PD on PD.DIS_DET_ID=DISDET.DIS_DET_ID " +  
		"   where r.request_type_id=52 ";  
		
		if( criteria.getFromDate() != null )
		{
			query += " and pat.trans_date >= to_date('"+criteria.getFormattedFromDate()+"' , 'dd/MM/yyyy hh24:MI:ss')";
		}
		if( criteria.getToDate() != null )
		{
			query +=" and pat.trans_date <= to_date('"+ criteria.getFormattedToDate()+"' , 'dd/MM/yyyy hh24:MI:ss')";
		}
		
		if( criteria.getMasrafBeneficiaryName() != null )
		{
			query +=" and lower( pd.other )like '%"+ criteria.getMasrafBeneficiaryName().toLowerCase() +"%'";
		}
		if( criteria.getMasrafName() != null )
		{
			query +=" and lower(m.masraf_name) like '%"+ criteria.getMasrafName().toLowerCase() +"%'";
		}
		query +="   group by " +
				"				PD.OTHER,cast(TO_CHAR( PAT.TRANS_DATE ,'Mon,dd yyyy') as varchar(11) ),M.MASRAF_NAME," +
				"				m.masraf_id,R.REQUEST_NUMBER,r.request_id " +  
				" 	order by  \"trxDate\", pd.other " ;  
		
		System.out.print("-------------" + query);
		String dataDef = "MasrafBeneficiaryTransDetailsDataDef";        
		reportQueryMap.put(dataDef, query+"");
	
	}
		
	}
