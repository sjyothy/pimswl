package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PendingPaymentsAfterSettlementCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;


public class PendingPaymentsAfterSettlementProcessor extends AbstractReportProcessor {
	
	private Logger logger = Logger.getLogger( PrintLeaseContractReportProcessor.class );

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = " SELECT * FROM PENDING_PAYMENTS_SETTLEMENT ";
		
		PendingPaymentsAfterSettlementCriteria criteria = ( PendingPaymentsAfterSettlementCriteria ) reportCriteria;
		
		query += whereClause(criteria);
		
		query += " ORDER BY CONTRACT_NUMBER, PAYMENT_TYPE, AMOUNT ";
		
		logger.logDebug("QUERY:" + query );
		
		String datadef = ReportConstant.DataDef.PENDING_PAYMENTS_AFTER_SETTLEMENT;
		
		reportQueryMap.put(datadef , query);
		
		String labelQuery = " SELECT "+
		
		
		
		/*******************************                           Arabic Labels             *********************************/
		"  '" + ReportMessageResource.getPropertyAr("contract.contractNumber") + "' \"lblContractNumberAr\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("contract.tenantName") + "' \"lblTenantNameAr\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("settlement.label.grpCustNo") + "' \"lblGRPCustomerNoAr\",\n" +
		
		"  '" + ReportMessageResource.getPropertyAr("grp.propertyName") + "' \"lblPropertyNameAr\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescAr\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("reportPendingPaymentsAfterSettlement.reportSearchPage.lblUnitAccNo") + "' \"lblUnitAccNoAr\",\n" +
		
		"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"lblAmountAr\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("paymentConfiguration.paymentType") + "' \"lblPaymentTypeAr\",\n" +
		
		"  '" + ReportMessageResource.getPropertyAr("report.columnName.serialNumber") + "' \"lblEF1\",\n" +
		    
		"  '" + ReportMessageResource.getPropertyAr("commons.records") + "' \"lblEF2\",\n" +
		"  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblEF3\",\n" +
		
		/*******************************                           Mandatory   Lables          *********************************/ 	        
        
			"  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
	        "  '" + ReportMessageResource.getPropertyEn("reportPendingPaymentsAfterSettlement.reportSearchPage.heading") + "' \"lblReportNameEn\",\n" + 
	        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
	        "  '" + ReportMessageResource.getPropertyAr("reportPendingPaymentsAfterSettlement.reportSearchPage.heading") + "' \"lblReportNameAr\",\n" + 
	        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 
	        " FROM DUAL "; 
		
	    datadef = ReportConstant.DataDef.PENDING_PAYMENTS_AFTER_SETTLEMENT_LABEL;
		
		reportQueryMap.put(datadef , labelQuery);
		
		return reportQueryMap;
	}

	private String whereClause(PendingPaymentsAfterSettlementCriteria criteria) {
		String whereClause = " WHERE 1=1 ";
		
		if (!"".equalsIgnoreCase( criteria.getCONTRACT_NUMBER())) {
			whereClause += " AND LOWER(CONTRACT_NUMBER) LIKE " + "'%" + criteria.getCONTRACT_NUMBER().trim().toLowerCase()+ "%'";
		}
		
		if (!"".equalsIgnoreCase( criteria.getTENANT_NAME())) {
			whereClause += " AND LOWER(TENANT_NAME) LIKE " + "'%" + criteria.getTENANT_NAME().trim().toLowerCase()+ "%'";
		}
		
		
		if (!"".equalsIgnoreCase( criteria.getGRP_CUSTOMER_NO())) {
			whereClause += " AND LOWER(GRP_CUSTOMER_NO) LIKE " + "'%" + criteria.getGRP_CUSTOMER_NO().trim().toLowerCase()+ "%'";
		}
		
		if (!"".equalsIgnoreCase( criteria.getPROPERTY_NAME())) {
			whereClause += " AND LOWER(PROPERTY_NAME) LIKE " + "'%" + criteria.getPROPERTY_NAME().trim().toLowerCase()+ "%'";
		}
		
		if (!"".equalsIgnoreCase( criteria.getUNIT_DESC())) {
			whereClause += " AND LOWER(UNIT_DESC) LIKE " + "'%" + criteria.getUNIT_DESC().trim().toLowerCase()+ "%'";
		}
		
		if (!"".equalsIgnoreCase( criteria.getUNIT_ACC_NO())) {
			whereClause += " AND LOWER(UNIT_ACC_NO) LIKE " + "'%" + criteria.getUNIT_ACC_NO().trim().toLowerCase()+ "%'";
		}
		
		
		//Unit Status
		if(!criteria.getPAYMENT_TYPE_ID().equals("-1")) {
			whereClause+= " AND PAYMENT_TYPE_ID='" + criteria.getPAYMENT_TYPE_ID()+"'";
		}	
		
		// AMOUNT
		if (!"".equalsIgnoreCase( criteria.getAMOUNT_FROM())) {
			whereClause += " AND AMOUNT >= " + criteria.getAMOUNT_FROM();
		}
		

		if (!"".equalsIgnoreCase( criteria.getAMOUNT_TO())) {
			whereClause += " AND AMOUNT <= " + criteria.getAMOUNT_TO();
		}
		
		// SETTLEMENT_DATE
		if( criteria.getSETTLEMENT_DATE_FROM()!=null ) {		 						
			whereClause += " AND to_date(SETTLEMENT_DATE, 'dd/MM/yyyy hh24:MI:ss') >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getSETTLEMENT_DATE_FROM() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		if( criteria.getSETTLEMENT_DATE_TO()!=null ) {								
			whereClause += " AND to_date(SETTLEMENT_DATE, 'dd/MM/yyyy hh24:MI:ss') <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getSETTLEMENT_DATE_TO() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}
		
		// CONTRACT_START_DATE
		if( criteria.getCONTRACT_START_DATE_FROM()!=null ) {		 						
			whereClause += " AND to_date(CONTRACT_START_DATE, 'dd/MM/yyyy hh24:MI:ss') >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getCONTRACT_START_DATE_FROM() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		if( criteria.getCONTRACT_START_DATE_TO()!=null ) {								
			whereClause += " AND to_date(CONTRACT_START_DATE, 'dd/MM/yyyy hh24:MI:ss')  <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getCONTRACT_START_DATE_TO() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}
		
		// CONTRACT_END_DATE
		if( criteria.getCONTRACT_END_DATE_FROM()!=null ) {		 						
			whereClause += " AND to_date(CONTRACT_END_DATE, 'dd/MM/yyyy hh24:MI:ss')  >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getCONTRACT_END_DATE_FROM() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		if( criteria.getCONTRACT_END_DATE_TO()!=null ) {								
			whereClause += " AND to_date(CONTRACT_END_DATE, 'dd/MM/yyyy hh24:MI:ss')  <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getCONTRACT_END_DATE_TO() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}

		
		// PAYMENT_DUE_ON
		if( criteria.getPAYMENT_DUE_ON_FROM()!=null ) {		 						
			whereClause += " AND to_date(PAYMENT_DUE_ON, 'dd/MM/yyyy hh24:MI:ss')   >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getPAYMENT_DUE_ON_FROM() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		if( criteria.getPAYMENT_DUE_ON_TO()!=null ) {								
			whereClause += " AND to_date(PAYMENT_DUE_ON, 'dd/MM/yyyy hh24:MI:ss')   <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getPAYMENT_DUE_ON_TO() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}
				
		// REQUEST_DATE
		if( criteria.getREQUEST_DATE_FROM()!=null ) {		 						
			whereClause += " AND to_date(REQUEST_DATE, 'dd/MM/yyyy hh24:MI:ss')   >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getREQUEST_DATE_FROM() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
		}	
		
		if( criteria.getREQUEST_DATE_TO()!=null ) {								
			whereClause += " AND to_date(REQUEST_DATE, 'dd/MM/yyyy hh24:MI:ss')   <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getREQUEST_DATE_TO() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
		}
		
		return whereClause;
	}

}
