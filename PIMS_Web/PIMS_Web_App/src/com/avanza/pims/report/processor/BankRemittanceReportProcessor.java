package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.BankRemittanceReportCriteria;
import com.avanza.pims.report.criteria.CollectionReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;

public class BankRemittanceReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		
		BankRemittanceReportCriteria criteria = (BankRemittanceReportCriteria) reportCriteria;

		

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////

		String query ="SELECT " +
		" ps.payment_schedule_id \"paymentScheduleId\", "+
        " PROP.COMMERCIAL_NAME  \"propertyName\" , " +
        " PSUP.UNIT_COST_CENTER  \"unitNumber\" , " +
        " CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) , " +

        " CNTRT.CONTRACT_NUMBER  \"contractNumber\" , " +
        " CNTRT.CONTRACT_ID \"contractId\" ,"+
        " REQ.REQUEST_ID  \"requestId\" ,"+
        " REQ.REQUEST_NUMBER \"requestNumber\" ,"+
        
//        -- Tenant Name
        " LTRIM ( PRSN.FIRST_NAME ||' ' || NVL(PRSN.MIDDLE_NAME,'')  || ' '  || PRSN.LAST_NAME  || PRSN.COMPANY_NAME  ) \"tenantName\" , " +
        " PRSN.MIDDLE_NAME   , " +
        " PRSN.LAST_NAME   , " +
        " PRSN.COMPANY_NAME  , " +
        
        " PRCT.RECEIPT_NUMBER \"receiptNumber\" , " +
//      " PRCT.TRANSACTION_DATE \"receiptDate\" ,"+
        
        " CAST(to_char(PRCT.CREATED_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"receiptDate\",\n"+ 
        " prct.PAYMENT_RECEIPT_ID \"receiptId\" ,"+          
//        -- Receipt Status
        " DD_CS.DATA_DESC_EN  \"receiptStatusEn\" , " +
        " DD_CS.DATA_DESC_AR  \"receiptStatusAr\" , " +
        " DD_CT.DATA_DESC_EN  \"chequeTypeEn\" , " +
        " DD_CT.DATA_DESC_AR  \"chequeTypeAr\" , " +       
        
        " PMT.PAYMENT_METHOD_ID  \"paymentMethodEn\" , " +
        " PMT.PAYMENT_METHOD_ID  \"paymentMethodAr\" , " +
        " sUser.FULL_NAME  \"receiptBy\" , " +
        " PS.AMOUNT  \"receiptAmount\" , " +
      // -- Payment Type 
        " PT.DESCRIPTION_EN\"paymentTypeEn\"," +
        " PT.DESCRIPTION_Ar\"paymentTypeAr\"," +
        " PT.PAYMENT_TYPE_ID\"paymentTypeId\"," +
//        -- Cheque Number
        " PRCD.METHOD_REF_NO  \"chequeNumber\" , " +
        " PRCD.METHOD_REF_DATE  \"chequeDate\" , " +
        " PRCD.AMOUNT  \"chequeAmount\" , " +
        " PRCD.PAYMENT_RECEIPT_DETAIL_ID \"paymentReceiptDetailId\" ,"+
//        -- CHQUE TYPE LEFT HERE
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_EN  end \"bankNameEn\" , " +
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_AR  end \"bankNameAr\" ," +

        //" PS.PAYMENT_NUMBER  , " +
        //" PS.PAYMENT_DUE_ON  , " +
        //" PS.PAYMENT_TYPE_ID , " +
        //" PYT.DESCRIPTION_EN " +

        " FAC.bank_remitance_account_name  \"bankremitanceaccountname\" ,"+
        " FAC.GRP_ACCOUNT_NUMBER \"grpAccountNumber\","+
        
        " '' \"actionNumber\","+
        " '' \"facDesc\","+
        " '' \"facTrxTypeName\","+
        " '' \"facTrxTypeDebit\","+
        " '' \"memoLineName\","+
        " '' \"facReceiptTypeId\","+
        " '' \"facActivityName\""+
        " FROM payment_schedule ps "+  
        " LEFT JOIN "+ 
		" contract cntrt ON ps.contract_id = cntrt.contract_id "+ 
		
        " LEFT JOIN PAYMENT_SCH_UNIT_PROP PSUP ON PS.PAYMENT_SCHEDULE_ID = PSUP.PAYMENT_SCHEDULE_ID "+
        " LEFT JOIN PAYMENT_TYPE PT ON PS.PAYMENT_TYPE_ID = PT.PAYMENT_TYPE_ID "+
        " LEFT JOIN PROPERTY PROP ON PROP.PROPERTY_ID = PSUP.PROPERTY_ID " +
        " LEFT JOIN REQUEST REQ ON PS.REQUEST_ID = REQ.REQUEST_ID " +
        " LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
        " LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
        " LEFT JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
        " LEFT JOIN DOMAIN_DATA DD_CS ON PS.STATUS_ID = DD_CS.DOMAIN_DATA_ID " +
        " LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " + 
        " ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " + 
        " LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
        " INNER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
        " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
        " LEFT JOIN DOMAIN_DATA DD_CT ON PRCD.CHEQUE_TYPE = DD_CT.DOMAIN_DATA_ID " +
        " LEFT JOIN financial_acc_configuration fac ON (    " +
        "                                                 ps.ownership_type_id =fac.owner_ship_type_id "+
        "                                                 AND ps.payment_type_id = fac.payment_type_id "+ 
        "                                                 and pmt.payment_method_id =fac.payment_method_id "+
        "                                               ) "+

        " LEFT OUTER JOIN  payment_receipt prct ON PRCD.PAYMENT_RECEIPT_ID = prct.PAYMENT_RECEIPT_ID "+ 
        " LEFT JOIN person prsn ON prct.paid_by = prsn.person_id"+
        " LEFT JOIN SEC_USER sUser on prct.created_by = sUser.login_id ";
		////////////////////////////////
	                    
				
		String condition="";
							
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
			
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(PSUP.UNIT_NUMBER)  like  lower('%" + criteria.getUnitNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
													" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
													" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
													" ) " ;
							
							if(criteria.getReceiptNumber().length() >0)
								condition+=" AND lower(PRCT.RECEIPT_NUMBER) like  lower('%" + criteria.getReceiptNumber() +"%')";
							
							if(criteria.getReceiptDateFrom() !=null  )								
							  condition += " AND PRCT.CREATED_ON >=" +
									"to_date ('"+ 
							                     DateUtil.convertToDisplayDate( criteria.getReceiptDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  " ;
							        
                             if( criteria.getReceiptDateTo()!=null )
                            	 condition += " AND PRCT.CREATED_ON  <=  to_date ('" + DateUtil.convertToDisplayDate( criteria.getReceiptDateTo())+
                            		                                         " 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";
							if(criteria.getReceiptBy().length() >0 && ! criteria.getReceiptBy().equals( "-1" ) )
								condition+=" AND PRCT.CREATED_BY like  '%" + criteria.getReceiptBy() +"%'";
							 
							
							if(!criteria.getBankName().equals("-1"))
								condition+= " AND BANK.BANK_ID  =  " + criteria.getBankName();
							
							if(!criteria.getChequeType().equals("-1"))
								condition+= " AND PRCD.CHEQUE_TYPE  =  " + criteria.getChequeType();
							
							if(!criteria.getPaymentMethod().equals("-1"))
								{
								String paymentMethodCriteria;
								if(criteria.getPaymentMethod().equals("34001"))
									paymentMethodCriteria="1";
								else if(criteria.getPaymentMethod().equals("34002"))
									paymentMethodCriteria="2";
								else if(criteria.getPaymentMethod().equals("34003"))
									paymentMethodCriteria="4";
								else
									paymentMethodCriteria="3";
//								condition+= " AND PRCD.PAYMENT_METHOD_ID  =  " + criteria.getPaymentMethod() +""	;
//								condition+= " AND PMT.DESCRIPTION  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getPaymentMethod() +"')"	;
								condition+= " AND PRCD.PAYMENT_METHOD_ID  = " + paymentMethodCriteria;
								}
							
							if(!criteria.getPaymentType().equals("-1"))
								condition+= " AND PS.PAYMENT_TYPE_ID  =  '" + criteria.getPaymentType() +"'"	;
							
							if(!criteria.getRemittanceAccountName().equals("-1") && !criteria.getRemittanceAccountName().equals("%%"))
								condition+= " AND FAC.BANK_REMITANCE_ACCOUNT_NAME ='" + criteria.getRemittanceAccountName()+"'";

							if(!criteria.getRemittanceAccountName().equals("-1") && criteria.getRemittanceAccountName().equals("%%"))
								condition+= " AND FAC.BANK_REMITANCE_ACCOUNT_NAME LIKE'" + criteria.getRemittanceAccountName()+"'";
							
							
							List<DomainDataView> psList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
							DomainDataView ddv = CommonUtil.getIdFromType( psList, WebConstants.CANCELED);
					            condition+= " AND PS.MIGRATED = 0 AND PRCD.IS_DELETED=0 AND PRCD.RECORD_STATUS=1 AND PRCT.IS_DELETED=0 AND PRCT.RECORD_STATUS=1 " +
					            		"  AND PS.STATUS_ID <>" +ddv.getDomainDataId();
					            	
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY PRCT.PAYMENT_RECEIPT_ID, PRCD.PAYMENT_METHOD_ID,PRCD.BANK_ID,PS.AMOUNT  ";
//							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.BANK_REMMITANCE_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblReceiptStatusEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receipt.receiptBy") + "' \"lblReceiptByEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.receiptAmount") + "' \"lblReceiptAmountEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.amountCol") + "' \"lblChequeAmountEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" +

        		"  '" + ReportMessageResource.getPropertyEn("chequeList.chequeType") + "' \"lblChequeTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.chequeDate") + "' \"lblChequeDateEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("collection.transactionType") + "' \"lblTransactionTypeEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.cash") + "' \"lblPMCashEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("payment.draft") + "' \"lblPMDraftEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.cheque") + "' \"lblChequeEn\",\n" +
        		
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblReceiptStatusAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receipt.receiptBy") + "' \"lblReceiptByAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.receiptAmount") + "' \"lblReceiptAmountAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"lblChequeAmountAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.chequeType") + "' \"lblChequeTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.chequeDate") + "' \"lblChequeDateAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("collection.transactionType") + "' \"lblTransactionTypeAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.cash") + "' \"lblPMCashAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("payment.draft") + "' \"lblPMDraftAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.cheque") + "' \"lblChequeAr\",\n" +
        		
		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.collectionReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.collectionReport.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.COLLECTION_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;

	}
			

}

