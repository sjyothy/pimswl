package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PrintLeaseContractReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.TransferContract;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;

public class PrintLeaseContractReportProcessor extends AbstractReportProcessor 
{
	private transient Logger logger = Logger.getLogger( PrintLeaseContractReportProcessor.class );
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		PrintLeaseContractReportCriteria printLeaseContractReportCriteria = (PrintLeaseContractReportCriteria) reportCriteria;
//        String query =  "SELECT \n" + 
//				        "  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) \"contractId\",\n" + 
//				        "  to_char( CNTRT.CONTRACT_DATE , 'dd-MON-yyyy' ) \"contractDate\",\n" + 
//				        "  CNTRT.CONTRACT_NUMBER \"contractNumber\",\n" + 
//				        "  NVL( U.UNIT_DESC, '---') \"contractRemarks\",\n" + 
//				        "  to_char( CNTRT.START_DATE , 'dd-MON-yyyy' ) \"contractStartDate\",\n" + 
//				        "  to_char( CNTRT.END_DATE , 'dd-MON-yyyy' ) \"contractEndDate\",\n" + 
//				        "  to_char( CNTRT.ORIGINAL_START_DATE , 'dd-MON-yyyy' ) \"contractOriginalStartDate\",\n" + 
//				        "  to_char( CNTRT.ORIGINAL_END_DATE , 'dd-MON-yyyy' ) \"contractOriginalEndDate\",\n" + 
//				        "  CNTRT.RENT_AMOUNT \"contractRentAmount\",\n" + 
//				        "  CNTRT.RENT_AMOUNT \"contractAskingPrice\",\n" + 
//				        "  CNTRT.CONTRACT_TYPE_ID \"contractType\",\n" +
//				        "  U.UNIT_NUMBER \"unitNumber\",\n" +
//				        "  coalesce(cast ( U.unit_ew_utility_no as varchar(500)),cast(u.dewa_number as varchar(500)))  \"dewaNumber\",\n" +
//				       
//				        "  case when PROP.PROPERTY_NAME_EN  is not null then " +
//				        " 					REPLACE (PROP.PROPERTY_NAME,'-',' ')  ||' / '|| REPLACE (PROP.PROPERTY_NAME_EN,'-',' ')  ||'/'||NVL( U.ACCOUNT_NUMBER, '---' )  " +
//				        "  else  " +
//				        " 					REPLACE (PROP.PROPERTY_NAME,'-',' ')  ||'/'||NVL( U.ACCOUNT_NUMBER, '---' )  " +
//				        "  end \"propertyName\",\n" +
//				        
//				        "  case when PROP.PROPERTY_NAME_EN  is not null then " +
//				        "			REPLACE (PROP.COMMERCIAL_NAME,'-',' ')  "+//"    REPLACE (PROP.COMMERCIAL_NAME,'-',' ') ||' / '||NVL( U.ACCOUNT_NUMBER, '---' )  " +//"					REPLACE (PROP.COMMERCIAL_NAME,'-',' ') ||' / '|| REPLACE (PROP.PROPERTY_NAME_EN,'-',' ')  ||' / '||NVL( U.ACCOUNT_NUMBER, '---' ) " +
//				        "  else 			REPLACE (PROP.COMMERCIAL_NAME,'-',' ')  " +
//				        "  end \"propertyCommercialName\",\n" +
//				        " case when Instr(COMMERCIAL_NAME ,U.ACCOUNT_NUMBER) >0  then '' else (NVL( U.ACCOUNT_NUMBER, '---' )|| ' / ' ) end \"field2\", "+
//				        "  	REPLACE (PROP.PROPERTY_NAME_EN,'-',' ') ||' / '||NVL( U.ACCOUNT_NUMBER, '---' )   \"field1\",\n" +
//				        "  DD.DATA_DESC_EN \"domainDataOwnerNameEn\",\n" + 
//				        "  DD.DATA_DESC_AR \"domainDataOwnerNameAr\",\n" + 
//				        "  CI.ADDRESS1 \"contactInfoAddress1\",\n" + 
//				        "  CI.ADDRESS2 \"contactInfoAddress2\",\n" + 
//				       
//				        "  PRSN.PASSPORT_NUMBER \"passportNumber\",\n" +
//				        "  to_char( PRSN.PASSPORT_EXPIRY_DATE , 'dd-MON-yyyy' ) \"passportExpiryDate\",\n" +
//				        "  PRSN.RESIDENSE_VISA_NUMBER \"visaNumber\",\n" +
//				        "  to_char( PRSN.RESIDENSE_VIDA_EXP_DATE , 'dd-MON-yyyy' ) \"visaExpiryDate\",\n" +
//				        "  RGN_NATIONALITY.DESCRIPTION_AR \"nationality\",\n" +
//				        "  RGN_CITY.DESCRIPTION_AR \"community\",\n" +
//				        
//				        "  CI.OFFICE_PHONE \"officePhone\",\n" +
//				        "  CI.STREET \"street\",\n" +
//				        "  PRSN.DESIGNATION \"designation\",\n" +
//				        
//				        
//				        "  PRSN.FIRST_NAME \"personFirstName\",\n" + 
//				        "  PRSN.MIDDLE_NAME \"personMiddleName\",\n" + 
//				        "  PRSN.LAST_NAME \"personLastName\",\n" + 
//				        "  PRSN.COMPANY_NAME \"personCompanyName\",\n" + 
//				        "  PRSN.FULL_NAME_EN \"personFullNameEn\"\n" +
//				        "  FROM \n" + 
//				        "  CONTRACT CNTRT \n" + 
//				        "  INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID\n" + 
//				        "  INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID\n" + 
//				        "  INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID \n" + 
//				        "  INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID\n" + 
//				        "  INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID \n" + 
//				        "  LEFT JOIN REGION RGN_NATIONALITY ON RGN_NATIONALITY.REGION_ID = PRSN.NATIONALITY_ID \n"+ 
//				        "  INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID\n" + 
//				        "  INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID\n" + 
//				        "  LEFT JOIN REGION RGN_CITY ON RGN_CITY.REGION_ID = CI.CITY_ID \n "+
//				        "  WHERE\n" + 
//				        "  CNTRT.CONTRACT_ID = " + printLeaseContractReportCriteria.getContractId();
		String query =  " select * from lease_contract_report_view where \"contractId\" = " +printLeaseContractReportCriteria.getContractId();
        String dataDef = ReportConstant.DataDef.PRINT_LEASE_CONTRACT;        
        reportQueryMap.put(dataDef, query);
        
        query = "SELECT\n" + 
		        "  PS.PAYMENT_SCHEDULE_ID \"paymentScheduleId\",\n" + 
		        "  PS.PAYMENT_TYPE_ID \"paymentTypeId\",\n" +
		        "  PS.PAYMENT_NUMBER \"paymentScheduleNumber\",\n" + 
		        "  CAST( PS.CONTRACT_ID AS VARCHAR(10) ) \"contractId\",\n" +
		        "  CAST( PS.PAYMENT_DUE_ON AS VARCHAR(10) ) \"paymentScheduleDueOn\",\n" + 
		        "  to_char( PS.PAYMENT_DATE , 'dd-MON-yyyy' ) \"paymentSchedulePaymentDate\",\n" +
		        "  cast( PS.PAYMENT_MODE_ID  AS VARCHAR(10) )  \"paymentModeId\",\n" +
		        "  PS.AMOUNT \"paymentScheduleAmount\",\n" + 
		        "  PT.DESCRIPTION_EN \"paymentTypeDescEn\",\n" + 
		        "  PT.DESCRIPTION_AR \"paymentTypeDescAr\",\n" + 
		        "  DDSTATUS.DATA_DESC_EN \"domainDataStatusEn\",\n" + 
		        "  DDSTATUS.DATA_DESC_AR \"domainDataStatusAr\",\n" + 
		        "  DDMODE.DATA_DESC_EN \"domainDataPaymentModeEn\",\n" + 
		        "  DDMODE.DATA_DESC_AR \"domainDataPaymentModeAr\",\n" + 
		        "  NVL(PRD.METHOD_REF_NO, '---') \"paymentRecptDetailMethodRefNo\",\n" + 
		        "  CASE WHEN PRD.MIGRATED IS NOT NULL AND PRD.MIGRATED =1 THEN PRD.BANK_NAME else NVL(B.BANK_EN, '---') end \"bankEn\",\n" + 
		        "  CASE WHEN PRD.MIGRATED IS NOT NULL AND PRD.MIGRATED =1 THEN PRD.BANK_NAME else NVL(B.BANK_AR, '---') end \"bankAr\"\n" + 
		        "  FROM\n" + 
		        "  PAYMENT_SCHEDULE PS \n" + 
		        "  INNER JOIN PAYMENT_TYPE PT ON PS.PAYMENT_TYPE_ID = PT.PAYMENT_TYPE_ID\n" + 
		        "  INNER JOIN DOMAIN_DATA DDSTATUS ON PS.STATUS_ID = DDSTATUS.DOMAIN_DATA_ID \n" + 
		        "  INNER JOIN DOMAIN_DATA DDMODE ON PS.PAYMENT_MODE_ID = DDMODE.DOMAIN_DATA_ID   \n" + 
		        "  LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRT ON PS.PAYMENT_SCHEDULE_ID = PRT.PAYMENT_SCHEDULE_ID\n" + 
		        "  LEFT JOIN PAYMENT_RECEIPT_DETAIL PRD ON PRT.PAYMENT_RECEIPT_DETAIL_ID = PRD.PAYMENT_RECEIPT_DETAIL_ID\n" +
		        "  LEFT JOIN BANK B ON B.BANK_ID = PRD.BANK_ID\n" + 
		        "  WHERE \n" + 
		        "  PS.CONTRACT_ID = " + printLeaseContractReportCriteria.getContractId()+
		        "  AND PS.IS_DELETED=0 AND PS.RECORD_STATUS=1 AND " +
		        "  DDSTATUS.DATA_VALUE IN( 'PAYMENT_SCHEDULE_COLLECTED','PAYMENT_SCHEDULE_PENDING','REALIZED'," +
		        "                          'PAYMENT_SCHEDULE_STATUS_DRAFT','PAID')";
        dataDef = ReportConstant.DataDef.PRINT_LEASE_CONTRACT_PAYMENT_SCHEDULE;        
        reportQueryMap.put(dataDef, query);
        
        query = "SELECT \n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.date") + "' \"lblDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.date") + "' \"lblDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNoEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNoAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.tab.unit.propertyname") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.tab.unit.propertyname") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("attachment.docDesc") + "' \"lblDescriptionEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("attachment.docDesc") + "' \"lblDescriptionAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.elementAddress") + "' \"lblElementAddressEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.elementAddress") + "' \"lblElementAddressAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.ownerName") + "' \"lblOwnerNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.ownerName") + "' \"lblOwnerNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.tenantNameCol") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.tenantNameCol") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.periodOfTenancy") + "' \"lblPeriodOfTenancyEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.periodOfTenancy") + "' \"lblPeriodOfTenancyAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.from") + "' \"lblFromEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.from") + "' \"lblFromAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.to") + "' \"lblToEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.to") + "' \"lblToAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.rentAmount") + "' \"lblRentEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.rentAmount") + "' \"lblRentAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.askingPrice") + "' \"lblAskingPriceEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.askingPrice") + "' \"lblAskingPriceAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblDueOnEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblDueOnAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("attachment.grid.docTypeCol") + "' \"lblTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("attachment.grid.docTypeCol") + "' \"lblTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.refNo") + "' \"lblRefNoEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.refNo") + "' \"lblRefNoAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("application.status.gridHeader") + "' \"lblStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("application.status.gridHeader") + "' \"lblStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentMode") + "' \"lblModeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentMode") + "' \"lblModeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bank") + "' \"lblBankEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bank") + "' \"lblBankAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.amount") + "' \"lblAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.amount") + "' \"lblAmountAr\" \n" +
		        "  FROM DUAL";
        dataDef = ReportConstant.DataDef.PRINT_LEASE_CONTRACT_LABEL;        
        reportQueryMap.put(dataDef, query); 
		
        //For Checking whether report is created before Collect Payment i.e before it became active
        //Start
        List<DomainDataView> contractStatusList = CommonUtil.getDomainDataListForDomainType( WebConstants.CONTRACT_STATUS ); 
        DomainDataView activeStatus = CommonUtil.getIdFromType( contractStatusList , WebConstants.CONTRACT_STATUS_ACTIVE );
        dataDef = ReportConstant.DataDef.PRINT_LEASE_CONTRACT_PARAMETER;
        String printBeforePaymentCollected = "0";
        String messageForPrintBeforePaymentCollected ="";
        String messageArForPrintBeforePaymentCollected = "";
        try
        {
	        UtilityServiceAgent usa = new UtilityServiceAgent();
	        messageForPrintBeforePaymentCollected = usa.getValueFromSystemConfigration( WebConstants.MSG_EN_PRINT_CONTRACT_BEFORE_ACTIVE);
	        messageArForPrintBeforePaymentCollected = usa.getValueFromSystemConfigration( WebConstants.MSG_AR_PRINT_CONTRACT_BEFORE_ACTIVE);
	        if ( printLeaseContractReportCriteria.getStatusId()!= null &&
	        	 new Long ( printLeaseContractReportCriteria.getStatusId() ).compareTo( activeStatus.getDomainDataId() ) != 0)
	        {
	        	printBeforePaymentCollected = "1";
	        	new PropertyService().markContractAsPrintBeforeActive( new Long ( printLeaseContractReportCriteria.getContractId() ), 
	        			                                               CommonUtil.getLoggedInUser() );
	        }
        }
        catch( Exception e)
        {
        	logger.LogException("Error Occured while fetch En/Ar Msg from System Config", e);
        }
    	query = "SELECT \n " + 
                " '"+ printBeforePaymentCollected +"'\"printBeforePaymentCollected\" ,\n"+
    	        " '"+ messageForPrintBeforePaymentCollected +"'\"messageBeforePaymentCollected\" ,\n "+
    	        " '"+ messageArForPrintBeforePaymentCollected +"'\"msgArBeforePaymentCollected\" \n "+
    	        " FROM DUAL";

        reportQueryMap.put(dataDef, query);
        //Finish
        return reportQueryMap;
	}
	
	

}
