package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.MaintenanceReportCriteria;

public class MaintenanceReportProcessor  extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		MaintenanceReportCriteria criteria = (MaintenanceReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);		
		String query = "SELECT  *  FROM  MAINTENANCE_REPORT_VIEW ";
	    String condition="";
		if(criteria.getContractNumber().length() >0)
			condition+=" AND lower(\"leaseContractNumber\") like   lower('%" + criteria.getContractNumber().trim() +"%')";
		if(criteria.getRequestNumber().trim().length() >0 )
			condition+=" AND lower(\"requestNumber\") like   lower('%" + criteria.getRequestNumber().trim() +"%')";
				
		if(condition.length() > 0)
	    	query+=" where 1=1  \n"+ condition;
//		String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER ASC, ps.payment_type_id ASC, ps.payment_due_on ASC, ps.payment_number ASC";
//		query+=sorting;

		System.out.print("-------------" + query);
        String dataDef = "MaintenanceReportDataDef";        
        reportQueryMap.put(dataDef, query+"");
		
        
        
        return reportQueryMap;
	}


}
