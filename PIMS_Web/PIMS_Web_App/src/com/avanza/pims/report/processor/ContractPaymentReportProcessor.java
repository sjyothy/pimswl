package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractPaymentReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ContractPaymentReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		ContractPaymentReportCriteria criteria = (ContractPaymentReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
	
				String query=" SELECT * FROM CONTRACT_PAYMENTS ";
				
		String condition = " WHERE 1=1 ";
			if(criteria.getContractId() != null )
					condition += " AND CONTRACTID = "+ criteria.getContractId();
		query 	+= condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PAYMENT_RECEIPT_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        
		query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/

				"  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("unit.unitDescription") + "' \"lblUnitDescEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.customerName") + "' \"lblCustomerNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("attachment.docDesc") + "' \"lblDescriptionEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.period") + "' \"lblPeriodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblReceiptTotalAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.amountCol") + "' \"lblAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.chequeNumberORTransferNumber") + "' \"lblChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.costCenter") + "' \"lblCostCenterEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.grpCustNo") + "' \"lblCustomerNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentNumber") + "' \"lblPaymentNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cheque.amount") + "' \"lblChequeAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldBankName") + "' \"lblOldBankEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeNumber") + "' \"lblOldChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeDate") + "' \"oldChequeDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeAmount") + "' \"lblOldChequeAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("applicationDetails.ownershipType") + "' \"lblOwnershipTypeEn\",\n" +
		        
		        
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
				"  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.customerName") + "' \"lblCustomerNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("attachment.docDesc") + "' \"lblDescriptionAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.period") + "' \"lblPeriodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblReceiptTotalAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"lblAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.chequeNumberORTransferNumber") + "' \"lblChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPayMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.costCenter") + "' \"lblCostCenterAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.grpCustNo") + "' \"lblCustomerNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentNumber") + "' \"lblPaymentNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cheque.amount") + "' \"lblChequeAmountArss\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldBankName") + "' \"lblOldBankAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeNumber") + "' \"lblOldChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeDate") + "' \"oldChequeDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeAmount") + "' \"lblOldChequeAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("applicationDetails.ownershipType") + "' \"lblOwnershipTypeAr\",\n" +

		        

		        /*******************************                           Mandatory Info             *********************************/	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.rpt") + "' \"lblReportNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn(criteria.getReportProcedureNameEn())  + "' \"lblProcedureEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr(criteria.getReportProcedureNameAr())  + "' \"lblProcedureAr\",\n" +
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 
		        
		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PAYMENT_RECEIPT_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
