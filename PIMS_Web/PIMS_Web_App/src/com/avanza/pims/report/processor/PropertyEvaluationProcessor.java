package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PropertyEvaluationCriteria;
import com.avanza.pims.web.WebConstants;

public class PropertyEvaluationProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		PropertyEvaluationCriteria criteria = (PropertyEvaluationCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String text="Test";
		System.out.println(""+text);
		String propertyEvalQuery = "SELECT " ;
		
		propertyEvalQuery += 

"PROP.COMMERCIAL_NAME propertyName, " +
"PROP.PROPERTY_NUMBER propertyNumber, " +

"EVRST.RECOMMENDATION inspectorReport, " +
"EVRST.CREATED_BY inspectorName, " +

"(SELECT CONFIGVALUE FROM SYSTEM_CONFIGURATION WHERE CONFIGKEY='"+WebConstants.SYS_CONFIG_KEY_PROPERTY_HOS_NAME+"') hosName " +

"FROM REQUEST REQ " +

"INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = REQ.PROPERTY_ID " +
"INNER JOIN EVALUATION_RESULTS EVRST ON EVRST.REQUEST_ID=REQ.REQUEST_ID ";
		
		String propertyEvalCondition="WHERE REQ.REQUEST_ID='" + criteria.getRequestId() +"'"; 
		propertyEvalQuery+=propertyEvalCondition;
		
		String propertyEvalDataDef = ReportConstant.DataDef.PROPERTY_EVALUATION_REPORT;        
		reportQueryMap.put(propertyEvalDataDef, propertyEvalQuery+"");
		
		
		addPropertyEvalDetailSubReport(criteria,reportQueryMap);
		addHosRecommendationSubReport(criteria,reportQueryMap);
		addRentCommitteRecommendationSubReport(criteria,reportQueryMap);
		
        return reportQueryMap;
	}
	
	
	private void addPropertyEvalDetailSubReport(PropertyEvaluationCriteria criteria, Map<String, String> reportQueryMap) {
		
		String propertyEvalDetailQuery = "SELECT ";
		
		if (criteria.isWholeProperty()) { // whole property
			
			propertyEvalDetailQuery +=
			
			"DISTINCT UNT.UNIT_TYPE_ID unitTypeId, " +
			"EVRST.PROPERTY_VALUE newRentValue, " +
			"(SELECT DATA_DESC_AR FROM DOMAIN_DATA WHERE DOMAIN_DATA_ID=UNT.UNIT_TYPE_ID) unitTypeAr, " +
			"'- - -' toArea, " +
			"'- - -' fromArea, " +
			"'- - -' frontSide " +
			
			"FROM REQUEST REQ " +
			
			"INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = REQ.PROPERTY_ID " +
			"INNER JOIN FLOOR FLR ON FLR.PROPERTY_ID = PROP.PROPERTY_ID " +
			"INNER JOIN UNIT UNT ON UNT.FLOOR_ID = FLR.FLOOR_ID " +
			"INNER JOIN EVALUATION_RESULTS EVRST ON EVRST.REQUEST_ID=REQ.REQUEST_ID " +			
			
			"WHERE REQ.REQUEST_ID='"+criteria.getRequestId()+"'";
			
			
		} else { // unit type
			
			propertyEvalDetailQuery +=
				"REQD.UNIT_TYPE_ID unitTypeId, " +
				"UNIT_TYPE_AMOUNT newRentValue, " +
				"AREA_TO toArea, " +
				"AREA_FROM fromArea, " +
				"(SELECT DATA_DESC_AR FROM DOMAIN_DATA WHERE DOMAIN_DATA_ID=REQD.UNIT_SIDE_ID) frontSide, " +
				"(SELECT DATA_DESC_AR FROM DOMAIN_DATA WHERE DOMAIN_DATA_ID=REQD.UNIT_TYPE_ID) unitTypeAr " +
				"FROM REQUEST_DETAIL REQD " +
				"WHERE REQD.REQUEST_ID='"+criteria.getRequestId()+"'";
				
		}
		
		//propertyEvalDetailQuery += " --//propertyEvalDetail";
		
		String propertyEvaluationUnitDetailDataDef = ReportConstant.DataDef.PROPERTY_EVALUATION_UNIT_DETAIL_REPORT + 
		"#" + "propertyEvalDetail";        
		reportQueryMap.put(propertyEvaluationUnitDetailDataDef, propertyEvalDetailQuery+"");
		
	}
	
	
	private void addHosRecommendationSubReport(PropertyEvaluationCriteria criteria, Map<String, String> reportQueryMap) {
		
		String condition = " WHERE TYPE_ID=(SELECT DOMAIN_DATA_ID FROM DOMAIN_DATA WHERE DATA_VALUE='" + WebConstants.EvaluationApplication.HOS_RECOMMENDATION+ "')";
		condition += " AND REQUEST_ID='" + criteria.getRequestId() + "'";
		//condition += " --//hosRecommendation";
		
		String query = "SELECT " ;
		query += "TO_CHAR(CREATED_ON,'DD/MM/YYYY HH:MM:SS AM') dateTime, " +
				 "TEXT recommendation " ;
		query +=" FROM EVALUATION_RECOMMENDATION ";
		query += condition;
		
		System.out.println("-- QUERY --" + query);
		String dataDef = ReportConstant.DataDef.HOS_RECOMMENDATION_SUBREPORT + "#" + "hosRecommendation";        
		reportQueryMap.put(dataDef, query+"");
		
	}
	
	private void addRentCommitteRecommendationSubReport(PropertyEvaluationCriteria criteria, Map<String, String> reportQueryMap) {
		
		String condition = " WHERE TYPE_ID=(SELECT DOMAIN_DATA_ID FROM DOMAIN_DATA WHERE DATA_VALUE='" + WebConstants.EvaluationApplication.RENT_COMMITTE_RECOMMENDATION+ "')";
		condition += " AND REQUEST_ID='" + criteria.getRequestId() + "'";
		//condition += " --//rentCommitteRecommendation";
		
		String query = "SELECT " ;
		query += "TO_CHAR(CREATED_ON,'DD/MM/YYYY HH:MM:SS AM') dateTime, " +
				 "TEXT recommendation " ;
		query +=" FROM EVALUATION_RECOMMENDATION ";
		query += condition;
		
		System.out.println("-- QUERY --" + query);
		String dataDef = ReportConstant.DataDef.RENT_COMMITTE_RECOMMENDATION_SUBREPORT + "#" + "rentCommitteRecommendation";        
		reportQueryMap.put(dataDef, query+"");
		
	}
			

}
