package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.TenantStatementReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantStatementReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		TenantStatementReportCriteria criteria = (TenantStatementReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////
		String query = "SELECT " +
							//	-- Tenant Name
						" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  || '|' || PRSN.GRP_CUSTOMER_NO  \"tenantName\", " +
				       " PRSN.MIDDLE_NAME , " +
						" PRSN.LAST_NAME , " +
						" PRSN.COMPANY_NAME, " +
						" CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) , " +
						"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
						"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
						"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
						" DD_TT.DATA_DESC_EN ,\n"+
					     " DD_TT.DATA_DESC_AR ,\n"+
					     " PRSN.PERSON_TYPE ,\n"+
					     " PRSN.PERSON_ID  \"tenantId\",\n"+
					     
				
						" PROP.COMMERCIAL_NAME \"propertyName\" , " +
						" U.UNIT_NUMBER \"unitNumber\" , " +
				       " CNTRT.RENT_AMOUNT \"contractRentValue\" , " +
				       
				       " NVL(PRCT.RECEIPT_NUMBER, '---') \"receiptNumber\" , " +
//				       " PRCT.TRANSACTION_DATE \"contractNumber\" , " +
				       " PYT.DESCRIPTION_EN \"paymentTypeEn\" , " +
				       " PYT.DESCRIPTION_AR \"paymentTypeAr\" , " +
						"PMT.DESCRIPTION \"paymentMethod\" , " +
				       
				       " PRCD.METHOD_REF_NO  , " +
				       " PRCD.METHOD_REF_DATE , " +
				       " PRCD.CHEQUE_OWNER  , " +
				//       -- Cheque Status
				       " DD_CS.DATA_DESC_EN \"statusEn\" , " +
				       " DD_CS.DATA_DESC_AR \"statusAr\" , " +
				       " DD_CS.DOMAIN_DATA_ID\"statusID\" , " +
				       
				       " BANK.BANK_EN  , " +
				       " BANK.BANK_AR  , " +
						"CAST(to_char(PRCT.CREATED_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"receiptTransactionDate\" , " +
				       " PRCD.AMOUNT \"receiptAmount\" " +
				       
				       
				       
				       " FROM  " +
						" CONTRACT CNTRT  " +
						" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
						" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
						" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID  " +
						" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
						" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID  " +
						" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
						" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
						" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
				       " INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
				       " INNER JOIN DOMAIN_DATA DD_CS ON PS.STATUS_ID = DD_CS.DOMAIN_DATA_ID " +
						       
					   " INNER JOIN PAYMENT_RECEIPT_TERMS PRC  " +
				       " ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID  " +
				                                      
	                   " INNER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
				       " INNER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
				       " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
				       " INNER  JOIN payment_receipt prct ON prcd.PAYMENT_RECEIPT_ID = prct.PAYMENT_RECEIPT_ID " +
				        " LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;
				       
				//       --  Total Outstanding  = Sum of Transaction with status ( Pending ) 
				//       --  Total Receipt  = Sum of Transaction with status ( Collected ( Cheque + Cash ) ) 
				//       --  Total Cleared  = Sum of Transaction with status ( Realized ( Cheque ) + Collected ( Cash ) ) 
				       
				       
	                    
				String condition="";
				
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
							
						/*		condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
												" ) " ;*/
							if(!criteria.getTenantType().equals("-1"))
							{
								String tenantTypeCriteria;
								if(criteria.getTenantType().equals("26002"))
									tenantTypeCriteria="0";
								else 
									tenantTypeCriteria="1";
								condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
							}	
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like  lower('%" + criteria.getUnitNumber() +"%')";

							
					
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER , PS.PAYMENT_DUE_ON";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.TENANT_STATEMENT;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.startDate") + "' \"lblContractStartDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.tab.unit.rent") + "' \"lblContractRentValueEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("payment.receiptAmount") + "' \"lblReceiptAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptTransactionDateEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.startDate") + "' \"lblContractStartDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.tab.unit.rent") + "' \"lblContractRentValueAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("payment.receiptAmount") + "' \"lblReceiptAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptTransactionDateAr\",\n" +

		        /*******************************                           Summary Lables           *********************************/	        

		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.totalOutstandingAmount") + "' \"lblTotalOutstandingAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblTotalReceiptAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.totalClearedAmount") + "' \"lblTotalClearedAmountEn\",\n" + 

		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.totalOutstandingAmount") + "' \"lblTotalOutstandingAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblTotalReceiptAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.totalClearedAmount") + "' \"lblTotalClearedAmountAr\",\n" + 

		    	

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.TENANT_STATEMENT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
