package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.VendorOutstandingPaymentReportCriteria;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;


public class VendorOutstandingPaymentReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		VendorOutstandingPaymentReportCriteria criteria = (VendorOutstandingPaymentReportCriteria) reportCriteria;
 

 		String query = "SELECT  PS.PAYMENT_NUMBER \"paymentNumber\"," +
									"CAST( to_char(PS.PAYMENT_DUE_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDate\" , " +				
									"NVL(PYT.DESCRIPTION_EN, '---')  \"paymentTypeEn\"," +
									"NVL(PYT.DESCRIPTION_AR, '---')   \"paymentTypeAr\"," +
									"PS.AMOUNT \"paymentAmount\" ," +
									"PRSN.ACCOUNT_NUMBER \"vendorNumber\" , " +
									"PRSN.COMPANY_NAME \"vendorName\"," +
									"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
									"DD.DATA_DESC_EN \"paymentStatusEn\"," +
									"DD.DATA_DESC_AR \"paymentStatusAr\"," +
							//		"PS.IS_RECEIVED ," +                                  // used here just for testing purpose
									"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
									"DD2.DATA_DESC_AR  \"contractTypeAr\" ," +
									" CNTRT.CONTRACT_TYPE_ID, "+
									" PYT.PAYMENT_TYPE_ID, "+
									" PS.STATUS_ID "+
									
									" FROM " +
									" CONTRACT CNTRT " +
									" INNER JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID = PRSN.PERSON_ID " +
									" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
									" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC  " +
									" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID  " +
									" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
									" INNER JOIN DOMAIN_DATA DD ON PS.STATUS_ID = DD.DOMAIN_DATA_ID "+
									" INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID" ;

				String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							
							if(criteria.getVendorName().length() >0)
							condition+= " AND lower(PRSN.COMPANY_NAME) like lower( '%" + criteria.getVendorName() +"%')" ;
							
							if(criteria.getVendorNumber().length() >0)
								condition+=" AND PRSN.ACCOUNT_NUMBER =  " + criteria.getVendorNumber();
							if(!criteria.getContractType().equals("-1"))
								condition+= " AND CNTRT.CONTRACT_TYPE_ID  = " +criteria.getContractType();
							
							if(!criteria.getPaymentType().equals("-1"))
								condition+= " AND PYT.PAYMENT_TYPE_ID  = " +criteria.getPaymentType();
							if( !criteria.getPaymentStatus().equals("-1"))
								condition+= " AND PS.STATUS_ID  = " +criteria.getPaymentStatus();
							if(criteria.getPaymentNumber().length() >0)
								condition+=" AND lower(	PS.PAYMENT_NUMBER) = lower('" + criteria.getPaymentNumber() +"')";
							if(criteria.getPaymentAmount().length() >0)
								condition+=" AND PS.AMOUNT =  " + criteria.getPaymentAmount() ;
							
							if(criteria.getPaymentDateFrom()!=null  && criteria.getPaymentDateTo()!=null)								
								condition += " AND PS.PAYMENT_DUE_ON between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getPaymentDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getPaymentDateTo() )+"', 'dd/MM/yyyy')";
							
										
				
							if(condition.length() > 0)
						    	query+=" where 1=1 \n"+ condition;
			
			
			System.out.print("-------------" + query);
			String dataDef = ReportConstant.DataDef.VENDOR_OUTSTANDING_PAYMENT_REPORT;        
			reportQueryMap.put(dataDef, query+"");
			
			
			query = "SELECT \n" + 
			
			/*******************************                           English Labels             *********************************/
			"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentdate") + "' \"lblPaymentDateEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("chequeList.paymentAmountCol") + "' \"lblPaymentAmountEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("constructionContract.vendorNo") + "' \"lblVendorNumberEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("constructionContract.vendorName") + "' \"lblVendorNameEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
	        "  '" + ReportMessageResource.getPropertyEn("contract.contractType") + "' \"lblContractTypeEn\",\n" +
			"  '" + ReportMessageResource.getPropertyEn("chequeList.message.paymentStatus") + "' \"lblPaymentStatusEn\",\n" + 
			
			
			/*******************************                           Arabic Labels             *********************************/
			
			"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentdate") + "' \"lblPaymentDateAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("chequeList.paymentAmountCol") + "' \"lblPaymentAmountAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("constructionContract.vendorNo") + "' \"lblVendorNumberAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("constructionContract.vendorName") + "' \"lblVendorNameAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
	        "  '" + ReportMessageResource.getPropertyAr("contract.contractType") + "' \"lblContractTypeAr\",\n" +
			"  '" + ReportMessageResource.getPropertyAr("chequeList.message.paymentStatus") + "' \"lblPaymentStatusAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("chequeList.message.paymentStatus") + "' \"lblPaymentStatusAr\",\n" +
			
			
			/*******************************                           Mandatory   Lables          *********************************/ 	        
			"  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyEn("commons.report.name.vendorOutstandingPayment.rpt") + "' \"lblReportNameEn\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
			"  '" + ReportMessageResource.getPropertyAr("commons.report.name.vendorOutstandingPayment.rpt") + "' \"lblReportNameAr\",\n" + 
			"    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 
			
			"FROM DUAL";
			dataDef = ReportConstant.DataDef.VENDOR_OUTSTANDING_PAYMENT_REPORT_LABEL;        
			reportQueryMap.put(dataDef, query);         
			
			return reportQueryMap;
			}
			

}

