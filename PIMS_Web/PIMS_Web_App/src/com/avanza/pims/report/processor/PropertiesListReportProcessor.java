package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PropertiesListReportCriteria;


import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PropertiesListReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		PropertiesListReportCriteria criteria = (PropertiesListReportCriteria) reportCriteria;
               
		
		String query =  " SELECT DISTINCT PROP.PROPERTY_ID ," +
						" PROP.PROPERTY_NUMBER \"propertyNumber\",\n"+
			            " PROP.COMMERCIAL_NAME \"propetyName\",\n"+
			            " PRSN.IS_COMPANY  ,\n"+
			            " CASE WHEN PROP.MIGRATED = 1 THEN  to_char( PROP.CREATED_ON , 'dd-MON-yyyy' ) WHEN (PROP.MIGRATED = 0 OR PROP.MIGRATED IS NULL) AND PROP.STATUS_ID = 10017 THEN to_char( PROP.UPDATED_ON , 'dd-MON-yyyy' ) ELSE  to_char( PROP.RECEIVING_DATE , 'dd-MON-yyyy' ) END  \"total\",\n"+
//			            i was unable to add new field in the report thats why i used 'total' for receiving date
// -- Property Type Left
			            
			            " DD_PT.DATA_DESC_EN \"propertyTypeEn\",\n"+
			            " DD_PT.DATA_DESC_AR \"propertyTypeAr\",\n"+
			            
				        " DD_TT.DATA_DESC_EN ,\n"+
				        " DD_TT.DATA_DESC_AR ,\n"+
				        " PRSN.PERSON_TYPE \n,"+
            
// -- Addresss
			            " CI.ADDRESS1 ||' '|| CI.ADDRESS2 \"address\",\n"+
           	            " RGN_AREA.DESCRIPTION_AR \"emirate\",\n"+
			           " to_char((select DISTINCT COUNT(F1.FLOOR_ID) FROM FLOOR F1 " +   
			           " INNER JOIN  PROPERTY PROP1 ON  (PROP1.PROPERTY_ID=F1.PROPERTY_ID ) "+
			           " WHERE   PROP1.PROPERTY_ID = PROP.PROPERTY_ID  ),'999999') \"totalFloor\" ,"+
					   " to_char((select DISTINCT COUNT(U1.unit_id) FROM UNIT U1 "+ 
					   " INNER JOIN FLOOR F1  ON U1.FLOOR_ID = F1.FLOOR_ID "+
					   " INNER JOIN  PROPERTY PROP1 ON  (PROP1.PROPERTY_ID=F1.PROPERTY_ID )"+
					   " WHERE   PROP1.PROPERTY_ID = PROP.PROPERTY_ID ),'999999') \"totalUnits\" "+
				            
            
// -- TOTAL for Property - SUM of Count (DISTINCT U.UNIT_ID)
//        
// -- GRAND TOTAL for ALL Property - SUM ( TOTAL for Property )
            
					     " FROM  PROPERTY PROP "+
						 " LEFT JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID "+ 
						 " LEFT JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID "+
						 " LEFT JOIN CONTRACT CNTRT ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID "+ 
						 " Left JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID "+
						 " LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID "+ 
						 " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID "+
						 " LEFT JOIN DOMAIN_DATA DD_FT ON F.TYPE_ID = DD_FT.DOMAIN_DATA_ID "+
						 " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID "+
						 " LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;

        	String condition="";
			        	if(criteria.getTxtPropertyNumber().length()>0)
			        	condition+=" AND lower(PROP.PROPERTY_NUMBER) like lower('%"+criteria.getTxtPropertyNumber()+"%')";
			        	if(!criteria.getCboPropertyType().equals("-1"))
			        	condition+=" AND PROP.TYPE_ID="+criteria.getCboPropertyType();
			         	if(criteria.getTxtPropertyName().length()>0)
			        		condition+=" AND lower(PROP.COMMERCIAL_NAME) like lower('%"+criteria.getTxtPropertyName()+"%')";
			           	if(!criteria.getCboPropertyUsage().equals("-1"))
			        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getCboPropertyUsage();
			        	if(criteria.getTxtLandNumber().length()>0)
			        		condition+=" AND PROP.LAND_NUMBER like '%"+criteria.getTxtLandNumber()+"%'";
			        	if(!criteria.getCboEmirate().equals("-1"))
			        		condition+=" AND RGN_AREA.REGION_ID = '"+criteria.getCboEmirate()+"'";
			        	
			        	if( criteria.getClndrFromDate() !=null  )								
							condition += " AND (  (MIGRATED = 1 AND PROP.STATUS_ID = 10017 AND PROP.CREATED_ON >= to_date('"+DateUtil.convertToDisplayDate( criteria.getClndrFromDate())+" 00:00:00','dd/MM/yyyy  hh24:MI:ss' )) OR  ( (MIGRATED = 0 OR MIGRATED IS NULL) AND PROP.STATUS_ID = 10017 AND PROP.UPDATED_ON >= to_date('"+DateUtil.convertToDisplayDate( criteria.getClndrFromDate())+" 00:00:00','dd/MM/yyyy  hh24:MI:ss' )) )";
					    if( criteria.getCldrToDate() !=null  )
					    	condition += " AND ( (MIGRATED = 1  AND PROP.STATUS_ID = 10017 AND PROP.CREATED_ON <= to_date('"+DateUtil.convertToDisplayDate( criteria.getCldrToDate())+" 23:59:59','dd/MM/yyyy  hh24:MI:ss' )) OR ((MIGRATED = 0 OR MIGRATED IS NULL) AND PROP.STATUS_ID = 10017 AND  PROP.UPDATED_ON <= to_date('"+DateUtil.convertToDisplayDate( criteria.getCldrToDate())+" 23:59:59','dd/MM/yyyy  hh24:MI:ss' )) )";
					    
//			        	if(criteria.getClndrFromDate() !=null  && criteria.getCldrToDate()!=null)
//							condition += " AND PROP.RECEIVING_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrFromDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getCldrToDate() )+"', 'dd/MM/yyyy')";
			        	if(criteria.getTxtUnitNumber().length()>0)
			        		condition+=" AND lower(U.UNIT_NUMBER) like lower('%"+criteria.getTxtUnitNumber()+"%')";
			        	if(!criteria.getCboUnittype().equals("-1"))
			        		condition+=" AND U.UNIT_TYPE_ID="+criteria.getCboUnittype();
			        	if(criteria.getTxtFloorNumber().length()>0)
			        		condition+=" AND lower(F.FLOOR_NUMBER) like lower('%"+criteria.getTxtFloorNumber()+"%')";
			        	
			        	if(criteria.getTxtTenantName() .length() >0)
							condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTxtTenantName() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTxtTenantName() +"%')" +			
											" ) " ;
			        	if(criteria.getTxtContractNumber().length()>0)
			        		condition+="AND lower(CNTRT.CONTRACT_NUMBER) like lower('%"+criteria.getTxtContractNumber()+"%')";
			        	if(!criteria.getCboTenantType().equals("-1"))
							condition+=" AND PRSN.PERSON_TYPE = " + criteria.getCboTenantType();
     			
     			query+=" WHERE 1=1 \n"+condition+
//     					" GROUP BY PROP.PROPERTY_NUMBER ,"+
////     					" PROP.PROPERTY_ID ,"+
//						" PROP.COMMERCIAL_NAME ,"+
//						" DD_PT.DATA_DESC_EN ,"+
//						" DD_PT.DATA_DESC_AR ,"+
//						" CI.ADDRESS1 ," +
//						" CI.ADDRESS2 ,"+
//						" RGN_AREA.DESCRIPTION_AR ,"+
//						" U.UNIT_ID ,"+
//						" F.FLOOR_ID ,"+
//						" F.FLOOR_NAME ,"+
//						" F.FLOOR_NUMBER ,"+
//						" DD_FT.DATA_DESC_EN ,"+
//						" DD_FT.DATA_DESC_AR"+
						" ORDER BY PROP.PROPERTY_ID ";

			        	
      
        	   			
        String dataDef = ReportConstant.DataDef.PROPERTIES_LIST;        
        reportQueryMap.put(dataDef, query+"");
        
        // /////////////LABELS////////////////////////////////
        query = "SELECT \n" + 

        /**
-		 * ***************************** English Labels
		 * ********************************
		 */
		
        		 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.propertyNumber") + "' \"lblPropertyNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.address") + "' \"lblAddressEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.emirates") + "' \"lblEmirateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalUnits") + "' \"lblTotalUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalFloors") + "' \"lblTotalFloorsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalProperties") + "' \"lblTotalEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.propertiesListReport.rpt") + "' \"lblReportNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
		       
		        
		        
		        /**
				 * ***************************** Arabic Labels
				 * ********************************
				 */
		             
		        
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.propertyNumber") + "' \"lblPropertyNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.address") + "' \"lblAddressAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.emirates") + "' \"lblEmirateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalUnits") + "' \"lblTotalUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalFloors") + "' \"lblTotalFloorsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalProperties") + "' \"lblTotalAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.propertiesListReport.rpt") + "' \"lblReportNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" ,\n" + 
		       
		        " '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" +

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PROPERTIES_LIST_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;

      
	}

}
