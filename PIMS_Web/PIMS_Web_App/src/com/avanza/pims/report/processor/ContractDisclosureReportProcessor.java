package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractDisclosureReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;

public class ContractDisclosureReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		ContractDisclosureReportCriteria contractDisclosureReportCriteria = (ContractDisclosureReportCriteria) reportCriteria;
        
        String query =  "SELECT \n" + 
				        "  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) \"contractId\",\n" + 
				        "  to_char( CNTRT.CONTRACT_DATE, 'dd-MON-yyyy' ) \"contractDate\",\n" + 
				        "  CNTRT.CONTRACT_NUMBER \"contractNumber\",\n" + 
				        "  NVL( CNTRT.REMARKS, '---') \"contractRemarks\",\n" + 
				        "  to_char( CNTRT.START_DATE , 'dd-MON-yyyy' ) \"contractStartDate\",\n" + 
				        "  to_char( CNTRT.END_DATE , 'dd-MON-yyyy' ) \"contractEndDate\",\n" + 
				        "  to_char( CNTRT.ORIGINAL_START_DATE , 'dd-MON-yyyy' ) \"contractOriginalStartDate\",\n" + 
				        "  to_char( CNTRT.ORIGINAL_END_DATE, 'dd-MON-yyyy' ) \"contractOriginalEndDate\",\n" + 
				        "  CNTRT.RENT_AMOUNT \"contractRentAmount\",\n" + 
				        "  CNTRT.RENT_AMOUNT \"contractAskingPrice\",\n" + 
				        "  CNTRT.CONTRACT_TYPE_ID \"contractType\",\n" +
				        "  U.UNIT_NUMBER \"unitNumber\",\n" +
				        
				        "  case when PROP.PROPERTY_NAME_EN  is not null then " +
				        " 					PROP.PROPERTY_NAME ||' / '|| PROP.PROPERTY_NAME_EN||'/'||NVL( U.ACCOUNT_NUMBER, '---' )  " +
				        "  else  " +
				        " 					PROP.PROPERTY_NAME ||'/'||NVL( U.ACCOUNT_NUMBER, '---' )  " +
				        "  end \"propertyName\",\n" +
				        
				        "  case when PROP.PROPERTY_NAME_EN  is not null then " +
				        "					PROP.COMMERCIAL_NAME||' / '|| PROP.PROPERTY_NAME_EN ||'/'||NVL( U.ACCOUNT_NUMBER, '---' ) " +
				        "  else 			PROP.COMMERCIAL_NAME||'/'||NVL( U.ACCOUNT_NUMBER, '---' )  " +
				        "  end \"propertyCommercialName\",\n" +
				        
				        "  DD.DATA_DESC_EN \"domainDataOwnerNameEn\",\n" + 
				        "  DD.DATA_DESC_AR \"domainDataOwnerNameAr\",\n" + 
				        "  CI.ADDRESS1 \"contactInfoAddress1\",\n" + 
				        "  CI.ADDRESS2 \"contactInfoAddress2\",\n" + 
				       
				        "  PRSN.PASSPORT_NUMBER \"passportNumber\",\n" +
				        "  to_char( PRSN.PASSPORT_EXPIRY_DATE , 'dd-MON-yyyy' ) \"passportExpiryDate\",\n" +
				        "  PRSN.RESIDENSE_VISA_NUMBER \"visaNumber\",\n" +
				        "  to_char( PRSN.RESIDENSE_VIDA_EXP_DATE , 'dd-MON-yyyy' ) \"visaExpiryDate\",\n" +
				        "  RGN_NATIONALITY.DESCRIPTION_AR \"nationality\",\n" +
				        "  RGN_CITY.DESCRIPTION_AR \"community\",\n" +
				        
				        "  CI.OFFICE_PHONE \"officePhone\",\n" +
				        "  CI.STREET \"street\",\n" +
				        "  PRSN.DESIGNATION \"designation\",\n" +
				        
				        
				        "  PRSN.FIRST_NAME \"personFirstName\",\n" + 
				        "  PRSN.MIDDLE_NAME \"personMiddleName\",\n" + 
				        "  PRSN.LAST_NAME \"personLastName\",\n" + 
				        "  PRSN.COMPANY_NAME \"personCompanyName\"\n" + 
				        "FROM \n" + 
				        "  CONTRACT CNTRT \n" + 
				        "  INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID\n" + 
				        "  INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID\n" + 
				        "  INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID \n" + 
				        "  INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID\n" + 
				        "  INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID \n" + 
				        " LEFT JOIN REGION RGN_NATIONALITY ON RGN_NATIONALITY.REGION_ID = PRSN.NATIONALITY_ID \n"+ 
				        "  INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID\n" + 
				        "  INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID\n" + 
				        " LEFT JOIN REGION RGN_CITY ON RGN_CITY.REGION_ID = CI.CITY_ID \n "+
				        "WHERE\n" + 
				        "  CNTRT.CONTRACT_ID = " + contractDisclosureReportCriteria.getContractId();        
        String dataDef = ReportConstant.DataDef.CONTRACT_DISCLOSURE;        
        reportQueryMap.put(dataDef, query);
        
//        query = "SELECT\n" + 
//		        "  PS.PAYMENT_SCHEDULE_ID \"paymentScheduleId\",\n" + 
//		        "  PS.PAYMENT_NUMBER \"paymentScheduleNumber\",\n" + 
//		        "  CAST( PS.PAYMENT_DUE_ON AS VARCHAR(10) ) \"paymentScheduleDueOn\",\n" + 
//		        "  CAST( PS.PAYMENT_DATE AS VARCHAR(10) ) \"paymentSchedulePaymentDate\",\n" + 
//		        "  PS.AMOUNT \"paymentScheduleAmount\",\n" + 
//		        "  PT.DESCRIPTION_EN \"paymentTypeDescEn\",\n" + 
//		        "  PT.DESCRIPTION_AR \"paymentTypeDescAr\",\n" + 
//		        "  DDSTATUS.DATA_DESC_EN \"domainDataStatusEn\",\n" + 
//		        "  DDSTATUS.DATA_DESC_AR \"domainDataStatusAr\",\n" + 
//		        "  DDMODE.DATA_DESC_EN \"domainDataPaymentModeEn\",\n" + 
//		        "  DDMODE.DATA_DESC_AR \"domainDataPaymentModeAr\",\n" + 
//		        "  NVL(PRD.METHOD_REF_NO, '---') \"paymentRecptDetailMethodRefNo\",\n" + 
//		        "  NVL(B.BANK_EN, '---') \"bankEn\",\n" + 
//		        "  NVL(B.BANK_AR, '---') \"bankAr\"\n" + 
//		        "FROM\n" + 
//		        "  PAYMENT_SCHEDULE PS \n" + 
//		        "  INNER JOIN PAYMENT_TYPE PT ON PS.PAYMENT_TYPE_ID = PT.PAYMENT_TYPE_ID\n" + 
//		        "  INNER JOIN DOMAIN_DATA DDSTATUS ON PS.STATUS_ID = DDSTATUS.DOMAIN_DATA_ID \n" + 
//		        "  INNER JOIN DOMAIN_DATA DDMODE ON PS.PAYMENT_MODE_ID = DDMODE.DOMAIN_DATA_ID   \n" + 
//		        "  LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRT ON PS.PAYMENT_SCHEDULE_ID = PRT.PAYMENT_SCHEDULE_ID\n" + 
//		        "  LEFT JOIN PAYMENT_RECEIPT_DETAIL PRD ON PRT.PAYMENT_RECEIPT_DETAIL_ID = PRD.PAYMENT_RECEIPT_DETAIL_ID\n" +
//		        "  LEFT JOIN BANK B ON B.BANK_ID = PRD.BANK_ID\n" + 
//		        "WHERE \n" + 
//		        "  PS.CONTRACT_ID = " + contractDisclosureReportCriteria.getContractId()+
//		        "  AND PS.IS_DELETED=0 AND PS.RECORD_STATUS=1";
//        dataDef = ReportConstant.DataDef.PRINT_LEASE_CONTRACT_PAYMENT_SCHEDULE;        
//        reportQueryMap.put(dataDef, query);
        
//        query = "SELECT \n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("commons.date") + "' \"lblDateEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("commons.date") + "' \"lblDateAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNoEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNoAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.tab.unit.propertyname") + "' \"lblPropertyNameEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.tab.unit.propertyname") + "' \"lblPropertyNameAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("attachment.docDesc") + "' \"lblDescriptionEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("attachment.docDesc") + "' \"lblDescriptionAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.elementAddress") + "' \"lblElementAddressEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.elementAddress") + "' \"lblElementAddressAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.ownerName") + "' \"lblOwnerNameEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.ownerName") + "' \"lblOwnerNameAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.tenantNameCol") + "' \"lblTenantNameEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.tenantNameCol") + "' \"lblTenantNameAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.periodOfTenancy") + "' \"lblPeriodOfTenancyEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.periodOfTenancy") + "' \"lblPeriodOfTenancyAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("commons.from") + "' \"lblFromEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("commons.from") + "' \"lblFromAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("commons.to") + "' \"lblToEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("commons.to") + "' \"lblToAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("contract.rentAmount") + "' \"lblRentEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("contract.rentAmount") + "' \"lblRentAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("printLeaseContractReport.askingPrice") + "' \"lblAskingPriceEn\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyAr("printLeaseContractReport.askingPrice") + "' \"lblAskingPriceAr\",\n" + 
//		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNumberAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblDueOnEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblDueOnAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("attachment.grid.docTypeCol") + "' \"lblTypeEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("attachment.grid.docTypeCol") + "' \"lblTypeAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("chequeList.refNo") + "' \"lblRefNoEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("chequeList.refNo") + "' \"lblRefNoAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("application.status.gridHeader") + "' \"lblStatusEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("application.status.gridHeader") + "' \"lblStatusAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentMode") + "' \"lblModeEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentMode") + "' \"lblModeAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bank") + "' \"lblBankEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bank") + "' \"lblBankAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyEn("chequeList.amount") + "' \"lblAmountEn\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("chequeList.amount") + "' \"lblAmountAr\" \n" +
//		        "FROM DUAL";
//        dataDef = ReportConstant.DataDef.CONTRACT_DISCLOSURE_LABEL;        
//        reportQueryMap.put(dataDef, query); 
		
        return reportQueryMap;
	}
	
	

}
