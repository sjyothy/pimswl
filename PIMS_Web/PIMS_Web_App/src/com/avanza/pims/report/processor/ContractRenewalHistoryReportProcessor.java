package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractRenewalHistoryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ContractRenewalHistoryReportProcessor  extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		ContractRenewalHistoryReportCriteria criteria = (ContractRenewalHistoryReportCriteria) reportCriteria;

 		/*********************               Contract Number is Mandatory in this report      ***********************/

 		/* This revisedQuery is re-written by AVANZA\jawwad.ahmed */
 		String revisedQuery =   "SELECT " +
 								"CONNECT_BY_ROOT CAST(CNTRT.CONTRACT_ID AS VARCHAR(20)) \"TopParentID\", " +
 								"CONNECT_BY_ROOT CNTRT.CONTRACT_NUMBER \"TopParentNumber\", " +
 								"LEVEL \"Level\", " +
 								
 								"CAST(CNTRT.CONTRACT_ID AS VARCHAR(20))," +
	 							"OLD_CONTRACT_ID," +
								"CAST(CNTRT.CONTRACT_DATE AS VARCHAR(10)) \" contractDate \" ," +
								"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
								"NVL(CNTRT.REMARKS, '---') ," +
								"RGN_AREA.DESCRIPTION_AR \"emirates\",\n"+
								"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10)) \"contractStartDate\" , " +
								"CAST(to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
								"CNTRT.RENT_AMOUNT \"contractRentAmount\", " +
								"PROP.PROPERTY_NAME ," +
								"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
								"U.UNIT_NUMBER     \"unitNumber\" , " +
								"PRSN.FIRST_NAME   \"tenantFirstName\" , " +
								"PRSN.MIDDLE_NAME  \"tenantMiddleName\" , " +
								"PRSN.LAST_NAME    \"tenantLastName\" , " +
								"PRSN.COMPANY_NAME \"companyName\" , " +
								"DD.DATA_DESC_EN   \"ownershipTypeEn\" , " +
								"DD.DATA_DESC_AR   \"ownershipTypeAr\", " +
								"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
								"DD2.DATA_DESC_AR  \"contractTypeAr\", " +
								"DD3.DATA_DESC_EN  \"contractStatusEn\" , " +
								"DD3.DATA_DESC_AR  \"contractStatusAr\" " +
								
 								"FROM CONTRACT CNTRT  " +
								"INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID  " +
								"INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID  " +
								"INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID  " +
								"INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID  " +
								"INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
								"INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
								"LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID " +
								"INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
								"INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID  " +
								"INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID  ";
								
 								if(criteria.getContractNumber().length() > 0)
									revisedQuery += "START WITH CNTRT.contract_number LIKE '%"+ criteria.getContractNumber() +"%'  ";
								//else if(criteria.getTenantName().length() > 0)
									//revisedQuery += "START WITH  CNTRT.status_id <> 31006 AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
								
								revisedQuery += "CONNECT BY PRIOR   CNTRT.OLD_CONTRACT_ID = CNTRT.CONTRACT_ID	";
 		
 		String query = "SELECT CAST(CNTRT.CONTRACT_ID AS VARCHAR(20))," +
 							"OLD_CONTRACT_ID," +
							"CAST(CNTRT.CONTRACT_DATE AS VARCHAR(10)) \" contractDate \" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL(CNTRT.REMARKS, '---') ," +
							"RGN_AREA.DESCRIPTION_AR \"emirates\",\n"+
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10)) \"contractStartDate\" , " +
							"CAST(to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							"U.UNIT_NUMBER     \"unitNumber\" , " +
							"PRSN.FIRST_NAME   \"tenantFirstName\" , " +
							"PRSN.MIDDLE_NAME  \"tenantMiddleName\" , " +
							"PRSN.LAST_NAME    \"tenantLastName\" , " +
							"PRSN.COMPANY_NAME \"companyName\" , " +
							"DD.DATA_DESC_EN   \"ownershipTypeEn\" , " +
							"DD.DATA_DESC_AR   \"ownershipTypeAr\", " +
							"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
							"DD2.DATA_DESC_AR  \"contractTypeAr\", " +
							"DD3.DATA_DESC_EN  \"contractStatusEn\" , " +
							"DD3.DATA_DESC_AR  \"contractStatusAr\" " +

							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+
							" LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID \n"+
							" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID " +
							" INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID " ;
	

	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
							//if(criteria.getTenantName().length() >0)
								//condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
							
//							if(criteria.getPropertyName().length() >0)
//								condition+=" AND PROP.COMMERCIAL_NAME like   '%" + criteria.getPropertyName() +"%'";
//							if(!criteria.getOwnershipType().equals("-1"))
//								condition+= " AND DD.DOMAIN_DATA_ID = " + criteria.getOwnershipType(); 
//				//			if(!criteria.getContractType().equals("-1"))
//					//			condition+= " AND DD2.DOMAIN_DATA_ID = " + criteria.getContractType(); 
//							//if(!criteria.getStatus().equals("-1"))
//								//condition+= " AND DD3.DOMAIN_DATA_ID = " + criteria.getStatus(); 							
//							if(criteria.getUnitNumber().length() >0)
//								condition+=" AND CNTRT.RENT_AMOUNT  like   '%" + criteria.getUnitNumber() +"%'";
//							if(criteria.getTenantName() .length() >0)
//								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
//												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
//												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
//												" ) " ;							
//							if(criteria.getRentAmount().length()>0)
//								condition+=" AND U.METHOD_REF_NO  like   '%" + criteria.getRentAmount() +"%'";
//							if(!criteria.getEmirate().equals("-1"))
//								condition+= " AND RGN_AREA.REGION_ID  =  '" + criteria.getEmirate() +"'"	;


							if(condition.length() > 0)
					        	query+="where 1=1 " +condition + " AND CNTRT.status_id <> 31006 ";
		

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.CONTRACT_RENEWAL_HISTORY;        
        reportQueryMap.put(dataDef, revisedQuery+"");
		

        /**********************             It is not a sub-report but treat it as a sub-report here              *********************/
        
		query = "SELECT  DISTINCT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ,OLD_CONTRACT_ID," +
	//				"CAST( CNTRT.CONTRACT_DATE AS VARCHAR(10) ) \" contractDate \" ," +
					"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
					"NVL( CNTRT.REMARKS, '---') ," +
					"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
					"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
					"CNTRT.RENT_AMOUNT \"contractRentAmount\" ," +
					"PROP.PROPERTY_NAME ," +
//					"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
					"U.UNIT_NUMBER \"unitNo\" , " +
					"PRSN.FIRST_NAME \"tenantFirstName\" , " +
					"PRSN.MIDDLE_NAME \"tenantMiddleName\" , " +
					"PRSN.LAST_NAME \"tenantLastName\" , " +
					"PRSN.COMPANY_NAME \"companyName\" , " +
			//		"DD.DATA_DESC_EN  \"ownershipTypeEn\" , " +
				//	"DD.DATA_DESC_AR  \"ownershipTypeAr\", " +
					"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
					"DD2.DATA_DESC_AR  \"contractTypeAr\", " +
					"DD3.DATA_DESC_EN  \"contractStatusEn\" , " +
					"DD3.DATA_DESC_AR  \"contractStatusAr\" " +
			
					" FROM " +
					" CONTRACT CNTRT " +
					" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
					" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
					" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
					" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
					" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
					" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
					" INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID " +
					" INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID " +
					//"WHERE \n" + 
					"  START WITH  CNTRT.status_id <> 31006 AND CNTRT.CONTRACT_NUMBER like '%" + criteria.getContractNumber() +"%'"+
			        "  CONNECT BY PRIOR CNTRT.OLD_CONTRACT_ID = CNTRT.CONTRACT_ID";

			//System.out.print("\n-----sub query--------" + query);
			//dataDef = ReportConstant.DataDef.CONTRACT_RENEWAL_HISTORY_SUBREPORT;        
			//reportQueryMap.put(dataDef, query+"");
        
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.ownershipType") + "' \"lblOwnershipTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.state") + "' \"lblEmiratesEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.startDate") + "' \"lblContractStartDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.rentAmount") + "' \"lblContractRentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractType") + "' \"lblContractTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("ContractorSearch.contractStatus") + "' \"lblContractStatusEn\",\n" + 
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.ownershipType") + "' \"lblOwnershipTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("unit.number") + "' \"lblUnitNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.state") + "' \"lblEmiratesEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.startDate") + "' \"lblContractStartDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.rentAmount") + "' \"lblContractRentAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractType") + "' \"lblContractTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("ContractorSearch.contractStatus") + "' \"lblContractStatusAr\",\n" + 


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.contractRenewalHistory.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.contractRenewalHistory.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.CONTRACT_RENEWAL_HISTORY_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
