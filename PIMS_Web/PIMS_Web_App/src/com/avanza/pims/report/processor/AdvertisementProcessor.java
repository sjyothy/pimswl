package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.AdvertisementCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.NOLCriteria;
import com.avanza.pims.report.dataDef.AdvertisementDataDef;
import com.avanza.pims.ws.utility.UtilityService;
public class AdvertisementProcessor extends AbstractReportProcessor
{
	public AdvertisementProcessor()
	{
		
	}
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria)
	{
		
		AdvertisementCriteria criteria = (AdvertisementCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query= "select a.auction_id ,\n"+
					  " AMAF_ADDRESS_EN.configValue \"amafAddressEn\",\n"+
					  " AMAF_ADDRESS_AR.configValue \"amafAddressAr\",\n"+
					  " a.auction_number, \n"+
					  "  a.confiscation_percentage \"confiscationPercentage\", \n"+
					  " a.outbidding_value \"incrementalAmount\", \n"+
					  " CAST(to_char(a.request_end_date ,'dd/mm/yyyy') AS VARCHAR(10) )  \"registrationEndDate\" , " +
					  " a.auction_end_datetime , \n"+
					  " CAST(to_char(a.auction_start_datetime ,'dd/mm/yyyy') AS VARCHAR(10) )  \"auctionDateTime\" , " +
					  " a.request_start_date,\n"+
					  " au.unit_id,\n"+
					  " au.opening_price \"openingPrice\",\n"+
					  " au.deposit_amount \"depositAmount\",\n"+
					  " au.deposit_amount \"depositMax\",\n"+
					  " u.unit_number \"unitNumber\",\n"+
					  " u.unit_area \"unitArea\",\n"+
					  " f.floor_number,\n"+
					  " p.property_number,\n"+
					  " p.land_number \"landNumber\",\n"+
					  " p.commercial_Name \"propCommercialName\",\n"+
					  " RGN.DESCRIPTION_EN \"cityEn\",\n"+
					  " RGN.DESCRIPTION_AR \"cityAr\"\n"+
						"	from  auction a"+
						"	inner join auction_unit au on a.auction_id=au.auction_id \n"+
						"	inner join unit u on au.unit_id=u.unit_id  \n"+
						"	inner join floor f on u.floor_id=f.floor_id \n"+
						"	inner join property p on f.property_id=p.property_id \n"+
						"	left outer join contact_info ci on p.contact_info_id=ci.contact_info_id \n"+
						"	LEFT outer JOIN REGION RGN ON RGN.REGION_ID = CI.CITY_ID ,\n"+
						" (select configValue from system_configuration  where CONFIGKEY='AMAF_ADDRESS') AMAF_ADDRESS_EN,\n " +
						" (select configValue from system_configuration  where CONFIGKEY='AMAF_ADDRESS_AR') AMAF_ADDRESS_AR " +
						" where 1=1";
		String condition="";
		if(criteria.getAuctionId()!=null)
			condition+=" and a.auction_id =" + criteria.getAuctionId();
		query += condition;
		
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.ADVERTISEMENT_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		return reportQueryMap;
	}
			

}
