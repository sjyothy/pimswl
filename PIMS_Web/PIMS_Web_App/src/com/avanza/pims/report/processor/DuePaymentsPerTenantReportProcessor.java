package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.report.criteria.DuePaymentsPerTenantReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class DuePaymentsPerTenantReportProcessor extends AbstractReportProcessor{
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		DuePaymentsPerTenantReportCriteria criteria = (DuePaymentsPerTenantReportCriteria) reportCriteria;
		
		String query = "SELECT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
//							"CAST( CNTRT.CONTRACT_DATE AS VARCHAR(10) ) \" contractDate \" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
	//						"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractIssueDate\" , " +
		//					"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') , " +
							//"U.UNIT_NUMBER \"unitNumber\" , " +
							"DD.DATA_DESC_EN ," +
							"DD.DATA_DESC_AR ," +
							"CI.ADDRESS1 ," +
							"CI.ADDRESS2 ," +
							" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  \"tenantName\", " +
							"PRSN.COMPANY_NAME," +
							"CAST( to_char(PS.PAYMENT_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDate\" , " +
							"CAST( to_char(PRCD.METHOD_REF_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"chequeDate\" , " +
							"PS.PAYMENT_TYPE_ID," +
							"NVL(PYT.DESCRIPTION_EN, '---')  \"paymentTypeEn\" , " +
							"NVL(PYT.DESCRIPTION_AR, '---')  \"paymentTypeAr\" , " +
							"NVL(PRCD.METHOD_REF_NO, '---') \"chequeNumber\"," +
							"PRCD.AMOUNT \"amount\"," +
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_EN, '---') end \"bankNameEn\" , " +
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_AR,'---')  end \"bankNameAr\"  " +

							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
							" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON PS.STATUS_ID = DD2.DOMAIN_DATA_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
							" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON " +
							" PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
							" INNER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
							" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " ;
	

	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
							condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
											" ) " ;
							
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNumber() +"%')";
							if(criteria.getFromDate()!=null  && criteria.getToDate()!=null)								
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToDate() )+"', 'dd/MM/yyyy')";
							if(!criteria.getPaymentMethod().equals("-1"))
								condition+= " AND PMT.DESCRIPTION  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getPaymentMethod() +"')"	;
							if(criteria.getChequeNumber().length()>0)
								condition+=" AND lower(PRCD.METHOD_REF_NO)  like   lower('%" + criteria.getChequeNumber() +"%')";
							if(!criteria.getPaymentStatus().equals("-1"))
								condition+= " AND DD2.DATA_DESC_EN  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getPaymentStatus() +"')"	;
						//		condition+= " AND DD2.DATA_DESC_EN  =  '" + criteria.getSelectOnePaymentStatus() +"'"	;
							if(!criteria.getBankName().equals("-1"))
								condition+= " AND BANK.BANK_ID  =  '" + criteria.getBankName() +"'"	;


							if(condition.length() > 0)
					        	query+="where 1=1 \n"+ condition;
		

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.DUE_PAYMENTS_PER_TENANT;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("grp.amount") + "' \"lblAmountEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("grp.amount") + "' \"lblAmountAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateAr\",\n" +

		        /*******************************                           Summary Lables           *********************************/	        

		        "  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblTotalEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblTotalAr\",\n" + 


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.duePaymentsPerTenantReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.duePaymentsPerTenantReport.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.DUE_PAYMENTS_PER_TENANT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
