package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.Utils.DateUtil;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.TenantViolationsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantViolationsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		TenantViolationsReportCriteria criteria = (TenantViolationsReportCriteria) reportCriteria;
		
		String query = "SELECT  DISTINCT " +
							"PRSN.FIRST_NAME \"tenantFirstName\" , " +
							"PRSN.MIDDLE_NAME \"tenantMiddleName\" , " +
							"PRSN.LAST_NAME \"tenantLastName\" , " +
							"PRSN.COMPANY_NAME," +
							"PROP.COMMERCIAL_NAME  \"propertyName\" , " +
							"U.UNIT_NUMBER \"unitNumber\"," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							" PRSN.PERSON_ID \"tenantId\",\n"+
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							
							" CAST( to_char(IV.VIOLATION_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) \"violationDate\" , " +
							"IV.DESCRIPTION  \"violationNo\" , " +  //not used
//                            -- Category
							" VC.DESCRIPTION_EN   \"violationCategoryEn\" , " +
							" VC.DESCRIPTION_AR \"violationCategoryAr\" , " +
//                            -- Type
							" VT.DESCRIPTION_EN   \"violationTypeEn\" , " +
							" VT.DESCRIPTION_AR \"violationTypeAr\" , " +
                            //-- Status
							" DD_VS.DATA_DESC_EN   \"violationStatusEn\" , " +
							" DD_VS.DATA_DESC_AR \"violationStatusAr\" , " +
//                          -- Action
							" DD_VA.DATA_DESC_EN   \"violationActionsEn\" , " +
							" DD_VA.DATA_DESC_AR \"violationActionsAr\"  " +
                            "  , IV.VIOLATION_DATE "+
							" FROM " +
							" INSPECTION_VIOLATION IV " +
							" INNER JOIN CONTRACT_UNIT CU ON CU.CONTRACT_UNIT_ID =  IV.CONTRACT_UNIT_ID " +
							" INNER JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN PERSON PRSN ON PRSN.PERSON_ID  = CNTRT.TENANT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID  INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +

							" INNER JOIN VIOLATION_CATEGORY VC ON IV.CATEGORY_ID = VC.VIO_CAT_ID " +
							"  INNER JOIN VIOLATION_TYPE VT ON IV.TYPE_ID = VT.VIO_TYPE_ID " +
							" LEFT JOIN INSPECTION_VIOLATION_ACTION IVA ON IV.VIOLATION_ID = IVA.VIOLATION_ID " +
							
							" INNER JOIN DOMAIN_DATA DD_VS ON IV.STATUS_ID = DD_VS.DOMAIN_DATA_ID " +
							" LEFT JOIN DOMAIN_DATA DD_VA ON IVA.ACTION_ID = DD_VA.DOMAIN_DATA_ID " ;

        
        
	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
															" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
															" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
														" ) " ;
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
	    					if(!criteria.getPropertyOwnershipType().equals("-1"))
								condition+= " AND PROP.OWNERSHIP_TYPE_ID ='" + criteria.getPropertyOwnershipType() +"'"	;
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNumber() +"%')";
							if(criteria.getViolationDate()!=null )								
								condition += " AND IV.VIOLATION_DATE  = to_date ('"+ DateUtil.convertToDisplayDate( criteria.getViolationDate() ) +"', 'dd/MM/yyyy') ";
	    					if(!criteria.getViolationCategory().equals("-1"))
								condition+= " AND VC.VIO_CAT_ID ='" + criteria.getViolationCategory() +"'"	;
	    					if(!criteria.getViolationType().equals("-1"))
								condition+= " AND VT.VIO_TYPE_ID ='" + criteria.getViolationType() +"'"	;
	    					if(!criteria.getViolationStatus().equals("-1"))
								condition+= " AND DD_VS.DOMAIN_DATA_ID ='" + criteria.getViolationStatus() +"'"	;
	    					if(!criteria.getViolationAction().equals("-1"))
								condition+= " AND DD_VA.DOMAIN_DATA_ID ='" + criteria.getViolationAction() +"'"	;
	    					if(criteria.getTenantId().length() >0)
								condition+=" AND PRSN.PERSON_ID = " + criteria.getTenantId() ;

	    					
							if(condition.length() > 0)
					        	query+="where 1=1 \n"+ condition;

				String sorting =" ORDER BY PRSN.FIRST_NAME, PRSN.MIDDLE_NAME, PRSN.LAST_NAME, IV.VIOLATION_DATE desc ";
				query+=sorting;


		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.TENANT_VIOLATIONS_REPORT;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.id") + "' \"lblTenantIdEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.issueDate") + "' \"lblContractStartDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("violation.number") + "' \"lblViolationNoEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("violation.Date") + "' \"lblViolationDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("violation.violationCategory") + "' \"lblViolationCategoryEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("violation.violationType") + "' \"lblViolationTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("violation.violationStatus") + "' \"lblViolationStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("violation.violationActions") + "' \"lblViolationActionsEn\",\n" + 
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("tenants.id") + "' \"lblTenantIdAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("unit.number") + "' \"lblUnitNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.issueDate") + "' \"lblContractStartDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("violation.number") + "' \"lblViolationNoAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("violation.Date") + "' \"lblViolationDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("violation.violationCategory") + "' \"lblViolationCategoryAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("violation.violationType") + "' \"lblViolationTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("violation.violationStatus") + "' \"lblViolationStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("violation.violationActions") + "' \"lblViolationActionsAr\",\n" + 

		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantViolations.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantViolations.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.TENANT_VIOLATIONS_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
