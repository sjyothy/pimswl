package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ChequeWithdrawlReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ChequeWithdrawlReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		ChequeWithdrawlReportCriteria criteria = (ChequeWithdrawlReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = "SELECT " +
		" PAYMENT_SCHEDULE_ID , \n"+ 
		" PAYMENT_NUMBER , \n"+ 
		" PAYMENT_SCH_AMOUNT \"chequeAmount\", \n"+ 
		" BANK_NAME \"bankName\", \n"+ 
		" CAST(to_char(CHEQUE_DATE ,'dd/mm/yyyy') AS VARCHAR(10) )  \"chequeDate\" , " +
		" CHEQUE_NUMBER \"chequeNumber\", \n"+ 
		" PD_AMOUNT , \n"+ 
		" ACCOUNT_NUMBER \"tenantAccountNumber\", \n"+ 
		" PM_DESCRIPTION_EN , \n"+ 
		" GRP_PMT_METHOD , \n"+ 
		" PM_DESCRIPTION_AR , \n"+ 
		" GRP_ACCOUNT_NUMBER \"grpAccNumber\", \n"+ 
		" ACTION_NUMBER , \n"+ 
		" FAC_DECRIPTION , \n"+ 
		" BANK_REMITANCE_ACC_NAME , \n"+ 
		" TRX_NAME , \n"+ 
		" ACTIVITY_NAME , \n"+ 
		" FINANCIAL_ACC_CONFIGURATION_ID , \n"+ 
		" REMITANCE_BANK_NAME \"palaceHolder1\" , \n"+ 
		" REMITANCE_BANK_ACC_NAME \"bankRemitanceAccName\", \n"+ 
		" REMITANCE_BANK_ACC_NUMBER \"bankRemitanceAccNumber\", \n"+ 
		" CHEQUE_OWNER \"chequeOwner\", \n"+
		" (SELECT CONFIGVALUE from SYSTEM_CONFIGURATION WHERE CONFIGKEY='AMAF_REPRESENTATIVE_WITHDRAWL_REPORT') \"amafRepresentative\" \n"+
		" FROM CHEQUE_WITHDRAWL_VIEW \n";
		
		String condition ="where 1 = 1 ";
		if(criteria.getInCriteria() != null)
			condition += " AND PAYMENT_SCHEDULE_ID " + criteria.getInCriteria();
		query+=condition;
 		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.CHEQUE_WITHDRAWL_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        
		query = "SELECT \n" + 
		
        /*******************************                           English Labels             *********************************/

				"  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.chequeOwnerCol") + "' \"chequeOwnerEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("accountSearch.accountNumber") + "' \"tenantAccountNumberEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("report.chequeWithDrawlLetter.fields.chequeBank") + "' \"chequeBankEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.amountCol") + "' \"chequeAmountEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("payment.chequeDate") + "' \"chequeDateEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("payment.chequeNo") + "' \"chequeNumberEn\",\n" +
				
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
				"  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.chequeOwnerCol") + "' \"chequeOwnerAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("accountSearch.accountNumber") + "' \"tenantAccountNumberAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("report.chequeWithDrawlLetter.fields.chequeBank") + "' \"chequeBankAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"chequeAmountAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("payment.chequeDate") + "' \"chequeDateAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("payment.chequeNo") + "' \"chequeNumberAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("approveProjectDesign.tabs.designNotesTab.grid.Date") + "' \"date\",\n" +
		        

		        /*******************************                           Mandatory Info             *********************************/	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.title.chequeWithdrawlReport") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.title.chequeWithdrawlReport") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 
		        
		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.CHEQUE_WITHDRAWL_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
