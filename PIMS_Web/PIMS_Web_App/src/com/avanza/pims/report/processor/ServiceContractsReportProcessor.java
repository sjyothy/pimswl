package com.avanza.pims.report.processor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ServiceContractsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ServiceContractsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		ServiceContractsReportCriteria criteria = (ServiceContractsReportCriteria) reportCriteria;

// 		String query = "SELECT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
//							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
//							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"fromContractDate\" , " +
//							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"toContractDate\" , " +
//							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
//							" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  \"contractorName\", " +
//							"DD.DATA_DESC_EN  \"propertyOwnershipTypeEn\" , " +
//							"DD.DATA_DESC_AR  \"propertyOwnershipTypeAr\", " +
//							"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
//							"DD2.DATA_DESC_AR  \"contractTypeAr\", " +
//							"DD3.DATA_DESC_EN  \"statusEn\" , " +
//							"DD3.DATA_DESC_AR  \"statusAr\" " +
//							
//							" FROM " +
//							" CONTRACT CNTRT " +
//							" INNER JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID  = PRSN.PERSON_ID " +
//							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
//							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
//							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
//							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
//							" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
//							" INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID " +
//							" INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID " ;
 		String query=" select cntrt.contract_id ,\n"+
		 " cntrt.contract_number \"contractNumber\",\n"+
		 " CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"fromContractDate\" , \n" +
		 " CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"toContractDate\" , \n" +
		 " p.commercial_name \"propertyName\" ,\n"+
	  		 " tpa.tender_id ,\n"+
		 " u.unit_id ,\n"+
		 " p.property_id ,\n"+
		 " case is_Company when 0 then PRSN.FIRST_NAME ||' ' ||PRSN.MIDDLE_NAME||' '||PRSN.LAST_NAME else PRSN.Company_name end \"contractorName\", \n"+
	  
  
		 " PRSN.company_name \"companyName\",\n"+
		 " cntrt.Rent_amount \"contractAmount\",\n"+
		 " DD.DATA_DESC_EN  \"propertyOwnershipTypeEn\" ,\n"+
		 " DD.DATA_DESC_AR  \"propertyOwnershipTypeAr\",\n"+
		 " DD2.DATA_DESC_EN  \"contractTypeEn\" ,\n"+
		 " DD2.DATA_DESC_AR  \"contractTypeAr\",\n"+
		 " DD3.DATA_DESC_EN  \"statusEn\",\n"+
		 " DD3.DATA_DESC_AR  \"statusAr\" \n"+
		 "  from contract cntrt "+
		 //" left outer join tender tend on tend.tender_id  = cntrt.tender_id"+	   
		// "  INNER JOIN tender_properties tpa on cntrt.tender_id =  tpa.tender_id"+
		 "  INNER JOIN tender_properties tpa on cntrt.contract_id =  tpa.contract_id "+
		 " left outer join property p on tpa.property_id  = p.property_id"+
		 " left outer join unit u on u.unit_id  = tpa.unit_id"+
		 " INNER JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID  = PRSN.PERSON_ID"+
		 " INNER JOIN DOMAIN_DATA DD ON p.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID"+
		 "  INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID"+
		 " INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID ";
		 String condition="";
			if(criteria.getContractNumber().length() >0)
				condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
			if(criteria.getContractorName() .length() >0)
			condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +
							" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getContractorName() +"%')" +
							" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +			
							" ) " ;
			
			if(criteria.getPropertyName().length() >0)
				condition+=" AND lower(p.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
			if(!criteria.getPropertyOwnershipType().equals("-1"))
				condition+= " AND p.OWNERSHIP_TYPE_ID  =  '" + criteria.getPropertyOwnershipType() +"'"	;
			if(!criteria.getContractType().equals("-1"))
				condition+= " AND cntrt.CONTRACT_TYPE_ID = '" + criteria.getContractType() +"'"	;
			if(!criteria.getStatus().equals("-1"))
				condition+= " AND cntrt.STATUS_ID  =  " + criteria.getStatus();
			
			if(criteria.getFromContractDate()!=null  && criteria.getToContractDate()!=null)								
				condition += " AND cntrt.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDate() )+"', 'dd/MM/yyyy')";
			if(criteria.getFromContractDate()!=null )								
				condition += " AND cntrt.START_DATE = to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDate() ) +"', 'dd/MM/yyyy')  ";
			if(criteria.getToContractDate()!=null )
				condition += " AND cntrt.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getToContractDate() ) +"', 'dd/MM/yyyy')  ";
			if(condition.length() > 0)
	        	query+=" where 1=1 \n"+ condition;
		 /*query+="  union "+
		 " select cntrt.contract_id ,\n"+
		 " cntrt.contract_number ,\n"+
		 " CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , \n" +
		 " CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , \n" +
		 " p.commercial_name  ,\n"+
	  		 " tp.tender_id ,\n"+
		 " u.unit_id ,\n"+
		 " p.property_id ,\n"+
		 " PRSN.FIRST_NAME ||' ' ||PRSN.MIDDLE_NAME||' '||PRSN.LAST_NAME , \n"+
		 " DD.DATA_DESC_EN   ,\n"+
		 " DD.DATA_DESC_AR  ,\n"+
		 " DD2.DATA_DESC_EN  ,\n"+
		 " DD2.DATA_DESC_AR  ,\n"+
		 " DD3.DATA_DESC_EN ,\n"+
		 " DD3.DATA_DESC_AR   \n"+
		 "  from contract cntrt "+
		 "  INNER JOIN tender_properties tp on cntrt.contract_id =  tp.contract_id "+
		 " left outer join property p on tp.property_id  = p.property_id"+
		 " left outer join unit u on u.unit_id  = tp.unit_id"+
		 " INNER JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID  = PRSN.PERSON_ID"+
		 " INNER JOIN DOMAIN_DATA DD ON p.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID"+
		 " INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID"+
		 " INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID";
		

	     condition="";
				if(criteria.getContractNumber().length() >0)
					condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
				if(criteria.getContractorName() .length() >0)
				condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +
								" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getContractorName() +"%')" +
								" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +			
								" ) " ;
				
				if(criteria.getPropertyName().length() >0)
					condition+=" AND lower(p.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
				if(!criteria.getPropertyOwnershipType().equals("-1"))
					condition+= " AND p.OWNERSHIP_TYPE_ID  =  '" + criteria.getPropertyOwnershipType() +"'"	;
				if(!criteria.getContractType().equals("-1"))
					condition+= " AND cntrt.CONTRACT_TYPE_ID = '" + criteria.getContractType() +"'"	;
				if(!criteria.getStatus().equals("-1"))
					condition+= " AND cntrt.STATUS_ID  =  " + criteria.getStatus();
				
				if(criteria.getFromContractDate()!=null  && criteria.getToContractDate()!=null)								
					condition += " AND cntrt.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDate() )+"', 'dd/MM/yyyy')";
				if(criteria.getFromContractDate()!=null )								
					condition += " AND cntrt.START_DATE = to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDate() ) +"', 'dd/MM/yyyy')  ";
				if(criteria.getToContractDate()!=null )
					condition += " AND cntrt.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getToContractDate() ) +"', 'dd/MM/yyyy')  ";
				if(condition.length() > 0)
		        	query+=" where 1=1 \n"+ condition;
		
		*/

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.SERVICE_CONTRACTS;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.contractStartDate") + "' \"lblFromContractDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.enddate") + "' \"lblToContractDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("ContractorSearch.contractStatus") + "' \"lblStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contractor.type") + "' \"lblContractorTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("serviceContract.contractor.name") + "' \"lblContractorNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("applicationDetails.ownershipType") + "' \"lblPropertyOwnershipTypeEn\",\n" + 
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.contractStartDate") + "' \"lblFromContractDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.enddate") + "' \"lblToContractDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("ContractorSearch.contractStatus") + "' \"lblStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contractor.type") + "' \"lblContractorTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("serviceContract.contractor.name") + "' \"lblContractorNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("applicationDetails.ownershipType") + "' \"lblPropertyOwnershipTypeAr\",\n" + 

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.serviceContracts.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.serviceContracts.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.SERVICE_CONTRACTS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}

