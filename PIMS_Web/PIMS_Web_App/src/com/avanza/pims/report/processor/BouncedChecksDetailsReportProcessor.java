package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.BouncedChecksDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class BouncedChecksDetailsReportProcessor extends AbstractReportProcessor{
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		BouncedChecksDetailsReportCriteria criteria = (BouncedChecksDetailsReportCriteria) reportCriteria;
		
		String query = "SELECT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
//							"CAST( CNTRT.CONTRACT_DATE AS VARCHAR(10) ) \" contractDate \" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
	//						"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractIssueDate\" , " +
		//					"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							//"U.UNIT_NUMBER \"unitNumber\" , " +
							"DD.DATA_DESC_EN ," +
							"DD.DATA_DESC_AR ," +
							"CI.ADDRESS1 ," +
							"CI.ADDRESS2 ," +
							"CASE WHEN CNTRT.CONTRACT_ID IS NOT NULL THEN LTRIM ( PRSN.FIRST_NAME ||' ' || NVL(PRSN.MIDDLE_NAME,'')  || ' '  || PRSN.LAST_NAME  || PRSN.COMPANY_NAME  ) " +
							" ELSE  LTRIM ( rcptPrsn.FIRST_NAME ||' ' || NVL(rcptPrsn.MIDDLE_NAME,'')  || ' '  || rcptPrsn.LAST_NAME  || rcptPrsn.COMPANY_NAME  ) END \"tenantName\"  , " +
							
							"CAST( to_char(PS.PAYMENT_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDate\" , " +
							"CAST( to_char(PRCD.METHOD_REF_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"chequeDate\" , " +
							"PS.PAYMENT_TYPE_ID," +
							"NVL(PYT.DESCRIPTION_EN, '---')  \"paymentTypeEn\" , " +
							"NVL(PYT.DESCRIPTION_AR, '---')  \"paymentTypeAr\" , " +
							"NVL(PRCD.METHOD_REF_NO, '---') \"chequeNumber\"," +
							"PRCD.AMOUNT \"amount\"," +
							"PRCT.RECEIPT_NUMBER \"receiptNumber\"," +
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME " +
							"        ELSE  NVL(BANK.BANK_EN, '---') END \"bankNameEn\" , " +
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME " +
							"        ELSE NVL(BANK.BANK_AR,'---') END \"bankNameAr\"  " +

							" FROM payment_schedule PS "+  
					        " LEFT JOIN "+ 
							" CONTRACT CNTRT ON PS.CONTRACT_ID = CNTRT.CONTRACT_ID" +
							" LEFT JOIN PAYMENT_SCH_UNIT_PROP PSUP ON PS.PAYMENT_SCHEDULE_ID = PSUP.PAYMENT_SCHEDULE_ID "+
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" LEFT JOIN PROPERTY PROP ON PROP.PROPERTY_ID = PSUP.PROPERTY_ID " +
							" LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							" LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" LEFT JOIN REQUEST REQ ON PS.REQUEST_ID = REQ.REQUEST_ID " +					
							" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON PS.STATUS_ID = DD2.DOMAIN_DATA_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
							" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON " +
							" PRC.PAYMENT_RECEIPT_DETAIL_ID =PRCD.PAYMENT_RECEIPT_DETAIL_ID" +
							" INNER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
							" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
                            " LEFT OUTER JOIN  payment_receipt prct ON PRCD.PAYMENT_RECEIPT_ID = prct.PAYMENT_RECEIPT_ID "+ 
                            " LEFT JOIN person rcptPrsn ON prct.paid_by = rcptPrsn.person_id";
        


	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName().trim().length() >0)
							condition+= " AND (" +
									        "    lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(PRSN.COMPANY_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(rcptPrsn.COMPANY_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(rcptPrsn.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(rcptPrsn.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName().trim() +"%')" +
											" OR lower(rcptPrsn.LAST_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
											" ) " ;
							
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(PSUP.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNumber() +"%')";
							if(criteria.getFromDate() !=null  )								
								  condition += " AND PS.PAYMENT_DUE_ON >=" +
										"to_date ('"+ 
								                     DateUtil.convertToDisplayDate( criteria.getFromDate() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  " ;
								        
	                             if( criteria.getToDate()!=null )
	                            	 condition += " AND PS.PAYMENT_DUE_ON <=  to_date ('" + DateUtil.convertToDisplayDate(  criteria.getToDate())+
	                            		                                         " 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";

	 							if(criteria.getReceiptDateFrom() !=null  )								
									  condition += " AND PRCT.CREATED_ON >=" +
											"to_date ('"+ 
									                     DateUtil.convertToDisplayDate( criteria.getReceiptDateFrom() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  " ;
									        
		                             if( criteria.getReceiptDateTo()!=null )
		                            	 condition += " AND PRCT.CREATED_ON <=  to_date ('" + DateUtil.convertToDisplayDate(  criteria.getReceiptDateTo())+
		                            		                                         " 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";
	                             
							if(criteria.getChequeNumber().length()>0)
								condition+=" AND lower(PRCD.METHOD_REF_NO)  like   lower('%" + criteria.getChequeNumber() +"%')";
							if(!criteria.getPaymentStatus().equals("-1"))
								condition+= " AND ps.status_id =  " + criteria.getPaymentStatus() ;
							else
								condition+= " and ps.status_id in (36001,36018)";
							if(!criteria.getBankName().equals("-1"))
								condition+= " AND BANK.BANK_ID  =  '" + criteria.getBankName() +"'"	;
							if(criteria.getBankNameText() != null && criteria.getBankNameText().trim().length()> 0 )
								condition+= " AND LOWER (PRCD.BANK_NAME)  LIKE '%" + criteria.getBankNameText().trim().toLowerCase() +"%'"	;


							if(condition.length() > 0)
					        	query+=" where 1=1  \n"+ condition;
							else 
								query+=" where 1=1 and  ps.status_id in (36001,36018) \n";

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.BOUNCED_CHECKS_DETAILS;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("grp.amount") + "' \"lblAmountEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("grp.amount") + "' \"lblAmountAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.paymentDate") + "' \"lblPaymentDateAr\",\n" +

		        /*******************************                           Summary Lables           *********************************/	        

		        "  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblTotalAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblTotalAmountAr\",\n" + 


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.bouncedChecksDetails.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.bouncedChecksDetails.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.BOUNCED_CHECKS_DETAILS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
