package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.ConstructionProjectsReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ConstructionProjectsSummaryReportProcessor extends AbstractReportProcessor{

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		ConstructionProjectsReportCriteria criteria = (ConstructionProjectsReportCriteria) reportCriteria;

 		String query = "SELECT  " +
 							"PROJ.PROJECT_NUMBER \"projectNumber\" , " +
							"DD.DATA_DESC_EN  \"projectStatusEn\"," +
							"DD.DATA_DESC_AR  \"projectStatusAr\"," +
							"PROJ.PROJECT_NAME \"projectName\" , " +
							"DD2.DATA_DESC_EN  \"projectTypeEn\" , " +
							"DD2.DATA_DESC_AR  \"projectTypeAr\", " +
							"PROJFA.COMPLETION_PERCENTAGE  \"completionPercentage\" , " +
							"PM.MILESTONE_NAME  \"achievedMilestone\" " +
							" FROM " +
							" PROJECT PROJ " +
							" INNER JOIN DOMAIN_DATA DD ON PROJ.PROJECT_STATUS_ID = DD.DOMAIN_DATA_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON PROJ.PROJECT_TYPE_ID = DD2.DOMAIN_DATA_ID " +
							" left outer JOIN PROJECT_FOLLOWUP_ACTIVITY PROJFA ON PROJ.PROJECT_ID = PROJFA.PROJECT_ID " +
							" left outer JOIN PROJECT_MILESTONE PM ON PM.PROJECT_MILESTONE_ID = PROJFA.PROJECT_MILESTONE_ID " ;
	

	    		String condition="";
							if(criteria.getProjectNumber().length() >0)
								condition+=" AND lower(PROJ.PROJECT_NUMBER) like   lower('%" + criteria.getProjectNumber() +"%')";
							if(criteria.getProjectName() .length() >0)
								condition+=" AND lower(PROJ.PROJECT_NAME) like   lower('%" + criteria.getProjectName() +"%')";
							
							if(!criteria.getProjectStatus().equals("-1"))
								condition+= " AND DD.DOMAIN_DATA_ID = " + criteria.getProjectStatus(); 
							if(!criteria.getProjectType().equals("-1"))
								condition+= " AND DD2.DOMAIN_DATA_ID = " + criteria.getProjectType(); 
			
						//	if(condition.length() > 0)
//					        	query+="where PROJFA.IS_LAST_FOLLOW_UP = 1 \n"+ condition;
							query+="where 1= 1 \n"+ condition;
		

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.CONSTRUCTION_PROJECTS_SUMMARY;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
        		"  '" + ReportMessageResource.getPropertyEn("project.projectNumber") + "' \"lblProjectNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("project.projectStatus") + "' \"lblProjectStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("project.projectName") + "' \"lblProjectNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("project.projectType") + "' \"lblProjectTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("projectMilestone.completionPercentage") + "' \"lblCompletionPercentageEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("projectMilestone.milestoneName") + "' \"lblAchievedMilestoneEn\",\n" + 
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("project.projectNumber") + "' \"lblProjectNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("project.projectStatus") + "' \"lblProjectStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("project.projectName") + "' \"lblProjectNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("project.projectType") + "' \"lblProjectTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("projectMilestone.completionPercentage") + "' \"lblCompletionPercentageAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("projectMilestone.milestoneName") + "' \"lblAchievedMilestoneAr\",\n" + 

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.constructionProjectsSummary.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.constructionProjectsSummary.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.CONSTRUCTION_PROJECTS_SUMMARY_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}

