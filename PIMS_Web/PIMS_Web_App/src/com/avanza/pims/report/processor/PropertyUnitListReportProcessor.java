package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PropertyUnitListReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;

public class PropertyUnitListReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Long activeContractStatusId =  CommonUtil.getDomainDataId(WebConstants.CONTRACT_STATUS, WebConstants.CONTRACT_STATUS_ACTIVE);
		
		PropertyUnitListReportCriteria criteria = (PropertyUnitListReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		///////////////////////////
		
		String query = "SELECT " +
		" 1 \"Field14\" , " +
		" PRSN.FIRST_NAME \"Field11\" , " +
		" DD_OT.DATA_DESC_AR \"Field12\" , " +
		" CNTRT.CONTRACT_NUMBER \"Field13\" , " +
		" prop.land_number \"Field15\" , " +
		" prop.receiving_date \"Field1\" , " +
		" PROP.PROPERTY_NUMBER \"propertyNumberEn\" , " +
        " PROP.COMMERCIAL_NAME  \"propertyNameEn\" , " +
        
//        -- Property Type 
        
        " DD_PT.DATA_DESC_EN  \"propertyTypeEn\" , " +
        " DD_PT.DATA_DESC_AR  \"propertyTypeAr\" , " +
        
//        -- Addresss
        " CI.ADDRESS1 || ' ' || CI.ADDRESS2 \"addressEn\" , " +
        " CI.ADDRESS2  , " +
        
        " RGN_AREA.DESCRIPTION_AR \"emirateEn\" , " +
        
        " F.FLOOR_ID  , " +
        " F.FLOOR_NAME , " +
        " DD_FT.DATA_DESC_EN  , " +
        " DD_FT.DATA_DESC_AR  ,  " +
        " F.FLOOR_NUMBER \"floorNumberEn\" , " +
        
        " U.UNIT_NUMBER \"unitNumberEn\" , " +
        " U.UNIT_DESC \"unitDesc\" , " +
        " U.RENT_VALUE \"annualRent\" , " +
        " U.UNIT_AREA \"area\" , " +
        " U.ACCOUNT_NUMBER \"unitCostCenter\" , " +
//        " count(DISTINCT U.UNIT_ID) \"totalUnitsEn\" ,"+
        
        
//        -- Unit Type
        " DD_UT.DATA_DESC_EN \"unitTypeEn\" , " +
        " DD_UT.DATA_DESC_AR  \"unitTypeAr\" , " +
        
//        -- Unit Side Type
        " DD_US.DATA_DESC_EN \"unitSideEn\" , " +
        " DD_US.DATA_DESC_AR  \"unitSideAr\" ,  " +
//        --Unit Status 
        " DD_USTATUS.DATA_DESC_EN \"unitStatusEn\" , " +
        " DD_USTATUS.DATA_DESC_AR  \"unitStatusAr\" ,  " +
        
        
        " U.NO_OF_BED  \"numberOfRoomsEn\" ," +
        " U.USAGE_TYPE_ID \"usageTypeId\" ,"+
		" DD_UU.DATA_DESC_EN \"usageTypeEn\" ,"+	
		" DD_UU.DATA_DESC_AR	\"usageTypeAr\"  "+
        
        
//        -- Sum of Unit  -- Count ( Unit Rows )
        
//        -- GRAND TOTAL for ALL Property -- SUM  ( Sum of All Property )
        
" FROM  " +

" PROPERTY PROP " +
        " INNER JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
        " INNER JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID  " +
       // " LEFT JOIN CONTRACT_UNIT CU ON CU.UNIT_ID = U.UNIT_ID " +
        //" LEFT JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID AND CNTRT.STATUS_ID = " + activeContractStatusId.toString() +
        " LEFT JOIN (CONTRACT CNTRT INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID AND CNTRT.STATUS_ID = 310010) " +
        " ON CU.UNIT_ID = U.UNIT_ID" +
        " LEFT JOIN PERSON PRSN ON PRSN.PERSON_ID = CNTRT.TENANT_ID " +
        " INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
        " LEFT JOIN DOMAIN_DATA DD_OT ON PROP.OWNERSHIP_TYPE_ID = DD_OT.DOMAIN_DATA_ID  " +
        " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID " +

        " LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_US ON U.UNIT_SIDE_TYPE_ID = DD_US.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_USTATUS ON U.STATUS_ID = DD_USTATUS.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_FT ON F.TYPE_ID = DD_FT.DOMAIN_DATA_ID " +
        " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.CITY_ID " +
        " LEFT JOIN DOMAIN_DATA DD_UU ON DD_UU.DOMAIN_DATA_ID = U.USAGE_TYPE_ID ";
                    

				String condition = "";
				
							if(criteria.getTxtContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getTxtContractNumber() +"%')";
							
							if(criteria.getTxtTenantName().length() >0)
							    condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTxtTenantName().trim() +"%')";	
							    //" AND (PRSN.FIRST_NAME || ' ' || PRSN.MIDDLE_NAME || ' ' || PRSN.LAST_NAME) LIKE '%"+ criteria.getTxtTenantName().trim() +"%'" ;
								/*condition+= " AND (PRSN.FIRST_NAME like '%" + criteria.getTxtTenantName().trim() +"%'" +
													" OR PRSN.MIDDLE_NAME like  '%" + criteria.getTxtTenantName().trim() +"%'"+
													" OR PRSN.LAST_NAME like  '%" + criteria.getTxtTenantName().trim() +"%'"+
													" ) ";*/

							if(criteria.getTxtPropertyName().length() >0)
								condition+=" AND PROP.COMMERCIAL_NAME like  '%" + criteria.getTxtPropertyName() +"%'";
							if(criteria.getTxtUnitNumber()!=null && criteria.getTxtUnitNumber().length() >0)
								condition+=" AND lower (U.UNIT_NUMBER)  like lower( '%" + criteria.getTxtUnitNumber() +"%')";
							if(criteria.getTxtCostCenter().length() >0)
								condition+=" AND lower (U.ACCOUNT_NUMBER)  like lower( '%" + criteria.getTxtCostCenter() +"%')";
							if(criteria.getClndrFromDate() !=null  && criteria.getCldrToDate()!=null)
								condition += " AND PROP.RECEIVING_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrFromDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getCldrToDate() )+"', 'dd/MM/yyyy')";
							if(!criteria.getCboPropertyType().equals("-1"))
								condition+= " AND PROP.TYPE_ID = '" + criteria.getCboPropertyType() +"'"	;
							if(!criteria.getCboPropertyUsage().equals("-1"))
				        		condition+=" AND U.USAGE_TYPE_ID="+criteria.getCboPropertyUsage()+"";
							if(!criteria.getCboUnitStatus().equals("-1")){
								condition+= " AND U.STATUS_ID  =  " + criteria.getCboUnitStatus();
							}
							else {
								condition+= " AND U.STATUS_ID  <> 2011 ";
							}
								
							if(!criteria.getCboUnittype().equals("-1"))
								condition+= " AND U.UNIT_TYPE_ID  =  " + criteria.getCboUnittype();
							if(!criteria.getCboCommunity().equals("-1"))
				        		condition+=" AND RGN_AREA.REGION_ID= '"+criteria.getCboCommunity()+"'";
							if(criteria.getTxtFloorNumber().length() >0)
								condition+=" AND lower(F.FLOOR_NUMBER) like lower('%" + criteria.getTxtFloorNumber()+"%')" ;
							if(criteria.getTxtLandNumber().length()>0)
				        		condition+=" AND PROP.LAND_NUMBER like '%"+criteria.getTxtLandNumber()+"%'";
							if(criteria.getTxtPropertNumber().length()>0)
					        	condition+=" AND lower(PROP.PROPERTY_NUMBER) like lower('%"+criteria.getTxtPropertNumber()+"%')";
						
							if( criteria.getUnitRentAmountFrom() != null &&  criteria.getUnitRentAmountFrom().length() > 0 )
								condition+=" AND U.RENT_VALUE >= "+criteria.getUnitRentAmountFrom().trim();
					
							if( criteria.getUnitRentAmountTo() != null &&  criteria.getUnitRentAmountTo().length() > 0 )
								condition+=" AND U.RENT_VALUE <= "+criteria.getUnitRentAmountTo().trim();
					
							if( criteria.getContractRentAmountFrom() != null &&  criteria.getContractRentAmountFrom().length() > 0 )
								condition+=" AND CNTRT.RENT_AMOUNT >= "+criteria.getContractRentAmountFrom().trim();
					
							if( criteria.getContractRentAmountTo() != null &&  criteria.getContractRentAmountTo().length() > 0 )
								condition+=" AND CNTRT.RENT_AMOUNT <= "+criteria.getContractRentAmountTo().trim();
					
							if( criteria.getTxtUnitNumber() != null &&  criteria.getTxtUnitNumber().length() > 0 )
								condition+=" AND U.UNIT_NUMBER LIKE '%"+criteria.getTxtUnitNumber().trim() + "%'";
					
							if( criteria.getTxtCostCenter() != null &&  criteria.getTxtCostCenter().length() > 0 )
								condition+=" AND lower (U.ACCOUNT_NUMBER)  like lower( '%" + criteria.getTxtCostCenter().trim() +"%')";
								//condition+=" AND U.ACCOUNT_NUMBER = "+criteria.getTxtCostCenter().trim(); // field name different in DB.
					
							if( criteria.getTxtOwnerShip() != null &&  !criteria.getTxtOwnerShip().equals("-1") )
								condition+=" AND PROP.OWNERSHIP_TYPE_ID = "+criteria.getTxtOwnerShip().trim();
					
							
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY F.floor_sequence, f.type_id,u.unit_number ";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PROPERTY_UNIT_LIST;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("property.number") + "' \"lblPropertyNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.Emirate") + "' \"lblEmirateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.unitType") + "' \"lblUnitTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentFloorNumber") + "' \"lblFloorNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblAddressEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiry.unitSide") + "' \"lblUnitSideEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.unit.numberOfBedroom") + "' \"lblNumberOfRoomsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalUnits") + "' \"lblTotalUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalUnits") + "' \"lblGrandSumEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("propertyUnitListRrpoert.commercialUsageCount") + "' \"lblcommercialEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("propertyUnitListRrpoert.residentialUsageCount") + "' \"lblResidentialEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("propertyUnitListRrpoert.multipurposeUsageCount") + "' \"lblMultipurposeEn\",\n" +
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("property.number") + "' \"lblPropertyNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.Emirate") + "' \"lblEmirateAn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.unitType") + "' \"lblUnitTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentFloorNumber") + "' \"lblFloorNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblAddressAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiry.unitSide") + "' \"lblUnitSideAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.unit.numberOfBedroom") + "' \"lblNumberOfRoomsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalUnits") + "' \"lblTotalUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalUnits") + "' \"lblGrandSumAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("propertyUnitListRrpoert.commercialUsageCount") + "' \"lblcommercialAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("propertyUnitListRrpoert.residentialUsageCount") + "' \"lblResidentialAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("propertyUnitListRrpoert.multipurposeUsageCount") + "' \"lblMultipurposeAr\",\n" +

		        /*******************************                           Summary Lables           *********************************/	        
		        
		        
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.totalUnits") + "' \"lblNumberOfUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.totalUnits") + "' \"lblNumberOfUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.BedRooms") + "' \"lblUnitCountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.totalBedRooms") + "' \"lblTotalUnitCountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("unit.BedRooms") + "' \"lblUnitCountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.totalBedRooms") + "' \"lblTotalUnitCountAr\" , \n" + 

		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.propertyUnitListReport.rpt") + "' \"lblPropertyUnitListReportEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.propertyUnitListReport.rpt") + "' \"lblPropertyUnitListReportAr\" , \n" + 
	             "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
        System.out.print("222-------------" + query);
		dataDef = ReportConstant.DataDef.PROPERTY_UNIT_LIST_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
