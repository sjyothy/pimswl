package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PortFolioDetailsCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;


public class SummaryInvestmentEvaluationProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		PortFolioDetailsCriteria criteria = ( PortFolioDetailsCriteria ) reportCriteria;
        int currentQuarter = 0;
        int previousQuarter =0;
        
        if( criteria.getQuarter()!=null && criteria.getQuarter().trim().length()>0 ) {
        	currentQuarter = Integer.parseInt( criteria.getQuarter());
        	if( !criteria.getQuarter().equals("159001") )
        	previousQuarter= Integer.parseInt( criteria.getQuarter())-1;
        }		
        
        String query = " SELECT R.*,"
        	    +currentQuarter+"\"parameterCurrQuarter\", " +
                +previousQuarter+"\"parameterPrvQuarter\" "+ 
        		" FROM PORTFOLIO_DETAILS_REPORT R ";
        query += whereClause( criteria );	        
        System.out.print("-------------" + query);
        
		String dataDef = ReportConstant.DataDef.PORTFOLIO_DETAILS;        
		reportQueryMap.put(dataDef, query+"");         
        
        
		
        query = "SELECT \n" + 
        

        
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.aggregateAppDep") + "' \"aggregateAppDepEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.fees") + "' \"feesEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.totalInvestmentCost") + "' \"totalInvestmentCostEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.investmentAssetName") + "' \"investmentAssetNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.assetClassName") + "' \"assetClassNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.investmentPeriod") + "' \"investmentPeriodEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.noUnits") + "' \"noUnitsEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.currnoUnits") + "' \"currnoUnitsEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.unitPrice") + "' \"unitPriceEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.currunitPrice") + "' \"currunitPriceEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.evalPrvQrt") + "' \"evalPrvQrtEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.evalCurrQrt") + "' \"evalCurrQrtEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.qrtAppDep") + "' \"qrtAppDepEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.realizedProfitAgg") + "' \"realizedProfitAggEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.realizedProfitQrt") + "' \"realizedProfitQrtEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.percentTotalInv") + "' \"percentTotalInvEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.aggROI") + "' \"aggROIEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.qrtROI") + "' \"qrtROIEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.weightedqrtROI") + "' \"weightedqrtROIEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.weightedROI") + "' \"weightedROIEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolio.manage.tab.portfolio.account") + "' \"accountEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("paymentSchedule.percentage") + "' \"accountPercentageEn\",\n" +
        		        		
		        
		        /*******************************                           Arabic Labels             *********************************/
/*	TODO MAJID FOR UNDERSTANDING I AM USING ENGLISH */
        		
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.aggregateAppDep") + "' \"aggregateAppDepAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.fees") + "' \"feesAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.totalInvestmentCost") + "' \"totalInvestmentCostAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.investmentAssetName") + "' \"investmentAssetNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.assetClassName") + "' \"assetClassNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.investmentPeriod") + "' \"investmentPeriodAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.noUnits") + "' \"noUnitsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.currnoUnits") + "' \"currnoUnitsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.unitPrice") + "' \"unitPriceAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.currunitPrice") + "' \"currunitPriceAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.evalPrvQrt") + "' \"evalPrvQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.evalCurrQrt") + "' \"evalCurrQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.qrtAppDep") + "' \"qrtAppDepAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.realizedProfitAgg") + "' \"realizedProfitAggAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.realizedProfitQrt") + "' \"realizedProfitQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.qrtROI") + "' \"qrtROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.aggROI") + "' \"aggROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.percentTotalInv") + "' \"percentTotalInvAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.weightedqrtROI") + "' \"weightedqrtROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolioDetailsReport.lbl.weightedROI") + "' \"weightedROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("portfolio.manage.tab.portfolio.account") + "' \"accountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("paymentSchedule.percentage") + "' \"accountPercentageAr\",\n" +
/*TOOD END */
        		/*	TODO MAJID FOR UNDERSTANDING I AM USING ENGLISH */	        
/*
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.aggregateAppDep") + "' \"aggregateAppDepAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.fees") + "' \"feesAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.totalInvestmentCost") + "' \"totalInvestmentCostAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.investmentAssetName") + "' \"investmentAssetNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.assetClassName") + "' \"assetClassNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.investmentPeriod") + "' \"investmentPeriodAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.noUnits") + "' \"noUnitsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.currnoUnits") + "' \"currnoUnitsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.unitPrice") + "' \"unitPriceAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.currunitPrice") + "' \"currunitPriceAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.evalPrvQrt") + "' \"evalPrvQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.evalCurrQrt") + "' \"evalCurrQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.qrtAppDep") + "' \"qrtAppDepAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.realizedProfitAgg") + "' \"realizedProfitAggAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.realizedProfitQrt") + "' \"realizedProfitQrtAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.qrtROI") + "' \"qrtROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.aggROI") + "' \"aggROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.percentTotalInv") + "' \"percentTotalInvAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.weightedqrtROI") + "' \"weightedqrtROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolioDetailsReport.lbl.weightedROI") + "' \"weightedROIAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("portfolio.manage.tab.portfolio.account") + "' \"accountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("paymentSchedule.percentage") + "' \"accountPercentageAr\",\n" +
*/        		
        		
        		/* TOOD END */
        		
        		
        		
        		
        		
		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        
        		"  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("summaryInvestmentEvaluationReport.lbl.reportName") + "' \"lblReportNameEn\",\n" +
		        
		        //TODO  FOR UNDERSTANDING I COMMENTED THIS OUT MAJID
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("summaryInvestmentEvaluationReport.lbl.reportName") + "' \"lblReportNameAr\",\n" +
		        
		        
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        
		        
		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PORTFOLIO_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        

        return reportQueryMap;

      
	}
	
	public String whereClause( PortFolioDetailsCriteria criteria )	{
		
		int currentQuarter = 0;
	    int previousQuarter = 0;
	    if( criteria.getQuarter()!=null && criteria.getQuarter().trim().length()>0 )   {
	       currentQuarter = Integer.parseInt( criteria.getQuarter());
	       if( !criteria.getQuarter().equals("159001") ) {
	    	   previousQuarter= Integer.parseInt( criteria.getQuarter())-1;
	       }  
	    }
		
		String where = " WHERE 1=1 ";
		if( criteria.getPortfolioNumber() !=null && criteria.getPortfolioNumber().trim().length() > 0 )
		    where += " AND lower( PORTFOLIO_NUMBER ) like '%"+criteria.getPortfolioNumber().trim().toLowerCase()+"%'"; 
		if( criteria.getPortfoliName() !=null && criteria.getPortfoliName().trim().length() > 0 )
		    where += " AND ( lower( PORTFOLIO_name_en ) like '%"+criteria.getPortfoliName().trim().toLowerCase()+"%' or lower( PORTFOLIO_name_ar ) like '%"+criteria.getPortfoliName().trim().toLowerCase()+"%'   )"; 
		
		if( criteria.getQuarter()!=null && criteria.getQuarter().trim().length()>0 ) {
			where += " AND  " +
				" (" +
					" EVALUATION_QUARTER_ID = '"+currentQuarter + "'"
					//;
					+ " OR " +
					" EVALUATION_QUARTER_ID = '"+previousQuarter + "'" +
				") ";
		}	
		
		
		if( criteria.getYear() != null && criteria.getYear().trim().length()>0 )
			where += " AND EVALUATION_YEAR = '"+criteria.getYear()+"'";
		
		/*
		if( criteria.getAccountType() != null && criteria.getAccountType().trim().length()>0 )
			where += " AND ACCOUNT_TYPE_ID = '"+criteria.getAccountType()+"'";
		*/
		
		
		if (criteria.getAssetName()!=null && !"".equalsIgnoreCase(criteria.getAssetName())) {
		    where += " AND ( lower( ASSET_NAME_EN ) like '%"+criteria.getAssetName().trim().toLowerCase()+"%' OR lower( ASSET_NAME_AR ) like '%"+criteria.getAssetName().trim().toLowerCase()+"%'   )";		
		}
		
		if (criteria.getAssetCategory()!=null && !"".equalsIgnoreCase(criteria.getAssetCategory())) {
		    where += " AND ( lower( ASSET_CLASS_NAME_EN ) like '%"+criteria.getAssetCategory().trim().toLowerCase()+"%' OR lower( ASSET_CLASS_NAME_AR ) like '%"+criteria.getAssetCategory().trim().toLowerCase()+"%'   )";		
		}

		return  where;
		
	}
	
	

}
