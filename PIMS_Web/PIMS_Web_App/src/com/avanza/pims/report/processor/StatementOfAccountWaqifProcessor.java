package com.avanza.pims.report.processor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.avanza.core.util.Logger;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class StatementOfAccountWaqifProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		
		Session session = (Session)EntityManager.getBroker().getSession();
		Query query = session.getNamedQuery("com.avanza.pims.entity.Endowment.SP_ENDOWMENT_MASRAF_COUNT").setParameter("endowmentId", -1);

		List  list = query.list();
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{
			StringBuilder ccCsv = new StringBuilder("");
			for (String cc: criteria.getCostCenters()) 
			{
				ccCsv.append("'").append( cc ).append( "'," );
			}
			String cc=ccCsv.toString();
			if(cc.length() > 0 && cc.contains(","))
			{
				cc= cc.substring(0, (cc.length()-1) );
			}
//			executeEndowmentDetailsQuery(criteria,reportQueryMap,cc );
			executeTransactionQuery(criteria,reportQueryMap,cc );
//			executeChequeDetalsQuery(criteria,reportQueryMap,cc );
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}
	@SuppressWarnings( "unchecked" )
	private void executeEndowmentDetailsQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception 
	{
		
		Endowment endowment = EntityManager.getBroker().findById(Endowment.class, criteria.getEndowmentId() );
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		Double openingBalance    = 0.00d;
		Double closingBalance    = 0.00d;
		Double finalBalance = 0.00d;
		Session session = (Session)EntityManager.getBroker().getSession();
		Query query = session.getNamedQuery("com.avanza.pims.entity.SPStmtAccountWaqif.SPGETWAQIFBALANCEREPORT");
		query.setParameter("fromdate", criteria.getFormattedFromDate());
		query.setParameter("todate", criteria.getFormattedToDate() );
		query.setParameter("waqifName", criteria.getEndowmentId());
		List  list = query.list();
		if( list != null && list.size() > 0 )
		{
			Object[] obj =  (Object[])list.get(0);
			if( obj[0] != null )
			{
				openingBalance = Double.valueOf( obj[0].toString() ) ;
			}
			if( obj[1] != null )
			{
				closingBalance = Double.valueOf( obj[1].toString() ) ;
			}
		}
		String queryStr  = " select '"+ 
								   endowment.getEndowmentNum() + "' as \"endowmentNumber\", '" +
								   endowment.getCostCenter()+ "' as \"costCenter\",'"+
								   criteria.getFormattedFromDateForReportDisplay() + "' as \"fromDate\",'"+
								   criteria.getFormattedToDateForReportDisplay() + "' as \"toDate\",'"+
								   criteria.getEndowmentId().toString()+"' as \"endowmentId\",'"+
								   endowment.getEndowmentName()+"' as \"name\", cast('" +
								   oneDForm.format( closingBalance )+"'  as DECIMAL(20,2) )  as \"balance\",cast('" +
								   oneDForm.format( openingBalance )+"'  as DECIMAL(20,2) )  as \"openingbalance\",cast('" +
								   oneDForm.format( finalBalance )+"' as DECIMAL(20,2) )  as \"finalBalance\" " +
						" from DUAL " ;

		System.out.print("-------------" + queryStr);
//		query += condition.toString();
		String dataDef = "EndowmentDetailsDataDef";        
		reportQueryMap.put(dataDef, queryStr+"");
	}

	private void executeTransactionQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		
		query += " select '"+ criteria.getReportGeneratedBy() +"' as \"field3\", "+
					  " EF.FILE_ID \"fileId\","+
					  " EF.FILE_NUMBER \"fileNumber\","+
					  " EF.STATUS_ID \"fileStatusId\","+
					  " EF.FILE_NAME \"fileName\","+
					  " DDEFS.DATA_DESC_EN \"fileStatusEn\","+
					  " DDEFS.DATA_DESC_AR \"fileStatusAr\","+
					  " P.PERSON_ID \"waqifId\","+
					  " P.GRP_CUSTOMER_NO \"waqifGRPNumber\","+
					  " CASE"+
					  " WHEN P.COMPANY_NAME IS NULL"+
					    " OR LENGTH (LTRIM(RTRIM(P.COMPANY_NAME)))<=0"+
					    " THEN"+
					    " CASE"+
					      " when P.MIDDLE_NAME is null AND P.LAST_NAME    IS NULL"+
					        " THEN P.FIRST_NAME"+
					        " ELSE"+
					        " case"+
					          " when P.MIDDLE_NAME is not null and P.LAST_NAME    is null"+ 
					            " THEN P.FIRST_NAME|| ' '|| P.MIDDLE_NAME"+
					            " ELSE"+
					            " case"+
					              " WHEN P.MIDDLE_NAME IS NULL AND P.LAST_NAME    IS NOT NULL"+
					                " THEN P.FIRST_NAME|| ' '|| P.LAST_NAME"+
					                " ELSE P.FIRST_NAME|| ' '|| P.MIDDLE_NAME||' '|| P.LAST_NAME"+
					                " END"+
					              " END"+
					          "     END"+
					  "   ELSE P.COMPANY_NAME"+
					  " END           AS \"waqifName\","+
					  " E.ENDOWMENT_ID  AS \"endowmentId\","+
					  " ENDOWMENT_NUM AS \"endowmentNum\","+
					  " E.ENDOWMENT_NAME \"endowmentName\","+
					  " E.ENDOW_FOR \"endowFor\","+
					  " AST.ASSET_TYPE_ID \"assetTypeId\","+
					  " AST.ASSET_TYPE_NAME_AR \"assetTypeAr\","+
					  " AST.ASSET_TYPE_NAME_EN \"assetTypeEn\","+
					  " E.COST_CENTER AS \"costCenter\","+
					  " \"trxDate\",\"credit\",\"debit\",\"amount\","+
					  " \"descriptionAr\",\"descriptionEn\","+
					  " EP.PURPOSE_NAME_AR \"endowmentPurpose\","+
					  " EC.CATEGORY_NAME_AR \"endowmentCategory\","+
					  " DDES.DATA_DESC_AR \"endowmentStatus\","+
					  " DDEMC.DATA_DESC_AR \"endowmentMainCategory\","+
					  " E.FIELD_DETAILS_1 \"endowmentLandNum\","+
					  " E.FIELD_DETAILS_3 \"endowmentCommunity\","+
					  " E.ENDOW_FOR \"field4\","+
					  " prop.property_number \"endowmentProperty\","+
					  " TEMC.COUNT \"endowmentBenefs\","+
					  " CASE"+
					  " WHEN PRSN.PERSON_ID IS NULL then 'AMAF'" +
					  "ELSE "+
					  " case WHEN PRSN.COMPANY_NAME IS NULL"+
					    " OR LENGTH (LTRIM(RTRIM(PRSN.COMPANY_NAME)))<=0"+
					    " THEN"+
					    " CASE"+
					      " when PRSN.MIDDLE_NAME is null AND PRSN.LAST_NAME    IS NULL"+
					        " THEN PRSN.FIRST_NAME"+
					        " ELSE"+
					        " case"+
					          " when PRSN.MIDDLE_NAME is not null and PRSN.LAST_NAME    is null"+ 
					            " THEN PRSN.FIRST_NAME|| ' '|| PRSN.MIDDLE_NAME"+
					            " ELSE"+
					            " case"+
					              " WHEN PRSN.MIDDLE_NAME IS NULL AND PRSN.LAST_NAME    IS NOT NULL"+
					                " THEN PRSN.FIRST_NAME|| ' '|| PRSN.LAST_NAME"+
					                " ELSE PRSN.FIRST_NAME|| ' '|| PRSN.MIDDLE_NAME||' '|| PRSN.LAST_NAME"+
					                " END"+
					              " END"+
					          "     END"+
					  "   ELSE PRSN.COMPANY_NAME"+
					  " END " +
					  " END   AS \"endowmentManager\","+
					  " \"trxTypeEn\",\"trxTypeAr\","+
					  " \"transId\","+
					  " coalesce(OB.\"openingBalance\",0) \"openingBalance\",";
					  
					if( criteria.getFromDate() != null )
					{
						query+= "'"+criteria.getFormattedFromDateForReportDisplay()+"' as \"field1\",";
					}
					if( criteria.getToDate() != null )
					{
						query+= "'"+criteria.getFormattedToDateForReportDisplay()+"'\"field2\",";
					}
					query+=	" coalesce(CB.\"closingBalance\",0) \"closingBalance\""+ 
					" from ENDOWMENT_FILE EF"+
					" inner join PERSON P on EF.FILE_OWNER=P.PERSON_ID"+
					" inner join DOMAIN_DATA DDEFS on DDEFS.DOMAIN_DATA_ID=EF.STATUS_ID"+
					" LEFT JOIN END_FILE_ASSO EFA ON EF.FILE_id=EFA.FILE_ID"+
					" LEFT JOIN ENDOWMENT_PURPOSE EP ON EP.PURPOSE_ID=EFA.PURPOSE_ID"+
					" LEFT JOIN ENDOWMENT_CATEGORY EC ON EC.CATEGORY_ID=EFA.CATEGORY_ID"+
					" left join DOMAIN_DATA DDES on DDES.DOMAIN_DATA_ID=EFA.STATUS_ID"+
					" LEFT JOIN ENDOWMENT E ON E.ENDOWMENT_ID=EFA.ENDOWMENT_ID"+
					
					" left join DOMAIN_DATA DDEMC on DDEMC.DOMAIN_DATA_ID=E.main_category_id"+
					" LEFT JOIN PROPERTY prop ON E.ENDOWMENT_ID=prop.ENDOWMENT_ID"+
					" LEFT JOIN PERSON PRSN ON E.MANAGER_ID=PRSN.PERSON_ID"+
					" LEFT JOIN TEMP_ENDOWMENT_MASRAF_COUNT TEMC ON TEMC.ENDOWMENT_ID=E.ENDOWMENT_ID"+
					" LEFT JOIN STATEMENT_OF_ACCOUNT_ENDOWMENT STE ON ( STE.ENDOWMENT_ID=E.ENDOWMENT_ID";
					if( criteria.getFromDate() != null )
					{
						query+= " and trans_date >= to_date('"+criteria.getFormattedFromDate()+"' , 'dd/MM/yyyy hh24:MI:ss')";
					}
					if( criteria.getToDate() != null )
					{
						query+=" and trans_date <= to_date('"+ criteria.getFormattedToDate()+"' , 'dd/MM/yyyy hh24:MI:ss')";
					}
					query+=") left join ASSET_TYPE AST on AST.ASSET_TYPE_ID=E.ENDOWMENT_TYPE_ID"+
					" LEFT JOIN ("+
			        " 				select coalesce(sum(\"amount\"),0) as \"openingBalance\", endowment_id from statement_of_account_endowment "+
			        " 				where trans_date<to_date('"+criteria.getFormattedFromDate()+"','dd/MM/yyyy hh24:mi:ss') group by endowment_id  " +
			        " ) OB on OB.endowment_id=e.endowment_id"+
			        " LEFT JOIN ("+
			        " 				select coalesce(sum(\"amount\"),0) as \"closingBalance\", endowment_id from statement_of_account_endowment "+
			        " 				where trans_date<to_date('"+criteria.getFormattedToDate()+"','dd/MM/yyyy hh24:mi:ss') group by endowment_id  " +
			        " ) CB on CB.endowment_id=e.endowment_id"+
					" where    EF.IS_DELETED =0";
					//" AND      p.person_id     =34894"
					
			

		if(criteria.getWaqifName() != null)
		{
			condition.append(" AND (LOWER(P.FIRST_NAME ||' '||P.MIDDLE_NAME||' '||P.LAST_NAME) like '%").append( criteria.getWaqifName()).
					  append("%' OR lower(p.COMPANY_NAME) like lower( '%") .append(  criteria.getWaqifName() ).append("%') )");
		}
		if(criteria.getEndowmentId() != null)
		{
			condition.append(" and E.ENDOWMENT_ID = ").append(  criteria.getEndowmentId() );
		}
		if(criteria.getEndowmentFileNumber() != null )
		{
			condition.append(" and lower(EF.FILE_NUMBER) like '%").append(  criteria.getEndowmentFileNumber() ).append("%'");
		}
		if(criteria.getEndowmentFileName() != null )
		{
			condition.append(" and lower(EF.FILE_NAME) like '%").append(  criteria.getEndowmentFileName() ).append("%'");
		}
		condition.append( " ORDER BY \"waqifName\",EF.FILE_ID,e.endowment_id,\"trxDate\",\"transId\"" );
		query += condition.toString();
		System.out.print("-------------" + query);
		reportQueryMap.put("WaqifBalanceDetailsDataDef", query);
	
	}
	private void executeChequeDetalsQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		query += " select ENDOWMENT_ID as \"endowmentId\",  COST_CENTER as \"costCenter\", \"requestNumber\", \"requestType\", \"paymentScheduleId\", " +
				 " \"paymentNumber\", \"status\", \"statusId\", \"paymentDueOn\", \"amount\", \"description\", \"methodRefNo\", \"bank\", " +
				 " \"realizedOn\", \"createOn\" ,\"field1\" ,\"field2\" ,null as \"field3\" " +
				 " from STMT_END_CHQ_DET_VIEW WHERE 1=1" ;
		if(criteria.getEndowmentId() != null)
		{
			condition.append(" and ENDOWMENT_ID = ").append(  criteria.getPersonId() );
		}
		if(ccCsv.length() > 0 )
		{
			condition.append(" and cost_center in (").append(  ccCsv ).append(")");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "ChequeDetailsDataDef#ChequeDetails";        
		reportQueryMap.put(dataDef, query+"");
	
		
	}
	
	}
