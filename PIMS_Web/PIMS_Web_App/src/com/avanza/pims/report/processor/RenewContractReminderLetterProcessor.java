package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.RenewContractsReminderLetterCriteria;

public class RenewContractReminderLetterProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		RenewContractsReminderLetterCriteria criteria = (RenewContractsReminderLetterCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = " SELECT " ;
		if(criteria.getContractRentQuery() != null && criteria.getContractRentQuery().trim().length() > 0)
			query += " A.NEW_RENT_AMOUNT renewalRent,";
		else
			query += criteria.getRentValue() + " renewalRent," ;
		
		query +="PROP.CONTACT_INFO_ID," +
		"CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) CONTRACT_ID," +
		"CNTRT.CONTRACT_NUMBER  contractNo,"  +
		"TO_CHAR( CNTRT.START_DATE , 'dd-MON-yyyy' ) contractStartDate," +
		"TO_CHAR( CNTRT.END_DATE , 'dd-MON-yyyy' ) expiryDate, " +
		"NVL(PROP.COMMERCIAL_NAME, '---') propertyName," + 
		"U.UNIT_NUMBER  ||' / '|| U.UNIT_DESC unitNo," +   
		"COALESCE(PRSN.CELL_NUMBER,(SELECT " +
		" telephoneNo FROM ( " + 
		"SELECT " +
		"   CASE " +
		"          WHEN CI.MOBILE_NUMBER IS NOT NULL " +
		"             THEN CI.MOBILE_NUMBER " +
		"          WHEN CI.OFFICE_PHONE IS NOT NULL " +
		"            THEN CI.OFFICE_PHONE " +
		"          WHEN CI.HOME_PHONE IS NOT NULL " +
		"            THEN CI.HOME_PHONE " +
		"          ELSE NULL " +
		"   END telephoneno " +
		 " ,PCI.PERSON_ID personId " +
		"FROM PERSON_CONTACT_INFO PCI " +
		"LEFT JOIN CONTACT_INFO CI ON PCI.CONTACT_INFO_ID =CI.CONTACT_INFO_ID " +
		"ORDER BY CI.CONTACT_INFO_ID DESC " +
		") WHERE personId = PRSN.PERSON_ID AND telephoneNo IS NOT NULL AND ROWNUM=1)) telephoneNo," +
		"PRSN.FIRST_NAME personFirstName,"+
		"PRSN.MIDDLE_NAME personMiddleName,"+
		"PRSN.LAST_NAME personLastName,"+
		"PRSN.COMPANY_NAME personCompanyName,"+
		"R.DESCRIPTION_AR AS community " +
		"FROM  CONTRACT CNTRT "+ 
		"INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +  
		"INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID "+ 
		"INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
		"INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " + 
		"INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " + 
		"LEFT JOIN CONTACT_INFO CIPROP ON CIPROP.CONTACT_INFO_ID =PROP.CONTACT_INFO_ID " + 
		"LEFT JOIN REGION R ON R.REGION_ID=CIPROP.CITY_ID" ;
		
		if(criteria.getContractRentQuery() != null && criteria.getContractRentQuery().trim().length() > 0)
			{
				query += " INNER JOIN " +criteria.getContractRentQuery();
				query += "A ON CNTRT.CONTRACT_ID = A.CONTRACT_ID ";
			} 
		
		String condition=" WHERE CNTRT.CONTRACT_ID IN ("+criteria.getContractIdsCSV()+")"; 
		query+=condition;
		
		String dataDef = ReportConstant.DataDef.RENEW_CONTRACT_REMINDER_LETTER;        
		reportQueryMap.put(dataDef, query+"");
	        
        return reportQueryMap;
	}
			

}
