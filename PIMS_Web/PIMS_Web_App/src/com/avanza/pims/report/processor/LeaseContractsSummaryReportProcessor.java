 package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RegionView;

public class LeaseContractsSummaryReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		LeaseContractsSummaryReportCriteria criteria = (LeaseContractsSummaryReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		String query = "SELECT DISTINCT " +
							" CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
							"CAST( to_char(CNTRT.CONTRACT_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractDate\" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"CNTRT.RENT_AMOUNT  \"contractAmount\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  \"contractEndDate\", " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							"U.UNIT_NUMBER \"unitNo\" , " +
							"U.UNIT_DESC \"unitDescription\" , " +
							"U.ACCOUNT_NUMBER \"unitCostCenter\" , " +
							"PRSN.FIRST_NAME \"tenantFirstName\" , " +
							"PRSN.MIDDLE_NAME \"tenantMiddleName\" , " +
							"PRSN.LAST_NAME \"tenantLastName\" , " +
							"PRSN.COMPANY_NAME \"companyName\" , " +
							"case when prsn.is_blacklisted =1 then 'نعم' else 'لا' end as \"field1\" ,"+
							"PRSN2.FIRST_NAME \"occupantFirstName\" , " +
							"PRSN2.LAST_NAME \"occupantLastName\" , " +
							"DD.DATA_DESC_EN  \"statusEn\" , " +
							"DD.DATA_DESC_AR  \"statusAr\", " +
							"coalesce(CI.HOME_PHONE,coalesce(cr.cell_number,coalesce(CRCI.HOME_PHONE,CRCI.OFFICE_PHONE))) \"phoneNumber\", "+
							"PRSN.CELL_NUMBER \"tenantCellNumber\", "+
							"PRSN.PERSON_ID \"personId\", "+
							"CNTRT.CONTRACT_ID \"contractId\", "+
							"CI.CONTACT_INFO_ID \"contactInfoId\", "+
							" COALESCE(CAST( U.UNIT_EW_UTILITY_NO as nvarchar2(30)),cast(U.DEWA_NUMBER as nvarchar2(30)))  \"field2\" , " +
							" PROP.LAND_NUMBER \"field3\" , " +
							" U.UNIT_AREA \"field4\", " +
							"RGN.DESCRIPTION_AR \"cityName\" " +
							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" LEFT JOIN (CONTACT_INFO PRCI LEFT JOIN REGION RGN ON PRCI.CITY_ID = RGN.REGION_ID) ON PRCI.CONTACT_INFO_ID=PROP.CONTACT_INFO_ID "+
							" INNER JOIN DOMAIN_DATA DD ON CNTRT.STATUS_ID = DD.DOMAIN_DATA_ID " +
							" left outer JOIN CONTRACT_UNIT_OCCUPIER CUO ON CUO.CONTRACT_UNIT_ID = CU.CONTRACT_UNIT_ID " +
							" left outer JOIN PERSON PRSN2 ON CUO.PERSON_ID = PRSN2.PERSON_ID "+ 
							" LEFT JOIN PERSON_CONTACT_INFO PCI ON PRSN.PERSON_ID = PCI.PERSON_ID "+
							" LEFT JOIN CONTACT_INFO CI ON CI.CONTACT_INFO_ID=PCI.CONTACT_INFO_ID "+
							" left join contact_reference cr on cr.tenant_id= prsn.person_id "+
							" left join contact_info crci on cr.contact_info_id= crci.contact_info_id ";
		
				          
				String condition="";
				
							if(criteria.getContractNumber().length() > 0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber().toLowerCase().trim() +"%')";
							if(criteria.getTenantName().length() >0)
								condition += " AND "+
						                     " lower(" +
											 "       (case when PRSN.First_NAME  is not null and length(PRSN.first_name)>0      then ' ' || PRSN.First_NAME   else '' end) || "+  
											 "       (case when PRSN.MIDDLE_NAME  is not null and length(PRSN.middle_name)>0    then ' ' || PRSN.MIDDLE_NAME else '' end)  || "+
											 "       (case when PRSN.last_NAME  is not null and length(PRSN.last_name)>0        then ' ' || PRSN.last_NAME else '' end)    || "+
											 "       (case when PRSN.Company_NAME  is not null and length(PRSN.Company_name)>0  then ' ' || PRSN.Company_NAME   else '' end) "+
											 
											 "      )" +
										    "         LIKE lower('%"+ criteria.getTenantName().toLowerCase().trim() +"%')";
							
								/*condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName().trim() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +			
												" OR lower(PRSN.COMPANY_NAME) like lower( '%" + criteria.getTenantName().trim() +"%')" +
								" ) " ;*/
							if(criteria.getIsBlackList() != null && criteria.getIsBlackList().equals("0"))
							{
							 condition += " and PRSN.is_blacklisted =0";
							}
							else if(criteria.getIsBlackList() != null && criteria.getIsBlackList().equals("1"))
							{
								 condition += " and PRSN.is_blacklisted =1";
								}
							if(criteria.getPropertyComercialName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyComercialName().trim()  +"%')";
							if(criteria.getUnitNum().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNum().trim() +"%')";
							if(criteria.getFromContractDateRich()!=null )								
								condition += " AND CNTRT.START_DATE >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss') ";
							if(criteria.getToContractDateRich()!=null )								
								condition += " AND CNTRT.START_DATE <= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getToContractDateRich() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss') ";
							if(criteria.getFromContractExpDateRich()!=null  )
								condition += " AND CNTRT.END_DATE >= to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ) +" 00:00:00', 'dd/MM/yyyy hh24:MI:ss')  ";
							
							if( criteria.getToContractExpDateRich()!=null )
								condition += " AND CNTRT.END_DATE <= to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() ) +" 23:59:59', 'dd/MM/yyyy hh24:MI:ss')";
							if(!criteria.getContractType().equals("-1"))
								condition+= " AND CNTRT.CONTRACT_TYPE_ID  =  '" + criteria.getContractType() +"'"	;
							if(!criteria.getContractStatus().equals("-1"))
								condition+= " AND CNTRT.STATUS_ID   =  " + criteria.getContractStatus();
					
							if( criteria.getUnitRentAmountFrom() != null &&  criteria.getUnitRentAmountFrom().length() > 0 )
								condition+=" AND U.RENT_VALUE >= "+criteria.getUnitRentAmountFrom().trim();
					
							if( criteria.getUnitRentAmountTo() != null &&  criteria.getUnitRentAmountTo().length() > 0 )
								condition+=" AND U.RENT_VALUE <= "+criteria.getUnitRentAmountTo().trim();
					
							if( criteria.getContractRentAmountFrom() != null &&  criteria.getContractRentAmountFrom().length() > 0 )
								condition+=" AND CNTRT.RENT_AMOUNT >= "+criteria.getContractRentAmountFrom().trim();
					
							if( criteria.getContractRentAmountTo() != null &&  criteria.getContractRentAmountTo().length() > 0 )
								condition+=" AND CNTRT.RENT_AMOUNT <= "+criteria.getContractRentAmountTo().trim();
					
							if( criteria.getCostCenter() != null &&  criteria.getCostCenter().length() > 0 )
								condition+=" AND U.ACCOUNT_NUMBER = '"+criteria.getCostCenter().trim() + "'";
							
							if( criteria.getPropertyNumber() != null &&  criteria.getPropertyNumber().length() > 0 )
								condition+=" AND PROP.PROPERTY_NUMBER = '"+criteria.getPropertyNumber().trim()+"'";
							
							if( criteria.getCity() != null &&  !criteria.getCity().equals("-1") )
								condition+=" AND PRCI.CITY_ID = "+criteria.getCity().trim();
							
							if(condition.length() > 0)
					        	query+="where 1=1 and (cntrt.contract_category = 82002)  AND (cntrt.is_deleted = 0) AND (cntrt.record_status = 1) \n"+ condition;

				String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER DESC, \"phoneNumber\" DESC";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.LEASE_CONTRACTS_SUMMARY;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractDate") + "' \"lblContractDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.occupantName") + "' \"lblOccupantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblStatusEn\",\n" +
		        
		        
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.startdate") + "' \"lblContractStartDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.enddate") + "' \"lblContractEndDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.Amount") + "' \"lblContractAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitNumber") + "' \"lblUnitNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitDescription") + "' \"lblUnitDescriptionEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractDate") + "' \"lblContractDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.occupantName") + "' \"lblOccupantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblStatusAr\",\n" + 

		        
		        
		    	"  '" + ReportMessageResource.getPropertyAr("settlement.label.startdate") + "' \"lblContractStartDateAr\",\n" +
		    	"  '" + ReportMessageResource.getPropertyAr("settlement.label.enddate") + "' \"lblContractEndDateAr\",\n" +
		    	"  '" + ReportMessageResource.getPropertyAr("contract.Amount") + "' \"lblContractAmountAr\",\n" +
		    	"  '" + ReportMessageResource.getPropertyAr("unit.unitNumber") + "' \"lblUnitNumberAr\",\n" +
		    	"  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescriptionAr\",\n" +
		    	
		        
		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.leaseContractsSummary.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.leaseContractsSummary.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.LEASE_CONTRACTS_SUMMARY_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			
	 public Map<String, String> getReportParametersMap(IReportCriteria reportCriteria){
		 
		 PropertyService propertyService = new PropertyService();
		 LeaseContractsSummaryReportCriteria criteria = (LeaseContractsSummaryReportCriteria) reportCriteria;
		 HashMap<String, String> parameters = new  HashMap<String, String>();
		 
		 
			if(criteria.getContractNumber().length() > 0)
				parameters.put("param1", "" + criteria.getContractNumber().trim());
			else
				parameters.put("param1", "--");
			
			if(criteria.getTenantName().length() >0)
				parameters.put("param10", "" + criteria.getTenantName().trim());
			else
				parameters.put("param10", "--");
			
			if(criteria.getPropertyComercialName().length() >0)
				parameters.put("param2", "" + criteria.getPropertyComercialName().trim());
			else
				parameters.put("param2", "--");
			
			if(criteria.getUnitNum().length() >0)
				parameters.put("param11", "" + criteria.getUnitNum().trim());
			else
				parameters.put("param11", "--");
			
			if(criteria.getFromContractDateRich()!=null )	
				parameters.put("param5", "" + DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ));
			else
				parameters.put("param5", "--");
			
			if(criteria.getToContractDateRich()!=null )	
				parameters.put("param14", "" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() ));
			else
				parameters.put("param14", "--");
			
			if(criteria.getFromContractExpDateRich()!=null  )
				parameters.put("param6", "" + DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ));
			else
				parameters.put("param6", "--");
			
			if( criteria.getToContractExpDateRich()!=null )
				parameters.put("param15", "" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() ));
			else
				parameters.put("param15", "--");
			
			if(!criteria.getContractType().equals("-1")){
				try {
					DomainDataView ddv = propertyService.getDomainDataByDomainDataId(new Long(criteria.getContractType()));
					parameters.put("param7", ddv.getDataDescAr());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param7", "error");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param7", "error");
				}
			}
			else
				parameters.put("param7", "--");
			
			if(!criteria.getContractStatus().equals("-1"))
				try {
					DomainDataView ddv = propertyService.getDomainDataByDomainDataId(new Long(criteria.getContractStatus()));
					parameters.put("param16", ddv.getDataDescAr());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param16", "error");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param16", "error");
				}
			else
				parameters.put("param16", "--");
			
			if( criteria.getUnitRentAmountFrom() != null &&  criteria.getUnitRentAmountFrom().length() > 0 )
				parameters.put("param3", "" + criteria.getUnitRentAmountFrom().trim());
			else
				parameters.put("param3", "--");
			
			if( criteria.getUnitRentAmountTo() != null &&  criteria.getUnitRentAmountTo().length() > 0 )
				parameters.put("param12", "" + criteria.getUnitRentAmountTo().trim());
			else
				parameters.put("param12", "--");
			
			if( criteria.getContractRentAmountFrom() != null &&  criteria.getContractRentAmountFrom().length() > 0 )
				parameters.put("param4", "" + criteria.getContractRentAmountFrom().trim());
			else
				parameters.put("param4", "--");
			
			if( criteria.getContractRentAmountTo() != null &&  criteria.getContractRentAmountTo().length() > 0 )
				parameters.put("param13", "" + criteria.getContractRentAmountTo().trim());
			else
				parameters.put("param13", "--");
			
			if( criteria.getCostCenter() != null &&  criteria.getCostCenter().length() > 0 )
				parameters.put("param8", "" + criteria.getCostCenter().trim());
			else
				parameters.put("param8", "--");
			
			if( criteria.getPropertyNumber() != null &&  criteria.getPropertyNumber().length() > 0 )
				parameters.put("param17", "" + criteria.getPropertyNumber().trim());
			else
				parameters.put("param17", "--");
			
			if( criteria.getCity() != null &&  !criteria.getCity().equals("-1") ){
				try {
					RegionView rv = propertyService.getRegionViewByRegionId(new Long(criteria.getCity()));
					parameters.put("param9", "" + rv.getRegionName());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param9", "(error)");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param9", "(error)");
				}
			}
			else
				parameters.put("param9", "--");
			
			parameters.put("param18", "unused");

		 return parameters;
	 }
}
