package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ChequeDetailsListReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;

public class ChequeDetailsListReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
	    ChequeDetailsListReportCriteria criteria = (ChequeDetailsListReportCriteria) reportCriteria;
        
        String query =  "SELECT CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) \"contractId\",\n" +
        				"coalesce(CNTRT.CONTRACT_NUMBER,coalesce(fc.file_number,fr.file_number)) \"contractNumber\",\n"+
        				"NVL( CNTRT.REMARKS, '---') ,\n"+
        				"CAST( to_char(CNTRT.START_DATE,'dd/mm/yyyy')AS VARCHAR(10) ) \"contractStartDate\",\n"+
        				"CAST( to_char(CNTRT.END_DATE ,'dd/mm/yyyy')AS VARCHAR(10) ) \"contractEndDate\" ,\n"+
           				"PROP.COMMERCIAL_NAME \"propertyName\",\n"+
                        "U.UNIT_NUMBER \"unitNumber\",\n"+
                        "case when prsn.person_id is not null then " +
                        " cast( (PRSN.FIRST_NAME||' ' || PRSN.MIDDLE_NAME ||' ' || PRSN.LAST_NAME ) as varchar2(512) ) else cast(coalesce( pfc.passport_name,pfr.passport_name) as varchar2(512) ) "+
                        " end \"tenantName\","+
                        "PRSN.COMPANY_NAME,"+
                        " DD_TT.DATA_DESC_EN \"tenantTypeEn\",\n"+
				        " DD_TT.DATA_DESC_AR \"tenantTypeAr\",\n"+
				        " PRSN.PERSON_TYPE, \n"+
				        " DD_CT.DATA_DESC_EN  \"chequeTypeEn\" , " +
				        " DD_CT.DATA_DESC_AR  \"chequeTypeAr\" , " +  
                      
                       "PRCT.RECEIPT_NUMBER \"receiptNumber\",\n"+
                       "CAST(to_char(PRCT.CREATED_ON,'dd/mm/yyyy')As VARCHAR(20)) \"receiptDate\",\n"+
                        
                        //"PRCD.METHOD_REF_NO \"chequeNumber\",\n"+
                        "CASE WHEN prcd.method_ref_no IS NOT NULL AND oprcd.method_ref_no IS NOT NULL THEN NVL (prcd.method_ref_no || ' --> ' || oprcd.method_ref_no,'---') ELSE NVL (prcd.method_ref_no, '---') END \"chequeNumber\",\n"+
                        
                        
                        "CAST(to_char(PRCD.METHOD_REF_DATE,'dd/mm/yyyy')As VARCHAR(20)) \"chequeDueDate\",\n"+
                        "PRCD.CHEQUE_OWNER \"chequeOwnerName\",\n"+
                    	" PRSN.PERSON_ID \"tenantId\",\n"+
                    	  " DD_TT.DATA_DESC_EN \"tenantTypeEn\",\n"+
					        " DD_TT.DATA_DESC_AR \"tenantTypeAr\",\n"+
					        " PRSN.PERSON_TYPE ,\n"+
                        
                       // -- Cheque Status
                        "DD_CS.DATA_DESC_EN \"chequeStatusEn\",\n"+
                        "DD_CS.DATA_DESC_AR \"chequeStatusAr\",\n"+
                        
                       //CHQUE TYPE LEFT HERE
                        "CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_EN end \"chequeBankNameEn\",\n"+
                        "CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_AR end \"chequeBankNameAr\",\n"+
                        "PRCD.AMOUNT \"chequeAmount\",\n"+
                        "PS.PAYMENT_NUMBER,\n"+
                        "PS.PAYMENT_DUE_ON ,\n"+
                        "PS.PAYMENT_TYPE_ID,\n"+
                        "PYT.DESCRIPTION_AR as \"paymentType\"  ,\n"+
                        "PRCD.METHOD_REF_NO ,\n"+ 
                        "PMT.DESCRIPTION ,\n"+
                        "BANK.BANK_EN ,\n"+ 
                        "PS.AMOUNT \n"+
                                               
                        "FROM PAYMENT_SCHEDULE PS \n"+
                        " INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID\n"+
                        " LEFT JOIN CONTRACT CNTRT ON PS.CONTRACT_ID = CNTRT.CONTRACT_ID \n"+ 
                        " LEFT JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID\n"+
                        " LEFT JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID\n"+
				        " LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID\n"+ 
				        " LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID\n"+
				        " LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID\n"+ 
				        " LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID\n"+
				        " LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID\n"+
				        " left  join payment_details pd on ps.payment_schedule_id = pd.payment_schedule_id "+
				        " left  join Collection_trx ct on pd.payment_details_id = ct.payment_details_id "+
				        " left  join request r on ct.request_id= r.request_id "+
				        " left  join inheritance_file fr on r.inheritance_file_id = fr.inheritance_file_id "+
				        " left  join person pfr on fr.file_owner_id= pfr.person_id "+
				        " left  join inheritance_file fc on ct.file_id = fc.inheritance_file_id "+
				        " left  join person pfc on fc.file_owner_id= pfc.person_id "+
                        " LEFT JOIN DOMAIN_DATA DD_CS ON PS.STATUS_ID = DD_CS.DOMAIN_DATA_ID\n"+
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID\n"+ 
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID\n"+
                        
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS oprc ON oprc.payment_schedule_id = ps.old_payment_schedule_id\n"+
                        " LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL oprcd ON oprcd.payment_receipt_detail_id = oprc.payment_receipt_detail_id\n"+
                        
                        " LEFT JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID\n"+
                        " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID\n"+
                        " LEFT JOIN payment_receipt PRCT ON PRCD.PAYMENT_RECEIPT_ID = PRCT.PAYMENT_RECEIPT_ID " +
                        " LEFT JOIN DOMAIN_DATA DD_CT ON PRCD.CHEQUE_TYPE = DD_CT.DOMAIN_DATA_ID " +
                        " LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;
        
      String condition="";
        if(criteria.getContractNumber().length()>0)
        	condition="AND CNTRT.CONTRACT_NUMBER LIKE '%"+criteria.getContractNumber()+"%'";
        if(criteria.getTenantName() .length() >0)
			condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
							" ) " ;
        if(criteria.getPropertyName().length() >0)
			condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
        if(criteria.getUnitNumber().length() >0)
			condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNumber() +"%')";

        
       
        if(!criteria.getBankName().equals("-1"))
			condition+= " AND BANK.BANK_ID  =  " + criteria.getBankName();
        if(!criteria.getPaymentStatus().equals("-1"))
        	condition+="AND PS.STATUS_ID=" +criteria.getPaymentStatus();
        	
        	
        
        ///still remaining
        // if(!(criteria.getTenantType().equals(-1)))
        // 	 condition+="AND "   
        if(criteria.getChequeAmount().length()>0)
        	condition+="AND PRCD.AMOUNT  ="+criteria.getChequeAmount();
        if(criteria.getChequeNumber().length()>0)
        	condition+="AND PRCD.METHOD_REF_NO LIKE '%"+criteria.getChequeNumber()+"%'";
        if(criteria.getChequeOwnerName().length()>0)
        	condition+="AND  lower(PRCD.CHEQUE_OWNER) LIKE lower('%"+criteria.getChequeOwnerName()+"%')";
        if(criteria.getContractStartDateFrom()!=null  && criteria.getContractStartDateTo()!=null)								
			condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractStartDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getContractStartDateTo())+"', 'dd/MM/yyyy')";
		if(criteria.getExpiryDateFrom()!=null  && criteria.getExpiryDateTo()!=null)
			condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getExpiryDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getExpiryDateTo())+"', 'dd/MM/yyyy')";
		if(!criteria.getPropertyOwnershipType().equals("-1"))
        	condition+="AND PROP.OWNERSHIP_TYPE_ID= '" +criteria.getPropertyOwnershipType()+"'";
		if(!criteria.getPaymentType().equals("-1"))
        	condition+="AND PS.PAYMENT_TYPE_ID="  +criteria.getPaymentType();
		if(criteria.getPaymentNumber().length()>0)
			condition+="AND lower(PS.PAYMENT_NUMBER) like lower('%"+criteria.getPaymentNumber()+"%')" ;
		if(criteria.getReceiptNumber().length()>0)
			condition+="AND lower(PRCT.RECEIPT_NUMBER) like lower('%"+criteria.getReceiptNumber()+"%')";
		if(criteria.getReceiptDate()!=null )
		{
			
		 condition+="AND PRCT.TRANSACTION_DATE >= " +
		 "to_date('"+DateUtil.convertToDisplayDate(criteria.getReceiptDate())+",00:00:00','dd/MM/yyyy  hh24:MI:ss')"+
		 "AND PRCT.TRANSACTION_DATE <= " +
		 "to_date('"+DateUtil.convertToDisplayDate(criteria.getReceiptDate())+",23:59:59','dd/MM/yyyy  hh24:MI:ss')";
			
		}
		if(criteria.getChequeNumber().length()>0)
			condition+="AND PRCD.METHOD_REF_NO like '%"+criteria.getChequeNumber()+"%'";
		if(!criteria.getChequeType().equals("-1"))
			condition+= " AND PRCD.CHEQUE_TYPE  =  " + criteria.getChequeType();
		
		if(criteria.getDueDate()!=null )
			condition+="AND PRCD.METHOD_REF_DATE >= to_date('"+DateUtil.convertToDisplayDate(criteria.getDueDate())+"','dd/MM/yyyy')";
		
		if(criteria.getDueDateTo()!=null )
			condition+="AND PRCD.METHOD_REF_DATE <= to_date('"+DateUtil.convertToDisplayDate(criteria.getDueDateTo())+"','dd/MM/yyyy')";
		
		if(criteria.getTenantId().length() >0)
			condition+=" AND PRSN.PERSON_ID = " + criteria.getTenantId() ;
		if(!criteria.getTenantType().equals("-1") )
		{
			List<DomainDataView> ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
		    DomainDataView ddvCompany= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_COMPANY);
		    if( criteria.getTenantType().equals( ddvCompany.getDomainDataId().toString() ) )
   			  condition+=" AND PRSN.IS_COMPANY= 1" ;
		    else 
			  condition+=" AND PRSN.IS_COMPANY= 0" ;
		
		    
		}
//		pmt.payment_method_id=1   => because we only wants the cheque details
		if(condition.length()>0)
				     query+="WHERE 1=1 and pmt.payment_method_id=1 and ps.is_deleted=0 \n"+condition+" ORDER BY CNTRT.CONTRACT_ID , PS.PAYMENT_DUE_ON";
		else 
					query+="WHERE 1=1 and pmt.payment_method_id=1  and ps.is_deleted=0   ORDER BY CNTRT.CONTRACT_ID , PS.PAYMENT_DUE_ON";
        	      	     	
				        
				           
        String dataDef = ReportConstant.DataDef.CHEQUE_DETAILS_LIST;        
        reportQueryMap.put(dataDef, query+"");
        
        ///////////////LABELS////////////////////////////////
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
		        "  '" + ReportMessageResource.getPropertyEn("tenants.id") + "' \"lblTenantIdEn\",\n" + 
				"  '" + ReportMessageResource.getPropertyEn("contract.tenanttype") + "' \"lblTenantTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.startDate") + "' \"lblContractStartDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptNumber") + "' \"lblReceiptNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.chequeNumberCol") + "' \"lblChequeNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymentdate") + "' \"lblChequeDueDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.bankNameCol") + "' \"lblChequeBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeAmountCol") + "' \"lblChequeAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.status") + "' \"lblStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblTotalAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cheuqe.owner") + "' \"lblChequeOwnerNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeType") + "' \"lblChequeTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.chequeDetailsReport.rpt.r") + "' \"lblChequeDetailsReportEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("tenants.id") + "' \"lblTenantIdAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("contract.tenanttype") + "' \"lblTenantTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.startDate") + "' \"lblContractStartDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptNumber") + "' \"lblReceiptNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.chequeNumberCol") + "' \"lblChequeNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymentdate") + "' \"lblChequeDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblTotalAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.bankNameCol") + "' \"lblChequeBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeAmountCol") + "' \"lblChequeAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.status") + "' \"lblStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cheuqe.owner") + "' \"lblChequeOwnerNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.chequeDetailsReport.rpt.r") + "' \"lblChequeDetailsReportAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeType") + "' \"lblChequeTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.CHEQUE_DETAILS_LIST_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;

      
	}
	
	

}
