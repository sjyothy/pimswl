package com.avanza.pims.report.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.avanza.core.util.Logger;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.StatementOfAccountNewView;
import com.avanza.pims.entity.UnusedUnblocked;
import com.avanza.pims.entity.ViewStatementOfAccountPersonDetails;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.mems.endowment.RetainedProfitService;
import com.avanza.pims.ws.vo.StatementOfAccountCriteriaView;
import com.avanza.pims.ws.vo.mems.MinorBalanceSummaryView;

public class StatementOfAccountNewProcessor extends AbstractReportProcessor 
{
	static Logger logger = Logger.getLogger(StatementOfAccountNewProcessor.class);
	static DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	
	@SuppressWarnings("unchecked")
	public static MinorBalanceSummaryView getStatementOfAccountForMobile(StatementOfAccountCriteria criteria )throws Exception
	{
		MinorBalanceSummaryView ret = executePersonDetailsBalanceSummaryQuery(criteria,null,false);
		
		List<StatementOfAccountNewView>  trxDetails = PersonalAccountTrxService.
															getStatementOfAccountTransList(
																					CommonUtil.transformToStatementOfAccountCriteriaView(criteria) 
																					
																					 	  );
		ret.setTransactionDetails(trxDetails );
		return ret;
	}

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{
			StringBuilder ccCsv = new StringBuilder("");
			for (String cc: criteria.getCostCenters()) 
			{
				ccCsv.append("'").append( cc ).append( "'," );
			}
			String cc=ccCsv.toString();
			if(cc.length() > 0 && cc.contains(","))
			{
				cc= cc.substring(0, (cc.length()-1) );
			}
			executePersonDetailsBalanceSummaryQuery(criteria,reportQueryMap,true );
			executePersonalAccountTransQuery(criteria,reportQueryMap,cc );
			executeChequeDetalsQuery(criteria,reportQueryMap,cc );
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
			
		}
		return reportQueryMap;
	}

	@SuppressWarnings( "unchecked" )
	public static MinorBalanceSummaryView executePersonDetailsBalanceSummaryQuery(
																					StatementOfAccountCriteria criteria, 
																					Map<String, String> reportQueryMap,
																					boolean isStmtOfAccountRprt 
																				  )   throws Exception 
	{
		
		MinorBalanceSummaryView minorBalanceSummaryView = new MinorBalanceSummaryView();
        
		PersonalAccountTrxService.getPersonalDetails(
														CommonUtil.transformToStatementOfAccountCriteriaView(criteria), 
														minorBalanceSummaryView 
													);
		
		if( !isStmtOfAccountRprt )
		{
			PersonalAccountTrxService.getBalanceDetailsWithinDateWindow(
																		 CommonUtil.transformToStatementOfAccountCriteriaView(criteria),
																		 minorBalanceSummaryView 
																		);
		}
		else
		{
	        for ( String criteriaCostCenter : criteria.getCostCenters() )
			{
				minorBalanceSummaryView.setTotalBalance(    
														  minorBalanceSummaryView.getTotalBalance() + 
														  PersonalAccountTrxService.getBalanceForCostCenter(criteriaCostCenter)
															
													   );
			}
	        
	        //Get Blocking Balance
			minorBalanceSummaryView.setBlockingBalance(
														BlockingAmountService.getBlockingAmount(	
																								criteria.getPersonId(), 
																								criteria.getInheritanceBeneficiaryId(), 
																								criteria.getInheritancefileId(),
																								criteria.getFromDate(),
																								criteria.getToDate()
																							   )
													  );
			//Get Unused unblocked 
			//TODO :Date Criteria to add, As of 7-JUN-2015  its not being used in balance calculation therefore not searching by date criteria		
			UnusedUnblocked unusedUnblocked = BlockingAmountService.getUnusedUnblocked(criteria.getPersonId(),null,criteria.getInheritancefileId());
			if(unusedUnblocked != null)
			{
				minorBalanceSummaryView.setUnUsedUnBlockAmount( unusedUnblocked.getAmount() );
			}
			
			//Subtract Blocking Amount from total balance
			if( minorBalanceSummaryView.getBlockingBalance().compareTo( minorBalanceSummaryView.getTotalBalance()  ) <= 0 )
			{
				minorBalanceSummaryView.setAvailableBalance( minorBalanceSummaryView.getTotalBalance() - minorBalanceSummaryView.getBlockingBalance() );
			}
	        //Get Activated Retained Profits 
			//TODO :Date Criteria to add, As of 7-JUN-2015  its not being used in balance calculation therefore not searching by date criteria
			minorBalanceSummaryView.setActivatedRetainedAmount( 
																RetainedProfitService.getSumOfActiveRetainedProfitAmount(	
																										    				criteria.getPersonId(), 
																										    				criteria.getInheritanceBeneficiaryId(), 
																										    				criteria.getInheritancefileId()
																								  						)
															  );
	
		
			if( minorBalanceSummaryView.getAvailableBalance().compareTo( 0d ) <  0 )
			{
				minorBalanceSummaryView.setAvailableBalance ( 0.00d );
			}
			fillDataDef(
						criteria, 
						reportQueryMap, 
						minorBalanceSummaryView
					   );
		}
		return minorBalanceSummaryView;
	}
	
		
	
	/**
	 * @param criteria
	 * @param reportQueryMap
	 * @param minorBalanceSummaryView

	 */
	@SuppressWarnings("unchecked")
	public static void fillDataDef(
										StatementOfAccountCriteria criteria,
										Map<String, String> reportQueryMap, 
										MinorBalanceSummaryView minorBalanceSummaryView
								   )throws Exception
   {
		String query = "";
		
		StringBuilder condition = new StringBuilder("");
		 
		query += " select '"+ 
				  minorBalanceSummaryView.getFileNumbers() + "' as \"fileNumber\", '" +
				  minorBalanceSummaryView.getCostCenter()+ "' as \"costCenter\",'"+
				  minorBalanceSummaryView.getOldCostCenter()+"' \"oldCostCenter\",'"+
				  criteria.getPersonId().toString()+"' as \"personId\",'"+
				  minorBalanceSummaryView.getName()+"' as \"name\", cast('" +
				  MinorBalanceSummaryView.oneDForm.format( minorBalanceSummaryView.getBlockingBalance()  )+"'  as DECIMAL(20,2) )  as \"blockingAmount\", cast('" +
				  MinorBalanceSummaryView.oneDForm.format( minorBalanceSummaryView.getTotalBalance() )+"'  as DECIMAL(20,2) )  as \"balance\", cast('" +
				  MinorBalanceSummaryView.oneDForm.format( minorBalanceSummaryView.getUnUsedUnBlockAmount() )+"'  as DECIMAL(20,2) )  as \"unUsedUnblock\", cast('" +
				  MinorBalanceSummaryView.oneDForm.format( minorBalanceSummaryView.getAvailableBalance() )+"' as DECIMAL(20,2) )  as \"finalBalance\", cast('" +
				  MinorBalanceSummaryView.oneDForm.format( minorBalanceSummaryView.getActivatedRetainedAmount() )+"' as DECIMAL(20,2) )  as \"retainedProfitBalance\" "+
				   " from DUAL " ;

		System.out.print("-------------" + query);
		query += condition.toString();
		String dataDef = "PersonDetailsDataDef";        
		reportQueryMap.put(dataDef, query+"");
	}

	private void executePersonalAccountTransQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		query += " SELECT PERSON_ID as \"personId\", COST_CENTER as \"costCenter\", \"ownerName\", \"trxDate\", \"credit\", \"debit\"," +
				" \"amount\", \"descriptionAr\", \"descriptionEn\", \"trxTypeEn\", \"trxTypeAr\", \"transId\" FROM STATEMENT_OF_ACCOUNT_NEW_VIEW  WHERE 1=1 ";
		if(criteria.getPersonId() != null)
		{
			condition.append(" and PERSON_ID = ").append(  criteria.getPersonId() );
		}
		if(ccCsv.length() > 0 )
		{
			condition.append(" and cost_center in (").append(  ccCsv).append(")");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "PersonalTransDetailsDataDef#PersonalTransDetails";        
		reportQueryMap.put(dataDef, query+"");
	
	}

	private void executeChequeDetalsQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		query += " select PERSON_ID as \"personId\", COST_CENTER as \"costCenter\", \"requestNumber\", \"requestType\", \"paymentScheduleId\", \"collectionTrxId\", " +
				 " \"paymentNumber\", \"status\", \"statusId\", \"paymentDueOn\", \"amount\", \"description\", \"methodRefNo\", \"bank\", \"realizedOn\", \"createOn\" ,\"field1\" ,\"field2\" ,\"field3\" " +
				 " from STMT_OF_ACCOUNT_CHQ_DET_VIEW WHERE 1=1" ;
		if(criteria.getPersonId() != null)
		{
			condition.append(" and PERSON_ID = ").append(  criteria.getPersonId() );
		}
		if(ccCsv.length() > 0 )
		{
			
			condition.append(" and cost_center in (").append(  ccCsv ).append(")");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "ChequeDetailsDataDef#ChequeDetails";        
		reportQueryMap.put(dataDef, query+"");
	
		
	}
	
}
