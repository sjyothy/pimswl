package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.MEMSPaymentReceiptReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class MEMSPaymentReceiptReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		MEMSPaymentReceiptReportCriteria criteria = (MEMSPaymentReceiptReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);

		String query = "SELECT * FROM MEMS_PAYMENT_RECEIPT";

		String condition = "";

		if (criteria.getRequestId() != null && criteria.getRequestId().length() > 0)
		{
			condition += " AND REQUEST_ID="		+ criteria.getRequestId();
		}
		if (condition.length() > 0)
			query += " WHERE 1=1 \n" + condition;

		//String sorting = " ORDER BY receiptDate";

		//query += sorting;

		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.MEMS_PAYMENT_RECEIPT_REPORT;
		reportQueryMap.put(dataDef, query + "");

		query = "SELECT \n" +

		/******************************* English Labels********************************/

		"  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.fileNumber")
				+ "' \"lblFileNumberEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.fileOwner")
				+ "' \"lblFileOwnerNameEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.generalAccountNumber")
				+ "' \"lblgeneralAccountNumberEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.receiptDate")
				+ "' \"lblReceiptDateEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.receiptNumber")
				+ "' \"lblReceiptNumberEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.amount")
				+ "' \"lblAmountEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.relatedTo")
				+ "' \"lblRelatedToEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.description")
				+ "' \"lblDescriptionEn\",\n"
				+

				/** * ***************************** Arabic Labels * ********************************	 */

				"  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.fileNumber")
				+ "' \"lblFileNumberAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.fileOwner")
				+ "' \"lblFileOwnerNameAr\",\n"
				+ "  '"
				+ ReportMessageResource
				.getPropertyAr("report.memspaymentreceiptreport.generalAccountNumber")
				+ "' \"lblgeneralAccountNumberAr\",\n"				
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.receiptDate")
				+ "' \"lblReceiptDateAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.receiptNumber")
				+ "' \"lblReceiptNumberAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.amount")
				+ "' \"lblAmountAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.relatedTo")
				+ "' \"lblRelatedToAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.description")
				+ "' \"lblDescriptionAr\",\n"
				+

				/******************************* Mandatory Info******************************** */

				"  '"
				+ ReportMessageResource
						.getPropertyEn("memsReportHeader")
				+ "' \"lblMEMSHeaderEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyEn("report.memspaymentreceiptreport.title")
				+ "' \"lblReportNameEn\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("memsReportHeader")
				+ "' \"lblMEMSHeaderAr\",\n"
				+ "  '"
				+ ReportMessageResource
						.getPropertyAr("report.memspaymentreceiptreport.title")
				+ "' \"lblReportNameAr\",\n"
				+ "  '"				
				+ criteria.getReportGeneratedBy() + "' \"lblLoggedInUser\"\n" +

				"FROM DUAL";
		dataDef = ReportConstant.DataDef.MEMS_PAYMENT_RECEIPT_REPORT_LABEL;
		reportQueryMap.put(dataDef, query);

		return reportQueryMap;
	}

}
