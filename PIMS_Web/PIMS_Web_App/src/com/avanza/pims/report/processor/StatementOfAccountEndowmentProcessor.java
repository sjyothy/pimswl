package com.avanza.pims.report.processor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.avanza.core.util.Logger;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class StatementOfAccountEndowmentProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{
			StringBuilder ccCsv = new StringBuilder("");
			for (String cc: criteria.getCostCenters()) 
			{
				ccCsv.append("'").append( cc ).append( "'," );
			}
			String cc=ccCsv.toString();
			if(cc.length() > 0 && cc.contains(","))
			{
				cc= cc.substring(0, (cc.length()-1) );
			}
			executeEndowmentDetailsQuery(criteria,reportQueryMap,cc );
			executeTransactionQuery(criteria,reportQueryMap,cc );
//			executeChequeDetalsQuery(criteria,reportQueryMap,cc );
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}
	@SuppressWarnings( "unchecked" )
	private void executeEndowmentDetailsQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception 
	{
		
		Endowment endowment = EntityManager.getBroker().findById(Endowment.class, criteria.getEndowmentId() );
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		Double openingBalance    = 0.00d;
		Double closingBalance    = 0.00d;
		Double finalBalance = 0.00d;
		Session session = (Session)EntityManager.getBroker().getSession();
		Query query = session.getNamedQuery("com.avanza.pims.entity.SPStmtAccountEndowment.spgetendowmentbalancesummary");
		query.setParameter("fromdate", criteria.getFormattedFromDate());
		query.setParameter("todate", criteria.getFormattedToDate() );
		query.setParameter("endowmentId", criteria.getEndowmentId());
		List  list = query.list();
		if( list != null && list.size() > 0 )
		{
			Object[] obj =  (Object[])list.get(0);
			if( obj[0] != null )
			{
				openingBalance = Double.valueOf( obj[0].toString() ) ;
			}
			if( obj[1] != null )
			{
				closingBalance = Double.valueOf( obj[1].toString() ) ;
			}
		}
		String queryStr  = " select '"+ 
								   endowment.getEndowmentNum() + "' as \"endowmentNumber\", '" +
								   endowment.getCostCenter()+ "' as \"costCenter\",'"+
								   criteria.getFormattedFromDateForReportDisplay() + "' as \"fromDate\",'"+
								   criteria.getFormattedToDateForReportDisplay() + "' as \"toDate\",'"+
								   criteria.getEndowmentId().toString()+"' as \"endowmentId\",'"+
								   endowment.getEndowmentName()+"' as \"name\", cast('" +
								   oneDForm.format( closingBalance )+"'  as DECIMAL(20,2) )  as \"balance\",cast('" +
								   oneDForm.format( openingBalance )+"'  as DECIMAL(20,2) )  as \"openingbalance\",cast('" +
								   oneDForm.format( finalBalance )+"' as DECIMAL(20,2) )  as \"finalBalance\" " +
						" from DUAL " ;

		System.out.print("-------------" + queryStr);
//		query += condition.toString();
		String dataDef = "EndowmentDetailsDataDef";        
		reportQueryMap.put(dataDef, queryStr+"");
	}

	private void executeTransactionQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		
		query += " SELECT "+ 
		" CAST(E.ENDOWMENT_ID AS number(20)) as \"endowmentId\", cast(E.COST_CENTER as varchar2(20))as \"costCenter\", " +
		 " null as \"trxDate\",  SUM(COALESCE(DISTRIBUTED_AMOUNT,0)) as \"credit\"," +
		 " 0 as \"debit\", SUM(COALESCE(DISTRIBUTED_AMOUNT,0)) \"amount\", cast('Deposit from property payment'  as nvarchar2(250)) as \"descriptionAr\", " +
		 " 'Deposit from property payment' as \"descriptionEn\", 'Distributed from contracts' as \"trxTypeEn\", " +
		 " cast('Distributed from contracts' as nvarchar2(500))  as \"trxTypeAr\", e.ENDOWMENT_ID  as \"transId\" "+
		" FROM CONTRACT_DIST_INFO_LOG CD INNER JOIN PROPERTY P ON CD.PROPERTY_ID=P.PROPERTY_ID INNER JOIN ENDOWMENT E ON P.ENDOWMENT_ID=E.ENDOWMENT_ID"+
		" WHERE 1=1" ;
		if(criteria.getEndowmentId() != null)
		{
			query +="  and E.ENDOWMENT_ID = "+criteria.getEndowmentId() ;
		}
		query +="  group by e.endowment_id,e.cost_Center union all ";
		query += " SELECT ENDOWMENT_ID as \"endowmentId\", COST_CENTER as \"costCenter\", " +
				 " \"trxDate\", \"credit\", \"debit\", \"amount\", \"descriptionAr\", \"descriptionEn\", \"trxTypeEn\", " +
				 " \"trxTypeAr\", \"transId\" FROM STATEMENT_OF_ACCOUNT_ENDOWMENT  WHERE 1=1 ";
		if(criteria.getEndowmentId() != null)
		{
			condition.append(" and ENDOWMENT_ID = ").append(  criteria.getEndowmentId() );
		}
		if(ccCsv.length() > 0 )
		{
			condition.append(" and cost_center in (").append(  ccCsv).append(")");
		}
		if( criteria.getFromDate() != null )
		{
			condition.append( " and trans_date >= to_date('"+criteria.getFormattedFromDate()+"' , 'dd/MM/yyyy hh24:MI:ss')");
		}
		if( criteria.getToDate() != null )
		{
			condition.append( " and trans_date <= to_date('"+ criteria.getFormattedToDate()+"' , 'dd/MM/yyyy hh24:MI:ss')");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "EndowmentTransDetailsDataDef#EndowmentTransDetails";        
		reportQueryMap.put(dataDef, query+"");
	
	}
	private void executeChequeDetalsQuery(StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap,String ccCsv )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
		query += " select ENDOWMENT_ID as \"endowmentId\",  COST_CENTER as \"costCenter\", \"requestNumber\", \"requestType\", \"paymentScheduleId\", " +
				 " \"paymentNumber\", \"status\", \"statusId\", \"paymentDueOn\", \"amount\", \"description\", \"methodRefNo\", \"bank\", " +
				 " \"realizedOn\", \"createOn\" ,\"field1\" ,\"field2\" ,null as \"field3\" " +
				 " from STMT_END_CHQ_DET_VIEW WHERE 1=1" ;
		if(criteria.getEndowmentId() != null)
		{
			condition.append(" and ENDOWMENT_ID = ").append(  criteria.getPersonId() );
		}
		if(ccCsv.length() > 0 )
		{
			condition.append(" and cost_center in (").append(  ccCsv ).append(")");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "ChequeDetailsDataDef#ChequeDetails";        
		reportQueryMap.put(dataDef, query+"");
	
		
	}
	
	}
