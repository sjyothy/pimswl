package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.VendorsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class VendorsReportProcessor extends AbstractReportProcessor{
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		VendorsReportCriteria criteria = (VendorsReportCriteria) reportCriteria;
 		
 		String query = "SELECT  DISTINCT" +
			" PSN.ACCOUNT_NUMBER\"venderGRPNumber\" , " +
		"DD.DATA_DESC_EN  \"vendorStatusEn\"," +
		"DD.DATA_DESC_AR  \"vendorStatusAr\"," +
		"PSN.COMPANY_NAME \"vendorName\" , " +
		"DD2.DATA_DESC_EN  \"vendorTypeEn\" , " +
		"DD2.DATA_DESC_AR  \"vendorTypeAr\", " +
		"RGN_AREA.DESCRIPTION_AR \"emirate\" , " +
		"AREA.DESCRIPTION_AR \"area\" ,"+
		"CI.HOME_PHONE \"telephoneNumber\", " +
		"CI.FAX \"faxNumber\", " +
		"CI.EMAIL \"email\", " +
		"PSN.FIRST_NAME || ' ' || PSN.MIDDLE_NAME || ' ' || PSN.LAST_NAME \"contactPerson\" , " +							
		"PSN.LICENSE_NUMBER \"licenseNumber\", " +
		"DD3.DATA_DESC_EN \"licenseSourceEn\" ," +
		"DD3.DATA_DESC_AR \"licenseSourceAr\" " +
		" FROM " +
		" PERSON PSN " +
		" INNER JOIN ASSIGNED_PERSON_TYPE APT ON (PSN.PERSON_ID = APT.PERSON_ID AND APT.PERSON_TYPE_ID=30011) " +
		" INNER JOIN DOMAIN_DATA DD ON PSN.STATUS_ID = DD.DOMAIN_DATA_ID " +
		" INNER JOIN DOMAIN_DATA DD2 ON  APT.PERSON_TYPE_ID = DD2.DOMAIN_DATA_ID " +
		" INNER JOIN DOMAIN_DATA DD3 ON  PSN.LICENSE_SOURCE = DD3.DOMAIN_DATA_ID " +
		" INNER JOIN PERSON_CONTACT_INFO PCI  ON PCI.PERSON_ID = PSN.PERSON_ID " +
		" INNER JOIN CONTACT_INFO CI ON CI.CONTACT_INFO_ID = PCI.CONTACT_INFO_ID "+
		" LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID "+
		" inner JOIN REGION AREA ON RGN_AREA.REGION_ID=AREA.PARENT_REGION_ID";
			 							

String condition="";


		if(criteria.getVenderGRPNumber().length() >0)
			condition+=" AND lower(PSN.ACCOUNT_NUMBER) like   lower('%" + criteria.getVenderGRPNumber() +"%')";
		if(!criteria.getVendorStatus().equals("-1"))
			condition+= " AND DD.DOMAIN_DATA_ID = " + criteria.getVendorStatus(); 
		if(criteria.getVendorName().length() >0)
			condition+=" AND lower(PSN.COMPANY_NAME) like   lower('%" + criteria.getVendorName() +"%')";
		if(!criteria.getVendorType().equals("-1"))
			condition+= " AND DD2.DOMAIN_DATA_ID = " + criteria.getVendorType(); 
		if(!criteria.getEmirate().equals("-1"))
			condition+= " AND RGN_AREA.REGION_ID=  '"+criteria.getEmirate()+"'";
		if(criteria.getTelephoneNumber().length() >0)
			condition+=" AND lower(CI.HOME_PHONE) like   lower('%" + criteria.getTelephoneNumber() +"%')";
		if(criteria.getFaxNumber().length() >0)
			condition+=" AND lower(CI.FAX) like   lower('%" + criteria.getFaxNumber() +"%')";
		if(criteria.getEmail().length() >0)
			condition+=" AND lower(CI.EMAIL) like   lower('%" + criteria.getEmail() +"%')";
		if(criteria.getLicenseNumber().length() >0)
			condition+=" AND lower(PSN.LICENSE_NUMBER) like   lower('%" + criteria.getLicenseNumber() +"%')";
		if(!criteria.getLicenseSource().equals("-1"))
			condition+=" AND lower(PSN.LICENSE_SOURCE)=" + criteria.getLicenseSource();

		if(condition.length() > 0)
        	query+=" where 1 = 1 \n"+ condition;


System.out.print("-------------" + query);
String dataDef = ReportConstant.DataDef.VENDORS_REPORT;        
reportQueryMap.put(dataDef, query+"");
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
        		"  '" + ReportMessageResource.getPropertyEn("grp.grpAccountNo") + "' \"lblVenderGRPNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.vendorStatus") + "' \"lblVendorStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.vendorName") + "' \"lblVendorNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("purchaseTenderDocument.vendorType") + "' \"lblVendorTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.state") + "' \"lblEmirateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.homephone") + "' \"lblTelephoneNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.fax") + "' \"lblFaxNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.email") + "' \"lblEmailEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contractor.name.gridHeader") + "' \"lblContactPersonEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("tenants.licenseNumber") + "' \"lblLicenseNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("tenants.licenseSource") + "' \"lblLicenseSourceEn\",\n" + 
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("grp.grpAccountNo") + "' \"lblVenderGRPNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.vendorStatus") + "' \"lblVendorStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.vendorName") + "' \"lblVendorNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("purchaseTenderDocument.vendorType") + "' \"lblVendorTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.state") + "' \"lblEmirateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.homephone") + "' \"lblTelephoneNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.fax") + "' \"lblFaxNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.email") + "' \"lblEmailAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contractor.name.gridHeader") + "' \"lblContactPersonAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("tenants.licenseNumber") + "' \"lblLicenseNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("tenants.licenseSource") + "' \"lblLicenseSourceAr\",\n" + 

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.vendorsReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.vendorsReport.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.VENDORS_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}

