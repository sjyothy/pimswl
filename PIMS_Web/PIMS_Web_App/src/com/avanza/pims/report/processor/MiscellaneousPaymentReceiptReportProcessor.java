package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.MEMSPaymentReceiptReportCriteria;
import com.avanza.pims.web.util.CommonUtil;

public class MiscellaneousPaymentReceiptReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		MEMSPaymentReceiptReportCriteria criteria = (MEMSPaymentReceiptReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = "SELECT MISCELLANEOUS_PAYMENT_RECEIPT.*,'"+criteria.getReportGeneratedBy()+"' as \"field4\" FROM MISCELLANEOUS_PAYMENT_RECEIPT";

		String condition = "";

		if (criteria.getRequestId() != null && criteria.getRequestId().length() > 0)
		{
			condition += " AND REQUEST_ID="		+ criteria.getRequestId();
		}
		if (criteria.getPaymentScheduleId() != null && criteria.getPaymentScheduleId().length() > 0)
		{
			condition += " AND PAYMENT_SCHEDULE_ID="+ criteria.getPaymentScheduleId();
		}
		if (condition.length() > 0)
			query += " WHERE 1=1 \n" + condition;


		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.MEMS_PAYMENT_RECEIPT_REPORT;
		reportQueryMap.put(dataDef, query + "");

		return reportQueryMap;
	}

}
