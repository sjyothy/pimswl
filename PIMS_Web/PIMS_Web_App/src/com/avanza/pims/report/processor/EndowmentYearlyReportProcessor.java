package com.avanza.pims.report.processor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.avanza.core.util.Logger;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.criteria.EndowmentYearlyReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class EndowmentYearlyReportProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		
		Session session = (Session)EntityManager.getBroker().getSession();
		Query query = session.getNamedQuery("com.avanza.pims.entity.Endowment.SP_ENDOWMENT_MASRAF_COUNT").setParameter("endowmentId", -1);

		List  list = query.list();
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		EndowmentYearlyReportCriteria criteria = (EndowmentYearlyReportCriteria) reportCriteria;
		try
		{
			
			executeTransactionQuery(criteria,reportQueryMap );
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}
	private void executeTransactionQuery(
											EndowmentYearlyReportCriteria criteria, 
											Map<String, String> reportQueryMap 
										)throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		String previousYear = String.valueOf( Integer.valueOf(criteria.getForYear()) - 1 );
		
		query += " select '"+ criteria.getReportGeneratedBy() +"' as \"field1\", '"+
					 criteria.getForYear() +"' as \"field2\", '"+
					 previousYear +"' as \"field3\", "+
					  " EF.FILE_ID \"endowmentFileId\","+
					  " EF.FILE_NUMBER \"endowmentFileNumber\","+
					  " EF.FILE_Name \"endowmentFileName\","+
					  
					  " P.PERSON_ID \"waaqifId\","+
					  
					  " CASE"+
					  " WHEN P.COMPANY_NAME IS NULL"+
					    " OR LENGTH (LTRIM(RTRIM(P.COMPANY_NAME)))<=0"+
					    " THEN"+
					    " CASE"+
					      " when P.MIDDLE_NAME is null AND P.LAST_NAME    IS NULL"+
					        " THEN P.FIRST_NAME"+
					        " ELSE"+
					        " case"+
					          " when P.MIDDLE_NAME is not null and P.LAST_NAME    is null"+ 
					            " THEN P.FIRST_NAME|| ' '|| P.MIDDLE_NAME"+
					            " ELSE"+
					            " case"+
					              " WHEN P.MIDDLE_NAME IS NULL AND P.LAST_NAME    IS NOT NULL"+
					                " THEN P.FIRST_NAME|| ' '|| P.LAST_NAME"+
					                " ELSE P.FIRST_NAME|| ' '|| P.MIDDLE_NAME||' '|| P.LAST_NAME"+
					                " END"+
					              " END"+
					          "     END"+
					  "   ELSE P.COMPANY_NAME"+
					  " END           AS \"waaqifName\","+
					  " E.ENDOWMENT_ID  AS \"endowmentId\","+
					  " E.ENDOWMENT_NAME \"endowmentName\","+

					  " AST.ASSET_TYPE_ID \"endowmentTypeId\","+
					  " AST.ASSET_TYPE_NAME_AR \"endowmentType\","+
					  " E.FIELD_DETAILS_1 \"endowmentLandNumber\","+
					  " COALESCE( cast(r.description_ar as varchar2(500) ),cast( E.FIELD_DETAILS_3  as varchar2(500) )) \"endowmentPropertyCommunity\","+
					  " E.FIELD_DETAILS_4 \"endowmentPropertyArea\","+
					  "COALESCE( cast(prop.commercial_name as varchar2(500) ),cast(prop.property_name  as varchar2(500) ))\"endowmentProperty\" ,"+
					  
					  
					  " TEMC.CSV_BENEFICIARIES \"endowmentBeneficiaries\","+
					  " CASE"+
					  " WHEN PRSN.PERSON_ID IS NULL then 'مؤسسة الأوقاف وشؤون القًصّر'" +
					  "ELSE "+
					  " case WHEN PRSN.COMPANY_NAME IS NULL"+
					    " OR LENGTH (LTRIM(RTRIM(PRSN.COMPANY_NAME)))<=0"+
					    " THEN"+
					    " CASE"+
					      " when PRSN.MIDDLE_NAME is null AND PRSN.LAST_NAME    IS NULL"+
					        " THEN PRSN.FIRST_NAME"+
					        " ELSE"+
					        " case"+
					          " when PRSN.MIDDLE_NAME is not null and PRSN.LAST_NAME    is null"+ 
					            " THEN PRSN.FIRST_NAME|| ' '|| PRSN.MIDDLE_NAME"+
					            " ELSE"+
					            " case"+
					              " WHEN PRSN.MIDDLE_NAME IS NULL AND PRSN.LAST_NAME    IS NOT NULL"+
					                " THEN PRSN.FIRST_NAME|| ' '|| PRSN.LAST_NAME"+
					                " ELSE PRSN.FIRST_NAME|| ' '|| PRSN.MIDDLE_NAME||' '|| PRSN.LAST_NAME"+
					                " END"+
					              " END"+
					          "     END"+
					  "   ELSE PRSN.COMPANY_NAME"+
					  " END " +
					  " END   AS \"endowmentManager\","+
					
					" 	coalesce(tdw.\"totalDeposits\",0) \"totalDeposits\"," +
					" 	coalesce(tdw.\"totalWithDrawals\",0) \"totalWithDrawals\", "+
					" 	coalesce(residentialUnits.\"residentialUnits\",0) \"totalResidentialUnits\" ,"+
					" 	coalesce(commercialUnits.\"commercialUnits\",0) \"totalCommercialUnits\" ,"+
					" 	coalesce(totalUnits.\"totalUnits\",0) \"totalUnits\" ,"+
					" 	((coalesce( rentedUnits.\"rented\",0)*100.00)/coalesce( totalUnits.\"totalUnits\",1)) \"percentageOfRentedUnits\" ,"+
					" masrafBenef.\"paidTo\" \"field4\""+
					" from ENDOWMENT_FILE EF"+
					" inner join PERSON P on EF.FILE_OWNER=P.PERSON_ID"+
					" inner join DOMAIN_DATA DDEFS on DDEFS.DOMAIN_DATA_ID=EF.STATUS_ID"+
					" LEFT JOIN END_FILE_ASSO EFA ON EF.FILE_id=EFA.FILE_ID"+
					" LEFT JOIN ENDOWMENT_PURPOSE EP ON EP.PURPOSE_ID=EFA.PURPOSE_ID"+
					" LEFT JOIN ENDOWMENT_CATEGORY EC ON EC.CATEGORY_ID=EFA.CATEGORY_ID"+
					" left join DOMAIN_DATA DDES on DDES.DOMAIN_DATA_ID=EFA.STATUS_ID"+
					" LEFT JOIN ENDOWMENT E ON E.ENDOWMENT_ID=EFA.ENDOWMENT_ID"+
					" left join DOMAIN_DATA DDEMC on DDEMC.DOMAIN_DATA_ID=E.main_category_id"+
					" LEFT JOIN PROPERTY prop ON E.ENDOWMENT_ID=prop.ENDOWMENT_ID"+
					" left join CONTACT_INFO CI  on CI.CONTACT_INFO_ID=prop.CONTACT_INFO_ID " + 
					" left join region r on r.region_id=ci.city_id"+
					" LEFT JOIN PERSON PRSN ON E.MANAGER_ID=PRSN.PERSON_ID"+
					" LEFT JOIN TEMP_ENDOWMENT_MASRAF_COUNT TEMC ON TEMC.ENDOWMENT_ID=E.ENDOWMENT_ID"+
					" left join ASSET_TYPE AST on AST.ASSET_TYPE_ID=E.ENDOWMENT_TYPE_ID "+
					
					" LEFT JOIN ("+
					"            select P.PROPERTY_ID,COUNT(U.UNIT_ID) \"residentialUnits\" from UNIT U " +
					"			 inner join FLOOR F on  F.FLOOR_ID=U.FLOOR_ID inner join PROPERTY P  on P.PROPERTY_ID= F.PROPERTY_ID "+
					"			 where U.STATUS_ID not in (2011)  and u.is_Deleted= 0 and P.ENDOWMENT_ID is not null and u.usage_type_id=6002"+
					"			 group by P.PROPERTY_ID"+
					" 			)" +
					" residentialUnits on residentialUnits.property_id=e.endowment_id " +
					" LEFT JOIN ("+
					"            select P.PROPERTY_ID,COUNT(U.UNIT_ID) \"commercialUnits\" from UNIT U " +
					"			 inner join FLOOR F on  F.FLOOR_ID=U.FLOOR_ID inner join PROPERTY P  on P.PROPERTY_ID= F.PROPERTY_ID "+
					"			 where U.STATUS_ID not in (2011)  and u.is_Deleted= 0 and P.ENDOWMENT_ID is not null and u.usage_type_id=6001"+
					"			 group by P.PROPERTY_ID"+
					" 			)" +
					" commercialUnits on commercialUnits.property_id=e.endowment_id " +
					" LEFT JOIN ("+
					"            select P.PROPERTY_ID,COUNT(U.UNIT_ID) \"totalUnits\" from UNIT U " +
					"			 inner join FLOOR F on  F.FLOOR_ID=U.FLOOR_ID inner join PROPERTY P  on P.PROPERTY_ID= F.PROPERTY_ID "+
					"			 where U.STATUS_ID not in (2011)  and u.is_Deleted= 0 and P.ENDOWMENT_ID is not null "+
					"			 group by P.PROPERTY_ID"+
					" 			)" +
					" totalUnits  on totalUnits.property_id=e.endowment_id " +
					" LEFT JOIN ("+
					"            select P.PROPERTY_ID,COUNT(U.UNIT_ID) \"rented\" from UNIT U " +
					"			 inner join FLOOR F on  F.FLOOR_ID=U.FLOOR_ID inner join PROPERTY P  on P.PROPERTY_ID= F.PROPERTY_ID "+
					"			 where U.STATUS_ID=2008  and u.is_Deleted= 0 and P.ENDOWMENT_ID is not null "+
					"			 group by P.PROPERTY_ID"+
					" 			)" +
					" rentedUnits  on rentedUnits.property_id=e.endowment_id " +
//					" LEFT JOIN ("+
//					"            select P.PROPERTY_ID,COUNT(U.UNIT_ID) \"vacant\" from UNIT U " +
//					"			 inner join FLOOR F on  F.FLOOR_ID=U.FLOOR_ID inner join PROPERTY P  on P.PROPERTY_ID= F.PROPERTY_ID "+
//					"			 where U.STATUS_ID=2005  and u.is_Deleted= 0 and P.ENDOWMENT_ID is not null "+
//					"			 group by P.PROPERTY_ID"+
//					" 			)" +
//					" vacantUnits  on vacantUnits.property_id=e.endowment_id " +
					" LEFT JOIN ("+
					"					select  "+
					"				distinct \"fromEndowmentId\",\"paidTo\" "+ 
					"				from STATEMENT_OF_ACCOUNT_MASRAF "+
					"				where \"paidTo\"  is not null and \"amount\"<0 "+
			        "					and  trans_date>=to_date('"+criteria.getFormatterWithOutTime().format( criteria.getFromDate() )+" 00:00:00','dd/MM/yyyy hh24:mi:ss') " +
			        "					and trans_date<=to_date('"+criteria.getFormatterWithOutTime().format( criteria.getToDate() )+" 23:59:59','dd/MM/yyyy hh24:mi:ss') " +
					" 			) " +
					"			masrafBenef on masrafBenef.\"fromEndowmentId\"=e.endowment_id"+

					" LEFT JOIN ("+
			        " 				select  " +
			        "						coalesce(SUM( \"credit\"),0) as \"totalDeposits\","+
			        "						coalesce(SUM( \"debit\"),0) as \"totalWithDrawals\", "+
			        "						endowment_id " +
			        "				from statement_of_account_endowment "+
			        " 				where   1=1" +
			        "					and  trans_date>=to_date('"+criteria.getFormatterWithOutTime().format( criteria.getFromDate() )+" 00:00:00','dd/MM/yyyy hh24:mi:ss') " +
			        "					and trans_date<=to_date('"+criteria.getFormatterWithOutTime().format( criteria.getToDate() )+" 23:59:59','dd/MM/yyyy hh24:mi:ss') " +
			        "				group by endowment_id  " +
			        " ) tdw on tdw.endowment_id=e.endowment_id"+
					" where    EF.IS_DELETED =0";

		if(criteria.getWaaqifName() != null && criteria.getWaaqifName().length() >0 )
		{
			condition.append(" AND (LOWER(P.FIRST_NAME ||' '||P.MIDDLE_NAME||' '||P.LAST_NAME) like '%").append( criteria.getWaaqifName()).
					  append("%' OR lower(p.COMPANY_NAME) like lower( '%") .append(  criteria.getWaaqifName() ).append("%') )");
		}
		if(criteria.getEndowmentName()  != null && criteria.getEndowmentName().length() > 0 )
		{
			condition.append(" and lower(E.ENDOWMENT_NAME) like  '%").append(  criteria.getEndowmentName().toLowerCase().trim() ).append("%'");
		}
		if(criteria.getEndowmentFileNumber() != null && criteria.getEndowmentFileNumber().length() > 0 )
		{
			condition.append(" and lower(EF.FILE_NUMBER) like '%").append(  criteria.getEndowmentFileNumber().toLowerCase() ).append("%'");
		}
		if(criteria.getEndowmentFileName() != null && criteria.getEndowmentFileName().length() > 0 )
		{
			condition.append(" and lower(EF.FILE_NAME) like '%").append(  criteria.getEndowmentFileName().toLowerCase() ).append("%'");
		}
		condition.append( " ORDER BY \"waaqifName\",EF.FILE_ID,e.endowment_id" );
		query += condition.toString();
		System.out.print("-------------" + query);
		reportQueryMap.put("EndowmentYearlyReportDataDef", query);
	
	}
	
	
	}
