package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ApplicationDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ApplicationDetailsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		ApplicationDetailsReportCriteria criteria = (ApplicationDetailsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////
		String query = " SELECT DISTINCT " +
				" COALESCE (cpu.unit_number,"+
                       "   apu.unit_number,"+
                       "  rdpu.unit_number,"+
                       "  scpu.unit_number,"+
                       "   ipu.unit_number"+
                       "  )\"unitNumber\","+
                " COALESCE (cpu.unit_acc_num,"+
                       "   apu.unit_acc_num,"+
                       "  rdpu.unit_acc_num,"+
                       "  scpu.unit_acc_num,"+
                       "   ipu.unit_acc_num"+
                       "  )\"field1\","+
                " COALESCE (cpu.unit_id,"+
                        "  apu.unit_id,"+
                         " rdpu.unit_id,"+
                         " scpu.unit_id,"+
                         " ipu.unit_id"+
                         ") \"UNIT_ID\","+
                " COALESCE (cpu.property_id,"+
                  "        apu.property_id,"+
                   "       rdpu.property_id,"+
                    "      scpu.property_id,"+
                     "     scppu.property_id,"+
                      "    ipu.property_id"+
                       "  )\"PROPERTY_ID\","+
                " COALESCE (cpu.commercial_name,"+
                  "        apu.commercial_name,"+
                   "       rdpu.commercial_name,"+
                    "      scpu.commercial_name,"+
                     "     scppu.commercial_name,"+
                      "    ipu.commercial_name "+
                       "  )\"propertyName\","+
                " COALESCE (cpu.property_name,"+
                  "        apu.property_name,"+
                   "       rdpu.property_name,"+
                    "      scpu.property_name,"+
                     "     scppu.property_name,"+
                     "     ipu.property_name "+
                      "   )\"endowedName\","+
                " COALESCE (cpu.unit_type_desc_en,"+
                  "        apu.unit_type_desc_en,"+
                   "       rdpu.unit_type_desc_en,"+
                    "      scpu.unit_type_desc_en,"+
                      "    ipu.unit_type_desc_en "+
                       "  )\"unitTypeEn\","+
                "COALESCE (cpu.unit_type_desc_ar,"+
                 "         apu.unit_type_desc_ar,"+
                  "        rdpu.unit_type_desc_ar,"+
                   "       scpu.unit_type_desc_ar,"+
                     "     ipu.unit_type_desc_ar"+
                      "   )\"unitTypeAr\","+
                " COALESCE(cpu.owner_ship_type_desc_en,apu.owner_ship_type_desc_en,rdpu.owner_ship_type_desc_en,"+
                "          scpu.owner_ship_type_desc_en,"+
"                     scppu.owner_ship_type_desc_en,"+
 "                    ipu.owner_ship_type_desc_en"+
  "                  )\"propertyOwnershipTypeEn\","+
"                COALESCE (cpu.owner_ship_type_desc_ar,"+
  "                   apu.owner_ship_type_desc_ar,"+
  "                   rdpu.owner_ship_type_desc_ar,"+
   "                  scpu.owner_ship_type_desc_ar,"+
    "                 scppu.owner_ship_type_desc_ar,"+
     "                ipu.owner_ship_type_desc_ar"+
     "               )\"propertyOwnershipTypeAr\","+
"                r.request_id, r.request_number\"applicationNumber\","+
 "               dd.data_desc_en\"applicationStatusEn\","+
                " dd.data_desc_ar\"applicationStatusAr\","+
                  " prsn_app.first_name||' '||prsn_app.middle_name||' '|| prsn_app.company_name\"applicationName\","+
                  " prsn.first_name||' '||prsn.middle_name||' '|| prsn.last_name|| prsn.company_name\"tenantName\","+
                " prsn.is_company\"tenantTypeEn\","+
                " prsn.is_company\"tenantTypeAr\","+
                " CAST(TO_CHAR (r.request_date, 'dd/mm/yyyy') AS VARCHAR (10)) \"applicationDateFrom\","+
                " rtp.request_type_en\"applicationTypeEn\","+
                " rtp.request_type_ar\"applicationTypeAr\","+
                //" r.description\"applicationDescription\","+
                " COALESCE (cpu.unit_desc,   apu.unit_desc,  rdpu.unit_desc,  scpu.unit_desc,   ipu.unit_desc  )\"applicationDescription\","+
                " COALESCE (cntrt.contract_number,a.auction_number)\"contractNumber\","+
                " dd_cs.data_desc_en\"contractStatusEn\","+
                " dd_cs.data_desc_ar\"contractStatusAr\", "+
    			" CASE WHEN R.REQUEST_TYPE_ID=8 THEN "+ 
				"   ( CAST "+
                "   (TO_CHAR (R.SETTLEMENT_DATE, 'dd/mm/yyyy') AS VARCHAR (10)) "+
                " ) ELSE "+
				" CASE WHEN R.REQUEST_TYPE_ID=11 THEN "+ 
				" ( CAST "+
                "   ( RFD.REQUEST_KEY_VALUE AS VARCHAR (10))"+
                " ) END "+
				" END AS \"settlementDate\", "+
				
				" NT.NOTE_ID\"field2\","+
				" NT.ENTITY_ID\"field3\","+
				" NOTE_TYPE_ID\"field4\","+
				" NT.CREATED_BY\"field5\","+
				" NT.CREATED_ON\"field6\","+
				" NT.UPDATED_ON\"field7\","+
				" SYSTEM_NOTE_EN\"field8\","+
				" SYSTEM_NOTE_AR\"field9\","+
				" (select su.full_name_sec from sec_user su where su.login_id = NT.created_by) \"field10\" \n " +

				

                " FROM request r LEFT JOIN request_type rtp ON r.request_type_id =rtp.request_type_id "+
                " LEFT JOIN person prsn_app ON r.applicant_id = prsn_app.person_id "+
                " LEFT JOIN domain_data dd ON r.status_id = dd.domain_data_id "+
                " LEFT JOIN contract cntrt ON cntrt.contract_id = r.contract_id "+
                " LEFT JOIN contract_unit cu ON cntrt.contract_id = cu.contract_id "+
                " LEFT JOIN domain_data dd_cs ON cntrt.status_id = dd_cs.domain_data_id"+
                " LEFT JOIN person prsn ON cntrt.tenant_id = prsn.person_id "+
                " LEFT JOIN request_detail rd ON r.request_id = rd.request_id"+
                " LEFT JOIN auction_unit au ON rd.auction_unit_id = au.auction_unit_id"+
                " LEFT JOIN auction a ON au.auction_id = a.auction_id "+
                " LEFT JOIN tender_properties stp ON cntrt.contract_id = stp.contract_id"+
                " LEFT JOIN inquiry i ON r.request_id = i.request_id "+
                " LEFT JOIN inquiry_units iu ON i.inquiry_id = iu.inquiry_id "+
                " LEFT JOIN property_unit_view ipu ON iu.unit_id = ipu.unit_id "+
                " LEFT JOIN property_unit_view apu ON apu.unit_id = au.unit_id "+
                " LEFT JOIN property_unit_view cpu ON cpu.unit_id = cu.unit_id "+
                " LEFT JOIN property_unit_view rdpu ON rd.unit_id = rdpu.unit_id "+
                " LEFT JOIN property_unit_view scpu ON stp.unit_id = scpu.unit_id"+
				" LEFT JOIN REQUEST_FIELD_DETAIL RFD ON ( R.REQUEST_ID =RFD.REQUEST_ID AND RFD.REQUEST_KEY_ID=20 ) "+
                " LEFT JOIN property_unit_view scppu ON stp.property_id = scppu.property_id "+
                "LEFT JOIN NOTE NT ON r.request_id = NT.ENTITY_ID AND NT.NOTE_TYPE_ID =21001";
		
		        
		///////////////////////////////
	                    
				String condition="";
				
							if(criteria.getApplicationNumber().length() >0)
								condition+=" AND lower(R.REQUEST_NUMBER) like lower('%" + criteria.getApplicationNumber() +"%')";
							if( criteria.getApplicationDateFrom() !=null  )								
								condition += " AND R.REQUEST_DATE >= to_date('"+DateUtil.convertToDisplayDate( criteria.getApplicationDateFrom())+" 00:00:00','dd/MM/yyyy  hh24:MI:ss' ) ";
						    if( criteria.getApplicationDateTo() !=null  )
						    	condition += " AND R.REQUEST_DATE <= to_date('"+DateUtil.convertToDisplayDate( criteria.getApplicationDateTo())+" 23:59:59','dd/MM/yyyy  hh24:MI:ss' )";
							
							if(!criteria.getApplicationType().equals("-1"))
								condition+= " AND R.REQUEST_TYPE_ID  =  '" + criteria.getApplicationType() +"'"	;
							
							if(!criteria.getUnitType().equals("-1")) {
								condition+=" AND (" +
										" CPU.UNIT_TYPE_ID  =  '" + criteria.getUnitType() +"'"	+
										" OR APU.UNIT_TYPE_ID  =  '" + criteria.getUnitType() +"'"	+
										" OR IPU.UNIT_TYPE_ID  =  '" + criteria.getUnitType() +"'"	+
										" OR RDPU.UNIT_TYPE_ID  =  '" + criteria.getUnitType() +"'"	+
										" OR SCPU.UNIT_TYPE_ID  =  '" + criteria.getUnitType() +"'"	+
										  " )";
							}
							
							
							if(!criteria.getApplicationStatus().equals("-1"))
								condition+= " AND R.STATUS_ID  =  '" + criteria.getApplicationStatus() +"'"	;
						

							if(criteria.getApplicantName() .length() >0)
								condition+= " AND (lower(PRSN_APP.FIRST_NAME) like lower( '%" + criteria.getApplicantName() +"%')" +
												" OR lower(PRSN_APP.MIDDLE_NAME) like  lower( '%" + criteria.getApplicantName() +"%')" +
												" OR lower(PRSN_APP.LAST_NAME) like lower( '%" + criteria.getApplicantName() +"%')" +			
												" ) " ;
							
							if(criteria.getContractNumber().length() >0)
								condition+=" AND ( lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%') " +
										"    OR lower(A.AUCTION_NUMBER) like lower('%" + criteria.getContractNumber() +"%') ) ";
							if(!criteria.getContractStatus().equals("-1"))
								condition+= " AND CNTRT.STATUS_ID  =  " + criteria.getContractStatus();

							
							if(criteria.getTenantName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
												" ) " ;
							if(!criteria.getTenantType().equals("-1"))
							{
								String tenantTypeCriteria;
								if(criteria.getTenantType().equals("26002"))
									tenantTypeCriteria="0";
								else 
									tenantTypeCriteria="1";
								condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
							}	

							
							
							if(criteria.getPropertyName().length() >0)
							{
								condition+=" AND (" +
										" lower(CPU.COMMERCIAL_NAME) like  lower('%" +  criteria.getPropertyName() +"%')"+
								        " OR lower(APU.COMMERCIAL_NAME) like  lower('%" +  criteria.getPropertyName() +"%')"+
								        " OR lower(IPU.COMMERCIAL_NAME) like  lower('%" +  criteria.getPropertyName() +"%')"+
								        " OR lower(RDPU.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')"+
								        " OR lower(SCPU.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')"+
								           " )";
								
							}
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND (" +
										"  lower(CPU.UNIT_NUMBER) like  lower('%" +  criteria.getUnitNumber() +"%')"+
								        " OR lower(APU.UNIT_NUMBER) like  lower('%" +  criteria.getUnitNumber() +"%')"+
								        " OR lower(IPU.UNIT_NUMBER) like  lower('%" +  criteria.getUnitNumber() +"%')"+
								        " OR lower(RDPU.UNIT_NUMBER) like  lower('%" + criteria.getUnitNumber() +"%')"+
								        " OR lower(SCPU.UNIT_NUMBER) like  lower('%" + criteria.getUnitNumber() +"%')"+
								           " )";
							if(!criteria.getPropertyOwnershipType().equals("-1"))
								condition+=" AND (" +
								"  lower(CPU.OWNER_SHIP_TYPE_ID)  =" +  criteria.getPropertyOwnershipType()+
						        " OR lower(APU.OWNER_SHIP_TYPE_ID)  =" +  criteria.getPropertyOwnershipType()+
						        " OR lower(IPU.OWNER_SHIP_TYPE_ID)  =" +  criteria.getPropertyOwnershipType()+
						        " OR lower(RDPU.OWNER_SHIP_TYPE_ID) =" + criteria.getPropertyOwnershipType()+
						        " OR lower(SCPU.OWNER_SHIP_TYPE_ID) =" + criteria.getPropertyOwnershipType()+
						           " )";
					
							if(condition.length() > 0)
					        	query+=" WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY R.REQUEST_NUMBER,NT.UPDATED_ON DESC";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.APPLICATION_DETAILS_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("inquiryApplication.applicationNumber") + "' \"lblApplicationNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblApplicationStatusEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.applicantName") + "' \"lblApplicationNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.date") + "' \"lblApplicationDateFromEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("request.requestType") + "' \"lblApplicationTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.dsescription") + "' \"lblApplicationDescriptionEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("grp.ownership") + "' \"lblPropertyOwnershipTypeEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("receiveProperty.unitNo") + "' \"lblUnitNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.unitType") + "' \"lblUnitTypeEn\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.tenanttype") + "' \"lblTenantTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblContractStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.applicationDetailSReport.rpt") + "' \"lblApplicationDetailsReportEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.company") + "' \"lblCompanyEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.individual") + "' \"lblIndividualEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\" ,\n" +
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("inquiryApplication.applicationNumber") + "' \"lblApplicationNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblApplicationStatusAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.applicantName") + "' \"lblApplicationNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.date") + "' \"lblApplicationDateFromAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("request.requestType") + "' \"lblApplicationTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.dsescription") + "' \"lblApplicationDescriptionAr\",\n" +
        		 "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.company") + "' \"lblCompanyAr\",\n" +
 		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.individual") + "' \"lblIndividualAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("grp.ownership") + "' \"lblPropertyOwnershipTypeAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.unitNo") + "' \"lblUnitNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.unitType") + "' \"lblUnitTypeAr\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.tenanttype") + "' \"lblTenantTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblContractStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.applicationDetailSReport.rpt") + "' \"lblApplicationDetailsReportAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" , \n" +
		        
		        
	           "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.APPLICATION_DETAILS_REPORT_LABEL;  
		
		System.out.print("2222-------------" + query);
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
