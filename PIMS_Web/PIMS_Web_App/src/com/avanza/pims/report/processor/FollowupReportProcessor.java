package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.CollectionReportCriteria;
import com.avanza.pims.report.criteria.FollowupReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;

public class FollowupReportProcessor extends AbstractReportProcessor {
@SuppressWarnings( "unchecked" )
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		
		FollowupReportCriteria criteria = (FollowupReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		String condition="";
		////////////////////////////////

		String query ="SELECT distinct" +
//		--FOLLOWUP 
		" FU.FOLLOW_UP_TYPE_ID,\n " +
		" DD_OW.DATA_DESC_EN ||' - ' || DD_OW.DATA_DESC_AR \"ExtraField1\", \n " +
		" ft.LOCATION \"ExtraField2\", \n " +
		" DD_FUS.DATA_DESC_AR \"ExtraField3\", \n "+ 
		" FU.START_DATE,\n " + 
		" FU.END_DATE,\n " + 
		" CAST(to_char(FU.FOLLOW_UP_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"followupDate\",\n "+
		" FU.COMPLETION_PERCENTAGE,\n " + 
		" FU.ACTUAL_COST,\n " + 
		" FU.REMARKS,\n " + 
		" FU.PROJECT_MILESTONE_ID,\n " + 
		" FU.FOLLOW_UP_NUMBER \"followupNumber\",\n " +

//		--PAYMENT RECEIPT DETAILS 
		" ft.LOCATION,\n " +
		" PRD.AMOUNT \"chequeAmount\",\n " +
		" PRD.BOUNCED_REASON,\n " +
		" PRD.CHEQUE_OWNER,\n " +
		" PRD.CHEQUE_TYPE,\n " +
		" CAST(to_char(PRD.METHOD_REF_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"chequeDate\",\n " +
		" PRD.METHOD_STATUS_ID,\n " +
		" PRD.PAYMENT_METHOD_ID,\n " +
		" PRD.METHOD_REF_NO \"chequeNumber\",\n " +

//		--BANK 
		"(CASE \n" + 
	  		" WHEN PRD.BANK_ID IS NULL \n" +
			" THEN PRD.BANK_NAME \n"+
			" ELSE case when BNK.BANK_EN != BNK.BANK_AR  then BNK.BANK_EN ||' - '||BNK.BANK_AR else BNK.BANK_EN  end \n"+
		   " END\n "+
		   " )	\"bankNameEn\",\n"+
	   "(CASE \n" + 
	  		" WHEN PRD.BANK_ID IS NULL \n" +
			" THEN PRD.BANK_NAME \n"+
			" ELSE case when BNK.BANK_EN != BNK.BANK_AR  then BNK.BANK_EN ||' - '||BNK.BANK_AR else BNK.BANK_AR  end \n"+
		   " END\n "+
		   " )	\"bankNameAr\",\n"+
		  	
//		" PRD.BANK_NAME \"bankNameEn\",\n " + 
//		" PRD.BANK_NAME \"bankNameAr\",\n " +

//		--CONTRACT 
		" CNTRCT.CONTRACT_NUMBER \"contractNumber\",\n " + 
		" CNTRCT.CONTRACT_DATE,\n " + 
		" CNTRCT.STATUS_ID,\n " + 
		" CNTRCT.RENT_AMOUNT,\n " + 
		" CNTRCT.START_DATE,\n " + 
		" CNTRCT.END_DATE,\n " + 
		" CNTRCT.CONTRACT_CATEGORY,\n " +

//		--PEROSN 
		" case when (full_name_en is null or length (full_name_en)<=0) then " +
		"	LTRIM ( PRSN.FIRST_NAME ||' ' || NVL(PRSN.MIDDLE_NAME,'')  || ' '  || PRSN.LAST_NAME  || PRSN.COMPANY_NAME  ) ||coalesce(' - '||PRSN.cell_number,'')  " +
		" else		" +
		"   LTRIM ( PRSN.FIRST_NAME ||' ' || NVL(PRSN.MIDDLE_NAME,'')  || ' '  || PRSN.LAST_NAME  || PRSN.COMPANY_NAME  )|| ' / '||PRSN.FULL_NAME_EN  ||coalesce(' - '||PRSN.cell_number,'')" +
		" end \"tenantName\" , " +
		" PRSN.DATE_OF_BIRTH,\n " + 
		" PRSN.CELL_NUMBER,\n " + 
		" PRSN.DESIGNATION,\n " + 
		" PRSN.SEC_CELL_NUMBER,\n " +

//		--UNIT 
		" UNT.UNIT_NUMBER,\n " + 
		" UNT.RENT_VALUE,\n " +

//		--FLOOR 
		" FLR.FLOOR_NAME,\n " + 
		" FLR.FLOOR_NUMBER,\n " +

//		--PROPERTY 
		" PROP.PROPERTY_NUMBER,\n " + 
		" PROP.COMMERCIAL_NAME,\n " + 
		" PROP.LAND_NUMBER\n " +
//These lines creating duplicate records, thats why commented.
//		--FOLLOW_UP_PROGRESS
		/*" FUP.COURT_RESULT,\n " + 
		" FUP.COMMENTS,\n " + 
		" FUP.FOLLOW_UP_PROGRESS_DATE,\n " +*/

//		--FOLLOW_UP_ACTIONS
	/*	" FUA.DESCRIPTION_AR,\n " + 
		" FUA.DESCRIPTION_EN,\n " + 
		" FUA.FOLLOW_UP_STATUS,\n " +
		" FUA.DATA_VALUE " +*/

		" FROM FOLLOW_UP FU \n" + 
						 
		" INNER JOIN PAYMENT_RECEIPT_DETAIL PRD ON PRD.PAYMENT_RECEIPT_DETAIL_ID = FU.PAYMENT_RECEIPT_DETAIL_ID \n" +
		" INNER JOIN PAYMENT_RECEIPT_TERMS PRT ON PRD.PAYMENT_RECEIPT_DETAIL_ID = PRT.PAYMENT_RECEIPT_DETAIL_ID \n" +
		" INNER JOIN PAYMENT_SCHEDULE PS ON PS.PAYMENT_SCHEDULE_ID= PRT.PAYMENT_SCHEDULE_ID\n" +
		" INNER JOIN FINANCIAL_TRANSACTION FT ON FT.PAYMENT_SCHEDULE_ID= PS.PAYMENT_SCHEDULE_ID\n" +
		" LEFT JOIN BANK BNK ON PRD.BANK_ID = BNK.BANK_ID \n" +

		" LEFT JOIN CONTRACT CNTRCT ON CNTRCT.CONTRACT_ID = FU.CONTRACT_ID\n " +

		" LEFT JOIN PERSON PRSN ON PRSN.PERSON_ID = CNTRCT.TENANT_ID\n " +

		" LEFT JOIN CONTRACT_UNIT CU ON CU.CONTRACT_ID = CNTRCT.CONTRACT_ID\n " +

		" LEFT JOIN UNIT UNT ON UNT.UNIT_ID =CU.UNIT_ID\n " +

		" LEFT JOIN FLOOR FLR ON FLR.FLOOR_ID =UNT.FLOOR_ID\n " +

		" LEFT JOIN PROPERTY PROP ON PROP.PROPERTY_ID = FLR.PROPERTY_ID\n " +

		" LEFT JOIN FOLLOW_UP_PROGRESS FUP ON FUP.FOLLOW_UP_ID = FU.FOLLOW_UP_ID\n " +

		" LEFT JOIN FOLLOW_UP_ACTIONS FUA ON FUA.FOLLOW_UP_ACTIONS_ID = FUP.FOLLOW_UP_ACTIONS_ID\n " +
		
		" LEFT JOIN DOMAIN_DATA DD_FUS ON DD_FUS.DOMAIN_DATA_ID = FU.STATUS_ID\n " +
		
		" LEFT JOIN DOMAIN_DATA DD_OW ON DD_OW.DOMAIN_DATA_ID = FT.OWNERSHIP_TYPE_ID " ;
        
		////////////////////////////////
	                    
		condition += " AND FT.IS_DELETED = 0" ;
		if( !criteria.getAction().equals("-1") )
			condition += " AND FUP.FOLLOW_UP_ACTIONS_ID = " + criteria.getAction();
		
		if( !criteria.getBank().equals("-1") )
			condition += " AND BNK.BANK_ID = " + criteria.getBank();
		
		if( criteria.getChequeNumber().trim().length() > 0 )
			condition += " AND LOWER(PRD.METHOD_REF_NO) like LOWER('%" + criteria.getChequeNumber() + "%')";
		
		if( criteria.getContractNumber().trim().length() > 0 )
			condition += " AND LOWER(CNTRCT.CONTRACT_NUMBER) like LOWER('%" + criteria.getContractNumber() + "%')";
		
		if(criteria.getFollowupNumber().trim().length() > 0)
			condition += " AND LOWER(FU.FOLLOW_UP_NUMBER) like LOWER('%" + criteria.getFollowupNumber() + "%')";
		
		if(criteria.getFollowupFrom()!=null  && criteria.getFollowupTo()!=null)								
			condition += " AND FU.START_DATE BETWEEN TO_DATE ('"+ DateUtil.convertToDisplayDate( criteria.getFollowupFrom() ) +"', 'dd/MM/yyyy')  AND TO_DATE ('" + DateUtil.convertToDisplayDate( criteria.getFollowupTo() )+"', 'dd/MM/yyyy')";
		
		else if(criteria.getFollowupFrom() != null)
			condition += " AND FU.START_DATE >= TO_DATE('" + DateUtil.convertToDisplayDate( criteria.getFollowupFrom() ) +"', 'dd/MM/yyyy')";
		
		else if(criteria.getFollowupTo() != null)
			condition += " AND FU.START_DATE <= TO_DATE('" + DateUtil.convertToDisplayDate( criteria.getFollowupTo() ) +"', 'dd/MM/yyyy')";
		
		if(criteria.getPropertyName() != null && criteria.getPropertyName().length() > 0)
			condition += " AND LOWER(PROP.COMMERCIAL_NAME) LIKE LOWER('%"+criteria.getPropertyName()+"%')";
		
		if(criteria.getUnitNumber() != null && criteria.getUnitNumber().length() > 0)
			condition += " 	AND LOWER(UNT.UNIT_NUMBER) LIKE LOWER('%"+criteria.getUnitNumber()+"%')";
		
		if(criteria.getTenantName() .length() >0)
			condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
		/*
			condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.COMPANY_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
							" ) " ;*/
		
		if(!criteria.getStatus().equals("-1"))
			condition += " AND FU.STATUS_ID = "+criteria.getStatus();
		
		if(criteria.getPaymentDateFrom()!=null  && criteria.getPaymentDateTo()!=null)								
			condition += " AND PRD.METHOD_REF_DATE BETWEEN TO_DATE ('"+ DateUtil.convertToDisplayDate( criteria.getPaymentDateFrom() ) +"', 'dd/MM/yyyy')  AND TO_DATE ('" + DateUtil.convertToDisplayDate( criteria.getPaymentDateTo() )+"', 'dd/MM/yyyy')";
		
		else if(criteria.getPaymentDateFrom() != null)
			condition += " AND PRD.METHOD_REF_DATE >= TO_DATE('" + DateUtil.convertToDisplayDate( criteria.getPaymentDateFrom() ) +"', 'dd/MM/yyyy')";
		
		else if(criteria.getPaymentDateTo() != null)
			condition += " AND PRD.METHOD_REF_DATE <= TO_DATE('" + DateUtil.convertToDisplayDate( criteria.getPaymentDateTo() ) +"', 'dd/MM/yyyy')";
	
		if(condition.length() > 0)
        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY FU.FOLLOW_UP_NUMBER  ";
//							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FOLLOWUP_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		
        query = "SELECT \n" + 
        
        
 
        		/*******************************                           English Labels             *********************************/
        		"  '" + ReportMessageResource.getPropertyEn("followup.number") + "' \"lblFollowupNumberEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("followup.date") + "' \"lblFollowupDateEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("payment.chequeDate") + "' \"lblChequeDateEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.chequeNumberCol") + "' \"lblChequeNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("cheque.amount") + "' \"lblChequeAmountEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("contract.contractNumber") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("collectPayment.TotalAmount") + "' \"lblTotalAmountEn\",\n" +
		        
		        /*******************************                           Arabic Labels             *********************************/
        		"  '" + ReportMessageResource.getPropertyAr("followup.number") + "' \"lblFollowupNumberAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("followup.date") + "' \"lblFollowupDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("payment.chequeDate") + "' \"lblChequeDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.chequeNumberCol") + "' \"lblChequeNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("cheque.amount") + "' \"lblChequeAmountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("contract.contractNumber") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("collectPayment.TotalAmount") + "' \"lblTotalAmountAr\",\n" +
        		
		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.followupReport.jsp") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.followupReport.jsp") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.FOLLOWUP_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}

