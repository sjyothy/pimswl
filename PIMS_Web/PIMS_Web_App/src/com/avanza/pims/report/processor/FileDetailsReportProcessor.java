package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.FileDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class FileDetailsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		FileDetailsReportCriteria criteria = (FileDetailsReportCriteria) reportCriteria;
		
		executeFileDetailQuery(criteria,reportQueryMap);
		executeBeneficiariesDetailQuery(criteria,reportQueryMap);
		executeFileAssetsDetailsQuery(criteria,reportQueryMap);
		executeFileSharesDetailsQuery(criteria,reportQueryMap);
		executeLabelQuery(criteria,reportQueryMap);
        
		return reportQueryMap;
	}

	private void executeLabelQuery(FileDetailsReportCriteria criteria, Map<String, String> reportQueryMap) 
	{
		String query = "";
		query = "SELECT \n" + 
	       
        "  '" + ReportMessageResource.getPropertyEn("report.label.accountnumber") + "' \"lblAccountNumberEn\",\n" +
        
        /*******************************                           Arabic Labels             *********************************/
        
        "  '" + ReportMessageResource.getPropertyAr("report.label.accountnumber") + "' \"lblAccountNumberAr\",\n" + 
        

        /*******************************                           Mandatory   Lables          *********************************/ 	        
        
        "  '" + ReportMessageResource.getPropertyEn("memsReportHeader") + "' \"lblMEMSHeaderEn\",\n" + 
        "  '" + ReportMessageResource.getPropertyEn("reportName.fileDetails") + "' \"lblReportNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("reportName.fileDetails") + "' \"lblReportNameAr\",\n" + 
        "  '" + ReportMessageResource.getPropertyEn("memsReportHeader") + "' \"lblMEMSHeaderAr\",\n" +
         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

        "FROM DUAL";
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FILE_DETAILS_LABEL;        
		reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeFileSharesDetailsQuery(FileDetailsReportCriteria criteria, Map<String, String> reportQueryMap)  
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_SHARE_DETAILS ";
		condition += " WHERE FILEID = " + criteria.getFileId();
		//condition += " --//Shares";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FILE_DETAILS_SHARES +"#Shares";        
		reportQueryMap.put(dataDef, query+"");
	}

	private void executeFileAssetsDetailsQuery(FileDetailsReportCriteria criteria, Map<String, String> reportQueryMap)  
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAILS ";
		condition += " WHERE FILEID = " + criteria.getFileId();
//		condition += " --//Assets";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FILE_DETAILS_ASSET+"#Assets";        
		reportQueryMap.put(dataDef, query+"");
	}

	private void executeBeneficiariesDetailQuery(FileDetailsReportCriteria criteria, Map<String, String> reportQueryMap)  
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM BENEFICIARY_DETAILS ";
		condition += " WHERE FILEID = " + criteria.getFileId();
//		condition += " --//Beneficiary";
		query += condition;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FILE_DETAILS_BENEFICIARY+"#Beneficiary";        
		reportQueryMap.put(dataDef, query+"");
	}

	private void executeFileDetailQuery(FileDetailsReportCriteria criteria, Map<String, String> reportQueryMap) 
	{
		String query = "";
		String condition = "";
		query += " SELECT * FROM FILE_DETAILS  ";
		condition += " WHERE FILEID = " + criteria.getFileId();
		
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.FILE_DETAILS;        
		reportQueryMap.put(dataDef, query+"");
	}
			

}
