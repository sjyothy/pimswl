package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.InheritanceDetailsReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;

public class InheritanceDetailsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		InheritanceDetailsReportCriteria criteria = (InheritanceDetailsReportCriteria) reportCriteria;
		
		executeInheritanceDetailsQuery(criteria,reportQueryMap);
		executeLandAndPropertiesQuery(criteria,reportQueryMap);
		executeBankTransferQuery(criteria,reportQueryMap);
		executeLicenceQuery(criteria,reportQueryMap);
		executeAnimalQuery(criteria,reportQueryMap);
		executeCashQuery(criteria,reportQueryMap);
		executeChequeQuery(criteria,reportQueryMap);
	    executeLoanLiabilityQuery(criteria,reportQueryMap);
		executeStockShareQuery(criteria,reportQueryMap);
		executePensionQuery(criteria,reportQueryMap);
		executeVehicleQuery(criteria,reportQueryMap);
		executeJewelryQuery(criteria,reportQueryMap);
		executeTransportationQuery(criteria,reportQueryMap);
		executeLabelQuery(criteria,reportQueryMap);
        
		return reportQueryMap;
	}


	private void executeLabelQuery(InheritanceDetailsReportCriteria criteria, Map<String, String> reportQueryMap) 
	{
		String query = "";
		query = "SELECT \n" + 
	       

        /*******************************                           Mandatory   Lables          *********************************/ 	        
        
        "  '" + ReportMessageResource.getPropertyEn("memsReportHeader") + "' \"lblMEMSHeaderEn\",\n" + 
        "  '" + ReportMessageResource.getPropertyEn("reportName.inhDetails") + "' \"lblReportNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("reportName.inhDetails") + "' \"lblReportNameAr\",\n" + 
        "  '" + ReportMessageResource.getPropertyEn("memsReportHeader") + "' \"lblMEMSHeaderAr\",\n" +
         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

        "FROM DUAL";
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.INHERITANCE_DETAILS_LABEL;        
		reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeLandAndPropertiesQuery(InheritanceDetailsReportCriteria criteria, Map<String, String> reportQueryMap)  
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId()+ " AND ASSETTYPEID = " + WebConstants.AssetType.LAND_PROPERTIES;
//		condition += " --LandAndProperties";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.LAND_AND_PROPERTIES_DETAILS+"#LandAndProperties";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"LandAndProperties",ReportConstant.DataDef.INHERITANCE_DETAILS_LAND_LABEL);
	}
	private void executeBankTransferQuery(InheritanceDetailsReportCriteria criteria, Map<String, String> reportQueryMap)  
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId()+ " AND ASSETTYPEID = " + WebConstants.AssetType.BANK_TRANSFER;
//		condition += " --BankTransfer";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.BANK_TRANSFER_DETAILS+"#BankTransfer";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"BankTransfer",ReportConstant.DataDef.INHERITANCE_DETAILS_BANK_TRANSFER_LABEL);
		
	}
	private void executeInheritanceDetailsQuery(InheritanceDetailsReportCriteria criteria, Map<String, String> reportQueryMap) 
	{
		String query = "";
		String condition = "";
		query += " SELECT * FROM FILE_DETAILS_WITH_ASSET_TYPE  ";
		condition += " WHERE FILEID = " + criteria.getFileId();
		
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.INHERITANCE_DETAILS;        
		reportQueryMap.put(dataDef, query+"");
	}
	private void executeLicenceQuery(InheritanceDetailsReportCriteria criteria,	Map<String, String> reportQueryMap) 
	{
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.LICENSES;
//		condition += " --// License";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.LICENSE_DETAILS+"#License";        
		reportQueryMap.put(dataDef, query+"");		
		executeSubReportLabelQuery(reportQueryMap,"License",ReportConstant.DataDef.INHERITANCE_DETAILS_LICENSES_LABEL);
	}
	private void executeTransportationQuery(
			InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.TRANSPORTATIONS;
//		condition += " --//Transportation";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.TRANSPORTATION_DETAILS+"#Transportation";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Transportation",ReportConstant.DataDef.INHERITANCE_DETAILS_TRANSPORTATION_LABEL);
		
	}


	private void executeJewelryQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.JEWELLERY;
//		condition += " --//Jewelry";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.JEWELRY_DETAILS+"#Jewelry";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Jewelry",ReportConstant.DataDef.INHERITANCE_DETAILS_JEWELRY_LABEL);
		
	}


	private void executeVehicleQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.VEHICLES;
//		condition += " --//Vehicle";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.VEHICLE_DETAILS+"#Vehicle";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Vehicle",ReportConstant.DataDef.INHERITANCE_DETAILS_VEHICLE_LABEL);
		
	}


	private void executePensionQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.PENSION;
//		condition += " --//Pension";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PENSION_DETAILS+"#Pension";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Pension",ReportConstant.DataDef.INHERITANCE_DETAILS_PENSION_LABEL);
		
	}


	private void executeStockShareQuery(
			InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.STOCK_SHARES;
//		condition += " --//StockShare";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.STOCK_SHARE_DETAILS+"#StockShare";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"StockShare",ReportConstant.DataDef.INHERITANCE_DETAILS_STOCK_SHARE_LABEL);
		
	}


	private void executeLoanLiabilityQuery(
			InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		String subreportName = "LoanLiability";
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.LOAN_LIABILITIES;
//		condition += " --//"+subreportName;
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.LOAN_AND_LIABILITIES_DETAILS+"#LoanLiability";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,subreportName,
				                               ReportConstant.DataDef.INHERITANCE_DETAILS_LOAN_LIABILITY_LABEL);
		
		
		
	}


	private void executeChequeQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.CHEQUE;
//		condition += " --//Cheque";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.CHEQUE_DETAILS+"#Cheque";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Cheque",ReportConstant.DataDef.INHERITANCE_DETAILS_CHEQUE_LABEL);
		
	}


	private void executeCashQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {
		String query = "";
		String condition = "";
		
		query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
		condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.CASH;
//		condition += " --//Cash";
		query += condition;
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.CASH_DETAILS+"#Cash";        
		reportQueryMap.put(dataDef, query+"");
		executeSubReportLabelQuery(reportQueryMap,"Cash",ReportConstant.DataDef.INHERITANCE_DETAILS_CASH_LABEL);
		
	}


	private void executeAnimalQuery(InheritanceDetailsReportCriteria criteria,
			Map<String, String> reportQueryMap) {String query = "";
			String condition = "";
			
			query += " SELECT * FROM ASSET_DETAIL_FOR_REPORT ";
			condition += " WHERE FILEID = " + criteria.getFileId() + " AND ASSETTYPEID = " + WebConstants.AssetType.ANIMAL;
//			condition += " --//Animal";
			query += condition;
			System.out.print("-------------" + query);
			String dataDef = ReportConstant.DataDef.ANIMAL_DETAILS+"#Animal";        
			reportQueryMap.put(dataDef, query+"");
			executeSubReportLabelQuery(reportQueryMap,"Animal",ReportConstant.DataDef.INHERITANCE_DETAILS_ANIMAL_LABEL);
		
	}
	private void executeSubReportLabelQuery(Map<String, String> reportQueryMap, String subReportName,String labelDataDefName) 
	{
		String query = "";
		query = "SELECT \n" + 
	       

        /*******************************                           Mandatory   Lables          *********************************/ 	        
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.assetName") + "' \"assetNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.assetTye") + "' \"assetTyeEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.assetDesc") + "' \"assetDescEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.fromDate") + "' \"fromDateEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.amount") + "' \"amountEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.revenue") + "' \"revenueEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.isIncomExp") + "' \"isIncomExpEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.dueDate") + "' \"dueDateEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.chequeNumber") + "' \"chequeNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.bankTrNumber") + "' \"bankTrNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.cash") + "' \"cashEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.bankName") + "' \"bankNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.gotvDeptName") + "' \"gotvDeptNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.concernedPerson") + "' \"concernedPersonEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.cellPhone") + "' \"cellPhoneEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.officePhone") + "' \"officePhoneEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.devolution") + "' \"devolutionEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.landNumber") + "' \"landNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.email") + "' \"emailEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.fax") + "' \"faxEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.status") + "' \"statusEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.revenueType") + "' \"revenueTypeEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.isFinal") + "' \"isFinalEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.assetNumber") + "' \"assetNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.region") + "' \"regionEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.animalType") + "' \"animalTypeEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.quantity") + "' \"quantityEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.vehicleType") + "' \"vehicleTypeEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.vehicleCategory") + "' \"vehicleCategoryEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.plateNumber") + "' \"plateNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.transferParty") + "' \"transferPartyEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.company") + "' \"companyEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.noOfShares") + "' \"noOfSharesEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.listingParty") + "' \"listingPartyEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.licenseName") + "' \"licenseNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.licenseNumber") + "' \"licenseNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.rentValue") + "' \"rentValueEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.party") + "' \"partyEn\",\n" +        
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.typeGoldSilver") + "' \"typeGoldSilverEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.weightInGm") + "' \"weightInGmEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.noOfJewelleries") + "' \"noOfJewelleriesEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.transferNumber") + "' \"transferNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.accountName") + "' \"accountNameEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.accountNumber") + "' \"accountNumberEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("inhDetails.reportLabel.isAmafAcc") + "' \"isAmafAccEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("contract.person.Manager") + "' \"managerEn\",\n" +
        "  '" + ReportMessageResource.getPropertyEn("thirdPartyUnit.amafManager") + "' \"amafManagerEn\",\n" +
        
        
    	/////////////////////////arabic resources
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.assetName") + "' \"assetNameAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.assetTye") + "' \"assetTyeAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.assetDesc") + "' \"assetDescAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.fromDate") + "' \"fromDateAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.amount") + "' \"amountAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.revenue") + "' \"revenueAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.isIncomExp") + "' \"isIncomExpAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.dueDate") + "' \"dueDateAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.chequeNumber") + "' \"chequeNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.bankTrNumber") + "' \"bankTrNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.cash") + "' \"cashAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.bankName") + "' \"bankNameAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.gotvDeptName") + "' \"gotvDeptNameAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.concernedPerson") + "' \"concernedPersonAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.cellPhone") + "' \"cellPhoneAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.officePhone") + "' \"officePhoneAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.devolution") + "' \"devolutionAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.landNumber") + "' \"landNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.email") + "' \"emailAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.fax") + "' \"faxAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.status") + "' \"statusAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.revenueType") + "' \"revenueTypeAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.isFinal") + "' \"isFinalAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.assetNumber") + "' \"assetNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.region") + "' \"regionAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.animalType") + "' \"animalTypeAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.quantity") + "' \"quantityAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.vehicleType") + "' \"vehicleTypeAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.vehicleCategory") + "' \"vehicleCategoryAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.plateNumber") + "' \"plateNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.transferParty") + "' \"transferPartyAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.company") + "' \"companyAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.noOfShares") + "' \"noOfSharesAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.listingParty") + "' \"listingPartyAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.licenseName") + "' \"licenseNameAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.licenseNumber") + "' \"licenseNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.rentValue") + "' \"rentValueAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.party") + "' \"partyAr\",\n" +        
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.typeGoldSilver") + "' \"typeGoldSilverAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.weightInGm") + "' \"weightInGmAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.noOfJewelleries") + "' \"noOfJewelleriesAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.transferNumber") + "' \"transferNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.accountName") + "' \"accountNameAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.accountNumber") + "' \"accountNumberAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("contract.person.Manager") + "' \"managerAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("thirdPartyUnit.amafManager") + "' \"amafManagerAr\",\n" +
        "  '" + ReportMessageResource.getPropertyAr("inhDetails.reportLabel.isAmafAcc") + "' \"isAmafAccAr\"\n" +
        "FROM DUAL ";
//        "FROM DUAL --//" + subReportName;
		System.out.print("-------------" + query);
		reportQueryMap.put(labelDataDefName+"#"+subReportName, query+"");
		
	}		

}
