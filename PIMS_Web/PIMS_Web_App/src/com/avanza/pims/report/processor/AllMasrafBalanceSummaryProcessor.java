package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;

public class AllMasrafBalanceSummaryProcessor extends AbstractReportProcessor 
{
	Logger logger = Logger.getLogger(this.getClass());
	
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;
		try
		{

			executeQuery(criteria,reportQueryMap);
		}    
		catch(Exception e)
		{
			logger.LogException("Error occured:", e);
		}
		return reportQueryMap;
	}

	private void executeQuery(
								StatementOfAccountCriteria criteria, Map<String, String> reportQueryMap
							 )throws Exception  
	{
		String query = "";
		StringBuilder condition = new StringBuilder("");
		
//		query +=" SELECT   * FROM 	MASRAF_BALANCE_SUMMARY  WHERE 1=1 ";
		
		query +=
		  " select  " +
		  "         '"+criteria.getReportGeneratedBy()+"' as \"field1\","+
		  "         '"+criteria.getForYear()+"' as \"field2\","+
		  "         '"+criteria.getPreviousYear()+"' as \"field3\","+
		  "			'"+criteria.getShowSubMasrafTrx()+"' as \"showOnlyMainMasraf\","+
		  "         cast(coalesce(m.parent_masraf_id,m.masraf_id ) as varchar2(50 ) )\"compositeMasrafId\","+
		  "         M.MASRAF_ID \"masrafId\",M.MASRAF_NAME \"masrafName\",M.GL_SEGMENT_FIVE \"masrafGL\",M.GRP_ACC_NO \"masrafGRPAccNo\","+
		  "         coalesce(cast(MP.MASRAF_ID as varchar2(50)),'-1') \"parentMasrafId\",MP.MASRAF_NAME \"parentMasrafName\"," +
		  "			MP.GL_SEGMENT_FIVE \"parentMasrafGL\",MP.GRP_ACC_NO \"parentMasrafGRPAccNo\","+
		  
		  "         coalesce(\"totalBalance\",0) \"totalBalance\"," +
		  "			coalesce(\"totalCredit\",0)\"totalCredit\"," +
		  "			coalesce(\"totalDebit\",0) \"totalDebit\","+
		  
		  "         coalesce(\"totalBalanceLastYear\",0) \"totalBalanceLastYear\"," +
		  "			coalesce(\"totalCreditLastYear\",0)\"totalCreditLastYear\"," +
		  "			coalesce(\"totalDebitLastYear\",0) \"totalDebitLastYear\","+
		  "         coalesce ( \"requestedAmount\" ,0) as \"requestedAmount\" ,"+
		  "         (coalesce(\"totalBalance\",0)- coalesce(\"requestedAmount\" ,0)) \"availableBalance\""+
		  " from MASRAF M"+
		  " left join masraf mp on m.PARENT_MASRAF_ID=mp.masraf_id"+
		  " left join"+
		  " ("+
		  "   select "+
		  "   coalesce(SUM(case when PAT.AMOUNT >=0 then PAT.AMOUNT else 0 end ),0) as \"totalCredit\","+
		  "   coalesce(sum(case when PAT.AMOUNT <0 then  PAT.AMOUNT else 0 end),0) as \"totalDebit\","+
		  "   efb.masraf_id"+ 
		  "   from "+
		  "   PERSONAL_ACCOUNT_TRANS pat"+
		  "   inner join PERSONAL_ACCOUNT PA on PA.PERSONAL_ACCOUNT_ID=PAT.PERSONAL_ACCOUNT_ID"+
		  "   inner join (select distinct person_id,masraf_id from end_file_ben where is_deleted=0 ) efb on (efb.person_id=pa.person_id or pa.masraf_id=efb.masraf_id)"+
		  "   WHERE 1=1 and"+
		  "         TRANS_DATE between TO_DATE('"+criteria.getFormattedFromDate()+"','dd-MM-RRRR hh24:mi:ss') and " +
		  "																		TO_DATE('"+criteria.getFormattedToDate()+"','dd-MM-RRRR hh24:mi:ss')"+
		  "   group by efb.masraf_id"+
		            
		  " ) SAM on M.MASRAF_ID=SAM.masraf_id"+
		  " left join"+
		  " ("+
		  "   select coalesce(SUM(AMOUNT),0) as \"totalBalance\","+
		  "   efb.masraf_id"+
		  "   from "+
		  "   PERSONAL_ACCOUNT_TRANS pat"+
		  "   inner join PERSONAL_ACCOUNT PA on PA.PERSONAL_ACCOUNT_ID=PAT.PERSONAL_ACCOUNT_ID"+
		  "   inner join (select distinct person_id,masraf_id from end_file_ben where is_deleted=0 ) efb on (efb.person_id=pa.person_id or pa.masraf_id=efb.masraf_id)"+
		  "   WHERE 1=1 and"+
		  "         TRANS_DATE <= TO_DATE('"+criteria.getFormattedToDate()+"','dd-MM-RRRR hh24:mi:ss')"+
		  "   group by efb.MASRAF_ID"+
		  " )SAM_TB_SELECTED_YEAR on m.masraf_id=SAM_TB_SELECTED_YEAR.masraf_id"+
		  " left join"+
		  " ("+
		  "   select "+
		  "   coalesce(SUM(case when PAT.AMOUNT >=0 then PAT.AMOUNT else 0 end ),0) as \"totalCreditLastYear\","+
		  "   coalesce(sum(case when PAT.AMOUNT <0 then  PAT.AMOUNT else 0 end),0) as \"totalDebitLastYear\","+
		  "   efb.masraf_id"+ 
		  "   from "+
		  "   PERSONAL_ACCOUNT_TRANS pat"+
		  "   inner join PERSONAL_ACCOUNT PA on PA.PERSONAL_ACCOUNT_ID=PAT.PERSONAL_ACCOUNT_ID"+
		  "   inner join (select distinct person_id,masraf_id from end_file_ben where is_deleted=0 ) efb on (efb.person_id=pa.person_id or pa.masraf_id=efb.masraf_id)"+
		  "   WHERE 1=1 and"+
		  
		  "         TRANS_DATE between TO_DATE('"+criteria.getFormattedFromDatePreviousYear()+"','dd-MM-RRRR hh24:mi:ss') and " +
		  "																		TO_DATE('"+criteria.getFormattedToDatePreviousYear()+"','dd-MM-RRRR hh24:mi:ss')"+
		  "   group by efb.MASRAF_ID"+
		  " )SAM_LAST_YEAR on m.masraf_id=SAM_LAST_YEAR.masraf_id"+
		  " left join"+
		  
		  " ("+
		  "   select coalesce(SUM(AMOUNT),0) as \"totalBalanceLastYear\","+
		  "   efb.masraf_id"+
		  "   from "+
		  "   PERSONAL_ACCOUNT_TRANS pat"+
		  "   inner join PERSONAL_ACCOUNT PA on PA.PERSONAL_ACCOUNT_ID=PAT.PERSONAL_ACCOUNT_ID"+
		  "   inner join (select distinct person_id,masraf_id from end_file_ben where is_deleted=0 ) efb on (efb.person_id=pa.person_id or pa.masraf_id=efb.masraf_id)"+
		  "   WHERE 1=1 and"+
		  
		  "         TRANS_DATE <= TO_DATE('"+criteria.getFormattedToDatePreviousYear()+"','dd-MM-RRRR hh24:mi:ss')"+
		  "   group by efb.MASRAF_ID"+
		  " )SAM_TB_LAST_YEAR on m.masraf_id=SAM_TB_LAST_YEAR.masraf_id"+
		  " left join"+ 
		  " ("+
		  "   select efb.MASRAF_ID,SUM(DD.AMOUNT)  as \"requestedAmount\""+ 
		  "   from "+
		  "   DISBURSEMENT_DETAILS DD "+
		  "   left join dis_req_beneficiary Drb on drb.dis_det_id=dd.dis_det_id "+
		  "   inner join (select distinct person_id,masraf_id from end_file_ben where is_deleted=0 ) efb on (efb.person_id=drb.person_id or dd.masraf_id=efb.masraf_id)"+
		  
		  
//		  "   inner join MASRAF M on DD.MASRAF_ID=M.MASRAF_ID"+
		  "   inner join request r on r.request_id=dd.request_id"+
		  "   where "+
		  "   DD.STATUS not in (165003,165006,165007) and"+ 
		  "   DD.IS_DELETED=0 and "+
		  "   ( DD.IS_DISBURSED is null or DD.IS_DISBURSED = 0 ) and"+
		  "   R.IS_DELETED=0"+
		  "   and R.STATUS_ID not in (9003)"+
		  "   group by efb.MASRAF_ID"+
		  " ) SQ on SQ. MASRAF_ID =M.MASRAF_ID"+
		  " where  1=1 and M.IS_DELETED=0 "+
		

		  " order by \"compositeMasrafId\",m.parent_masraf_id desc ";

		
		
		
		if( criteria.getMasrafName() != null && criteria.getMasrafName().trim().length() > 0 )
		{
			condition.append( " and lower(\"masrafName\") like '%"+ criteria.getMasrafName().trim().toLowerCase()+"%'");
		}
		query += condition.toString();
		System.out.print("-------------" + query);
		String dataDef = "AllMasrafBalanceDetailsDataDef";        
		reportQueryMap.put(dataDef, query);
	
	}
		
	}
