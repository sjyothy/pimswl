package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PaymentReceiptReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PaymentReceiptReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		PaymentReceiptReportCriteria criteria = (PaymentReceiptReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////
	/*	String query = "SELECT " +
							" PRCT.RECEIPT_NUMBER \"receiptNumber\" , " +
					        //" CNTRT.CONTRACT_NUMBER \"contractNumber\" , " +
							  " '' \"contractNumber\" , " +
					//        -- Tenant Name
					        //" PRSN.PERSON_ID , " +
					        " '', " +
					        //" PRSN.FIRST_NAME ||' ' ||PRSN.MIDDLE_NAME||' '||PRSN.LAST_NAME \"customerName\" , " +
					        " '' \"customerName\" , " +
//					        " PRSN.MIDDLE_NAME , " +
//					        " PRSN.LAST_NAME , " +
//					        " PRSN.COMPANY_NAME, " +
					        " '' , " +
					        " '' , " +
					        " '', " +
					//        -- Description
					//      " U.UNIT_NUMBER || ' - ' ||  PROP.COMMERCIAL_NAME || ' - ' || NVL (LAND_NUMBER, '--')  \"description\" , " +
					        " '' \"description\" , " +
					//        -- Period of
					        //" CAST(to_char( CNTRT.START_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) ||  '-' ||CAST(to_char( CNTRT.END_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) \"period\" ,  " +
					        " CAST(to_char( sysdate ,'dd/mm/yyyy') AS VARCHAR(10) ) ||  '-' ||CAST(to_char( sysdate ,'dd/mm/yyyy') AS VARCHAR(10) ) \"period\" ,  " +
					//        -- Receipt Date
					        " CAST(to_char( PRCT.CREATED_ON ,'dd/mm/yyyy') AS VARCHAR(10) ) \"receiptDate\" , " +
					//        -- Total Amount - Sum of all the Transaction in Detail
					        
					//        -- DETAILS INFOMRATION
					        
					//        -- Cheque Date 
					        " CAST(to_char( PRCD.METHOD_REF_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) \"chequeDate\" ,  " +
//					        " PRCD.METHOD_REF_DATE \"chequeDate\" , " +
					        " BANK.BANK_EN , " +
					        " BANK.BANK_AR \"bank\" ,  " +
					//        -- Cheque Number
					        " PRCD.METHOD_REF_NO \"chequeNumber\" , " + 
					        " PMT.DESCRIPTION, " +
					//        -- Amount
					        " PRCD.AMOUNT \"amount\" " +
					                            
					        " FROM " + 
					      //  " CONTRACT CNTRT " + 
//					        " INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
//					        " INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID  " +
//					        " INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
//					        " INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
//					        " LEFT JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
					        " PAYMENT_SCHEDULE "+//PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
					        " INNER  JOIN PAYMENT_RECEIPT_TERMS PRC  ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
					        
					        " INNER  JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
					        " INNER JOIN PAYMENT_RECEIPT PRCT  ON PRCD.PAYMENT_RECEIPT_ID = PRCT.PAYMENT_RECEIPT_ID " +
					        " LEFT JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
					        " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " ;
        
*/      
		
		String query = "SELECT " +
		  " ps.payment_schedule_id \"paymentScheduleId\", prcd.payment_method_id ,"+
		  " ps.payment_number \"paymentNumber\", pt.description_en || ' - ' || pt.description_ar \"paymentType\", ps.amount \"psAmount\" , "+ 
		  "PRCD.PAYMENT_RECEIPT_DETAIL_ID,PRCD.METHOD_REF_DATE ," +
		  " cnt.contract_number \"contractNumber\" , " +
		  " P.GRP_CUSTOMER_NO \"customerNumber\" , " +
		  " (case when cnt.contract_id is not null then unt.account_number when auctionUnit.auction_unit_id is not null then aunit.account_number else '' end)  \"costCenter\", "+
	        
	        " '' \"description\" , " +
		" PRCT.RECEIPT_NUMBER \"receiptNumber\" , " +
		" unt.UNIT_DESC \"unitDesc\" ," +
        " CAST(to_char( sysdate ,'dd/mm/yyyy') AS VARCHAR(10) ) ||  '-' ||CAST(to_char( sysdate ,'dd/mm/yyyy') AS VARCHAR(10) ) \"period\" ,  " +
        " CAST(to_char( PRCT.CREATED_ON ,'dd/mm/yyyy') AS VARCHAR(10) ) \"receiptDate\" , " +
//        -- DETAILS INFOMRATION
        
//        -- Cheque Date 
        " CAST(to_char( PRCD.METHOD_REF_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) \"chequeDate\" ,  " +
        " CAST(to_char( OLD_PRCD.METHOD_REF_DATE ,'dd/mm/yyyy') AS VARCHAR(10) ) \"oldChequeDate\" ,  " +
//        " PRCD.METHOD_REF_DATE \"chequeDate\" , " +
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_EN end \"bank\" , " +
        " CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else BANK.BANK_AR end ,  " +
        " CASE WHEN OLD_PRCD.MIGRATED IS NOT NULL AND OLD_PRCD.MIGRATED =1 THEN OLD_PRCD.BANK_NAME else OLD_BANK.BANK_EN end \"oldBank\" , " +
        " CASE WHEN OLD_PRCD.MIGRATED IS NOT NULL AND OLD_PRCD.MIGRATED =1 THEN OLD_PRCD.BANK_NAME else OLD_BANK.BANK_AR end ,  " +
        
        " RQST.REQUEST_NUMBER \"requestNumber\" , "+
//        -- Cheque Number
        " PRCD.METHOD_REF_NO \"chequeNumber\" , " +
        " OLD_PRCD.METHOD_REF_NO \"oldChequeNumber\" , " + 
        " PMT.DESCRIPTION_AR \"paymentMethod\", " +
        " OLD_PS.AMOUNT \"oldChequeAmount\"," +
//        -- Amount
        //" PRCD.AMOUNT \"amount\" ," +
	    " (case when pmt.payment_method_id = 2 then null else prcd.amount end) \"amount\" , " + 
                            
//        --Paid By Name
        " ltrim ( rtrim(" +
        "                coalesce(         "+
  " ( coalesce (P.first_name,'') || ' '|| coalesce (P.middle_name,'')|| ' '|| coalesce (P.last_name,'')||' '||    coalesce (P.company_name,'') ),"+ 
   " coalesce ( CAST( P.contractor_name_AR AS VARCHAR(250) ),P.contractor_name_en,'')                  ) )) as \"customerName\" ,"+
   " puv.OWNERSHIP_TYPE_ID ," +
   " puv.OWNER_SHIP_TYPE_DESC_EN \"ownershipTypeEn\","+	
   " puv.OWNER_SHIP_TYPE_DESC_AR \"ownershipTypeAr\" \n"+
        " FROM "+
        " PAYMENT_RECEIPT PRCT  INNER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCT.PAYMENT_RECEIPT_ID=PRCD.PAYMENT_RECEIPT_ID "+  
        " INNER  JOIN PAYMENT_RECEIPT_TERMS PRC ON PRCD.PAYMENT_RECEIPT_DETAIL_ID = PRC.PAYMENT_RECEIPT_DETAIL_ID "+
        " INNER  JOIN PAYMENT_SCHEDULE PS ON PRC.PAYMENT_SCHEDULE_ID=PS.PAYMENT_SCHEDULE_ID " + 
        
        "LEFT JOIN payment_type pt ON pt.payment_type_id = ps.payment_type_id "+
        " LEFT JOIN contract cnt ON cnt.contract_id = ps.contract_id " +       
        " LEFT JOIN contract_unit cnt_unit ON cnt_unit.contract_id = cnt.contract_id " +
        " LEFT JOIN unit unt ON unt.unit_id = cnt_unit.unit_id " +
        
        " LEFT JOIN auction_unit auctionUnit ON auctionUnit.auction_unit_id = ps.auction_unit_id " +
		" LEFT JOIN unit aunit ON aunit.unit_id = auctionUnit.unit_id " +
        
        " LEFT JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID  "+
        " LEFT JOIN PERSON P ON PRCT.PAID_BY=P.PERSON_ID "+
        " LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID "+
        " LEFT JOIN REQUEST RQST ON RQST.REQUEST_ID = PS.REQUEST_ID "+
		" LEFT JOIN PAYMENT_SCHEDULE OLD_PS ON OLD_PS.PAYMENT_SCHEDULE_ID = PS.OLD_PAYMENT_SCHEDULE_ID "+
		" LEFT JOIN PAYMENT_RECEIPT_TERMS OLD_PRT ON OLD_PRT.PAYMENT_SCHEDULE_ID = OLD_PS.PAYMENT_SCHEDULE_ID "+
		" LEFT JOIN PAYMENT_RECEIPT_DETAIL OLD_PRCD ON OLD_PRCD.PAYMENT_RECEIPT_DETAIL_ID = OLD_PRT.PAYMENT_RECEIPT_DETAIL_ID "+
		" LEFT JOIN BANK OLD_BANK ON OLD_BANK.BANK_ID = OLD_PRCD.BANK_ID "+
		" left join property_unit_view PUV ON  (unt.UNIT_ID = puv.UNIT_ID OR aunit.UNIT_ID = puv.UNIT_ID)";
        		///////////////////////////////
		                
				String condition="";
				
//						if(criteria.getContractId().length() >0)
//								condition+= " AND CNTRT.CONTRACT_ID = '" + criteria.getContractId() +"'"	;

//							if(criteria.getPaymentScheduleId() .length() >0)
//								condition+= " AND PS.PAYMENT_SCHEDULE_ID = '" + criteria.getPaymentScheduleId() +"'"	;
							
							//Here receipt_id will be present in receipt_number
							if(criteria.getReceiptNumber().length() >0)
								condition+=" AND PRCT.PAYMENT_RECEIPT_ID =  " + criteria.getReceiptNumber() ;
							else if(criteria.getPaymentScheduleId() .length() >0)
							     condition+=" AND PRCT.PAYMENT_RECEIPT_ID =  " + 
							 " ( " +
							 "   SELECT PR.PAYMENT_RECEIPT_ID FROM PAYMENT_SCHEDULE PS INNER JOIN PAYMENT_RECEIPT_TERMS PRT " +
							 "   ON PRT.PAYMENT_SCHEDULE_ID = PS.PAYMENT_SCHEDULE_ID "+
							 "   INNER JOIN PAYMENT_RECEIPT_DETAIL PRD ON PRD.PAYMENT_RECEIPT_DETAIL_ID = PRT.PAYMENT_RECEIPT_DETAIL_ID " + 
							 "   INNER JOIN PAYMENT_RECEIPT PR ON PR.PAYMENT_RECEIPT_ID = PRD.PAYMENT_RECEIPT_ID "+
							 "   WHERE PS.PAYMENT_SCHEDULE_ID= "+ criteria.getPaymentScheduleId() +
							 " )";
//							
						if(condition.length() > 0)
				        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY  PRCD.METHOD_REF_DATE";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PAYMENT_RECEIPT_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        
		query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/

				"  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
				"  '" + ReportMessageResource.getPropertyEn("unit.unitDescription") + "' \"lblUnitDescEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.customerName") + "' \"lblCustomerNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("attachment.docDesc") + "' \"lblDescriptionEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.period") + "' \"lblPeriodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.receiptDate") + "' \"lblReceiptDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblReceiptTotalAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.amountCol") + "' \"lblAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.chequeNumberORTransferNumber") + "' \"lblChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.costCenter") + "' \"lblCostCenterEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.grpCustNo") + "' \"lblCustomerNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentNumber") + "' \"lblPaymentNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cheque.amount") + "' \"lblChequeAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldBankName") + "' \"lblOldBankEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeNumber") + "' \"lblOldChequeNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeDate") + "' \"oldChequeDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.paymentReceipt.fieldName.oldChequeAmount") + "' \"lblOldChequeAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("applicationDetails.ownershipType") + "' \"lblOwnershipTypeEn\",\n" +
		        
		        
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
				"  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
				"  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.customerName") + "' \"lblCustomerNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("attachment.docDesc") + "' \"lblDescriptionAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.period") + "' \"lblPeriodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.receiptDate") + "' \"lblReceiptDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantStatement.totalReceiptAmount") + "' \"lblReceiptTotalAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.amountCol") + "' \"lblAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.chequeNumberORTransferNumber") + "' \"lblChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.oldChequeDate") + "' \"lblChequeDateAr\",\n" +
//		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPayMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.costCenter") + "' \"lblCostCenterAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.grpCustNo") + "' \"lblCustomerNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentNumber") + "' \"lblPaymentNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cheque.amount") + "' \"lblChequeAmountArss\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldBankName") + "' \"lblOldBankAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeNumber") + "' \"lblOldChequeNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeDate") + "' \"oldChequeDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.paymentReceipt.fieldName.oldChequeAmount") + "' \"lblOldChequeAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("applicationDetails.ownershipType") + "' \"lblOwnershipTypeAr\",\n" +

		        

		        /*******************************                           Mandatory Info             *********************************/	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.paymentReceipt.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.paymentReceipt.rpt") + "' \"lblReportNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn(criteria.getReportProcedureNameEn())  + "' \"lblProcedureEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr(criteria.getReportProcedureNameAr())  + "' \"lblProcedureAr\",\n" +
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 
		        
		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PAYMENT_RECEIPT_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
