package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.PropertyTenantsListReportCriteria;
import com.avanza.pims.report.criteria.PropertyUnitListReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PropertyTenantsListReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		PropertyTenantsListReportCriteria criteria = (PropertyTenantsListReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		///////////////////////////
		
		String query = "SELECT " +
		" CNTRT.CONTRACT_ID ,"+
		" U.UNIT_TYPE_ID ,"+
		" PROP.PROPERTY_NUMBER \"propertyNumber\" , " +
        " PROP.COMMERCIAL_NAME  \"propertyName\" , " +
        
//        -- Property Type 
        
        " DD_PT.DATA_DESC_EN  \"propertyTypeEn\" , " +
        " DD_PT.DATA_DESC_AR  \"propertyTypeAr\" , " +
        
//        -- Addresss
        " CI.ADDRESS1 || ' ' || CI.ADDRESS2 \"address\" , " +
        " RGN_AREA.DESCRIPTION_AR \"emirate\" , " +
        " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  \"tenantName\", " +
        " CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"startDate\" , " +
		" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"endDate\" , " +
		" nvl(CNTRT.CONTRACT_NUMBER, '---') || ' | ' || nvl(DD_CS.DATA_DESC_AR, '---')  \"contractNumber\"," +
		" SUM(PMT_S.AMOUNT) \"totalRentalIncome\" ,"+
		" (SELECT SUM(PMT_S2.AMOUNT) FROM PAYMENT_SCHEDULE PMT_S2 "+ 
 		"WHERE (PMT_S2.IS_RECEIVED='Y' AND PMT_S2.contract_ID = CNTRT.contract_ID AND STATUS_ID=36006)   ) \"totalPrepaidRent\" ,"+
		" CNTRT.RENT_AMOUNT \"rentAnnual\", "+
		" RGN_COM.DESCRIPTION_AR \"community\", "+
		" U.UNIT_NUMBER \"unitNumber\", "+
		" U.ACCOUNT_NUMBER \"costCenter\" "+

        
		" FROM  " +

		" PROPERTY PROP " +
        " INNER JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
        //" INNER JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID  " +
        " INNER JOIN ((UNIT U LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID) INNER JOIN CONTRACT_UNIT CUNIT ON U.UNIT_ID = CUNIT.UNIT_ID) ON U.FLOOR_ID = F.FLOOR_ID " + 
        //" LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID " +
        //" INNER JOIN CONTRACT CNTRT ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID " +
        " INNER JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = CUNIT.CONTRACT_ID  " +
        " INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
        " INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
        " LEFT JOIN PAYMENT_SCHEDULE PMT_S ON CNTRT.CONTRACT_ID=PMT_S.CONTRACT_ID "+
        " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID " +         
        " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID " +
        " LEFT JOIN REGION RGN_COM ON RGN_COM.REGION_ID = CI.CITY_ID " +
        " LEFT JOIN DOMAIN_DATA DD_CS ON DD_CS.DOMAIN_DATA_ID = CNTRT.STATUS_ID ";
        
                        

				String condition = "";
				
							if(criteria.getTxtContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getTxtContractNumber() +"%')";
							
							if(criteria.getTxtTenantName().length() >0)
								condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTxtTenantName().trim() +"%')";
							if(criteria.getTxtPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getTxtPropertyName() +"%')";						
							if(criteria.getClndrContractStartDateFrom() !=null  && criteria.getClndrContractStartDateTo()!=null)
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrContractStartDateFrom()) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getClndrContractStartDateTo() )+"','dd/MM/yyyy')";
							if(criteria.getClndrContractEndDateFrom() !=null  && criteria.getClndrContractEndDateTo()!=null)
								condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrContractEndDateFrom()) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getClndrContractEndDateTo() )+"','dd/MM/yyyy')";
							if(!criteria.getCboPropertyType().equals("-1"))
								condition+= " AND PROP.TYPE_ID = '" + criteria.getCboPropertyType() +"'"	;
							if(!criteria.getCboPropertyUsage().equals("-1"))
				        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getCboPropertyUsage();
							if(!criteria.getCboUnittype().equals("-1"))
								condition+= " AND U.UNIT_TYPE_ID  =  " + criteria.getCboUnittype();
							if(!criteria.getCboEmirate().equals("-1"))
				        		condition+=" AND CI.STATE_ID= '"+criteria.getCboEmirate()+"'";
							if(criteria.getTxtPropertNumber().length()>0)
				        		condition+=" AND PROP.PROPERTY_NUMBER= '"+criteria.getTxtPropertNumber()+"'";
							if(criteria.getTxtCostCenter().length()>0)
				        		condition+=" AND U.ACCOUNT_NUMBER like '%"+criteria.getTxtCostCenter()+"%'";
							if(criteria.getTxtUnitNumber().length()>0)
				        		condition+=" AND U.UNIT_NUMBER like '%"+criteria.getTxtUnitNumber()+"%'";
							if(!criteria.getCboCommunity().equals("-1"))
								condition+=" AND CI.CITY_ID = '"+criteria.getCboCommunity()+"'";
							if(!criteria.getCboContractStatus().equals("-1"))
								condition+=" AND CNTRT.STATUS_ID = "+criteria.getCboContractStatus()+" ";
					
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				query+="GROUP BY "+
						" CNTRT.contract_ID, "+
						" U.UNIT_TYPE_ID ,"+
						" PROP.PROPERTY_NUMBER  , "+
						" PROP.COMMERCIAL_NAME   , "+
						" DD_PT.DATA_DESC_EN   , "+
						" DD_PT.DATA_DESC_AR  , "+
						" CI.ADDRESS1 || ' ' || CI.ADDRESS2  , "+
						" RGN_AREA.DESCRIPTION_AR  , "+
					    " PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  ,"+
						" CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) , "+
						" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  , "+
						//" CNTRT.CONTRACT_NUMBER || ' | ' || DD_CS.DATA_DESC_AR  ," +
						" NVL (cntrt.contract_number, '---') || ' | '  || NVL (dd_cs.data_desc_ar, '---'), "+
						" CNTRT.RENT_AMOUNT, " + 
						" RGN_COM.DESCRIPTION_AR,  U.UNIT_NUMBER,  U.ACCOUNT_NUMBER "; 
				String sorting =" ORDER BY PROP.PROPERTY_NUMBER , PROP.COMMERCIAL_NAME ";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PROPERTY_TENANTS_LIST;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("property.number") + "' \"lblPropertyNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("property.type") + "' \"lblPropertyTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.Emirate") + "' \"lblEmirateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblAddressEn\",\n" +		        		        

		        "  '" + ReportMessageResource.getPropertyEn("tenants.name") + "' \"lblTenantNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.fromDate") + "' \"lblFromDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.toDate") + "' \"lblToDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.contractNumber") + "' \"lblContractNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.TotalContractValue") + "' \"lblContractValueEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.rentalIncome") + "' \"lblRentalIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.prepaidRent") + "' \"lblPrepaidRentEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.unit.annualRent") + "' \"lblRentAnnualEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblTotalRentalIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblTotalPrepaidIncomeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.total") + "' \"lblTotalEn\",\n" +
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("property.number") + "' \"lblPropertyNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("property.type") + "' \"lblPropertyTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.Emirate") + "' \"lblEmirateAr\",\n" +
		   		"  '" + ReportMessageResource.getPropertyAr("tenants.name") + "' \"lblTenantNameAr\",\n" +
		   		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblAddressAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.fromDate") + "' \"lblFromDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.toDate") + "' \"lblToDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.contractNumber") + "' \"lblContractNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.TotalContractValue") + "' \"lblContractValueAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.rentalIncome") + "' \"lblRentalIncomeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.prepaidRent") + "' \"lblPrepaidRentAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.unit.annualRent") + "' \"lblRentAnnualAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblTotalRentalIncomeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblTotalPrepaidIncomeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.total") + "' \"lblTotalAr\",\n" +
		               

		     

		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.propertyTenantsListReport.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.propertyTenantsListReport.rpt") + "' \"lblReportNameAr\" , \n" + 
	             "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
        System.out.print("222-------------" + query);
		dataDef = ReportConstant.DataDef.PROPERTY_TENANTS_LIST_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
