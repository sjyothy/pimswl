package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.TenantDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantDetailsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		TenantDetailsReportCriteria criteria = (TenantDetailsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		//////////////tenant details//////////////////
		String query=" SELECT DISTINCT" +
				" PRSN.PERSON_ID \"TenantIdNum\", "+
						" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME ||' ' ||PRSN.LAST_NAME \"tenantName\",\n"+
				        " PRSN.COMPANY_NAME ,\n"+
				        " PRSN.NATIONALITY_ID \"nationalId\","+
//                        -- Nationality 
                        " RGN.DESCRIPTION_AR \"nationality\",\n"+
//                        -- Marital Status
                        " DD2.DATA_DESC_EN \"maritalStatusEn\",\n"+
                        " DD2.DATA_DESC_AR \"maritalStatusAr\",\n"+
                        " CAST(to_char(PRSN.DATE_OF_BIRTH,'dd/mm/yyyy') AS VARCHAR(10) ) \"dateOfBirth\",\n"+ 
                        
//                        "( case when (nvl("+is_company+",0)= 0) then " +'Individual'+" else" +'Company'+" end ) as Type"+
                        
                        " PRSN.SOCIAL_SEC_NUMBER \"tenantNumber\", \n"+
                        " PRSN.PERSON_ID \"tenantId\",\n"+
                        " PRSN.IS_COMPANY \"tenantTypeEn\",\n"+
                        " PRSN.PASSPORT_NUMBER \"passportNumber\",\n"+
                        " CAST(to_char(PRSN.PASSPORT_ISSUE_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"passportIssueDate\",\n"+
                        " CAST(to_char(PRSN.PASSPORT_EXPIRY_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"passportExpiryDate\",\n"+
                        " PRSN.PASSPORT_ISSUE_PLACE_ID ,\n"+
                        " RGN_PPIP.DESCRIPTION_AR \"passportSourceEn\",\n"+
                        " RGN_PPIP.DESCRIPTION_AR \"passportSourceAr\",\n"+
                        " PRSN.RESIDENSE_VISA_NUMBER \"visaNumber\",\n"+
                        " CAST(to_char(PRSN.RESIDENSE_VIDA_EXP_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"visaExpiryDate\",\n"+ 
                        
                        " PRSN.DESIGNATION \"profession\",\n"+
                        " PRSN.WORKING_COMPANY \"workplace\",\n"+
                        
                        
                        " PRSN.LICENSE_NUMBER \"companyLicenseNumber\",\n"+
                        " DD4.DATA_DESC_EN \"licenseSourceEn\",\n"+
                        " DD4.DATA_DESC_AR \"licenseSourceAr\",\n"+
                        " PRSN.LICENSE_SOURCE ,\n"+
                        " CAST(to_char(PRSN.LICENSE_ISSUE_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"licenseIssueDate\",\n"+
                        " CAST(to_char(PRSN.LICENSE_EXPIRY_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"licenseExpiryDate\",\n"+
                                              
//                        -- Country
                        " RGN_CNTRY.DESCRIPTION_AR \"country\",\n"+
                        
//                        -- City
                        " RGN_CITY.DESCRIPTION_AR \"city\",\n"+
                        
//                        -- Area
                        " RGN_AREA.DESCRIPTION_AR \"area\",\n"+
				                        
				        " CI.ADDRESS1 \"Address1\",\n"+
				        " CI.ADDRESS2 \"Address2\",\n"+
                        " CI.STREET \"street\",\n"+
                        " CI.OFFICE_PHONE \"officePhone1\", \n"+
                        " CI.HOME_PHONE \"residenceNumbe\", \n"+
                        " CI.FAX \"fax\",\n"+
                        " PRSN.CELL_NUMBER \"cellNumber1\" ,\n"+
                        " PRSN.SEC_CELL_NUMBER \"cellNumber2\" ,\n"+
                        
                        " CI.POST_CODE \"PO_Box\",\n"+
                        " CI.EMAIL \"email\", "+
                        " DD_TT.DATA_DESC_EN \"tenantTypeEn\",\n"+
				        " DD_TT.DATA_DESC_AR \"tenantTypeAr\",\n"+
				        " PRSN.PERSON_TYPE \n"+
                        
        " FROM PERSON PRSN \n"+ 
        " LEFT JOIN PERSON_CONTACT_INFO PCI ON PCI.PERSON_ID = PRSN.PERSON_ID \n"+
        " LEFT JOIN CONTACT_INFO CI ON PCI.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+
                     
        " LEFT JOIN REGION RGN ON RGN.REGION_ID = PRSN.NATIONALITY_ID \n"+ 
        " LEFT JOIN CONTRACT CNTRT ON CNTRT.TENANT_ID = PRSN.PERSON_ID \n"+
        " LEFT JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID \n"+
        " LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID \n"+
        " LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID \n"+
        " LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID \n"+ 
        " LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID \n"+
        " LEFT JOIN REGION RGN_PPIP ON RGN_PPIP.REGION_ID = PRSN.PASSPORT_ISSUE_PLACE_ID \n"+
        
	    " LEFT JOIN DOMAIN_DATA DD2 ON PRSN.MARITIAL_STATUS_ID = DD2.DOMAIN_DATA_ID \n"+
	    " LEFT JOIN DOMAIN_DATA DD4 ON PRSN.LICENSE_SOURCE = DD4.DOMAIN_DATA_ID \n"+
                        
         " LEFT JOIN REGION RGN_CNTRY ON RGN_CNTRY.REGION_ID = CI.COUNTRY_ID \n"+ 
         " LEFT JOIN REGION RGN_CITY ON RGN_CITY.REGION_ID = CI.CITY_ID  \n"+
         " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID \n"+
 		" LEFT JOIN DOMAIN_DATA DD_TT ON PRSN.PERSON_TYPE = DD_TT.DOMAIN_DATA_ID " ;
		
		String condition="";
		if(criteria.getContractNumber() != null && criteria.getContractNumber().length() >0)
			condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getContractNumber() +"%')";
	
//		if(criteria.getTenantId() != null && criteria.getTenantId().length() >0)
//			condition+=" AND PRSN.PERSON_ID = " + criteria.getTenantId() ;
		if(criteria.getNationalId() != null && criteria.getNationalId().length() >0)
			condition+=" AND PRSN.NATIONALITY_ID = " + criteria.getNationalId() ;
		if(criteria.getTenantNo().length() >0)
			condition+=" AND PRSN.SOCIAL_SEC_NUMBER = '" + criteria.getTenantNo()+"'" ;
		if(!criteria.getTenantType().equals("-1"))
		{
			String tenantTypeCriteria;
			if(criteria.getTenantType().equals("26002"))
				tenantTypeCriteria="0";
			else 
				tenantTypeCriteria="1";
			condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
		}	
		
		if(criteria.getTenantName().length() > 0)
			condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
			/*condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
							" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
							" ) " ;*/
		if(!criteria.getNationality().equals("-1"))
			condition+=" AND RGN.REGION_ID = '" + criteria.getNationality()+"'";
		
		if(criteria.getPassportNumber().length()>0)
			condition+=" AND lower(PRSN.PASSPORT_NUMBER) like  lower('%" + criteria.getPassportNumber()+"%')";
		
		if(criteria.getVisaNumber().length() >0)
			condition+=" AND lower(PRSN.RESIDENSE_VISA_NUMBER) like  lower('%" + criteria.getVisaNumber() +"%')";
		
		if(criteria.getLicenseNumber() != null && criteria.getLicenseNumber().length() >0)
			condition+=" AND lower(PRSN.LICENSE_NUMBER) like  lower('%" + criteria.getLicenseNumber() +"%')";
		
		if(criteria.getMobileNumber().length() >0)
			condition+=" AND PRSN.CELL_NUMBER like  '%" + criteria.getMobileNumber() +"%'";
		
		if(criteria.getPropertyName().length() >0)
			condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyName() +"%')";
		
		if(criteria.getContractStartDateFrom()!=null  && criteria.getContractStartDateTo()!=null)								
			condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getContractStartDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getContractStartDateTo())+"', 'dd/MM/yyyy')";
		
		if(criteria.getExpiryDateFrom()!=null  && criteria.getExpiryDateTo()!=null)
			condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getExpiryDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getExpiryDateTo())+"', 'dd/MM/yyyy')";
		
		if(!criteria.getLicenseSource().equals("-1"))
			condition+=" AND PRSN.LICENSE_SOURCE=" + criteria.getLicenseSource();

		if(criteria.getPhoneNumber().length()>0)
			condition+=" AND CI.HOME_PHONE like  '%" + criteria.getPhoneNumber() +"%'";

		if(!criteria.getPropertyOwnershipType().equals("-1"))
			condition+= " AND PROP.OWNERSHIP_TYPE_ID = '" + criteria.getPropertyOwnershipType() +"'"	;
//		if(criteria.getTenantId().length()>0)
//			condition+= " AND PRSN.PERSON_ID =" + criteria.getTenantId();
		
		
        
       if(condition.length() > 0)
		      	query+="WHERE 1=1 \n"+ condition;

//				String sorting =" ORDER BY PRSN.FIRST_NAME, PRSN.MIDDLE_NAME, PRSN.LAST_NAME";
							
//				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.TENANT_DETAILS_REPORT_EXT;        
		reportQueryMap.put(dataDef, query+"");
		
		String query2 =  "SELECT "+
		  
		 " CR.Tenant_id \"refTenantId\","+
		 " dd_title.data_desc_ar \"refTitle\","+
		 " CR.first_name || ' ' || cr.middle_name || ' ' || cr.last_name  \"refName\","+
		 " cr.relationship_id \"refRelationship\","+
		 " cr.remarks \"refRemarks\","+
		 " cr.nationality_id \"refNationality\","+
		 " cr.cell_number \"refCellNumber\","+
		 " ci.street \"refStreet\","+
		 " ci.post_code \"retPostalCode\","+
		 " rgn_state.description_ar \"refState\","+
		 " rgn_country.description_ar\"refCountry\"," +
		 " rgn_city.description_ar \"refCity\","+
		 " ci.home_phone \"refHomePhone\","+
		 " ci.office_phone \"refOfficePhone\","+
		 " ci.fax \"refFax\", "+
		 " ci.email \"refEmail\","+
		 " ci.mobile_number \"refMobileNumber\","+
		 " ci.DESIGNATION \"refDesignation\""+

		 " FROM PERSON PRSN "+
		 " LEFT JOIN CONTACT_REFERENCE CR ON CR.tenant_id = prsn.person_id "+
		 " LEFT JOIN CONTACT_INFO CI ON CI.CONTACT_INFO_ID = CR.CONTACT_INFO_ID "+
		 " LEFT JOIN REGION rgn_city ON rgn_city.region_id = ci.city_id"+
		 " LEFT JOIN REGION rgn_country ON rgn_country.region_id = ci.country_id"+
		 " LEFT JOIN REGION rgn_state ON rgn_state.region_id = ci.state_id"+
		 " LEFT JOIN DOMAIN_DATA dd_title ON dd_title.domain_data_id = cr.title_id";
		
		System.out.print("------------- Query2:" + query2);
		String dataDef2 = ReportConstant.DataDef.TENANT_DETAILS_REPORT + "#ContactReference";        
		reportQueryMap.put(dataDef2, query2+"");
		
		
		
		  // /////////////LABELS////////////////////////////////
        
		
		query = "SELECT \n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantNo") + "' \"lblTenantNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.nationalId") + "' \"lblNationalIdEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.socialSecNo") + "' \"lblIdNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.nationality") + "' \"lblNationalityEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.maritalstatus") + "' \"lblMaritalStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.type") + "' \"lblTenantTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.dateofbirth") + "' \"lblDateOfBirthEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("customer.passportNumber") + "' \"lblPassportNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.passport.issueplace") + "' \"lblPassportSourceEn\",\n" +
  		        "  '" + ReportMessageResource.getPropertyEn("commons.passport.issuedate") + "' \"lblPassportIssueDateEn\",\n" +	
  		        "  '" + ReportMessageResource.getPropertyEn("commons.passport.expirydate") + "' \"lblPassportExpiryDateEn\",\n" +  		        
		        "  '" + ReportMessageResource.getPropertyEn("tenants.visa.number") + "' \"lblVisaNumberEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.source") + "' \"lblVisaSourceEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("contract.issueDate") + "' \"lblVisaIssueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.visa.expirydate") + "' \"lblVisaExpiryDateEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("inquiryApplication.profession") + "' \"lblProfessionEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("tenants.workplace") + "' \"lblWorkplaceEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("tenants.license.licenseno") + "' \"lblCompanyLicenseNumberEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("ContractorSearch.licenseSource") + "' \"lblLicenseSourceEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.company") + "' \"lblCompanyEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.individual") + "' \"lblIndividualEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.id") + "' \"lblTenantIdEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("tenants.license.issuedate") + "' \"lblLicenseIssueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.license.expirydate") + "' \"lblLicenseExpiryDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.country") + "' \"lblCountryEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("ContractorSearch.city") + "' \"lblCityEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.area") + "' \"lblAreaEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.street") + "' \"lblStreetEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.officephone") + "' \"lblOfficePhone_1En\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.homephone") + "' \"lblResidenceNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.fax") + "' \"lblFaxEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.address1") + "' \"lblAddress1En\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contact.address2") + "' \"lblAddress2En\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiryApplication.poBox") + "' \"lblPO_BoxEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("studyManage.emailCol") + "' \"lblEmailEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.contactInfo") + "' \"lblContactInfoEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.personalInfo") + "' \"lblPersonalInfoEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.cellnumber1") + "' \"lblCellNumber1En\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.cellnumber2") + "' \"lblCellNumber2En\",\n" +
		        
		        
//		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblAddressInfoEn\",\n" +

		     		  
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		     
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantNo") + "' \"lblTenantNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.socialSecNo") + "' \"lblIdNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.nationality") + "' \"lblNationalityAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.nationalId") + "' \"lblNationalIdAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.maritalstatus") + "' \"lblMaritalStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.type") + "' \"lblTenantTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.dateofbirth") + "' \"lblDateOfBirthAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("customer.passportNumber") + "' \"lblPassportNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.passport.issueplace") + "' \"lblPassportSourceAr\",\n" +
  		        "  '" + ReportMessageResource.getPropertyAr("commons.passport.issuedate") + "' \"lblPassportIssueDateAr\",\n" +	
  		        "  '" + ReportMessageResource.getPropertyAr("commons.passport.expirydate") + "' \"lblPassportExpiryDateAr\",\n" +  		        
		        "  '" + ReportMessageResource.getPropertyAr("tenants.visa.number") + "' \"lblVisaNumberAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("commons.source") + "' \"lblVisaSourceAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("contract.issueDate") + "' \"lblVisaIssueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.visa.expirydate") + "' \"lblVisaExpiryDateAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("inquiryApplication.profession") + "' \"lblProfessionAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("tenants.workplace") + "' \"lblWorkplaceAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("tenants.license.licenseno") + "' \"lblCompanyLicenseNumberAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("ContractorSearch.licenseSource") + "' \"lblLicenseSourceAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.company") + "' \"lblCompanyAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.individual") + "' \"lblIndividualAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.id") + "' \"lblTenantIdAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("tenants.license.issuedate") + "' \"lblLicenseIssueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.license.expirydate") + "' \"lblLicenseExpiryDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.country") + "' \"lblCountryAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("ContractorSearch.city") + "' \"lblCityAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.area") + "' \"lblAreaAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.street") + "' \"lblStreetAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.officephone") + "' \"lblOfficePhone1Ar\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.homephone") + "' \"lblResidenceNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.fax") + "' \"lblFaxAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.address1") + "' \"lblAddress1Ar\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.address2") + "' \"lblAddress2Ar\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiryApplication.poBox") + "' \"lblPO_BoxAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.email") + "' \"lblEmailAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.contactInfo") + "' \"lblContactInfoAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.personalInfo") + "' \"lblPersonalInfoAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.cellnumber1") + "' \"lblCellNumber1Ar\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.cellnumber2") + "' \"lblCellNumber2Ar\",\n" +

		        

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantDetailsReport.rpt") + "' \"lblTenantDetailsReportEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantDetailsReport.rpt") + "' \"lblTenantDetailsReportAr\" ,\n" + 
		         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.TENANT_DETAILS_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
