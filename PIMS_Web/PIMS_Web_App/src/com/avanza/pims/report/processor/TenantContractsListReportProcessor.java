package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.TenantContractsListCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class TenantContractsListReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
 		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 		TenantContractsListCriteria criteria = (TenantContractsListCriteria) reportCriteria;


		String query = "SELECT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
//							"CAST( CNTRT.CONTRACT_DATE AS VARCHAR(10) ) \" contractDate \" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractStartDate\" , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							" CNTRT.RENT_AMOUNT \"contractRentValue\" , " +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\" , " +
							"U.UNIT_NUMBER || ' | ' || U.ACCOUNT_NUMBER || ' | ' || RGN_COM.DESCRIPTION_AR \"unitNumber\" , " +
							"DD.DATA_DESC_EN ," +
							"DD.DATA_DESC_AR ," +
							" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME||' ' || PRSN.LAST_NAME  \"tenantName\", " +
							"PRSN.CELL_NUMBER \"cellNumber\"," +
							
							"CI.HOME_PHONE \"residencePhone\" , " +
							"CI.OFFICE_PHONE \"officePhone\" , " +
							"CI.POST_CODE \"pOBox\" , " +
							"CI.EMAIL \"email\" , " +
							" CI.ADDRESS1 ||' '|| CI.ADDRESS2  \"address\", " +		
							"DD2.DATA_DESC_EN  \"contractTypeEn\" , " +
							"DD2.DATA_DESC_AR  \"contractTypeAr\", " +
							"DD3.DATA_DESC_EN  \"contractStatusEn\" , " +
							"DD3.DATA_DESC_AR  \"contractStatusAr\" ," +
							" PRSN.PERSON_ID \"tenantNumber\",\n"+
					        " PRSN.IS_COMPANY \"tenantTypeEn\",\n"+
					        " PRSN.IS_COMPANY \"tenantTypeAr\",\n"+
					        " PRSN.PERSON_TYPE  \n"+
							//" U.ACCOUNT_NUMBER \"costCenter\" , " + 
							//" RGN_COM.DESCRIPTION_AR \"community\" "+        
							
							" FROM " +
							" CONTRACT CNTRT " +
							" INNER JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" INNER JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" INNER JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" INNER JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							//" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" INNER JOIN (CONTACT_INFO CI  LEFT JOIN REGION RGN_COM ON RGN_COM.REGION_ID = CI.CITY_ID) ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							//" INNER JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
							//" INNER JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							//" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
							//" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
							//" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON " +
							//" PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
							" INNER JOIN DOMAIN_DATA DD2 ON CNTRT.CONTRACT_TYPE_ID = DD2.DOMAIN_DATA_ID " +
							" INNER JOIN DOMAIN_DATA DD3 ON CNTRT.STATUS_ID = DD3.DOMAIN_DATA_ID " ;
	

	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
/*							condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
											" ) " ;*/
							
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyName() +"%')";
							if(criteria.getFromContractDate()!=null  && criteria.getToContractDate()!=null)								
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDate() )+"', 'dd/MM/yyyy')";
							if(criteria.getFromContractExpDate()!=null  && criteria.getToContractExpDate()!=null)
								condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractExpDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractExpDate() )+"', 'dd/MM/yyyy')";
							if(!criteria.getPropertyOwnershipType().equals("-1"))
					        	condition+="AND PROP.OWNERSHIP_TYPE_ID= '" +criteria.getPropertyOwnershipType()+"'";
							if(criteria.getTenantId().length() >0)
								condition+=" AND PRSN.PERSON_ID = " + criteria.getTenantId() ;
							if(!criteria.getTenantType().equals("-1"))
							{
								String tenantTypeCriteria;
								if(criteria.getTenantType().equals("26002"))
									tenantTypeCriteria="0";
								else 
									tenantTypeCriteria="1";
								condition+=" AND PRSN.IS_COMPANY = " + tenantTypeCriteria;
							}	
							
							if(!criteria.getCommunity().equals("-1"))
								condition+=" AND RGN_COM.REGION_ID = " + criteria.getCommunity() +" ";
							if(criteria.getCostCenter().length() >0)
								condition+=" AND lower(U.ACCOUNT_NUMBER) like lower('%" + criteria.getCostCenter() +"%')";
							if(criteria.getUnitNumber().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER) like lower('%" + criteria.getUnitNumber() +"%')";
							
							if(condition.length() > 0)
					        	query+="where 1=1 \n"+ condition;
		

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.TENANT_CONTRACTS_LIST;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 
   /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.startDate") + "' \"lblcontractStartDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblcontractEndDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.officephone") + "' \"lblOfficePhoneEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.homephone") + "' \"lblResidenceNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("applicationDetails.cell") + "' \"lblCellNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.addressLineOne") + "' \"lblAddressEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.postcode") + "' \"lblPOBoxEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contact.email") + "' \"lblEmailEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("tenants.id") + "' \"lblTenantNumberEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("tenants.type") + "' \"lblTenantTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("botContractDetails.RentAmount") + "' \"lblContractRentValueEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("ContractorSearch.contractStatus") + "' \"lblContractStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.contractType") + "' \"lblContractTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.company") + "' \"lblCompanyEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("tenants.tpye.individual") + "' \"lblIndividualEn\",\n" +
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.startDate") + "' \"lblcontractStartDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblcontractEndDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("unit.number") + "' \"lblUnitNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.officephone") + "' \"lblOfficePhoneAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.homephone") + "' \"lblResidenceNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("applicationDetails.cell") + "' \"lblCellNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.addressLineOne") + "' \"lblAddressAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contact.postcode") + "' \"lblPOBoxAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.id") + "' \"lblTenantNumberAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("tenants.type") + "' \"lblTenantTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contact.email") + "' \"lblEmailAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("botContractDetails.RentAmount") + "' \"lblContractRentValueAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("ContractorSearch.contractStatus") + "' \"lblContractStatusAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.contractType") + "' \"lblContractTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.company") + "' \"lblCompanyAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("tenants.tpye.individual") + "' \"lblIndividualAr\",\n" +


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.tenantContractsList.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.tenantContractsList.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.TENANT_CONTRACTS_LIST_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
