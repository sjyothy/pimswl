package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.UnitInquiryReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.util.DateUtil;

public class UnitInquiryReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		
		UnitInquiryReportCriteria criteria = (UnitInquiryReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		///////////////////////////
		
		String query = "SELECT " +
		" PROP.PROPERTY_NUMBER , " +
        " PROP.COMMERCIAL_NAME  \"propertyName\" , " +
        
//        -- Property Type 
        
        " DD_PT.DATA_DESC_EN , " +
        " DD_PT.DATA_DESC_AR   , " +
        
//        -- Addresss
        " CI.ADDRESS1 || ' ' || CI.ADDRESS2  , " +
        " RGN_AREA.DESCRIPTION_AR , " +
        " F.FLOOR_ID  , " +
        " F.FLOOR_NAME , " +
        " F.FLOOR_NUMBER \"floorNumber\" , " +
        " U.UNIT_NUMBER \"unitNumber\" , " +
        " DD_UT.DATA_DESC_EN \"unitTypeEn\" , " +
        " DD_UT.DATA_DESC_AR  \"unitTypeAr\" , " +
        " DD_UUT.DATA_DESC_ER  \"usageEn\" , " +
        " DD_UUT.DATA_DESC_AR  \"usageAr\" , " +
        " DD_US.DATA_DESC_EN \"unitSideEn\" , " +
        " DD_US.DATA_DESC_AR  \"unitSideAr\" ,  " +
        " DD_UST.DATA_DESC_EN  \"unitStatusEn\" ,  " +
        " DD_UST.DATA_DESC_AR  \"unitStatusAr\" ,  " +
        " DD_UD.DATA_DESC_EN  \"unitDescriptionEn\" ,  " +
        " DD_UD.DATA_DESC_AR  \"unitDescriptionAr\" ,  " +
        " U.NO_OF_BED  \"noOfBedRooms\" " +
        " U.NO_OF_LIVING  \"noOfLivingRooms\" " +
        " U.NO_OF_BATH  \"noOfBathRooms\" " +
        " U.RENT_VALUE  \"rentValue\" " +
        " U.UNIT_EW_UTILITY_NO  \"ewUtilityNo\" " +
        " U.UNIT_REMARKS  \"remarks\" " +
        
" FROM  " +

" PROPERTY PROP " +
        " INNER JOIN FLOOR F ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
        " INNER JOIN UNIT U ON U.FLOOR_ID = F.FLOOR_ID  " +
        " LEFT JOIN CONTRACT CNTRT ON CNTRT.PROPERTY_ID = PROP.PROPERTY_ID " +
        
        " INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +

        " LEFT JOIN DOMAIN_DATA DD_PT ON PROP.TYPE_ID = DD_PT.DOMAIN_DATA_ID " +

        " LEFT JOIN DOMAIN_DATA DD_UT ON U.UNIT_TYPE_ID = DD_UT.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_UUT ON U.USAGE_TYPE_ID = DD_UUT.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_US ON U.UNIT_SIDE_TYPE_ID = DD_US.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_UST ON U.STATUS_ID = DD_UST.DOMAIN_DATA_ID " +
        " LEFT JOIN DOMAIN_DATA DD_UD ON UNIT_DESC = DD_UD.DOMAIN_DATA_ID " +
        " LEFT JOIN REGION RGN_AREA ON RGN_AREA.REGION_ID = CI.STATE_ID " ;
                    

				String condition = "";
				
							if(criteria.getTxtContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like lower('%" + criteria.getTxtContractNumber() +"%')";
							
							if(criteria.getTxtTenantName().length() >0)
								condition+= " AND (PRSN.FIRST_NAME like '%" + criteria.getTxtTenantName() +"%'" +
													" OR PRSN.MIDDLE_NAME like  '%" + criteria.getTxtTenantName() +"%'"+
													" OR PRSN.LAST_NAME like  '%" + criteria.getTxtTenantName() +"%'"+
													" ) " ;
							if(criteria.getTxtPropertyName().length() >0)
								condition+=" AND PROP.COMMERCIAL_NAME like  '%" + criteria.getTxtPropertyName() +"%'";
							if(criteria.getTxtUnitNumber().length() >0)
								condition+=" AND U.UNIT_NUMBER  like  '%" + criteria.getTxtUnitNumber() +"%'";
							if(criteria.getClndrFromDate() !=null  && criteria.getCldrToDate()!=null)
								condition += " AND PROP.RECEIVING_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getClndrFromDate() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getCldrToDate() )+"', 'dd/MM/yyyy')";
							if(!criteria.getCboPropertyType().equals("-1"))
								condition+= " AND PROP.TYPE_ID = '" + criteria.getCboPropertyType() +"'"	;
							if(!criteria.getCboPropertyUsage().equals("-1"))
				        		condition+=" AND PROP.USAGE_TYPE_ID="+criteria.getCboPropertyUsage()+"";
							if(!criteria.getCboUnittype().equals("-1"))
								condition+= " AND U.UNIT_TYPE_ID  =  " + criteria.getCboUnittype();
							if(!criteria.getCboEmirate().equals("-1"))
				        		condition+=" AND RGN_AREA.REGION_ID= '"+criteria.getCboEmirate()+"'";
							if(criteria.getTxtFloorNumber().length() >0)
								condition+=" AND F.FLOOR_NUMBER =" + criteria.getTxtFloorNumber() ;
							if(criteria.getTxtLandNumber().length()>0)
				        		condition+=" AND PROP.LAND_NUMBER like '%"+criteria.getTxtLandNumber()+"%'";
							if(criteria.getTxtPropertNumber().length()>0)
					        	condition+=" AND lower(PROP.PROPERTY_NUMBER) like lower('%"+criteria.getTxtPropertNumber()+"%')";
						
					
							if(condition.length() > 0)
					        	query+="WHERE 1=1 \n"+ condition;

				String sorting =" ORDER BY PROP.PROPERTY_NUMBER , PROP.COMMERCIAL_NAME ";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.UNIT_INQUIRY_REPORT_DATADEF;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
 
        /*******************************                           English Labels             *********************************/
		
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.replaceCheque.unitType") + "' \"lblUnitTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentFloorNumber") + "' \"lblFloorNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiry.unitSide") + "' \"lblUnitSideEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.unit.numberOfBedroom") + "' \"lblNoOfBedRoomsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.UnitUsusageType") + "' \"lblUsageEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiry.noOfBathMax") + "' \"lblNoOfBathRoomsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiry.rentvalue") + "' \"lblRentValueEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitStatus") + "' \"lblUnitStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitDescription") + "' \"lblUnitDescriptionEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("inquiry.noOfLivingMax") + "' \"lblNoOfLivingRoomsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.EWUtilityNo") + "' \"lblEwUtilityNoEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.unitRemarks") + "' \"lblRemarksEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.replaceCheque.unitType") + "' \"lblUnitTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentFloorNumber") + "' \"lblFloorNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiry.unitSide") + "' \"lblUnitSideAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.unit.numberOfBedroom") + "' \"lblNoOfBedRoomsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.UnitUsusageType") + "' \"lblUsageAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiry.noOfBathMax") + "' \"lblNoOfBathRoomsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiry.rentvalue") + "' \"lblRentValueAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.unitStatus") + "' \"lblUnitStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescriptionAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("inquiry.noOfLivingMax") + "' \"lblNoOfLivingRoomsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.EWUtilityNo") + "' \"lblEwUtilityNoAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.unitRemarks") + "' \"lblRemarksAr\",\n" +


		        /*******************************                           Log in User             *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.unit.inquiry") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.unit.inquiry") + "' \"lblReportNameAr\" , \n" + 
	             "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
        System.out.print("222-------------" + query);
		dataDef = ReportConstant.DataDef.UNIT_INQUIRY_REPORT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
