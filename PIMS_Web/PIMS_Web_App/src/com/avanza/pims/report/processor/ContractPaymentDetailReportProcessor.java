package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractPaymentDetailReportCriteria;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class ContractPaymentDetailReportProcessor  extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		ContractPaymentDetailReportCriteria criteria = (ContractPaymentDetailReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);		
		String query = "SELECT  CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) ) ," +
							"CAST( CNTRT.CONTRACT_DATE AS VARCHAR(10) ) \" contractDate \" ," +
							"CNTRT.CONTRACT_NUMBER  \"contractNumber\"," +
							"NVL( CNTRT.REMARKS, '---') ," +
							"CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractIssueDate\" , " +
							"CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractEndDate\" , " +
							"CNTRT.RENT_AMOUNT ," +
							"PROP.PROPERTY_NAME ," +
							"NVL(PROP.COMMERCIAL_NAME, '---') \"propertyComercialName\" , " +
							"U.UNIT_NUMBER \"unitNo\" , " +
							"DD.DATA_DESC_EN ," +
							"DD.DATA_DESC_AR ," +
							"CI.ADDRESS1 ," +
							"CI.ADDRESS2 ," +
							" rt.request_type_en \"requestTypeEn\" , "+
							" rt.request_type_ar \"requestTypeAr\" , "+
							"PRSN.FIRST_NAME \"tenantFirstName\" , " +
							"PRSN.MIDDLE_NAME \"tenantMiddleName\" , " +
							"PRSN.LAST_NAME \"tenantLastName\" , " +
							"PRSN.COMPANY_NAME," +
							"PS.PAYMENT_NUMBER || '|' || u.account_number || '|' || prcpt.receipt_number || '|' || DD_CS.DATA_DESC_AR \"paymentNo\" , " +
							"CAST( to_char(PS.PAYMENT_DUE_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDueDate\" , " +
							"PS.PAYMENT_TYPE_ID," +
							"NVL(PYT.DESCRIPTION_EN, '---')  \"paymentTypeEn\" , " +
							"NVL(PYT.DESCRIPTION_AR, '---')  \"paymentTypeAr\" , " +
							//"NVL(PRCD.METHOD_REF_NO, '---') \"chequeNumber\"," +
							" CASE WHEN prcd.method_ref_no IS NOT NULL AND oprcd.method_ref_no IS NOT NULL THEN NVL (prcd.method_ref_no || ' --> ' || oprcd.method_ref_no, '---') WHEN prcd.method_ref_no IS NULL AND oprcd.method_ref_no IS NOT NULL THEN NVL (' --> ' || oprcd.method_ref_no,'---') ELSE NVL (prcd.method_ref_no, '---') END \"chequeNumber\", " +
							"PS.AMOUNT \"paymentAmount\"," +
							"PMT.DESCRIPTION \"paymentMethod\" , " +
							" PS.REQUEST_ID \"requestId\" ,"+
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_EN, '---') end \"chequeBankNameEn\" , " +
							"CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_AR,'---') end \"chequeBankNameAr\" , " +
							"DD2.DATA_DESC_EN  \"paymentStatusEn\" , " +
							"DD2.DATA_DESC_AR  \"paymentStatusAr\" " +
							" FROM " +
							" CONTRACT CNTRT " +
							" LEFT JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" LEFT JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
							" LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID" +
							" LEFT JOIN DOMAIN_DATA DD ON PROP.OWNERSHIP_TYPE_ID = DD.DOMAIN_DATA_ID " +
							" LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" LEFT JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID " +
							" LEFT JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							" left JOIN DOMAIN_DATA DD2 ON PS.STATUS_ID = DD2.DOMAIN_DATA_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC " +
							" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON " +
							" PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS oprc ON oprc.payment_schedule_id = ps.old_payment_schedule_id " + 
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL oprcd ON oprcd.payment_receipt_detail_id = oprc.payment_receipt_detail_id" +
							" LEFT OUTER JOIN PAYMENT_RECEIPT prcpt ON prcpt.PAYMENT_RECEIPT_ID =  prcd.PAYMENT_RECEIPT_ID " +
							" LEFT OUTER JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
							" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
							" left join request reqst on reqst.request_id=ps.request_id "+
							" left join request_type rt ON reqst.request_type_id = rt.request_type_id " +
							" LEFT JOIN DOMAIN_DATA DD_CS ON cntrt.status_id = DD_CS.domain_data_id";
	

	    		String condition="";
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER) like   lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition+= " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
							/*condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
											" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
											" ) " ;*/
							
							if(criteria.getPropertyComercialName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like   lower('%" + criteria.getPropertyComercialName() +"%')";
							if(criteria.getUnitNum().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like   lower('%" + criteria.getUnitNum() +"%')";
							if(criteria.getFromContractDateRich()!=null  && criteria.getToContractDateRich()!=null)								
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() )+"', 'dd/MM/yyyy')";
							if(criteria.getFromContractExpDateRich()!=null  && criteria.getToContractExpDateRich()!=null)
								condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() )+"', 'dd/MM/yyyy')";
							if(!criteria.getSelectOnePaymentMethod().equals("-1"))
								condition+= " AND PMT.DESCRIPTION  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getSelectOnePaymentMethod() +"')"	;
							if(criteria.getChequeNum().length()>0)
								condition+=" AND lower(PRCD.METHOD_REF_NO)  like   lower('%" + criteria.getChequeNum() +"%')";
							if(!criteria.getSelectOnePaymentStatus().equals("-1"))
								condition+= " AND DD2.DATA_DESC_EN  =  (select  DATA_DESC_EN from DOMAIN_DATA where DOMAIN_DATA_ID ='" + criteria.getSelectOnePaymentStatus() +"')"	;
						//		condition+= " AND DD2.DATA_DESC_EN  =  '" + criteria.getSelectOnePaymentStatus() +"'"	;
							if(!criteria.getBankName().equals("-1"))
								condition+= " AND BANK.BANK_ID  =  '" + criteria.getBankName() +"'"	;
							if(!criteria.getContractStatus().equals("-1"))
								condition+= " AND CNTRT.STATUS_ID  =  " + criteria.getContractStatus() +""	;
							if(criteria.getCostCenter().length() > 0)
								condition+= " AND lower(U.ACCOUNT_NUMBER) LIKE  lower('%" + criteria.getCostCenter() + "%')"	;
							if(criteria.getReceiptNumber().length() > 0)
								condition+= " AND lower(prcpt.RECEIPT_NUMBER) LIKE  lower('%" + criteria.getReceiptNumber() + "%')"	;
							
							if(condition.length() > 0)
					        	query+=" where 1=1  \n"+ condition;
							String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER ASC, ps.payment_type_id ASC, ps.payment_due_on ASC, ps.payment_number ASC";
							query+=sorting;

		System.out.print("-------------" + query);
        String dataDef = ReportConstant.DataDef.CONTRACT_PAYMENT_DETAIL;        
        reportQueryMap.put(dataDef, query+"");
		
        
        query = "SELECT \n" + 

        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.issueDate") + "' \"lblcontractIssueDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblcontractEndDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyComercialNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("unit.number") + "' \"lblUnitNumEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNoEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contractPayments.relatedTo") + "' \"lblRelatedToEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentAmountCol") + "' \"lblPaymentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.message.paymentStatus") + "' \"lblPaymentStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("common.contract") + "' \"lblContractEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.issueDate") + "' \"lblcontractIssueDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblcontractEndDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyComercialNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("unit.number") + "' \"lblUnitNumAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNoAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentAmountCol") + "' \"lblPaymentAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contractPayments.relatedTo") + "' \"lblRelatedToAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.message.paymentStatus") + "' \"lblPaymentStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("common.contract") + "' \"lblContractAr\",\n" +

		        /*******************************                           Summary Lables           *********************************/	        

		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.contractPaymentDetail.totalPaymentAmount") + "' \"lblTotalPaymentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.contractPaymentDetail.totalPaymentAmount") + "' \"lblTotalPaymentAmountAr\",\n" + // , placed here if login name will be uncommnet


		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.contractPaymentDetail.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.contractPaymentDetail.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.CONTRACT_PAYMENT_DETAIL_LABEL;     
		System.out.print("-------------" + query);
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}


}
