package com.avanza.pims.report.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.RequestTaskActivityCriteria;
import com.avanza.pims.web.WebConstants;

public class RequestTaskActivityProcessor extends AbstractReportProcessor 
{

	DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@SuppressWarnings( "unchecked" )
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		RequestTaskActivityCriteria criteria = ( RequestTaskActivityCriteria ) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		if( 
			criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_RENEW_CONTRACT.toString()  )||
			criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_LEASE_CONTRACT.toString() ) 
		  )  
		{
			executeQueryForLeaseContract(criteria,reportQueryMap);
		}

		else if( 
					criteria.getRequestTypeId().equals( WebConstants.MemsRequestType.INHERITANCE_FILE.toString() ) 
		  	   )  
		{
			executeQueryForInheritanceFile(criteria,reportQueryMap);
		}
		else if( 
				criteria.getRequestTypeId().equals( WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT.toString() ) 
		  	   )
		{
			executeQueryForCollection(criteria,reportQueryMap);
		}
		else if( 
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_MAINTENANCE_REQUEST.toString() ) 
		  	   )
		{
			executeQueryForMaintenanceRequest(criteria,reportQueryMap);
		}
		else if( criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_TERMINATE_CONTRACT .toString()  ))
		{
			executeQueryForTerminateContractRequest (criteria,reportQueryMap);
		}
		else if( criteria.getRequestTypeId().equals(  WebConstants.REQUEST_TYPE_TRANSFER_CONTRACT.toString() ) )
		{
			executeQueryForTransferContract(criteria,reportQueryMap);
		}
		else if( 
				criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_REPLACE_CHEQUE.toString() ) ||
				criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_PAY_BOUNCE_CHEQUE.toString() )
			   )
		{
			executeQueryForReplaceCheque(criteria,reportQueryMap);
		}
		else if( 
				criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_NOL.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_CLEARANCE_LETTER.toString() )
			   )
		{
			executeQueryForPIMS(criteria, reportQueryMap);
		}
		else if( 
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.PAYMENT_REQUEST_ID.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.PAY_VENDOR.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.INVESTMENT_REQUEST_ID.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.REVIEW_CONFIRM_DISBURSEMENTS_ID.toString() ) 
		  	   )
		{
			executeQueryForMEMSPaymentsProcess(criteria,reportQueryMap);			
		}
		else if( 
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.BLOCKING_REQUEST.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.UNBLOCKING_REQUEST.toString() ) ||
				criteria.getRequestTypeId().equals( Constant.MemsRequestType.NOL_REQUEST_ID.toString() ) 
				 
		  	   )
		{
			executeQueryForMemsMiscellaneaousRequests(criteria, reportQueryMap);
		}
        return reportQueryMap;
	}

	private void fillWhereClause( RequestTaskActivityCriteria criteria ,StringBuilder whereClause  )
	{
		if( criteria.getRequestNumber() != null && criteria.getRequestNumber().trim().length() > 0)
        {
			whereClause.append( " AND  lower(\"requestNumber\") like ").append( " '%" ).append( criteria.getRequestNumber().toLowerCase() ).append("%'" );
        }
        if( criteria.getRequestTypeId() != null && ! criteria.getRequestTypeId().equals("-1") )
        {
			whereClause.append( " AND \"requestTypeId\" = ").append( criteria.getRequestTypeId() );
        }
        if( criteria.getFromDate() != null )
        {
			String dateStr = formatterWithOutTime.format((Date) criteria.getFromDate() )+ " 00:00:00";
			whereClause.append( " AND \"requestCreatedOn\" >= to_Date('").append( dateStr ).append("','dd/MM/RRRR hh24:mi:ss')");
        }
        if( criteria.getToDate() != null )
        {
			String dateStr = formatterWithOutTime.format((Date) criteria.getToDate() )+ " 23:59:59";
			whereClause.append( " AND \"requestCreatedOn\" <= to_Date('").append( dateStr ).append("','dd/MM/RRRR hh24:mi:ss')");
        }
	}
	
	private void executeQueryForMEMSPaymentsProcess(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  " +
					 " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", " +
					 " case when RTA.\"sentReview_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , " +
					 "																			 RTA.\"sentReview_C\" ) else -100000 end  \"timeLapseSentForReview\", "+
					 
					 " case when RTA.\"reviewed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\"," +
					 "																		   RTA.\"reviewed_C\" ) else -100000 end  \"timeLapseReviewed\", "+
					 " case when RTA.\"SUSPENDED_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\"," +
					 "																		    RTA.\"SUSPENDED_C\" ) else -100000 end  \"timeLapseSuspended\", "+
					 
					 
					 " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   " +
					 "	 then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else " +
					 
					 " case when RTA.\"rejected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ," +
					 "																		   RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "+
					 
					 " case when RTA.\"financeRejected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , " +
					 "																				  RTA.\"financeRejected_C\" ) else -100000 end  \"timeLapseFinanceRejected\", "+
					 
					 " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\"," +
					 "																			RTA.\"Completed_C\" ) else -100000 end  \"timeLapseComplete\", "+				 
					 "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "RequestTaskMemsPaymentsDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeQueryForMemsMiscellaneaousRequests(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  " +
					 " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", " +
					 " case when RTA.\"sentReview_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,  RTA.\"sentReview_C\" ) else -100000 end  \"timeLapseSentForReview\", "+
					 " case when RTA.\"reviewed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"reviewed_C\" ) else -100000 end  \"timeLapseReviewed\", "+
					 " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   " +
					 "	 then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else " +
					 "		case when RTA.\"rejected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "+ 
					 " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"Completed_C\" ) else -100000 end  \"timeLapseComplete\", "+				 
					 "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
//						 
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "RequestTaskMemsPaymentsDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeQueryForInheritanceFile(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"Completed_C\" )   else   -100000   end  \"timeLapseCompleted\", "
				+ " case when RTA.\"initialLimitationDone_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"initialLimitationDone_C\" ) else   -100000   end  \"timeLapseInitLimitDone\", "
				+ " case when RTA.\"finalLimitationDone_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"finalLimitationDone_C\" ) else   -100000   end  \"timeLapseFinLimitnDone\", "
				+ " case when RTA.\"distributionCompleted_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"distributionCompleted_C\" ) else   -100000   end  \"timeLapseDisComp\", "
				+ " null as \"timeLapseInitLimitReject\", "
				+ " case when RTA.\"researcherAssigned_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"researcherAssigned_C\" ) else   -100000   end  \"timeLapseResAssigned\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
			 append( " AND \"requestTypeId\" in ( ").
			 append(criteria.getRequestTypeId() ).
			 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskInhFileDataDef";        
			reportQueryMap.put(dataDef, query+"");
	}

	private void executeQueryForCollection(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"collectedDistributionReq_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"collectedDistributionReq_C\" )     else   -100000   end  \"field21\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"Completed_C\" )   else   -100000   end  \"timeLapseComplete\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "RequestTaskCollectionDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}
	
	private void executeQueryForMaintenanceRequest (RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"evaluationReq_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"evaluationReq_C\" )     else   -100000   end  \"timeLapseEvalReq\", "
				+ " case when RTA.\"evaluationDone_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"evaluationDone_C\" )     else   -100000   end  \"timeLapseEvalDone\", "
				+ " case when RTA.\"sentReview_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"sentReview_C\" )     else   -100000   end  \"timeLapseSentReview\", "
				+ " case when RTA.\"reviewed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"reviewed_C\" )     else   -100000   end  \"timeLapseReviewed\", "
				+ " case when RTA.\"followup_Req_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"followup_Req_C\" )     else   -100000   end  \"timeLapseFollowupReq\", "
				+ " case when RTA.\"followupDone_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"followupDone_C\" )     else   -100000   end  \"timeLapseFollowupDone\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"Completed_C\" )   else   -100000   end  \"timeLapseCompleted\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId()).
//						 append(",").
//						 append(Constant.REQUEST_TYPE_MAINTENANCE_REQUEST.toString()).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskMaintenanceDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeQueryForTerminateContractRequest (RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"SVProgress_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"SVProgress_C\" )     else   -100000   end  \"timeLapseSVProgress\", "
				+ " case when RTA.\"SVDone_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"SVDone_C\" )     else   -100000   end  \"timeLapseSVDone\", "
				+ " case when RTA.\"sentReview_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"sentReview_C\" )     else   -100000   end  \"timeLapseSentReview\", "
				+ " null as \"timeLapseSVRequired\","
				+ " case when RTA.\"settled_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"settled_C\" )     else   -100000   end  \"timeLapseSettled\", "
				+ " case when RTA.\"collectionRequired_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"collectionRequired_C\" )     else   -100000   end  \"timeLapseCollReq\", "
				+ " case when RTA.\"clIssued_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"clIssued_C\" )     else   -100000   end  \"timeLapseClIssued\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\",RTA.\"Completed_C\" )   else   -100000   end  \"timeLapseCompleted\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(String.valueOf( Constant.REQUEST_TYPE_CANCEL_CONTRACT_ID)).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskTerminateDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}
	
	private void executeQueryForLeaseContract(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"contractPrinted_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"contractPrinted_C\" )     else   -100000   end  \"timeLapseContractPrinted\", "
				+ " case when RTA.\"disclosurePrinted_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"disclosurePrinted_C\" )     else   -100000   end  \"timeLapseDisclosurePrinted\", "
				+ " case when RTA.\"Completed_C\" is not null   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"Completed_C\" ) else "
				+ "		case when RTA.\"paymentCollected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"paymentCollected_C\" ) else -100000 end end  \"timeLapseComplete\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
//						 append(",").
//						 append(String.valueOf( Constant.RENEW_CONTRACT)).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskContractDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeQueryForTransferContract(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"pocCollected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"pocCollected_C\" )     else   -100000   end  \"timeLapsePocCollected\", "
				+ " case when RTA.\"ponCollected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"ponCollected_C\" )     else   -100000   end  \"timeLapsePonCollected\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"Completed_C\" )     else   -100000   end  \"timeLapseCompleted\", "
				+ " case when RTA.\"settled_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"settled_C\" )     else   -100000   end  \"timeLapseSettled\", "
				+ " case when RTA.\"rvverified_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"rvverified_C\" )     else   -100000   end  \"timeLapseRVVerified\", "
				+ " case when RTA.\"contractCreated_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"contractCreated_C\" )     else   -100000   end  \"timeLapseContractCreated\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1  ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(String.valueOf( Constant.REQUEST_TYPE_TRANSFER_CONTRACT)).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskTransferDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}
	
	private void executeQueryForReplaceCheque(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"cwPrinted_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"cwPrinted_C\" )     else   -100000   end  \"timeLapseCwPrinted\", "
				+ " case when RTA.\"ncCollected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"ncCollected_C\" )     else   -100000   end  \"timeLapseNcCollected\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"Completed_C\" )     else   -100000   end  \"timeLapseCompleted\", "
				+ " case when RTA.\"ocReturned_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"ocReturned_C\" )     else   -100000   end  \"timeLapseOcReturned\", "
				+ " case when RTA.\"chequeFromBank_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"chequeFromBank_C\" )     else   -100000   end  \"timeLapseChequeFromBank\", "
				+ " case when RTA.\"contractCreated_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"contractCreated_C\" )     else   -100000   end  \"timeLapseContractCreated\", "
				+ " case when RTA.\"reviewed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"reviewed_C\" )     else   -100000   end  \"timeLapseReviewed\", "
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1 ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
//						 append( "," ).
//						 append(String.valueOf( Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE )  ).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskReplaceDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

	private void executeQueryForPIMS(RequestTaskActivityCriteria criteria , Map<String, String> reportQueryMap) 
	{
			String query = "";
			query += "  SELECT  "
				+ " GETMINUTESFROMDATEDIFF (RTA.\"requestCreatedOn\",RTA.\"approvalRequired_C\")  \"timeLapseAppReq\", "
				+ " case when RTA.\"Completed_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"Completed_C\" )     else   -100000   end  \"timeLapseComplete\", "
				+ " case when RTA.\"paymentCollected_C\" is not null  then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" , RTA.\"paymentCollected_C\" )     " 
				+"  else   " 
				+" 		  case when RTA.\"ncCollected_C\" is not null " 
				+"                    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"ncCollected_C\" ) " 
				+"                     else -100000 " 
				+"		  end " 
				+"  end  \"timeLapseCollected\", "
				
				+ " case when RTA.\"approved_C\" is not null  and rta.\"STATUSID\" not  in (9004,90056,90042,90041)   "
				+ "	    then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"approved_C\" ) else "
				+ "		case when RTA.\"rejected_C\" is not null " +
						"        then GETMINUTESFROMDATEDIFF ( RTA.\"requestCreatedOn\" ,RTA.\"rejected_C\" ) else -100000 end end  \"timeLapseApprove\", "
				+ "  RTA.* from REQUESTTASKACTIVITYVIEW  RTA " ;
			
			StringBuilder whereClause = new StringBuilder(" where 1=1 ");
			whereClause .
						 append( " AND \"requestTypeId\" in ( ").
						 append(criteria.getRequestTypeId() ).
//						 append( "," ).
//						 append(String.valueOf( Constant.REQUEST_TYPE_NOL)  ).
//						 append( "," ).
//						 append(String.valueOf( Constant.REQUEST_TYPE_CLEARANCE_LETTER)  ).
						 append( ")");
			fillWhereClause(criteria,whereClause);
            query+=whereClause.toString();

			String dataDef = "ReqTaskPIMSDataDef";        
			reportQueryMap.put(dataDef, query+"");
		
	}

}

