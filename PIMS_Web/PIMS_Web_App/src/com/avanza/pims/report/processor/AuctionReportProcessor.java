package com.avanza.pims.report.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.AuctionReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.ReceiptReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.collaxa.cube.xml.xpath.functions.xml.GetChildElementFunction;

public class AuctionReportProcessor extends AbstractReportProcessor {
@SuppressWarnings( "unchecked" )
		
public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		AuctionReportCriteria criteria = (AuctionReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		////////////////////////////////

		String query ="SELECT " +
		   "  BIDDER_ID  \"bidderId\",\n" +  
		   "  AUCTION_NUMBER  \"auctionNumber\",\n" +   
		   "  CELL_NUMBER  \"bidderCellNumber\",\n" +   
		   "  HOME_PHONE ,\n" +   
		   "  OFFICE_PHONE   \"telephoneNumber\",\n" +  
		   " ATTENDED, \n" +
		   " TOTAL_DEPOSIT_AMOUNT \"depositAmount\",\n" + 
		   " TOTAL_CONFISCATED_AMOUNT,\n" + 
		   " ANY_WIN, \n" +
	       " AUCTION_END_DATETIME, CONFISCATION_PERCENTAGE, OUTBIDDING_VALUE, \n"+
		   "  ADDRESS1 ,\n" +  
		   "  AUCTION_TITLE  \"auctionTitle\",\n"+  
		   "  REQUEST_START_DATE  \"registrationStartDate\",\n" +  
		   "  REQUEST_END_DATE  \"registrationEndDate\",\n" +  
		   "  REQUEST_NUMBER  \"requestNumber\",\n" +
		   "  RECEIPT_NUMBER  \"field4\",\n" +
		   "  AUCTION_PUBLISH_DATE  \"publicationDate\",\n"+  
		   "  AUCTION_START_DATETIME  \"auctionDate\",\n" +  
		   "  AUCTION_END_DATETIME  ,\n" +  
		   "  DEPOSIT_AMOUNT  \"field5\",\n" +
		   "  BIDDER_NAME  \"bidderName\",\n" +  
		   "  BID_PRICE  \"field6\",\n" +
		   "  UNIT_NUMBER  \"unitNumber\",\n" +
		   "  PROPERTY_NUMBER  \"field2\",\n" +
		   "  PROPERTY_NAME  \"field3\",\n" +
		   "  UNIT_DESC  \"field1\",\n" +
		   "  AUCTION_ID  \"auctionId\",\n" +  
		   "  REQUEST_ID  \"requestId\",\n" + 
		   "  UNIT_ID \"unitId\",\n" + 
		   "  OUTBIDDING_VALUE \"outBiddingValue\",\n" +
		   "  AUCTION_STATUS_EN \"auctionStatusEn\",\n" + 
		   "  AUCTION_STATUS_AR \"auctionStatusAr\"," +
		   "  COST_CENTER \"unitCostCenter\", \n" +   
		   "  AUCTION_REG_STATUS_EN \"bidderStatusEn\", "+
		   "  AUCTION_REG_STATUS_AR \"bidderStatusAr\" "+
		   " FROM " +
		   "  AUCTION_BIDDER_DETAIL_VIEW \n"+
	       " WHERE 1=1 AND BIDDER_ID IS NOT NULL";
		String condition="";
		if(criteria.getAuctionId()!=null)
			condition += " AND AUCTION_ID = " + criteria.getAuctionId();
		query +=condition;

		query +=" order BY BIDDER_NAME, REQUEST_ID, UNIT_ID	\n" ;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.AUCTION_BIDDER_REPORT;        
		reportQueryMap.put(dataDef, query+"");
				
		query = "SELECT \n" ;
				
        		query += "  '" + ReportMessageResource.getPropertyEn("auction.number") + "' \"lblAuctionNumber\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auction.refund.auctionTitle") + "' \"lblAuctionDesc\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auction.date") + "' \"lblAuctionDate\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auction.publishingDate") + "' \"lblAuctionPublishDate\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auction.requestStartDate") + "' \"lblRegistrationStartDate\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auction.requestEndDate") + "' \"lblRegistrationEndDate\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumber\",\n" +
        		
        		"  '" + ReportMessageResource.getPropertyEn("bidder.firstName") + "' \"lblBidderName\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("report.field.resiteredUnits") + "' \"lblUnitsRegistered\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auctionUnit.depositColumn") + "' \"lblDepositAmount\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("inquiryApplication.phoneNumber") + "' \"lblPhoneNumber\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("auctionBidderReport.bidderAttendanbceStatus") + "' \"lblBidderStatusEn\",\n" +
        		
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("report.summaryField.totalReceipts") + "' \"lblTotalReceiptsAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.number") + "' \"lblAuctionNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.refund.auctionTitle") + "' \"lblAuctionDescAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.date") + "' \"lblAuctionDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.publishingDate") + "' \"lblAuctionPublishDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.requestStartDate") + "' \"lblRegistrationStartDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auction.requestEndDate") + "' \"lblRegistrationEndDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("AuctionDepositRefundApproval.DataTable.RequestNumber") + "' \"lblRequestNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("bidder.firstName") + "' \"lblBidderNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("report.field.resiteredUnits") + "' \"lblUnitsRegisteredAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auctionUnit.depositColumn") + "' \"lblDepositAmountAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("inquiryApplication.phoneNumber") + "' \"lblPhoneNumberAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("auctionBidderReport.bidderAttendanbceStatus") + "' \"lblBidderStatusAr\",\n" +
		        /*******************************                           User Info  -- Mandatory Info            *********************************/	        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("port.title.auctionBidderReport") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("port.title.auctionBidderReport") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.AUCTION_BIDDER_LABEL_REPORT;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}

