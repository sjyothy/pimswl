package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RegionView;

public class LeaseContractDetailReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		LeaseContractsSummaryReportCriteria criteria = (LeaseContractsSummaryReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		
		String query = "SELECT " +
							" CAST( CNTRT.CONTRACT_ID AS VARCHAR(20) )  , " +
							" CNTRT.CONTRACT_NUMBER \"contractNumber\", " +
							//" CNTRT.RENT_AMOUNT  \"rentValue\"," +
							" CAST(to_char(CNTRT.START_DATE,'dd/mm/yyyy') AS VARCHAR(10) ) \"contractIssueDate\", " +
							" CAST( to_char(CNTRT.END_DATE,'dd/mm/yyyy') AS VARCHAR(10) )  \"contractEndDate\" , " +
							" CNTRT.RENT_AMOUNT \"contractAmount\", " +
							" NVL(PROP.COMMERCIAL_NAME, '---') \"propertyName\", " +
					                        
						//	" -- Unit Detail - (Master Information) " +
							" U.UNIT_NUMBER \"unitNumber\", " +
							" U.UNIT_DESC , " +
							" F.FLOOR_NAME \"floorName\", " +
							" F.FLOOR_NUMBER \"floorNumber\", " +
							" U.ACCOUNT_NUMBER , " +
							" COALESCE(CAST( U.UNIT_EW_UTILITY_NO as nvarchar2(30)),cast(U.DEWA_NUMBER as nvarchar2(30)))  \"dewaNumber\" , " +
							" PROP.LAND_NUMBER \"landNumber\" , " +
							" U.NO_OF_BED  , " +
							" U.UNIT_AREA \"unitArea\", " +
							" U.DEPOSIT_AMOUNT , " +
							" U.RENT_VALUE \"rentValue\", " +
						//	" -- Occupant Name " +
							" PRSN2.FIRST_NAME \"occupantFirstName\", " +
							" PRSN2.MIDDLE_NAME , " +
							" PRSN2.LAST_NAME \"occupantLastName\", " +					        
					//		" -- Tenant Name " +
							" PRSN.FIRST_NAME \"tenantFirstName\", " +
							" PRSN.MIDDLE_NAME \"tenantMiddleName\", " +
							" PRSN.LAST_NAME \"tenantLastName\", " +
							" PRSN.COMPANY_NAME , " +
					                        
						//	" -- Contract Status " +
							" DD2.DATA_DESC_EN \"statusEn\", " +
							" DD2.DATA_DESC_AR \"statusAr\", " +
					                        
						//	" -- Detail Section  - Payment Infomration " +
							" PS.PAYMENT_NUMBER  || ' | ' || RGN.DESCRIPTION_AR  \"paymentNo\", " +
							" CAST( TO_CHAR(PS.PAYMENT_DUE_ON,'dd/mm/yyyy') AS VARCHAR(10) ) \"paymentDueDate\", " +
					
							" PYT.DESCRIPTION_EN  \"paymentTypeEn\", " +
							" PYT.DESCRIPTION_AR  \"paymentTypeAr\", " +
					                        
						//	" -- Receipt Info " +
							" NVL(PRCT.RECEIPT_NUMBER, '---') \"receiptNumber\", " +
							" NVL(PRCD.METHOD_REF_NO,'---') \"chequeNumber\", " + 
							" PMT.DESCRIPTION , " +
							" CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_EN,'---') end \"chequeBankNameEn\", " + 
							" CASE WHEN PRCD.MIGRATED IS NOT NULL AND PRCD.MIGRATED =1 THEN PRCD.BANK_NAME else NVL(BANK.BANK_AR, '---') end \"chequeBankNameAr\", " +
					                        
							" PRCD.CREATED_BY , " +
					                        
						//	" -- Payment  Status " +
							" DD_PS.DATA_DESC_EN \"paymentStatusEn\", " +
							" DD_PS.DATA_DESC_AR \"paymentStatusAr\", " +
							" PS.AMOUNT \"paymentAmount\" " +
							
							" FROM  " +
							" CONTRACT CNTRT  " +
							" LEFT JOIN PERSON PRSN ON CNTRT.TENANT_ID = PRSN.PERSON_ID " +
							" LEFT JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
							" LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID  " +
							" LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID " +
							" LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID  " +
					        " LEFT JOIN (CONTACT_INFO CI LEFT JOIN REGION RGN ON CI.CITY_ID = RGN.REGION_ID) ON CI.CONTACT_INFO_ID=PROP.CONTACT_INFO_ID " +
							//" INNER JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID " +
							" LEFT JOIN PAYMENT_SCHEDULE PS ON CNTRT.CONTRACT_ID = PS.CONTRACT_ID  AND PS.PAYMENT_TYPE_ID IN (1,2,3)" +
							" LEFT JOIN PAYMENT_TYPE PYT ON PS.PAYMENT_TYPE_ID = PYT.PAYMENT_TYPE_ID " +
							" LEFT JOIN DOMAIN_DATA DD2 ON CNTRT.STATUS_ID = DD2.DOMAIN_DATA_ID " +
					                        
							" LEFT OUTER JOIN PAYMENT_RECEIPT_TERMS PRC  " +
							" ON PS.PAYMENT_SCHEDULE_ID = PRC.PAYMENT_SCHEDULE_ID  " +
					                                                                                         
							" LEFT OUTER JOIN PAYMENT_RECEIPT_DETAIL PRCD ON PRCD.PAYMENT_RECEIPT_DETAIL_ID=PRC.PAYMENT_RECEIPT_DETAIL_ID " +
							" LEFT JOIN PAYMENT_METHOD PMT ON PRCD.PAYMENT_METHOD_ID = PMT.PAYMENT_METHOD_ID " +
							" LEFT JOIN BANK ON BANK.BANK_ID = PRCD.BANK_ID " +
					                        
							" LEFT outer JOIN CONTRACT_UNIT_OCCUPIER CUO ON CUO.CONTRACT_UNIT_ID = CU.CONTRACT_UNIT_ID " +
							" LEFT outer JOIN PERSON PRSN2 ON CUO.PERSON_ID = PRSN2.PERSON_ID " +
							" LEFT JOIN PAYMENT_RECEIPT PRCT ON PRCT.PAYMENT_RECEIPT_ID=PRCD.PAYMENT_RECEIPT_ID " +
							" LEFT JOIN DOMAIN_DATA DD_PS ON PS.STATUS_ID = DD_PS.DOMAIN_DATA_ID " ;
                        
				String condition="";
				
							if(criteria.getContractNumber().length() >0)
								condition+=" AND lower(CNTRT.CONTRACT_NUMBER)like lower('%" + criteria.getContractNumber() +"%')";
							if(criteria.getTenantName() .length() >0)
								condition += " AND lower(NVL(PRSN.FIRST_NAME,'') || ' ' || NVL(PRSN.MIDDLE_NAME,'') || ' ' || NVL(PRSN.LAST_NAME,'')) LIKE lower('%"+ criteria.getTenantName().trim() +"%')";
								/*
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getTenantName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getTenantName() +"%')" +			
								" ) " ;*/
							if(criteria.getPropertyComercialName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME) like  lower('%" + criteria.getPropertyComercialName() +"%')";
							if(criteria.getUnitNum().length() >0)
								condition+=" AND lower(U.UNIT_NUMBER)  like  lower('%" + criteria.getUnitNum() +"%')";
							if(criteria.getFromContractDateRich()!=null  && criteria.getToContractDateRich()!=null)								
								condition += " AND CNTRT.START_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() )+"', 'dd/MM/yyyy')";
							if(criteria.getFromContractExpDateRich()!=null  && criteria.getToContractExpDateRich()!=null)
								condition += " AND CNTRT.END_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() )+"', 'dd/MM/yyyy')";
							if(!criteria.getContractType().equals("-1"))
								condition+= " AND CNTRT.CONTRACT_TYPE_ID = '" + criteria.getContractType() +"'"	;
							if(!criteria.getContractStatus().equals("-1"))
								condition+= " AND CNTRT.STATUS_ID  =  " + criteria.getContractStatus();
					
							if( criteria.getCostCenter() != null &&  criteria.getCostCenter().length() > 0 )
								condition+=" AND U.ACCOUNT_NUMBER = '"+criteria.getCostCenter().trim() + "'";
							
							if( criteria.getPropertyNumber() != null &&  criteria.getPropertyNumber().length() > 0 )
								condition+=" AND PROP.PROPERTY_NUMBER = '"+criteria.getPropertyNumber().trim()+"'";
							
							if( criteria.getCity() != null &&  !criteria.getCity().equals("-1") )
								condition+=" AND CI.CITY_ID = "+criteria.getCity().trim();
							
							if(condition.length() > 0)
					        	query+="WHERE 1=1  AND CNTRT.CONTRACT_CATEGORY=82002  AND (cntrt.is_deleted = 0) AND (cntrt.record_status = 1) \n"+ condition;
							else 
								query+="WHERE 1=1  AND CNTRT.CONTRACT_CATEGORY=82002  AND (cntrt.is_deleted = 0) AND (cntrt.record_status = 1) \n";

				String sorting =" ORDER BY CNTRT.CONTRACT_NUMBER , PS.PAYMENT_DUE_ON";
							
				query+=sorting;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.LEASE_CONTRACTS_DETAIL;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.property.Name") + "' \"lblPropertyNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("contract.issueDate") + "' \"lblContractIssueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("contract.endDate") + "' \"lblContractEndDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.occupantName") + "' \"lblOccupantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.tenantName") + "' \"lblTenantNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblStatusEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitDescription") + "' \"lblUnitDescEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("unit.unitArea") + "' \"lblUnitAreaEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.floor.Name") + "' \"lblFloorNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("floor.floorNo") + "' \"lblFloorNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.tab.unit.rent") + "' \"lblRentValueEn\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyEn("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNoEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.chequeNo") + "' \"lblChequeNumEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.bankNameCol") + "' \"lblBankNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.paymentAmountCol") + "' \"lblPaymentAmountEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("chequeList.message.paymentStatus") + "' \"lblPaymentStatusEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
        		"  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.property.Name") + "' \"lblPropertyNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("contract.issueDate") + "' \"lblContractIssueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("contract.endDate") + "' \"lblContractEndDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.occupantName") + "' \"lblOccupantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.tenantName") + "' \"lblTenantNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblStatusAr\",\n" + 
		        
		        "  '" + ReportMessageResource.getPropertyAr("receiveProperty.rentUnitNumber") + "' \"lblUnitNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.unitDescription") + "' \"lblUnitDescAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("unit.unitArea") + "' \"lblUnitAreaAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.floor.Name") + "' \"lblFloorNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("floor.floorNo") + "' \"lblFloorNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.tab.unit.rent") + "' \"lblRentValueAr\",\n" +		        

		        "  '" + ReportMessageResource.getPropertyAr("bouncedChequesList.paymentNumberCol") + "' \"lblPaymentNoAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.paymentDueOn") + "' \"lblPaymentDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("cancelContract.payment.paymentmethod") + "' \"lblPaymentMethodAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.chequeNo") + "' \"lblChequeNumAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.bankNameCol") + "' \"lblBankNameAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.paymentAmountCol") + "' \"lblPaymentAmountAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("chequeList.message.paymentStatus") + "' \"lblPaymentStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
		        

		        /*******************************                           User Info and Mandatory Info             *********************************/	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.leaseContractsDetail.rpt") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.leaseContractsDetail.rpt") + "' \"lblReportNameAr\",\n" + 
		        "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.LEASE_CONTRACTS_DETAIL_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
	
	
	 public Map<String, String> getReportParametersMap(IReportCriteria reportCriteria){
		 return super.getReportParametersMap(reportCriteria);
		/* PropertyService propertyService = new PropertyService();
		 LeaseContractsSummaryReportCriteria criteria = (LeaseContractsSummaryReportCriteria) reportCriteria;
		 HashMap<String, String> parameters = new  HashMap<String, String>();
		 
		 
			if(criteria.getContractNumber().length() > 0)
				parameters.put("param1", "" + criteria.getContractNumber().trim());
			else
				parameters.put("param1", "--");
			
			if(criteria.getTenantName().length() >0)
				parameters.put("param10", "" + criteria.getTenantName().trim());
			else
				parameters.put("param10", "--");
			
			if(criteria.getPropertyComercialName().length() >0)
				parameters.put("param2", "" + criteria.getPropertyComercialName().trim());
			else
				parameters.put("param2", "--");
			
			if(criteria.getUnitNum().length() >0)
				parameters.put("param11", "" + criteria.getUnitNum().trim());
			else
				parameters.put("param11", "--");
			
			if(criteria.getFromContractDateRich()!=null )	
				parameters.put("param5", "" + DateUtil.convertToDisplayDate( criteria.getFromContractDateRich() ));
			else
				parameters.put("param5", "--");
			
			if(criteria.getToContractDateRich()!=null )	
				parameters.put("param14", "" + DateUtil.convertToDisplayDate( criteria.getToContractDateRich() ));
			else
				parameters.put("param14", "--");
			
			if(criteria.getFromContractExpDateRich()!=null  )
				parameters.put("param6", "" + DateUtil.convertToDisplayDate( criteria.getFromContractExpDateRich() ));
			else
				parameters.put("param6", "--");
			
			if( criteria.getToContractExpDateRich()!=null )
				parameters.put("param15", "" + DateUtil.convertToDisplayDate( criteria.getToContractExpDateRich() ));
			else
				parameters.put("param15", "--");
			
			if(!criteria.getContractType().equals("-1")){
				try {
					DomainDataView ddv = propertyService.getDomainDataByDomainDataId(new Long(criteria.getContractType()));
					parameters.put("param7", ddv.getDataDescAr());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param7", "error");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param7", "error");
				}
			}
			else
				parameters.put("param7", "--");
			
			if(!criteria.getContractStatus().equals("-1"))
				try {
					DomainDataView ddv = propertyService.getDomainDataByDomainDataId(new Long(criteria.getContractStatus()));
					parameters.put("param16", ddv.getDataDescAr());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param16", "error");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param16", "error");
				}
			else
				parameters.put("param16", "--");
			
			if( criteria.getUnitRentAmountFrom() != null &&  criteria.getUnitRentAmountFrom().length() > 0 )
				parameters.put("param3", "" + criteria.getUnitRentAmountFrom().trim());
			else
				parameters.put("param3", "--");
			
			if( criteria.getUnitRentAmountTo() != null &&  criteria.getUnitRentAmountTo().length() > 0 )
				parameters.put("param12", "" + criteria.getUnitRentAmountTo().trim());
			else
				parameters.put("param12", "--");
			
			if( criteria.getContractRentAmountFrom() != null &&  criteria.getContractRentAmountFrom().length() > 0 )
				parameters.put("param4", "" + criteria.getContractRentAmountFrom().trim());
			else
				parameters.put("param4", "--");
			
			if( criteria.getContractRentAmountTo() != null &&  criteria.getContractRentAmountTo().length() > 0 )
				parameters.put("param13", "" + criteria.getContractRentAmountTo().trim());
			else
				parameters.put("param13", "--");
			
			if( criteria.getCostCenter() != null &&  criteria.getCostCenter().length() > 0 )
				parameters.put("param8", "" + criteria.getCostCenter().trim());
			else
				parameters.put("param8", "--");
			
			if( criteria.getPropertyNumber() != null &&  criteria.getPropertyNumber().length() > 0 )
				parameters.put("param17", "" + criteria.getPropertyNumber().trim());
			else
				parameters.put("param17", "--");
			
			if( criteria.getCity() != null &&  !criteria.getCity().equals("-1") ){
				try {
					RegionView rv = propertyService.getRegionViewByRegionId(new Long(criteria.getCity()));
					parameters.put("param9", "" + rv.getRegionName());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					parameters.put("param9", "(error)");
				} catch (PimsBusinessException e) {
					e.printStackTrace();
					parameters.put("param9", "(error)");
				}
			}
			else
				parameters.put("param9", "--");
			
			parameters.put("param18", "unused");

		 return parameters;
*/
	 }

}
