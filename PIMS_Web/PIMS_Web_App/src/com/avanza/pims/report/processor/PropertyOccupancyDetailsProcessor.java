package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PropertyOccupancyDetailsReportCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;

import com.avanza.pims.report.message.resource.ReportMessageResource;

public class PropertyOccupancyDetailsProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		PropertyOccupancyDetailsReportCriteria criteria = (PropertyOccupancyDetailsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		//////////////tenant details//////////////////
		String query=" SELECT  PROP.COMMERCIAL_NAME \"propertyName\",\n" +
		" PROP.PROPERTY_ID \"propertyId\", \n" +
		" U.UNIT_ID \"unitId\",\n " +
		" U.RENT_VALUE \"rentValue\",\n"+
		" U.STATUS_ID \"statusId\",\n"+
		" DD_US.DATA_DESC_EN \"unitStatusEn\", \n"+
		" DD_US.DATA_DESC_AR \"unitStatusAr\", \n"+ 
		" PROP.OWNERSHIP_TYPE_ID \"ownershipTypeId\",\n"+
		" U.USAGE_TYPE_ID \"usageTypeId\" ,\n"+
		" DD_OT.DATA_DESC_EN \"ownershipTypeEn\",\n" +
		" DD_OT.DATA_DESC_AR \"ownershipTypeAr\", \n" +
		" DD_UT.DATA_DESC_EN \"usageTypeEn\",\n" +
		" DD_UT.DATA_DESC_AR \"usageTypeAr\" \n" +
		" FROM   UNIT U \n"+    
		" INNER JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID \n"+  
		" INNER JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID \n"+
		" INNER JOIN DOMAIN_DATA DD_OT ON PROP.OWNERSHIP_TYPE_ID = DD_OT.DOMAIN_DATA_ID \n"  +
		" INNER JOIN DOMAIN_DATA DD_UT ON U.USAGE_TYPE_ID = DD_UT.DOMAIN_DATA_ID \n" +
		" INNER JOIN DOMAIN_DATA DD_US ON U.STATUS_ID = DD_US.DOMAIN_DATA_ID \n" ;
		
		
		String condition=" WHERE U.STATUS_ID IN (2005,2008,2014,2015) ";
		if(!criteria.getUnitUsage().equals("-1"))
			condition += " AND U.USAGE_TYPE_ID = '" + criteria.getUnitUsage()+"'";
		
		if(!criteria.getPropertyOwnerShip().equals("-1"))
			condition += " AND PROP.OWNERSHIP_TYPE_ID = '" + criteria.getPropertyOwnerShip()+"'";
      	
		if(criteria.getBuildingName() != null && criteria.getBuildingName().trim().length() > 0)
			condition += " AND LOWER(PROP.COMMERCIAL_NAME) LIKE LOWER('%"+criteria.getBuildingName().trim()+"%')";
		
		query+= condition;

		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.PROPERTY_OCCUPANCY_DETAILS;        
		reportQueryMap.put(dataDef, query+"");
		
		  // /////////////LABELS////////////////////////////////
        
		
		query = "SELECT \n" + 
		         
		       
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.endowmentBiuldings") + "' \"lblEndowmentEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.minorsBuildings") + "' \"lblMinorsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.amafBiuldings") + "' \"lblAmafEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.amafAndMinorsBiuldings") + "' \"lblAmafAndMinorsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.residentialUnits") + "' \"lblResidentialUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.commercialUnits") + "' \"lblCommercialUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.allUnits") + "' \"lblAllUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalNOU") + "' \"lblTotalUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalROU") + "' \"lblTotalRentOfUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalNOVU") + "' \"lblTotalVacantUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalROVU") + "' \"lblTotalRentOfVacantUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalNORU") + "' \"lblTotalRentedUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalRORU") + "' \"lblTotalRentOfRentedUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalNOCU") + "' \"lblTotalContractualUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.totalROCU") + "' \"lblTotalRentOfCUnitsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancySummary.label.grandTotal") + "' \"lblGrandTotalEn\",\n" +
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.endowmentBiuldings") + "' \"lblEndowmentAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.minorsBuildings") + "' \"lblMinorsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.amafBiuldings") + "' \"lblAmafAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.amafAndMinorsBiuldings") + "' \"lblAmafAndMinorsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.residentialUnits") + "' \"lblResidentialUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.commercialUnits") + "' \"lblCommercialUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.allUnits") + "' \"lblAllUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalNOU") + "' \"lblTotalUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalROU") + "' \"lblTotalRentOfUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalNOVU") + "' \"lblTotalVacantUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalROVU") + "' \"lblTotalRentOfVacantUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalNORU") + "' \"lblTotalRentedUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalRORU") + "' \"lblTotalRentOfRentedUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalNOCU") + "' \"lblTotalContractualUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.totalROCU") + "' \"lblTotalRentOfCUnitsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancySummary.label.grandTotal") + "' \"lblGrandTotalAr\",\n" +
		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.propertyOccupancyDetails") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.propertyOccupancyDetails") + "' \"lblReportNameAr\" ,\n" + 
		         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.PROPERTY_OCCUPANCY_DETAILS_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
