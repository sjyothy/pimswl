package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.SettlementReportCriteria;
import com.avanza.pims.ws.vo.PaymentBean;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class SettlementReportProcessor extends AbstractReportProcessor {

	transient Logger logger = Logger.getLogger(SettlementReportProcessor.class);
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) 
	{
		String methodName = "getReportQueryMap|";
		SettlementReportCriteria criteria = (SettlementReportCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		logger.logInfo( methodName + "Start|requestId:%s", criteria.getRequestId()  );
		String finalQuery ="";
        String dataDefQuery = "";
        String calculationsQuery = "";
		String queryTemplate = "";
        String dataDef;
        //String finalQuery = "";
        RequestServiceAgent rsa = new RequestServiceAgent();
        
      try        
       {  
    	  if(criteria.getRequestId() != null)
    	  {
    		 finalQuery = rsa.getSettlementQuery( criteria.getRequestId() );
    		 if( finalQuery != null && finalQuery.trim().length() > 0 )
    		 {
			     int separatorIndex = finalQuery.indexOf("next");
			     dataDefQuery = finalQuery.substring(0,separatorIndex);
			     calculationsQuery = finalQuery.substring(separatorIndex+4);
    		 }
    	  }
    	  
    	  if( finalQuery == null || finalQuery.trim().length() <= 0 )
    	  {
    		
    		  finalQuery = createSettlementQuery( criteria );
    		  int separatorIndex = finalQuery.indexOf("next");
    		  
			   dataDefQuery = finalQuery.substring(0,separatorIndex);
			  calculationsQuery = finalQuery.substring(separatorIndex+4);
			  //saveSettlementQuery( criteria, finalQuery );
			
    	  }
    	   logger.logInfo( methodName + "dataDefQuery:%s", dataDefQuery  );         
	        
	        dataDef = ReportConstant.DataDef.SETTLEMENT_REPORT_PAYMENTS_TABLE;        
			reportQueryMap.put(dataDef, dataDefQuery);   
			dataDef = ReportConstant.DataDef.SETTLEMENT_REPORT_CALCULATIONS;        
			reportQueryMap.put(dataDef, calculationsQuery);  
		  
       }
      catch (Exception e) 
      {
    	  logger.LogException( methodName + "ErrorOccured", e  );
			
      }
		// /////////////LABELS////////////////////////////////
		dataDefQuery = "SELECT \n" + 

        /**
-		 * ***************************** English Labels
		 * ********************************
		 */
		
		        "  '" + ReportMessageResource.getPropertyEn("commons.Contract.Number") + "' \"lblContractNumberEn\",\n" +  
		        "  '" + ReportMessageResource.getPropertyEn("contract.tenantName") + "' \"lblTenantNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.costCenter") + "' \"lblCostCenterEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.grpCustNo") + "' \"lblGrpCustomerNumberEn\",\n" +
		  
		        
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.startdate") + "' \"lblContractStartDateEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.enddate") + "' \"lblContractEndDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.rentalamount") + "' \"lblContractRentalAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.evacdate") + "' \"lblEvacuationDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.actualrentperiod") + "' \"lblRentPeriodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.actualrentamt") + "' \"lblRentAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.months") + "' \"lblMonthsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.days") + "' \"lblDaysEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.totaldepositamt") + "' \"lblDepositAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.totalrealizedamt") + "' \"lblRealizedAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.paidamt") + "' \"lblPaidAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.unrealizedamt") + "' \"lblUnRealizedAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.evacfines") + "' \"lblEvacuationFinesEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.unpaidamt") + "' \"lblUnpaidAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.label.unpaidfines") + "' \"lblUnpaidFinesEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymenttype") + "' \"lblPaymentTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymentdate") + "' \"lblPaymentDueDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymentmethod") + "' \"lblMethodEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.paymentmethod") + "' \"lblPaymentAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("paymentSchedule.description") + "' \"lblPaymentDescriptionEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.status") + "' \"lblStatusEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.receiptno") + "' \"lblReceiptNumberEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.totalrefundamount") + "' \"lbltotalRefundAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.totaldueamount") + "' \"lbltotalDueAmountEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.lbl.rentPaymentsDetails") + "' \"lblRentPaymentsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.lbl.otherPayments") + "' \"lblOtherPaymentsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.lbl.returnCheques") + "' \"lblReturnedPaymentsEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("settlement.payment.currency") + "' \"lblPaymentCurrency\",\n" +		       		        
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.settlementReport.rpt") + "' \"lblSettlementReportEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\",\n" +
		       
		        
		        
		        /**
				 * ***************************** Arabic Labels
				 * ********************************
				 */
		             
		        "  '" + ReportMessageResource.getPropertyAr("commons.Contract.Number") + "' \"lblContractNumberAr\",\n" +  
		        "  '" + ReportMessageResource.getPropertyAr("contract.tenantName") + "' \"lblTenantNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.costCenter") + "' \"lblCostCenterAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.grpCustNo") + "' \"lblGrpCustomerNumberAr\",\n" +
		        
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.startdate") + "' \"lblContractStartDateAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.enddate") + "' \"lblContractEndDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.rentalamount") + "' \"lblContractRentalAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.evacdate") + "' \"lblEvacuationDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.actualrentperiod") + "' \"lblRentPeriodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.actualrentamt") + "' \"lblRentAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.months") + "' \"lblMonthsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.days") + "' \"lblDaysAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.totaldepositamt") + "' \"lblDepositAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.totalrealizedamt") + "' \"lblRealizedAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.paidamt") + "' \"lblPaidAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.unrealizedamt") + "' \"lblUnRealizedAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.evacfines") + "' \"lblEvacuationFinesAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.unpaidamt") + "' \"lblUnpaidAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.label.unpaidfines") + "' \"lblUnpaidFinesAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymenttype") + "' \"lblPaymentTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymentdate") + "' \"lblPaymentDueDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymentmethod") + "' \"lblMethodAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.paymentmethod") + "' \"lblPaymentAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("paymentSchedule.description") + "' \"lblPaymentDescriptionAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.status") + "' \"lblStatusAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.receiptno") + "' \"lblReceiptNumberAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.totalrefundamount") + "' \"lbltotalRefundAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.totaldueamount") + "' \"lbltotalDueAmountAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.lbl.rentPaymentsDetails") + "' \"lblRentPaymentsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.lbl.otherPayments") + "' \"lblOtherPaymentsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.lbl.returnCheques") + "' \"lblReturnedPaymentsAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("settlement.payment.currency") + "' \"lblPaymentCurrencyAr\",\n" +     
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.settlementReport.rpt") + "' \"lblSettlementReportAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" ,\n" + 
		       
		        " '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" +

		        "FROM DUAL";
        logger.logInfo( methodName + "queryTemplate:%s", queryTemplate);
		dataDef = ReportConstant.DataDef.SETTLEMENT_REPORT_LABEL;        
		reportQueryMap.put(dataDef, dataDefQuery);         
        
        return reportQueryMap;
   
      
	}
   @SuppressWarnings( "unchecked" )
	private String createSettlementQuery( SettlementReportCriteria criteria  )
	{
		String methodName = "createSettlementQuery|";
		logger.logInfo( methodName + "Start" );
		String finalQuery ="";
		String queryTemplate = "";
		String dataDefQuery = "";
        String calculationsQuery = "";
        String union = "UNION ALL\n";
        int listSize = 0;
		  //////// All Payments////////////////////////
	    queryTemplate= " SELECT '%s'  \"paymentTypeEn\" , " +
	 	   "'%s'  \"paymentTypeAr\" , " +
	 	   "'%s'  \"paymentDueDate\" , " +
	 	   "'%s'  \"paymentMethod\" , " +
	 	   "'%s'  \"paymentAmount\" , " +
	 	   "'%s'  \"paymentStatusEn\" , " +
	 	   "'%s'  \"paymentStatusAr\" , " +
	 	   "'%s'  \"paymentDescriptionEn\" ," +
	 	   "'%s'  \"paymentDescriptionAr\" ," +
	 	   "'%s'  \"category\" ," +
	 	   "'%s'  \"receiptNumber\"  FROM DUAL \n" ;   
    
        listSize = criteria.getAllPayments().size();
        for (int index = 0; index < listSize; index++)
        {
        	PaymentBean payment = criteria.getAllPayments().get( index );        	
        	
        	if(payment.getPaymentScheduleView() != null)
        		{
        			dataDefQuery += String.format(queryTemplate, payment.getPaymentType(), payment.getPaymentType(),
        							payment.getDueDate(), payment.getPaymentMethod(), payment.getAmountVal(),
					                payment.getStatus(), payment.getStatus(), payment.getPaymentScheduleView().getDescription(),
					                payment.getPaymentScheduleView().getDescription(), payment.getCategory(), payment.getRecieptNo() );
        		}
        	else
        	{
        			dataDefQuery += String.format(queryTemplate, payment.getPaymentType(), payment.getPaymentType(),
						payment.getDueDate(), payment.getPaymentMethod(), payment.getAmountVal(),
		                payment.getStatus(), payment.getStatus(), "","", payment.getCategory(), payment.getRecieptNo() );
        	}
        	if ( index != listSize - 1 )
        		dataDefQuery += union;
        }
        //This is for the case when there are no payments against the contract.
        if( dataDefQuery == null || dataDefQuery.trim().length()<=0 )
        	{
        			dataDefQuery += String.format(queryTemplate, "", "","", "", "","", "", "","", "", "" );
        	}
        finalQuery = dataDefQuery;
        
         //////// Calculated Data////////////////////////
        
        calculationsQuery = "SELECT \n" +
        
        "  '" + criteria.getContractNumber() + "' \"contractNumber\",\n" +
        "  '" + criteria.getTenantName() + "' \"tenantName\",\n" +
        "  '" + criteria.getGrpCustomerNumber() + "' \"grpCustomerNumber\",\n" +
        "  '" + criteria.getCostCenter() + "' \"costCenter\",\n" +
        
        "  '" + criteria.getContractStartDate() + "' \"contractStartDate\",\n" +
        "  '" + criteria.getContractEndDate() + "' \"contractEndDate\",\n" +
        "  '" + criteria.getEvacuationDateString() + "' \"evacuationDate\",\n" +
        "  '" + criteria.getContractRentalAmount() + "' \"contractRentalAmount\",\n" +
        "  '" + criteria.getMonths() + "' \"months\",\n" +
        "  '" + criteria.getDays() + "' \"days\",\n" +
        "  '" + criteria.getActualRentalAmount() + "' \"rentAmount\",\n" +
        "  '" + criteria.getTotalDepositAmount() + "' \"depositAmount\",\n" +
        "  '" + criteria.getTotalRealizedAmount() + "' \"realizedAmount\",\n" +
        "  '" + criteria.getTotalPaidAmount() + "' \"paidAmount\",\n" +
        "  '" + criteria.getTotalUnrealizedAmount() + "' \"unRealizedAmount\",\n" +
        "  '" + criteria.getTotalEvacFines() + "' \"evacuationFines\",\n" +
        "  '" + criteria.getOtherUnpaidfines() + "' \"unpaidFines\",\n" +
        "  '" + criteria.getTotalUnpaidAmount() + "' \"unpaidAmount\",\n" +
        "  '" + criteria.getTotalRefundAmount() + "' \"totalRefundAmount\",\n" +
        "  '" + criteria.getTotalDueAmount() + "' \"totalDueAmount\",\n" +
        "  '" + criteria.getCreditMemo() + "' \"creditMemo\"\n" +
        "FROM DUAL";
		
		finalQuery +="next";
		finalQuery+=(calculationsQuery);
		logger.logInfo( "Final Query:%s", finalQuery);
		logger.logInfo( methodName + "Finish");
		return finalQuery;
	}
   @SuppressWarnings( "unchecked" )
	private void saveSettlementQuery( SettlementReportCriteria criteria, String finalQuery )throws PimsBusinessException
	{
		String methodName = "saveSettlementQuery|";
		logger.logInfo( methodName + "Start|requestId:%s", criteria.getRequestId() );
		if( criteria.getRequestId() != null)	
			new RequestServiceAgent().saveSettlementRequestQuery(criteria.getRequestId(), finalQuery);
		logger.logInfo( methodName + "Finish");
	}
   @SuppressWarnings( "unchecked" )
	public void createAndSaveSettlementQuery( SettlementReportCriteria criteria )throws PimsBusinessException 
	{
		String methodName = "createAndSaveSettlementQuery|";
		logger.logInfo( methodName + "Start");
		String finalQuery  = createSettlementQuery( criteria );
		saveSettlementQuery(criteria, finalQuery);
		logger.logInfo( methodName + "Finish");
	}
}
