package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class StatementOfAccountProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		StatementOfAccountCriteria criteria = (StatementOfAccountCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		//////////////tenant details//////////////////
		String query="";
		query += " SELECT "+  
		 " ACCOUNT_NUMBER \"accountNumber\"\n," +
		 " OWNER_NAME \"accountOwnerName\"\n," +
		 " TRX_DATE \"trxDate\"\n," +
		 " TRX_TYPE_EN \"trxTypeEn\"\n," + 
		 " TRX_TYPE_AR \"trxTypeAr\"\n,"  +
		 " AMOUNT \"amount\"\n," +
		 " DESC_EN \"descEn\"\n,"  +
		 " DESC_AR \"descAr\"\n,"  +
		 " TRANS_ID \"trxId\"\n,"  +
		 " PERSON_ID \"personId\" \n" +
		 " FROM STATEMENT_OF_ACCOUNT_VIEW ";
		
      	query+="WHERE 1=1 AND PERSON_ID = "+ criteria.getPersonId();

		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.STATEMENT_OF_ACCOUNT;        
		reportQueryMap.put(dataDef, query+"");
		
		  // /////////////LABELS////////////////////////////////
        
		
		query = "SELECT \n" + 
        
          		 
		         
		       
		        "  '" + ReportMessageResource.getPropertyEn("report.label.accountnumber") + "' \"lblAccountNumberEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.label.accountOwner") + "' \"lblAccountOwnerNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.fromDate") + "' \"lblFromDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.toDate") + "' \"lblToDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("collection.transactionType") + "' \"lblTrxTypeEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("order.transaction.date") + "' \"lblTrxDateEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("paymentConfiguration.description") + "' \"lblDescEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.label.debit") + "' \"lblDebitEn\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyEn("report.label.credit") + "' \"lblCreditEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("report.label.balance") + "' \"lblBalanceEn\",\n" +
		        
		        
		        
		        /*******************************                           Arabic Labels             *********************************/
		        
		        "  '" + ReportMessageResource.getPropertyAr("report.label.accountnumber") + "' \"lblAccountNumberAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.label.accountOwner") + "' \"lblAccountOwnerNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.fromDate") + "' \"lblFromDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.toDate") + "' \"lblToDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("collection.transactionType") + "' \"lblTrxTypeAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("order.transaction.date") + "' \"lblTrxDateAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("paymentConfiguration.description") + "' \"lblDescAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.label.debit") + "' \"lblDebitAr\",\n" +		        
		        "  '" + ReportMessageResource.getPropertyAr("report.label.credit") + "' \"lblCreditAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("report.label.balance") + "' \"lblBalanceAr\",\n" +
		        

		        /*******************************                           Mandatory   Lables          *********************************/ 	        
		        
		        "  '" + ReportMessageResource.getPropertyEn("memsReportHeader") + "' \"lblMEMSHeaderEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyEn("report.heading.statementOfAccount") + "' \"lblReportNameEn\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("memsReportHeader") + "' \"lblMEMSHeaderAr\",\n" + 
		        "  '" + ReportMessageResource.getPropertyAr("report.heading.statementOfAccount") + "' \"lblReportNameAr\" ,\n" + 
		         "    '"+ criteria.getReportGeneratedBy()+"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.STATEMENT_OF_ACCOUNT_LABEL;        
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
