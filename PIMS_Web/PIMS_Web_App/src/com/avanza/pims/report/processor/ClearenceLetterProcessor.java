package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ClearenceLetterCriteria;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.NOLCriteria;
public class ClearenceLetterProcessor extends AbstractReportProcessor
{
	public ClearenceLetterProcessor()
	{
		
	}
	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria)
	{
		
		ClearenceLetterCriteria criteria = (ClearenceLetterCriteria) reportCriteria;
		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query=  "SELECT REQST.REQUEST_NUMBER,\n" +
						   "REQST.REQUEST_ID ,\n"+
					       "C_UNIT.CONTRACT_ID ,\n"+
					       "CNTRCT.CONTRACT_NUMBER ,\n"+
					       " to_char( CNTRCT.START_DATE  , 'dd-MON-yyyy' ) \"rentStartDate\",\n"+
					       " to_char( CNTRCT.END_DATE   , 'dd-MON-yyyy' ) \"rentEndDate\",\n"+
					       " to_char( CNTRCT.SETTLEMENT_DATE  , 'dd-MON-yyyy' ) \"evacuationDate\",\n"+
					       "UNT.UNIT_DESC \"unitDescription\" ,\n"+
					       "PROP.PROPERTY_NAME \"propertyName\" ,\n"+
					       "PROP.PROPERTY_NUMBER ,\n"+
					       "PROP.COMMERCIAL_NAME \"propCommercialName\", \n"+
					       "PROP.LAND_NUMBER \"landNumber\",\n"+
					       "case \n"+ 
					       "when PRSN.IS_COMPANY =0 THEN \n"+
					       "PRSN.FIRST_NAME||' '||MIDDLE_NAME||' '||LAST_NAME \n"+ 
					       "ELSE \n"+
					       "PRSN.company_name end  \"tenantName\" ,\n"+
					       "RGN.DESCRIPTION_EN \"regionNameEn\" ,\n"+
					       "RGN.DESCRIPTION_AR \"regionNameAr\" ,\n"+
					       "CI.CONTACT_INFO_ID\n"+ 
						  
						   "FROM  REQUEST REQST \n"+ 
						   "INNER JOIN CONTRACT CNTRCT ON CNTRCT.CONTRACT_ID = REQST.CONTRACT_ID \n"+ 
						   "INNER JOIN CONTRACT_UNIT C_UNIT ON C_UNIT.CONTRACT_ID = CNTRCT.CONTRACT_ID \n"+
						   "INNER JOIN UNIT UNT ON UNT.UNIT_ID = C_UNIT.UNIT_ID \n"+
						   "inner join FLOOR FLR ON UNT.FLOOR_ID=FLR.FLOOR_ID \n"+
						   "INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = FLR.PROPERTY_ID \n"+ 
						   "INNER JOIN PERSON PRSN ON CNTRCT.TENANT_ID = PRSN.PERSON_ID \n"+ 
						   "LEFT JOIN CONTACT_INFO CI ON PROP.CONTACT_INFO_ID = CI.CONTACT_INFO_ID \n"+ 
						   "LEFT JOIN REGION RGN ON RGN.REGION_ID = CI.CITY_ID \n"+ 
						   "WHERE 1=1 \n"; 
		String condition="";
		if(criteria.getRequestId().length() >0)
			condition+=" AND lower(REQST.REQUEST_ID) like lower('%" + criteria.getRequestId()+"%')";
		
		query += condition;
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.CLEARENCE_LETTER_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		return reportQueryMap;
	}
			

}
