package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.UnitEvaluationCriteria;
import com.avanza.pims.web.WebConstants;

public class UnitEvaluationProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		UnitEvaluationCriteria criteria = (UnitEvaluationCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		String query = "SELECT " ;
		
		query += 
			
		"PROP.COMMERCIAL_NAME propertyName, " +
		"PROP.PROPERTY_NUMBER propertyNumber, " +
		"UNT.UNIT_NUMBER unitNumber, " +
		
		"DD.DATA_DESC_AR unitTypeAR, " +
		"UNT.ACCOUNT_NUMBER costCenter, " +
		
		"EVRST.RECOMMENDATION inspectorReport, " +
		"EVRST.CREATED_BY inspectorName, " +
		"EVRST.PROPERTY_VALUE newRentValue, " +
		
		"(SELECT CONFIGVALUE FROM SYSTEM_CONFIGURATION WHERE CONFIGKEY='"+WebConstants.SYS_CONFIG_KEY_PROPERTY_HOS_NAME+"') hosName ";
		
		query += "FROM REQUEST REQ " ;
		
		query +=
		"INNER JOIN UNIT UNT ON UNT.UNIT_ID=REQ.UNIT_ID " +
		"INNER JOIN DOMAIN_DATA DD ON DD.DOMAIN_DATA_ID = UNT.UNIT_TYPE_ID " +
		"INNER JOIN FLOOR FLR ON FLR.FLOOR_ID = UNT.FLOOR_ID " +
		"INNER JOIN PROPERTY PROP ON PROP.PROPERTY_ID = FLR.PROPERTY_ID " +

		"INNER JOIN EVALUATION_RESULTS EVRST ON EVRST.REQUEST_ID=REQ.REQUEST_ID ";
		
		String condition="WHERE REQ.REQUEST_ID='" + criteria.getRequestId() +"' ";
		
		query+=condition;
		
		String dataDef = ReportConstant.DataDef.UNIT_EVALUATION_REPORT;        
		reportQueryMap.put(dataDef, query+"");
		
		System.out.println("-- QUERY --" + query); 
		
		addHosRecommendationSubReport(criteria,reportQueryMap);
		addRentCommitteRecommendationSubReport(criteria,reportQueryMap);
		
		return reportQueryMap;
	}
	
	private void addHosRecommendationSubReport(UnitEvaluationCriteria criteria, Map<String, String> reportQueryMap) {
		
		String condition = " WHERE TYPE_ID=(SELECT DOMAIN_DATA_ID FROM DOMAIN_DATA WHERE DATA_VALUE='" + WebConstants.EvaluationApplication.HOS_RECOMMENDATION+ "')";
		condition += " AND REQUEST_ID='" + criteria.getRequestId() + "'";
		//condition += " --//hosRecommendation";
		
		String query = "SELECT " ;
		query += "TO_CHAR(CREATED_ON,'DD/MM/YYYY HH:MM:SS AM') dateTime, " +
				 "TEXT recommendation " ;
		query +=" FROM EVALUATION_RECOMMENDATION ";
		query += condition;
		
		System.out.println("-- QUERY --" + query);
		String dataDef = ReportConstant.DataDef.HOS_RECOMMENDATION_SUBREPORT + "#" + "hosRecommendation";        
		reportQueryMap.put(dataDef, query+"");
		
	}
	
	private void addRentCommitteRecommendationSubReport(UnitEvaluationCriteria criteria, Map<String, String> reportQueryMap) {
		
		String condition = " WHERE TYPE_ID=(SELECT DOMAIN_DATA_ID FROM DOMAIN_DATA WHERE DATA_VALUE='" + WebConstants.EvaluationApplication.RENT_COMMITTE_RECOMMENDATION+ "')";
		condition += " AND REQUEST_ID='" + criteria.getRequestId() + "'";
		//condition += " --//rentCommitteRecommendation";
		
		String query = "SELECT " ;
		query += "TO_CHAR(CREATED_ON,'DD/MM/YYYY HH:MM:SS AM') dateTime, " +
				 "TEXT recommendation " ;
		query +=" FROM EVALUATION_RECOMMENDATION ";
		query += condition;
		
		System.out.println("-- QUERY --" + query);
		String dataDef = ReportConstant.DataDef.RENT_COMMITTE_RECOMMENDATION_SUBREPORT + "#" + "rentCommitteRecommendation";         
		reportQueryMap.put(dataDef, query+"");
		
	}
			

}
