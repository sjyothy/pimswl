package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.RequestDetailsReportCriteria;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.RequestView;

public class RequestDetailsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		RequestDetailsReportCriteria criteria = (RequestDetailsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
 
		
		String query="";
		RequestView requestView = new RequestView();;
		RequestService rService = new RequestService();
		try {
			if(criteria.getRequestId() != null && criteria.getRequestId().length() >0)
			{	requestView = rService.getRequestById(new Long(criteria.getRequestId()));
		
		
			if(requestView != null && requestView.getRequestTypeId() != null )
			{
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_MAINTENANCE_APPLICATION) == 0)
				{
					query += "SELECT * FROM RQST_DTL_MAINTENANCE_NEW WHERE REQUESTID = "+criteria.getRequestId();
				}
				else if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME) == 0)
				{
					query += "SELECT * FROM  RQST_DTL_CHANGE_TENANT WHERE REQUESTID = "+criteria.getRequestId();
				} 
				
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_AUCTION_REGISTERATION) == 0)
				{
					query += "SELECT * FROM  RQST_DTL_AUCTION WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_PTOPERTY_EVALUATION) == 0)
				{
					query += "SELECT * FROM RQST_DTL_EVALUATION WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_TENDER_PURCHACE_REQUEST) == 0)
				{
					query += "SELECT * FROM RQST_DTL_TENDER_PURCHASE WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_TENDER_INQUIRY) == 0 || requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_CONSTRUCTION_TENDER_INQUIRY) == 0)
				{
					query += "SELECT * FROM RQST_DTL_CONS_TENDER_INQ WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_TENDER_EXTEND_APPLICATION) == 0)
				{
					query += "SELECT * FROM RQST_DTL_MAINT_EXTEND_TENDER WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
				if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_CONSTRUCTION_TENDER_EXTEND_APPLICATION) == 0)
				{
					query += "SELECT * FROM RQST_DTL_CONST_EXTEND_TENDER WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
					if(requestView.getRequestTypeId().compareTo(WebConstants.MemsRequestType.MASRAF_DISBURSEMENT) == 0 
					  )
				{
				    query += "SELECT * FROM MASRAF_DISB_REPORT_DETAILS WHERE \"requestId\" = "+criteria.getRequestId();
				}
					else
						if(
						   requestView.getRequestTypeId().compareTo(WebConstants.MemsRequestType.ENDOWMENT_DISBURSEMENT_REQUEST) == 0
						  )
					{
					    query += "SELECT * FROM ENDOW_DISB_REPORT_DETAILS WHERE \"requestId\" = "+criteria.getRequestId();
					}

				else
					if(requestView.getRequestTypeId().longValue() >= 29 && requestView.getRequestTypeId().longValue() <= 39 )
				{
				    query += "SELECT * FROM RQST_DTL_MEMS WHERE REQUESTID = "+criteria.getRequestId();
				}
				else
						if(requestView.getRequestTypeId().longValue() == WebConstants.MemsRequestType.ENDOWMENT_NOL_REQUEST_ID.longValue() )
					{
					    query += "SELECT * FROM RQST_DTL_MEMS_ENDOWMENT_NOL WHERE REQUESTID = "+criteria.getRequestId();
					}
					

				else
				{
					query += "SELECT '"+criteria.getReportGeneratedBy()+"' as \"loggedInUser\", RQST_DTL_CONTRACT.* FROM RQST_DTL_CONTRACT WHERE REQUESTID = "+criteria.getRequestId();
				}
				//TODO: write code here
			}
		}
		}  catch (PimsBusinessException e) {
			e.printStackTrace();
		}
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.REQUEST_DETAILS_REPORT;        
		reportQueryMap.put(dataDef, query);
        
        return reportQueryMap;
	}
			

}
