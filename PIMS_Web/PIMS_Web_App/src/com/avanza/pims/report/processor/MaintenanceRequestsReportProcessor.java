package com.avanza.pims.report.processor;

import java.util.HashMap;
import java.util.Map;

import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.criteria.MaintenanceRequestsReportCriteria;
import com.avanza.pims.report.message.resource.ReportMessageResource;

public class MaintenanceRequestsReportProcessor extends AbstractReportProcessor {

	public Map<String, String> getReportQueryMap(IReportCriteria reportCriteria) {
		
		MaintenanceRequestsReportCriteria criteria = (MaintenanceRequestsReportCriteria) reportCriteria;

		Map<String, String> reportQueryMap = new HashMap<String, String>(0);
		
		////////////////////////////////
		String query = "SELECT  " +
		" PRSN.FIRST_NAME ||' '|| PRSN.MIDDLE_NAME ||' ' ||PRSN.LAST_NAME \"contractorName\" ," +
        " REQST.REQUEST_NUMBER \"maintenanceRequestNo\" , " +
        " REQST.MAINTENANCE_DETAILS \"requestDetails\" , " +
        " PROP.COMMERCIAL_NAME  \"propertyName\" ,\n" +      
		" CAST(to_char(REQST.REQUEST_DATE ,'dd/mm/yyyy') AS VARCHAR(10) )  \"maintenanceDate\" , " +
        " DD_POT.DATA_DESC_EN \"ownershipTypeEn\" , " +
        " DD_POT.DATA_DESC_AR \"ownershipTypeAr\" , " +
        " DD_RP.DATA_DESC_EN \"maintenancePriorityEn\" , " +
        " DD_RP.DATA_DESC_AR \"maintenancePriorityAr\" , " +    
        " DD_RS.DATA_DESC_EN \"requestSourceEn\" , " +
        " DD_RS.DATA_DESC_AR \"requestSourceAr\" , " +      
        " MNTNC.DESCRIPTION_EN  \"maintenanceTypeEn\", " +
        " MNTNC.DESCRIPTION_AR  \"maintenanceTypeAr\", " +
//        " DD_PR.DATA_DESC_EN  \"propertyOwnershipTypeEn\" , " +
//        " DD_PR.DATA_DESC_AR  \"propertyOwnershipTypeAr\" , " +
        " REQST.WITHIN_CONTRACT  \"underContract\" , " +
        " DD_RST.DATA_DESC_EN  \"requestStatusEn\" , " +
        " DD_RST.DATA_DESC_AR  \"requestStatusAr\"  " +
//        maintenance priority missing still      
" FROM  " +
		" REQUEST REQST " +
        " LEFT JOIN CONTRACT CNTRT ON CNTRT.CONTRACT_ID = REQST.CONTRACT_ID " +
		" LEFT JOIN PERSON PRSN ON CNTRT.CONTRACTOR_ID = PRSN.PERSON_ID " +
		" LEFT JOIN CONTRACT_UNIT CU ON CNTRT.CONTRACT_ID = CU.CONTRACT_ID " +
        " LEFT JOIN UNIT U ON U.UNIT_ID = CU.UNIT_ID " +
		" LEFT JOIN FLOOR F ON F.FLOOR_ID = U.FLOOR_ID  " +
		" LEFT JOIN PROPERTY PROP ON F.PROPERTY_ID = PROP.PROPERTY_ID " +
		" LEFT JOIN DOMAIN_DATA DD_POT ON PROP.OWNERSHIP_TYPE_ID = DD_POT.DOMAIN_DATA_ID " + 
		" LEFT JOIN DOMAIN_DATA DD_RS ON REQST.REQUEST_SOURCE= DD_RS.DOMAIN_DATA_ID " +
		" LEFT JOIN MAINTENANCE_TYPE MNTNC ON REQST.MAINTENANCE_TYPE_ID=MNTNC.MAINTENANCE_TYPE_ID " +
        " left JOIN DOMAIN_DATA DD_RST ON REQST.STATUS_ID= DD_RST.DOMAIN_DATA_ID " +
        " LEFT JOIN TENDER_WORK_SCOPE TWS ON CNTRT.CONTRACT_ID = TWS.CONTRACT_ID "+
        " LEFT JOIN WORK_TYPE WT ON WT.WORK_TYPE_ID = TWS.WORK_TYPE_ID " +
		" LEFT JOIN DOMAIN_DATA DD_RP ON REQST.REQUEST_PRIORITY_ID = DD_RP.DOMAIN_DATA_ID ";
		

		///////////////////////////////
	                    
				String condition="";
				
							if(criteria.getMaintenanceRequestNo().length() >0)
								condition+=" AND lower(REQST.REQUEST_NUMBER) like lower('%" + criteria.getMaintenanceRequestNo() +"%')";
							if(criteria.getContractorName() .length() >0)
								condition+= " AND (lower(PRSN.FIRST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +
												" OR lower(PRSN.MIDDLE_NAME) like  lower( '%" + criteria.getContractorName() +"%')" +
												" OR lower(PRSN.LAST_NAME) like lower( '%" + criteria.getContractorName() +"%')" +			
												" ) " ;					
							if(criteria.getPropertyName().length() >0)
								condition+=" AND lower(PROP.COMMERCIAL_NAME ) like lower('%" + criteria.getPropertyName() +"%')";
							
							if(criteria.getMaintenanceRequestDateFrom()!=null  && criteria.getMaintenanceRequestDateTo()!=null)								
								condition += " AND REQST.REQUEST_DATE between to_date ('"+ DateUtil.convertToDisplayDate( criteria.getMaintenanceRequestDateFrom() ) +"', 'dd/MM/yyyy')  AND to_date ('" + DateUtil.convertToDisplayDate( criteria.getMaintenanceRequestDateTo())+"', 'dd/MM/yyyy')";
						if(!criteria.getRequestSource().equals("-1"))
								condition+=" AND REQST.REQUEST_SOURCE ="+  criteria.getRequestSource();
						if(!criteria.getMaintenanceWorkType().equals("-1"))
							condition+= " AND WT.WORK_TYPE_ID  =  " + criteria.getMaintenanceWorkType() 	;
						if(!criteria.getOwnershipType().equals("-1"))
							condition+= " AND PROP.OWNERSHIP_TYPE_ID  =  " + criteria.getOwnershipType()	;
						if(!criteria.getMaintenanceType().equals("-1"))
								condition+= " AND MNTNC.MAINTENANCE_TYPE_ID  =  '" + criteria.getMaintenanceType() +"'"	;
						if(!criteria.getUnderContract().equals("-1"))
							condition+= " AND lower(REQST.WITHIN_CONTRACT)  like lower('%" + criteria.getUnderContract() +"%')"	;
						if(!criteria.getRequestStatus().equals("-1"))
							condition+= " AND REQST.STATUS_ID  = " + criteria.getRequestStatus();
						if(!criteria.getMaintenancePriority().equals("-1"))
							condition+= " AND REQST.REQUEST_PRIORITY_ID  =" + criteria.getMaintenancePriority()	;
												
						
							
							if(condition.length() > 0)
					        	query+=" WHERE 1=1 AND REQST.REQUEST_TYPE_ID=14 \n"+ condition;
							else
								query+=" WHERE 1=1 AND REQST.REQUEST_TYPE_ID=14 \n";
							
		
		System.out.print("-------------" + query);
		String dataDef = ReportConstant.DataDef.MAINTENANCE_REQUESTS_REPORT;        
		reportQueryMap.put(dataDef, query+"");
        query = "SELECT \n" + 
        
        
 
        /*******************************                           English Labels             *********************************/
		
        		"  '" + ReportMessageResource.getPropertyEn("request.numberCol") + "' \"lblMaintenanceRequestNoEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("serviceContract.contractor.name") + "' \"lblContractorNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("attachment.datetime") + "' \"lblMaintenanceDateEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("chequeList.propertyCommercialName") + "' \"lblPropertyNameEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("receiveProperty.ownershipType") + "' \"lblOwnershipTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("request.source") + "' \"lblRequestSourceEn\",\n" +        		
        		"  '" + ReportMessageResource.getPropertyEn("applicationDetails.maintenanceType") + "' \"lblMaintenanceTypeEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("commons.underContract") + "' \"lblUnderContractEn\",\n" + 
        		"  '" + ReportMessageResource.getPropertyEn("commons.maintenancePriority") + "' \"lblMaintenancePriorityEn\",\n" +
        		"  '" + ReportMessageResource.getPropertyEn("request.requestStatus") + "' \"lblRequestStatusEn\",\n" +
        		
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.name.maintenanceRequestsReport.rpt") + "' \"lblReportNameEn\",\n" +
		        "  '" + ReportMessageResource.getPropertyEn("commons.report.pims.header") + "' \"lblPIMSHeaderEn\" ,\n" +
		        
		        /*******************************                           Arabic Labels             *********************************/
		        "  '" + ReportMessageResource.getPropertyAr("request.numberCol") + "' \"lblMaintenanceRequestNoAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("serviceContract.contractor.name") + "' \"lblContractorNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("attachment.datetime") + "' \"lblMaintenanceDateAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("chequeList.propertyCommercialName") + "' \"lblPropertyNameAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("receiveProperty.ownershipType") + "' \"lblOwnershipTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("request.source") + "' \"lblRequestSourceAr\",\n" +        		
        		"  '" + ReportMessageResource.getPropertyAr("applicationDetails.maintenanceType") + "' \"lblMaintenanceTypeAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("commons.underContract") + "' \"lblUnderContractAr\",\n" + 
        		"  '" + ReportMessageResource.getPropertyAr("commons.maintenancePriority") + "' \"lblMaintenancePriorityAr\",\n" +
        		"  '" + ReportMessageResource.getPropertyAr("request.requestStatus") + "' \"lblRequestStatusAr\",\n" +
		       
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.name.maintenanceRequestsReport.rpt") + "' \"lblReportNameAr\",\n" +
		        "  '" + ReportMessageResource.getPropertyAr("commons.report.pims.header") + "' \"lblPIMSHeaderAr\" , \n" +
		        
		        
	           "    '"+ criteria.getReportGeneratedBy() +"' \"lblLoggedInUser\"\n" + 

		        "FROM DUAL";
		dataDef = ReportConstant.DataDef.MAINTENANCE_REQUESTS_REPORT_LABEL;  
		
		System.out.print("-------------" + query);
		reportQueryMap.put(dataDef, query);         
        
        return reportQueryMap;
	}
			

}
