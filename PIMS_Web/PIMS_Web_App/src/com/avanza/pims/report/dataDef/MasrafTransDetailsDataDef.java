package com.avanza.pims.report.dataDef;



public class MasrafTransDetailsDataDef extends AbstractReportDataDef 

{
	private String criteriaMasrafId;
	private String fromEndowmentName;
	private String fromEndowmentId;
	private String masrafId;
	private String showSubMasrafTrx;
	private String masrafName;
	private String fromCostCenter;
	private String trxDate;
	private Double credit;
	private Double debit;
	private Double amount;
	private Double openingBalanceFromMainReport;
	private String descriptionAr;
	private String descriptionEn;
	private String trxTypeEn;
	private String trxTypeAr;
	private String transId;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	

	public String getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}
	public String getDescriptionEn() {
		return descriptionEn;
	}
	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}
	public String getTrxTypeEn() {
		return trxTypeEn;
	}
	public void setTrxTypeEn(String trxTypeEn) {
		this.trxTypeEn = trxTypeEn;
	}
	public String getTrxTypeAr() {
		return trxTypeAr;
	}
	public void setTrxTypeAr(String trxTypeAr) {
		this.trxTypeAr = trxTypeAr;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getMasrafId() {
		return masrafId;
	}
	public void setMasrafId(String masrafId) {
		this.masrafId = masrafId;
	}
	public String getFromCostCenter() {
		return fromCostCenter;
	}
	public void setFromCostCenter(String fromCostCenter) {
		this.fromCostCenter = fromCostCenter;
	}

	public String getFromEndowmentName() {
		return fromEndowmentName;
	}
	public void setFromEndowmentName(String fromEndowmentName) {
		this.fromEndowmentName = fromEndowmentName;
	}
	public String getFromEndowmentId() {
		return fromEndowmentId;
	}
	public void setFromEndowmentId(String fromEndowmentId) {
		this.fromEndowmentId = fromEndowmentId;
	}
	public String getShowSubMasrafTrx() {
		return showSubMasrafTrx;
	}
	public void setShowSubMasrafTrx(String showSubMasrafTrx) {
		this.showSubMasrafTrx = showSubMasrafTrx;
	}
	public String getMasrafName() {
		return masrafName;
	}
	public void setMasrafName(String masrafName) {
		this.masrafName = masrafName;
	}
	public String getCriteriaMasrafId() {
		return criteriaMasrafId;
	}
	public void setCriteriaMasrafId(String criteriaMasrafId) {
		this.criteriaMasrafId = criteriaMasrafId;
	}
	public Double getOpeningBalanceFromMainReport() {
		return openingBalanceFromMainReport;
	}
	public void setOpeningBalanceFromMainReport(Double openingBalanceFromMainReport) {
		this.openingBalanceFromMainReport = openingBalanceFromMainReport;
	}
	
	
}