package com.avanza.pims.report.dataDef;

public class PendingPaymentsAfterSettlementLabelDataDef extends AbstractReportDataDef {
    /*******************************                           English Labels             *********************************/
	

	
    /*******************************                           Arabic Labels             *********************************/
	
	private String lblContractNumberAr;
	private String lblTenantNameAr;
	private String lblGRPCustomerNoAr;
	
	private String lblPropertyNameAr;
	private String lblUnitDescAr;
	private String lblUnitAccNoAr;
	
	private String lblAmountAr;
	private String lblPaymentTypeAr;
	
	
	private String lblEF1;
	private String lblEF2;
	private String lblEF3;
	private String lblEF4;
	private String lblEF5;
	private String lblEF6;
	private String lblEF7;
	private String lblEF8;
	private String lblEF9;
	
	private String lblEF10;
	private String lblEF11;
	private String lblEF12;
	private String lblEF13;
	private String lblEF14;
	private String lblEF15;
	private String lblEF16;
	private String lblEF17;
	private String lblEF18;
	private String lblEF19;
	private String lblEF20;
	
    
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	

	
	public String getLblAmountAr() {
		return lblAmountAr;
	}
	public void setLblAmountAr(String lblAmountAr) {
		this.lblAmountAr = lblAmountAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}


	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblGRPCustomerNoAr() {
		return lblGRPCustomerNoAr;
	}
	public void setLblGRPCustomerNoAr(String lblGRPCustomerNoAr) {
		this.lblGRPCustomerNoAr = lblGRPCustomerNoAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitDescAr() {
		return lblUnitDescAr;
	}
	public void setLblUnitDescAr(String lblUnitDescAr) {
		this.lblUnitDescAr = lblUnitDescAr;
	}
	public String getLblUnitAccNoAr() {
		return lblUnitAccNoAr;
	}
	public void setLblUnitAccNoAr(String lblUnitAccNoAr) {
		this.lblUnitAccNoAr = lblUnitAccNoAr;
	}
	public String getLblEF1() {
		return lblEF1;
	}
	public void setLblEF1(String lblEF1) {
		this.lblEF1 = lblEF1;
	}
	public String getLblEF2() {
		return lblEF2;
	}
	public void setLblEF2(String lblEF2) {
		this.lblEF2 = lblEF2;
	}
	public String getLblEF3() {
		return lblEF3;
	}
	public void setLblEF3(String lblEF3) {
		this.lblEF3 = lblEF3;
	}
	public String getLblEF4() {
		return lblEF4;
	}
	public void setLblEF4(String lblEF4) {
		this.lblEF4 = lblEF4;
	}
	public String getLblEF5() {
		return lblEF5;
	}
	public void setLblEF5(String lblEF5) {
		this.lblEF5 = lblEF5;
	}
	public String getLblEF6() {
		return lblEF6;
	}
	public void setLblEF6(String lblEF6) {
		this.lblEF6 = lblEF6;
	}
	public String getLblEF7() {
		return lblEF7;
	}
	public void setLblEF7(String lblEF7) {
		this.lblEF7 = lblEF7;
	}
	public String getLblEF8() {
		return lblEF8;
	}
	public void setLblEF8(String lblEF8) {
		this.lblEF8 = lblEF8;
	}
	public String getLblEF9() {
		return lblEF9;
	}
	public void setLblEF9(String lblEF9) {
		this.lblEF9 = lblEF9;
	}
	public String getLblEF10() {
		return lblEF10;
	}
	public void setLblEF10(String lblEF10) {
		this.lblEF10 = lblEF10;
	}
	public String getLblEF11() {
		return lblEF11;
	}
	public void setLblEF11(String lblEF11) {
		this.lblEF11 = lblEF11;
	}
	public String getLblEF12() {
		return lblEF12;
	}
	public void setLblEF12(String lblEF12) {
		this.lblEF12 = lblEF12;
	}
	public String getLblEF13() {
		return lblEF13;
	}
	public void setLblEF13(String lblEF13) {
		this.lblEF13 = lblEF13;
	}
	public String getLblEF14() {
		return lblEF14;
	}
	public void setLblEF14(String lblEF14) {
		this.lblEF14 = lblEF14;
	}
	public String getLblEF15() {
		return lblEF15;
	}
	public void setLblEF15(String lblEF15) {
		this.lblEF15 = lblEF15;
	}
	public String getLblEF16() {
		return lblEF16;
	}
	public void setLblEF16(String lblEF16) {
		this.lblEF16 = lblEF16;
	}
	public String getLblEF17() {
		return lblEF17;
	}
	public void setLblEF17(String lblEF17) {
		this.lblEF17 = lblEF17;
	}
	public String getLblEF18() {
		return lblEF18;
	}
	public void setLblEF18(String lblEF18) {
		this.lblEF18 = lblEF18;
	}
	public String getLblEF19() {
		return lblEF19;
	}
	public void setLblEF19(String lblEF19) {
		this.lblEF19 = lblEF19;
	}
	public String getLblEF20() {
		return lblEF20;
	}
	public void setLblEF20(String lblEF20) {
		this.lblEF20 = lblEF20;
	}
	

}
