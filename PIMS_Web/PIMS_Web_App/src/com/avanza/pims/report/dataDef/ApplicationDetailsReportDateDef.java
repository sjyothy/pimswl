package com.avanza.pims.report.dataDef;

public class ApplicationDetailsReportDateDef extends AbstractReportDataDef
{
	private String applicationNumber;

	
	private String applicationStatusAr;
	private String applicationStatusEn;
	

	private String applicationName;


	private String applicationTypeEn;
	private String applicationTypeAr;
	
	private String applicationDateFrom;
	private String applicationDescription;
	
	private String propertyName;


	private String propertyOwnershipTypeEn;
	private String propertyOwnershipTypeAr;
	

	private String unitNumber;
	private String unitTypeEn;
	private String unitTypeAr;
	

	private String contractNumber;
	
	private String contractStatusEn;
	private String contractStatusAr;
	
	private String tenantName;
	private String tenantTypeEn;
	private String tenantTypeAr;
    private String settlementDate;	
    private String field1;
    private String field2;
    private String field3;
    private String field4;
    private String field5;
    private String field6;
    private String field7;
    private String field8;
    private String field9;
    private String field10;
	
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}


	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getApplicationDateFrom() {
		return applicationDateFrom;
	}
	public void setApplicationDateFrom(String applicationDateFrom) {
		this.applicationDateFrom = applicationDateFrom;
	}
	public String getApplicationDescription() {
		return applicationDescription;
	}
	public void setApplicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	


	public String getApplicationStatusAr() {
		return applicationStatusAr;
	}
	public void setApplicationStatusAr(String applicationStatusAr) {
		this.applicationStatusAr = applicationStatusAr;
	}
	public String getApplicationStatusEn() {
		return applicationStatusEn;
	}
	public void setApplicationStatusEn(String applicationStatusEn) {
		this.applicationStatusEn = applicationStatusEn;
	}
	public String getApplicationTypeEn() {
		return applicationTypeEn;
	}
	public void setApplicationTypeEn(String applicationTypeEn) {
		this.applicationTypeEn = applicationTypeEn;
	}
	public String getApplicationTypeAr() {
		return applicationTypeAr;
	}
	public void setApplicationTypeAr(String applicationTypeAr) {
		this.applicationTypeAr = applicationTypeAr;
	}
	public String getPropertyOwnershipTypeEn() {
		return propertyOwnershipTypeEn;
	}
	public void setPropertyOwnershipTypeEn(String propertyOwnershipTypeEn) {
		this.propertyOwnershipTypeEn = propertyOwnershipTypeEn;
	}
	public String getPropertyOwnershipTypeAr() {
		return propertyOwnershipTypeAr;
	}
	public void setPropertyOwnershipTypeAr(String propertyOwnershipTypeAr) {
		this.propertyOwnershipTypeAr = propertyOwnershipTypeAr;
	}
	public String getUnitTypeEn() {
		return unitTypeEn;
	}
	public void setUnitTypeEn(String unitTypeEn) {
		this.unitTypeEn = unitTypeEn;
	}
	public String getUnitTypeAr() {
		return unitTypeAr;
	}
	public void setUnitTypeAr(String unitTypeAr) {
		this.unitTypeAr = unitTypeAr;
	}
	public String getContractStatusEn() {
		return contractStatusEn;
	}
	public void setContractStatusEn(String contractStatusEn) {
		this.contractStatusEn = contractStatusEn;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}
	public String getTenantTypeEn() {
		return tenantTypeEn;
	}
	public void setTenantTypeEn(String tenantTypeEn) {
		this.tenantTypeEn = tenantTypeEn;
	}
	public String getTenantTypeAr() {
		return tenantTypeAr;
	}
	public void setTenantTypeAr(String tenantTypeAr) {
		this.tenantTypeAr = tenantTypeAr;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
}
