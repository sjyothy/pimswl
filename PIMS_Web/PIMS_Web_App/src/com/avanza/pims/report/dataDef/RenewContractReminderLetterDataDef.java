package com.avanza.pims.report.dataDef;

public class RenewContractReminderLetterDataDef extends AbstractReportDataDef {
	
	private String community;
	private String propertyName;
	
	private String telephoneNo;
	private String contractNo;
	private String unitNo;
	private String expiryDate;
	private String renewalRent;
	
	private String personFirstName;
	private String personMiddleName;
	private String personLastName;
	private String personCompanyName;
	
	
	
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getTelephoneNo() {
		return telephoneNo;
	}
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getRenewalRent() {
		return renewalRent;
	}
	public void setRenewalRent(String renewalRent) {
		this.renewalRent = renewalRent;
	}
	public String getPersonFirstName() {
		return personFirstName;
	}
	public void setPersonFirstName(String personFirstName) {
		this.personFirstName = personFirstName;
	}
	public String getPersonMiddleName() {
		return personMiddleName;
	}
	public void setPersonMiddleName(String personMiddleName) {
		this.personMiddleName = personMiddleName;
	}
	public String getPersonLastName() {
		return personLastName;
	}
	public void setPersonLastName(String personLastName) {
		this.personLastName = personLastName;
	}
	public String getPersonCompanyName() {
		return personCompanyName;
	}
	public void setPersonCompanyName(String personCompanyName) {
		this.personCompanyName = personCompanyName;
	}

}
