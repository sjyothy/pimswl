package com.avanza.pims.report.dataDef;


public class PortfolioReportLabelDataDef extends AbstractReportDataDef
{
	/*******************************                           English Labels             *********************************/

    private String aggregateAppDepEn;
    private String assetClassNameEn;
    private String investmentAssetNameEn;
    private String totalInvestmentCostEn;
    private String feesEn;
    private String investmentPeriodEn;
    private String noUnitsEn;
    private String currnoUnitsEn;
    private String unitPriceEn;
    private String currunitPriceEn;
    private String evalPrvQrtEn;
    private String evalCurrQrtEn;
    private String qrtAppDepEn;
    private String realizedProfitAggEn;
    private String qrtROIEn;
    private String aggROIEn;
    private String percentTotalInvEn;
    private String weightedROIEn;
    private String weightedqrtROIEn;
    private String realizedProfitQrtEn;
    private String accountEn;
    private String accountPercentageEn;
    
	/*******************************                           Arabic Labels             *********************************/
    private String aggregateAppDepAr;
    private String assetClassNameAr;
    private String investmentAssetNameAr;
    private String totalInvestmentCostAr;
    private String feesAr;
    private String investmentPeriodAr;
    private String noUnitsAr;
    private String currnoUnitsAr;
    private String unitPriceAr;
    private String currunitPriceAr;
    private String evalPrvQrtAr;
    private String evalCurrQrtAr;
    private String qrtAppDepAr;
    private String realizedProfitAggAr;
    private String qrtROIAr;
    private String aggROIAr;
    private String percentTotalInvAr;
    private String weightedROIAr;
    private String weightedqrtROIAr;
    private String realizedProfitQrtAr;
    private String accountAr;
    private String accountPercentageAr;
    
    
    
    /*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getAggregateAppDepEn() {
		return aggregateAppDepEn;
	}
	public void setAggregateAppDepEn(String aggregateAppDepEn) {
		this.aggregateAppDepEn = aggregateAppDepEn;
	}
	public String getAssetClassNameEn() {
		return assetClassNameEn;
	}
	public void setAssetClassNameEn(String assetClassNameEn) {
		this.assetClassNameEn = assetClassNameEn;
	}
	public String getInvestmentAssetNameEn() {
		return investmentAssetNameEn;
	}
	public void setInvestmentAssetNameEn(String investmentAssetNameEn) {
		this.investmentAssetNameEn = investmentAssetNameEn;
	}
	public String getTotalInvestmentCostEn() {
		return totalInvestmentCostEn;
	}
	public void setTotalInvestmentCostEn(String totalInvestmentCostEn) {
		this.totalInvestmentCostEn = totalInvestmentCostEn;
	}
	public String getFeesEn() {
		return feesEn;
	}
	public void setFeesEn(String feesEn) {
		this.feesEn = feesEn;
	}
	public String getInvestmentPeriodEn() {
		return investmentPeriodEn;
	}
	public void setInvestmentPeriodEn(String investmentPeriodEn) {
		this.investmentPeriodEn = investmentPeriodEn;
	}
	public String getNoUnitsEn() {
		return noUnitsEn;
	}
	public void setNoUnitsEn(String noUnitsEn) {
		this.noUnitsEn = noUnitsEn;
	}
	public String getCurrnoUnitsEn() {
		return currnoUnitsEn;
	}
	public void setCurrnoUnitsEn(String currnoUnitsEn) {
		this.currnoUnitsEn = currnoUnitsEn;
	}
	public String getUnitPriceEn() {
		return unitPriceEn;
	}
	public void setUnitPriceEn(String unitPriceEn) {
		this.unitPriceEn = unitPriceEn;
	}
	public String getCurrunitPriceEn() {
		return currunitPriceEn;
	}
	public void setCurrunitPriceEn(String currunitPriceEn) {
		this.currunitPriceEn = currunitPriceEn;
	}
	public String getEvalPrvQrtEn() {
		return evalPrvQrtEn;
	}
	public void setEvalPrvQrtEn(String evalPrvQrtEn) {
		this.evalPrvQrtEn = evalPrvQrtEn;
	}
	public String getEvalCurrQrtEn() {
		return evalCurrQrtEn;
	}
	public void setEvalCurrQrtEn(String evalCurrQrtEn) {
		this.evalCurrQrtEn = evalCurrQrtEn;
	}
	public String getQrtAppDepEn() {
		return qrtAppDepEn;
	}
	public void setQrtAppDepEn(String qrtAppDepEn) {
		this.qrtAppDepEn = qrtAppDepEn;
	}
	public String getRealizedProfitAggEn() {
		return realizedProfitAggEn;
	}
	public void setRealizedProfitAggEn(String realizedProfitAggEn) {
		this.realizedProfitAggEn = realizedProfitAggEn;
	}
	public String getQrtROIEn() {
		return qrtROIEn;
	}
	public void setQrtROIEn(String qrtROIEn) {
		this.qrtROIEn = qrtROIEn;
	}
	public String getAggROIEn() {
		return aggROIEn;
	}
	public void setAggROIEn(String aggROIEn) {
		this.aggROIEn = aggROIEn;
	}
	public String getPercentTotalInvEn() {
		return percentTotalInvEn;
	}
	public void setPercentTotalInvEn(String percentTotalInvEn) {
		this.percentTotalInvEn = percentTotalInvEn;
	}
	public String getWeightedROIEn() {
		return weightedROIEn;
	}
	public void setWeightedROIEn(String weightedROIEn) {
		this.weightedROIEn = weightedROIEn;
	}
	public String getWeightedqrtROIEn() {
		return weightedqrtROIEn;
	}
	public void setWeightedqrtROIEn(String weightedqrtROIEn) {
		this.weightedqrtROIEn = weightedqrtROIEn;
	}
	public String getAggregateAppDepAr() {
		return aggregateAppDepAr;
	}
	public void setAggregateAppDepAr(String aggregateAppDepAr) {
		this.aggregateAppDepAr = aggregateAppDepAr;
	}
	public String getAssetClassNameAr() {
		return assetClassNameAr;
	}
	public void setAssetClassNameAr(String assetClassNameAr) {
		this.assetClassNameAr = assetClassNameAr;
	}
	public String getInvestmentAssetNameAr() {
		return investmentAssetNameAr;
	}
	public void setInvestmentAssetNameAr(String investmentAssetNameAr) {
		this.investmentAssetNameAr = investmentAssetNameAr;
	}
	public String getTotalInvestmentCostAr() {
		return totalInvestmentCostAr;
	}
	public void setTotalInvestmentCostAr(String totalInvestmentCostAr) {
		this.totalInvestmentCostAr = totalInvestmentCostAr;
	}
	public String getFeesAr() {
		return feesAr;
	}
	public void setFeesAr(String feesAr) {
		this.feesAr = feesAr;
	}
	public String getInvestmentPeriodAr() {
		return investmentPeriodAr;
	}
	public void setInvestmentPeriodAr(String investmentPeriodAr) {
		this.investmentPeriodAr = investmentPeriodAr;
	}
	public String getNoUnitsAr() {
		return noUnitsAr;
	}
	public void setNoUnitsAr(String noUnitsAr) {
		this.noUnitsAr = noUnitsAr;
	}
	public String getCurrnoUnitsAr() {
		return currnoUnitsAr;
	}
	public void setCurrnoUnitsAr(String currnoUnitsAr) {
		this.currnoUnitsAr = currnoUnitsAr;
	}
	public String getUnitPriceAr() {
		return unitPriceAr;
	}
	public void setUnitPriceAr(String unitPriceAr) {
		this.unitPriceAr = unitPriceAr;
	}
	public String getCurrunitPriceAr() {
		return currunitPriceAr;
	}
	public void setCurrunitPriceAr(String currunitPriceAr) {
		this.currunitPriceAr = currunitPriceAr;
	}
	public String getEvalPrvQrtAr() {
		return evalPrvQrtAr;
	}
	public void setEvalPrvQrtAr(String evalPrvQrtAr) {
		this.evalPrvQrtAr = evalPrvQrtAr;
	}
	public String getEvalCurrQrtAr() {
		return evalCurrQrtAr;
	}
	public void setEvalCurrQrtAr(String evalCurrQrtAr) {
		this.evalCurrQrtAr = evalCurrQrtAr;
	}
	public String getQrtAppDepAr() {
		return qrtAppDepAr;
	}
	public void setQrtAppDepAr(String qrtAppDepAr) {
		this.qrtAppDepAr = qrtAppDepAr;
	}
	public String getRealizedProfitAggAr() {
		return realizedProfitAggAr;
	}
	public void setRealizedProfitAggAr(String realizedProfitAggAr) {
		this.realizedProfitAggAr = realizedProfitAggAr;
	}
	public String getQrtROIAr() {
		return qrtROIAr;
	}
	public void setQrtROIAr(String qrtROIAr) {
		this.qrtROIAr = qrtROIAr;
	}
	public String getAggROIAr() {
		return aggROIAr;
	}
	public void setAggROIAr(String aggROIAr) {
		this.aggROIAr = aggROIAr;
	}
	public String getPercentTotalInvAr() {
		return percentTotalInvAr;
	}
	public void setPercentTotalInvAr(String percentTotalInvAr) {
		this.percentTotalInvAr = percentTotalInvAr;
	}
	public String getWeightedROIAr() {
		return weightedROIAr;
	}
	public void setWeightedROIAr(String weightedROIAr) {
		this.weightedROIAr = weightedROIAr;
	}
	public String getWeightedqrtROIAr() {
		return weightedqrtROIAr;
	}
	public void setWeightedqrtROIAr(String weightedqrtROIAr) {
		this.weightedqrtROIAr = weightedqrtROIAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getRealizedProfitQrtEn() {
		return realizedProfitQrtEn;
	}
	public void setRealizedProfitQrtEn(String realizedProfitQrtEn) {
		this.realizedProfitQrtEn = realizedProfitQrtEn;
	}
	public String getRealizedProfitQrtAr() {
		return realizedProfitQrtAr;
	}
	public void setRealizedProfitQrtAr(String realizedProfitQrtAr) {
		this.realizedProfitQrtAr = realizedProfitQrtAr;
	}
	public String getAccountEn() {
		return accountEn;
	}
	public void setAccountEn(String accountEn) {
		this.accountEn = accountEn;
	}
	public String getAccountPercentageEn() {
		return accountPercentageEn;
	}
	public void setAccountPercentageEn(String accountPercentageEn) {
		this.accountPercentageEn = accountPercentageEn;
	}
	public String getAccountAr() {
		return accountAr;
	}
	public void setAccountAr(String accountAr) {
		this.accountAr = accountAr;
	}
	public String getAccountPercentageAr() {
		return accountPercentageAr;
	}
	public void setAccountPercentageAr(String accountPercentageAr) {
		this.accountPercentageAr = accountPercentageAr;
	}
	
}
