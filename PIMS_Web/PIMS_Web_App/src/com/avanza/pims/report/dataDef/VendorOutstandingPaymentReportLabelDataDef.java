package com.avanza.pims.report.dataDef;

public class VendorOutstandingPaymentReportLabelDataDef extends AbstractReportDataDef{

    /*******************************                           English Labels             *********************************/
	
	private String lblPaymentNumberEn;
	private String lblPaymentDateEn;
	private String lblPaymentTypeEn;
	private String lblPaymentAmountEn;
	private String lblVendorNumberEn;
	private String lblVendorNameEn;
	private String lblContractNumberEn;
	private String lblContractTypeEn;
	private String lblPaymentStatusEn;

    /*******************************                           Arabic Labels             *********************************/

	private String lblPaymentNumberAr;
	private String lblPaymentDateAr;
	private String lblPaymentTypeAr;
	private String lblPaymentAmountAr;
	private String lblVendorNumberAr;
	private String lblVendorNameAr;
	private String lblContractNumberAr;
	private String lblContractTypeAr;
	private String lblPaymentStatusAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblPaymentNumberEn() {
		return lblPaymentNumberEn;
	}
	public void setLblPaymentNumberEn(String lblPaymentNumberEn) {
		this.lblPaymentNumberEn = lblPaymentNumberEn;
	}
	public String getLblPaymentDateEn() {
		return lblPaymentDateEn;
	}
	public void setLblPaymentDateEn(String lblPaymentDateEn) {
		this.lblPaymentDateEn = lblPaymentDateEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblVendorNumberEn() {
		return lblVendorNumberEn;
	}
	public void setLblVendorNumberEn(String lblVendorNumberEn) {
		this.lblVendorNumberEn = lblVendorNumberEn;
	}
	public String getLblVendorNameEn() {
		return lblVendorNameEn;
	}
	public void setLblVendorNameEn(String lblVendorNameEn) {
		this.lblVendorNameEn = lblVendorNameEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractTypeEn() {
		return lblContractTypeEn;
	}
	public void setLblContractTypeEn(String lblContractTypeEn) {
		this.lblContractTypeEn = lblContractTypeEn;
	}
	public String getLblPaymentStatusEn() {
		return lblPaymentStatusEn;
	}
	public void setLblPaymentStatusEn(String lblPaymentStatusEn) {
		this.lblPaymentStatusEn = lblPaymentStatusEn;
	}
	public String getLblPaymentNumberAr() {
		return lblPaymentNumberAr;
	}
	public void setLblPaymentNumberAr(String lblPaymentNumberAr) {
		this.lblPaymentNumberAr = lblPaymentNumberAr;
	}
	public String getLblPaymentDateAr() {
		return lblPaymentDateAr;
	}
	public void setLblPaymentDateAr(String lblPaymentDateAr) {
		this.lblPaymentDateAr = lblPaymentDateAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblVendorNumberAr() {
		return lblVendorNumberAr;
	}
	public void setLblVendorNumberAr(String lblVendorNumberAr) {
		this.lblVendorNumberAr = lblVendorNumberAr;
	}
	public String getLblVendorNameAr() {
		return lblVendorNameAr;
	}
	public void setLblVendorNameAr(String lblVendorNameAr) {
		this.lblVendorNameAr = lblVendorNameAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractTypeAr() {
		return lblContractTypeAr;
	}
	public void setLblContractTypeAr(String lblContractTypeAr) {
		this.lblContractTypeAr = lblContractTypeAr;
	}
	public String getLblPaymentStatusAr() {
		return lblPaymentStatusAr;
	}
	public void setLblPaymentStatusAr(String lblPaymentStatusAr) {
		this.lblPaymentStatusAr = lblPaymentStatusAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
	

}
