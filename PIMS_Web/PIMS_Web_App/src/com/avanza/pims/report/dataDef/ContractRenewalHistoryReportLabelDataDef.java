package com.avanza.pims.report.dataDef;

public class ContractRenewalHistoryReportLabelDataDef extends AbstractReportDataDef {

    /*******************************                           English Labels             *********************************/
	
	private String lblPropertyNameEn;
	private String lblOwnershipTypeEn;
	private String lblEmiratesEn;	
	private String lblAreaEn;				
	private String lblUnitNumberEn;
	private String lblContractNumberEn;
	private String lblContractStartDateEn;	 			
	private String lblContractEndDateEn;	  			
	private String lblContractRentAmountEn;	 			
	private String lblTenantNameEn;	 			
	private String lblContractTypeEn;	 			
	private String lblContractStatusEn;

    /*******************************                           Arabic Labels             *********************************/
	
	private String lblPropertyNameAr;
	private String lblOwnershipTypeAr;
	private String lblEmiratesAr;	
	private String lblAreaAr;				
	private String lblUnitNumberAr;
	private String lblContractNumberAr;
	private String lblContractStartDateAr;	 			
	private String lblContractEndDateAr;	  			
	private String lblContractRentAmountAr;	 			
	private String lblTenantNameAr;	 			
	private String lblContractTypeAr;	 			
	private String lblContractStatusAr;

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblOwnershipTypeEn() {
		return lblOwnershipTypeEn;
	}
	public void setLblOwnershipTypeEn(String lblOwnershipTypeEn) {
		this.lblOwnershipTypeEn = lblOwnershipTypeEn;
	}
	public String getLblEmiratesEn() {
		return lblEmiratesEn;
	}
	public void setLblEmiratesEn(String lblEmiratesEn) {
		this.lblEmiratesEn = lblEmiratesEn;
	}
	public String getLblAreaEn() {
		return lblAreaEn;
	}
	public void setLblAreaEn(String lblAreaEn) {
		this.lblAreaEn = lblAreaEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblContractRentAmountEn() {
		return lblContractRentAmountEn;
	}
	public void setLblContractRentAmountEn(String lblContractRentAmountEn) {
		this.lblContractRentAmountEn = lblContractRentAmountEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblContractTypeEn() {
		return lblContractTypeEn;
	}
	public void setLblContractTypeEn(String lblContractTypeEn) {
		this.lblContractTypeEn = lblContractTypeEn;
	}
	public String getLblContractStatusEn() {
		return lblContractStatusEn;
	}
	public void setLblContractStatusEn(String lblContractStatusEn) {
		this.lblContractStatusEn = lblContractStatusEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblOwnershipTypeAr() {
		return lblOwnershipTypeAr;
	}
	public void setLblOwnershipTypeAr(String lblOwnershipTypeAr) {
		this.lblOwnershipTypeAr = lblOwnershipTypeAr;
	}
	public String getLblEmiratesAr() {
		return lblEmiratesAr;
	}
	public void setLblEmiratesAr(String lblEmiratesAr) {
		this.lblEmiratesAr = lblEmiratesAr;
	}
	public String getLblAreaAr() {
		return lblAreaAr;
	}
	public void setLblAreaAr(String lblAreaAr) {
		this.lblAreaAr = lblAreaAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblContractRentAmountAr() {
		return lblContractRentAmountAr;
	}
	public void setLblContractRentAmountAr(String lblContractRentAmountAr) {
		this.lblContractRentAmountAr = lblContractRentAmountAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblContractTypeAr() {
		return lblContractTypeAr;
	}
	public void setLblContractTypeAr(String lblContractTypeAr) {
		this.lblContractTypeAr = lblContractTypeAr;
	}
	public String getLblContractStatusAr() {
		return lblContractStatusAr;
	}
	public void setLblContractStatusAr(String lblContractStatusAr) {
		this.lblContractStatusAr = lblContractStatusAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
	
	
	
	

}
