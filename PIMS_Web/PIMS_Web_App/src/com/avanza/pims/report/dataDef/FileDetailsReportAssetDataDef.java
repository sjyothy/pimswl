package com.avanza.pims.report.dataDef;


public class FileDetailsReportAssetDataDef extends AbstractReportDataDef
{
	   private Long iAssetId;
	   private Long fileId;
	   private Long assetid;
	   private Long isFinal;
	   private Long isConfirm;
	   private String assetNameEn;
	   private String assetNameAr;
	   private String assetDesc;
	   private String assetNumber;
	   private Double expRevenue;
	   private Double amount;
	   private Long isIncomeExp;
	   private Long govtDeptId;
	   private String filedNumber;
	   private Long regionId;
	   private String fieldStatus;
	   private Double devolution;
	   private String fieldType;
	   private String vehicalCat;
	   private String fieldParty;
	   private String company;
	   private String licenceName;
	   private Long  bankId;
	   private String accountName;
	   private Long isAmafAccount;
	   private Long revenueType;
	   private String fromDate;
	   private String dueDate;
	   private String regionName;
	   private String assetTypeEn;
	   private String assetTypeAr;
	   private String revenueTypeEn;
	   private String revenueTypeAr;
	   private String filed1;//isManagerAmaf;
	   private String filed2;//managerId;
	   private String filed3;//managerName;
	   private String filed4;
	   private String filed5;
	   private String filed6;
	   private String filed7;
	   private String filed8;
	   private String filed9;
	   private String filed10;
	public Long getIAssetId() {
		return iAssetId;
	}
	public void setIAssetId(Long assetId) {
		iAssetId = assetId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public Long getAssetid() {
		return assetid;
	}
	public void setAssetid(Long assetid) {
		this.assetid = assetid;
	}
	public Long getIsFinal() {
		return isFinal;
	}
	public void setIsFinal(Long isFinal) {
		this.isFinal = isFinal;
	}
	public Long getIsConfirm() {
		return isConfirm;
	}
	public void setIsConfirm(Long isConfirm) {
		this.isConfirm = isConfirm;
	}
	public String getAssetNameEn() {
		return assetNameEn;
	}
	public void setAssetNameEn(String assetNameEn) {
		this.assetNameEn = assetNameEn;
	}
	public String getAssetNameAr() {
		return assetNameAr;
	}
	public void setAssetNameAr(String assetNameAr) {
		this.assetNameAr = assetNameAr;
	}
	public String getAssetDesc() {
		return assetDesc;
	}
	public void setAssetDesc(String assetDesc) {
		this.assetDesc = assetDesc;
	}
	public String getAssetNumber() {
		return assetNumber;
	}
	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}
	public Double getExpRevenue() {
		return expRevenue;
	}
	public void setExpRevenue(Double expRevenue) {
		this.expRevenue = expRevenue;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Long getIsIncomeExp() {
		return isIncomeExp;
	}
	public void setIsIncomeExp(Long isIncomeExp) {
		this.isIncomeExp = isIncomeExp;
	}
	public Long getGovtDeptId() {
		return govtDeptId;
	}
	public void setGovtDeptId(Long govtDeptId) {
		this.govtDeptId = govtDeptId;
	}
	public String getFiledNumber() {
		return filedNumber;
	}
	public void setFiledNumber(String filedNumber) {
		this.filedNumber = filedNumber;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public String getFieldStatus() {
		return fieldStatus;
	}
	public void setFieldStatus(String fieldStatus) {
		this.fieldStatus = fieldStatus;
	}
	public Double getDevolution() {
		return devolution;
	}
	public void setDevolution(Double devolution) {
		this.devolution = devolution;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getVehicalCat() {
		return vehicalCat;
	}
	public void setVehicalCat(String vehicalCat) {
		this.vehicalCat = vehicalCat;
	}
	public String getFieldParty() {
		return fieldParty;
	}
	public void setFieldParty(String fieldParty) {
		this.fieldParty = fieldParty;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLicenceName() {
		return licenceName;
	}
	public void setLicenceName(String licenceName) {
		this.licenceName = licenceName;
	}
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Long getIsAmafAccount() {
		return isAmafAccount;
	}
	public void setIsAmafAccount(Long isAmafAccount) {
		this.isAmafAccount = isAmafAccount;
	}
	public Long getRevenueType() {
		return revenueType;
	}
	public void setRevenueType(Long revenueType) {
		this.revenueType = revenueType;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getAssetTypeEn() {
		return assetTypeEn;
	}
	public void setAssetTypeEn(String assetTypeEn) {
		this.assetTypeEn = assetTypeEn;
	}
	public String getAssetTypeAr() {
		return assetTypeAr;
	}
	public void setAssetTypeAr(String assetTypeAr) {
		this.assetTypeAr = assetTypeAr;
	}
	public String getRevenueTypeEn() {
		return revenueTypeEn;
	}
	public void setRevenueTypeEn(String revenueTypeEn) {
		this.revenueTypeEn = revenueTypeEn;
	}
	public String getRevenueTypeAr() {
		return revenueTypeAr;
	}
	public void setRevenueTypeAr(String revenueTypeAr) {
		this.revenueTypeAr = revenueTypeAr;
	}
	public String getFiled1() {
		return filed1;
	}
	public void setFiled1(String filed1) {
		this.filed1 = filed1;
	}
	public String getFiled2() {
		return filed2;
	}
	public void setFiled2(String filed2) {
		this.filed2 = filed2;
	}
	public String getFiled3() {
		return filed3;
	}
	public void setFiled3(String filed3) {
		this.filed3 = filed3;
	}
	public String getFiled4() {
		return filed4;
	}
	public void setFiled4(String filed4) {
		this.filed4 = filed4;
	}
	public String getFiled5() {
		return filed5;
	}
	public void setFiled5(String filed5) {
		this.filed5 = filed5;
	}
	public String getFiled6() {
		return filed6;
	}
	public void setFiled6(String filed6) {
		this.filed6 = filed6;
	}
	public String getFiled7() {
		return filed7;
	}
	public void setFiled7(String filed7) {
		this.filed7 = filed7;
	}
	public String getFiled8() {
		return filed8;
	}
	public void setFiled8(String filed8) {
		this.filed8 = filed8;
	}
	public String getFiled9() {
		return filed9;
	}
	public void setFiled9(String filed9) {
		this.filed9 = filed9;
	}
	public String getFiled10() {
		return filed10;
	}
	public void setFiled10(String filed10) {
		this.filed10 = filed10;
	}
}
	