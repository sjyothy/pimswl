package com.avanza.pims.report.dataDef;

public class TenantStatementReportDataDef extends AbstractReportDataDef{

	private String tenantName;
	private String contractNumber;
	private String contractStartDate;
	private String contractEndDate;
	private String propertyName;
	private String unitNumber;
	private String contractRentValue;
	private String receiptNumber;
	private String tenantId;
	
	private String paymentTypeEn;	
	private String paymentTypeAr;
	
	private String receiptAmount;
	
	private String statusEn;
	private String statusAr;
	
	private String paymentMethod;	
	private String receiptTransactionDate;	

	private String totalOutstandingAmount;
	private String totalReceiptAmount;
	private String totalClearedAmount;
	private String totalBalanceAmount;
	private String grpNumber;
	
	private String statusID;
	
	
	
	public String getStatusID() {
		return statusID;
	}
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractRentValue() {
		return contractRentValue;
	}
	public void setContractRentValue(String contractRentValue) {
		this.contractRentValue = contractRentValue;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getTotalOutstandingAmount() {
		return totalOutstandingAmount;
	}
	public void setTotalOutstandingAmount(String totalOutstandingAmount) {
		this.totalOutstandingAmount = totalOutstandingAmount;
	}
	public String getTotalReceiptAmount() {
		return totalReceiptAmount;
	}
	public void setTotalReceiptAmount(String totalReceiptAmount) {
		this.totalReceiptAmount = totalReceiptAmount;
	}
	public String getTotalClearedAmount() {
		return totalClearedAmount;
	}
	public void setTotalClearedAmount(String totalClearedAmount) {
		this.totalClearedAmount = totalClearedAmount;
	}
	public String getTotalBalanceAmount() {
		return totalBalanceAmount;
	}
	public void setTotalBalanceAmount(String totalBalanceAmount) {
		this.totalBalanceAmount = totalBalanceAmount;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getReceiptTransactionDate() {
		return receiptTransactionDate;
	}
	public void setReceiptTransactionDate(String receiptTransactionDate) {
		this.receiptTransactionDate = receiptTransactionDate;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getGrpNumber() {
		return grpNumber;
	}
	public void setGrpNumber(String grpNumber) {
		this.grpNumber = grpNumber;
	}
	
	
}
