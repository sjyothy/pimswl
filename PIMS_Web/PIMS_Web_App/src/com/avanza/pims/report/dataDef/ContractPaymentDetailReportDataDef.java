package com.avanza.pims.report.dataDef;

public class ContractPaymentDetailReportDataDef extends AbstractReportDataDef {
	private String contractNumber;
	private String contractIssueDate;
	private String contractEndDate;
	private String contractDate;
	private String propertyComercialName;
	private String unitNo;
	private String tenantFirstName;
	private String tenantMiddleName;
	private String tenantLastName;
	private String paymentNo;
	private String paymentDueDate;
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentMethod;
	private String chequeNumber;
	private String chequeBankNameEn;
	private String chequeBankNameAr;
	private String paymentAmount;
	private String paymentStatusEn;
	private String paymentStatusAr;
	private String totalSumAmount;
	private String requestId; 
	private String requestTypeEn;
	private String requestTypeAr;
	private String costCenter;
	private String receiptNumber;
	private String contractStatusAr;
	
	
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractIssueDate() {
		return contractIssueDate;
	}
	public void setContractIssueDate(String contractIssueDate) {
		this.contractIssueDate = contractIssueDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getPropertyComercialName() {
		return propertyComercialName;
	}
	public void setPropertyComercialName(String propertyComercialName) {
		this.propertyComercialName = propertyComercialName;
	}
	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
		public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeBankNameEn() {
		return chequeBankNameEn;
	}
	public void setChequeBankNameEn(String chequeBankNameEn) {
		this.chequeBankNameEn = chequeBankNameEn;
	}
	public String getChequeBankNameAr() {
		return chequeBankNameAr;
	}
	public void setChequeBankNameAr(String chequeBankNameAr) {
		this.chequeBankNameAr = chequeBankNameAr;
	}
	
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public String getPaymentStatusEn() {
		return paymentStatusEn;
	}
	public void setPaymentStatusEn(String paymentStatusEn) {
		this.paymentStatusEn = paymentStatusEn;
	}
	public String getPaymentStatusAr() {
		return paymentStatusAr;
	}
	public void setPaymentStatusAr(String paymentStatusAr) {
		this.paymentStatusAr = paymentStatusAr;
	}
	public String getTotalSumAmount() {
		return totalSumAmount;
	}
	public void setTotalSumAmount(String totalSumAmount) {
		this.totalSumAmount = totalSumAmount;
	}
	public String getRequestTypeEn() {
		return requestTypeEn;
	}
	public void setRequestTypeEn(String requestTypeEn) {
		this.requestTypeEn = requestTypeEn;
	}
	public String getRequestTypeAr() {
		return requestTypeAr;
	}
	public void setRequestTypeAr(String requestTypeAr) {
		this.requestTypeAr = requestTypeAr;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}

	
	
	
	
}
