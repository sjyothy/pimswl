package com.avanza.pims.report.dataDef;

public class ContractRenewalHistorySubReportDataDef extends AbstractReportDataDef{

	private String contractNumber;				
	private String contractStartDate;	 			
	private String contractEndDate;	  			
	private String contractRentAmount;	 			
	private String tenantFirstName;	 			
	private String tenantMiddleName;
	private String tenantLastName;
	private String contractTypeEn;	 			
	private String contractTypeAr;	 			
	private String contractStatusEn;
	private String contractStatusAr;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractRentAmount() {
		return contractRentAmount;
	}
	public void setContractRentAmount(String contractRentAmount) {
		this.contractRentAmount = contractRentAmount;
	}
	
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getContractTypeEn() {
		return contractTypeEn;
	}
	public void setContractTypeEn(String contractTypeEn) {
		this.contractTypeEn = contractTypeEn;
	}
	public String getContractTypeAr() {
		return contractTypeAr;
	}
	public void setContractTypeAr(String contractTypeAr) {
		this.contractTypeAr = contractTypeAr;
	}
	public String getContractStatusEn() {
		return contractStatusEn;
	}
	public void setContractStatusEn(String contractStatusEn) {
		this.contractStatusEn = contractStatusEn;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}

	
	

}
