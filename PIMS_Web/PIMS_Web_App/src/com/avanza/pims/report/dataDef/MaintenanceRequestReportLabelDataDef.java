package com.avanza.pims.report.dataDef;

public class MaintenanceRequestReportLabelDataDef extends AbstractReportDataDef 
{
	private String lblMaintenanceRequestNoEn;
	private String lblContractorNameEn;
	private String  lblMaintenanceDateEn;
	private String  lblPropertyNameEn;
	private String  lblOwnershipTypeEn;
	private String  lblRequestSourceEn;
	private String lblMaintenanceTypeEn;
	private String lblUnderContractEn;
	private String lblMaintenancePriorityEn;
	private String lblRequestStatusEn;
	///////////
	private String lblMaintenanceRequestNoAr;
	private String lblContractorNameAr;
	private String  lblMaintenanceDateAr;
	private String  lblPropertyNameAr;
	private String  lblOwnershipTypeAr;
	private String  lblRequestSourceAr;
	private String lblMaintenanceTypeAr;
	private String lblUnderContractAr;
	private String lblMaintenancePriorityAr;
	private String lblRequestStatusAr;
	////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	public String getLblMaintenanceRequestNoEn() {
		return lblMaintenanceRequestNoEn;
	}
	public void setLblMaintenanceRequestNoEn(String lblMaintenanceRequestNoEn) {
		this.lblMaintenanceRequestNoEn = lblMaintenanceRequestNoEn;
	}
	public String getLblContractorNameEn() {
		return lblContractorNameEn;
	}
	public void setLblContractorNameEn(String lblContractorNameEn) {
		this.lblContractorNameEn = lblContractorNameEn;
	}
	public String getLblMaintenanceDateEn() {
		return lblMaintenanceDateEn;
	}
	public void setLblMaintenanceDateEn(String lblMaintenanceDateEn) {
		this.lblMaintenanceDateEn = lblMaintenanceDateEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblOwnershipTypeEn() {
		return lblOwnershipTypeEn;
	}
	public void setLblOwnershipTypeEn(String lblOwnershipTypeEn) {
		this.lblOwnershipTypeEn = lblOwnershipTypeEn;
	}
	public String getLblRequestSourceEn() {
		return lblRequestSourceEn;
	}
	public void setLblRequestSourceEn(String lblRequestSourceEn) {
		this.lblRequestSourceEn = lblRequestSourceEn;
	}
	public String getLblMaintenanceTypeEn() {
		return lblMaintenanceTypeEn;
	}
	public void setLblMaintenanceTypeEn(String lblMaintenanceTypeEn) {
		this.lblMaintenanceTypeEn = lblMaintenanceTypeEn;
	}
	public String getLblUnderContractEn() {
		return lblUnderContractEn;
	}
	public void setLblUnderContractEn(String lblUnderContractEn) {
		this.lblUnderContractEn = lblUnderContractEn;
	}
	public String getLblMaintenancePriorityEn() {
		return lblMaintenancePriorityEn;
	}
	public void setLblMaintenancePriorityEn(String lblMaintenancePriorityEn) {
		this.lblMaintenancePriorityEn = lblMaintenancePriorityEn;
	}
	public String getLblRequestStatusEn() {
		return lblRequestStatusEn;
	}
	public void setLblRequestStatusEn(String lblRequestStatusEn) {
		this.lblRequestStatusEn = lblRequestStatusEn;
	}
	public String getLblMaintenanceRequestNoAr() {
		return lblMaintenanceRequestNoAr;
	}
	public void setLblMaintenanceRequestNoAr(String lblMaintenanceRequestNoAr) {
		this.lblMaintenanceRequestNoAr = lblMaintenanceRequestNoAr;
	}
	public String getLblContractorNameAr() {
		return lblContractorNameAr;
	}
	public void setLblContractorNameAr(String lblContractorNameAr) {
		this.lblContractorNameAr = lblContractorNameAr;
	}
	public String getLblMaintenanceDateAr() {
		return lblMaintenanceDateAr;
	}
	public void setLblMaintenanceDateAr(String lblMaintenanceDateAr) {
		this.lblMaintenanceDateAr = lblMaintenanceDateAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblOwnershipTypeAr() {
		return lblOwnershipTypeAr;
	}
	public void setLblOwnershipTypeAr(String lblOwnershipTypeAr) {
		this.lblOwnershipTypeAr = lblOwnershipTypeAr;
	}
	public String getLblRequestSourceAr() {
		return lblRequestSourceAr;
	}
	public void setLblRequestSourceAr(String lblRequestSourceAr) {
		this.lblRequestSourceAr = lblRequestSourceAr;
	}
	public String getLblMaintenanceTypeAr() {
		return lblMaintenanceTypeAr;
	}
	public void setLblMaintenanceTypeAr(String lblMaintenanceTypeAr) {
		this.lblMaintenanceTypeAr = lblMaintenanceTypeAr;
	}
	public String getLblUnderContractAr() {
		return lblUnderContractAr;
	}
	public void setLblUnderContractAr(String lblUnderContractAr) {
		this.lblUnderContractAr = lblUnderContractAr;
	}
	public String getLblMaintenancePriorityAr() {
		return lblMaintenancePriorityAr;
	}
	public void setLblMaintenancePriorityAr(String lblMaintenancePriorityAr) {
		this.lblMaintenancePriorityAr = lblMaintenancePriorityAr;
	}
	public String getLblRequestStatusAr() {
		return lblRequestStatusAr;
	}
	public void setLblRequestStatusAr(String lblRequestStatusAr) {
		this.lblRequestStatusAr = lblRequestStatusAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	
	
	
	
}
