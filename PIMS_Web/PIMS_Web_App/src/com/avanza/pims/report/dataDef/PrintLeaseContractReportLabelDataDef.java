package com.avanza.pims.report.dataDef;

public class PrintLeaseContractReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblDateEn;
	private String lblDateAr;
	private String lblContractNoEn;
	private String lblContractNoAr;
	private String lblPropertyNameEn;
	private String lblPropertyNameAr;
	private String lblDescriptionEn;
	private String lblDescriptionAr;
	private String lblElementAddressEn;
	private String lblElementAddressAr;
	private String lblOwnerNameEn;
	private String lblOwnerNameAr;
	private String lblTenantNameEn;
	private String lblTenantNameAr;
	private String lblPeriodOfTenancyEn;
	private String lblPeriodOfTenancyAr;
	private String lblFromEn;
	private String lblFromAr;
	private String lblToEn;
	private String lblToAr;
	private String lblRentEn;
	private String lblRentAr;
	private String lblAskingPriceEn;
	private String lblAskingPriceAr;
	
	private String lblPaymentNumberEn;
	private String lblPaymentNumberAr;
	private String lblDueOnEn;
	private String lblDueOnAr;
	private String lblTypeEn;
	private String lblTypeAr;
	private String lblRefNoEn;
	private String lblRefNoAr;
	private String lblStatusEn;
	private String lblStatusAr;
	private String lblModeEn;
	private String lblModeAr;
	private String lblBankEn;
	private String lblBankAr;
	private String lblAmountEn;
	private String lblAmountAr;
    
    
	
	
	public String getLblDateEn() {
		return lblDateEn;
	}
	public void setLblDateEn(String lblDateEn) {
		this.lblDateEn = lblDateEn;
	}
	public String getLblDateAr() {
		return lblDateAr;
	}
	public void setLblDateAr(String lblDateAr) {
		this.lblDateAr = lblDateAr;
	}
	public String getLblContractNoEn() {
		return lblContractNoEn;
	}
	public void setLblContractNoEn(String lblContractNoEn) {
		this.lblContractNoEn = lblContractNoEn;
	}
	public String getLblContractNoAr() {
		return lblContractNoAr;
	}
	public void setLblContractNoAr(String lblContractNoAr) {
		this.lblContractNoAr = lblContractNoAr;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblDescriptionEn() {
		return lblDescriptionEn;
	}
	public void setLblDescriptionEn(String lblDescriptionEn) {
		this.lblDescriptionEn = lblDescriptionEn;
	}
	public String getLblDescriptionAr() {
		return lblDescriptionAr;
	}
	public void setLblDescriptionAr(String lblDescriptionAr) {
		this.lblDescriptionAr = lblDescriptionAr;
	}
	public String getLblElementAddressEn() {
		return lblElementAddressEn;
	}
	public void setLblElementAddressEn(String lblElementAddressEn) {
		this.lblElementAddressEn = lblElementAddressEn;
	}
	public String getLblElementAddressAr() {
		return lblElementAddressAr;
	}
	public void setLblElementAddressAr(String lblElementAddressAr) {
		this.lblElementAddressAr = lblElementAddressAr;
	}
	public String getLblOwnerNameEn() {
		return lblOwnerNameEn;
	}
	public void setLblOwnerNameEn(String lblOwnerNameEn) {
		this.lblOwnerNameEn = lblOwnerNameEn;
	}
	public String getLblOwnerNameAr() {
		return lblOwnerNameAr;
	}
	public void setLblOwnerNameAr(String lblOwnerNameAr) {
		this.lblOwnerNameAr = lblOwnerNameAr;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPeriodOfTenancyEn() {
		return lblPeriodOfTenancyEn;
	}
	public void setLblPeriodOfTenancyEn(String lblPeriodOfTenancyEn) {
		this.lblPeriodOfTenancyEn = lblPeriodOfTenancyEn;
	}
	public String getLblPeriodOfTenancyAr() {
		return lblPeriodOfTenancyAr;
	}
	public void setLblPeriodOfTenancyAr(String lblPeriodOfTenancyAr) {
		this.lblPeriodOfTenancyAr = lblPeriodOfTenancyAr;
	}
	public String getLblFromEn() {
		return lblFromEn;
	}
	public void setLblFromEn(String lblFromEn) {
		this.lblFromEn = lblFromEn;
	}
	public String getLblFromAr() {
		return lblFromAr;
	}
	public void setLblFromAr(String lblFromAr) {
		this.lblFromAr = lblFromAr;
	}
	public String getLblToEn() {
		return lblToEn;
	}
	public void setLblToEn(String lblToEn) {
		this.lblToEn = lblToEn;
	}
	public String getLblToAr() {
		return lblToAr;
	}
	public void setLblToAr(String lblToAr) {
		this.lblToAr = lblToAr;
	}
	public String getLblRentEn() {
		return lblRentEn;
	}
	public void setLblRentEn(String lblRentEn) {
		this.lblRentEn = lblRentEn;
	}
	public String getLblRentAr() {
		return lblRentAr;
	}
	public void setLblRentAr(String lblRentAr) {
		this.lblRentAr = lblRentAr;
	}
	public String getLblAskingPriceEn() {
		return lblAskingPriceEn;
	}
	public void setLblAskingPriceEn(String lblAskingPriceEn) {
		this.lblAskingPriceEn = lblAskingPriceEn;
	}
	public String getLblAskingPriceAr() {
		return lblAskingPriceAr;
	}
	public void setLblAskingPriceAr(String lblAskingPriceAr) {
		this.lblAskingPriceAr = lblAskingPriceAr;
	}
	public String getLblPaymentNumberEn() {
		return lblPaymentNumberEn;
	}
	public void setLblPaymentNumberEn(String lblPaymentNumberEn) {
		this.lblPaymentNumberEn = lblPaymentNumberEn;
	}
	public String getLblPaymentNumberAr() {
		return lblPaymentNumberAr;
	}
	public void setLblPaymentNumberAr(String lblPaymentNumberAr) {
		this.lblPaymentNumberAr = lblPaymentNumberAr;
	}
	public String getLblDueOnEn() {
		return lblDueOnEn;
	}
	public void setLblDueOnEn(String lblDueOnEn) {
		this.lblDueOnEn = lblDueOnEn;
	}
	public String getLblDueOnAr() {
		return lblDueOnAr;
	}
	public void setLblDueOnAr(String lblDueOnAr) {
		this.lblDueOnAr = lblDueOnAr;
	}
	public String getLblTypeEn() {
		return lblTypeEn;
	}
	public void setLblTypeEn(String lblTypeEn) {
		this.lblTypeEn = lblTypeEn;
	}
	public String getLblTypeAr() {
		return lblTypeAr;
	}
	public void setLblTypeAr(String lblTypeAr) {
		this.lblTypeAr = lblTypeAr;
	}
	public String getLblRefNoEn() {
		return lblRefNoEn;
	}
	public void setLblRefNoEn(String lblRefNoEn) {
		this.lblRefNoEn = lblRefNoEn;
	}
	public String getLblRefNoAr() {
		return lblRefNoAr;
	}
	public void setLblRefNoAr(String lblRefNoAr) {
		this.lblRefNoAr = lblRefNoAr;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblModeEn() {
		return lblModeEn;
	}
	public void setLblModeEn(String lblModeEn) {
		this.lblModeEn = lblModeEn;
	}
	public String getLblModeAr() {
		return lblModeAr;
	}
	public void setLblModeAr(String lblModeAr) {
		this.lblModeAr = lblModeAr;
	}
	public String getLblBankEn() {
		return lblBankEn;
	}
	public void setLblBankEn(String lblBankEn) {
		this.lblBankEn = lblBankEn;
	}
	public String getLblBankAr() {
		return lblBankAr;
	}
	public void setLblBankAr(String lblBankAr) {
		this.lblBankAr = lblBankAr;
	}
	public String getLblAmountEn() {
		return lblAmountEn;
	}
	public void setLblAmountEn(String lblAmountEn) {
		this.lblAmountEn = lblAmountEn;
	}
	public String getLblAmountAr() {
		return lblAmountAr;
	}
	public void setLblAmountAr(String lblAmountAr) {
		this.lblAmountAr = lblAmountAr;
	}
}
