package com.avanza.pims.report.dataDef;

import java.util.Date;

public class FollowupReportLabelDataDef extends AbstractReportDataDef
{
	/*******************************                           English Labels             *********************************/

	private String lblFollowupNumberEn;
	private String lblFollowupDateEn;
	private String lblChequeNumberEn;
	private String lblChequeAmountEn;
	private String lblChequeDateEn;
	private String lblTenantNameEn;
	private String lblBankNameEn;
	private String lblContractNumberEn;
	private String lblTotalAmountEn;

	/*************************                        Arabic Labels             *********************************/
	
	private String lblFollowupNumberAr;
	private String lblFollowupDateAr;
	private String lblChequeNumberAr;
	private String lblChequeAmountAr;
	private String lblChequeDateAr;
	private String lblTenantNameAr;
	private String lblBankNameAr;
	private String lblContractNumberAr;
	private String lblTotalAmountAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	//Extra fields
	private String extraField1;
	private String extraField2;
	private String extraField3;
	private String extraField4;
	private String extraField5;
	
	public String getLblFollowupNumberEn() {
		return lblFollowupNumberEn;
	}
	public void setLblFollowupNumberEn(String lblFollowupNumberEn) {
		this.lblFollowupNumberEn = lblFollowupNumberEn;
	}
	public String getLblFollowupDateEn() {
		return lblFollowupDateEn;
	}
	public void setLblFollowupDateEn(String lblFollowupDateEn) {
		this.lblFollowupDateEn = lblFollowupDateEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblFollowupNumberAr() {
		return lblFollowupNumberAr;
	}
	public void setLblFollowupNumberAr(String lblFollowupNumberAr) {
		this.lblFollowupNumberAr = lblFollowupNumberAr;
	}
	public String getLblFollowupDateAr() {
		return lblFollowupDateAr;
	}
	public void setLblFollowupDateAr(String lblFollowupDateAr) {
		this.lblFollowupDateAr = lblFollowupDateAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getExtraField1() {
		return extraField1;
	}
	public void setExtraField1(String extraField1) {
		this.extraField1 = extraField1;
	}
	public String getExtraField2() {
		return extraField2;
	}
	public void setExtraField2(String extraField2) {
		this.extraField2 = extraField2;
	}
	public String getExtraField3() {
		return extraField3;
	}
	public void setExtraField3(String extraField3) {
		this.extraField3 = extraField3;
	}
	public String getExtraField4() {
		return extraField4;
	}
	public void setExtraField4(String extraField4) {
		this.extraField4 = extraField4;
	}
	public String getExtraField5() {
		return extraField5;
	}
	public void setExtraField5(String extraField5) {
		this.extraField5 = extraField5;
	}
	public String getLblTotalAmountEn() {
		return lblTotalAmountEn;
	}
	public void setLblTotalAmountEn(String lblTotalAmountEn) {
		this.lblTotalAmountEn = lblTotalAmountEn;
	}
	public String getLblTotalAmountAr() {
		return lblTotalAmountAr;
	}
	public void setLblTotalAmountAr(String lblTotalAmountAr) {
		this.lblTotalAmountAr = lblTotalAmountAr;
	}

	/***********************************SearchCriteria*******************************/
	
	
	
	
}
