package com.avanza.pims.report.dataDef;

public class PropertyTenantsListReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyTypeEn;
	private String lblPropertyNameEn;
	private String lblEmirateEn;
	private String lblTenantNameEn;
	private String lblFromDateEn;
	private String lblToDateEn;
	private String lblContractNumberEn;
	private String lblContractValueEn; 
	private String lblRentalIncomeEn;
	private String lblPrepaidRentEn;
	private String lblRentAnnualEn;
	private String lblTotalRentalIncomeEn;
	private String lblTotalPrepaidRentEn;
	private String lblAddressEn;
	private String lblTotalEn;
	private String lblTotalAr;
	/////////Arabic Label/////////
	private String lblPropertyNumberAr;
	private String lblPropertyTypeAr;
	private String lblPropertyNameAr;
	private String lblEmirateAr;
	private String lblTenantNameAr;
	private String lblFromDateAr;
	private String lblToDateAr;
	private String lblContractNumberAr;
	private String lblContractValueAr; 
	private String lblRentalIncomeAr;
	private String lblPrepaidRentAr;
	private String lblRentAnnualAr;
	private String lblTotalRentalIncomeAr;
	private String lblTotalPrepaidRentAr;
	private String lblAddressAr;
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	//////////////////////
	public String getLblPropertyNumberEn() {
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblFromDateEn() {
		return lblFromDateEn;
	}
	public void setLblFromDateEn(String lblFromDateEn) {
		this.lblFromDateEn = lblFromDateEn;
	}
	
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractValueEn() {
		return lblContractValueEn;
	}
	public void setLblContractValueEn(String lblContractValueEn) {
		this.lblContractValueEn = lblContractValueEn;
	}
	public String getLblRentalIncomeEn() {
		return lblRentalIncomeEn;
	}
	public void setLblRentalIncomeEn(String lblRentalIncomeEn) {
		this.lblRentalIncomeEn = lblRentalIncomeEn;
	}
	public String getLblPrepaidRentEn() {
		return lblPrepaidRentEn;
	}
	public void setLblPrepaidRentEn(String lblPrepaidRentEn) {
		this.lblPrepaidRentEn = lblPrepaidRentEn;
	}
	public String getLblRentAnnualEn() {
		return lblRentAnnualEn;
	}
	public void setLblRentAnnualEn(String lblRentAnnualEn) {
		this.lblRentAnnualEn = lblRentAnnualEn;
	}
	public String getLblTotalRentalIncomeEn() {
		return lblTotalRentalIncomeEn;
	}
	public void setLblTotalRentalIncomeEn(String lblTotalRentalIncomeEn) {
		this.lblTotalRentalIncomeEn = lblTotalRentalIncomeEn;
	}
	public String getLblTotalPrepaidRentEn() {
		return lblTotalPrepaidRentEn;
	}
	public void setLblTotalPrepaidRentEn(String lblTotalPrepaidRentEn) {
		this.lblTotalPrepaidRentEn = lblTotalPrepaidRentEn;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblEmirateAr() {
		return lblEmirateAr;
	}
	public void setLblEmirateAr(String lblEmirateAr) {
		this.lblEmirateAr = lblEmirateAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblFromDateAr() {
		return lblFromDateAr;
	}
	public void setLblFromDateAr(String lblFromDateAr) {
		this.lblFromDateAr = lblFromDateAr;
	}
	public String getLblToDateAr() {
		return lblToDateAr;
	}
	public void setLblToDateAr(String lblToDateAr) {
		this.lblToDateAr = lblToDateAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractValueAr() {
		return lblContractValueAr;
	}
	public void setLblContractValueAr(String lblContractValueAr) {
		this.lblContractValueAr = lblContractValueAr;
	}
	public String getLblRentalIncomeAr() {
		return lblRentalIncomeAr;
	}
	public void setLblRentalIncomeAr(String lblRentalIncomeAr) {
		this.lblRentalIncomeAr = lblRentalIncomeAr;
	}
	public String getLblPrepaidRentAr() {
		return lblPrepaidRentAr;
	}
	public void setLblPrepaidRentAr(String lblPrepaidRentAr) {
		this.lblPrepaidRentAr = lblPrepaidRentAr;
	}
	public String getLblRentAnnualAr() {
		return lblRentAnnualAr;
	}
	public void setLblRentAnnualAr(String lblRentAnnualAr) {
		this.lblRentAnnualAr = lblRentAnnualAr;
	}
	public String getLblTotalRentalIncomeAr() {
		return lblTotalRentalIncomeAr;
	}
	public void setLblTotalRentalIncomeAr(String lblTotalRentalIncomeAr) {
		this.lblTotalRentalIncomeAr = lblTotalRentalIncomeAr;
	}
	public String getLblTotalPrepaidRentAr() {
		return lblTotalPrepaidRentAr;
	}
	public void setLblTotalPrepaidRentAr(String lblTotalPrepaidRentAr) {
		this.lblTotalPrepaidRentAr = lblTotalPrepaidRentAr;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblToDateEn() {
		return lblToDateEn;
	}
	public void setLblToDateEn(String lblToDateEn) {
		this.lblToDateEn = lblToDateEn;
	}
	public String getLblTotalEn() {
		return lblTotalEn;
	}
	public void setLblTotalEn(String lblTotalEn) {
		this.lblTotalEn = lblTotalEn;
	}
	public String getLblTotalAr() {
		return lblTotalAr;
	}
	public void setLblTotalAr(String lblTotalAr) {
		this.lblTotalAr = lblTotalAr;
	}
	
	
	
}
