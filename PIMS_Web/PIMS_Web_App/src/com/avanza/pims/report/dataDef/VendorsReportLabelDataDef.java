package com.avanza.pims.report.dataDef;

public class VendorsReportLabelDataDef {

    /*******************************                           English Labels             *********************************/

	private String lblVenderGRPNumberEn;
	private String lblVendorStatusEn;
	//private String lbl vendorArabicName
	private String lblVendorNameEn;
	private String lblVendorTypeEn;
	private String lblEmirateEn; 
	private String lblCityEn;
	private String lblAreaEn;
	private String lblTelephoneNumberEn;
	private String lblFaxNumberEn;
	private String lblContactPersonEn;
	private String lblEmailEn;
	private String lblLicenseNumberEn;
	private String lblLicenseSourceEn;
    
	/*******************************                           Arabic Labels             *********************************/

	
	private String lblVenderGRPNumberAr;
	private String lblVendorStatusAr;
	//private String lbl vendorArabicName
	private String lblVendorNameAr;
	private String lblVendorTypeAr;
	private String lblEmirateAr; 
	private String lblCityAr;
	private String lblAreaAr;
	private String lblTelephoneNumberAr;
	private String lblFaxNumberAr;
	private String lblContactPersonAr;
	private String lblEmailAr;
	private String lblLicenseNumberAr;
	private String lblLicenseSourceAr;

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblVenderGRPNumberEn() {
		return lblVenderGRPNumberEn;
	}
	public void setLblVenderGRPNumberEn(String lblVenderGRPNumberEn) {
		this.lblVenderGRPNumberEn = lblVenderGRPNumberEn;
	}
	public String getLblVendorStatusEn() {
		return lblVendorStatusEn;
	}
	public void setLblVendorStatusEn(String lblVendorStatusEn) {
		this.lblVendorStatusEn = lblVendorStatusEn;
	}
	public String getLblVendorNameEn() {
		return lblVendorNameEn;
	}
	public void setLblVendorNameEn(String lblVendorNameEn) {
		this.lblVendorNameEn = lblVendorNameEn;
	}
	public String getLblVendorTypeEn() {
		return lblVendorTypeEn;
	}
	public void setLblVendorTypeEn(String lblVendorTypeEn) {
		this.lblVendorTypeEn = lblVendorTypeEn;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblCityEn() {
		return lblCityEn;
	}
	public void setLblCityEn(String lblCityEn) {
		this.lblCityEn = lblCityEn;
	}
	public String getLblAreaEn() {
		return lblAreaEn;
	}
	public void setLblAreaEn(String lblAreaEn) {
		this.lblAreaEn = lblAreaEn;
	}
	public String getLblTelephoneNumberEn() {
		return lblTelephoneNumberEn;
	}
	public void setLblTelephoneNumberEn(String lblTelephoneNumberEn) {
		this.lblTelephoneNumberEn = lblTelephoneNumberEn;
	}
	public String getLblFaxNumberEn() {
		return lblFaxNumberEn;
	}
	public void setLblFaxNumberEn(String lblFaxNumberEn) {
		this.lblFaxNumberEn = lblFaxNumberEn;
	}
	public String getLblContactPersonEn() {
		return lblContactPersonEn;
	}
	public void setLblContactPersonEn(String lblContactPersonEn) {
		this.lblContactPersonEn = lblContactPersonEn;
	}
	public String getLblEmailEn() {
		return lblEmailEn;
	}
	public void setLblEmailEn(String lblEmailEn) {
		this.lblEmailEn = lblEmailEn;
	}
	public String getLblLicenseNumberEn() {
		return lblLicenseNumberEn;
	}
	public void setLblLicenseNumberEn(String lblLicenseNumberEn) {
		this.lblLicenseNumberEn = lblLicenseNumberEn;
	}
	public String getLblLicenseSourceEn() {
		return lblLicenseSourceEn;
	}
	public void setLblLicenseSourceEn(String lblLicenseSourceEn) {
		this.lblLicenseSourceEn = lblLicenseSourceEn;
	}
	public String getLblVenderGRPNumberAr() {
		return lblVenderGRPNumberAr;
	}
	public void setLblVenderGRPNumberAr(String lblVenderGRPNumberAr) {
		this.lblVenderGRPNumberAr = lblVenderGRPNumberAr;
	}
	public String getLblVendorStatusAr() {
		return lblVendorStatusAr;
	}
	public void setLblVendorStatusAr(String lblVendorStatusAr) {
		this.lblVendorStatusAr = lblVendorStatusAr;
	}
	public String getLblVendorNameAr() {
		return lblVendorNameAr;
	}
	public void setLblVendorNameAr(String lblVendorNameAr) {
		this.lblVendorNameAr = lblVendorNameAr;
	}
	public String getLblVendorTypeAr() {
		return lblVendorTypeAr;
	}
	public void setLblVendorTypeAr(String lblVendorTypeAr) {
		this.lblVendorTypeAr = lblVendorTypeAr;
	}
	public String getLblEmirateAr() {
		return lblEmirateAr;
	}
	public void setLblEmirateAr(String lblEmirateAr) {
		this.lblEmirateAr = lblEmirateAr;
	}
	public String getLblCityAr() {
		return lblCityAr;
	}
	public void setLblCityAr(String lblCityAr) {
		this.lblCityAr = lblCityAr;
	}
	public String getLblAreaAr() {
		return lblAreaAr;
	}
	public void setLblAreaAr(String lblAreaAr) {
		this.lblAreaAr = lblAreaAr;
	}
	public String getLblTelephoneNumberAr() {
		return lblTelephoneNumberAr;
	}
	public void setLblTelephoneNumberAr(String lblTelephoneNumberAr) {
		this.lblTelephoneNumberAr = lblTelephoneNumberAr;
	}
	public String getLblFaxNumberAr() {
		return lblFaxNumberAr;
	}
	public void setLblFaxNumberAr(String lblFaxNumberAr) {
		this.lblFaxNumberAr = lblFaxNumberAr;
	}
	public String getLblContactPersonAr() {
		return lblContactPersonAr;
	}
	public void setLblContactPersonAr(String lblContactPersonAr) {
		this.lblContactPersonAr = lblContactPersonAr;
	}
	public String getLblEmailAr() {
		return lblEmailAr;
	}
	public void setLblEmailAr(String lblEmailAr) {
		this.lblEmailAr = lblEmailAr;
	}
	public String getLblLicenseNumberAr() {
		return lblLicenseNumberAr;
	}
	public void setLblLicenseNumberAr(String lblLicenseNumberAr) {
		this.lblLicenseNumberAr = lblLicenseNumberAr;
	}
	public String getLblLicenseSourceAr() {
		return lblLicenseSourceAr;
	}
	public void setLblLicenseSourceAr(String lblLicenseSourceAr) {
		this.lblLicenseSourceAr = lblLicenseSourceAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}

	
}
