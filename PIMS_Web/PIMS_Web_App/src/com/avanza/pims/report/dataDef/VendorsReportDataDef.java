package com.avanza.pims.report.dataDef;

public class VendorsReportDataDef extends AbstractReportDataDef{

	private String venderGRPNumber;
	private String vendorStatusEn;
	private String vendorStatusAr;
	//private String vendorArabicName
	private String vendorName;
	private String vendorTypeEn;
	private String vendorTypeAr;
	private String emirate; 
	private String city;
	private String area;
	private String telephoneNumber;
	private String faxNumber;
	private String contactPerson;
	private String email;
	private String licenseNumber;
	private String licenseSourceEn;
	private String licenseSourceAr;
	public String getVenderGRPNumber() {
		return venderGRPNumber;
	}
	public void setVenderGRPNumber(String venderGRPNumber) {
		this.venderGRPNumber = venderGRPNumber;
	}
	public String getVendorStatusEn() {
		return vendorStatusEn;
	}
	public void setVendorStatusEn(String vendorStatusEn) {
		this.vendorStatusEn = vendorStatusEn;
	}
	public String getVendorStatusAr() {
		return vendorStatusAr;
	}
	public void setVendorStatusAr(String vendorStatusAr) {
		this.vendorStatusAr = vendorStatusAr;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorTypeEn() {
		return vendorTypeEn;
	}
	public void setVendorTypeEn(String vendorTypeEn) {
		this.vendorTypeEn = vendorTypeEn;
	}
	public String getVendorTypeAr() {
		return vendorTypeAr;
	}
	public void setVendorTypeAr(String vendorTypeAr) {
		this.vendorTypeAr = vendorTypeAr;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getLicenseSourceEn() {
		return licenseSourceEn;
	}
	public void setLicenseSourceEn(String licenseSourceEn) {
		this.licenseSourceEn = licenseSourceEn;
	}
	public String getLicenseSourceAr() {
		return licenseSourceAr;
	}
	public void setLicenseSourceAr(String licenseSourceAr) {
		this.licenseSourceAr = licenseSourceAr;
	}
	

	
}
