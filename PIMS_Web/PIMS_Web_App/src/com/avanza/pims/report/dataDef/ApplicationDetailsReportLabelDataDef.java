package com.avanza.pims.report.dataDef;

public class ApplicationDetailsReportLabelDataDef extends AbstractReportDataDef 
{
	private String lblApplicationNumberEn;
	private String lblApplicationStatusEn;
	private String lblApplicationNameEn;
	private String lblApplicationTypeEn;
	private String lblApplicationDateFromEn;
	
	private String lblApplicationDescriptionEn;
	private String lblPropertyNameEn;
	private String lblPropertyOwnershipTypeEn;
	private String lblUnitNumberEn;
	private String lblUnitTypeEn;
	private String lblContractNumberEn;
	private String lblContractStatusEn;
	private String lblTenantNameEn;
	private String lblTenantTypeEn;
	private String lblIndividualEn;
	private String lblCompanyEn;
	
	/*******************************                           Arabic Labels             *********************************/
	
	private String lblApplicationNumberAr;
	private String lblApplicationStatusAr;
	private String lblApplicationNameAr;
	private String lblApplicationTypeAr;
	private String lblApplicationDateFromAr;
	private String lblApplicationDescriptionAr;
	private String lblPropertyNameAr;
	private String lblPropertyOwnershipTypeAr;
	private String lblUnitNumberAr;
	private String lblUnitTypeAr;
	private String lblContractNumberAr;
	private String lblContractStatusAr;
	private String lblTenantNameAr;
	private String lblTenantTypeAr;
	private String lblIndividualAr;
	private String lblCompanyAr;

	////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblApplicationDetailsReportEn;
	private String lblPIMSHeaderAr;
	private String lblApplicationDetailsReportAr;
	
	//////////////////////

	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
//	private String lblPIMSHeaderEn;
//	private String lblReportNameEn;
//	private String lblPIMSHeaderAr;
//	private String lblReportNameAr;
	

	public String getLblApplicationNumberEn() {
		return lblApplicationNumberEn;
	}
	public void setLblApplicationNumberEn(String lblApplicationNumberEn) {
		this.lblApplicationNumberEn = lblApplicationNumberEn;
	}
	public String getLblApplicationStatusEn() {
		return lblApplicationStatusEn;
	}
	public void setLblApplicationStatusEn(String lblApplicationStatusEn) {
		this.lblApplicationStatusEn = lblApplicationStatusEn;
	}
	public String getLblApplicationNameEn() {
		return lblApplicationNameEn;
	}
	public void setLblApplicationNameEn(String lblApplicationNameEn) {
		this.lblApplicationNameEn = lblApplicationNameEn;
	}
	public String getLblApplicationTypeEn() {
		return lblApplicationTypeEn;
	}
	public void setLblApplicationTypeEn(String lblApplicationTypeEn) {
		this.lblApplicationTypeEn = lblApplicationTypeEn;
	}
	public String getLblApplicationDateFromEn() {
		return lblApplicationDateFromEn;
	}
	public void setLblApplicationDateFromEn(String lblApplicationDateFromEn) {
		this.lblApplicationDateFromEn = lblApplicationDateFromEn;
	}
	public String getLblApplicationDescriptionEn() {
		return lblApplicationDescriptionEn;
	}
	public void setLblApplicationDescriptionEn(String lblApplicationDescriptionEn) {
		this.lblApplicationDescriptionEn = lblApplicationDescriptionEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyOwnershipTypeEn() {
		return lblPropertyOwnershipTypeEn;
	}
	public void setLblPropertyOwnershipTypeEn(String lblPropertyOwnershipTypeEn) {
		this.lblPropertyOwnershipTypeEn = lblPropertyOwnershipTypeEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblUnitTypeEn() {
		return lblUnitTypeEn;
	}
	public void setLblUnitTypeEn(String lblUnitTypeEn) {
		this.lblUnitTypeEn = lblUnitTypeEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStatusEn() {
		return lblContractStatusEn;
	}
	public void setLblContractStatusEn(String lblContractStatusEn) {
		this.lblContractStatusEn = lblContractStatusEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantTypeEn() {
		return lblTenantTypeEn;
	}
	public void setLblTenantTypeEn(String lblTenantTypeEn) {
		this.lblTenantTypeEn = lblTenantTypeEn;
	}
	public String getLblApplicationNumberAr() {
		return lblApplicationNumberAr;
	}
	public void setLblApplicationNumberAr(String lblApplicationNumberAr) {
		this.lblApplicationNumberAr = lblApplicationNumberAr;
	}
	public String getLblApplicationStatusAr() {
		return lblApplicationStatusAr;
	}
	public void setLblApplicationStatusAr(String lblApplicationStatusAr) {
		this.lblApplicationStatusAr = lblApplicationStatusAr;
	}
	public String getLblApplicationNameAr() {
		return lblApplicationNameAr;
	}
	public void setLblApplicationNameAr(String lblApplicationNameAr) {
		this.lblApplicationNameAr = lblApplicationNameAr;
	}
	public String getLblApplicationTypeAr() {
		return lblApplicationTypeAr;
	}
	public void setLblApplicationTypeAr(String lblApplicationTypeAr) {
		this.lblApplicationTypeAr = lblApplicationTypeAr;
	}
	public String getLblApplicationDateFromAr() {
		return lblApplicationDateFromAr;
	}
	public void setLblApplicationDateFromAr(String lblApplicationDateFromAr) {
		this.lblApplicationDateFromAr = lblApplicationDateFromAr;
	}
	public String getLblApplicationDescriptionAr() {
		return lblApplicationDescriptionAr;
	}
	public void setLblApplicationDescriptionAr(String lblApplicationDescriptionAr) {
		this.lblApplicationDescriptionAr = lblApplicationDescriptionAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblPropertyOwnershipTypeAr() {
		return lblPropertyOwnershipTypeAr;
	}
	public void setLblPropertyOwnershipTypeAr(String lblPropertyOwnershipTypeAr) {
		this.lblPropertyOwnershipTypeAr = lblPropertyOwnershipTypeAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblUnitTypeAr() {
		return lblUnitTypeAr;
	}
	public void setLblUnitTypeAr(String lblUnitTypeAr) {
		this.lblUnitTypeAr = lblUnitTypeAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStatusAr() {
		return lblContractStatusAr;
	}
	public void setLblContractStatusAr(String lblContractStatusAr) {
		this.lblContractStatusAr = lblContractStatusAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblTenantTypeAr() {
		return lblTenantTypeAr;
	}
	public void setLblTenantTypeAr(String lblTenantTypeAr) {
		this.lblTenantTypeAr = lblTenantTypeAr;
	}
	public String getLblApplicationDetailsReportEn() {
		return lblApplicationDetailsReportEn;
	}
	public void setLblApplicationDetailsReportEn(
			String lblApplicationDetailsReportEn) {
		this.lblApplicationDetailsReportEn = lblApplicationDetailsReportEn;
	}
	public String getLblApplicationDetailsReportAr() {
		return lblApplicationDetailsReportAr;
	}
	public void setLblApplicationDetailsReportAr(
			String lblApplicationDetailsReportAr) {
		this.lblApplicationDetailsReportAr = lblApplicationDetailsReportAr;
	}
	

	
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblIndividualEn() {
		return lblIndividualEn;
	}
	public void setLblIndividualEn(String lblIndividualEn) {
		this.lblIndividualEn = lblIndividualEn;
	}
	public String getLblCompanyEn() {
		return lblCompanyEn;
	}
	public void setLblCompanyEn(String lblCompanyEn) {
		this.lblCompanyEn = lblCompanyEn;
	}
	public String getLblIndividualAr() {
		return lblIndividualAr;
	}
	public void setLblIndividualAr(String lblIndividualAr) {
		this.lblIndividualAr = lblIndividualAr;
	}
	public String getLblCompanyAr() {
		return lblCompanyAr;
	}
	public void setLblCompanyAr(String lblCompanyAr) {
		this.lblCompanyAr = lblCompanyAr;
	}
	

	
}
