package com.avanza.pims.report.dataDef;

public class StatementOfAccountLabelDataDef extends AbstractReportDataDef
{	
	
	private String lblTrxDateEn;
	private String lblAccountNumberEn;
	private String lblAccountOwnerNameEn;
	private String lblFromDateEn;
	private String lblToDateEn;
	private String lblDescEn;
	private String lblDescAr;
	private String lblDebitEn;
	private String lblCreditEn;
	private String lblTrxTypeEn;
	private String lblTrxTypeAr;
	private String lblBalanceEn;
	private String lblField1;
	private String lblField2;
	private String lblField3;
	private String lblField4;
	private String lblField5;
	private String lblField6;
	private String lblField7;
	private String lblField8;
	private String lblField9;
	
	private String lblTrxDateAr;
	private String lblAccountNumberAr;
	private String lblAccountOwnerNameAr;
	private String lblFromDateAr;
	private String lblToDateAr;
	private String lblDebitAr;
	private String lblCreditAr;
	private String lblBalanceAr;
	
	////////Report and PIMS Header///////////////
	private String lblMEMSHeaderEn;
	private String lblReportNameEn;
	private String lblMEMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	//////////////////////
	public String getLblMEMSHeaderEn() {
		return lblMEMSHeaderEn;
	}
	public void setLblMEMSHeaderEn(String lblMEMSHeaderEn) {
		this.lblMEMSHeaderEn = lblMEMSHeaderEn;
	}
	public String getLblMEMSHeaderAr() {
		return lblMEMSHeaderAr;
	}
	public void setLblMEMSHeaderAr(String lblMEMSHeaderAr) {
		this.lblMEMSHeaderAr = lblMEMSHeaderAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblTrxDateEn() {
		return lblTrxDateEn;
	}
	public void setLblTrxDateEn(String lblTrxDateEn) {
		this.lblTrxDateEn = lblTrxDateEn;
	}
	public String getLblAccountNumberEn() {
		return lblAccountNumberEn;
	}
	public void setLblAccountNumberEn(String lblAccountNumberEn) {
		this.lblAccountNumberEn = lblAccountNumberEn;
	}
	public String getLblAccountOwnerNameEn() {
		return lblAccountOwnerNameEn;
	}
	public void setLblAccountOwnerNameEn(String lblAccountOwnerNameEn) {
		this.lblAccountOwnerNameEn = lblAccountOwnerNameEn;
	}
	public String getLblFromDateEn() {
		return lblFromDateEn;
	}
	public void setLblFromDateEn(String lblFromDateEn) {
		this.lblFromDateEn = lblFromDateEn;
	}
	public String getLblToDateEn() {
		return lblToDateEn;
	}
	public void setLblToDateEn(String lblToDateEn) {
		this.lblToDateEn = lblToDateEn;
	}
	public String getLblDescEn() {
		return lblDescEn;
	}
	public void setLblDescEn(String lblDescEn) {
		this.lblDescEn = lblDescEn;
	}
	public String getLblDescAr() {
		return lblDescAr;
	}
	public void setLblDescAr(String lblDescAr) {
		this.lblDescAr = lblDescAr;
	}
	public String getLblDebitEn() {
		return lblDebitEn;
	}
	public void setLblDebitEn(String lblDebitEn) {
		this.lblDebitEn = lblDebitEn;
	}
	public String getLblCreditEn() {
		return lblCreditEn;
	}
	public void setLblCreditEn(String lblCreditEn) {
		this.lblCreditEn = lblCreditEn;
	}
	public String getLblTrxTypeEn() {
		return lblTrxTypeEn;
	}
	public void setLblTrxTypeEn(String lblTrxTypeEn) {
		this.lblTrxTypeEn = lblTrxTypeEn;
	}
	public String getLblTrxTypeAr() {
		return lblTrxTypeAr;
	}
	public void setLblTrxTypeAr(String lblTrxTypeAr) {
		this.lblTrxTypeAr = lblTrxTypeAr;
	}
	public String getLblBalanceEn() {
		return lblBalanceEn;
	}
	public void setLblBalanceEn(String lblBalanceEn) {
		this.lblBalanceEn = lblBalanceEn;
	}
	public String getLblField1() {
		return lblField1;
	}
	public void setLblField1(String lblField1) {
		this.lblField1 = lblField1;
	}
	public String getLblField2() {
		return lblField2;
	}
	public void setLblField2(String lblField2) {
		this.lblField2 = lblField2;
	}
	public String getLblField3() {
		return lblField3;
	}
	public void setLblField3(String lblField3) {
		this.lblField3 = lblField3;
	}
	public String getLblField4() {
		return lblField4;
	}
	public void setLblField4(String lblField4) {
		this.lblField4 = lblField4;
	}
	public String getLblField5() {
		return lblField5;
	}
	public void setLblField5(String lblField5) {
		this.lblField5 = lblField5;
	}
	public String getLblField6() {
		return lblField6;
	}
	public void setLblField6(String lblField6) {
		this.lblField6 = lblField6;
	}
	public String getLblField7() {
		return lblField7;
	}
	public void setLblField7(String lblField7) {
		this.lblField7 = lblField7;
	}
	public String getLblField8() {
		return lblField8;
	}
	public void setLblField8(String lblField8) {
		this.lblField8 = lblField8;
	}
	public String getLblField9() {
		return lblField9;
	}
	public void setLblField9(String lblField9) {
		this.lblField9 = lblField9;
	}
	public String getLblTrxDateAr() {
		return lblTrxDateAr;
	}
	public void setLblTrxDateAr(String lblTrxDateAr) {
		this.lblTrxDateAr = lblTrxDateAr;
	}
	public String getLblAccountNumberAr() {
		return lblAccountNumberAr;
	}
	public void setLblAccountNumberAr(String lblAccountNumberAr) {
		this.lblAccountNumberAr = lblAccountNumberAr;
	}
	public String getLblAccountOwnerNameAr() {
		return lblAccountOwnerNameAr;
	}
	public void setLblAccountOwnerNameAr(String lblAccountOwnerNameAr) {
		this.lblAccountOwnerNameAr = lblAccountOwnerNameAr;
	}
	public String getLblFromDateAr() {
		return lblFromDateAr;
	}
	public void setLblFromDateAr(String lblFromDateAr) {
		this.lblFromDateAr = lblFromDateAr;
	}
	public String getLblToDateAr() {
		return lblToDateAr;
	}
	public void setLblToDateAr(String lblToDateAr) {
		this.lblToDateAr = lblToDateAr;
	}
	public String getLblDebitAr() {
		return lblDebitAr;
	}
	public void setLblDebitAr(String lblDebitAr) {
		this.lblDebitAr = lblDebitAr;
	}
	public String getLblCreditAr() {
		return lblCreditAr;
	}
	public void setLblCreditAr(String lblCreditAr) {
		this.lblCreditAr = lblCreditAr;
	}
	public String getLblBalanceAr() {
		return lblBalanceAr;
	}
	public void setLblBalanceAr(String lblBalanceAr) {
		this.lblBalanceAr = lblBalanceAr;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	

}
