package com.avanza.pims.report.dataDef;

public class TenantDetailsReportExtDataDef extends AbstractReportDataDef {

	private Integer TenantIdNum;
	private String tenantNumber;
	private String tenantId;
	private String nationalId;
	private String idNumber;
	private String tenantName;
	private String nationality;
	private String maritalStatusEn;
	private String maritalStatusAr;
	private String tenantTypeEn;
	private String tenantTypeAr;
	private String dateOfBirth;
	private String passportNumber;
	private String passportSourceEn;
	private String passportSourceAr;
	private String passportIssueDate;
	private String passportExpiryDate;
	private String visaNumber;
	private String visaSource;
	private String visaIssueDate;
	private String visaExpiryDate;
	private String profession;
	private String workplace;
	private String companyLicenseNumber;
	private String licenseSourceEn;
	private String licenseSourceAr;
	private String licenseIssueDate;
	private String licenseExpiryDate;
	private String country;
	private String city;
	private String area;
	private String street;
	private String officePhone1;
	private String officePhone2;
	private String residenceNumber;
	private String fax;
	private String cellNumber1;
	private String cellNumber2;
	private String Address1;
	private String Address2;
	private String PO_Box;
	private String email;
	
	private String refTitle;
	private String refName;
	private String refRelationship;
	private String refRemarks;
	private String refNationality;
	private String refCellNumber;
	private String refStreet;
	private String retPostalCode;
	private String refState;
	private String refCountry;
	private String refHomePhone;
	private String refOfficePhone;
	private String refFax;
	private String refEmail;
	private String refMobileNumber;
	private String refDesignation;


	private String extraField1;
	private String extraField2;
	private String extraField3;
	private String extraField4;
	private String extraField5;
	private String extraField6;
	private String extraField7;
	private String extraField8;
	private String extraField9;
	private String extraField10;
	private String extraField11;
	private String extraField12;
	private String extraField13;
	private String extraField14;
	private String extraField15;
	private String extraField16;
	private String extraField17;
	private String extraField18;
	private String extraField19;
	private String extraField20;
	
	public String getTenantNumber() {
		return tenantNumber;
	}
	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getMaritalStatusEn() {
		return maritalStatusEn;
	}
	public void setMaritalStatusEn(String maritalStatusEn) {
		this.maritalStatusEn = maritalStatusEn;
	}
	public String getMaritalStatusAr() {
		return maritalStatusAr;
	}
	public void setMaritalStatusAr(String maritalStatusAr) {
		this.maritalStatusAr = maritalStatusAr;
	}
	public String getTenantTypeEn() {
		return tenantTypeEn;
	}
	public void setTenantTypeEn(String tenantTypeEn) {
		this.tenantTypeEn = tenantTypeEn;
	}
	public String getTenantTypeAr() {
		return tenantTypeAr;
	}
	public void setTenantTypeAr(String tenantTypeAr) {
		this.tenantTypeAr = tenantTypeAr;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPassportSourceEn() {
		return passportSourceEn;
	}
	public void setPassportSourceEn(String passportSourceEn) {
		this.passportSourceEn = passportSourceEn;
	}
	public String getPassportSourceAr() {
		return passportSourceAr;
	}
	public void setPassportSourceAr(String passportSourceAr) {
		this.passportSourceAr = passportSourceAr;
	}
	public String getPassportIssueDate() {
		return passportIssueDate;
	}
	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}
	public String getVisaSource() {
		return visaSource;
	}
	public void setVisaSource(String visaSource) {
		this.visaSource = visaSource;
	}
	public String getVisaIssueDate() {
		return visaIssueDate;
	}
	public void setVisaIssueDate(String visaIssueDate) {
		this.visaIssueDate = visaIssueDate;
	}
	public String getVisaExpiryDate() {
		return visaExpiryDate;
	}
	public void setVisaExpiryDate(String visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getWorkplace() {
		return workplace;
	}
	public void setWorkplace(String workplace) {
		this.workplace = workplace;
	}
	public String getCompanyLicenseNumber() {
		return companyLicenseNumber;
	}
	public void setCompanyLicenseNumber(String companyLicenseNumber) {
		this.companyLicenseNumber = companyLicenseNumber;
	}
	public String getLicenseSourceEn() {
		return licenseSourceEn;
	}
	public void setLicenseSourceEn(String licenseSourceEn) {
		this.licenseSourceEn = licenseSourceEn;
	}
	public String getLicenseSourceAr() {
		return licenseSourceAr;
	}
	public void setLicenseSourceAr(String licenseSourceAr) {
		this.licenseSourceAr = licenseSourceAr;
	}
	public String getLicenseIssueDate() {
		return licenseIssueDate;
	}
	public void setLicenseIssueDate(String licenseIssueDate) {
		this.licenseIssueDate = licenseIssueDate;
	}
	public String getLicenseExpiryDate() {
		return licenseExpiryDate;
	}
	public void setLicenseExpiryDate(String licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getOfficePhone1() {
		return officePhone1;
	}
	public void setOfficePhone1(String officePhone1) {
		this.officePhone1 = officePhone1;
	}
	public String getOfficePhone2() {
		return officePhone2;
	}
	public void setOfficePhone2(String officePhone2) {
		this.officePhone2 = officePhone2;
	}
	public String getResidenceNumber() {
		return residenceNumber;
	}
	public void setResidenceNumber(String residenceNumber) {
		this.residenceNumber = residenceNumber;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getCellNumber1() {
		return cellNumber1;
	}
	public void setCellNumber1(String cellNumber1) {
		this.cellNumber1 = cellNumber1;
	}
	public String getCellNumber2() {
		return cellNumber2;
	}
	public void setCellNumber2(String cellNumber2) {
		this.cellNumber2 = cellNumber2;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public String getPO_Box() {
		return PO_Box;
	}
	public void setPO_Box(String box) {
		PO_Box = box;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRefTitle() {
		return refTitle;
	}
	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}
	public String getRefName() {
		return refName;
	}
	public void setRefName(String refName) {
		this.refName = refName;
	}
	public String getRefRelationship() {
		return refRelationship;
	}
	public void setRefRelationship(String refRelationship) {
		this.refRelationship = refRelationship;
	}
	public String getRefRemarks() {
		return refRemarks;
	}
	public void setRefRemarks(String refRemarks) {
		this.refRemarks = refRemarks;
	}
	public String getRefNationality() {
		return refNationality;
	}
	public void setRefNationality(String refNationality) {
		this.refNationality = refNationality;
	}
	public String getRefCellNumber() {
		return refCellNumber;
	}
	public void setRefCellNumber(String refCellNumber) {
		this.refCellNumber = refCellNumber;
	}
	public String getRefStreet() {
		return refStreet;
	}
	public void setRefStreet(String refStreet) {
		this.refStreet = refStreet;
	}
	public String getRetPostalCode() {
		return retPostalCode;
	}
	public void setRetPostalCode(String retPostalCode) {
		this.retPostalCode = retPostalCode;
	}
	public String getRefState() {
		return refState;
	}
	public void setRefState(String refState) {
		this.refState = refState;
	}
	public String getRefCountry() {
		return refCountry;
	}
	public void setRefCountry(String refCountry) {
		this.refCountry = refCountry;
	}
	public String getRefHomePhone() {
		return refHomePhone;
	}
	public void setRefHomePhone(String refHomePhone) {
		this.refHomePhone = refHomePhone;
	}
	public String getRefOfficePhone() {
		return refOfficePhone;
	}
	public void setRefOfficePhone(String refOfficePhone) {
		this.refOfficePhone = refOfficePhone;
	}
	public String getRefFax() {
		return refFax;
	}
	public void setRefFax(String refFax) {
		this.refFax = refFax;
	}
	public String getRefEmail() {
		return refEmail;
	}
	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}
	public String getRefMobileNumber() {
		return refMobileNumber;
	}
	public void setRefMobileNumber(String refMobileNumber) {
		this.refMobileNumber = refMobileNumber;
	}
	public String getRefDesignation() {
		return refDesignation;
	}
	public void setRefDesignation(String refDesignation) {
		this.refDesignation = refDesignation;
	}
	public String getExtraField1() {
		return extraField1;
	}
	public void setExtraField1(String extraField1) {
		this.extraField1 = extraField1;
	}
	public String getExtraField2() {
		return extraField2;
	}
	public void setExtraField2(String extraField2) {
		this.extraField2 = extraField2;
	}
	public String getExtraField3() {
		return extraField3;
	}
	public void setExtraField3(String extraField3) {
		this.extraField3 = extraField3;
	}
	public String getExtraField4() {
		return extraField4;
	}
	public void setExtraField4(String extraField4) {
		this.extraField4 = extraField4;
	}
	public String getExtraField5() {
		return extraField5;
	}
	public void setExtraField5(String extraField5) {
		this.extraField5 = extraField5;
	}
	public String getExtraField6() {
		return extraField6;
	}
	public void setExtraField6(String extraField6) {
		this.extraField6 = extraField6;
	}
	public String getExtraField7() {
		return extraField7;
	}
	public void setExtraField7(String extraField7) {
		this.extraField7 = extraField7;
	}
	public String getExtraField8() {
		return extraField8;
	}
	public void setExtraField8(String extraField8) {
		this.extraField8 = extraField8;
	}
	public String getExtraField9() {
		return extraField9;
	}
	public void setExtraField9(String extraField9) {
		this.extraField9 = extraField9;
	}
	public String getExtraField10() {
		return extraField10;
	}
	public void setExtraField10(String extraField10) {
		this.extraField10 = extraField10;
	}
	public String getExtraField11() {
		return extraField11;
	}
	public void setExtraField11(String extraField11) {
		this.extraField11 = extraField11;
	}
	public String getExtraField12() {
		return extraField12;
	}
	public void setExtraField12(String extraField12) {
		this.extraField12 = extraField12;
	}
	public String getExtraField13() {
		return extraField13;
	}
	public void setExtraField13(String extraField13) {
		this.extraField13 = extraField13;
	}
	public String getExtraField14() {
		return extraField14;
	}
	public void setExtraField14(String extraField14) {
		this.extraField14 = extraField14;
	}
	public String getExtraField15() {
		return extraField15;
	}
	public void setExtraField15(String extraField15) {
		this.extraField15 = extraField15;
	}
	public String getExtraField16() {
		return extraField16;
	}
	public void setExtraField16(String extraField16) {
		this.extraField16 = extraField16;
	}
	public String getExtraField17() {
		return extraField17;
	}
	public void setExtraField17(String extraField17) {
		this.extraField17 = extraField17;
	}
	public String getExtraField18() {
		return extraField18;
	}
	public void setExtraField18(String extraField18) {
		this.extraField18 = extraField18;
	}
	public String getExtraField19() {
		return extraField19;
	}
	public void setExtraField19(String extraField19) {
		this.extraField19 = extraField19;
	}
	public String getExtraField20() {
		return extraField20;
	}
	public void setExtraField20(String extraField20) {
		this.extraField20 = extraField20;
	}
	public Integer getTenantIdNum() {
		return TenantIdNum;
	}
	public void setTenantIdNum(Integer tenantIdNum) {
		TenantIdNum = tenantIdNum;
	}
	
	
}
