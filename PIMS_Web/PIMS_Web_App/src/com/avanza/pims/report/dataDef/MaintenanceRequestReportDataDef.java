package com.avanza.pims.report.dataDef;

public class MaintenanceRequestReportDataDef extends AbstractReportDataDef 
{
	private String maintenanceRequestNo;
	private String contractorName;
	private String  maintenanceDate;
	private String  propertyName;
	private String  ownershipTypeEn;
	private String  ownershipTypeAr;
	private String  requestSourceEn;
	private String  requestSourceAr;
	private String maintenanceTypeEn;
	private String maintenanceTypeAr;
	private String underContract;
	private String maintenancePriorityEn;
	private String maintenancePriorityAr;
	private String requestStatusEn;
	private String requestStatusAr;
	private String requestDetails;
	
	public String getMaintenanceRequestNo() {
		return maintenanceRequestNo;
	}
	public void setMaintenanceRequestNo(String maintenanceRequestNo) {
		this.maintenanceRequestNo = maintenanceRequestNo;
	}
	public String getContractorName() {
		return contractorName;
	}
	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}
	public String getMaintenanceDate() {
		return maintenanceDate;
	}
	public void setMaintenanceDate(String maintenanceDate) {
		this.maintenanceDate = maintenanceDate;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	
	
	public String getUnderContract() {
		return underContract;
	}
	public void setUnderContract(String underContract) {
		this.underContract = underContract;
	}
	public String getOwnershipTypeEn() {
		return ownershipTypeEn;
	}
	public void setOwnershipTypeEn(String ownershipTypeEn) {
		this.ownershipTypeEn = ownershipTypeEn;
	}
	public String getOwnershipTypeAr() {
		return ownershipTypeAr;
	}
	public void setOwnershipTypeAr(String ownershipTypeAr) {
		this.ownershipTypeAr = ownershipTypeAr;
	}
	public String getMaintenanceTypeEn() {
		return maintenanceTypeEn;
	}
	public void setMaintenanceTypeEn(String maintenanceTypeEn) {
		this.maintenanceTypeEn = maintenanceTypeEn;
	}
	public String getMaintenanceTypeAr() {
		return maintenanceTypeAr;
	}
	public void setMaintenanceTypeAr(String maintenanceTypeAr) {
		this.maintenanceTypeAr = maintenanceTypeAr;
	}
	public String getRequestSourceEn() {
		return requestSourceEn;
	}
	public void setRequestSourceEn(String requestSourceEn) {
		this.requestSourceEn = requestSourceEn;
	}
	public String getRequestSourceAr() {
		return requestSourceAr;
	}
	public void setRequestSourceAr(String requestSourceAr) {
		this.requestSourceAr = requestSourceAr;
	}
	public String getRequestStatusEn() {
		return requestStatusEn;
	}
	public void setRequestStatusEn(String requestStatusEn) {
		this.requestStatusEn = requestStatusEn;
	}
	public String getRequestStatusAr() {
		return requestStatusAr;
	}
	public void setRequestStatusAr(String requestStatusAr) {
		this.requestStatusAr = requestStatusAr;
	}
	public String getMaintenancePriorityEn() {
		return maintenancePriorityEn;
	}
	public void setMaintenancePriorityEn(String maintenancePriorityEn) {
		this.maintenancePriorityEn = maintenancePriorityEn;
	}
	public String getMaintenancePriorityAr() {
		return maintenancePriorityAr;
	}
	public void setMaintenancePriorityAr(String maintenancePriorityAr) {
		this.maintenancePriorityAr = maintenancePriorityAr;
	}
	public String getRequestDetails() {
		return requestDetails;
	}
	public void setRequestDetails(String requestDetails) {
		this.requestDetails = requestDetails;
	}
	
	
}
