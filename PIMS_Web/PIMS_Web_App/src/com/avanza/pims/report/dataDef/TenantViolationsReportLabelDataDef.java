package com.avanza.pims.report.dataDef;

public class TenantViolationsReportLabelDataDef extends AbstractReportDataDef{
	
    /*******************************                           English Labels             *********************************/

	private String lblTenantIdEn;
	private String lblTenantNameEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblContractNumberEn;
	private String lblContractStartDateEn;
	private String lblContractEndDateEn;
	private String lblViolationNoEn;
	private String lblViolationDateEn;
	private String lblViolationCategoryEn;
	private String lblViolationTypeEn;
	private String lblViolationStatusEn;
	private String lblViolationActionsEn;
	
	
    /*******************************                           Arabic Labels             *********************************/

	private String lblTenantIdAr;
	private String lblTenantNameAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblContractNumberAr;
	private String lblContractStartDateAr;
	private String lblContractEndDateAr;
	private String lblViolationNoAr;
	private String lblViolationDateAr;
	private String lblViolationCategoryAr;
	private String lblViolationTypeAr;
	private String lblViolationStatusAr;
	private String lblViolationActionsAr;

	/*******************************                           Mandatory Labels             *********************************/

	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblViolationNoEn() {
		return lblViolationNoEn;
	}
	public void setLblViolationNoEn(String lblViolationNoEn) {
		this.lblViolationNoEn = lblViolationNoEn;
	}
	public String getLblViolationDateEn() {
		return lblViolationDateEn;
	}
	public void setLblViolationDateEn(String lblViolationDateEn) {
		this.lblViolationDateEn = lblViolationDateEn;
	}
	public String getLblViolationCategoryEn() {
		return lblViolationCategoryEn;
	}
	public void setLblViolationCategoryEn(String lblViolationCategoryEn) {
		this.lblViolationCategoryEn = lblViolationCategoryEn;
	}
	public String getLblViolationTypeEn() {
		return lblViolationTypeEn;
	}
	public void setLblViolationTypeEn(String lblViolationTypeEn) {
		this.lblViolationTypeEn = lblViolationTypeEn;
	}
	public String getLblViolationStatusEn() {
		return lblViolationStatusEn;
	}
	public void setLblViolationStatusEn(String lblViolationStatusEn) {
		this.lblViolationStatusEn = lblViolationStatusEn;
	}
	public String getLblViolationActionsEn() {
		return lblViolationActionsEn;
	}
	public void setLblViolationActionsEn(String lblViolationActionsEn) {
		this.lblViolationActionsEn = lblViolationActionsEn;
	}
	
	public String getLblTenantIdEn() {
		return lblTenantIdEn;
	}
	public void setLblTenantIdEn(String lblTenantIdEn) {
		this.lblTenantIdEn = lblTenantIdEn;
	}
	public String getLblTenantIdAr() {
		return lblTenantIdAr;
	}
	public void setLblTenantIdAr(String lblTenantIdAr) {
		this.lblTenantIdAr = lblTenantIdAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblViolationNoAr() {
		return lblViolationNoAr;
	}
	public void setLblViolationNoAr(String lblViolationNoAr) {
		this.lblViolationNoAr = lblViolationNoAr;
	}
	public String getLblViolationDateAr() {
		return lblViolationDateAr;
	}
	public void setLblViolationDateAr(String lblViolationDateAr) {
		this.lblViolationDateAr = lblViolationDateAr;
	}
	public String getLblViolationCategoryAr() {
		return lblViolationCategoryAr;
	}
	public void setLblViolationCategoryAr(String lblViolationCategoryAr) {
		this.lblViolationCategoryAr = lblViolationCategoryAr;
	}
	public String getLblViolationTypeAr() {
		return lblViolationTypeAr;
	}
	public void setLblViolationTypeAr(String lblViolationTypeAr) {
		this.lblViolationTypeAr = lblViolationTypeAr;
	}
	public String getLblViolationStatusAr() {
		return lblViolationStatusAr;
	}
	public void setLblViolationStatusAr(String lblViolationStatusAr) {
		this.lblViolationStatusAr = lblViolationStatusAr;
	}
	public String getLblViolationActionsAr() {
		return lblViolationActionsAr;
	}
	public void setLblViolationActionsAr(String lblViolationActionsAr) {
		this.lblViolationActionsAr = lblViolationActionsAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
	

}
