package com.avanza.pims.report.dataDef;

public class UnitsPerTenantsReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyNameEn;
	private String lblPropertyTypeEn;
	private String lblAddressEn;
	private String lblEmiratesEn;
	private String lblTenantNameEn;
	private String lblContractNumberEn;
	private String lblFromDateEn;
	private String lblToDateEn;
	private String lblContractValueEn;
	private String lblRentalIncomeEn;
	private String lblTotalRentalIncomeEn;	
	
	
	//////Arabic Label//////
	
	private String lblPropertyNumberAr;
	private String lblPropertyNameAr;
	private String lblPropertyTypeAr;
	private String lblAddressAr;
	private String lblEmiratesAr;
	private String lblTenantNameAr;
	private String lblContractNumberAr;
	private String lblFromDateAr;
	private String lblToDateAr;
	private String lblContractValueAr;
	private String lblRentalIncomeAr;
	private String lblTotalRentalIncomeAr;	
	
	
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	
	public String getLblPropertyNumberEn() 
	{
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropetyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblFromDateEn() {
		return lblFromDateEn;
	}
	public void setLblFromDateEn(String lblFromDateEn) {
		this.lblFromDateEn = lblFromDateEn;
	}
	public String getLblToDateEn() {
		return lblToDateEn;
	}
	public void setLblToDateEn(String lblToDateEn) {
		this.lblToDateEn = lblToDateEn;
	}
	public String getLblContractValueEn() {
		return lblContractValueEn;
	}
	public void setLblContractValueEn(String lblContractValueEn) {
		this.lblContractValueEn = lblContractValueEn;
	}
	public String getLblRentalIncomeEn() {
		return lblRentalIncomeEn;
	}
	public void setLblRentalIncomeEn(String lblRentalIncomeEn) {
		this.lblRentalIncomeEn = lblRentalIncomeEn;
	}
	public String getLblTotalRentalIncomeEn() {
		return lblTotalRentalIncomeEn;
	}
	public void setLblTotalRentalIncomeEn(String lblTotalRentalIncomeEn) {
		this.lblTotalRentalIncomeEn = lblTotalRentalIncomeEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblFromDateAr() {
		return lblFromDateAr;
	}
	public void setLblFromDateAr(String lblFromDateAr) {
		this.lblFromDateAr = lblFromDateAr;
	}
	public String getLblToDateAr() {
		return lblToDateAr;
	}
	public void setLblToDateAr(String lblToDateAr) {
		this.lblToDateAr = lblToDateAr;
	}
	public String getLblContractValueAr() {
		return lblContractValueAr;
	}
	public void setLblContractValueAr(String lblContractValueAr) {
		this.lblContractValueAr = lblContractValueAr;
	}
	public String getLblRentalIncomeAr() {
		return lblRentalIncomeAr;
	}
	public void setLblRentalIncomeAr(String lblRentalIncomeAr) {
		this.lblRentalIncomeAr = lblRentalIncomeAr;
	}
	public String getLblTotalRentalIncomeAr() {
		return lblTotalRentalIncomeAr;
	}
	public void setLblTotalRentalIncomeAr(String lblTotalRentalIncomeAr) {
		this.lblTotalRentalIncomeAr = lblTotalRentalIncomeAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblEmiratesEn() {
		return lblEmiratesEn;
	}
	public void setLblEmiratesEn(String lblEmiratesEn) {
		this.lblEmiratesEn = lblEmiratesEn;
	}
	public String getLblEmiratesAr() {
		return lblEmiratesAr;
	}
	public void setLblEmiratesAr(String lblEmiratesAr) {
		this.lblEmiratesAr = lblEmiratesAr;
	}
	
}
