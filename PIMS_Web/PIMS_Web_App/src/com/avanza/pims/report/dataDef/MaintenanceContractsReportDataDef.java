package com.avanza.pims.report.dataDef;

public class MaintenanceContractsReportDataDef {
	private String contractNumber;
	private String maintenanceworkTypeEn;
	private String maintenanceworkTypeAr;
	private String issueDate;
	private String contractorFirstName;
	private String contractorMiddleName;
	private String contractorLastName;
	private String contractStatusEn;
	private String contractStatusAr;
	private String workScope;
	
	public String getWorkScope() {
		return workScope;
	}
	public void setWorkScope(String workScope) {
		this.workScope = workScope;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getMaintenanceworkTypeEn() {
		return maintenanceworkTypeEn;
	}
	public void setMaintenanceworkTypeEn(String maintenanceworkTypeEn) {
		this.maintenanceworkTypeEn = maintenanceworkTypeEn;
	}
	public String getMaintenanceworkTypeAr() {
		return maintenanceworkTypeAr;
	}
	public void setMaintenanceworkTypeAr(String maintenanceworkTypeAr) {
		this.maintenanceworkTypeAr = maintenanceworkTypeAr;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getContractorFirstName() {
		return contractorFirstName;
	}
	public void setContractorFirstName(String contractorFirstName) {
		this.contractorFirstName = contractorFirstName;
	}
	public String getContractorMiddleName() {
		return contractorMiddleName;
	}
	public void setContractorMiddleName(String contractorMiddleName) {
		this.contractorMiddleName = contractorMiddleName;
	}
	public String getContractorLastName() {
		return contractorLastName;
	}
	public void setContractorLastName(String contractorLastName) {
		this.contractorLastName = contractorLastName;
	}
	public String getContractStatusEn() {
		return contractStatusEn;
	}
	public void setContractStatusEn(String contractStatusEn) {
		this.contractStatusEn = contractStatusEn;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}

	

	
}
