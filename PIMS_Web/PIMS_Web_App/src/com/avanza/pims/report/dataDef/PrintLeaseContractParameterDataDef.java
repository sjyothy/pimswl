package com.avanza.pims.report.dataDef;

public class PrintLeaseContractParameterDataDef extends AbstractReportDataDef 
{
	private String printBeforePaymentCollected = "0";
	private String messageBeforePaymentCollected = "";
	private String msgArBeforePaymentCollected = "";
	public String getPrintBeforePaymentCollected()
	{
		return printBeforePaymentCollected;
	}
	public void setPrintBeforePaymentCollected(String printBeforePaymentCollected) 
	{
		this.printBeforePaymentCollected = printBeforePaymentCollected;
	}
	public String getMessageBeforePaymentCollected() {
		return messageBeforePaymentCollected;
	}
	public void setMessageBeforePaymentCollected(
			String messageBeforePaymentCollected) {
		this.messageBeforePaymentCollected = messageBeforePaymentCollected;
	}
	public String getMsgArBeforePaymentCollected() {
		return msgArBeforePaymentCollected;
	}
	public void setMsgArBeforePaymentCollected(String msgArBeforePaymentCollected) {
		this.msgArBeforePaymentCollected = msgArBeforePaymentCollected;
	}
	
	

}
