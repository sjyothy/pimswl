package com.avanza.pims.report.dataDef;

public class UnitEvaluationDataDef extends AbstractReportDataDef {
	
	private String propertyName;
	private String propertyNumber;
	
	private String unitNumber;
	private String unitTypeAR;
	
	private String costCenter;
	private String newRentValue;
	
	private String inspectorReport;
	
	private String hosName;
	private String inspectorName;

	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getUnitTypeAR() {
		return unitTypeAR;
	}
	public void setUnitTypeAR(String unitTypeAR) {
		this.unitTypeAR = unitTypeAR;
	}
	public String getNewRentValue() {
		return newRentValue;
	}
	public void setNewRentValue(String newRentValue) {
		this.newRentValue = newRentValue;
	}
	public String getInspectorReport() {
		return inspectorReport;
	}
	public void setInspectorReport(String inspectorReport) {
		this.inspectorReport = inspectorReport;
	}
	public String getHosName() {
		return hosName;
	}
	public void setHostName(String hosName) {
		this.hosName = hosName;
	}
	public String getInspectorName() {
		return inspectorName;
	}
	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	
	

}
