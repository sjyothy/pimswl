package com.avanza.pims.report.dataDef;

public class RenewContractsSummaryReportLabelDataDef extends AbstractReportDataDef {
	
    /*******************************                           English Labels             *********************************/
	private String lblContractNumberEn;
	private String lblPropertyNameEn;
	private String lblContractDateEn;
	private String lblOccupantNameEn;
	private String lblStatusEn;
	private String lblTenantNameEn;

    /*******************************                           Arabic Labels             *********************************/

	private String lblContractNumberAr;
	private String lblPropertyNameAr;
	private String lblContractDateAr;
	private String lblOccupantNameAr;
	private String lblStatusAr;
	private String lblTenantNameAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblContractDateEn() {
		return lblContractDateEn;
	}
	public void setLblContractDateEn(String lblContractDateEn) {
		this.lblContractDateEn = lblContractDateEn;
	}
	public String getLblOccupantNameEn() {
		return lblOccupantNameEn;
	}
	public void setLblOccupantNameEn(String lblOccupantNameEn) {
		this.lblOccupantNameEn = lblOccupantNameEn;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblContractDateAr() {
		return lblContractDateAr;
	}
	public void setLblContractDateAr(String lblContractDateAr) {
		this.lblContractDateAr = lblContractDateAr;
	}
	public String getLblOccupantNameAr() {
		return lblOccupantNameAr;
	}
	public void setLblOccupantNameAr(String lblOccupantNameAr) {
		this.lblOccupantNameAr = lblOccupantNameAr;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}

	
	
}
