package com.avanza.pims.report.dataDef;


public class RequestTaskActivityDataDef extends AbstractReportDataDef 
{

	private Long requestId;
	private Long requestTypeId;
	private Long statusId;
	private String typeEn;
	private String typeAr;
	private String requestNumber;
	private String statusEn;
	private String statusAr;
	private String requestCreatedOn;
	private String approvalRequired_C;
	private String sentReview_C;
	private String reviewed_C;
	private String approved_C;
	private String rejected_C;
	private String cancelled_C;
	private String Completed_C;
	private String sentBack_C;
	private String resubmitted_C;
	private String field1_C;
	private String field2_C;
	private String field3_C;
	private String field4_C;
	private String field5_C;
	private String field6_C;
	private String field7_C;
	private String field8_C;
	private String field9_C;
	private String field31_C;
	private String field32_C;
	private String field33_C;
	private String field34_C;
	private String field35_C;
	private String field23_C;
	private String field24_C;
	private String field25_C;
	
	private String field10_C;
	private String field12_C;
	private String field13_C;
	private String field14_C;
	private String field15_C;
	private String field16_C;
	private String field17_C;
	private String field18_C;
	private String field19_C;
	private String field20_C;
	private String field21_C;
	private String field22_C;
	private String field36_C;
	private String field37_C;
	private String field38_C;
	private String field39_C;
	private String field40_C;
	
	private Long field26_C;
	private Long field27_C;
	private Long field28_C;
	private Long field29_C;
	private Long field30_C;
	
	private Double timeLapseAppReq;
	private Double timeLapseApprove;

	private Double timeLapseSentForReview;
	private Double timeLapseReviewed;
	private Double timeLapseRe;
	
	private Double timeLapseComplete;
	private Double field51;
	private Double field52;
	private Double field53;
	private Double field54;
	private Double field55;
	
	private Double field56;
	private Double field57;
	private Double field58;
	private Double field59;
	private Double field60;
	
	
}
