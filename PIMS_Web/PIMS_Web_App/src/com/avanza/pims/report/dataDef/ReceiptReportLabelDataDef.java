package com.avanza.pims.report.dataDef;

import java.util.Date;

public class ReceiptReportLabelDataDef extends AbstractReportDataDef
{
	/*******************************                           English Labels             *********************************/

	private String contractNumber;
	private String propertyName;
	private String unitNumber;
	private String receiptBy;
	private String paymentType;
	private String receiptNumber;
	private String chequeType;
	private String receiptDate;
	private String paymentMethod;
	private String requestNumber;
	private String paidBy;
	private String lblTenantNameEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblContractNumberEn;
	private String lblReceiptNumberEn;
	private String lblReceiptDateEn;
	private String lblReceiptStatusEn;
	private String lblReceiptByEn;
	private String lblReceiptAmountEn;
	private String lblChequeAmountEn;
	private String lblBankNameEn;
	private String lblChequeTypeEn;
	private String lblChequeNumberEn;
	private String lblChequeDateEn;
	private String lblTransactionTypeEn;
	private String lblPaymentTypeEn;
	private String lblCancelledReasonRequestEn;
	private String lblCancelledReasonContractEn;
	
	private String lblPaymentMethodEn;
	private String lblPMCashEn;
	private String lblPMDraftEn;
	private String lblChequeEn;
	private String lblSerialNumberEn;
	private String lblPaidByEn;
	private String lblRequestNumberEn;
	private String lblTotalReceiptsEn;
	private String lblSumOfAmountEn;

	/*******************************                           Arabic Labels             *********************************/
    private String lblTenantNameAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblContractNumberAr;
	private String lblReceiptNumberAr;
	private String lblReceiptDateAr;
	private String lblReceiptStatusAr;
	private String lblReceiptByAr;
	private String lblReceiptAmountAr;
	private String lblChequeAmountAr;
	private String lblBankNameAr;
	private String lblChequeTypeAr;
	private String lblChequeNumberAr;
	private String lblChequeDateAr;
	private String lblTransactionTypeAr;
	private String lblPaymentTypeAr;
	
	private String lblPaymentMethodAr;
	private String lblPMCashAr;
	private String lblPMDraftAr;
	private String lblChequeAr;
	private String lblSerialNumberAr;
	private String lblPaidByAr;
	private String lblRequestNumberAr;
	private String lblTotalReceiptsAr;
	private String lblSumOfAmountAr;
	private String lblCancelledReasonRequestAr;
	private String lblCancelledReasonContractAr;
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;

	/***********************************SearchCriteria*******************************/
	private String enteredCriteriaReceivedBy;
	private String enteredCriteriaRemittanceAccountName;
	private String enteredCriteriaReceiptDateFrom;
	private String enteredCriteriaReceiptDateTo;
	private String lblCancellationReasonEn;
	private String lblCancellationReasonAr;
	private String cancelledFlag;
	
	
	
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblReceiptStatusEn() {
		return lblReceiptStatusEn;
	}
	public void setLblReceiptStatusEn(String lblReceiptStatusEn) {
		this.lblReceiptStatusEn = lblReceiptStatusEn;
	}
	public String getLblReceiptByEn() {
		return lblReceiptByEn;
	}
	public void setLblReceiptByEn(String lblReceiptByEn) {
		this.lblReceiptByEn = lblReceiptByEn;
	}
	public String getLblReceiptAmountEn() {
		return lblReceiptAmountEn;
	}
	public void setLblReceiptAmountEn(String lblReceiptAmountEn) {
		this.lblReceiptAmountEn = lblReceiptAmountEn;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblChequeTypeEn() {
		return lblChequeTypeEn;
	}
	public void setLblChequeTypeEn(String lblChequeTypeEn) {
		this.lblChequeTypeEn = lblChequeTypeEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblTransactionTypeEn() {
		return lblTransactionTypeEn;
	}
	public void setLblTransactionTypeEn(String lblTransactionTypeEn) {
		this.lblTransactionTypeEn = lblTransactionTypeEn;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblReceiptStatusAr() {
		return lblReceiptStatusAr;
	}
	public void setLblReceiptStatusAr(String lblReceiptStatusAr) {
		this.lblReceiptStatusAr = lblReceiptStatusAr;
	}
	public String getLblReceiptByAr() {
		return lblReceiptByAr;
	}
	public void setLblReceiptByAr(String lblReceiptByAr) {
		this.lblReceiptByAr = lblReceiptByAr;
	}
	public String getLblReceiptAmountAr() {
		return lblReceiptAmountAr;
	}
	public void setLblReceiptAmountAr(String lblReceiptAmountAr) {
		this.lblReceiptAmountAr = lblReceiptAmountAr;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblChequeTypeAr() {
		return lblChequeTypeAr;
	}
	public void setLblChequeTypeAr(String lblChequeTypeAr) {
		this.lblChequeTypeAr = lblChequeTypeAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblTransactionTypeAr() {
		return lblTransactionTypeAr;
	}
	public void setLblTransactionTypeAr(String lblTransactionTypeAr) {
		this.lblTransactionTypeAr = lblTransactionTypeAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblPMCashEn() {
		return lblPMCashEn;
	}
	public void setLblPMCashEn(String lblPMCashEn) {
		this.lblPMCashEn = lblPMCashEn;
	}
	public String getLblPMDraftEn() {
		return lblPMDraftEn;
	}
	public void setLblPMDraftEn(String lblPMDraftEn) {
		this.lblPMDraftEn = lblPMDraftEn;
	}
	public String getLblPMCashAr() {
		return lblPMCashAr;
	}
	public void setLblPMCashAr(String lblPMCashAr) {
		this.lblPMCashAr = lblPMCashAr;
	}
	public String getLblPMDraftAr() {
		return lblPMDraftAr;
	}
	public void setLblPMDraftAr(String lblPMDraftAr) {
		this.lblPMDraftAr = lblPMDraftAr;
	}
	public String getLblChequeEn() {
		return lblChequeEn;
	}
	public void setLblChequeEn(String lblChequeEn) {
		this.lblChequeEn = lblChequeEn;
	}
	public String getLblChequeAr() {
		return lblChequeAr;
	}
	public void setLblChequeAr(String lblChequeAr) {
		this.lblChequeAr = lblChequeAr;
	}
	public String getEnteredCriteriaReceivedBy() {
		return enteredCriteriaReceivedBy;
	}
	public void setEnteredCriteriaReceivedBy(String enteredCriteriaReceivedBy) {
		this.enteredCriteriaReceivedBy = enteredCriteriaReceivedBy;
	}
	public String getEnteredCriteriaRemittanceAccountName() {
		return enteredCriteriaRemittanceAccountName;
	}
	public void setEnteredCriteriaRemittanceAccountName(
			String enteredCriteriaRemittanceAccountName) {
		this.enteredCriteriaRemittanceAccountName = enteredCriteriaRemittanceAccountName;
	}
	public String getEnteredCriteriaReceiptDateFrom() {
		return enteredCriteriaReceiptDateFrom;
	}
	public void setEnteredCriteriaReceiptDateFrom(
			String enteredCriteriaReceiptDateFrom) {
		this.enteredCriteriaReceiptDateFrom = enteredCriteriaReceiptDateFrom;
	}
	public String getEnteredCriteriaReceiptDateTo() {
		return enteredCriteriaReceiptDateTo;
	}
	public void setEnteredCriteriaReceiptDateTo(String enteredCriteriaReceiptDateTo) {
		this.enteredCriteriaReceiptDateTo = enteredCriteriaReceiptDateTo;
	}
	public String getLblSerialNumberEn() {
		return lblSerialNumberEn;
	}
	public void setLblSerialNumberEn(String lblSerialNumberEn) {
		this.lblSerialNumberEn = lblSerialNumberEn;
	}
	public String getLblPaidByEn() {
		return lblPaidByEn;
	}
	public void setLblPaidByEn(String lblPaidByEn) {
		this.lblPaidByEn = lblPaidByEn;
	}
	public String getLblSerialNumberAr() {
		return lblSerialNumberAr;
	}
	public void setLblSerialNumberAr(String lblSerialNumberAr) {
		this.lblSerialNumberAr = lblSerialNumberAr;
	}
	public String getLblPaidByAr() {
		return lblPaidByAr;
	}
	public void setLblPaidByAr(String lblPaidByAr) {
		this.lblPaidByAr = lblPaidByAr;
	}
	public String getLblRequestNumberEn() {
		return lblRequestNumberEn;
	}
	public void setLblRequestNumberEn(String lblRequestNumberEn) {
		this.lblRequestNumberEn = lblRequestNumberEn;
	}
	public String getLblRequestNumberAr() {
		return lblRequestNumberAr;
	}
	public void setLblRequestNumberAr(String lblRequestNumberAr) {
		this.lblRequestNumberAr = lblRequestNumberAr;
	}
	public String getLblTotalReceiptsEn() {
		return lblTotalReceiptsEn;
	}
	public void setLblTotalReceiptsEn(String lblTotalReceiptsEn) {
		this.lblTotalReceiptsEn = lblTotalReceiptsEn;
	}
	public String getLblSumOfAmountEn() {
		return lblSumOfAmountEn;
	}
	public void setLblSumOfAmountEn(String lblSumOfAmountEn) {
		this.lblSumOfAmountEn = lblSumOfAmountEn;
	}
	public String getLblTotalReceiptsAr() {
		return lblTotalReceiptsAr;
	}
	public void setLblTotalReceiptsAr(String lblTotalReceiptsAr) {
		this.lblTotalReceiptsAr = lblTotalReceiptsAr;
	}
	public String getLblSumOfAmountAr() {
		return lblSumOfAmountAr;
	}
	public void setLblSumOfAmountAr(String lblSumOfAmountAr) {
		this.lblSumOfAmountAr = lblSumOfAmountAr;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getReceiptBy() {
		return receiptBy;
	}
	public void setReceiptBy(String receiptBy) {
		this.receiptBy = receiptBy;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getChequeType() {
		return chequeType;
	}
	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getPaidBy() {
		return paidBy;
	}
	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}
	public String getLblCancelledReasonRequestEn() {
		return lblCancelledReasonRequestEn;
	}
	public void setLblCancelledReasonRequestEn(String lblCancelledReasonRequestEn) {
		this.lblCancelledReasonRequestEn = lblCancelledReasonRequestEn;
	}
	public String getLblCancelledReasonRequestAr() {
		return lblCancelledReasonRequestAr;
	}
	public void setLblCancelledReasonRequestAr(String lblCancelledReasonRequestAr) {
		this.lblCancelledReasonRequestAr = lblCancelledReasonRequestAr;
	}
	public String getLblCancelledReasonContractEn() {
		return lblCancelledReasonContractEn;
	}
	public void setLblCancelledReasonContractEn(String lblCancelledReasonContractEn) {
		this.lblCancelledReasonContractEn = lblCancelledReasonContractEn;
	}
	public String getLblCancelledReasonContractAr() {
		return lblCancelledReasonContractAr;
	}
	public void setLblCancelledReasonContractAr(String lblCancelledReasonContractAr) {
		this.lblCancelledReasonContractAr = lblCancelledReasonContractAr;
	}
	public String getLblCancellationReasonEn() {
		return lblCancellationReasonEn;
	}
	public void setLblCancellationReasonEn(String lblCancellationReasonEn) {
		this.lblCancellationReasonEn = lblCancellationReasonEn;
	}
	public String getLblCancellationReasonAr() {
		return lblCancellationReasonAr;
	}
	public void setLblCancellationReasonAr(String lblCancellationReasonAr) {
		this.lblCancellationReasonAr = lblCancellationReasonAr;
	}
	public String getCancelledFlag() {
		return cancelledFlag;
	}
	public void setCancelledFlag(String cancelledFlag) {
		this.cancelledFlag = cancelledFlag;
	}
	
	
}
