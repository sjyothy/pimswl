package com.avanza.pims.report.dataDef;

public class TenantViolationsReportDataDef extends AbstractReportDataDef {
	private String tenantId;
	private String tenantFirstName;
	private String tenantMiddleName;
	private String tenantLastName;
	private String propertyName;
	private String unitNumber;
	private String contractNumber;
	private String contractStartDate;
	private String contractEndDate;
	private String violationNo;
	private String violationDate;
	private String violationCategoryEn;
	private String violationCategoryAr;
	private String violationTypeEn;
	private String violationTypeAr;
	private String violationStatusEn;
	private String violationStatusAr;
	private String violationActionsEn;
	private String violationActionsAr;
	
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getViolationNo() {
		return violationNo;
	}
	public void setViolationNo(String violationNo) {
		this.violationNo = violationNo;
	}
	public String getViolationDate() {
		return violationDate;
	}
	public void setViolationDate(String violationDate) {
		this.violationDate = violationDate;
	}
	public String getViolationCategoryEn() {
		return violationCategoryEn;
	}
	public void setViolationCategoryEn(String violationCategoryEn) {
		this.violationCategoryEn = violationCategoryEn;
	}
	public String getViolationCategoryAr() {
		return violationCategoryAr;
	}
	public void setViolationCategoryAr(String violationCategoryAr) {
		this.violationCategoryAr = violationCategoryAr;
	}
	public String getViolationTypeEn() {
		return violationTypeEn;
	}
	public void setViolationTypeEn(String violationTypeEn) {
		this.violationTypeEn = violationTypeEn;
	}
	public String getViolationTypeAr() {
		return violationTypeAr;
	}
	public void setViolationTypeAr(String violationTypeAr) {
		this.violationTypeAr = violationTypeAr;
	}
	public String getViolationStatusEn() {
		return violationStatusEn;
	}
	public void setViolationStatusEn(String violationStatusEn) {
		this.violationStatusEn = violationStatusEn;
	}
	public String getViolationStatusAr() {
		return violationStatusAr;
	}
	public void setViolationStatusAr(String violationStatusAr) {
		this.violationStatusAr = violationStatusAr;
	}
	public String getViolationActionsEn() {
		return violationActionsEn;
	}
	public void setViolationActionsEn(String violationActionsEn) {
		this.violationActionsEn = violationActionsEn;
	}
	public String getViolationActionsAr() {
		return violationActionsAr;
	}
	public void setViolationActionsAr(String violationActionsAr) {
		this.violationActionsAr = violationActionsAr;
	}

	
	


}
