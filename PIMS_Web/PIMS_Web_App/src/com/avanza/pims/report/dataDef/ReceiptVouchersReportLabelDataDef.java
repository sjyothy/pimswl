package com.avanza.pims.report.dataDef;

public class ReceiptVouchersReportLabelDataDef extends AbstractReportDataDef 
{
	
	private String lblTenantNameEn;
	private String lblTenantIdEn;
	private String lblTenantTypeEn;
	private String lblUnitNumberEn;
	private String lblContractNumberEn;
	private String lblContractStartDateEn;
	private String lblExpiryDateEn;
	private String lblPropertyNameEn;
	private String lblPropertyOwnershipTypeEn;
	private String lblPaymentNumberEn;
	private String lblPaymentTypeEn;
	private String lblPaymentDueDateEn;
	private String lblPaymentMethodEn;
	private String lblReceiptNumberEn;
	private String lblReceiptDateEn;
	private String lblChequeNumberEn;
	private String lblChequeAmountEn;
	private String lblChequeTypeEn;
	private String lblBankNameEn;
	private String lblChequeOwnerNameEn;
	private String lblTransactionNumberEn;
	private String lblCompanyEn;
	private String lblCompanyAr;
	private String lblIndividualEn;
	private String lblIndividualAr;
	
	
	
	 /*******************************                           Arabic Labels             *********************************/
	
	private String lblTenantNameAr;
	private String lblTenantIdAr;
	private String lblTenantTypeAr;
	private String lblUnitNumberAr;
	private String lblContractNumberAr;
	private String lblContractStartDateAr;
	private String lblExpiryDateAr;
	private String lblPropertyNameAr;
	private String lblPropertyOwnershipTypeAr;
	private String lblPaymentNumberAr;
	private String lblPaymentTypeAr;
	private String lblPaymentDueDateAr;
	private String lblPaymentMethodAr;
	private String lblReceiptNumberAr;
	private String lblReceiptDateAr;
	private String lblChequeNumberAr;
	private String lblChequeAmountAr;
	private String lblChequeTypeAr;
	private String lblBankNameAr;
	private String lblChequeOwnerNameAr;
	private String lblTransactionNumberAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantIdEn() {
		return lblTenantIdEn;
	}
	public void setLblTenantIdEn(String lblTenantIdEn) {
		this.lblTenantIdEn = lblTenantIdEn;
	}
	public String getLblTenantTypeEn() {
		return lblTenantTypeEn;
	}
	public void setLblTenantTypeEn(String lblTenantTypeEn) {
		this.lblTenantTypeEn = lblTenantTypeEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblExpiryDateEn() {
		return lblExpiryDateEn;
	}
	public void setLblExpiryDateEn(String lblExpiryDateEn) {
		this.lblExpiryDateEn = lblExpiryDateEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyOwnershipTypeEn() {
		return lblPropertyOwnershipTypeEn;
	}
	public void setLblPropertyOwnershipTypeEn(String lblPropertyOwnershipTypeEn) {
		this.lblPropertyOwnershipTypeEn = lblPropertyOwnershipTypeEn;
	}
	public String getLblPaymentNumberEn() {
		return lblPaymentNumberEn;
	}
	public void setLblPaymentNumberEn(String lblPaymentNumberEn) {
		this.lblPaymentNumberEn = lblPaymentNumberEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentDueDateEn() {
		return lblPaymentDueDateEn;
	}
	public void setLblPaymentDueDateEn(String lblPaymentDueDateEn) {
		this.lblPaymentDueDateEn = lblPaymentDueDateEn;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblChequeTypeEn() {
		return lblChequeTypeEn;
	}
	public void setLblChequeTypeEn(String lblChequeTypeEn) {
		this.lblChequeTypeEn = lblChequeTypeEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblChequeOwnerNameEn() {
		return lblChequeOwnerNameEn;
	}
	public void setLblChequeOwnerNameEn(String lblChequeOwnerNameEn) {
		this.lblChequeOwnerNameEn = lblChequeOwnerNameEn;
	}
	public String getLblTransactionNumberEn() {
		return lblTransactionNumberEn;
	}
	public void setLblTransactionNumberEn(String lblTransactionNumberEn) {
		this.lblTransactionNumberEn = lblTransactionNumberEn;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblTenantIdAr() {
		return lblTenantIdAr;
	}
	public void setLblTenantIdAr(String lblTenantIdAr) {
		this.lblTenantIdAr = lblTenantIdAr;
	}
	public String getLblTenantTypeAr() {
		return lblTenantTypeAr;
	}
	public void setLblTenantTypeAr(String lblTenantTypeAr) {
		this.lblTenantTypeAr = lblTenantTypeAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblExpiryDateAr() {
		return lblExpiryDateAr;
	}
	public void setLblExpiryDateAr(String lblExpiryDateAr) {
		this.lblExpiryDateAr = lblExpiryDateAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblPropertyOwnershipTypeAr() {
		return lblPropertyOwnershipTypeAr;
	}
	public void setLblPropertyOwnershipTypeAr(String lblPropertyOwnershipTypeAr) {
		this.lblPropertyOwnershipTypeAr = lblPropertyOwnershipTypeAr;
	}
	public String getLblPaymentNumberAr() {
		return lblPaymentNumberAr;
	}
	public void setLblPaymentNumberAr(String lblPaymentNumberAr) {
		this.lblPaymentNumberAr = lblPaymentNumberAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentDueDateAr() {
		return lblPaymentDueDateAr;
	}
	public void setLblPaymentDueDateAr(String lblPaymentDueDateAr) {
		this.lblPaymentDueDateAr = lblPaymentDueDateAr;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblChequeTypeAr() {
		return lblChequeTypeAr;
	}
	public void setLblChequeTypeAr(String lblChequeTypeAr) {
		this.lblChequeTypeAr = lblChequeTypeAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblChequeOwnerNameAr() {
		return lblChequeOwnerNameAr;
	}
	public void setLblChequeOwnerNameAr(String lblChequeOwnerNameAr) {
		this.lblChequeOwnerNameAr = lblChequeOwnerNameAr;
	}
	public String getLblTransactionNumberAr() {
		return lblTransactionNumberAr;
	}
	public void setLblTransactionNumberAr(String lblTransactionNumberAr) {
		this.lblTransactionNumberAr = lblTransactionNumberAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblCompanyEn() {
		return lblCompanyEn;
	}
	public void setLblCompanyEn(String lblCompanyEn) {
		this.lblCompanyEn = lblCompanyEn;
	}
	public String getLblCompanyAr() {
		return lblCompanyAr;
	}
	public void setLblCompanyAr(String lblCompanyAr) {
		this.lblCompanyAr = lblCompanyAr;
	}
	public String getLblIndividualEn() {
		return lblIndividualEn;
	}
	public void setLblIndividualEn(String lblIndividualEn) {
		this.lblIndividualEn = lblIndividualEn;
	}
	public String getLblIndividualAr() {
		return lblIndividualAr;
	}
	public void setLblIndividualAr(String lblIndividualAr) {
		this.lblIndividualAr = lblIndividualAr;
	}
}
