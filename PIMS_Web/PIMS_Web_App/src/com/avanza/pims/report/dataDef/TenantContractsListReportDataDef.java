package com.avanza.pims.report.dataDef;

public class TenantContractsListReportDataDef extends AbstractReportDataDef{

	private String tenantNumber;
	private String tenantName;
	private String tenantTypeEn;
	private String tenantTypeAr;
	private String officePhone;
	private String residencePhone;
	private String cellNumber;
	private String address; 
	private String pOBox;
	private String email;

	private String contractNumber;
	private String contractStartDate;
	private String contractEndDate;
	private String contractRentValue;
	private String contractStatusEn;
	private String contractStatusAr;
	private String contractTypeEn;
	private String contractTypeAr;
	private String propertyName;
	private String unitNumber;
	private String costCenter;
	private String community;
	
	public String getTenantNumber() {
		return tenantNumber;
	}
	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getResidencePhone() {
		return residencePhone;
	}
	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPOBox() {
		return pOBox;
	}
	public void setPOBox(String box) {
		pOBox = box;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractRentValue() {
		return contractRentValue;
	}
	public void setContractRentValue(String contractRentValue) {
		this.contractRentValue = contractRentValue;
	}
	public String getContractStatusEn() {
		return contractStatusEn;
	}
	public void setContractStatusEn(String contractStatusEn) {
		this.contractStatusEn = contractStatusEn;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}
	public String getContractTypeEn() {
		return contractTypeEn;
	}
	public void setContractTypeEn(String contractTypeEn) {
		this.contractTypeEn = contractTypeEn;
	}
	public String getContractTypeAr() {
		return contractTypeAr;
	}
	public void setContractTypeAr(String contractTypeAr) {
		this.contractTypeAr = contractTypeAr;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getTenantTypeEn() {
		return tenantTypeEn;
	}
	public void setTenantTypeEn(String tenantTypeEn) {
		this.tenantTypeEn = tenantTypeEn;
	}
	public String getTenantTypeAr() {
		return tenantTypeAr;
	}
	public void setTenantTypeAr(String tenantTypeAr) {
		this.tenantTypeAr = tenantTypeAr;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}


}
