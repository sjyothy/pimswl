package com.avanza.pims.report.dataDef;

public class InheritanceDetailsTransportationLabelDataDef extends AbstractReportDataDef
{	
	///English////
	private String	assetNameEn;
	private String	assetTyeEn;
	private String	assetDescEn;
	private String	fromDateEn;
	private String	amountEn;
	private String	revenueEn;
	private String	isIncomExpEn;
	private String	dueDateEn;
	private String	chequeNumberEn;
	private String	bankTrNumberEn;
	private String	cashEn;
	private String	bankNameEn;
	private String	gotvDeptNameEn;
	private String	concernedPersonEn;
	private String	cellPhoneEn;
	private String	officePhoneEn;
	private String	devolutionEn;
	private String	landNumberEn;
	private String	emailEn;
	private String	faxEn;
	private String	statusEn;
	private String	revenueTypeEn;
	private String	isFinalEn;
	private String	assetNumberEn;
	private String	regionEn;
	private String	animalTypeEn;
	private String	quantityEn;
	private String	vehicleTypeEn;
	private String	vehicleCategoryEn;
	private String	plateNumberEn;
	private String	transferPartyEn;
	private String	companyEn;
	private String	noOfSharesEn;
	private String	listingPartyEn;
	private String	licenseNameEn;
	private String	licenseNumberEn;
	private String	rentValueEn;
	private String	partyEn;
	private String	typeGoldSilverEn;
	private String	weightInGmEn;
	private String	noOfJewelleriesEn;
	private String	transferNumberEn;
	private String	accountNameEn;
	private String	isAmafAccEn;
	private String	accountNumberEn;
	private String managerEn;
	private String amafManagerEn;
	private String amafManagerAr;
	///Arabic////
	private String	assetNameAr;
	private String	assetTyeAr;
	private String	assetDescAr;
	private String	fromDateAr;
	private String	amountAr;
	private String	revenueAr;
	private String	isIncomExpAr;
	private String	dueDateAr;
	private String	chequeNumberAr;
	private String	bankTrNumberAr;
	private String	cashAr;
	private String	bankNameAr;
	private String	gotvDeptNameAr;
	private String	concernedPersonAr;
	private String	cellPhoneAr;
	private String	officePhoneAr;
	private String	devolutionAr;
	private String	landNumberAr;
	private String	emailAr;
	private String	faxAr;
	private String	statusAr;
	private String	revenueTypeAr;
	private String	isFinalAr;
	private String	assetNumberAr;
	private String	regionAr;
	private String	animalTypeAr;
	private String	quantityAr;
	private String	vehicleTypeAr;
	private String	vehicleCategoryAr;
	private String	plateNumberAr;
	private String	transferPartyAr;
	private String	companyAr;
	private String	noOfSharesAr;
	private String	listingPartyAr;
	private String	licenseNameAr;
	private String	licenseNumberAr;
	private String	rentValueAr;
	private String	partyAr;
	private String	typeGoldSilverAr;
	private String	weightInGmAr;
	private String	noOfJewelleriesAr;
	private String	transferNumberAr;
	private String	accountNameAr;
	private String	isAmafAccAr;
	private String	accountNumberAr;
	private String managerAr;
	public String getAssetNameEn() {
		return assetNameEn;
	}
	public void setAssetNameEn(String assetNameEn) {
		this.assetNameEn = assetNameEn;
	}
	public String getAssetTyeEn() {
		return assetTyeEn;
	}
	public void setAssetTyeEn(String assetTyeEn) {
		this.assetTyeEn = assetTyeEn;
	}
	public String getAssetDescEn() {
		return assetDescEn;
	}
	public void setAssetDescEn(String assetDescEn) {
		this.assetDescEn = assetDescEn;
	}
	public String getFromDateEn() {
		return fromDateEn;
	}
	public void setFromDateEn(String fromDateEn) {
		this.fromDateEn = fromDateEn;
	}
	public String getAmountEn() {
		return amountEn;
	}
	public void setAmountEn(String amountEn) {
		this.amountEn = amountEn;
	}
	public String getRevenueEn() {
		return revenueEn;
	}
	public void setRevenueEn(String revenueEn) {
		this.revenueEn = revenueEn;
	}
	public String getIsIncomExpEn() {
		return isIncomExpEn;
	}
	public void setIsIncomExpEn(String isIncomExpEn) {
		this.isIncomExpEn = isIncomExpEn;
	}
	public String getDueDateEn() {
		return dueDateEn;
	}
	public void setDueDateEn(String dueDateEn) {
		this.dueDateEn = dueDateEn;
	}
	public String getChequeNumberEn() {
		return chequeNumberEn;
	}
	public void setChequeNumberEn(String chequeNumberEn) {
		this.chequeNumberEn = chequeNumberEn;
	}
	public String getBankTrNumberEn() {
		return bankTrNumberEn;
	}
	public void setBankTrNumberEn(String bankTrNumberEn) {
		this.bankTrNumberEn = bankTrNumberEn;
	}
	public String getCashEn() {
		return cashEn;
	}
	public void setCashEn(String cashEn) {
		this.cashEn = cashEn;
	}
	public String getBankNameEn() {
		return bankNameEn;
	}
	public void setBankNameEn(String bankNameEn) {
		this.bankNameEn = bankNameEn;
	}
	public String getGotvDeptNameEn() {
		return gotvDeptNameEn;
	}
	public void setGotvDeptNameEn(String gotvDeptNameEn) {
		this.gotvDeptNameEn = gotvDeptNameEn;
	}
	public String getConcernedPersonEn() {
		return concernedPersonEn;
	}
	public void setConcernedPersonEn(String concernedPersonEn) {
		this.concernedPersonEn = concernedPersonEn;
	}
	public String getCellPhoneEn() {
		return cellPhoneEn;
	}
	public void setCellPhoneEn(String cellPhoneEn) {
		this.cellPhoneEn = cellPhoneEn;
	}
	public String getOfficePhoneEn() {
		return officePhoneEn;
	}
	public void setOfficePhoneEn(String officePhoneEn) {
		this.officePhoneEn = officePhoneEn;
	}
	public String getDevolutionEn() {
		return devolutionEn;
	}
	public void setDevolutionEn(String devolutionEn) {
		this.devolutionEn = devolutionEn;
	}
	public String getLandNumberEn() {
		return landNumberEn;
	}
	public void setLandNumberEn(String landNumberEn) {
		this.landNumberEn = landNumberEn;
	}
	public String getEmailEn() {
		return emailEn;
	}
	public void setEmailEn(String emailEn) {
		this.emailEn = emailEn;
	}
	public String getFaxEn() {
		return faxEn;
	}
	public void setFaxEn(String faxEn) {
		this.faxEn = faxEn;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getIsFinalEn() {
		return isFinalEn;
	}
	public void setIsFinalEn(String isFinalEn) {
		this.isFinalEn = isFinalEn;
	}
	public String getAssetNumberEn() {
		return assetNumberEn;
	}
	public void setAssetNumberEn(String assetNumberEn) {
		this.assetNumberEn = assetNumberEn;
	}
	public String getRegionEn() {
		return regionEn;
	}
	public void setRegionEn(String regionEn) {
		this.regionEn = regionEn;
	}
	public String getAnimalTypeEn() {
		return animalTypeEn;
	}
	public void setAnimalTypeEn(String animalTypeEn) {
		this.animalTypeEn = animalTypeEn;
	}
	public String getQuantityEn() {
		return quantityEn;
	}
	public void setQuantityEn(String quantityEn) {
		this.quantityEn = quantityEn;
	}
	public String getVehicleTypeEn() {
		return vehicleTypeEn;
	}
	public void setVehicleTypeEn(String vehicleTypeEn) {
		this.vehicleTypeEn = vehicleTypeEn;
	}
	public String getVehicleCategoryEn() {
		return vehicleCategoryEn;
	}
	public void setVehicleCategoryEn(String vehicleCategoryEn) {
		this.vehicleCategoryEn = vehicleCategoryEn;
	}
	public String getPlateNumberEn() {
		return plateNumberEn;
	}
	public void setPlateNumberEn(String plateNumberEn) {
		this.plateNumberEn = plateNumberEn;
	}
	public String getTransferPartyEn() {
		return transferPartyEn;
	}
	public void setTransferPartyEn(String transferPartyEn) {
		this.transferPartyEn = transferPartyEn;
	}
	public String getCompanyEn() {
		return companyEn;
	}
	public void setCompanyEn(String companyEn) {
		this.companyEn = companyEn;
	}
	public String getNoOfSharesEn() {
		return noOfSharesEn;
	}
	public void setNoOfSharesEn(String noOfSharesEn) {
		this.noOfSharesEn = noOfSharesEn;
	}
	public String getListingPartyEn() {
		return listingPartyEn;
	}
	public void setListingPartyEn(String listingPartyEn) {
		this.listingPartyEn = listingPartyEn;
	}
	public String getLicenseNameEn() {
		return licenseNameEn;
	}
	public void setLicenseNameEn(String licenseNameEn) {
		this.licenseNameEn = licenseNameEn;
	}
	public String getLicenseNumberEn() {
		return licenseNumberEn;
	}
	public void setLicenseNumberEn(String licenseNumberEn) {
		this.licenseNumberEn = licenseNumberEn;
	}
	public String getRentValueEn() {
		return rentValueEn;
	}
	public void setRentValueEn(String rentValueEn) {
		this.rentValueEn = rentValueEn;
	}
	public String getPartyEn() {
		return partyEn;
	}
	public void setPartyEn(String partyEn) {
		this.partyEn = partyEn;
	}
	public String getTypeGoldSilverEn() {
		return typeGoldSilverEn;
	}
	public void setTypeGoldSilverEn(String typeGoldSilverEn) {
		this.typeGoldSilverEn = typeGoldSilverEn;
	}
	public String getWeightInGmEn() {
		return weightInGmEn;
	}
	public void setWeightInGmEn(String weightInGmEn) {
		this.weightInGmEn = weightInGmEn;
	}
	public String getNoOfJewelleriesEn() {
		return noOfJewelleriesEn;
	}
	public void setNoOfJewelleriesEn(String noOfJewelleriesEn) {
		this.noOfJewelleriesEn = noOfJewelleriesEn;
	}
	public String getTransferNumberEn() {
		return transferNumberEn;
	}
	public void setTransferNumberEn(String transferNumberEn) {
		this.transferNumberEn = transferNumberEn;
	}
	public String getAccountNameEn() {
		return accountNameEn;
	}
	public void setAccountNameEn(String accountNameEn) {
		this.accountNameEn = accountNameEn;
	}
	public String getIsAmafAccEn() {
		return isAmafAccEn;
	}
	public void setIsAmafAccEn(String isAmafAccEn) {
		this.isAmafAccEn = isAmafAccEn;
	}
	public String getAssetNameAr() {
		return assetNameAr;
	}
	public void setAssetNameAr(String assetNameAr) {
		this.assetNameAr = assetNameAr;
	}
	public String getAssetTyeAr() {
		return assetTyeAr;
	}
	public void setAssetTyeAr(String assetTyeAr) {
		this.assetTyeAr = assetTyeAr;
	}
	public String getAssetDescAr() {
		return assetDescAr;
	}
	public void setAssetDescAr(String assetDescAr) {
		this.assetDescAr = assetDescAr;
	}
	public String getFromDateAr() {
		return fromDateAr;
	}
	public void setFromDateAr(String fromDateAr) {
		this.fromDateAr = fromDateAr;
	}
	public String getAmountAr() {
		return amountAr;
	}
	public void setAmountAr(String amountAr) {
		this.amountAr = amountAr;
	}
	public String getRevenueAr() {
		return revenueAr;
	}
	public void setRevenueAr(String revenueAr) {
		this.revenueAr = revenueAr;
	}
	public String getIsIncomExpAr() {
		return isIncomExpAr;
	}
	public void setIsIncomExpAr(String isIncomExpAr) {
		this.isIncomExpAr = isIncomExpAr;
	}
	public String getDueDateAr() {
		return dueDateAr;
	}
	public void setDueDateAr(String dueDateAr) {
		this.dueDateAr = dueDateAr;
	}
	public String getChequeNumberAr() {
		return chequeNumberAr;
	}
	public void setChequeNumberAr(String chequeNumberAr) {
		this.chequeNumberAr = chequeNumberAr;
	}
	public String getBankTrNumberAr() {
		return bankTrNumberAr;
	}
	public void setBankTrNumberAr(String bankTrNumberAr) {
		this.bankTrNumberAr = bankTrNumberAr;
	}
	public String getCashAr() {
		return cashAr;
	}
	public void setCashAr(String cashAr) {
		this.cashAr = cashAr;
	}
	public String getBankNameAr() {
		return bankNameAr;
	}
	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}
	public String getGotvDeptNameAr() {
		return gotvDeptNameAr;
	}
	public void setGotvDeptNameAr(String gotvDeptNameAr) {
		this.gotvDeptNameAr = gotvDeptNameAr;
	}
	public String getConcernedPersonAr() {
		return concernedPersonAr;
	}
	public void setConcernedPersonAr(String concernedPersonAr) {
		this.concernedPersonAr = concernedPersonAr;
	}
	public String getCellPhoneAr() {
		return cellPhoneAr;
	}
	public void setCellPhoneAr(String cellPhoneAr) {
		this.cellPhoneAr = cellPhoneAr;
	}
	public String getOfficePhoneAr() {
		return officePhoneAr;
	}
	public void setOfficePhoneAr(String officePhoneAr) {
		this.officePhoneAr = officePhoneAr;
	}
	public String getDevolutionAr() {
		return devolutionAr;
	}
	public void setDevolutionAr(String devolutionAr) {
		this.devolutionAr = devolutionAr;
	}
	public String getLandNumberAr() {
		return landNumberAr;
	}
	public void setLandNumberAr(String landNumberAr) {
		this.landNumberAr = landNumberAr;
	}
	public String getEmailAr() {
		return emailAr;
	}
	public void setEmailAr(String emailAr) {
		this.emailAr = emailAr;
	}
	public String getFaxAr() {
		return faxAr;
	}
	public void setFaxAr(String faxAr) {
		this.faxAr = faxAr;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getRevenueTypeEn() {
		return revenueTypeEn;
	}
	public void setRevenueTypeEn(String revenueTypeEn) {
		this.revenueTypeEn = revenueTypeEn;
	}
	public String getRevenueTypeAr() {
		return revenueTypeAr;
	}
	public void setRevenueTypeAr(String revenueTypeAr) {
		this.revenueTypeAr = revenueTypeAr;
	}
	public String getIsFinalAr() {
		return isFinalAr;
	}
	public void setIsFinalAr(String isFinalAr) {
		this.isFinalAr = isFinalAr;
	}
	public String getAssetNumberAr() {
		return assetNumberAr;
	}
	public void setAssetNumberAr(String assetNumberAr) {
		this.assetNumberAr = assetNumberAr;
	}
	public String getRegionAr() {
		return regionAr;
	}
	public void setRegionAr(String regionAr) {
		this.regionAr = regionAr;
	}
	public String getAnimalTypeAr() {
		return animalTypeAr;
	}
	public void setAnimalTypeAr(String animalTypeAr) {
		this.animalTypeAr = animalTypeAr;
	}
	public String getQuantityAr() {
		return quantityAr;
	}
	public void setQuantityAr(String quantityAr) {
		this.quantityAr = quantityAr;
	}
	public String getVehicleTypeAr() {
		return vehicleTypeAr;
	}
	public void setVehicleTypeAr(String vehicleTypeAr) {
		this.vehicleTypeAr = vehicleTypeAr;
	}
	public String getVehicleCategoryAr() {
		return vehicleCategoryAr;
	}
	public void setVehicleCategoryAr(String vehicleCategoryAr) {
		this.vehicleCategoryAr = vehicleCategoryAr;
	}
	public String getPlateNumberAr() {
		return plateNumberAr;
	}
	public void setPlateNumberAr(String plateNumberAr) {
		this.plateNumberAr = plateNumberAr;
	}
	public String getTransferPartyAr() {
		return transferPartyAr;
	}
	public void setTransferPartyAr(String transferPartyAr) {
		this.transferPartyAr = transferPartyAr;
	}
	public String getCompanyAr() {
		return companyAr;
	}
	public void setCompanyAr(String companyAr) {
		this.companyAr = companyAr;
	}
	public String getNoOfSharesAr() {
		return noOfSharesAr;
	}
	public void setNoOfSharesAr(String noOfSharesAr) {
		this.noOfSharesAr = noOfSharesAr;
	}
	public String getListingPartyAr() {
		return listingPartyAr;
	}
	public void setListingPartyAr(String listingPartyAr) {
		this.listingPartyAr = listingPartyAr;
	}
	public String getLicenseNameAr() {
		return licenseNameAr;
	}
	public void setLicenseNameAr(String licenseNameAr) {
		this.licenseNameAr = licenseNameAr;
	}
	public String getLicenseNumberAr() {
		return licenseNumberAr;
	}
	public void setLicenseNumberAr(String licenseNumberAr) {
		this.licenseNumberAr = licenseNumberAr;
	}
	public String getRentValueAr() {
		return rentValueAr;
	}
	public void setRentValueAr(String rentValueAr) {
		this.rentValueAr = rentValueAr;
	}
	public String getPartyAr() {
		return partyAr;
	}
	public void setPartyAr(String partyAr) {
		this.partyAr = partyAr;
	}
	public String getTypeGoldSilverAr() {
		return typeGoldSilverAr;
	}
	public void setTypeGoldSilverAr(String typeGoldSilverAr) {
		this.typeGoldSilverAr = typeGoldSilverAr;
	}
	public String getWeightInGmAr() {
		return weightInGmAr;
	}
	public void setWeightInGmAr(String weightInGmAr) {
		this.weightInGmAr = weightInGmAr;
	}
	public String getNoOfJewelleriesAr() {
		return noOfJewelleriesAr;
	}
	public void setNoOfJewelleriesAr(String noOfJewelleriesAr) {
		this.noOfJewelleriesAr = noOfJewelleriesAr;
	}
	public String getTransferNumberAr() {
		return transferNumberAr;
	}
	public void setTransferNumberAr(String transferNumberAr) {
		this.transferNumberAr = transferNumberAr;
	}
	public String getAccountNameAr() {
		return accountNameAr;
	}
	public void setAccountNameAr(String accountNameAr) {
		this.accountNameAr = accountNameAr;
	}
	public String getIsAmafAccAr() {
		return isAmafAccAr;
	}
	public void setIsAmafAccAr(String isAmafAccAr) {
		this.isAmafAccAr = isAmafAccAr;
	}
	public String getAccountNumberEn() {
		return accountNumberEn;
	}
	public void setAccountNumberEn(String accountNumberEn) {
		this.accountNumberEn = accountNumberEn;
	}
	public String getAccountNumberAr() {
		return accountNumberAr;
	}
	public void setAccountNumberAr(String accountNumberAr) {
		this.accountNumberAr = accountNumberAr;
	}
	public String getManagerEn() {
		return managerEn;
	}
	public void setManagerEn(String managerEn) {
		this.managerEn = managerEn;
	}
	public String getManagerAr() {
		return managerAr;
	}
	public void setManagerAr(String managerAr) {
		this.managerAr = managerAr;
	}
	public String getAmafManagerEn() {
		return amafManagerEn;
	}
	public void setAmafManagerEn(String amafManagerEn) {
		this.amafManagerEn = amafManagerEn;
	}
	public String getAmafManagerAr() {
		return amafManagerAr;
	}
	public void setAmafManagerAr(String amafManagerAr) {
		this.amafManagerAr = amafManagerAr;
	}
	
	
}
