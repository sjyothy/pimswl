package com.avanza.pims.report.dataDef;

public class RequesttDetailsReportDataDef extends AbstractReportDataDef
{
	
	private String requestId;
	private String loggedInUser;
	private String newTntNameEn;
	private String newTntName;
	private String oldTntNameEn;
	private String oldTntName;
	private String requestNumber;
	private String unitId;
	private String unitNumber;
	private String tenantName;
	private String tenantNameEn;
	private String applicantName;
	private String applicantNameEn;
	private String propertyNumber;
	private String commercialName;
	private String unitTypeDescEn;
	private String unitTypeDescAr;
	private String ownershipTypeEn;
	private String ownershipTypeAr;
	private String unitTypeId;
	private String applicantId;
	private String auctionId;
	private String auctionNumber;
	private String requestDate;
	private String unitDesc;
	private String ownershipTypeId;
	private String requestTypeId;
	private String requestTypeEn;
	private String requestTypeAr;
	private String description;
	private String requestOrigination;
	private String auctionTitle;
	private String auctionStartDate;
	private String auctionEndDate;
	private String contractorId;
	private String contractId;
	private String tenderId;
	private String contractDate;
	private String contractTypeId;
	private String tenantId;
	private String rentAmount;
	private String startDate;
	private String endDate;
	private String settlementDate;
	private String oldContractNumber;
	private String requestStatusEn;
	private String requestStatusAr;
	private String contractNumber;
	private String unitCostCenter;
	private String propertyCostCenter;
	//MEMS related Changes
	private String inheritanceFileNumber;
	private String inheritanceFileOwner;
	private String inheritanceFileTypeEn;
	private String inheritanceFileTypeAr;
	private String inheritanceFileStatusEn;
	private String inheritanceFileStatusAr;

	
	private String nolTypeEn;
	private String nolTypeAr;
	private String nolReason;
	
	private String landNumber;
	private String propertyNameEn;
	
	private String workTypeAr;
	private String workTypeEn;

	
	private String maintenanceTypeEn;
	private String maintenanceTypeAr;
	private String maintenanceDetails;
	private String oldTenantNameEn;
	private String oldTenantName;
	

	private String endowmentFileNumber;
	private String endowmentFileOwner;
	private String endowmentFileName;


	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	
	
	public String getNewTntNameEn() {
		return newTntNameEn;
	}
	public void setNewTntNameEn(String newTntNameEn) {
		this.newTntNameEn = newTntNameEn;
	}
	public String getNewTntName() {
		return newTntName;
	}
	public void setNewTntName(String newTntName) {
		this.newTntName = newTntName;
	}
	public String getOldTntNameEn() {
		return oldTntNameEn;
	}
	public void setOldTntNameEn(String oldTntNameEn) {
		this.oldTntNameEn = oldTntNameEn;
	}
	public String getOldTntName() {
		return oldTntName;
	}
	public void setOldTntName(String oldTntName) {
		this.oldTntName = oldTntName;
	}
	public String getOldTenantNameEn() {
		return oldTenantNameEn;
	}
	public void setOldTenantNameEn(String oldTenantNameEn) {
		this.oldTenantNameEn = oldTenantNameEn;
	}
	public String getOldTenantName() {
		return oldTenantName;
	}
	public void setOldTenantName(String oldTenantName) {
		this.oldTenantName = oldTenantName;
	}
	public String getMaintenanceDetails() {
		return maintenanceDetails;
	}
	public void setMaintenanceDetails(String maintenanceDetails) {
		this.maintenanceDetails = maintenanceDetails;
	}
	public String getWorkTypeAr() {
		return workTypeAr;
	}
	public void setWorkTypeAr(String workTypeAr) {
		this.workTypeAr = workTypeAr;
	}
	public String getWorkTypeEn() {
		return workTypeEn;
	}
	public void setWorkTypeEn(String workTypeEn) {
		this.workTypeEn = workTypeEn;
	}
	public String getMaintenanceTypeEn() {
		return maintenanceTypeEn;
	}
	public void setMaintenanceTypeEn(String maintenanceTypeEn) {
		this.maintenanceTypeEn = maintenanceTypeEn;
	}
	public String getMaintenanceTypeAr() {
		return maintenanceTypeAr;
	}
	public void setMaintenanceTypeAr(String maintenanceTypeAr) {
		this.maintenanceTypeAr = maintenanceTypeAr;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public String getTenantNameEn() {
		return tenantNameEn;
	}
	public void setTenantNameEn(String tenantNameEn) {
		this.tenantNameEn = tenantNameEn;
	}
	public String getApplicantNameEn() {
		return applicantNameEn;
	}
	public void setApplicantNameEn(String applicantNameEn) {
		this.applicantNameEn = applicantNameEn;
	}
	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getPropertyNameEn() {
		return propertyNameEn;
	}
	public void setPropertyNameEn(String propertyNameEn) {
		this.propertyNameEn = propertyNameEn;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getCommercialName() {
		return commercialName;
	}
	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}
	public String getUnitTypeDescEn() {
		return unitTypeDescEn;
	}
	public void setUnitTypeDescEn(String unitTypeDescEn) {
		this.unitTypeDescEn = unitTypeDescEn;
	}
	public String getUnitTypeDescAr() {
		return unitTypeDescAr;
	}
	public void setUnitTypeDescAr(String unitTypeDescAr) {
		this.unitTypeDescAr = unitTypeDescAr;
	}
	public String getOwnershipTypeEn() {
		return ownershipTypeEn;
	}
	public void setOwnershipTypeEn(String ownershipTypeEn) {
		this.ownershipTypeEn = ownershipTypeEn;
	}
	public String getOwnershipTypeAr() {
		return ownershipTypeAr;
	}
	public void setOwnershipTypeAr(String ownershipTypeAr) {
		this.ownershipTypeAr = ownershipTypeAr;
	}
	public String getUnitTypeId() {
		return unitTypeId;
	}
	public void setUnitTypeId(String unitTypeId) {
		this.unitTypeId = unitTypeId;
	}
	public String getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}
	public String getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}
	public String getAuctionNumber() {
		return auctionNumber;
	}
	public void setAuctionNumber(String auctionNumber) {
		this.auctionNumber = auctionNumber;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getUnitDesc() {
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public String getOwnershipTypeId() {
		return ownershipTypeId;
	}
	public void setOwnershipTypeId(String ownershipTypeId) {
		this.ownershipTypeId = ownershipTypeId;
	}
	public String getRequestTypeId() {
		return requestTypeId;
	}
	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}
	public String getRequestTypeEn() {
		return requestTypeEn;
	}
	public void setRequestTypeEn(String requestTypeEn) {
		this.requestTypeEn = requestTypeEn;
	}
	public String getRequestTypeAr() {
		return requestTypeAr;
	}
	public void setRequestTypeAr(String requestTypeAr) {
		this.requestTypeAr = requestTypeAr;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRequestOrigination() {
		return requestOrigination;
	}
	public void setRequestOrigination(String requestOrigination) {
		this.requestOrigination = requestOrigination;
	}
	public String getAuctionTitle() {
		return auctionTitle;
	}
	public void setAuctionTitle(String auctionTitle) {
		this.auctionTitle = auctionTitle;
	}
	public String getAuctionStartDate() {
		return auctionStartDate;
	}
	public void setAuctionStartDate(String auctionStartDate) {
		this.auctionStartDate = auctionStartDate;
	}
	public String getAuctionEndDate() {
		return auctionEndDate;
	}
	public void setAuctionEndDate(String auctionEndDate) {
		this.auctionEndDate = auctionEndDate;
	}
	public String getContractorId() {
		return contractorId;
	}
	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getTenderId() {
		return tenderId;
	}
	public void setTenderId(String tenderId) {
		this.tenderId = tenderId;
	}
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getContractTypeId() {
		return contractTypeId;
	}
	public void setContractTypeId(String contractTypeId) {
		this.contractTypeId = contractTypeId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getRentAmount() {
		return rentAmount;
	}
	public void setRentAmount(String rentAmount) {
		this.rentAmount = rentAmount;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getOldContractNumber() {
		return oldContractNumber;
	}
	public void setOldContractNumber(String oldContractNumber) {
		this.oldContractNumber = oldContractNumber;
	}
	public String getRequestStatusEn() {
		return requestStatusEn;
	}
	public void setRequestStatusEn(String requestStatusEn) {
		this.requestStatusEn = requestStatusEn;
	}
	public String getRequestStatusAr() {
		return requestStatusAr;
	}
	public void setRequestStatusAr(String requestStatusAr) {
		this.requestStatusAr = requestStatusAr;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getUnitCostCenter() {
		return unitCostCenter;
	}
	public void setUnitCostCenter(String unitCostCenter) {
		this.unitCostCenter = unitCostCenter;
	}
	public String getPropertyCostCenter() {
		return propertyCostCenter;
	}
	public void setPropertyCostCenter(String propertyCostCenter) {
		this.propertyCostCenter = propertyCostCenter;
	}
	public String getInheritanceFileNumber() {
		return inheritanceFileNumber;
	}
	public void setInheritanceFileNumber(String inheritanceFileNumber) {
		this.inheritanceFileNumber = inheritanceFileNumber;
	}
	public String getInheritanceFileOwner() {
		return inheritanceFileOwner;
	}
	public void setInheritanceFileOwner(String inheritanceFileOwner) {
		this.inheritanceFileOwner = inheritanceFileOwner;
	}
	public String getInheritanceFileTypeEn() {
		return inheritanceFileTypeEn;
	}
	public void setInheritanceFileTypeEn(String inheritanceFileTypeEn) {
		this.inheritanceFileTypeEn = inheritanceFileTypeEn;
	}
	public String getInheritanceFileTypeAr() {
		return inheritanceFileTypeAr;
	}
	public void setInheritanceFileTypeAr(String inheritanceFileTypeAr) {
		this.inheritanceFileTypeAr = inheritanceFileTypeAr;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getInheritanceFileStatusEn() {
		return inheritanceFileStatusEn;
	}
	public void setInheritanceFileStatusEn(String inheritanceFileStatusEn) {
		this.inheritanceFileStatusEn = inheritanceFileStatusEn;
	}
	public String getInheritanceFileStatusAr() {
		return inheritanceFileStatusAr;
	}
	public void setInheritanceFileStatusAr(String inheritanceFileStatusAr) {
		this.inheritanceFileStatusAr = inheritanceFileStatusAr;
	}
	public String getNolTypeEn() {
		return nolTypeEn;
	}
	public void setNolTypeEn(String nolTypeEn) {
		this.nolTypeEn = nolTypeEn;
	}
	public String getNolTypeAr() {
		return nolTypeAr;
	}
	public void setNolTypeAr(String nolTypeAr) {
		this.nolTypeAr = nolTypeAr;
	}
	public String getEndowmentFileNumber() {
		return endowmentFileNumber;
	}
	public void setEndowmentFileNumber(String endowmentFileNumber) {
		this.endowmentFileNumber = endowmentFileNumber;
	}
	public String getEndowmentFileOwner() {
		return endowmentFileOwner;
	}
	public void setEndowmentFileOwner(String endowmentFileOwner) {
		this.endowmentFileOwner = endowmentFileOwner;
	}
	public String getEndowmentFileName() {
		return endowmentFileName;
	}
	public void setEndowmentFileName(String endowmentFileName) {
		this.endowmentFileName = endowmentFileName;
	}
	public String getNolReason() {
		return nolReason;
	}
	public void setNolReason(String nolReason) {
		this.nolReason = nolReason;
	}

	 
//	private String requestNumber;
//	private String currentUserEn;
//	private String currentUserAr;
//	private String createdbyEn;
//	private String createdbyAr;
//	private String requeststatusEn;
//	private String requeststatusAr;
//	private String requestTypeEn;
//	private String requestTypear;
//	private String contractNumber;
//	private String unitNumber;
//	private String propertyNumber;
//	private String propertyName;
//	private String firstName;
//	private String middleName;
//	private String lastName;
//	private String companyName;
//	private String isCompany;
//	private String startDate;
//	private String endDate;
//	private String evacuationDate;
//	private String field1;
//	private String field12;
//	public String getRequestNumber() {
//		return requestNumber;
//	}
//	public void setRequestNumber(String requestNumber) {
//		this.requestNumber = requestNumber;
//	}
//	public String getContractNumber() {
//		return contractNumber;
//	}
//	public void setContractNumber(String contractNumber) {
//		this.contractNumber = contractNumber;
//	}
//	public String getUnitNumber() {
//		return unitNumber;
//	}
//	public void setUnitNumber(String unitNumber) {
//		this.unitNumber = unitNumber;
//	}
//	public String getPropertyNumber() {
//		return propertyNumber;
//	}
//	public void setPropertyNumber(String propertyNumber) {
//		this.propertyNumber = propertyNumber;
//	}
//	public String getPropertyName() {
//		return propertyName;
//	}
//	public void setPropertyName(String propertyName) {
//		this.propertyName = propertyName;
//	}
//	public String getFirstName() {
//		return firstName;
//	}
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//	public String getMiddleName() {
//		return middleName;
//	}
//	public void setMiddleName(String middleName) {
//		this.middleName = middleName;
//	}
//	public String getLastName() {
//		return lastName;
//	}
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//	public String getCompanyName() {
//		return companyName;
//	}
//	public void setCompanyName(String companyName) {
//		this.companyName = companyName;
//	}
//	public String getIsCompany() {
//		return isCompany;
//	}
//	public void setIsCompany(String isCompany) {
//		this.isCompany = isCompany;
//	}
//	public String getStartDate() {
//		return startDate;
//	}
//	public void setStartDate(String startDate) {
//		this.startDate = startDate;
//	}
//	public String getEndDate() {
//		return endDate;
//	}
//	public void setEndDate(String endDate) {
//		this.endDate = endDate;
//	}
//	public String getRequeststatusEn() {
//		return requeststatusEn;
//	}
//	public void setRequeststatusEn(String requeststatusEn) {
//		this.requeststatusEn = requeststatusEn;
//	}
//	public String getRequeststatusAr() {
//		return requeststatusAr;
//	}
//	public void setRequeststatusAr(String requeststatusAr) {
//		this.requeststatusAr = requeststatusAr;
//	}
//	public String getRequestTypeEn() {
//		return requestTypeEn;
//	}
//	public void setRequestTypeEn(String requestTypeEn) {
//		this.requestTypeEn = requestTypeEn;
//	}
//	public String getRequestTypear() {
//		return requestTypear;
//	}
//	public void setRequestTypear(String requestTypear) {
//		this.requestTypear = requestTypear;
//	}
//	public String getCurrentUserEn() {
//		return currentUserEn;
//	}
//	public void setCurrentUserEn(String currentUserEn) {
//		this.currentUserEn = currentUserEn;
//	}
//	public String getCurrentUserAr() {
//		return currentUserAr;
//	}
//	public void setCurrentUserAr(String currentUserAr) {
//		this.currentUserAr = currentUserAr;
//	}
//	public String getCreatedbyEn() {
//		return createdbyEn;
//	}
//	public void setCreatedbyEn(String createdbyEn) {
//		this.createdbyEn = createdbyEn;
//	}
//	public String getCreatedbyAr() {
//		return createdbyAr;
//	}
//	public void setCreatedbyAr(String createdbyAr) {
//		this.createdbyAr = createdbyAr;
//	}
//	public String getEvacuationDate() {
//		return evacuationDate;
//	}
//	public void setEvacuationDate(String evacuationDate) {
//		this.evacuationDate = evacuationDate;
//	}
//	public String getField1() {
//		return field1;
//	}
//	public void setField1(String field1) {
//		this.field1 = field1;
//	}
//	public String getField12() {
//		return field12;
//	}
//	public void setField12(String field12) {
//		this.field12 = field12;
//	}
}
	