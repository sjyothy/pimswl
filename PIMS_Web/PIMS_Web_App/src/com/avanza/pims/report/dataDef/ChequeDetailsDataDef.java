package com.avanza.pims.report.dataDef;

public class ChequeDetailsDataDef  extends AbstractReportDataDef 
{
	   private String personId;
	   private String endowmentId;
	   private String masrafId;
	   private String costCenter;
	   private String requestNumber;
	   private String requestType;
	   private String paymentScheduleId; 
	   private String collectionTrxId;
	   private String paymentNumber;
	   private String status;
	   private String statusId;
	   private String paymentDueOn;
	   private String createOn;
	   private String realizedOn;
	   private String bank;
	   private String methodRefNo;
	   private String description;
	   private Double amount;
	   private String field1;
	   private String field2;
	   private String field3;
	   private String field4;
	   private String field5;
	   private String field6;
	   private String field7;
	   private String field8;
	   private String field9;
	   private String field10;
	   
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}
	public String getCollectionTrxId() {
		return collectionTrxId;
	}
	public void setCollectionTrxId(String collectionTrxId) {
		this.collectionTrxId = collectionTrxId;
	}
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getPaymentDueOn() {
		return paymentDueOn;
	}
	public void setPaymentDueOn(String paymentDueOn) {
		this.paymentDueOn = paymentDueOn;
	}
	public String getCreateOn() {
		return createOn;
	}
	public void setCreateOn(String createOn) {
		this.createOn = createOn;
	}
	public String getRealizedOn() {
		return realizedOn;
	}
	public void setRealizedOn(String realizedOn) {
		this.realizedOn = realizedOn;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getMethodRefNo() {
		return methodRefNo;
	}
	public void setMethodRefNo(String methodRefNo) {
		this.methodRefNo = methodRefNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
//	public String getAmount() {
//		return amount;
//	}
//	public void setAmount(String amount) {
//		this.amount = amount;
//	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmount() {
		return amount;
	}
	public String getEndowmentId() {
		return endowmentId;
	}
	public void setEndowmentId(String endowmentId) {
		this.endowmentId = endowmentId;
	}
	public String getMasrafId() {
		return masrafId;
	}
	public void setMasrafId(String masrafId) {
		this.masrafId = masrafId;
	}
	
}
