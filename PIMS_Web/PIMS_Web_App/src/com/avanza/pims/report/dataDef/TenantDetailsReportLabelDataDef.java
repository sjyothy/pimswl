package com.avanza.pims.report.dataDef;

public class TenantDetailsReportLabelDataDef extends AbstractReportDataDef
{
	private String lblTenantNumberEn;
	private String lblIdNumberEn;
	private String lblTenantNameEn;
	private String lblNationalIdEn; 
	private String lblNationalityEn;
	private String lblMaritalStatusEn;
	private String lblTenantTypeEn;
	private String lblDateOfBirthEn;
	private String lblPassportNumberEn;
	private String lblPassportSourceEn;
	private String lblPassportIssueDateEn;
	private String lblPassportExpiryDateEn;
	private String lblVisaNumberEn;
	private String lblVisaSourceEn;
	private String lblVisaIssueDateEn;
	private String lblVisaExpiryDateEn;
	private String lblProfessionEn;
	private String lblWorkplaceEn;
	private String lblCompanyLicenseNumberEn;
	private String lblLicenseSourceEn;
	private String lblLicenseIssueDateEn;
	private String lblLicenseExpiryDateEn;
	private String lblCountryEn;
	private String lblCityEn;
	private String lblAreaEn;
	private String lblStreetEn;
	private String lblOfficePhone_1En;
	private String lblOfficePhone_2En;
	private String lblResidenceNumberEn;
	private String lblFaxEn;
	private String lblCellNumber1En;
	private String lblCellNumber2En;
	private String lblAddress1En;
	private String lblAddress2En;
	private String lblPO_BoxEn;
	private String lblEmailEn;
	private String lblContactInfoEn;
	private String lblPersonalInfoEn;
	private String lblTenantNumberAr;
	private String lblIdNumberAr;
	private String lblTenantNameAr;
	private String lblNationalityAr;
	private String lblMaritalStatusAr;
	private String lblNationalIdAr; 
	private String lblTenantTypeAr;
	private String lblDateOfBirthAr;
	private String lblPassportNumberAr;
	private String lblPassportSourceAr;
	private String lblPassportIssueDateAr;
	private String lblPassportExpiryDateAr;
	private String lblVisaNumberAr;
	private String lblVisaSourceAr;
	private String lblVisaIssueDateAr;
	private String lblVisaExpiryDateAr;
	private String lblProfessionAr;
	private String lblWorkplaceAr;
	private String lblCompanyLicenseNumberAr;
	private String lblLicenseSourceAr;
	private String lblLicenseIssueDateAr;
	private String lblLicenseExpiryDateAr;
	private String lblCountryAr;
	private String lblCityAr;
	private String lblAreaAr;
	private String lblStreetAr;
	private String lblOfficePhone1Ar;
	private String lblOfficePhone2Ar;
	private String lblResidenceNumberAr;
	private String lblFaxAr;
	private String lblCellNumber1Ar;
	private String lblCellNumber2Ar;
	private String lblAddress1Ar;
	private String lblAddress2Ar;
	private String lblPO_BoxAr;
	private String lblEmailAr;
	private String lblContactInfoAr;
	private String lblPersonalInfoAr;
	private String lblIndividualEn;
	private String lblIndividualAr;
	private String lblCompanyEn;
	private String lblCompanyAr;
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblTenantDetailsReportEn;
	private String lblPIMSHeaderAr;
	private String lblTenantDetailsReportAr;
	private String lblLoggedInUser;
	//////////////////////
	
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblTenantDetailsReportEn() {
		return lblTenantDetailsReportEn;
	}
	public void setLblTenantDetailsReportEn(String lblTenantDetailsReportEn) {
		this.lblTenantDetailsReportEn = lblTenantDetailsReportEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblTenantDetailsReportAr() {
		return lblTenantDetailsReportAr;
	}
	public void setLblTenantDetailsReportAr(String lblTenantDetailsReportAr) {
		this.lblTenantDetailsReportAr = lblTenantDetailsReportAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblTenantNumberEn() {
		return lblTenantNumberEn;
	}
	public void setLblTenantNumberEn(String lblTenantNumberEn) {
		this.lblTenantNumberEn = lblTenantNumberEn;
	}
	public String getLblIdNumberEn() {
		return lblIdNumberEn;
	}
	public void setLblIdNumberEn(String lblIdNumberEn) {
		this.lblIdNumberEn = lblIdNumberEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblNationalityEn() {
		return lblNationalityEn;
	}
	public void setLblNationalityEn(String lblNationalityEn) {
		this.lblNationalityEn = lblNationalityEn;
	}
	public String getLblMaritalStatusEn() {
		return lblMaritalStatusEn;
	}
	public void setLblMaritalStatusEn(String lblMaritalStatusEn) {
		this.lblMaritalStatusEn = lblMaritalStatusEn;
	}
	public String getLblTenantTypeEn() {
		return lblTenantTypeEn;
	}
	public void setLblTenantTypeEn(String lblTenantTypeEn) {
		this.lblTenantTypeEn = lblTenantTypeEn;
	}
	public String getLblDateOfBirthEn() {
		return lblDateOfBirthEn;
	}
	public void setLblDateOfBirthEn(String lblDateOfBirthEn) {
		this.lblDateOfBirthEn = lblDateOfBirthEn;
	}
	public String getLblPassportNumberEn() {
		return lblPassportNumberEn;
	}
	public void setLblPassportNumberEn(String lblPassportNumberEn) {
		this.lblPassportNumberEn = lblPassportNumberEn;
	}
	public String getLblPassportSourceEn() {
		return lblPassportSourceEn;
	}
	public void setLblPassportSourceEn(String lblPassportSourceEn) {
		this.lblPassportSourceEn = lblPassportSourceEn;
	}
	public String getLblPassportIssueDateEn() {
		return lblPassportIssueDateEn;
	}
	public void setLblPassportIssueDateEn(String lblPassportIssueDateEn) {
		this.lblPassportIssueDateEn = lblPassportIssueDateEn;
	}
	public String getLblPassportExpiryDateEn() {
		return lblPassportExpiryDateEn;
	}
	public void setLblPassportExpiryDateEn(String lblPassportExpiryDateEn) {
		this.lblPassportExpiryDateEn = lblPassportExpiryDateEn;
	}
	public String getLblVisaNumberEn() {
		return lblVisaNumberEn;
	}
	public void setLblVisaNumberEn(String lblVisaNumberEn) {
		this.lblVisaNumberEn = lblVisaNumberEn;
	}
	public String getLblVisaSourceEn() {
		return lblVisaSourceEn;
	}
	public void setLblVisaSourceEn(String lblVisaSourceEn) {
		this.lblVisaSourceEn = lblVisaSourceEn;
	}
	public String getLblVisaIssueDateEn() {
		return lblVisaIssueDateEn;
	}
	public void setLblVisaIssueDateEn(String lblVisaIssueDateEn) {
		this.lblVisaIssueDateEn = lblVisaIssueDateEn;
	}
	public String getLblVisaExpiryDateEn() {
		return lblVisaExpiryDateEn;
	}
	public void setLblVisaExpiryDateEn(String lblVisaExpiryDateEn) {
		this.lblVisaExpiryDateEn = lblVisaExpiryDateEn;
	}
	public String getLblProfessionEn() {
		return lblProfessionEn;
	}
	public void setLblProfessionEn(String lblProfessionEn) {
		this.lblProfessionEn = lblProfessionEn;
	}
	public String getLblWorkplaceEn() {
		return lblWorkplaceEn;
	}
	public void setLblWorkplaceEn(String lblWorkplaceEn) {
		this.lblWorkplaceEn = lblWorkplaceEn;
	}
	public String getLblCompanyLicenseNumberEn() {
		return lblCompanyLicenseNumberEn;
	}
	public void setLblCompanyLicenseNumberEn(String lblCompanyLicenseNumberEn) {
		this.lblCompanyLicenseNumberEn = lblCompanyLicenseNumberEn;
	}
	public String getLblLicenseSourceEn() {
		return lblLicenseSourceEn;
	}
	public void setLblLicenseSourceEn(String lblLicenseSourceEn) {
		this.lblLicenseSourceEn = lblLicenseSourceEn;
	}
	public String getLblLicenseIssueDateEn() {
		return lblLicenseIssueDateEn;
	}
	public void setLblLicenseIssueDateEn(String lblLicenseIssueDateEn) {
		this.lblLicenseIssueDateEn = lblLicenseIssueDateEn;
	}
	public String getLblLicenseExpiryDateEn() {
		return lblLicenseExpiryDateEn;
	}
	public void setLblLicenseExpiryDateEn(String lblLicenseExpiryDateEn) {
		this.lblLicenseExpiryDateEn = lblLicenseExpiryDateEn;
	}
	public String getLblCountryEn() {
		return lblCountryEn;
	}
	public void setLblCountryEn(String lblCountryEn) {
		this.lblCountryEn = lblCountryEn;
	}
	public String getLblCityEn() {
		return lblCityEn;
	}
	public void setLblCityEn(String lblCityEn) {
		this.lblCityEn = lblCityEn;
	}
	public String getLblAreaEn() {
		return lblAreaEn;
	}
	public void setLblAreaEn(String lblAreaEn) {
		this.lblAreaEn = lblAreaEn;
	}
	public String getLblStreetEn() {
		return lblStreetEn;
	}
	public void setLblStreetEn(String lblStreetEn) {
		this.lblStreetEn = lblStreetEn;
	}
	public String getLblOfficePhone_1En() {
		return lblOfficePhone_1En;
	}
	public void setLblOfficePhone_1En(String lblOfficePhone_1En) {
		this.lblOfficePhone_1En = lblOfficePhone_1En;
	}
	public String getLblOfficePhone_2En() {
		return lblOfficePhone_2En;
	}
	public void setLblOfficePhone_2En(String lblOfficePhone_2En) {
		this.lblOfficePhone_2En = lblOfficePhone_2En;
	}
	public String getLblResidenceNumberEn() {
		return lblResidenceNumberEn;
	}
	public void setLblResidenceNumberEn(String lblResidenceNumberEn) {
		this.lblResidenceNumberEn = lblResidenceNumberEn;
	}
	public String getLblFaxEn() {
		return lblFaxEn;
	}
	public void setLblFaxEn(String lblFaxEn) {
		this.lblFaxEn = lblFaxEn;
	}
	public String getLblCellNumber1En() {
		return lblCellNumber1En;
	}
	public void setLblCellNumber1En(String lblCellNumber1En) {
		this.lblCellNumber1En = lblCellNumber1En;
	}
	public String getLblCellNumber2En() {
		return lblCellNumber2En;
	}
	public void setLblCellNumber2En(String lblCellNumber2En) {
		this.lblCellNumber2En = lblCellNumber2En;
	}
	public String getLblAddress1En() {
		return lblAddress1En;
	}
	public void setLblAddress1En(String lblAddress1En) {
		this.lblAddress1En = lblAddress1En;
	}
	public String getLblAddress2En() {
		return lblAddress2En;
	}
	public void setLblAddress2En(String lblAddress2En) {
		this.lblAddress2En = lblAddress2En;
	}
	public String getLblPO_BoxEn() {
		return lblPO_BoxEn;
	}
	public void setLblPO_BoxEn(String lblPO_BoxEn) {
		this.lblPO_BoxEn = lblPO_BoxEn;
	}
	public String getLblEmailEn() {
		return lblEmailEn;
	}
	public void setLblEmailEn(String lblEmailEn) {
		this.lblEmailEn = lblEmailEn;
	}
	
	public String getLblTenantNumberAr() {
		return lblTenantNumberAr;
	}
	public void setLblTenantNumberAr(String lblTenantNumberAr) {
		this.lblTenantNumberAr = lblTenantNumberAr;
	}
	public String getLblIdNumberAr() {
		return lblIdNumberAr;
	}
	public void setLblIdNumberAr(String lblIdNumberAr) {
		this.lblIdNumberAr = lblIdNumberAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblNationalityAr() {
		return lblNationalityAr;
	}
	public void setLblNationalityAr(String lblNationalityAr) {
		this.lblNationalityAr = lblNationalityAr;
	}
	public String getLblMaritalStatusAr() {
		return lblMaritalStatusAr;
	}
	public void setLblMaritalStatusAr(String lblMaritalStatusAr) {
		this.lblMaritalStatusAr = lblMaritalStatusAr;
	}
	public String getLblTenantTypeAr() {
		return lblTenantTypeAr;
	}
	public void setLblTenantTypeAr(String lblTenantTypeAr) {
		this.lblTenantTypeAr = lblTenantTypeAr;
	}
	public String getLblDateOfBirthAr() {
		return lblDateOfBirthAr;
	}
	public void setLblDateOfBirthAr(String lblDateOfBirthAr) {
		this.lblDateOfBirthAr = lblDateOfBirthAr;
	}
	public String getLblPassportNumberAr() {
		return lblPassportNumberAr;
	}
	public void setLblPassportNumberAr(String lblPassportNumberAr) {
		this.lblPassportNumberAr = lblPassportNumberAr;
	}
	public String getLblPassportSourceAr() {
		return lblPassportSourceAr;
	}
	public void setLblPassportSourceAr(String lblPassportSourceAr) {
		this.lblPassportSourceAr = lblPassportSourceAr;
	}
	public String getLblPassportIssueDateAr() {
		return lblPassportIssueDateAr;
	}
	public void setLblPassportIssueDateAr(String lblPassportIssueDateAr) {
		this.lblPassportIssueDateAr = lblPassportIssueDateAr;
	}
	public String getLblPassportExpiryDateAr() {
		return lblPassportExpiryDateAr;
	}
	public void setLblPassportExpiryDateAr(String lblPassportExpiryDateAr) {
		this.lblPassportExpiryDateAr = lblPassportExpiryDateAr;
	}
	public String getLblVisaNumberAr() {
		return lblVisaNumberAr;
	}
	public void setLblVisaNumberAr(String lblVisaNumberAr) {
		this.lblVisaNumberAr = lblVisaNumberAr;
	}
	public String getLblVisaSourceAr() {
		return lblVisaSourceAr;
	}
	public void setLblVisaSourceAr(String lblVisaSourceAr) {
		this.lblVisaSourceAr = lblVisaSourceAr;
	}
	public String getLblVisaIssueDateAr() {
		return lblVisaIssueDateAr;
	}
	public void setLblVisaIssueDateAr(String lblVisaIssueDateAr) {
		this.lblVisaIssueDateAr = lblVisaIssueDateAr;
	}
	public String getLblVisaExpiryDateAr() {
		return lblVisaExpiryDateAr;
	}
	public void setLblVisaExpiryDateAr(String lblVisaExpiryDateAr) {
		this.lblVisaExpiryDateAr = lblVisaExpiryDateAr;
	}
	public String getLblProfessionAr() {
		return lblProfessionAr;
	}
	public void setLblProfessionAr(String lblProfessionAr) {
		this.lblProfessionAr = lblProfessionAr;
	}
	public String getLblWorkplaceAr() {
		return lblWorkplaceAr;
	}
	public void setLblWorkplaceAr(String lblWorkplaceAr) {
		this.lblWorkplaceAr = lblWorkplaceAr;
	}
	public String getLblCompanyLicenseNumberAr() {
		return lblCompanyLicenseNumberAr;
	}
	public void setLblCompanyLicenseNumberAr(String lblCompanyLicenseNumberAr) {
		this.lblCompanyLicenseNumberAr = lblCompanyLicenseNumberAr;
	}
	public String getLblLicenseSourceAr() {
		return lblLicenseSourceAr;
	}
	public void setLblLicenseSourceAr(String lblLicenseSourceAr) {
		this.lblLicenseSourceAr = lblLicenseSourceAr;
	}
	public String getLblLicenseIssueDateAr() {
		return lblLicenseIssueDateAr;
	}
	public void setLblLicenseIssueDateAr(String lblLicenseIssueDateAr) {
		this.lblLicenseIssueDateAr = lblLicenseIssueDateAr;
	}
	public String getLblLicenseExpiryDateAr() {
		return lblLicenseExpiryDateAr;
	}
	public void setLblLicenseExpiryDateAr(String lblLicenseExpiryDateAr) {
		this.lblLicenseExpiryDateAr = lblLicenseExpiryDateAr;
	}
	public String getLblCountryAr() {
		return lblCountryAr;
	}
	public void setLblCountryAr(String lblCountryAr) {
		this.lblCountryAr = lblCountryAr;
	}
	public String getLblCityAr() {
		return lblCityAr;
	}
	public void setLblCityAr(String lblCityAr) {
		this.lblCityAr = lblCityAr;
	}
	public String getLblAreaAr() {
		return lblAreaAr;
	}
	public void setLblAreaAr(String lblAreaAr) {
		this.lblAreaAr = lblAreaAr;
	}
	public String getLblStreetAr() {
		return lblStreetAr;
	}
	public void setLblStreetAr(String lblStreetAr) {
		this.lblStreetAr = lblStreetAr;
	}
	public String getLblOfficePhone1Ar() {
		return lblOfficePhone1Ar;
	}
	public void setLblOfficePhone1Ar(String lblOfficePhone1Ar) {
		this.lblOfficePhone1Ar = lblOfficePhone1Ar;
	}
	public String getLblOfficePhone2Ar() {
		return lblOfficePhone2Ar;
	}
	public void setLblOfficePhone2Ar(String lblOfficePhone2Ar) {
		this.lblOfficePhone2Ar = lblOfficePhone2Ar;
	}
	public String getLblResidenceNumberAr() {
		return lblResidenceNumberAr;
	}
	public void setLblResidenceNumberAr(String lblResidenceNumberAr) {
		this.lblResidenceNumberAr = lblResidenceNumberAr;
	}
	public String getLblFaxAr() {
		return lblFaxAr;
	}
	public void setLblFaxAr(String lblFaxAr) {
		this.lblFaxAr = lblFaxAr;
	}
	public String getLblCellNumber1Ar() {
		return lblCellNumber1Ar;
	}
	public void setLblCellNumber1Ar(String lblCellNumber1Ar) {
		this.lblCellNumber1Ar = lblCellNumber1Ar;
	}
	public String getLblCellNumber2Ar() {
		return lblCellNumber2Ar;
	}
	public void setLblCellNumber2Ar(String lblCellNumber2Ar) {
		this.lblCellNumber2Ar = lblCellNumber2Ar;
	}
	public String getLblAddress1Ar() {
		return lblAddress1Ar;
	}
	public void setLblAddress1Ar(String lblAddress1Ar) {
		this.lblAddress1Ar = lblAddress1Ar;
	}
	public String getLblAddress2Ar() {
		return lblAddress2Ar;
	}
	public void setLblAddress2Ar(String lblAddress2Ar) {
		this.lblAddress2Ar = lblAddress2Ar;
	}
	public String getLblPO_BoxAr() {
		return lblPO_BoxAr;
	}
	public void setLblPO_BoxAr(String lblPO_BoxAr) {
		this.lblPO_BoxAr = lblPO_BoxAr;
	}
	public String getLblEmailAr() {
		return lblEmailAr;
	}
	public void setLblEmailAr(String lblEmailAr) {
		this.lblEmailAr = lblEmailAr;
	}
	public String getLblContactInfoEn() {
		return lblContactInfoEn;
	}
	public void setLblContactInfoEn(String lblContactInfoEn) {
		this.lblContactInfoEn = lblContactInfoEn;
	}
	public String getLblPersonalInfoEn() {
		return lblPersonalInfoEn;
	}
	public void setLblPersonalInfoEn(String lblPersonalInfoEn) {
		this.lblPersonalInfoEn = lblPersonalInfoEn;
	}
	public String getLblContactInfoAr() {
		return lblContactInfoAr;
	}
	public void setLblContactInfoAr(String lblContactInfoAr) {
		this.lblContactInfoAr = lblContactInfoAr;
	}
	public String getLblPersonalInfoAr() {
		return lblPersonalInfoAr;
	}
	public void setLblPersonalInfoAr(String lblPersonalInfoAr) {
		this.lblPersonalInfoAr = lblPersonalInfoAr;
	}
	public String getLblNationalIdEn() {
		return lblNationalIdEn;
	}
	public void setLblNationalIdEn(String lblNationalIdEn) {
		this.lblNationalIdEn = lblNationalIdEn;
	}
	public String getLblNationalIdAr() {
		return lblNationalIdAr;
	}
	public void setLblNationalIdAr(String lblNationalIdAr) {
		this.lblNationalIdAr = lblNationalIdAr;
	}
	public String getLblIndividualEn() {
		return lblIndividualEn;
	}
	public void setLblIndividualEn(String lblIndividualEn) {
		this.lblIndividualEn = lblIndividualEn;
	}
	public String getLblIndividualAr() {
		return lblIndividualAr;
	}
	public void setLblIndividualAr(String lblIndividualAr) {
		this.lblIndividualAr = lblIndividualAr;
	}
	public String getLblCompanyEn() {
		return lblCompanyEn;
	}
	public void setLblCompanyEn(String lblCompanyEn) {
		this.lblCompanyEn = lblCompanyEn;
	}
	public String getLblCompanyAr() {
		return lblCompanyAr;
	}
	public void setLblCompanyAr(String lblCompanyAr) {
		this.lblCompanyAr = lblCompanyAr;
	}
	
	

}
