package com.avanza.pims.report.dataDef;

public class FileDetailsReportLabelDataDef extends AbstractReportDataDef
{	

	////////Report and MEMS Header////////
	private String lblMEMSHeaderEn;
	private String lblReportNameEn;
	private String lblMEMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	
	public String getLblMEMSHeaderEn() {
		return lblMEMSHeaderEn;
	}
	public void setLblMEMSHeaderEn(String lblMEMSHeaderEn) {
		this.lblMEMSHeaderEn = lblMEMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblMEMSHeaderAr() {
		return lblMEMSHeaderAr;
	}
	public void setLblMEMSHeaderAr(String lblMEMSHeaderAr) {
		this.lblMEMSHeaderAr = lblMEMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
}
