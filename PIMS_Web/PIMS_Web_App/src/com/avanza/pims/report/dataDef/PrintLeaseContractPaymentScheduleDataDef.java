package com.avanza.pims.report.dataDef;

public class PrintLeaseContractPaymentScheduleDataDef extends AbstractReportDataDef {

	private String paymentScheduleId;
	private String paymentTypeId;
	private String paymentScheduleNumber;
	private String paymentScheduleDueOn;
	private String paymentSchedulePaymentDate;
	private String paymentScheduleAmount;
	private String paymentTypeDescEn;
	private String paymentTypeDescAr;
	private String domainDataStatusEn;
	private String domainDataStatusAr;
	private String domainDataPaymentModeEn;
	private String domainDataPaymentModeAr;
	private String paymentRecptDetailMethodRefNo;
	private String bankEn;
	private String bankAr;
	private String contractId;
	private String paymentModeId;
	
	public String getPaymentModeId() {
		return paymentModeId;
	}
	public void setPaymentModeId(String paymentModeId) {
		this.paymentModeId = paymentModeId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}
	public String getPaymentScheduleNumber() {
		return paymentScheduleNumber;
	}
	public void setPaymentScheduleNumber(String paymentScheduleNumber) {
		this.paymentScheduleNumber = paymentScheduleNumber;
	}
	public String getPaymentScheduleDueOn() {
		return paymentScheduleDueOn;
	}
	public void setPaymentScheduleDueOn(String paymentScheduleDueOn) {
		this.paymentScheduleDueOn = paymentScheduleDueOn;
	}
	public String getPaymentSchedulePaymentDate() {
		return paymentSchedulePaymentDate;
	}
	public void setPaymentSchedulePaymentDate(String paymentSchedulePaymentDate) {
		this.paymentSchedulePaymentDate = paymentSchedulePaymentDate;
	}
	public String getPaymentScheduleAmount() {
		return paymentScheduleAmount;
	}
	public void setPaymentScheduleAmount(String paymentScheduleAmount) {
		this.paymentScheduleAmount = paymentScheduleAmount;
	}
	public String getPaymentTypeDescEn() {
		return paymentTypeDescEn;
	}
	public void setPaymentTypeDescEn(String paymentTypeDescEn) {
		this.paymentTypeDescEn = paymentTypeDescEn;
	}
	public String getPaymentTypeDescAr() {
		return paymentTypeDescAr;
	}
	public void setPaymentTypeDescAr(String paymentTypeDescAr) {
		this.paymentTypeDescAr = paymentTypeDescAr;
	}
	public String getDomainDataStatusEn() {
		return domainDataStatusEn;
	}
	public void setDomainDataStatusEn(String domainDataStatusEn) {
		this.domainDataStatusEn = domainDataStatusEn;
	}
	public String getDomainDataStatusAr() {
		return domainDataStatusAr;
	}
	public void setDomainDataStatusAr(String domainDataStatusAr) {
		this.domainDataStatusAr = domainDataStatusAr;
	}
	public String getDomainDataPaymentModeEn() {
		return domainDataPaymentModeEn;
	}
	public void setDomainDataPaymentModeEn(String domainDataPaymentModeEn) {
		this.domainDataPaymentModeEn = domainDataPaymentModeEn;
	}
	public String getDomainDataPaymentModeAr() {
		return domainDataPaymentModeAr;
	}
	public void setDomainDataPaymentModeAr(String domainDataPaymentModeAr) {
		this.domainDataPaymentModeAr = domainDataPaymentModeAr;
	}
	public String getPaymentRecptDetailMethodRefNo() {
		return paymentRecptDetailMethodRefNo;
	}
	public void setPaymentRecptDetailMethodRefNo(
			String paymentRecptDetailMethodRefNo) {
		this.paymentRecptDetailMethodRefNo = paymentRecptDetailMethodRefNo;
	}
	public String getBankEn() {
		return bankEn;
	}
	public void setBankEn(String bankEn) {
		this.bankEn = bankEn;
	}
	public String getBankAr() {
		return bankAr;
	}
	public void setBankAr(String bankAr) {
		this.bankAr = bankAr;
	}
}
