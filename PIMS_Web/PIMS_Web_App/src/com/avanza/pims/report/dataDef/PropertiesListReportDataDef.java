package com.avanza.pims.report.dataDef;

public class PropertiesListReportDataDef extends AbstractReportDataDef {
	
	private String propertyNumber;
	private String propetyName;
	private String propertyTypeEn;
	private String propertyTypeAr;
	private String address;
	private String totalUnits;
	private String emirate;
	private String totalFloor;
	private String total;
//	private String propertyId;
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropetyName() {
		return propetyName;
	}
	public void setPropetyName(String propetyName) {
		this.propetyName = propetyName;
	}
	public String getPropertyTypeEn() {
		return propertyTypeEn;
	}
	public void setPropertyTypeEn(String propertyTypeEn) {
		this.propertyTypeEn = propertyTypeEn;
	}
	public String getPropertyTypeAr() {
		return propertyTypeAr;
	}
	public void setPropertyTypeAr(String propertyTypeAr) {
		this.propertyTypeAr = propertyTypeAr;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTotalUnits() {
		return totalUnits;
	}
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getTotalFloor() {
		return totalFloor;
	}
	public void setTotalFloor(String totalFloor) {
		this.totalFloor = totalFloor;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
}
