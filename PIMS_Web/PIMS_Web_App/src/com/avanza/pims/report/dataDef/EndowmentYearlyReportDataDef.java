package com.avanza.pims.report.dataDef;

public class EndowmentYearlyReportDataDef extends AbstractReportDataDef
{
	private String waaqifName;
	private String endowmentName;
	private String waaqifId;
	private String endowmentId;
	private String endowmentManager;
	private String endowmentTypeId;
	private String endowmentType;
	private String endowmentCreatedOn;
	private String endowmentFileId;
	private String endowmentFileNumber;
	private String endowmentFileName;
	private Double totalDeposits;
	private Double totalWithDrawals;
	private String percentageOfRentedUnits;
	private String totalUnits;
	private String totalCommercialUnits;
	private String totalResidentialUnits;
	private String endowmentPropertyArea;
	private String endowmentPropertyCommunity;
	private String endowmentPropertyType;
	private String endowmentLandNumber;
	private String endowmentBeneficiaries;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	
	public String getEndowmentFileId() {
		return endowmentFileId;
	}
	public void setEndowmentFileId(String endowmentFileId) {
		this.endowmentFileId = endowmentFileId;
	}
	public String getEndowmentName() {
		return endowmentName;
	}
	public void setEndowmentName(String endowmentName) {
		this.endowmentName = endowmentName;
	}
	public String getEndowmentManager() {
		return endowmentManager;
	}
	public void setEndowmentManager(String endowmentManager) {
		this.endowmentManager = endowmentManager;
	}
	public String getWaaqifId() {
		return waaqifId;
	}
	public void setWaaqifId(String waaqifId) {
		this.waaqifId = waaqifId;
	}
	public String getEndowmentId() {
		return endowmentId;
	}
	public void setEndowmentId(String endowmentId) {
		this.endowmentId = endowmentId;
	}
	public String getWaaqifName() {
		return waaqifName;
	}
	public void setWaaqifName(String waaqifName) {
		this.waaqifName = waaqifName;
	}
	public String getEndowmentTypeId() {
		return endowmentTypeId;
	}
	public void setEndowmentTypeId(String endowmentTypeId) {
		this.endowmentTypeId = endowmentTypeId;
	}
	public String getEndowmentType() {
		return endowmentType;
	}
	public void setEndowmentType(String endowmentType) {
		this.endowmentType = endowmentType;
	}
	public String getEndowmentCreatedOn() {
		return endowmentCreatedOn;
	}
	public void setEndowmentCreatedOn(String endowmentCreatedOn) {
		this.endowmentCreatedOn = endowmentCreatedOn;
	}
	public String getEndowmentFileNumber() {
		return endowmentFileNumber;
	}
	public void setEndowmentFileNumber(String endowmentFileNumber) {
		this.endowmentFileNumber = endowmentFileNumber;
	}
	public String getEndowmentFileName() {
		return endowmentFileName;
	}
	public void setEndowmentFileName(String endowmentFileName) {
		this.endowmentFileName = endowmentFileName;
	}
	public Double getTotalDeposits() {
		return totalDeposits;
	}
	public void setTotalDeposits(Double totalDeposits) {
		this.totalDeposits = totalDeposits;
	}
	public Double getTotalWithDrawals() {
		return totalWithDrawals;
	}
	public void setTotalWithDrawals(Double totalWithDrawals) {
		this.totalWithDrawals = totalWithDrawals;
	}
	public String getPercentageOfRentedUnits() {
		return percentageOfRentedUnits;
	}
	public void setPercentageOfRentedUnits(String percentageOfRentedUnits) {
		this.percentageOfRentedUnits = percentageOfRentedUnits;
	}
	public String getTotalUnits() {
		return totalUnits;
	}
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}
	public String getTotalCommercialUnits() {
		return totalCommercialUnits;
	}
	public void setTotalCommercialUnits(String totalCommercialUnits) {
		this.totalCommercialUnits = totalCommercialUnits;
	}
	public String getTotalResidentialUnits() {
		return totalResidentialUnits;
	}
	public void setTotalResidentialUnits(String totalResidentialUnits) {
		this.totalResidentialUnits = totalResidentialUnits;
	}
	public String getEndowmentPropertyArea() {
		return endowmentPropertyArea;
	}
	public void setEndowmentPropertyArea(String endowmentPropertyArea) {
		this.endowmentPropertyArea = endowmentPropertyArea;
	}
	public String getEndowmentPropertyCommunity() {
		return endowmentPropertyCommunity;
	}
	public void setEndowmentPropertyCommunity(String endowmentPropertyCommunity) {
		this.endowmentPropertyCommunity = endowmentPropertyCommunity;
	}
	public String getEndowmentPropertyType() {
		return endowmentPropertyType;
	}
	public void setEndowmentPropertyType(String endowmentPropertyType) {
		this.endowmentPropertyType = endowmentPropertyType;
	}
	public String getEndowmentLandNumber() {
		return endowmentLandNumber;
	}
	public void setEndowmentLandNumber(String endowmentLandNumber) {
		this.endowmentLandNumber = endowmentLandNumber;
	}
	public String getEndowmentBeneficiaries() {
		return endowmentBeneficiaries;
	}
	public void setEndowmentBeneficiaries(String endowmentBeneficiaries) {
		this.endowmentBeneficiaries = endowmentBeneficiaries;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}	 
}
	
