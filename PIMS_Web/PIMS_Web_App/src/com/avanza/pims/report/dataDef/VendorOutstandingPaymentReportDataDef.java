package com.avanza.pims.report.dataDef;

public class VendorOutstandingPaymentReportDataDef extends AbstractReportDataDef {

	private String paymentNumber;
	private String paymentDate;
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentAmount;
	private String vendorNumber;
	private String vendorName;
	private String contractNumber;
	private String contractTypeEn;
	private String contractTypeAr;
	private String paymentStatusEn;
	private String paymentStatusAr;
	
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getVendorNumber() {
		return vendorNumber;
	}
	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractTypeEn() {
		return contractTypeEn;
	}
	public void setContractTypeEn(String contractTypeEn) {
		this.contractTypeEn = contractTypeEn;
	}
	public String getContractTypeAr() {
		return contractTypeAr;
	}
	public void setContractTypeAr(String contractTypeAr) {
		this.contractTypeAr = contractTypeAr;
	}
	public String getPaymentStatusEn() {
		return paymentStatusEn;
	}
	public void setPaymentStatusEn(String paymentStatusEn) {
		this.paymentStatusEn = paymentStatusEn;
	}
	public String getPaymentStatusAr() {
		return paymentStatusAr;
	}
	public void setPaymentStatusAr(String paymentStatusAr) {
		this.paymentStatusAr = paymentStatusAr;
	}
	

	
	

}
