package com.avanza.pims.report.dataDef;

public class PropertyOccupancySummaryLabelDataDef extends AbstractReportDataDef
{
	//English
	private String lblTotalUnitsEn;
	private String lblTotalRentOfUnitsEn;
	private String lblTotalRentedUnitsEn;
	private String lblTotalRentOfRentedUnitsEn;
	private String lblTotalVacantUnitsEn;
	private String lblTotalRentOfVacantUnitsEn;
	private String lblTotalContractualUnitsEn;
	private String lblTotalRentOfCUnitsEn;
	private String lblResidentialUnitsEn;
	private String lblCommercialUnitsEn;
	private String lblAllUnitsEn;
	private String lblEndowmentEn;
	private String lblMinorsEn;
	private String lblAmafAndMinorsEn;
	private String lblAmafEn;
	private String lblReportNameEn;
	private String lblGrandTotalEn;
	private String lblPIMSHeaderEn;
	private String lblLoggedInUser;
	
	//Arabic
	private String lblTotalUnitsAr;
	private String lblTotalRentOfUnitsAr;
	private String lblTotalRentedUnitsAr;
	private String lblTotalRentOfRentedUnitsAr;
	private String lblTotalVacantUnitsAr;
	private String lblTotalRentOfVacantUnitsAr;
	private String lblTotalContractualUnitsAr;
	private String lblTotalRentOfCUnitsAr;
	private String lblResidentialUnitsAr;
	private String lblCommercialUnitsAr;
	private String lblAllUnitsAr;
	private String lblEndowmentAr;
	private String lblMinorsAr;
	private String lblAmafAndMinorsAr;
	private String lblAmafAr;
	private String lblReportNameAr;
	private String lblGrandTotalAr;
	private String lblPIMSHeaderAr;
	
	public String getLblTotalUnitsEn() {
		return lblTotalUnitsEn;
	}
	public void setLblTotalUnitsEn(String lblTotalUnitsEn) {
		this.lblTotalUnitsEn = lblTotalUnitsEn;
	}
	public String getLblTotalRentOfUnitsEn() {
		return lblTotalRentOfUnitsEn;
	}
	public void setLblTotalRentOfUnitsEn(String lblTotalRentOfUnitsEn) {
		this.lblTotalRentOfUnitsEn = lblTotalRentOfUnitsEn;
	}
	public String getLblTotalRentedUnitsEn() {
		return lblTotalRentedUnitsEn;
	}
	public void setLblTotalRentedUnitsEn(String lblTotalRentedUnitsEn) {
		this.lblTotalRentedUnitsEn = lblTotalRentedUnitsEn;
	}
	public String getLblTotalRentOfRentedUnitsEn() {
		return lblTotalRentOfRentedUnitsEn;
	}
	public void setLblTotalRentOfRentedUnitsEn(String lblTotalRentOfRentedUnitsEn) {
		this.lblTotalRentOfRentedUnitsEn = lblTotalRentOfRentedUnitsEn;
	}
	public String getLblTotalVacantUnitsEn() {
		return lblTotalVacantUnitsEn;
	}
	public void setLblTotalVacantUnitsEn(String lblTotalVacantUnitsEn) {
		this.lblTotalVacantUnitsEn = lblTotalVacantUnitsEn;
	}
	public String getLblTotalRentOfVacantUnitsEn() {
		return lblTotalRentOfVacantUnitsEn;
	}
	public void setLblTotalRentOfVacantUnitsEn(String lblTotalRentOfVacantUnitsEn) {
		this.lblTotalRentOfVacantUnitsEn = lblTotalRentOfVacantUnitsEn;
	}
	public String getLblTotalContractualUnitsEn() {
		return lblTotalContractualUnitsEn;
	}
	public void setLblTotalContractualUnitsEn(String lblTotalContractualUnitsEn) {
		this.lblTotalContractualUnitsEn = lblTotalContractualUnitsEn;
	}
	public String getLblResidentialUnitsEn() {
		return lblResidentialUnitsEn;
	}
	public void setLblResidentialUnitsEn(String lblResidentialUnitsEn) {
		this.lblResidentialUnitsEn = lblResidentialUnitsEn;
	}
	public String getLblCommercialUnitsEn() {
		return lblCommercialUnitsEn;
	}
	public void setLblCommercialUnitsEn(String lblCommercialUnitsEn) {
		this.lblCommercialUnitsEn = lblCommercialUnitsEn;
	}
	public String getLblAllUnitsEn() {
		return lblAllUnitsEn;
	}
	public void setLblAllUnitsEn(String lblAllUnitsEn) {
		this.lblAllUnitsEn = lblAllUnitsEn;
	}
	public String getLblEndowmentEn() {
		return lblEndowmentEn;
	}
	public void setLblEndowmentEn(String lblEndowmentEn) {
		this.lblEndowmentEn = lblEndowmentEn;
	}
	public String getLblMinorsEn() {
		return lblMinorsEn;
	}
	public void setLblMinorsEn(String lblMinorsEn) {
		this.lblMinorsEn = lblMinorsEn;
	}
	public String getLblAmafAndMinorsEn() {
		return lblAmafAndMinorsEn;
	}
	public void setLblAmafAndMinorsEn(String lblAmafAndMinorsEn) {
		this.lblAmafAndMinorsEn = lblAmafAndMinorsEn;
	}
	public String getLblTotalUnitsAr() {
		return lblTotalUnitsAr;
	}
	public void setLblTotalUnitsAr(String lblTotalUnitsAr) {
		this.lblTotalUnitsAr = lblTotalUnitsAr;
	}
	public String getLblTotalRentOfUnitsAr() {
		return lblTotalRentOfUnitsAr;
	}
	public void setLblTotalRentOfUnitsAr(String lblTotalRentOfUnitsAr) {
		this.lblTotalRentOfUnitsAr = lblTotalRentOfUnitsAr;
	}
	public String getLblTotalRentedUnitsAr() {
		return lblTotalRentedUnitsAr;
	}
	public void setLblTotalRentedUnitsAr(String lblTotalRentedUnitsAr) {
		this.lblTotalRentedUnitsAr = lblTotalRentedUnitsAr;
	}
	public String getLblTotalRentOfRentedUnitsAr() {
		return lblTotalRentOfRentedUnitsAr;
	}
	public void setLblTotalRentOfRentedUnitsAr(String lblTotalRentOfRentedUnitsAr) {
		this.lblTotalRentOfRentedUnitsAr = lblTotalRentOfRentedUnitsAr;
	}
	public String getLblTotalVacantUnitsAr() {
		return lblTotalVacantUnitsAr;
	}
	public void setLblTotalVacantUnitsAr(String lblTotalVacantUnitsAr) {
		this.lblTotalVacantUnitsAr = lblTotalVacantUnitsAr;
	}
	public String getLblTotalRentOfVacantUnitsAr() {
		return lblTotalRentOfVacantUnitsAr;
	}
	public void setLblTotalRentOfVacantUnitsAr(String lblTotalRentOfVacantUnitsAr) {
		this.lblTotalRentOfVacantUnitsAr = lblTotalRentOfVacantUnitsAr;
	}
	public String getLblTotalContractualUnitsAr() {
		return lblTotalContractualUnitsAr;
	}
	public void setLblTotalContractualUnitsAr(String lblTotalContractualUnitsAr) {
		this.lblTotalContractualUnitsAr = lblTotalContractualUnitsAr;
	}
	public String getLblResidentialUnitsAr() {
		return lblResidentialUnitsAr;
	}
	public void setLblResidentialUnitsAr(String lblResidentialUnitsAr) {
		this.lblResidentialUnitsAr = lblResidentialUnitsAr;
	}
	public String getLblCommercialUnitsAr() {
		return lblCommercialUnitsAr;
	}
	public void setLblCommercialUnitsAr(String lblCommercialUnitsAr) {
		this.lblCommercialUnitsAr = lblCommercialUnitsAr;
	}
	public String getLblAllUnitsAr() {
		return lblAllUnitsAr;
	}
	public void setLblAllUnitsAr(String lblAllUnitsAr) {
		this.lblAllUnitsAr = lblAllUnitsAr;
	}
	public String getLblEndowmentAr() {
		return lblEndowmentAr;
	}
	public void setLblEndowmentAr(String lblEndowmentAr) {
		this.lblEndowmentAr = lblEndowmentAr;
	}
	public String getLblMinorsAr() {
		return lblMinorsAr;
	}
	public void setLblMinorsAr(String lblMinorsAr) {
		this.lblMinorsAr = lblMinorsAr;
	}
	public String getLblAmafAndMinorsAr() {
		return lblAmafAndMinorsAr;
	}
	public void setLblAmafAndMinorsAr(String lblAmafAndMinorsAr) {
		this.lblAmafAndMinorsAr = lblAmafAndMinorsAr;
	}
	public String getLblAmafEn() {
		return lblAmafEn;
	}
	public void setLblAmafEn(String lblAmafEn) {
		this.lblAmafEn = lblAmafEn;
	}
	public String getLblAmafAr() {
		return lblAmafAr;
	}
	public void setLblAmafAr(String lblAmafAr) {
		this.lblAmafAr = lblAmafAr;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblTotalRentOfCUnitsEn() {
		return lblTotalRentOfCUnitsEn;
	}
	public void setLblTotalRentOfCUnitsEn(String lblTotalRentOfCUnitsEn) {
		this.lblTotalRentOfCUnitsEn = lblTotalRentOfCUnitsEn;
	}
	public String getLblTotalRentOfCUnitsAr() {
		return lblTotalRentOfCUnitsAr;
	}
	public void setLblTotalRentOfCUnitsAr(String lblTotalRentOfCUnitsAr) {
		this.lblTotalRentOfCUnitsAr = lblTotalRentOfCUnitsAr;
	}
	public String getLblGrandTotalEn() {
		return lblGrandTotalEn;
	}
	public void setLblGrandTotalEn(String lblGrandTotalEn) {
		this.lblGrandTotalEn = lblGrandTotalEn;
	}
	public String getLblGrandTotalAr() {
		return lblGrandTotalAr;
	}
	public void setLblGrandTotalAr(String lblGrandTotalAr) {
		this.lblGrandTotalAr = lblGrandTotalAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
}
