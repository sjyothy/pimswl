package com.avanza.pims.report.dataDef;

public class AuctionReportDataDef extends AbstractReportDataDef
{
	private String auctionId;
	private String auctionTitle;
	private String auctionNumber;
	private String auctionDesc;
	private String publicationDate;
	private String auctionDate;
	private String auctionStatusId;
	private String auctionStatusEn;
	private String auctionStatusAr;
	private String outBiddingValue;
	private String registrationStartDate;
	private String registrationEndDate;
	private String bidderId;
	private String bidderName;
	private String bidderCellNumber;
	private String telephoneNumber;
	private String auctionUnitId;
	private String unitId;
	private String unitCostCenter;
	private String unitNumber;
	private String depositAmount;
	private String requestId;
	private String requestNumber;
	private String requestStatusId;
	private String requestStatusEn;
	private String requestStatusAr;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8; 
	private String field9;
	private String field10;
	private String bidderStatusEn;
	private String bidderStatusAr;
	public String getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}
	public String getAuctionTitle() {
		return auctionTitle;
	}
	public void setAuctionTitle(String auctionTitle) {
		this.auctionTitle = auctionTitle;
	}
	public String getAuctionNumber() {
		return auctionNumber;
	}
	public void setAuctionNumber(String auctionNumber) {
		this.auctionNumber = auctionNumber;
	}
	public String getAuctionDesc() {
		return auctionDesc;
	}
	public void setAuctionDesc(String auctionDesc) {
		this.auctionDesc = auctionDesc;
	}
	public String getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	public String getAuctionDate() {
		return auctionDate;
	}
	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}
	public String getAuctionStatusId() {
		return auctionStatusId;
	}
	public void setAuctionStatusId(String auctionStatusId) {
		this.auctionStatusId = auctionStatusId;
	}
	public String getAuctionStatusEn() {
		return auctionStatusEn;
	}
	public void setAuctionStatusEn(String auctionStatusEn) {
		this.auctionStatusEn = auctionStatusEn;
	}
	public String getAuctionStatusAr() {
		return auctionStatusAr;
	}
	public void setAuctionStatusAr(String auctionStatusAr) {
		this.auctionStatusAr = auctionStatusAr;
	}
	public String getOutBiddingValue() {
		return outBiddingValue;
	}
	public void setOutBiddingValue(String outBiddingValue) {
		this.outBiddingValue = outBiddingValue;
	}
	public String getRegistrationStartDate() {
		return registrationStartDate;
	}
	public void setRegistrationStartDate(String registrationStartDate) {
		this.registrationStartDate = registrationStartDate;
	}
	public String getRegistrationEndDate() {
		return registrationEndDate;
	}
	public void setRegistrationEndDate(String registrationEndDate) {
		this.registrationEndDate = registrationEndDate;
	}
	public String getBidderId() {
		return bidderId;
	}
	public void setBidderId(String bidderId) {
		this.bidderId = bidderId;
	}
	public String getBidderName() {
		return bidderName;
	}
	public void setBidderName(String bidderName) {
		this.bidderName = bidderName;
	}
	public String getBidderCellNumber() {
		return bidderCellNumber;
	}
	public void setBidderCellNumber(String bidderCellNumber) {
		this.bidderCellNumber = bidderCellNumber;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getAuctionUnitId() {
		return auctionUnitId;
	}
	public void setAuctionUnitId(String auctionUnitId) {
		this.auctionUnitId = auctionUnitId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getUnitCostCenter() {
		return unitCostCenter;
	}
	public void setUnitCostCenter(String unitCostCenter) {
		this.unitCostCenter = unitCostCenter;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getRequestStatusId() {
		return requestStatusId;
	}
	public void setRequestStatusId(String requestStatusId) {
		this.requestStatusId = requestStatusId;
	}
	public String getRequestStatusEn() {
		return requestStatusEn;
	}
	public void setRequestStatusEn(String requestStatusEn) {
		this.requestStatusEn = requestStatusEn;
	}
	public String getRequestStatusAr() {
		return requestStatusAr;
	}
	public void setRequestStatusAr(String requestStatusAr) {
		this.requestStatusAr = requestStatusAr;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getBidderStatusEn() {
		return bidderStatusEn;
	}
	public void setBidderStatusEn(String bidderStatusEn) {
		this.bidderStatusEn = bidderStatusEn;
	}
	public String getBidderStatusAr() {
		return bidderStatusAr;
	}
	public void setBidderStatusAr(String bidderStatusAr) {
		this.bidderStatusAr = bidderStatusAr;
	}
	
}
