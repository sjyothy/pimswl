package com.avanza.pims.report.dataDef;

public class ContractRenewalHistoryReportDataDef extends AbstractReportDataDef {

	private String topParentID;
	private String topParentNumber;
	private String level;
	private String propertyName;
	private String ownershipTypeEn;
	private String ownershipTypeAr;
	private String emirates;	
	private String area;				
	private String unitNumber;	 			
	private String contractNumber;				
	private String contractStartDate;	 			
	private String contractEndDate;	  			
	private String contractRentAmount;	 			
	private String tenantFirstName;	 			
	private String tenantMiddleName;
	private String tenantLastName;
	private String contractTypeEn;	 			
	private String contractTypeAr;	 			
	private String contractStatusEn;
	private String contractStatusAr;
	private String companyName;

	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getOwnershipTypeEn() {
		return ownershipTypeEn;
	}
	public void setOwnershipTypeEn(String ownershipTypeEn) {
		this.ownershipTypeEn = ownershipTypeEn;
	}
	public String getOwnershipTypeAr() {
		return ownershipTypeAr;
	}
	public void setOwnershipTypeAr(String ownershipTypeAr) {
		this.ownershipTypeAr = ownershipTypeAr;
	}
	public String getEmirates() {
		return emirates;
	}
	public void setEmirates(String emirates) {
		this.emirates = emirates;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractRentAmount() {
		return contractRentAmount;
	}
	public void setContractRentAmount(String contractRentAmount) {
		this.contractRentAmount = contractRentAmount;
	}
	
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getContractTypeEn() {
		return contractTypeEn;
	}
	public void setContractTypeEn(String contractTypeEn) {
		this.contractTypeEn = contractTypeEn;
	}
	public String getContractTypeAr() {
		return contractTypeAr;
	}
	public void setContractTypeAr(String contractTypeAr) {
		this.contractTypeAr = contractTypeAr;
	}
	public String getContractStatusEn() {
		return contractStatusEn;
	}
	public void setContractStatusEn(String contractStatusEn) {
		this.contractStatusEn = contractStatusEn;
	}
	public String getContractStatusAr() {
		return contractStatusAr;
	}
	public void setContractStatusAr(String contractStatusAr) {
		this.contractStatusAr = contractStatusAr;
	}
	public String getTopParentID() {
		return topParentID;
	}
	public void setTopParentID(String topParentID) {
		this.topParentID = topParentID;
	}
	public String getTopParentNumber() {
		return topParentNumber;
	}
	public void setTopParentNumber(String topParentNumber) {
		this.topParentNumber = topParentNumber;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	

}
