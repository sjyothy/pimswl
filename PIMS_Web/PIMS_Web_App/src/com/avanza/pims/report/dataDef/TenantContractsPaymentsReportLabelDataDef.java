package com.avanza.pims.report.dataDef;

public class TenantContractsPaymentsReportLabelDataDef extends AbstractReportDataDef
{
	////////English Labels///////////
	private String lblTenantNumberEn;
	private String lblTenantNameEn;
	private String lblTenantTypeEn;
	private String lblCellNumberEn;
	private String lblContractNumberEn;
	private String lblContractStartDateEn;
	private String lblContractEndDateEn;
	private String lblContractRentValueEn;
	private String lblContractTypeEn;
	private String lblStatusEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblPaymentNumberEn;
	private String lblPaymentTypeEn;
	private String lblPaymentDateEn;
	private String lblPaymentMethodEn;
	private String lblPaymentAmountEn;
	private String lblReceiptNumberEn;
	private String lblReceiptDateEn;
	private String lblReceiptAmountEn;
	private String lblReceiptStatusEn;
	private String lblChequeNumberEn;
	private String lblChequeDateEn;
	private String lblChequeAmountEn;
	private String lblChequeTypeEn;
	private String lblBankNameEn;
	private String lblOwnerEn;
	private String lblTotalEn;
	
	///////Arabic Labels////////////
	private String lblTenantNumberAr;
	private String lblChequeAmountAr;
	private String lblTenantNameAr;
	private String lblTenantTypeAr;
	private String lblCellNumberAr;
	private String lblContractNumberAr;
	private String lblContractStartDateAr;
	private String lblContractEndDateAr;
	private String lblContractRentValueAr;
	private String lblContractTypeAr;
	private String lblStatusAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblPaymentNumberAr;
	private String lblPaymentTypeAr;
	private String lblPaymentDateAr;
	private String lblPaymentMethodAr;
	private String lblPaymentAmountAr;
	private String lblReceiptNumberAr;
	private String lblReceiptDateAr;
	private String lblReceiptAmountAr;
	private String lblReceiptStatusAr;
	private String lblChequeNumberAr;
	private String lblChequeDateAr;
	private String lblChequeTypeAr;
	private String lblBankNameAr;
	private String lblOwnerAr;
	private String lblTotalAr;
	private String lblIndividualEn;
	private String lblIndividualAr;
	private String lblCompanyEn;
	private String lblCompanyAr;
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	//////////////////////
	public String getLblTenantNumberEn() {
		return lblTenantNumberEn;
	}
	public void setLblTenantNumberEn(String lblTenantNumberEn) {
		this.lblTenantNumberEn = lblTenantNumberEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantTypeEn() {
		return lblTenantTypeEn;
	}
	public void setLblTenantTypeEn(String lblTenantTypeEn) {
		this.lblTenantTypeEn = lblTenantTypeEn;
	}
	public String getLblCellNumberEn() {
		return lblCellNumberEn;
	}
	public void setLblCellNumberEn(String lblCellNumberEn) {
		this.lblCellNumberEn = lblCellNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblContractRentValueEn() {
		return lblContractRentValueEn;
	}
	public void setLblContractRentValueEn(String lblContractRentValueEn) {
		this.lblContractRentValueEn = lblContractRentValueEn;
	}
	public String getLblContractTypeEn() {
		return lblContractTypeEn;
	}
	public void setLblContractTypeEn(String lblContractTypeEn) {
		this.lblContractTypeEn = lblContractTypeEn;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblPaymentNumberEn() {
		return lblPaymentNumberEn;
	}
	public void setLblPaymentNumberEn(String lblPaymentNumberEn) {
		this.lblPaymentNumberEn = lblPaymentNumberEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentDateEn() {
		return lblPaymentDateEn;
	}
	public void setLblPaymentDateEn(String lblPaymentDateEn) {
		this.lblPaymentDateEn = lblPaymentDateEn;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblReceiptAmountEn() {
		return lblReceiptAmountEn;
	}
	public void setLblReceiptAmountEn(String lblReceiptAmountEn) {
		this.lblReceiptAmountEn = lblReceiptAmountEn;
	}
	public String getLblReceiptStatusEn() {
		return lblReceiptStatusEn;
	}
	public void setLblReceiptStatusEn(String lblReceiptStatusEn) {
		this.lblReceiptStatusEn = lblReceiptStatusEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblChequeTypeEn() {
		return lblChequeTypeEn;
	}
	public void setLblChequeTypeEn(String lblChequeTypeEn) {
		this.lblChequeTypeEn = lblChequeTypeEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblOwnerEn() {
		return lblOwnerEn;
	}
	public void setLblOwnerEn(String lblOwnerEn) {
		this.lblOwnerEn = lblOwnerEn;
	}
	public String getLblTotalEn() {
		return lblTotalEn;
	}
	public void setLblTotalEn(String lblTotalEn) {
		this.lblTotalEn = lblTotalEn;
	}
	public String getLblTenantNumberAr() {
		return lblTenantNumberAr;
	}
	public void setLblTenantNumberAr(String lblTenantNumberAr) {
		this.lblTenantNumberAr = lblTenantNumberAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblTenantTypeAr() {
		return lblTenantTypeAr;
	}
	public void setLblTenantTypeAr(String lblTenantTypeAr) {
		this.lblTenantTypeAr = lblTenantTypeAr;
	}
	public String getLblCellNumberAr() {
		return lblCellNumberAr;
	}
	public void setLblCellNumberAr(String lblCellNumberAr) {
		this.lblCellNumberAr = lblCellNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	
	public String getLblContractRentValueAr() {
		return lblContractRentValueAr;
	}
	public void setLblContractRentValueAr(String lblContractRentValueAr) {
		this.lblContractRentValueAr = lblContractRentValueAr;
	}
	public String getLblContractTypeAr() {
		return lblContractTypeAr;
	}
	public void setLblContractTypeAr(String lblContractTypeAr) {
		this.lblContractTypeAr = lblContractTypeAr;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblPaymentNumberAr() {
		return lblPaymentNumberAr;
	}
	public void setLblPaymentNumberAr(String lblPaymentNumberAr) {
		this.lblPaymentNumberAr = lblPaymentNumberAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentDateAr() {
		return lblPaymentDateAr;
	}
	public void setLblPaymentDateAr(String lblPaymentDateAr) {
		this.lblPaymentDateAr = lblPaymentDateAr;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblReceiptAmountAr() {
		return lblReceiptAmountAr;
	}
	public void setLblReceiptAmountAr(String lblReceiptAmountAr) {
		this.lblReceiptAmountAr = lblReceiptAmountAr;
	}
	public String getLblReceiptStatusAr() {
		return lblReceiptStatusAr;
	}
	public void setLblReceiptStatusAr(String lblReceiptStatusAr) {
		this.lblReceiptStatusAr = lblReceiptStatusAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblChequeTypeAr() {
		return lblChequeTypeAr;
	}
	public void setLblChequeTypeAr(String lblChequeTypeAr) {
		this.lblChequeTypeAr = lblChequeTypeAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblOwnerAr() {
		return lblOwnerAr;
	}
	public void setLblOwnerAr(String lblOwnerAr) {
		this.lblOwnerAr = lblOwnerAr;
	}
	public String getLblTotalAr() {
		return lblTotalAr;
	}
	public void setLblTotalAr(String lblTotalAr) {
		this.lblTotalAr = lblTotalAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblIndividualEn() {
		return lblIndividualEn;
	}
	public void setLblIndividualEn(String lblIndividualEn) {
		this.lblIndividualEn = lblIndividualEn;
	}
	public String getLblIndividualAr() {
		return lblIndividualAr;
	}
	public void setLblIndividualAr(String lblIndividualAr) {
		this.lblIndividualAr = lblIndividualAr;
	}
	public String getLblCompanyEn() {
		return lblCompanyEn;
	}
	public void setLblCompanyEn(String lblCompanyEn) {
		this.lblCompanyEn = lblCompanyEn;
	}
	public String getLblCompanyAr() {
		return lblCompanyAr;
	}
	public void setLblCompanyAr(String lblCompanyAr) {
		this.lblCompanyAr = lblCompanyAr;
	}
}
	