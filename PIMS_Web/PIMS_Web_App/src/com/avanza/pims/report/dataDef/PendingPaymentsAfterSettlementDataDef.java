package com.avanza.pims.report.dataDef;

public class PendingPaymentsAfterSettlementDataDef extends AbstractReportDataDef {
	
	private String CONTRACT_ID;
	private String CONTRACT_NUMBER;
	private String SETTLEMENT_DATE;
	private String CONTRACT_START_DATE;
	private String CONTRACT_END_DATE;

	private String PAYMENT_TYPE;

	private String PAYMENT_SCHEDULE_ID;
	private String PAYMENT_NUMBER;
	private String AMOUNT;
	private String RENT_EXT_DUR;
	private String PAYMENT_DUE_ON;

	private String REQUEST_ID;
	private String REQUEST_DATE;
	private String REQUEST_NUMBER;

	private String REQUEST_TYPE;

	private String GRP_CUSTOMER_NO;

	private String TENANT_NAME;

	private String OWNERSHIP_TYPE_ID;
	private String OWNERSHIP_TYPE;
	private String PROPERTY_NAME;

	private String UNIT_ACC_NO;
	private String UNIT_DESC;
	
	private String EF1;
	private String EF2;
	private String EF3;
	private String EF4;
	private String EF5;
	private String EF6;
	private String EF7;
	private String EF8;
	private String EF9;
	
	private String EF10;
	private String EF11;
	private String EF12;
	private String EF13;
	private String EF14;
	private String EF15;
	private String EF16;
	private String EF17;
	private String EF18;
	private String EF19;
	private String EF20;
	
	public String getCONTRACT_ID() {
		return CONTRACT_ID;
	}
	public void setCONTRACT_ID(String contract_id) {
		CONTRACT_ID = contract_id;
	}
	public String getCONTRACT_NUMBER() {
		return CONTRACT_NUMBER;
	}
	public void setCONTRACT_NUMBER(String contract_number) {
		CONTRACT_NUMBER = contract_number;
	}
	public String getSETTLEMENT_DATE() {
		return SETTLEMENT_DATE;
	}
	public void setSETTLEMENT_DATE(String settlement_date) {
		SETTLEMENT_DATE = settlement_date;
	}
	public String getCONTRACT_START_DATE() {
		return CONTRACT_START_DATE;
	}
	public void setCONTRACT_START_DATE(String contract_start_date) {
		CONTRACT_START_DATE = contract_start_date;
	}
	public String getCONTRACT_END_DATE() {
		return CONTRACT_END_DATE;
	}
	public void setCONTRACT_END_DATE(String contract_end_date) {
		CONTRACT_END_DATE = contract_end_date;
	}
	public String getPAYMENT_TYPE() {
		return PAYMENT_TYPE;
	}
	public void setPAYMENT_TYPE(String payment_type) {
		PAYMENT_TYPE = payment_type;
	}
	public String getPAYMENT_SCHEDULE_ID() {
		return PAYMENT_SCHEDULE_ID;
	}
	public void setPAYMENT_SCHEDULE_ID(String payment_schedule_id) {
		PAYMENT_SCHEDULE_ID = payment_schedule_id;
	}
	public String getPAYMENT_NUMBER() {
		return PAYMENT_NUMBER;
	}
	public void setPAYMENT_NUMBER(String payment_number) {
		PAYMENT_NUMBER = payment_number;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String amount) {
		AMOUNT = amount;
	}
	public String getRENT_EXT_DUR() {
		return RENT_EXT_DUR;
	}
	public void setRENT_EXT_DUR(String rent_ext_dur) {
		RENT_EXT_DUR = rent_ext_dur;
	}
	public String getPAYMENT_DUE_ON() {
		return PAYMENT_DUE_ON;
	}
	public void setPAYMENT_DUE_ON(String payment_due_on) {
		PAYMENT_DUE_ON = payment_due_on;
	}
	public String getREQUEST_ID() {
		return REQUEST_ID;
	}
	public void setREQUEST_ID(String request_id) {
		REQUEST_ID = request_id;
	}
	public String getREQUEST_DATE() {
		return REQUEST_DATE;
	}
	public void setREQUEST_DATE(String request_date) {
		REQUEST_DATE = request_date;
	}
	public String getREQUEST_NUMBER() {
		return REQUEST_NUMBER;
	}
	public void setREQUEST_NUMBER(String request_number) {
		REQUEST_NUMBER = request_number;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String request_type) {
		REQUEST_TYPE = request_type;
	}
	public String getGRP_CUSTOMER_NO() {
		return GRP_CUSTOMER_NO;
	}
	public void setGRP_CUSTOMER_NO(String grp_customer_no) {
		GRP_CUSTOMER_NO = grp_customer_no;
	}
	public String getTENANT_NAME() {
		return TENANT_NAME;
	}
	public void setTENANT_NAME(String tenant_name) {
		TENANT_NAME = tenant_name;
	}
	public String getOWNERSHIP_TYPE_ID() {
		return OWNERSHIP_TYPE_ID;
	}
	public void setOWNERSHIP_TYPE_ID(String ownership_type_id) {
		OWNERSHIP_TYPE_ID = ownership_type_id;
	}
	public String getOWNERSHIP_TYPE() {
		return OWNERSHIP_TYPE;
	}
	public void setOWNERSHIP_TYPE(String ownership_type) {
		OWNERSHIP_TYPE = ownership_type;
	}
	public String getPROPERTY_NAME() {
		return PROPERTY_NAME;
	}
	public void setPROPERTY_NAME(String property_name) {
		PROPERTY_NAME = property_name;
	}
	public String getUNIT_ACC_NO() {
		return UNIT_ACC_NO;
	}
	public void setUNIT_ACC_NO(String unit_acc_no) {
		UNIT_ACC_NO = unit_acc_no;
	}
	public String getUNIT_DESC() {
		return UNIT_DESC;
	}
	public void setUNIT_DESC(String unit_desc) {
		UNIT_DESC = unit_desc;
	}
	public String getEF1() {
		return EF1;
	}
	public void setEF1(String ef1) {
		EF1 = ef1;
	}
	public String getEF2() {
		return EF2;
	}
	public void setEF2(String ef2) {
		EF2 = ef2;
	}
	public String getEF3() {
		return EF3;
	}
	public void setEF3(String ef3) {
		EF3 = ef3;
	}
	public String getEF4() {
		return EF4;
	}
	public void setEF4(String ef4) {
		EF4 = ef4;
	}
	public String getEF5() {
		return EF5;
	}
	public void setEF5(String ef5) {
		EF5 = ef5;
	}
	public String getEF6() {
		return EF6;
	}
	public void setEF6(String ef6) {
		EF6 = ef6;
	}
	public String getEF7() {
		return EF7;
	}
	public void setEF7(String ef7) {
		EF7 = ef7;
	}
	public String getEF8() {
		return EF8;
	}
	public void setEF8(String ef8) {
		EF8 = ef8;
	}
	public String getEF9() {
		return EF9;
	}
	public void setEF9(String ef9) {
		EF9 = ef9;
	}
	public String getEF10() {
		return EF10;
	}
	public void setEF10(String ef10) {
		EF10 = ef10;
	}
	public String getEF11() {
		return EF11;
	}
	public void setEF11(String ef11) {
		EF11 = ef11;
	}
	public String getEF12() {
		return EF12;
	}
	public void setEF12(String ef12) {
		EF12 = ef12;
	}
	public String getEF13() {
		return EF13;
	}
	public void setEF13(String ef13) {
		EF13 = ef13;
	}
	public String getEF14() {
		return EF14;
	}
	public void setEF14(String ef14) {
		EF14 = ef14;
	}
	public String getEF15() {
		return EF15;
	}
	public void setEF15(String ef15) {
		EF15 = ef15;
	}
	public String getEF16() {
		return EF16;
	}
	public void setEF16(String ef16) {
		EF16 = ef16;
	}
	public String getEF17() {
		return EF17;
	}
	public void setEF17(String ef17) {
		EF17 = ef17;
	}
	public String getEF18() {
		return EF18;
	}
	public void setEF18(String ef18) {
		EF18 = ef18;
	}
	public String getEF19() {
		return EF19;
	}
	public void setEF19(String ef19) {
		EF19 = ef19;
	}
	public String getEF20() {
		return EF20;
	}
	public void setEF20(String ef20) {
		EF20 = ef20;
	}

}
