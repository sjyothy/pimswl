package com.avanza.pims.report.dataDef;

public class PaymentSummaryReportDataDef extends AbstractReportDataDef{
	
	private String propertyNumber;
	private String propertyName;
	private String propertyOwnershipTypeEn;
	private String propertyOwnershipTypeAr;
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentAmount;  
	private String receiptDate;
	private String paymentDueDate;
	private String paymentStatusEn;
	private String paymentStatusAr;
	private String receiptNo;
	

	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPropertyOwnershipTypeEn() {
		return propertyOwnershipTypeEn;
	}
	public void setPropertyOwnershipTypeEn(String propertyOwnershipTypeEn) {
		this.propertyOwnershipTypeEn = propertyOwnershipTypeEn;
	}
	public String getPropertyOwnershipTypeAr() {
		return propertyOwnershipTypeAr;
	}
	public void setPropertyOwnershipTypeAr(String propertyOwnershipTypeAr) {
		this.propertyOwnershipTypeAr = propertyOwnershipTypeAr;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getPaymentStatusEn() {
		return paymentStatusEn;
	}
	public void setPaymentStatusEn(String paymentStatusEn) {
		this.paymentStatusEn = paymentStatusEn;
	}
	public String getPaymentStatusAr() {
		return paymentStatusAr;
	}
	public void setPaymentStatusAr(String paymentStatusAr) {
		this.paymentStatusAr = paymentStatusAr;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	

	


}
