package com.avanza.pims.report.dataDef;


public class ReqTaskInhFileDataDef extends AbstractReportDataDef 
{

	private Long requestId;
	private Long requestTypeId;
	private Long statusId;
	private String typeEn;
	private String typeAr;
	private String requestNumber;
	private String statusEn;
	private String statusAr;
	private String requestCreatedOn;
	private String approvalRequired_C;
	private String researcherAssigned_C;
	private String approved_C;
	private String rejected_C;
	private String distributionCompleted_C;
	private String Completed_C;
	private String finalLimitationDone_C;
	private String initialLimitationDone_C;
	private Double timeLapseAppReq;
	private Double timeLapseApprove;
	private Double timeLapseCompleted;
	private Double timeLapseDisComp;
	private Double timeLapseInitLimitReject;
	private Double timeLapseResAssigned;
	private Double timeLapseFinLimitnDone;
	
	private Double timeLapseInitLimitDone;
	private String field1_C;
	private String field2_C;
	private String field3_C;
	private String field4_C;
	private String field5_C;
	private String field6_C;
	private String field7_C;
	private String field8_C;
	private String field9_C;
	private Long field10_C;
	private Long field11_C;
	private Long field12_C;
	private Long field13_C;
	private Long field14_C;
	
	private Double field15;
	private Double field16;
	private Double field17;
	private Double field18;
	private Double field19;
	private Double field20;
	private Double field21;
	public Long getRequestId() {
		return requestId;
	}
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
	public Long getRequestTypeId() {
		return requestTypeId;
	}
	public void setRequestTypeId(Long requestTypeId) {
		this.requestTypeId = requestTypeId;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getTypeEn() {
		return typeEn;
	}
	public void setTypeEn(String typeEn) {
		this.typeEn = typeEn;
	}
	public String getTypeAr() {
		return typeAr;
	}
	public void setTypeAr(String typeAr) {
		this.typeAr = typeAr;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getRequestCreatedOn() {
		return requestCreatedOn;
	}
	public void setRequestCreatedOn(String requestCreatedOn) {
		this.requestCreatedOn = requestCreatedOn;
	}
	public String getApprovalRequired_C() {
		return approvalRequired_C;
	}
	public void setApprovalRequired_C(String approvalRequired_C) {
		this.approvalRequired_C = approvalRequired_C;
	}
	public String getResearcherAssigned_C() {
		return researcherAssigned_C;
	}
	public void setResearcherAssigned_C(String researcherAssigned_C) {
		this.researcherAssigned_C = researcherAssigned_C;
	}
	public String getApproved_C() {
		return approved_C;
	}
	public void setApproved_C(String approved_C) {
		this.approved_C = approved_C;
	}
	public String getRejected_C() {
		return rejected_C;
	}
	public void setRejected_C(String rejected_C) {
		this.rejected_C = rejected_C;
	}
	public String getDistributionCompleted_C() {
		return distributionCompleted_C;
	}
	public void setDistributionCompleted_C(String distributionCompleted_C) {
		this.distributionCompleted_C = distributionCompleted_C;
	}
	public String getCompleted_C() {
		return Completed_C;
	}
	public void setCompleted_C(String completed_C) {
		Completed_C = completed_C;
	}
	public Double getTimeLapseAppReq() {
		return timeLapseAppReq;
	}
	public void setTimeLapseAppReq(Double timeLapseAppReq) {
		this.timeLapseAppReq = timeLapseAppReq;
	}
	public Double getTimeLapseApprove() {
		return timeLapseApprove;
	}
	public void setTimeLapseApprove(Double timeLapseApprove) {
		this.timeLapseApprove = timeLapseApprove;
	}
	public Double getTimeLapseCompleted() {
		return timeLapseCompleted;
	}
	public void setTimeLapseCompleted(Double timeLapseCompleted) {
		this.timeLapseCompleted = timeLapseCompleted;
	}
	
	public Double getTimeLapseInitLimitReject() {
		return timeLapseInitLimitReject;
	}
	public void setTimeLapseInitLimitReject(Double timeLapseInitLimitReject) {
		this.timeLapseInitLimitReject = timeLapseInitLimitReject;
	}
	
	public String getField1_C() {
		return field1_C;
	}
	public void setField1_C(String field1_C) {
		this.field1_C = field1_C;
	}
	public String getField2_C() {
		return field2_C;
	}
	public void setField2_C(String field2_C) {
		this.field2_C = field2_C;
	}
	public String getField3_C() {
		return field3_C;
	}
	public void setField3_C(String field3_C) {
		this.field3_C = field3_C;
	}
	public String getField4_C() {
		return field4_C;
	}
	public void setField4_C(String field4_C) {
		this.field4_C = field4_C;
	}
	public String getField5_C() {
		return field5_C;
	}
	public void setField5_C(String field5_C) {
		this.field5_C = field5_C;
	}
	public String getField6_C() {
		return field6_C;
	}
	public void setField6_C(String field6_C) {
		this.field6_C = field6_C;
	}
	public String getField7_C() {
		return field7_C;
	}
	public void setField7_C(String field7_C) {
		this.field7_C = field7_C;
	}
	public String getField8_C() {
		return field8_C;
	}
	public void setField8_C(String field8_C) {
		this.field8_C = field8_C;
	}
	public String getField9_C() {
		return field9_C;
	}
	public void setField9_C(String field9_C) {
		this.field9_C = field9_C;
	}
	public Long getField10_C() {
		return field10_C;
	}
	public void setField10_C(Long field10_C) {
		this.field10_C = field10_C;
	}
	public Long getField11_C() {
		return field11_C;
	}
	public void setField11_C(Long field11_C) {
		this.field11_C = field11_C;
	}
	public Long getField12_C() {
		return field12_C;
	}
	public void setField12_C(Long field12_C) {
		this.field12_C = field12_C;
	}
	public Long getField13_C() {
		return field13_C;
	}
	public void setField13_C(Long field13_C) {
		this.field13_C = field13_C;
	}
	public Long getField14_C() {
		return field14_C;
	}
	public void setField14_C(Long field14_C) {
		this.field14_C = field14_C;
	}
	public Double getField15() {
		return field15;
	}
	public void setField15(Double field15) {
		this.field15 = field15;
	}
	public Double getField16() {
		return field16;
	}
	public void setField16(Double field16) {
		this.field16 = field16;
	}
	public Double getField17() {
		return field17;
	}
	public void setField17(Double field17) {
		this.field17 = field17;
	}
	public Double getField18() {
		return field18;
	}
	public void setField18(Double field18) {
		this.field18 = field18;
	}
	public Double getField19() {
		return field19;
	}
	public void setField19(Double field19) {
		this.field19 = field19;
	}
	public Double getField20() {
		return field20;
	}
	public void setField20(Double field20) {
		this.field20 = field20;
	}
	public Double getField21() {
		return field21;
	}
	public void setField21(Double field21) {
		this.field21 = field21;
	}
	public String getFinalLimitationDone_C() {
		return finalLimitationDone_C;
	}
	public void setFinalLimitationDone_C(String finalLimitationDone_C) {
		this.finalLimitationDone_C = finalLimitationDone_C;
	}
	
	public String getInitialLimitationDone_C() {
		return initialLimitationDone_C;
	}
	public void setInitialLimitationDone_C(String initialLimitationDone_C) {
		this.initialLimitationDone_C = initialLimitationDone_C;
	}
	public Double getTimeLapseDisComp() {
		return timeLapseDisComp;
	}
	public void setTimeLapseDisComp(Double timeLapseDisComp) {
		this.timeLapseDisComp = timeLapseDisComp;
	}
	public Double getTimeLapseResAssigned() {
		return timeLapseResAssigned;
	}
	public void setTimeLapseResAssigned(Double timeLapseResAssigned) {
		this.timeLapseResAssigned = timeLapseResAssigned;
	}
	public Double getTimeLapseInitLimitDone() {
		return timeLapseInitLimitDone;
	}
	public void setTimeLapseInitLimitDone(Double timeLapseInitLimitDone) {
		this.timeLapseInitLimitDone = timeLapseInitLimitDone;
	}
	public Double getTimeLapseFinLimitnDone() {
		return timeLapseFinLimitnDone;
	}
	public void setTimeLapseFinLimitnDone(Double timeLapseFinLimitnDone) {
		this.timeLapseFinLimitnDone = timeLapseFinLimitnDone;
	}

}
