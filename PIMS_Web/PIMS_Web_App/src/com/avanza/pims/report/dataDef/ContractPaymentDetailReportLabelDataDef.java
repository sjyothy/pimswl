package com.avanza.pims.report.dataDef;


public class ContractPaymentDetailReportLabelDataDef extends AbstractReportDataDef {

    /*******************************                           English Labels             *********************************/

	private String lblContractNumberEn;
	private String lblcontractIssueDateEn;
	private String lblcontractEndDateEn;
	private String lblPropertyComercialNameEn;
	private String lblUnitNumEn;
	private String lblTenantNameEn;
	private String lblPaymentNoEn;
	private String lblPaymentDueDateEn;
	private String lblPaymentMethodEn;
	private String lblPaymentTypeEn;
	private String lblChequeNumEn;
	private String lblBankNameEn;
	private String lblPaymentAmountEn;
	private String lblPaymentStatusEn;
	private String lblRelatedToEn;
	private String lblRelatedToAr;
	private String lblContractEn;
	/*******************************                           Arabic Labels             *********************************/
	
	private String lblContractNumberAr;
	private String lblContractIssueDateAr;
	private String lblContractEndDateAr;
	private String lblPropertyComercialNameAr;
	private String lblUnitNumAr;
	private String lblTenantNameAr;
	private String lblPaymentNoAr;
	private String lblPaymentDueDateAr;
	private String lblPaymentMethodAr;
	private String lblPaymentTypeAr;
	private String lblChequeNumAr;
	private String lblBankNameAr;
	private String lblPaymentAmountAr;
	private String lblPaymentStatusAr;
	private String lblContractAr;
	
    /*******************************                           Report Summary Labels             *********************************/
	private String lblTotalPaymentAmountEn;
	private String lblTotalPaymentAmountAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;

	
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblcontractIssueDateEn() {
		return lblcontractIssueDateEn;
	}
	public void setLblcontractIssueDateEn(String lblcontractIssueDateEn) {
		this.lblcontractIssueDateEn = lblcontractIssueDateEn;
	}
	public String getLblcontractEndDateEn() {
		return lblcontractEndDateEn;
	}
	public void setLblcontractEndDateEn(String lblcontractEndDateEn) {
		this.lblcontractEndDateEn = lblcontractEndDateEn;
	}
	public String getLblPropertyComercialNameEn() {
		return lblPropertyComercialNameEn;
	}
	public void setLblPropertyComercialNameEn(String lblPropertyComercialNameEn) {
		this.lblPropertyComercialNameEn = lblPropertyComercialNameEn;
	}
	public String getLblUnitNumEn() {
		return lblUnitNumEn;
	}
	public void setLblUnitNumEn(String lblUnitNumEn) {
		this.lblUnitNumEn = lblUnitNumEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblPaymentNoEn() {
		return lblPaymentNoEn;
	}
	public void setLblPaymentNoEn(String lblPaymentNoEn) {
		this.lblPaymentNoEn = lblPaymentNoEn;
	}
	public String getLblPaymentDueDateEn() {
		return lblPaymentDueDateEn;
	}
	public void setLblPaymentDueDateEn(String lblPaymentDueDateEn) {
		this.lblPaymentDueDateEn = lblPaymentDueDateEn;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblChequeNumEn() {
		return lblChequeNumEn;
	}
	public void setLblChequeNumEn(String lblChequeNumEn) {
		this.lblChequeNumEn = lblChequeNumEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblPaymentStatusEn() {
		return lblPaymentStatusEn;
	}
	public void setLblPaymentStatusEn(String lblPaymentStatusEn) {
		this.lblPaymentStatusEn = lblPaymentStatusEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractIssueDateAr() {
		return lblContractIssueDateAr;
	}
	public void setLblContractIssueDateAr(String lblContractIssueDateAr) {
		this.lblContractIssueDateAr = lblContractIssueDateAr;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblPropertyComercialNameAr() {
		return lblPropertyComercialNameAr;
	}
	public void setLblPropertyComercialNameAr(String lblPropertyComercialNameAr) {
		this.lblPropertyComercialNameAr = lblPropertyComercialNameAr;
	}
	public String getLblUnitNumAr() {
		return lblUnitNumAr;
	}
	public void setLblUnitNumAr(String lblUnitNumAr) {
		this.lblUnitNumAr = lblUnitNumAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPaymentNoAr() {
		return lblPaymentNoAr;
	}
	public void setLblPaymentNoAr(String lblPaymentNoAr) {
		this.lblPaymentNoAr = lblPaymentNoAr;
	}
	public String getLblPaymentDueDateAr() {
		return lblPaymentDueDateAr;
	}
	public void setLblPaymentDueDateAr(String lblPaymentDueDateAr) {
		this.lblPaymentDueDateAr = lblPaymentDueDateAr;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblChequeNumAr() {
		return lblChequeNumAr;
	}
	public void setLblChequeNumAr(String lblChequeNumAr) {
		this.lblChequeNumAr = lblChequeNumAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblPaymentStatusAr() {
		return lblPaymentStatusAr;
	}
	public void setLblPaymentStatusAr(String lblPaymentStatusAr) {
		this.lblPaymentStatusAr = lblPaymentStatusAr;
	}
	
	public String getLblTotalPaymentAmountEn() {
		return lblTotalPaymentAmountEn;
	}
	public void setLblTotalPaymentAmountEn(String lblTotalPaymentAmountEn) {
		this.lblTotalPaymentAmountEn = lblTotalPaymentAmountEn;
	}
	public String getLblTotalPaymentAmountAr() {
		return lblTotalPaymentAmountAr;
	}
	public void setLblTotalPaymentAmountAr(String lblTotalPaymentAmountAr) {
		this.lblTotalPaymentAmountAr = lblTotalPaymentAmountAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblRelatedToEn() {
		return lblRelatedToEn;
	}
	public void setLblRelatedToEn(String lblRelatedToEn) {
		this.lblRelatedToEn = lblRelatedToEn;
	}
	public String getLblRelatedToAr() {
		return lblRelatedToAr;
	}
	public void setLblRelatedToAr(String lblRelatedToAr) {
		this.lblRelatedToAr = lblRelatedToAr;
	}
	public String getLblContractEn() {
		return lblContractEn;
	}
	public void setLblContractEn(String lblContractEn) {
		this.lblContractEn = lblContractEn;
	}
	public String getLblContractAr() {
		return lblContractAr;
	}
	public void setLblContractAr(String lblContractAr) {
		this.lblContractAr = lblContractAr;
	}
	
	
	
	

}
