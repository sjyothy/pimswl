package com.avanza.pims.report.dataDef;

public class FileDetailsReportShareDataDef extends AbstractReportDataDef
{
		private Double sharePercentage;
		private Long isWill;
		private String assetNumber;
		private Long fileId;
		private String fileNumber;
		private String beneficiaryName;
		private String filed1;
		private String filed2;
		private String filed3;
		private String filed4;
		private String filed5;
		private String filed6;
		private String filed7;
		private String filed8;
		private String filed9;
		private String filed10;
		public Double getSharePercentage() {
			return sharePercentage;
		}
		public void setSharePercentage(Double sharePercentage) {
			this.sharePercentage = sharePercentage;
		}
		public Long getIsWill() {
			return isWill;
		}
		public void setIsWill(Long isWill) {
			this.isWill = isWill;
		}
		public String getAssetNumber() {
			return assetNumber;
		}
		public void setAssetNumber(String assetNumber) {
			this.assetNumber = assetNumber;
		}
		public Long getFileId() {
			return fileId;
		}
		public void setFileId(Long fileId) {
			this.fileId = fileId;
		}
		public String getFileNumber() {
			return fileNumber;
		}
		public void setFileNumber(String fileNumber) {
			this.fileNumber = fileNumber;
		}
		public String getBeneficiaryName() {
			return beneficiaryName;
		}
		public void setBeneficiaryName(String beneficiaryName) {
			this.beneficiaryName = beneficiaryName;
		}
		public String getFiled1() {
			return filed1;
		}
		public void setFiled1(String filed1) {
			this.filed1 = filed1;
		}
		public String getFiled2() {
			return filed2;
		}
		public void setFiled2(String filed2) {
			this.filed2 = filed2;
		}
		public String getFiled3() {
			return filed3;
		}
		public void setFiled3(String filed3) {
			this.filed3 = filed3;
		}
		public String getFiled4() {
			return filed4;
		}
		public void setFiled4(String filed4) {
			this.filed4 = filed4;
		}
		public String getFiled5() {
			return filed5;
		}
		public void setFiled5(String filed5) {
			this.filed5 = filed5;
		}
		public String getFiled6() {
			return filed6;
		}
		public void setFiled6(String filed6) {
			this.filed6 = filed6;
		}
		public String getFiled7() {
			return filed7;
		}
		public void setFiled7(String filed7) {
			this.filed7 = filed7;
		}
		public String getFiled8() {
			return filed8;
		}
		public void setFiled8(String filed8) {
			this.filed8 = filed8;
		}
		public String getFiled9() {
			return filed9;
		}
		public void setFiled9(String filed9) {
			this.filed9 = filed9;
		}
		public String getFiled10() {
			return filed10;
		}
		public void setFiled10(String filed10) {
			this.filed10 = filed10;
		}
}
	