package com.avanza.pims.report.dataDef;

public class SettlementReportCalculationsDataDef extends AbstractReportDataDef {
	
	private String contractStartDate;
	private String contractEndDate;
	
	private String contractRentalAmount;
	private String evacuationDate;
	private String rentPeriod;
	private String rentAmount;
	private String months;
	private String days;
	private String depositAmount;
	private String realizedAmount;
	private String paidAmount;
	private String unRealizedAmount;
	private String evacuationFines;
	private String unpaidAmount;
	private String unpaidFines;
	private String totalRefundAmount;
	private String totalDueAmount;
	
	private String contractNumber;
	private String tenantName;
	private String costCenter;
	private String grpCustomerNumber;
	private String creditMemo;
	
	public String getContractRentalAmount() {
		return contractRentalAmount;
	}
	public void setContractRentalAmount(String contractRentalAmount) {
		this.contractRentalAmount = contractRentalAmount;
	}
	public String getEvacuationDate() {
		return evacuationDate;
	}
	public void setEvacuationDate(String evacuationDate) {
		this.evacuationDate = evacuationDate;
	}
	public String getRentPeriod() {
		return rentPeriod;
	}
	public void setRentPeriod(String rentPeriod) {
		this.rentPeriod = rentPeriod;
	}
	public String getRentAmount() {
		return rentAmount;
	}
	public void setRentAmount(String rentAmount) {
		this.rentAmount = rentAmount;
	}
	public String getMonths() {
		return months;
	}
	public void setMonths(String months) {
		this.months = months;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(String realizedAmount) {
		this.realizedAmount = realizedAmount;
	}
	public String getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getUnRealizedAmount() {
		return unRealizedAmount;
	}
	public void setUnRealizedAmount(String unRealizedAmount) {
		this.unRealizedAmount = unRealizedAmount;
	}
	public String getEvacuationFines() {
		return evacuationFines;
	}
	public void setEvacuationFines(String evacuationFines) {
		this.evacuationFines = evacuationFines;
	}
	public String getUnpaidAmount() {
		return unpaidAmount;
	}
	public void setUnpaidAmount(String unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}
	public String getUnpaidFines() {
		return unpaidFines;
	}
	public void setUnpaidFines(String unpaidFines) {
		this.unpaidFines = unpaidFines;
	}
	public String getTotalRefundAmount() {
		return totalRefundAmount;
	}
	public void setTotalRefundAmount(String totalRefundAmount) {
		this.totalRefundAmount = totalRefundAmount;
	}
	public String getTotalDueAmount() {
		return totalDueAmount;
	}
	public void setTotalDueAmount(String totalDueAmount) {
		this.totalDueAmount = totalDueAmount;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getGrpCustomerNumber() {
		return grpCustomerNumber;
	}
	public void setGrpCustomerNumber(String grpCustomerNumber) {
		this.grpCustomerNumber = grpCustomerNumber;
	}
	public String getCreditMemo() {
		return creditMemo;
	}
	public void setCreditMemo(String creditMemo) {
		this.creditMemo = creditMemo;
	}
	
}
