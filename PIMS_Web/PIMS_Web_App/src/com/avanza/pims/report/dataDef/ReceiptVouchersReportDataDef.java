package com.avanza.pims.report.dataDef;

public class ReceiptVouchersReportDataDef extends AbstractReportDataDef 
{
	private String tenantName;
	private String tenantId;
	private String tenantTypeEn;
	private String tenantTypeAr;
	private String unitNumber;
	private String contractNumber;
	private String contractStartDate;
	private String expiryDate;
	private String propertyName;
	private String propertyOwnershipTypeEn;
	private String propertyOwnershipTypeAr;
	
	private String paymentNumber;
	
	private String paymentTypeEn;
	private String paymentTypeAr;
	
	private String PaymentDueDate;
	
	private String paymentMethodEn;
	private String paymentMethodAr;
	
	private String receiptNumber;
	private String receiptDate;
	private String chequeNumber;
	private String chequeAmount;
	
	private String chequeTypeEn;
	private String chequeTypeAr;
	
	private String bankNameEn;
	private String bankNameAr;
	
	private String chequeOwnerName;
	private String transactionNumber;
	
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPaymentDueDate() {
		return PaymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		PaymentDueDate = paymentDueDate;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public String getChequeOwnerName() {
		return chequeOwnerName;
	}
	public void setChequeOwnerName(String chequeOwnerName) {
		this.chequeOwnerName = chequeOwnerName;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentMethodEn() {
		return paymentMethodEn;
	}
	public void setPaymentMethodEn(String paymentMethodEn) {
		this.paymentMethodEn = paymentMethodEn;
	}
	public String getPaymentMethodAr() {
		return paymentMethodAr;
	}
	public void setPaymentMethodAr(String paymentMethodAr) {
		this.paymentMethodAr = paymentMethodAr;
	}
	public String getBankNameEn() {
		return bankNameEn;
	}
	public void setBankNameEn(String bankNameEn) {
		this.bankNameEn = bankNameEn;
	}
	public String getBankNameAr() {
		return bankNameAr;
	}
	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}
	public String getPropertyOwnershipTypeEn() {
		return propertyOwnershipTypeEn;
	}
	public void setPropertyOwnershipTypeEn(String propertyOwnershipTypeEn) {
		this.propertyOwnershipTypeEn = propertyOwnershipTypeEn;
	}
	public String getPropertyOwnershipTypeAr() {
		return propertyOwnershipTypeAr;
	}
	public void setPropertyOwnershipTypeAr(String propertyOwnershipTypeAr) {
		this.propertyOwnershipTypeAr = propertyOwnershipTypeAr;
	}
	public String getTenantTypeEn() {
		return tenantTypeEn;
	}
	public void setTenantTypeEn(String tenantTypeEn) {
		this.tenantTypeEn = tenantTypeEn;
	}
	public String getTenantTypeAr() {
		return tenantTypeAr;
	}
	public void setTenantTypeAr(String tenantTypeAr) {
		this.tenantTypeAr = tenantTypeAr;
	}
	public String getChequeTypeEn() {
		return chequeTypeEn;
	}
	public void setChequeTypeEn(String chequeTypeEn) {
		this.chequeTypeEn = chequeTypeEn;
	}
	public String getChequeTypeAr() {
		return chequeTypeAr;
	}
	public void setChequeTypeAr(String chequeTypeAr) {
		this.chequeTypeAr = chequeTypeAr;
	}
}
