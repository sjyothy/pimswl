package com.avanza.pims.report.dataDef;

public class PropertyOccupancyDetailsDataDef extends AbstractReportDataDef
{
	private String unitId;
	private String statusId;
	private String rentValue;
	private String ownershipTypeId;
	private String usageTypeId;
	private String usageTypeEn;
	private String usageTypeAr;
	private String ownershipTypeEn;
	private String ownershipTypeAr;
	private String unitStatusEn;
	private String unitStatusAr;
	private String propertyId;
	private String propertyName;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getRentValue() {
		return rentValue;
	}
	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}
	public String getOwnershipTypeId() {
		return ownershipTypeId;
	}
	public void setOwnershipTypeId(String ownershipTypeId) {
		this.ownershipTypeId = ownershipTypeId;
	}
	public String getUsageTypeId() {
		return usageTypeId;
	}
	public void setUsageTypeId(String usageTypeId) {
		this.usageTypeId = usageTypeId;
	}
	public String getUsageTypeEn() {
		return usageTypeEn;
	}
	public void setUsageTypeEn(String usageTypeEn) {
		this.usageTypeEn = usageTypeEn;
	}
	public String getUsageTypeAr() {
		return usageTypeAr;
	}
	public void setUsageTypeAr(String usageTypeAr) {
		this.usageTypeAr = usageTypeAr;
	}
	public String getOwnershipTypeEn() {
		return ownershipTypeEn;
	}
	public void setOwnershipTypeEn(String ownershipTypeEn) {
		this.ownershipTypeEn = ownershipTypeEn;
	}
	public String getOwnershipTypeAr() {
		return ownershipTypeAr;
	}
	public void setOwnershipTypeAr(String ownershipTypeAr) {
		this.ownershipTypeAr = ownershipTypeAr;
	}
	public String getUnitStatusEn() {
		return unitStatusEn;
	}
	public void setUnitStatusEn(String unitStatusEn) {
		this.unitStatusEn = unitStatusEn;
	}
	public String getUnitStatusAr() {
		return unitStatusAr;
	}
	public void setUnitStatusAr(String unitStatusAr) {
		this.unitStatusAr = unitStatusAr;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
}
	