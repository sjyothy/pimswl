package com.avanza.pims.report.dataDef;

public class BouncedChecksDetailsReportDataDef extends AbstractReportDataDef{
	private String bankNameEn;
	private String bankNameAr;
	private String chequeNumber;
	private String chequeDate;
	private String amount;
	private String contractNumber;
	private String tenantName;
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentDate;
	private String paymentScheduleStatus;	
	private String paymentReceiptDetailStatus;
	private String receiptNumber;
	private String collectedBy;
	public String getBankNameEn() {
		return bankNameEn;
	}
	public void setBankNameEn(String bankNameEn) {
		this.bankNameEn = bankNameEn;
	}
	public String getBankNameAr() {
		return bankNameAr;
	}
	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentScheduleStatus() {
		return paymentScheduleStatus;
	}
	public void setPaymentScheduleStatus(String paymentScheduleStatus) {
		this.paymentScheduleStatus = paymentScheduleStatus;
	}
	public String getPaymentReceiptDetailStatus() {
		return paymentReceiptDetailStatus;
	}
	public void setPaymentReceiptDetailStatus(String paymentReceiptDetailStatus) {
		this.paymentReceiptDetailStatus = paymentReceiptDetailStatus;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getCollectedBy() {
		return collectedBy;
	}
	public void setCollectedBy(String collectedBy) {
		this.collectedBy = collectedBy;
	}
	
	

}
