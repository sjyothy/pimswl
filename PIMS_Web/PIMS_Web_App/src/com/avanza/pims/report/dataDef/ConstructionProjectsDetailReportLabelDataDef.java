package com.avanza.pims.report.dataDef;

public class ConstructionProjectsDetailReportLabelDataDef extends AbstractReportDataDef{
    /*******************************                           English Labels             *********************************/
	private String lblProjectNumberEn;
	private String lblProjectStatusEn;
	private String lblProjectNameEn;
	private String lblProjectTypeEn;
	private String lblCompletionPercentageEn;
	private String lblAchievedMilestoneEn;

	private String lblFollowupDateEn;
	private String lblFollowupStatusEn;
	private String lblFollowupTypeEn;
	private String lblRemarksEn;
	
	
    /*******************************                           Arabic Labels             *********************************/

	private String lblProjectNumberAr;
	private String lblProjectStatusAr;
	private String lblProjectNameAr;
	private String lblProjectTypeAr;
	private String lblCompletionPercentageAr;
	private String lblAchievedMilestoneAr;

	private String lblFollowupDateAr;
	private String lblFollowupStatusAr;
	private String lblFollowupTypeAr;
	private String lblRemarksAr;

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblProjectNumberEn() {
		return lblProjectNumberEn;
	}
	public void setLblProjectNumberEn(String lblProjectNumberEn) {
		this.lblProjectNumberEn = lblProjectNumberEn;
	}
	public String getLblProjectStatusEn() {
		return lblProjectStatusEn;
	}
	public void setLblProjectStatusEn(String lblProjectStatusEn) {
		this.lblProjectStatusEn = lblProjectStatusEn;
	}
	public String getLblProjectNameEn() {
		return lblProjectNameEn;
	}
	public void setLblProjectNameEn(String lblProjectNameEn) {
		this.lblProjectNameEn = lblProjectNameEn;
	}
	public String getLblProjectTypeEn() {
		return lblProjectTypeEn;
	}
	public void setLblProjectTypeEn(String lblProjectTypeEn) {
		this.lblProjectTypeEn = lblProjectTypeEn;
	}
	public String getLblCompletionPercentageEn() {
		return lblCompletionPercentageEn;
	}
	public void setLblCompletionPercentageEn(String lblCompletionPercentageEn) {
		this.lblCompletionPercentageEn = lblCompletionPercentageEn;
	}
	public String getLblAchievedMilestoneEn() {
		return lblAchievedMilestoneEn;
	}
	public void setLblAchievedMilestoneEn(String lblAchievedMilestoneEn) {
		this.lblAchievedMilestoneEn = lblAchievedMilestoneEn;
	}
	public String getLblFollowupDateEn() {
		return lblFollowupDateEn;
	}
	public void setLblFollowupDateEn(String lblFollowupDateEn) {
		this.lblFollowupDateEn = lblFollowupDateEn;
	}
	public String getLblFollowupStatusEn() {
		return lblFollowupStatusEn;
	}
	public void setLblFollowupStatusEn(String lblFollowupStatusEn) {
		this.lblFollowupStatusEn = lblFollowupStatusEn;
	}
	public String getLblFollowupTypeEn() {
		return lblFollowupTypeEn;
	}
	public void setLblFollowupTypeEn(String lblFollowupTypeEn) {
		this.lblFollowupTypeEn = lblFollowupTypeEn;
	}
	public String getLblRemarksEn() {
		return lblRemarksEn;
	}
	public void setLblRemarksEn(String lblRemarksEn) {
		this.lblRemarksEn = lblRemarksEn;
	}
	public String getLblProjectNumberAr() {
		return lblProjectNumberAr;
	}
	public void setLblProjectNumberAr(String lblProjectNumberAr) {
		this.lblProjectNumberAr = lblProjectNumberAr;
	}
	public String getLblProjectStatusAr() {
		return lblProjectStatusAr;
	}
	public void setLblProjectStatusAr(String lblProjectStatusAr) {
		this.lblProjectStatusAr = lblProjectStatusAr;
	}
	public String getLblProjectNameAr() {
		return lblProjectNameAr;
	}
	public void setLblProjectNameAr(String lblProjectNameAr) {
		this.lblProjectNameAr = lblProjectNameAr;
	}
	public String getLblProjectTypeAr() {
		return lblProjectTypeAr;
	}
	public void setLblProjectTypeAr(String lblProjectTypeAr) {
		this.lblProjectTypeAr = lblProjectTypeAr;
	}
	public String getLblCompletionPercentageAr() {
		return lblCompletionPercentageAr;
	}
	public void setLblCompletionPercentageAr(String lblCompletionPercentageAr) {
		this.lblCompletionPercentageAr = lblCompletionPercentageAr;
	}
	public String getLblAchievedMilestoneAr() {
		return lblAchievedMilestoneAr;
	}
	public void setLblAchievedMilestoneAr(String lblAchievedMilestoneAr) {
		this.lblAchievedMilestoneAr = lblAchievedMilestoneAr;
	}
	public String getLblFollowupDateAr() {
		return lblFollowupDateAr;
	}
	public void setLblFollowupDateAr(String lblFollowupDateAr) {
		this.lblFollowupDateAr = lblFollowupDateAr;
	}
	public String getLblFollowupStatusAr() {
		return lblFollowupStatusAr;
	}
	public void setLblFollowupStatusAr(String lblFollowupStatusAr) {
		this.lblFollowupStatusAr = lblFollowupStatusAr;
	}
	public String getLblFollowupTypeAr() {
		return lblFollowupTypeAr;
	}
	public void setLblFollowupTypeAr(String lblFollowupTypeAr) {
		this.lblFollowupTypeAr = lblFollowupTypeAr;
	}
	public String getLblRemarksAr() {
		return lblRemarksAr;
	}
	public void setLblRemarksAr(String lblRemarksAr) {
		this.lblRemarksAr = lblRemarksAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
	
	
}
