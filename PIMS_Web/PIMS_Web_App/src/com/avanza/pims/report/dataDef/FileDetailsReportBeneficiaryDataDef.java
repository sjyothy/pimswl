package com.avanza.pims.report.dataDef;

public class FileDetailsReportBeneficiaryDataDef extends AbstractReportDataDef
{
	 private String fileNumber;
	 private Long inhBeneficiaryId;
	 private Long fileId;
	 private Long beneficiaryPersonId;
	 private String relationDesc;
	 private String beneficiaryName;
	 private String nurseName;
	 private String guardianName;
	 private String beneficiaryRelationEn;
	 private String beneficiaryRelationAr;
	 private String beneficiaryAccNumber;
	 private String disListTypeEn;
	 private String disListTypeAr;
	 private Long isStudent;
	 private Long isMinor;
	 private String jobDesc;
	 private String filed1;
	 private String filed2;
	 private String filed3;
	 private String filed4;
	 private String filed5;
	 private String filed6;
	 private String filed7;
	 private String filed8;
	 private String filed9;
	 private String filed10;
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Long getInhBeneficiaryId() {
		return inhBeneficiaryId;
	}
	public void setInhBeneficiaryId(Long inhBeneficiaryId) {
		this.inhBeneficiaryId = inhBeneficiaryId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public Long getBeneficiaryPersonId() {
		return beneficiaryPersonId;
	}
	public void setBeneficiaryPersonId(Long beneficiaryPersonId) {
		this.beneficiaryPersonId = beneficiaryPersonId;
	}
	public String getRelationDesc() {
		return relationDesc;
	}
	public void setRelationDesc(String relationDesc) {
		this.relationDesc = relationDesc;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getNurseName() {
		return nurseName;
	}
	public void setNurseName(String nurseName) {
		this.nurseName = nurseName;
	}
	public String getGuardianName() {
		return guardianName;
	}
	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}
	public String getBeneficiaryRelationEn() {
		return beneficiaryRelationEn;
	}
	public void setBeneficiaryRelationEn(String beneficiaryRelationEn) {
		this.beneficiaryRelationEn = beneficiaryRelationEn;
	}
	public String getBeneficiaryRelationAr() {
		return beneficiaryRelationAr;
	}
	public void setBeneficiaryRelationAr(String beneficiaryRelationAr) {
		this.beneficiaryRelationAr = beneficiaryRelationAr;
	}
	public String getBeneficiaryAccNumber() {
		return beneficiaryAccNumber;
	}
	public void setBeneficiaryAccNumber(String beneficiaryAccNumber) {
		this.beneficiaryAccNumber = beneficiaryAccNumber;
	}
	public String getDisListTypeEn() {
		return disListTypeEn;
	}
	public void setDisListTypeEn(String disListTypeEn) {
		this.disListTypeEn = disListTypeEn;
	}
	public String getDisListTypeAr() {
		return disListTypeAr;
	}
	public void setDisListTypeAr(String disListTypeAr) {
		this.disListTypeAr = disListTypeAr;
	}
	public String getFiled1() {
		return filed1;
	}
	public void setFiled1(String filed1) {
		this.filed1 = filed1;
	}
	public String getFiled2() {
		return filed2;
	}
	public void setFiled2(String filed2) {
		this.filed2 = filed2;
	}
	public String getFiled3() {
		return filed3;
	}
	public void setFiled3(String filed3) {
		this.filed3 = filed3;
	}
	public String getFiled4() {
		return filed4;
	}
	public void setFiled4(String filed4) {
		this.filed4 = filed4;
	}
	public String getFiled5() {
		return filed5;
	}
	public void setFiled5(String filed5) {
		this.filed5 = filed5;
	}
	public String getFiled6() {
		return filed6;
	}
	public void setFiled6(String filed6) {
		this.filed6 = filed6;
	}
	public String getFiled7() {
		return filed7;
	}
	public void setFiled7(String filed7) {
		this.filed7 = filed7;
	}
	public String getFiled8() {
		return filed8;
	}
	public void setFiled8(String filed8) {
		this.filed8 = filed8;
	}
	public String getFiled9() {
		return filed9;
	}
	public void setFiled9(String filed9) {
		this.filed9 = filed9;
	}
	public String getFiled10() {
		return filed10;
	}
	public void setFiled10(String filed10) {
		this.filed10 = filed10;
	}
	public Long getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(Long isStudent) {
		this.isStudent = isStudent;
	}
	public Long getIsMinor() {
		return isMinor;
	}
	public void setIsMinor(Long isMinor) {
		this.isMinor = isMinor;
	}
	public String getJobDesc() {
		return jobDesc;
	}
	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}
	
}
	