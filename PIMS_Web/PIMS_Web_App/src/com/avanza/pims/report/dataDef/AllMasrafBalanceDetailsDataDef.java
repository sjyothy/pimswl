package com.avanza.pims.report.dataDef;

public class AllMasrafBalanceDetailsDataDef extends AbstractReportDataDef
{
	private String masrafId;
	private String masrafName; 
	private String masrafGL;
	private String masrafGRPAccNo;
	private String parentMasrafId;
	private String parentMasrafName;
	private String parentMasrafGL;
	private String parentMasrafGRPAccNo;
	private String totalBalance;
	private String totalCredit;
	private String totalDebit;
	private String requestedAmount;
	private String availableBalance;
	private String showOnlyMainMasraf;
	private String compositeMasrafId;
	private String totalBalanceLastYear;
	private String totalCreditLastYear;
	private String totalDebitLastYear;
	private String balance;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	public String getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(String totalCredit) {
		this.totalCredit = totalCredit;
	}
	public String getTotalDebit() {
		return totalDebit;
	}
	public void setTotalDebit(String totalDebit) {
		this.totalDebit = totalDebit;
	}
	public String getCompositeMasrafId() {
		return compositeMasrafId;
	}
	public void setCompositeMasrafId(String compositeMasrafId) {
		this.compositeMasrafId = compositeMasrafId;
	}
	public String getShowOnlyMainMasraf() {
		return showOnlyMainMasraf;
	}
	public void setShowOnlyMainMasraf(String showOnlyMainMasraf) {
		this.showOnlyMainMasraf = showOnlyMainMasraf;
	}
	public String getTotalBalanceLastYear() {
		return totalBalanceLastYear;
	}
	public void setTotalBalanceLastYear(String totalBalanceLastYear) {
		this.totalBalanceLastYear = totalBalanceLastYear;
	}
	public String getTotalCreditLastYear() {
		return totalCreditLastYear;
	}
	public void setTotalCreditLastYear(String totalCreditLastYear) {
		this.totalCreditLastYear = totalCreditLastYear;
	}
	public String getTotalDebitLastYear() {
		return totalDebitLastYear;
	}
	public void setTotalDebitLastYear(String totalDebitLastYear) {
		this.totalDebitLastYear = totalDebitLastYear;
	}
	public String getMasrafName() {
		return masrafName;
	}
	public void setMasrafName(String masrafName) {
		this.masrafName = masrafName;
	}
	public String getMasrafId() {
		return masrafId;
	}
	public void setMasrafId(String masrafId) {
		this.masrafId = masrafId;
	}
	
	public String getMasrafGL() {
		return masrafGL;
	}
	public void setMasrafGL(String masrafGL) {
		this.masrafGL = masrafGL;
	}
	public String getMasrafGRPAccNo() {
		return masrafGRPAccNo;
	}
	public void setMasrafGRPAccNo(String masrafGRPAccNo) {
		this.masrafGRPAccNo = masrafGRPAccNo;
	}
	public String getParentMasrafId() {
		return parentMasrafId;
	}
	public void setParentMasrafId(String parentMasrafId) {
		this.parentMasrafId = parentMasrafId;
	}
	public String getParentMasrafName() {
		return parentMasrafName;
	}
	public void setParentMasrafName(String parentMasrafName) {
		this.parentMasrafName = parentMasrafName;
	}
	public String getParentMasrafGL() {
		return parentMasrafGL;
	}
	public void setParentMasrafGL(String parentMasrafGL) {
		this.parentMasrafGL = parentMasrafGL;
	}
	public String getParentMasrafGRPAccNo() {
		return parentMasrafGRPAccNo;
	}
	public void setParentMasrafGRPAccNo(String parentMasrafGRPAccNo) {
		this.parentMasrafGRPAccNo = parentMasrafGRPAccNo;
	}
	public String getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}
	public String getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public String getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	
}
	