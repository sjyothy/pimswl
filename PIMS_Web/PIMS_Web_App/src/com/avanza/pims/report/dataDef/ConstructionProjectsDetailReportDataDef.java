package com.avanza.pims.report.dataDef;

public class ConstructionProjectsDetailReportDataDef extends AbstractReportDataDef{
	private String projectNumber;
	private String projectStatusEn;
	private String projectStatusAr;
	private String projectName;
	private String projectTypeEn;
	private String projectTypeAr;
	private String completionPercentage;
	private String achievedMilestone;
	
	private String followupDate;
	private String followupStatusEn;
	private String followupStatusAr;
	private String followupTypeEn;
	private String followupTypeAr;
	private String remarks;
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectStatusEn() {
		return projectStatusEn;
	}
	public void setProjectStatusEn(String projectStatusEn) {
		this.projectStatusEn = projectStatusEn;
	}
	public String getProjectStatusAr() {
		return projectStatusAr;
	}
	public void setProjectStatusAr(String projectStatusAr) {
		this.projectStatusAr = projectStatusAr;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectTypeEn() {
		return projectTypeEn;
	}
	public void setProjectTypeEn(String projectTypeEn) {
		this.projectTypeEn = projectTypeEn;
	}
	public String getProjectTypeAr() {
		return projectTypeAr;
	}
	public void setProjectTypeAr(String projectTypeAr) {
		this.projectTypeAr = projectTypeAr;
	}
	public String getCompletionPercentage() {
		return completionPercentage;
	}
	public void setCompletionPercentage(String completionPercentage) {
		this.completionPercentage = completionPercentage;
	}
	public String getAchievedMilestone() {
		return achievedMilestone;
	}
	public void setAchievedMilestone(String achievedMilestone) {
		this.achievedMilestone = achievedMilestone;
	}
	public String getFollowupDate() {
		return followupDate;
	}
	public void setFollowupDate(String followupDate) {
		this.followupDate = followupDate;
	}
	public String getFollowupStatusEn() {
		return followupStatusEn;
	}
	public void setFollowupStatusEn(String followupStatusEn) {
		this.followupStatusEn = followupStatusEn;
	}
	public String getFollowupStatusAr() {
		return followupStatusAr;
	}
	public void setFollowupStatusAr(String followupStatusAr) {
		this.followupStatusAr = followupStatusAr;
	}
	public String getFollowupTypeEn() {
		return followupTypeEn;
	}
	public void setFollowupTypeEn(String followupTypeEn) {
		this.followupTypeEn = followupTypeEn;
	}
	public String getFollowupTypeAr() {
		return followupTypeAr;
	}
	public void setFollowupTypeAr(String followupTypeAr) {
		this.followupTypeAr = followupTypeAr;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	

}
