package com.avanza.pims.report.dataDef;

public class ChequeWithdrawlReportLabelDataDef extends AbstractReportDataDef{

	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	private	String bankRemitanceAccNameEn;
	private String chequeDateEn;
	private String chequeAmountEn;
	private String chequeNumberEn;
	private String tenantAccountNumberEn;
	private String chequeBankEn;
	private String chequeOwnerEn;
	private String palaceHolder1En;
	private String palaceHolder2En;
	private String palaceHolder3En;
	private String palaceHolder4En;
	private String date; 
	
	private	String bankRemitanceAccNameAr;
	private String chequeDateAr;
	private String chequeAmountAr;
	private String chequeNumberAr;
	private String tenantAccountNumberAr;
	private String chequeBankAr;
	private String chequeOwnerAr;
	private String palaceHolder1Ar;
	private String palaceHolder2Ar;
	private String palaceHolder3Ar;
	private String palaceHolder4Ar;
	
	
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getBankRemitanceAccNameEn() {
		return bankRemitanceAccNameEn;
	}
	public void setBankRemitanceAccNameEn(String bankRemitanceAccNameEn) {
		this.bankRemitanceAccNameEn = bankRemitanceAccNameEn;
	}
	public String getChequeDateEn() {
		return chequeDateEn;
	}
	public void setChequeDateEn(String chequeDateEn) {
		this.chequeDateEn = chequeDateEn;
	}
	public String getChequeAmountEn() {
		return chequeAmountEn;
	}
	public void setChequeAmountEn(String chequeAmountEn) {
		this.chequeAmountEn = chequeAmountEn;
	}
	public String getChequeNumberEn() {
		return chequeNumberEn;
	}
	public void setChequeNumberEn(String chequeNumberEn) {
		this.chequeNumberEn = chequeNumberEn;
	}
	public String getTenantAccountNumberEn() {
		return tenantAccountNumberEn;
	}
	public void setTenantAccountNumberEn(String tenantAccountNumberEn) {
		this.tenantAccountNumberEn = tenantAccountNumberEn;
	}
	public String getChequeBankEn() {
		return chequeBankEn;
	}
	public void setChequeBankEn(String chequeBankEn) {
		this.chequeBankEn = chequeBankEn;
	}
	public String getChequeOwnerEn() {
		return chequeOwnerEn;
	}
	public void setChequeOwnerEn(String chequeOwnerEn) {
		this.chequeOwnerEn = chequeOwnerEn;
	}
	public String getPalaceHolder1En() {
		return palaceHolder1En;
	}
	public void setPalaceHolder1En(String palaceHolder1En) {
		this.palaceHolder1En = palaceHolder1En;
	}
	public String getPalaceHolder2En() {
		return palaceHolder2En;
	}
	public void setPalaceHolder2En(String palaceHolder2En) {
		this.palaceHolder2En = palaceHolder2En;
	}
	public String getPalaceHolder3En() {
		return palaceHolder3En;
	}
	public void setPalaceHolder3En(String palaceHolder3En) {
		this.palaceHolder3En = palaceHolder3En;
	}
	public String getPalaceHolder4En() {
		return palaceHolder4En;
	}
	public void setPalaceHolder4En(String palaceHolder4En) {
		this.palaceHolder4En = palaceHolder4En;
	}
	public String getBankRemitanceAccNameAr() {
		return bankRemitanceAccNameAr;
	}
	public void setBankRemitanceAccNameAr(String bankRemitanceAccNameAr) {
		this.bankRemitanceAccNameAr = bankRemitanceAccNameAr;
	}
	public String getChequeDateAr() {
		return chequeDateAr;
	}
	public void setChequeDateAr(String chequeDateAr) {
		this.chequeDateAr = chequeDateAr;
	}
	public String getChequeAmountAr() {
		return chequeAmountAr;
	}
	public void setChequeAmountAr(String chequeAmountAr) {
		this.chequeAmountAr = chequeAmountAr;
	}
	public String getChequeNumberAr() {
		return chequeNumberAr;
	}
	public void setChequeNumberAr(String chequeNumberAr) {
		this.chequeNumberAr = chequeNumberAr;
	}
	public String getTenantAccountNumberAr() {
		return tenantAccountNumberAr;
	}
	public void setTenantAccountNumberAr(String tenantAccountNumberAr) {
		this.tenantAccountNumberAr = tenantAccountNumberAr;
	}
	public String getChequeBankAr() {
		return chequeBankAr;
	}
	public void setChequeBankAr(String chequeBankAr) {
		this.chequeBankAr = chequeBankAr;
	}
	public String getChequeOwnerAr() {
		return chequeOwnerAr;
	}
	public void setChequeOwnerAr(String chequeOwnerAr) {
		this.chequeOwnerAr = chequeOwnerAr;
	}
	public String getPalaceHolder1Ar() {
		return palaceHolder1Ar;
	}
	public void setPalaceHolder1Ar(String palaceHolder1Ar) {
		this.palaceHolder1Ar = palaceHolder1Ar;
	}
	public String getPalaceHolder2Ar() {
		return palaceHolder2Ar;
	}
	public void setPalaceHolder2Ar(String palaceHolder2Ar) {
		this.palaceHolder2Ar = palaceHolder2Ar;
	}
	public String getPalaceHolder3Ar() {
		return palaceHolder3Ar;
	}
	public void setPalaceHolder3Ar(String palaceHolder3Ar) {
		this.palaceHolder3Ar = palaceHolder3Ar;
	}
	public String getPalaceHolder4Ar() {
		return palaceHolder4Ar;
	}
	public void setPalaceHolder4Ar(String palaceHolder4Ar) {
		this.palaceHolder4Ar = palaceHolder4Ar;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
