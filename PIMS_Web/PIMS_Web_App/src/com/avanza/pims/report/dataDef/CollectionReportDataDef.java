package com.avanza.pims.report.dataDef;

public class CollectionReportDataDef extends AbstractReportDataDef
{
    private String tenantName;
	private String propertyName;
	private String unitNumber;
	private String contractNumber;
	private String contractId;
	private String receiptNumber;
	private String receiptDate;

	private String receiptStatusEn;
	private String receiptStatusAr;
	private String receiptId;
	private String receiptBy;
	private String receiptAmount;
	private String chequeAmount;
	
	private String bankNameEn;
	private String bankNameAr;
	
	private String chequeTypeEn;
	private String chequeTypeAr;
	
	private String chequeType;

	private String chequeNumber;
	private String chequeDate;
	
	
	private String paymentMethodEn;
	private String paymentMethodAr;
	private String transactionTypeEn;
	private String transactionTypeAr;
    private String paymentReceiptDetailId;	
    private String bankremitanceaccountname;
    private String facActivityName;
    private String facReceiptTypeId;
    private String memoLineName;
    private String facTrxTypeDebit;
    private String facTrxTypeName;
    private String facDesc;
    private String actionNumber;
    private String grpAccountNumber;
    private String requestNumber;
    private String requestId;
    private String paymentTypeAr;
    private String paymentTypeEn;
    private String paymentTypeId;
    private String paymentScheduleId;
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	public String getBankremitanceaccountname() {
		return bankremitanceaccountname;
	}
	public void setBankremitanceaccountname(String bankremitanceaccountname) {
		this.bankremitanceaccountname = bankremitanceaccountname;
	}
	public String getFacActivityName() {
		return facActivityName;
	}
	public void setFacActivityName(String facActivityName) {
		this.facActivityName = facActivityName;
	}
	public String getFacReceiptTypeId() {
		return facReceiptTypeId;
	}
	public void setFacReceiptTypeId(String facReceiptTypeId) {
		this.facReceiptTypeId = facReceiptTypeId;
	}
	public String getMemoLineName() {
		return memoLineName;
	}
	public void setMemoLineName(String memoLineName) {
		this.memoLineName = memoLineName;
	}
	public String getFacTrxTypeDebit() {
		return facTrxTypeDebit;
	}
	public void setFacTrxTypeDebit(String facTrxTypeDebit) {
		this.facTrxTypeDebit = facTrxTypeDebit;
	}
	public String getFacTrxTypeName() {
		return facTrxTypeName;
	}
	public void setFacTrxTypeName(String facTrxTypeName) {
		this.facTrxTypeName = facTrxTypeName;
	}
	public String getFacDesc() {
		return facDesc;
	}
	public void setFacDesc(String facDesc) {
		this.facDesc = facDesc;
	}
	public String getActionNumber() {
		return actionNumber;
	}
	public void setActionNumber(String actionNumber) {
		this.actionNumber = actionNumber;
	}
	public String getGrpAccountNumber() {
		return grpAccountNumber;
	}
	public void setGrpAccountNumber(String grpAccountNumber) {
		this.grpAccountNumber = grpAccountNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getReceiptBy() {
		return receiptBy;
	}
	public void setReceiptBy(String receiptBy) {
		this.receiptBy = receiptBy;
	}
	public String getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public String getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	
	public String getChequeType() {
		return chequeType;
	}
	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getReceiptStatusEn() {
		return receiptStatusEn;
	}
	public void setReceiptStatusEn(String receiptStatusEn) {
		this.receiptStatusEn = receiptStatusEn;
	}
	public String getReceiptStatusAr() {
		return receiptStatusAr;
	}
	public void setReceiptStatusAr(String receiptStatusAr) {
		this.receiptStatusAr = receiptStatusAr;
	}
	public String getBankNameEn() {
		return bankNameEn;
	}
	public void setBankNameEn(String bankNameEn) {
		this.bankNameEn = bankNameEn;
	}
	public String getBankNameAr() {
		return bankNameAr;
	}
	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}
	public String getChequeTypeEn() {
		return chequeTypeEn;
	}
	public void setChequeTypeEn(String chequeTypeEn) {
		this.chequeTypeEn = chequeTypeEn;
	}
	public String getChequeTypeAr() {
		return chequeTypeAr;
	}
	public void setChequeTypeAr(String chequeTypeAr) {
		this.chequeTypeAr = chequeTypeAr;
	}
	public String getTransactionTypeEn() {
		return transactionTypeEn;
	}
	public void setTransactionTypeEn(String transactionTypeEn) {
		this.transactionTypeEn = transactionTypeEn;
	}
	public String getTransactionTypeAr() {
		return transactionTypeAr;
	}
	public void setTransactionTypeAr(String transactionTypeAr) {
		this.transactionTypeAr = transactionTypeAr;
	}
	public String getPaymentMethodEn() {
		return paymentMethodEn;
	}
	public void setPaymentMethodEn(String paymentMethodEn) {
		this.paymentMethodEn = paymentMethodEn;
	}
	public String getPaymentMethodAr() {
		return paymentMethodAr;
	}
	public void setPaymentMethodAr(String paymentMethodAr) {
		this.paymentMethodAr = paymentMethodAr;
	}
	public String getPaymentReceiptDetailId() {
		return paymentReceiptDetailId;
	}
	public void setPaymentReceiptDetailId(String paymentReceiptDetailId) {
		this.paymentReceiptDetailId = paymentReceiptDetailId;
	}
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}

	
}
