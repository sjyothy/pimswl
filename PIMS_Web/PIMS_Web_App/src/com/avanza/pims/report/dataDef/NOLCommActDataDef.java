package com.avanza.pims.report.dataDef;

import com.avanza.pims.report.criteria.NOLCriteria;

public class NOLCommActDataDef extends AbstractReportDataDef
{
	
	private String commercialActivity;
	
	public String getCommercialActivity() {
		return commercialActivity;
	}

	public void setCommercialActivity(String commercialActivity) {
		this.commercialActivity = commercialActivity;
	}  
}
	