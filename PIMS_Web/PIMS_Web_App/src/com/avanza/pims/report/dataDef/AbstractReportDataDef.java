package com.avanza.pims.report.dataDef;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.util.CommonUtil;

public class AbstractReportDataDef implements IReportDataDef {
	  public  String getDateFormat()
		{
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
			
		}  
}
