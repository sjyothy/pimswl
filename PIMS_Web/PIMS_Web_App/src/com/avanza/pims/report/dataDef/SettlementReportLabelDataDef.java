package com.avanza.pims.report.dataDef;

public class SettlementReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblContractStartDateEn;
	private String lblContractStartDateAr;
	private String lblContractEndDateEn;
	private String lblContractEndDateAr;
	private String lblContractRentalAmountEn;
	private String lblContractRentalAmountAr;
	private String lblEvacuationDateEn;
	private String lblEvacuationDateAr;
	private String lblRentPeriodEn;
	private String lblRentPeriodAr;
	private String lblRentAmountEn;
	private String lblRentAmountAr;
	private String lblMonthsEn;
	private String lblMonthsAr;
	private String lblDaysEn;
	private String lblDaysAr;
	private String lblDepositAmountAr;
	private String lblDepositAmountEn;
	private String lblRealizedAmountEn;
	private String lblRealizedAmountAr;
	private String lblPaidAmountEn;
	private String lblPaidAmountAr;
	
	private String lblUnRealizedAmountEn;
	private String lblUnRealizedAmountAr;
	private String lblEvacuationFinesEn;
	private String lblEvacuationFinesAr;

	private String lblUnpaidAmountEn;
	private String lblUnpaidAmountAr;
	private String lblUnpaidFinesEn;
	private String lblUnpaidFinesAr;
	private String lblPaymentTypeEn;
	private String lblPaymentTypeAr;
	private String lblPaymentDueDateEn;
	private String lblPaymentDueDateAr;
	private String lblMethodEn;
	private String lblMethodAr;
	private String lblPaymentAmountEn;
	private String lblPaymentAmountAr;
	private String lblPaymentDescriptionEn;
	private String lblPaymentDescriptionAr;
	private String lblStatusEn;
	private String lblStatusAr;
	private String lblReceiptNumberEn;
	private String lblReceiptNumberAr;
	private String lbltotalRefundAmountEn;
	private String lbltotalDueAmountEn;
	private String lbltotalRefundAmountAr;
	private String lbltotalDueAmountAr;

	private String lblRentPaymentsEn; // remove these labels
	private String lblOtherPaymentsEn;
	private String lblReturnedPaymentsEn;
	private String lblRentPaymentsAr;
	private String lblOtherPaymentsAr;
	private String lblReturnedPaymentsAr;
	private String lblPaymentCurrency;
	private String lblPaymentCurrencyAr;
	
		
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblSettlementReportEn;
	private String lblPIMSHeaderAr;
	private String lblSettlementReportAr;
	private String lblLoggedInUser;
	
	private String lblContractNumberEn;
	private String lblContractNumberAr;
	private String lblTenantNameEn;
	private String lblTenantNameAr;
	private String lblCostCenterEn;
	private String lblCostCenterAr;
	private String lblGrpCustomerNumberEn;
	private String lblGrpCustomerNumberAr;
	
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	//////////////////////
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblContractRentalAmountEn() {
		return lblContractRentalAmountEn;
	}
	public void setLblContractRentalAmountEn(String lblContractRentalAmountEn) {
		this.lblContractRentalAmountEn = lblContractRentalAmountEn;
	}
	public String getLblContractRentalAmountAr() {
		return lblContractRentalAmountAr;
	}
	public void setLblContractRentalAmountAr(String lblContractRentalAmountAr) {
		this.lblContractRentalAmountAr = lblContractRentalAmountAr;
	}
	public String getLblEvacuationDateEn() {
		return lblEvacuationDateEn;
	}
	public void setLblEvacuationDateEn(String lblEvacuationDateEn) {
		this.lblEvacuationDateEn = lblEvacuationDateEn;
	}
	public String getLblEvacuationDateAr() {
		return lblEvacuationDateAr;
	}
	public void setLblEvacuationDateAr(String lblEvacuationDateAr) {
		this.lblEvacuationDateAr = lblEvacuationDateAr;
	}
	public String getLblRentPeriodEn() {
		return lblRentPeriodEn;
	}
	public void setLblRentPeriodEn(String lblRentPeriodEn) {
		this.lblRentPeriodEn = lblRentPeriodEn;
	}
	public String getLblRentPeriodAr() {
		return lblRentPeriodAr;
	}
	public void setLblRentPeriodAr(String lblRentPeriodAr) {
		this.lblRentPeriodAr = lblRentPeriodAr;
	}
	public String getLblRentAmountEn() {
		return lblRentAmountEn;
	}
	public void setLblRentAmountEn(String lblRentAmountEn) {
		this.lblRentAmountEn = lblRentAmountEn;
	}
	public String getLblRentAmountAr() {
		return lblRentAmountAr;
	}
	public void setLblRentAmountAr(String lblRentAmountAr) {
		this.lblRentAmountAr = lblRentAmountAr;
	}
	public String getLblMonthsEn() {
		return lblMonthsEn;
	}
	public void setLblMonthsEn(String lblMonthsEn) {
		this.lblMonthsEn = lblMonthsEn;
	}
	public String getLblMonthsAr() {
		return lblMonthsAr;
	}
	public void setLblMonthsAr(String lblMonthsAr) {
		this.lblMonthsAr = lblMonthsAr;
	}
	public String getLblDaysEn() {
		return lblDaysEn;
	}
	public void setLblDaysEn(String lblDaysEn) {
		this.lblDaysEn = lblDaysEn;
	}
	public String getLblDaysAr() {
		return lblDaysAr;
	}
	public void setLblDaysAr(String lblDaysAr) {
		this.lblDaysAr = lblDaysAr;
	}
	public String getLblDepositAmountAr() {
		return lblDepositAmountAr;
	}
	public void setLblDepositAmountAr(String lblDepositAmountAr) {
		this.lblDepositAmountAr = lblDepositAmountAr;
	}
	public String getLblDepositAmountEn() {
		return lblDepositAmountEn;
	}
	public void setLblDepositAmountEn(String lblDepositAmountEn) {
		this.lblDepositAmountEn = lblDepositAmountEn;
	}
	public String getLblRealizedAmountEn() {
		return lblRealizedAmountEn;
	}
	public void setLblRealizedAmountEn(String lblRealizedAmountEn) {
		this.lblRealizedAmountEn = lblRealizedAmountEn;
	}
	public String getLblRealizedAmountAr() {
		return lblRealizedAmountAr;
	}
	public void setLblRealizedAmountAr(String lblRealizedAmountAr) {
		this.lblRealizedAmountAr = lblRealizedAmountAr;
	}
	public String getLblPaidAmountEn() {
		return lblPaidAmountEn;
	}
	public void setLblPaidAmountEn(String lblPaidAmountEn) {
		this.lblPaidAmountEn = lblPaidAmountEn;
	}
	public String getLblPaidAmountAr() {
		return lblPaidAmountAr;
	}
	public void setLblPaidAmountAr(String lblPaidAmountAr) {
		this.lblPaidAmountAr = lblPaidAmountAr;
	}
	public String getLblUnRealizedAmountEn() {
		return lblUnRealizedAmountEn;
	}
	public void setLblUnRealizedAmountEn(String lblUnRealizedAmountEn) {
		this.lblUnRealizedAmountEn = lblUnRealizedAmountEn;
	}
	public String getLblUnRealizedAmountAr() {
		return lblUnRealizedAmountAr;
	}
	public void setLblUnRealizedAmountAr(String lblUnRealizedAmountAr) {
		this.lblUnRealizedAmountAr = lblUnRealizedAmountAr;
	}
	public String getLblEvacuationFinesEn() {
		return lblEvacuationFinesEn;
	}
	public void setLblEvacuationFinesEn(String lblEvacuationFinesEn) {
		this.lblEvacuationFinesEn = lblEvacuationFinesEn;
	}
	public String getLblEvacuationFinesAr() {
		return lblEvacuationFinesAr;
	}
	public void setLblEvacuationFinesAr(String lblEvacuationFinesAr) {
		this.lblEvacuationFinesAr = lblEvacuationFinesAr;
	}
	public String getLblUnpaidAmountEn() {
		return lblUnpaidAmountEn;
	}
	public void setLblUnpaidAmountEn(String lblUnpaidAmountEn) {
		this.lblUnpaidAmountEn = lblUnpaidAmountEn;
	}
	public String getLblUnpaidAmountAr() {
		return lblUnpaidAmountAr;
	}
	public void setLblUnpaidAmountAr(String lblUnpaidAmountAr) {
		this.lblUnpaidAmountAr = lblUnpaidAmountAr;
	}
	public String getLblUnpaidFinesEn() {
		return lblUnpaidFinesEn;
	}
	public void setLblUnpaidFinesEn(String lblUnpaidFinesEn) {
		this.lblUnpaidFinesEn = lblUnpaidFinesEn;
	}
	public String getLblUnpaidFinesAr() {
		return lblUnpaidFinesAr;
	}
	public void setLblUnpaidFinesAr(String lblUnpaidFinesAr) {
		this.lblUnpaidFinesAr = lblUnpaidFinesAr;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentDueDateEn() {
		return lblPaymentDueDateEn;
	}
	public void setLblPaymentDueDateEn(String lblPaymentDueDateEn) {
		this.lblPaymentDueDateEn = lblPaymentDueDateEn;
	}
	public String getLblPaymentDueDateAr() {
		return lblPaymentDueDateAr;
	}
	public void setLblPaymentDueDateAr(String lblPaymentDueDateAr) {
		this.lblPaymentDueDateAr = lblPaymentDueDateAr;
	}
	public String getLblMethodEn() {
		return lblMethodEn;
	}
	public void setLblMethodEn(String lblMethodEn) {
		this.lblMethodEn = lblMethodEn;
	}
	public String getLblMethodAr() {
		return lblMethodAr;
	}
	public void setLblMethodAr(String lblMethodAr) {
		this.lblMethodAr = lblMethodAr;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblSettlementReportEn() {
		return lblSettlementReportEn;
	}
	public void setLblSettlementReportEn(String lblSettlementReportEn) {
		this.lblSettlementReportEn = lblSettlementReportEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblSettlementReportAr() {
		return lblSettlementReportAr;
	}
	public void setLblSettlementReportAr(String lblSettlementReportAr) {
		this.lblSettlementReportAr = lblSettlementReportAr;
	}
	public String getLbltotalRefundAmountEn() {
		return lbltotalRefundAmountEn;
	}
	public void setLbltotalRefundAmountEn(String lbltotalRefundAmountEn) {
		this.lbltotalRefundAmountEn = lbltotalRefundAmountEn;
	}
	public String getLbltotalDueAmountEn() {
		return lbltotalDueAmountEn;
	}
	public void setLbltotalDueAmountEn(String lbltotalDueAmountEn) {
		this.lbltotalDueAmountEn = lbltotalDueAmountEn;
	}
	public String getLbltotalRefundAmountAr() {
		return lbltotalRefundAmountAr;
	}
	public void setLbltotalRefundAmountAr(String lbltotalRefundAmountAr) {
		this.lbltotalRefundAmountAr = lbltotalRefundAmountAr;
	}
	public String getLbltotalDueAmountAr() {
		return lbltotalDueAmountAr;
	}
	public void setLbltotalDueAmountAr(String lbltotalDueAmountAr) {
		this.lbltotalDueAmountAr = lbltotalDueAmountAr;
	}
	public String getLblPaymentDescriptionEn() {
		return lblPaymentDescriptionEn;
	}
	public void setLblPaymentDescriptionEn(String lblPaymentDescriptionEn) {
		this.lblPaymentDescriptionEn = lblPaymentDescriptionEn;
	}
	public String getLblPaymentDescriptionAr() {
		return lblPaymentDescriptionAr;
	}
	public void setLblPaymentDescriptionAr(String lblPaymentDescriptionAr) {
		this.lblPaymentDescriptionAr = lblPaymentDescriptionAr;
	}
	
	public String getLblRentPaymentsEn() {
		return lblRentPaymentsEn;
	}
	public void setLblRentPaymentsEn(String lblRentPaymentsEn) {
		this.lblRentPaymentsEn = lblRentPaymentsEn;
	}
	public String getLblOtherPaymentsEn() {
		return lblOtherPaymentsEn;
	}
	public void setLblOtherPaymentsEn(String lblOtherPaymentsEn) {
		this.lblOtherPaymentsEn = lblOtherPaymentsEn;
	}
	public String getLblRentPaymentsAr() {
		return lblRentPaymentsAr;
	}
	public void setLblRentPaymentsAr(String lblRentPaymentsAr) {
		this.lblRentPaymentsAr = lblRentPaymentsAr;
	}
	public String getLblOtherPaymentsAr() {
		return lblOtherPaymentsAr;
	}
	public void setLblOtherPaymentsAr(String lblOtherPaymentsAr) {
		this.lblOtherPaymentsAr = lblOtherPaymentsAr;
	}
	public String getLblReturnedPaymentsEn() {
		return lblReturnedPaymentsEn;
	}
	public void setLblReturnedPaymentsEn(String lblReturnedPaymentsEn) {
		this.lblReturnedPaymentsEn = lblReturnedPaymentsEn;
	}
	public String getLblReturnedPaymentsAr() {
		return lblReturnedPaymentsAr;
	}
	public void setLblReturnedPaymentsAr(String lblReturnedPaymentsAr) {
		this.lblReturnedPaymentsAr = lblReturnedPaymentsAr;
	}
	public String getLblPaymentCurrency() {
		return lblPaymentCurrency;
	}
	public void setLblPaymentCurrency(String lblPaymentCurrency) {
		this.lblPaymentCurrency = lblPaymentCurrency;
	}
	public String getLblPaymentCurrencyAr() {
		return lblPaymentCurrencyAr;
	}
	public void setLblPaymentCurrencyAr(String lblPaymentCurrencyAr) {
		this.lblPaymentCurrencyAr = lblPaymentCurrencyAr;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblCostCenterEn() {
		return lblCostCenterEn;
	}
	public void setLblCostCenterEn(String lblCostCenterEn) {
		this.lblCostCenterEn = lblCostCenterEn;
	}
	public String getLblCostCenterAr() {
		return lblCostCenterAr;
	}
	public void setLblCostCenterAr(String lblCostCenterAr) {
		this.lblCostCenterAr = lblCostCenterAr;
	}
	public String getLblGrpCustomerNumberEn() {
		return lblGrpCustomerNumberEn;
	}
	public void setLblGrpCustomerNumberEn(String lblGrpCustomerNumberEn) {
		this.lblGrpCustomerNumberEn = lblGrpCustomerNumberEn;
	}
	public String getLblGrpCustomerNumberAr() {
		return lblGrpCustomerNumberAr;
	}
	public void setLblGrpCustomerNumberAr(String lblGrpCustomerNumberAr) {
		this.lblGrpCustomerNumberAr = lblGrpCustomerNumberAr;
	}
	
	
}
