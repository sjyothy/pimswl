package com.avanza.pims.report.dataDef;

public class PaymentHistoryReportLabelDataDef extends AbstractReportDataDef{
    /*******************************                           English Labels             *********************************/
	
	private String lblBankNameEn;
	private String lblChequeNumberEn;
	private String lblChequeDateEn;
	private String lblAmountEn;
	private String lblContractNumberEn;
	private String lblTenantNameEn;
	private String lblPaymentTypeEn;
	private String lblPaymentDateEn;
	
    /*******************************                           Arabic Labels             *********************************/
	private String lblBankNameAr;
	private String lblChequeNumberAr;
	private String lblChequeDateAr;
	private String lblAmountAr;
	private String lblContractNumberAr;
	private String lblTenantNameAr;
	private String lblPaymentTypeAr;
	private String lblPaymentDateAr;
	
    /*******************************                           Report Summary Labels             *********************************/
	private String lblTotalAmountEn;
	private String lblTotalAmountAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblAmountEn() {
		return lblAmountEn;
	}
	public void setLblAmountEn(String lblAmountEn) {
		this.lblAmountEn = lblAmountEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentDateEn() {
		return lblPaymentDateEn;
	}
	public void setLblPaymentDateEn(String lblPaymentDateEn) {
		this.lblPaymentDateEn = lblPaymentDateEn;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblAmountAr() {
		return lblAmountAr;
	}
	public void setLblAmountAr(String lblAmountAr) {
		this.lblAmountAr = lblAmountAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentDateAr() {
		return lblPaymentDateAr;
	}
	public void setLblPaymentDateAr(String lblPaymentDateAr) {
		this.lblPaymentDateAr = lblPaymentDateAr;
	}
	public String getLblTotalAmountEn() {
		return lblTotalAmountEn;
	}
	public void setLblTotalAmountEn(String lblTotalAmountEn) {
		this.lblTotalAmountEn = lblTotalAmountEn;
	}
	public String getLblTotalAmountAr() {
		return lblTotalAmountAr;
	}
	public void setLblTotalAmountAr(String lblTotalAmountAr) {
		this.lblTotalAmountAr = lblTotalAmountAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	

}
