package com.avanza.pims.report.dataDef;

public class ApplicationSummaryReportDateDef extends AbstractReportDataDef
{
	private String applicationNumber;
	private String applicationStatusAr;
	private String applicationStatusEn;
	private String applicantName;
	private String applicationTypeEn;
	private String applicationTypeAr;
	private String applicationDateFrom;
	private String applicationDescription;
	private String createdBy;
	private String appPaidAmount;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;

	
	
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getApplicationStatusAr() {
		return applicationStatusAr;
	}
	public void setApplicationStatusAr(String applicationStatusAr) {
		this.applicationStatusAr = applicationStatusAr;
	}
	public String getApplicationStatusEn() {
		return applicationStatusEn;
	}
	public void setApplicationStatusEn(String applicationStatusEn) {
		this.applicationStatusEn = applicationStatusEn;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getApplicationTypeEn() {
		return applicationTypeEn;
	}
	public void setApplicationTypeEn(String applicationTypeEn) {
		this.applicationTypeEn = applicationTypeEn;
	}
	public String getApplicationTypeAr() {
		return applicationTypeAr;
	}
	public void setApplicationTypeAr(String applicationTypeAr) {
		this.applicationTypeAr = applicationTypeAr;
	}
	public String getApplicationDateFrom() {
		return applicationDateFrom;
	}
	public void setApplicationDateFrom(String applicationDateFrom) {
		this.applicationDateFrom = applicationDateFrom;
	}
	public String getApplicationDescription() {
		return applicationDescription;
	}
	public void setApplicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getAppPaidAmount() {
		return appPaidAmount;
	}
	public void setAppPaidAmount(String appPaidAmount) {
		this.appPaidAmount = appPaidAmount;
	}

	
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	
	
	
	
}
