package com.avanza.pims.report.dataDef;

public class PaymentReceiptReportLabelDataDef extends AbstractReportDataDef{

	/*******************************                           English Labels             *********************************/

	private String lblPaymentNumberEn;
	private String lblPaymentTypeEn;
	private String lblChequeAmountEn;
	private String lblReceiptNumberEn;
	private String lblContractNumberEn;
	private String lblCustomerNameEn;
	private String lblDescriptionEn;
	private String lblPeriodEn;
	private String lblReceiptDateEn;
	private String lblReceiptTotalAmountEn;
	private String lblAmountEn;
	private String lblChequeNumberEn;
	private String lblbankEn;
	private String lblChequeDateEn;
	private String lblCostCenterEn;
	private String lblCustomerNumberEn;
	private String lblUnitDescEn;
	private String lblPaymentMethodEn;
    private String lblOldBankEn;
    private String lblOldChequeNumberEn;
    private String lblRequestNumberEn;
    private String oldChequeDateEn;
    private String lblOldChequeAmountEn;
    private String lblOwnershipTypeEn;
	

	/*******************************                           Arabic Labels             *********************************/

	private String lblPaymentNumberAr;
	private String lblPaymentTypeAr;
	private String lblChequeAmountAr;
	private String lblReceiptNumberAr;
	private String lblContractNumberAr;
	private String lblCustomerNameAr;
	private String lblDescriptionAr;
	private String lblPeriodAr;
	private String lblReceiptDateAr;
	private String lblReceiptTotalAmountAr;
	private String lblAmountAr;
	private String lblChequeNumberAr;
	private String lblbankAr;
	private String lblChequeDateAr;
	private String lblCostCenterAr;
	private String lblCustomerNumberAr;
	private String lblUnitDescAr;
	private String lblPaymentMethodAr;
    private String lblOldBankAr;
    private String lblOldChequeNumberAr;
    private String lblRequestNumberAr;
    private String oldChequeDateAr;
    private String lblOldChequeAmountAr;
    private String lblOwnershipTypeAr;


	/*******************************                           Report Summary Labels             *********************************/
	private String lblTotalPaymentAmountEn;
	private String lblTotalPaymentAmountAr;
	private String lblProcedureEn;
	private String lblProcedureAr;
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblCustomerNameEn() {
		return lblCustomerNameEn;
	}
	public void setLblCustomerNameEn(String lblCustomerNameEn) {
		this.lblCustomerNameEn = lblCustomerNameEn;
	}
	public String getLblDescriptionEn() {
		return lblDescriptionEn;
	}
	public void setLblDescriptionEn(String lblDescriptionEn) {
		this.lblDescriptionEn = lblDescriptionEn;
	}
	public String getLblPeriodEn() {
		return lblPeriodEn;
	}
	public void setLblPeriodEn(String lblPeriodEn) {
		this.lblPeriodEn = lblPeriodEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblReceiptTotalAmountEn() {
		return lblReceiptTotalAmountEn;
	}
	public void setLblReceiptTotalAmountEn(String lblReceiptTotalAmountEn) {
		this.lblReceiptTotalAmountEn = lblReceiptTotalAmountEn;
	}
	public String getLblAmountEn() {
		return lblAmountEn;
	}
	public void setLblAmountEn(String lblAmountEn) {
		this.lblAmountEn = lblAmountEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblbankEn() {
		return lblbankEn;
	}
	public void setLblbankEn(String lblbankEn) {
		this.lblbankEn = lblbankEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblCustomerNameAr() {
		return lblCustomerNameAr;
	}
	public void setLblCustomerNameAr(String lblCustomerNameAr) {
		this.lblCustomerNameAr = lblCustomerNameAr;
	}
	public String getLblDescriptionAr() {
		return lblDescriptionAr;
	}
	public void setLblDescriptionAr(String lblDescriptionAr) {
		this.lblDescriptionAr = lblDescriptionAr;
	}
	public String getLblPeriodAr() {
		return lblPeriodAr;
	}
	public void setLblPeriodAr(String lblPeriodAr) {
		this.lblPeriodAr = lblPeriodAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblReceiptTotalAmountAr() {
		return lblReceiptTotalAmountAr;
	}
	public void setLblReceiptTotalAmountAr(String lblReceiptTotalAmountAr) {
		this.lblReceiptTotalAmountAr = lblReceiptTotalAmountAr;
	}
	public String getLblAmountAr() {
		return lblAmountAr;
	}
	public void setLblAmountAr(String lblAmountAr) {
		this.lblAmountAr = lblAmountAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblbankAr() {
		return lblbankAr;
	}
	public void setLblbankAr(String lblbankAr) {
		this.lblbankAr = lblbankAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblTotalPaymentAmountEn() {
		return lblTotalPaymentAmountEn;
	}
	public void setLblTotalPaymentAmountEn(String lblTotalPaymentAmountEn) {
		this.lblTotalPaymentAmountEn = lblTotalPaymentAmountEn;
	}
	public String getLblTotalPaymentAmountAr() {
		return lblTotalPaymentAmountAr;
	}
	public void setLblTotalPaymentAmountAr(String lblTotalPaymentAmountAr) {
		this.lblTotalPaymentAmountAr = lblTotalPaymentAmountAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblProcedureEn() {
		return lblProcedureEn;
	}
	public void setLblProcedureEn(String lblProcedureEn) {
		this.lblProcedureEn = lblProcedureEn;
	}
	public String getLblProcedureAr() {
		return lblProcedureAr;
	}
	public void setLblProcedureAr(String lblProcedureAr) {
		this.lblProcedureAr = lblProcedureAr;
	}
	public String getLblCostCenterEn() {
		return lblCostCenterEn;
	}
	public void setLblCostCenterEn(String lblCostCenterEn) {
		this.lblCostCenterEn = lblCostCenterEn;
	}
	public String getLblCustomerNumberEn() {
		return lblCustomerNumberEn;
	}
	public void setLblCustomerNumberEn(String lblCustomerNumberEn) {
		this.lblCustomerNumberEn = lblCustomerNumberEn;
	}
	public String getLblCostCenterAr() {
		return lblCostCenterAr;
	}
	public void setLblCostCenterAr(String lblCostCenterAr) {
		this.lblCostCenterAr = lblCostCenterAr;
	}
	public String getLblCustomerNumberAr() {
		return lblCustomerNumberAr;
	}
	public void setLblCustomerNumberAr(String lblCustomerNumberAr) {
		this.lblCustomerNumberAr = lblCustomerNumberAr;
	}
	public String getLblPaymentNumberEn() {
		return lblPaymentNumberEn;
	}
	public void setLblPaymentNumberEn(String lblPaymentNumberEn) {
		this.lblPaymentNumberEn = lblPaymentNumberEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblPaymentNumberAr() {
		return lblPaymentNumberAr;
	}
	public void setLblPaymentNumberAr(String lblPaymentNumberAr) {
		this.lblPaymentNumberAr = lblPaymentNumberAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblUnitDescEn() {
		return lblUnitDescEn;
	}
	public void setLblUnitDescEn(String lblUnitDescEn) {
		this.lblUnitDescEn = lblUnitDescEn;
	}
	public String getLblUnitDescAr() {
		return lblUnitDescAr;
	}
	public void setLblUnitDescAr(String lblUnitDescAr) {
		this.lblUnitDescAr = lblUnitDescAr;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblOldBankEn() {
		return lblOldBankEn;
	}
	public void setLblOldBankEn(String lblOldBankEn) {
		this.lblOldBankEn = lblOldBankEn;
	}
	public String getLblOldBankAr() {
		return lblOldBankAr;
	}
	public void setLblOldBankAr(String lblOldBankAr) {
		this.lblOldBankAr = lblOldBankAr;
	}
	public String getLblRequestNumberEn() {
		return lblRequestNumberEn;
	}
	public void setLblRequestNumberEn(String lblRequestNumberEn) {
		this.lblRequestNumberEn = lblRequestNumberEn;
	}
	public String getLblRequestNumberAr() {
		return lblRequestNumberAr;
	}
	public void setLblRequestNumberAr(String lblRequestNumberAr) {
		this.lblRequestNumberAr = lblRequestNumberAr;
	}
	public String getLblOldChequeNumberEn() {
		return lblOldChequeNumberEn;
	}
	public void setLblOldChequeNumberEn(String lblOldChequeNumberEn) {
		this.lblOldChequeNumberEn = lblOldChequeNumberEn;
	}
	public String getLblOldChequeNumberAr() {
		return lblOldChequeNumberAr;
	}
	public void setLblOldChequeNumberAr(String lblOldChequeNumberAr) {
		this.lblOldChequeNumberAr = lblOldChequeNumberAr;
	}
	public String getOldChequeDateEn() {
		return oldChequeDateEn;
	}
	public void setOldChequeDateEn(String oldChequeDateEn) {
		this.oldChequeDateEn = oldChequeDateEn;
	}
	public String getOldChequeDateAr() {
		return oldChequeDateAr;
	}
	public void setOldChequeDateAr(String oldChequeDateAr) {
		this.oldChequeDateAr = oldChequeDateAr;
	}
	public String getLblOldChequeAmountEn() {
		return lblOldChequeAmountEn;
	}
	public void setLblOldChequeAmountEn(String lblOldChequeAmountEn) {
		this.lblOldChequeAmountEn = lblOldChequeAmountEn;
	}
	public String getLblOldChequeAmountAr() {
		return lblOldChequeAmountAr;
	}
	public void setLblOldChequeAmountAr(String lblOldChequeAmountAr) {
		this.lblOldChequeAmountAr = lblOldChequeAmountAr;
	}
	public String getLblOwnershipTypeEn() {
		return lblOwnershipTypeEn;
	}
	public void setLblOwnershipTypeEn(String lblOwnershipTypeEn) {
		this.lblOwnershipTypeEn = lblOwnershipTypeEn;
	}
	public String getLblOwnershipTypeAr() {
		return lblOwnershipTypeAr;
	}
	public void setLblOwnershipTypeAr(String lblOwnershipTypeAr) {
		this.lblOwnershipTypeAr = lblOwnershipTypeAr;
	}
	
	

}
