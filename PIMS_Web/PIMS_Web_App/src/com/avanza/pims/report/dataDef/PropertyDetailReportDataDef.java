package com.avanza.pims.report.dataDef;

public class PropertyDetailReportDataDef extends AbstractReportDataDef {
	
	private String propertyNumber;
	private String propetyName;
	private String propertyTypeEn;
	private String propertyTypeAr;
	private String floorNumber;
	private String floorName;
	private String floorTypeEn;
	private String floorTypeAr;
	private String address;
	private String totalUnits;
	private String totalSum;
	private String emirate;
	private String unitType;
	private String grandTotal;
	private String numberOfUnits;
	private String floorId;
	private String unitId;
	private String unitUsageTypeId;
	private String unitUsageTypeEn;
	private String unitUsageTypeAr;
	private String ef1;
	private String ef2;
	private String ef3;
	private String ef4;
	private String ef5;
	private String ef6;
	private String ef7;
	private String ef8;
	private String ef9;
	private String ef10;
	private String ef11;
	private String ef12;
	private String ef13;
	private String ef14;
	private String ef15;
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropetyName() {
		return propetyName;
	}
	public void setPropetyName(String propetyName) {
		this.propetyName = propetyName;
	}
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTotalUnits() {
		return totalUnits;
	}
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getTotalSum() {
		return totalSum;
	}
	public void setTotalSum(String totalSum) {
		this.totalSum = totalSum;
	}
	public String getPropertyTypeEn() {
		return propertyTypeEn;
	}
	public void setPropertyTypeEn(String propertyTypeEn) {
		this.propertyTypeEn = propertyTypeEn;
	}
	public String getPropertyTypeAr() {
		return propertyTypeAr;
	}
	public void setPropertyTypeAr(String propertyTypeAr) {
		this.propertyTypeAr = propertyTypeAr;
	}
	public String getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}
	public String getFloorName() {
		return floorName;
	}
	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
	public String getFloorTypeEn() {
		return floorTypeEn;
	}
	public void setFloorTypeEn(String floorTypeEn) {
		this.floorTypeEn = floorTypeEn;
	}
	public String getFloorTypeAr() {
		return floorTypeAr;
	}
	public void setFloorTypeAr(String floorTypeAr) {
		this.floorTypeAr = floorTypeAr;
	}
	public String getNumberOfUnits() {
		return numberOfUnits;
	}
	public void setNumberOfUnits(String numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}
	public String getFloorId() {
		return floorId;
	}
	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getEf1() {
		return ef1;
	}
	public void setEf1(String ef1) {
		this.ef1 = ef1;
	}
	public String getEf2() {
		return ef2;
	}
	public void setEf2(String ef2) {
		this.ef2 = ef2;
	}
	public String getEf3() {
		return ef3;
	}
	public void setEf3(String ef3) {
		this.ef3 = ef3;
	}
	public String getEf4() {
		return ef4;
	}
	public void setEf4(String ef4) {
		this.ef4 = ef4;
	}
	public String getEf5() {
		return ef5;
	}
	public void setEf5(String ef5) {
		this.ef5 = ef5;
	}
	public String getEf6() {
		return ef6;
	}
	public void setEf6(String ef6) {
		this.ef6 = ef6;
	}
	public String getEf7() {
		return ef7;
	}
	public void setEf7(String ef7) {
		this.ef7 = ef7;
	}
	public String getEf8() {
		return ef8;
	}
	public void setEf8(String ef8) {
		this.ef8 = ef8;
	}
	public String getEf9() {
		return ef9;
	}
	public void setEf9(String ef9) {
		this.ef9 = ef9;
	}
	public String getEf10() {
		return ef10;
	}
	public void setEf10(String ef10) {
		this.ef10 = ef10;
	}
	public String getEf11() {
		return ef11;
	}
	public void setEf11(String ef11) {
		this.ef11 = ef11;
	}
	public String getEf12() {
		return ef12;
	}
	public void setEf12(String ef12) {
		this.ef12 = ef12;
	}
	public String getEf13() {
		return ef13;
	}
	public void setEf13(String ef13) {
		this.ef13 = ef13;
	}
	public String getEf14() {
		return ef14;
	}
	public void setEf14(String ef14) {
		this.ef14 = ef14;
	}
	public String getEf15() {
		return ef15;
	}
	public void setEf15(String ef15) {
		this.ef15 = ef15;
	}
	public String getUnitUsageTypeId() {
		return unitUsageTypeId;
	}
	public void setUnitUsageTypeId(String unitUsageTypeId) {
		this.unitUsageTypeId = unitUsageTypeId;
	}
	public String getUnitUsageTypeEn() {
		return unitUsageTypeEn;
	}
	public void setUnitUsageTypeEn(String unitUsageTypeEn) {
		this.unitUsageTypeEn = unitUsageTypeEn;
	}
	public String getUnitUsageTypeAr() {
		return unitUsageTypeAr;
	}
	public void setUnitUsageTypeAr(String unitUsageTypeAr) {
		this.unitUsageTypeAr = unitUsageTypeAr;
	}
	
	
	
	
}
