package com.avanza.pims.report.dataDef;


public class ServiceContractsReportDataDef extends AbstractReportDataDef{
	private String contractNumber;
	private String StatusEn;
	private String StatusAr;
	private String contractorTypeEn;
	private String contractorTypeAr;
	private String contractorName;
	private String fromContractDate;
	private String toContractDate;
	private String propertyName;
	private String propertyOwnershipTypeEn;
	private String propertyOwnershipTypeAr;
	private String contractTypeAr;
	private String contractAmount;
	private String companyName;
	
	
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getStatusEn() {
		return StatusEn;
	}
	public void setStatusEn(String statusEn) {
		StatusEn = statusEn;
	}
	public String getStatusAr() {
		return StatusAr;
	}
	public void setStatusAr(String statusAr) {
		StatusAr = statusAr;
	}
	public String getContractorTypeEn() {
		return contractorTypeEn;
	}
	public void setContractorTypeEn(String contractorTypeEn) {
		this.contractorTypeEn = contractorTypeEn;
	}
	public String getContractorTypeAr() {
		return contractorTypeAr;
	}
	public void setContractorTypeAr(String contractorTypeAr) {
		this.contractorTypeAr = contractorTypeAr;
	}
	public String getContractorName() {
		return contractorName;
	}
	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}
	public String getFromContractDate() {
		return fromContractDate;
	}
	public void setFromContractDate(String fromContractDate) {
		this.fromContractDate = fromContractDate;
	}
	public String getToContractDate() {
		return toContractDate;
	}
	public void setToContractDate(String toContractDate) {
		this.toContractDate = toContractDate;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyOwnershipTypeEn() {
		return propertyOwnershipTypeEn;
	}
	public void setPropertyOwnershipTypeEn(String propertyOwnershipTypeEn) {
		this.propertyOwnershipTypeEn = propertyOwnershipTypeEn;
	}
	public String getPropertyOwnershipTypeAr() {
		return propertyOwnershipTypeAr;
	}
	public void setPropertyOwnershipTypeAr(String propertyOwnershipTypeAr) {
		this.propertyOwnershipTypeAr = propertyOwnershipTypeAr;
	}
	public String getContractTypeAr() {
		return contractTypeAr;
	}
	public void setContractTypeAr(String contractTypeAr) {
		this.contractTypeAr = contractTypeAr;
	}
	

	public String getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}
