package com.avanza.pims.report.dataDef;

public class ChequeWithdrawlReportDataDef extends AbstractReportDataDef
{
	private	String bankRemitanceAccName;
	private String grpAccNumber;
	private String bankRemitanceAccNumber;
	private String chequeDate;
	private String chequeAmount;
	private String chequeNumber;
	private String tenantAccountNumber;
	private String bankName;
	private String chequeOwner;
	private String amafRepresentative;
	private String palaceHolder1;
	private String palaceHolder2;
	private String palaceHolder3;
	private String palaceHolder4;
	
	public String getBankRemitanceAccName() {
		return bankRemitanceAccName;
	}
	public void setBankRemitanceAccName(String bankRemitanceAccName) {
		this.bankRemitanceAccName = bankRemitanceAccName;
	}
	public String getBankRemitanceAccNumber() {
		return bankRemitanceAccNumber;
	}
	public void setBankRemitanceAccNumber(String bankRemitanceAccNumber) {
		this.bankRemitanceAccNumber = bankRemitanceAccNumber;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getTenantAccountNumber() {
		return tenantAccountNumber;
	}
	public void setTenantAccountNumber(String tenantAccountNumber) {
		this.tenantAccountNumber = tenantAccountNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeOwner() {
		return chequeOwner;
	}
	public void setChequeOwner(String chequeOwner) {
		this.chequeOwner = chequeOwner;
	}
	public String getAmafRepresentative() {
		return amafRepresentative;
	}
	public void setAmafRepresentative(String amafRepresentative) {
		this.amafRepresentative = amafRepresentative;
	}
	public String getPalaceHolder1() {
		return palaceHolder1;
	}
	public void setPalaceHolder1(String palaceHolder1) {
		this.palaceHolder1 = palaceHolder1;
	}
	public String getPalaceHolder2() {
		return palaceHolder2;
	}
	public void setPalaceHolder2(String palaceHolder2) {
		this.palaceHolder2 = palaceHolder2;
	}
	public String getPalaceHolder3() {
		return palaceHolder3;
	}
	public void setPalaceHolder3(String palaceHolder3) {
		this.palaceHolder3 = palaceHolder3;
	}
	public String getPalaceHolder4() {
		return palaceHolder4;
	}
	public void setPalaceHolder4(String palaceHolder4) {
		this.palaceHolder4 = palaceHolder4;
	}
	public String getGrpAccNumber() {
		return grpAccNumber;
	}
	public void setGrpAccNumber(String grpAccNumber) {
		this.grpAccNumber = grpAccNumber;
	}
}
