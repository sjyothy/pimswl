package com.avanza.pims.report.dataDef;

public class ConstructionProjectsSummaryReportDataDef extends AbstractReportDataDef{
	private String projectNumber;
	private String projectStatusEn;
	private String projectStatusAr;
	private String projectName;
	private String projectTypeEn;
	private String projectTypeAr;
	private String completionPercentage;
	private String achievedMilestone;
	
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectStatusEn() {
		return projectStatusEn;
	}
	public void setProjectStatusEn(String projectStatusEn) {
		this.projectStatusEn = projectStatusEn;
	}
	public String getProjectStatusAr() {
		return projectStatusAr;
	}
	public void setProjectStatusAr(String projectStatusAr) {
		this.projectStatusAr = projectStatusAr;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectTypeEn() {
		return projectTypeEn;
	}
	public void setProjectTypeEn(String projectTypeEn) {
		this.projectTypeEn = projectTypeEn;
	}
	public String getProjectTypeAr() {
		return projectTypeAr;
	}
	public void setProjectTypeAr(String projectTypeAr) {
		this.projectTypeAr = projectTypeAr;
	}
	public String getCompletionPercentage() {
		return completionPercentage;
	}
	public void setCompletionPercentage(String completionPercentage) {
		this.completionPercentage = completionPercentage;
	}
	public String getAchievedMilestone() {
		return achievedMilestone;
	}
	public void setAchievedMilestone(String achievedMilestone) {
		this.achievedMilestone = achievedMilestone;
	}

	

}
