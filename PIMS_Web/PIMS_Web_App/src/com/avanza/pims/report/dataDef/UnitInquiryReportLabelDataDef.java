package com.avanza.pims.report.dataDef;

public class UnitInquiryReportLabelDataDef extends AbstractReportDataDef 
{
	///////English Label///////////
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblUnitTypeEn;
	private String lblUsageEn;
	private String lblNoOfBedRoomsEn;
	private String lblNoOfBathRoomsEn;
	private String lblRentValueEn;
	private String lblFloorNumberEn;
	private String lblUnitStatusEn;
	private String lblUnitSideEn;
	private String lblUnitDescriptionEn;
	private String lblNoOfLivingRoomsEn;
	private String lblEwUtilityNoEn;
	private String lblRemarksEn;
	///////Arabic Label///////////
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblUnitTypeAr;
	private String lblUsageAr;
	private String lblNoOfBedRoomsAr;
	private String lblNoOfBathRoomsAr;
	private String lblRentValueAr;
	private String lblFloorNumberAr;
	private String lblUnitStatusAr;
	private String lblUnitSideAr;
	private String lblUnitDescriptionAr;
	private String lblNoOfLivingRoomsAr;
	private String lblEwUtilityNoAr;
	private String lblRemarksAr;

////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblUnitTypeEn() {
		return lblUnitTypeEn;
	}
	public void setLblUnitTypeEn(String lblUnitTypeEn) {
		this.lblUnitTypeEn = lblUnitTypeEn;
	}
	public String getLblUsageEn() {
		return lblUsageEn;
	}
	public void setLblUsageEn(String lblUsageEn) {
		this.lblUsageEn = lblUsageEn;
	}
	public String getLblNoOfBedRoomsEn() {
		return lblNoOfBedRoomsEn;
	}
	public void setLblNoOfBedRoomsEn(String lblNoOfBedRoomsEn) {
		this.lblNoOfBedRoomsEn = lblNoOfBedRoomsEn;
	}
	public String getLblNoOfBathRoomsEn() {
		return lblNoOfBathRoomsEn;
	}
	public void setLblNoOfBathRoomsEn(String lblNoOfBathRoomsEn) {
		this.lblNoOfBathRoomsEn = lblNoOfBathRoomsEn;
	}
	public String getLblRentValueEn() {
		return lblRentValueEn;
	}
	public void setLblRentValueEn(String lblRentValueEn) {
		this.lblRentValueEn = lblRentValueEn;
	}
	public String getLblFloorNumberEn() {
		return lblFloorNumberEn;
	}
	public void setLblFloorNumberEn(String lblFloorNumberEn) {
		this.lblFloorNumberEn = lblFloorNumberEn;
	}
	public String getLblUnitStatusEn() {
		return lblUnitStatusEn;
	}
	public void setLblUnitStatusEn(String lblUnitStatusEn) {
		this.lblUnitStatusEn = lblUnitStatusEn;
	}
	public String getLblUnitSideEn() {
		return lblUnitSideEn;
	}
	public void setLblUnitSideEn(String lblUnitSideEn) {
		this.lblUnitSideEn = lblUnitSideEn;
	}
	public String getLblUnitDescriptionEn() {
		return lblUnitDescriptionEn;
	}
	public void setLblUnitDescriptionEn(String lblUnitDescriptionEn) {
		this.lblUnitDescriptionEn = lblUnitDescriptionEn;
	}
	public String getLblNoOfLivingRoomsEn() {
		return lblNoOfLivingRoomsEn;
	}
	public void setLblNoOfLivingRoomsEn(String lblNoOfLivingRoomsEn) {
		this.lblNoOfLivingRoomsEn = lblNoOfLivingRoomsEn;
	}
	public String getLblEwUtilityNoEn() {
		return lblEwUtilityNoEn;
	}
	public void setLblEwUtilityNoEn(String lblEwUtilityNoEn) {
		this.lblEwUtilityNoEn = lblEwUtilityNoEn;
	}
	public String getLblRemarksEn() {
		return lblRemarksEn;
	}
	public void setLblRemarksEn(String lblRemarksEn) {
		this.lblRemarksEn = lblRemarksEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblUnitTypeAr() {
		return lblUnitTypeAr;
	}
	public void setLblUnitTypeAr(String lblUnitTypeAr) {
		this.lblUnitTypeAr = lblUnitTypeAr;
	}
	public String getLblUsageAr() {
		return lblUsageAr;
	}
	public void setLblUsageAr(String lblUsageAr) {
		this.lblUsageAr = lblUsageAr;
	}
	public String getLblNoOfBedRoomsAr() {
		return lblNoOfBedRoomsAr;
	}
	public void setLblNoOfBedRoomsAr(String lblNoOfBedRoomsAr) {
		this.lblNoOfBedRoomsAr = lblNoOfBedRoomsAr;
	}
	public String getLblNoOfBathRoomsAr() {
		return lblNoOfBathRoomsAr;
	}
	public void setLblNoOfBathRoomsAr(String lblNoOfBathRoomsAr) {
		this.lblNoOfBathRoomsAr = lblNoOfBathRoomsAr;
	}
	public String getLblRentValueAr() {
		return lblRentValueAr;
	}
	public void setLblRentValueAr(String lblRentValueAr) {
		this.lblRentValueAr = lblRentValueAr;
	}
	public String getLblFloorNumberAr() {
		return lblFloorNumberAr;
	}
	public void setLblFloorNumberAr(String lblFloorNumberAr) {
		this.lblFloorNumberAr = lblFloorNumberAr;
	}
	public String getLblUnitStatusAr() {
		return lblUnitStatusAr;
	}
	public void setLblUnitStatusAr(String lblUnitStatusAr) {
		this.lblUnitStatusAr = lblUnitStatusAr;
	}
	public String getLblUnitSideAr() {
		return lblUnitSideAr;
	}
	public void setLblUnitSideAr(String lblUnitSideAr) {
		this.lblUnitSideAr = lblUnitSideAr;
	}
	public String getLblUnitDescriptionAr() {
		return lblUnitDescriptionAr;
	}
	public void setLblUnitDescriptionAr(String lblUnitDescriptionAr) {
		this.lblUnitDescriptionAr = lblUnitDescriptionAr;
	}
	public String getLblNoOfLivingRoomsAr() {
		return lblNoOfLivingRoomsAr;
	}
	public void setLblNoOfLivingRoomsAr(String lblNoOfLivingRoomsAr) {
		this.lblNoOfLivingRoomsAr = lblNoOfLivingRoomsAr;
	}
	public String getLblEwUtilityNoAr() {
		return lblEwUtilityNoAr;
	}
	public void setLblEwUtilityNoAr(String lblEwUtilityNoAr) {
		this.lblEwUtilityNoAr = lblEwUtilityNoAr;
	}
	public String getLblRemarksAr() {
		return lblRemarksAr;
	}
	public void setLblRemarksAr(String lblRemarksAr) {
		this.lblRemarksAr = lblRemarksAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	
}
