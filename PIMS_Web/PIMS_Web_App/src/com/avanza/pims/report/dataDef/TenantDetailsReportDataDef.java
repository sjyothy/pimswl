package com.avanza.pims.report.dataDef;

public class TenantDetailsReportDataDef extends AbstractReportDataDef
{

	private Integer refTenantId;
	private String refTitle;
	private String refName;
	private String refRelationship;
	private String refRemarks;
	private String refNationality;
	private String refCellNumber;
	private String refStreet;
	private String retPostalCode;
	private String refState;
	private String refCountry;
	private String refCity;
	private String refHomePhone;
	private String refOfficePhone;
	private String refFax;
	private String refEmail;
	private String refMobileNumber;
	private String refDesignation;
	
	private String refExtra1;
	private String refExtra2;
	private String refExtra3;
	private String refExtra4;
	private String refExtra5;
	private String refExtra6;
	private String refExtra7;
	private String refExtra8;
	private String refExtra9;
	private String refExtra10;
	
	public Integer getRefTenantId() {
		return refTenantId;
	}
	public void setRefTenantId(Integer refTenantId) {
		this.refTenantId = refTenantId;
	}
	public String getRefTitle() {
		return refTitle;
	}
	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}
	public String getRefName() {
		return refName;
	}
	public void setRefName(String refName) {
		this.refName = refName;
	}
	public String getRefRelationship() {
		return refRelationship;
	}
	public void setRefRelationship(String refRelationship) {
		this.refRelationship = refRelationship;
	}
	public String getRefRemarks() {
		return refRemarks;
	}
	public void setRefRemarks(String refRemarks) {
		this.refRemarks = refRemarks;
	}
	public String getRefNationality() {
		return refNationality;
	}
	public void setRefNationality(String refNationality) {
		this.refNationality = refNationality;
	}
	public String getRefCellNumber() {
		return refCellNumber;
	}
	public void setRefCellNumber(String refCellNumber) {
		this.refCellNumber = refCellNumber;
	}
	public String getRefStreet() {
		return refStreet;
	}
	public void setRefStreet(String refStreet) {
		this.refStreet = refStreet;
	}
	public String getRetPostalCode() {
		return retPostalCode;
	}
	public void setRetPostalCode(String retPostalCode) {
		this.retPostalCode = retPostalCode;
	}
	public String getRefState() {
		return refState;
	}
	public void setRefState(String refState) {
		this.refState = refState;
	}
	public String getRefCountry() {
		return refCountry;
	}
	public void setRefCountry(String refCountry) {
		this.refCountry = refCountry;
	}
	public String getRefHomePhone() {
		return refHomePhone;
	}
	public void setRefHomePhone(String refHomePhone) {
		this.refHomePhone = refHomePhone;
	}
	public String getRefOfficePhone() {
		return refOfficePhone;
	}
	public void setRefOfficePhone(String refOfficePhone) {
		this.refOfficePhone = refOfficePhone;
	}
	public String getRefFax() {
		return refFax;
	}
	public void setRefFax(String refFax) {
		this.refFax = refFax;
	}
	public String getRefEmail() {
		return refEmail;
	}
	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}
	public String getRefMobileNumber() {
		return refMobileNumber;
	}
	public void setRefMobileNumber(String refMobileNumber) {
		this.refMobileNumber = refMobileNumber;
	}
	public String getRefDesignation() {
		return refDesignation;
	}
	public void setRefDesignation(String refDesignation) {
		this.refDesignation = refDesignation;
	}
	public String getRefExtra1() {
		return refExtra1;
	}
	public void setRefExtra1(String refExtra1) {
		this.refExtra1 = refExtra1;
	}
	public String getRefExtra2() {
		return refExtra2;
	}
	public void setRefExtra2(String refExtra2) {
		this.refExtra2 = refExtra2;
	}
	public String getRefExtra3() {
		return refExtra3;
	}
	public void setRefExtra3(String refExtra3) {
		this.refExtra3 = refExtra3;
	}
	public String getRefExtra4() {
		return refExtra4;
	}
	public void setRefExtra4(String refExtra4) {
		this.refExtra4 = refExtra4;
	}
	public String getRefExtra5() {
		return refExtra5;
	}
	public void setRefExtra5(String refExtra5) {
		this.refExtra5 = refExtra5;
	}
	public String getRefExtra6() {
		return refExtra6;
	}
	public void setRefExtra6(String refExtra6) {
		this.refExtra6 = refExtra6;
	}
	public String getRefExtra7() {
		return refExtra7;
	}
	public void setRefExtra7(String refExtra7) {
		this.refExtra7 = refExtra7;
	}
	public String getRefExtra8() {
		return refExtra8;
	}
	public void setRefExtra8(String refExtra8) {
		this.refExtra8 = refExtra8;
	}
	public String getRefExtra9() {
		return refExtra9;
	}
	public void setRefExtra9(String refExtra9) {
		this.refExtra9 = refExtra9;
	}
	public String getRefExtra10() {
		return refExtra10;
	}
	public void setRefExtra10(String refExtra10) {
		this.refExtra10 = refExtra10;
	}
	public String getRefCity() {
		return refCity;
	}
	public void setRefCity(String refCity) {
		this.refCity = refCity;
	}
	
}
	