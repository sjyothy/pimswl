package com.avanza.pims.report.dataDef;

public class PaymentReceiptReportDataDef extends AbstractReportDataDef{

	private String paymentNumber;
	private String paymentType;
	private String psAmount;
	private String receiptNumber;
	private String contractNumber;
	private String customerName;
	private String description;
	private String period;
	private String receiptDate;
	private String receiptTotalAmount;
	private String amount;
	private String chequeNumber;
	private String bank;
	private String chequeDate;
	private String costCenter;
	private String customerNumber;
    private String paymentScheduleId;
    private String unitDesc;
    private String requestNumber;
    private String paymentMethod;
    private String oldBank;
    private String oldChequeNumber;
    private String oldChequeDate;
    private String oldChequeAmount;
    private String ownershipTypeEn;
    private String ownershipTypeAr;
    private String requestid;	
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getReceiptTotalAmount() {
		return receiptTotalAmount;
	}
	public void setReceiptTotalAmount(String receiptTotalAmount) {
		this.receiptTotalAmount = receiptTotalAmount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPsAmount() {
		return psAmount;
	}
	public void setPsAmount(String psAmount) {
		this.psAmount = psAmount;
	}
	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}
	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}
	public String getUnitDesc() {
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getOldBank() {
		return oldBank;
	}
	public void setOldBank(String oldBank) {
		this.oldBank = oldBank;
	}
	public String getOldChequeNumber() {
		return oldChequeNumber;
	}
	public void setOldChequeNumber(String oldChequeNumber) {
		this.oldChequeNumber = oldChequeNumber;
	}
	public String getOldChequeDate() {
		return oldChequeDate;
	}
	public void setOldChequeDate(String oldChequeDate) {
		this.oldChequeDate = oldChequeDate;
	}
	public String getOldChequeAmount() {
		return oldChequeAmount;
	}
	public void setOldChequeAmount(String oldChequeAmount) {
		this.oldChequeAmount = oldChequeAmount;
	}
	public String getOwnershipTypeEn() {
		return ownershipTypeEn;
	}
	public void setOwnershipTypeEn(String ownershipTypeEn) {
		this.ownershipTypeEn = ownershipTypeEn;
	}
	public String getOwnershipTypeAr() {
		return ownershipTypeAr;
	}
	public void setOwnershipTypeAr(String ownershipTypeAr) {
		this.ownershipTypeAr = ownershipTypeAr;
	}
	public String getRequestid() {
		return requestid;
	}
	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

}
