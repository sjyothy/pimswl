package com.avanza.pims.report.dataDef;

public class TenantStatementReportLabelDataDef extends AbstractReportDataDef {

	/*******************************                           English Labels             *********************************/

	private String lblTenantNameEn;
	private String lblContractNumberEn;
	private String lblContractStartDateEn;
	private String lblContractEndDateEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblContractRentValueEn;
	
	private String lblReceiptNumberEn;
	private String lblPaymentTypeEn;
	private String lblReceiptAmountEn;
	private String lblStatusEn;
	private String lblPaymentEn;
	private String lblPaymentMethodEn;	
	private String lblReceiptTransactionDateEn;	
	private String lblTenantIdEn;
	
	/*******************************                           Arabic Labels             *********************************/
	
	private String lblTenantIdAr;
	private String lblTenantNameAr;
	private String lblContractNumberAr;
	private String lblContractStartDateAr;
	private String lblContractEndDateAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblContractRentValueAr;
	
	private String lblReceiptNumberAr;
	private String lblPaymentTypeAr;
	private String lblReceiptAmountAr;
	private String lblStatusAr;
	private String lblPaymentAr;
	private String lblPaymentMethodAr;	
	private String lblReceiptTransactionDateAr;	

	/*******************************                           Mandatory Labels             *********************************/
	private String lblTotalOutstandingAmountEn;
	private String lblTotalReceiptAmountEn;
	private String lblTotalClearedAmountEn;
	private String lblTotalBalanceAmountEn;

	private String lblTotalOutstandingAmountAr;
	private String lblTotalReceiptAmountAr;
	private String lblTotalClearedAmountAr;
	private String lblTotalBalanceAmountAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	

	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractRentValueEn() {
		return lblContractRentValueEn;
	}
	public void setLblContractRentValueEn(String lblContractRentValueEn) {
		this.lblContractRentValueEn = lblContractRentValueEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblReceiptAmountEn() {
		return lblReceiptAmountEn;
	}
	public void setLblReceiptAmountEn(String lblReceiptAmountEn) {
		this.lblReceiptAmountEn = lblReceiptAmountEn;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblTotalOutstandingAmountEn() {
		return lblTotalOutstandingAmountEn;
	}
	public void setLblTotalOutstandingAmountEn(String lblTotalOutstandingAmountEn) {
		this.lblTotalOutstandingAmountEn = lblTotalOutstandingAmountEn;
	}
	public String getLblTotalReceiptAmountEn() {
		return lblTotalReceiptAmountEn;
	}
	public void setLblTotalReceiptAmountEn(String lblTotalReceiptAmountEn) {
		this.lblTotalReceiptAmountEn = lblTotalReceiptAmountEn;
	}
	public String getLblTotalClearedAmountEn() {
		return lblTotalClearedAmountEn;
	}
	public void setLblTotalClearedAmountEn(String lblTotalClearedAmountEn) {
		this.lblTotalClearedAmountEn = lblTotalClearedAmountEn;
	}
	public String getLblTotalBalanceAmountEn() {
		return lblTotalBalanceAmountEn;
	}
	public void setLblTotalBalanceAmountEn(String lblTotalBalanceAmountEn) {
		this.lblTotalBalanceAmountEn = lblTotalBalanceAmountEn;
	}
	public String getLblPaymentEn() {
		return lblPaymentEn;
	}
	public void setLblPaymentEn(String lblPaymentEn) {
		this.lblPaymentEn = lblPaymentEn;
	}
	public String getLblTenantIdAr() {
		return lblTenantIdAr;
	}
	
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblReceiptTransactionDateEn() {
		return lblReceiptTransactionDateEn;
	}
	public void setLblReceiptTransactionDateEn(String lblReceiptTransactionDateEn) {
		this.lblReceiptTransactionDateEn = lblReceiptTransactionDateEn;
	}
	public void setLblTenantIdAr(String lblTenantIdAr) {
		this.lblTenantIdAr = lblTenantIdAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractRentValueAr() {
		return lblContractRentValueAr;
	}
	public void setLblContractRentValueAr(String lblContractRentValueAr) {
		this.lblContractRentValueAr = lblContractRentValueAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblReceiptAmountAr() {
		return lblReceiptAmountAr;
	}
	public void setLblReceiptAmountAr(String lblReceiptAmountAr) {
		this.lblReceiptAmountAr = lblReceiptAmountAr;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblTotalOutstandingAmountAr() {
		return lblTotalOutstandingAmountAr;
	}
	public void setLblTotalOutstandingAmountAr(String lblTotalOutstandingAmountAr) {
		this.lblTotalOutstandingAmountAr = lblTotalOutstandingAmountAr;
	}
	public String getLblTotalReceiptAmountAr() {
		return lblTotalReceiptAmountAr;
	}
	public void setLblTotalReceiptAmountAr(String lblTotalReceiptAmountAr) {
		this.lblTotalReceiptAmountAr = lblTotalReceiptAmountAr;
	}
	public String getLblTotalClearedAmountAr() {
		return lblTotalClearedAmountAr;
	}
	public void setLblTotalClearedAmountAr(String lblTotalClearedAmountAr) {
		this.lblTotalClearedAmountAr = lblTotalClearedAmountAr;
	}
	public String getLblTotalBalanceAmountAr() {
		return lblTotalBalanceAmountAr;
	}
	public void setLblTotalBalanceAmountAr(String lblTotalBalanceAmountAr) {
		this.lblTotalBalanceAmountAr = lblTotalBalanceAmountAr;
	}
	public String getLblPaymentAr() {
		return lblPaymentAr;
	}
	public void setLblPaymentAr(String lblPaymentAr) {
		this.lblPaymentAr = lblPaymentAr;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblReceiptTransactionDateAr() {
		return lblReceiptTransactionDateAr;
	}
	public void setLblReceiptTransactionDateAr(String lblReceiptTransactionDateAr) {
		this.lblReceiptTransactionDateAr = lblReceiptTransactionDateAr;
	}
	public String getLblTenantIdEn() {
		return lblTenantIdEn;
	}
	public void setLblTenantIdEn(String lblTenantIdEn) {
		this.lblTenantIdEn = lblTenantIdEn;
	}
	
	
}
