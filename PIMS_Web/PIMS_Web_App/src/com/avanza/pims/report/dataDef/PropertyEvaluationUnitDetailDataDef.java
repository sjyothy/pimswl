package com.avanza.pims.report.dataDef;

public class PropertyEvaluationUnitDetailDataDef extends AbstractReportDataDef {
	
	private String unitTypeId;
	
	private String newRentValue;
	private String unitTypeAR;
	private String toArea;
	private String fromArea;
	private String frontSide;
	
	public String getNewRentValue() {
		return newRentValue;
	}
	public void setNewRentValue(String newRentValue) {
		this.newRentValue = newRentValue;
	}
	public String getUnitTypeAR() {
		return unitTypeAR;
	}
	public void setUnitTypeAR(String unitTypeAR) {
		this.unitTypeAR = unitTypeAR;
	}
	public String getToArea() {
		return toArea;
	}
	public void setToArea(String toArea) {
		this.toArea = toArea;
	}
	public String getFromArea() {
		return fromArea;
	}
	public void setFromArea(String fromArea) {
		this.fromArea = fromArea;
	}
	public String getFrontSide() {
		return frontSide;
	}
	public void setFrontSide(String frontSide) {
		this.frontSide = frontSide;
	}
	public String getUnitTypeId() {
		return unitTypeId;
	}
	public void setUnitTypeId(String unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

}
