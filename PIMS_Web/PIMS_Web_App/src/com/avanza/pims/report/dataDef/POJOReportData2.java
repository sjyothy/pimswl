package com.avanza.pims.report.dataDef;

public class POJOReportData2 
{	
	private String innerData;

	public POJOReportData2(String innerData) 
	{
		this.innerData = innerData;
	}

	public String getInnerData() {
		return innerData;
	}

	public void setInnerData(String innerData) {
		this.innerData = innerData;
	}
}
