package com.avanza.pims.report.dataDef;

public class MaintenanceContractsReportLabelDataDef extends AbstractReportDataDef{

	/*******************************                           English Labels             *********************************/

	private String lblContractNumberEn;
	private String lblMaintenanceworkTypeEn;
	private String lblIssueDateEn;
	private String lblContractorNameEn;
	private String lblContractStatusEn;
	private String lblWorkScopeEn;

	/*******************************                           Arabic Labels             *********************************/

	private String lblContractNumberAr;
	private String lblMmaintenanceworkTypeAr;
	private String lblIssueDateAr;
	private String lblContractorNameAr;
	private String lblContractStatusAr;
	private String lblWorkScopeAr;

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblMaintenanceworkTypeEn() {
		return lblMaintenanceworkTypeEn;
	}
	public void setLblMaintenanceworkTypeEn(String lblMaintenanceworkTypeEn) {
		this.lblMaintenanceworkTypeEn = lblMaintenanceworkTypeEn;
	}
	public String getLblIssueDateEn() {
		return lblIssueDateEn;
	}
	public void setLblIssueDateEn(String lblIssueDateEn) {
		this.lblIssueDateEn = lblIssueDateEn;
	}
	public String getLblContractorNameEn() {
		return lblContractorNameEn;
	}
	public void setLblContractorNameEn(String lblContractorNameEn) {
		this.lblContractorNameEn = lblContractorNameEn;
	}
	public String getLblContractStatusEn() {
		return lblContractStatusEn;
	}
	public void setLblContractStatusEn(String lblContractStatusEn) {
		this.lblContractStatusEn = lblContractStatusEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblMmaintenanceworkTypeAr() {
		return lblMmaintenanceworkTypeAr;
	}
	public void setLblMmaintenanceworkTypeAr(String lblMmaintenanceworkTypeAr) {
		this.lblMmaintenanceworkTypeAr = lblMmaintenanceworkTypeAr;
	}
	public String getLblIssueDateAr() {
		return lblIssueDateAr;
	}
	public void setLblIssueDateAr(String lblIssueDateAr) {
		this.lblIssueDateAr = lblIssueDateAr;
	}
	public String getLblContractorNameAr() {
		return lblContractorNameAr;
	}
	public void setLblContractorNameAr(String lblContractorNameAr) {
		this.lblContractorNameAr = lblContractorNameAr;
	}
	public String getLblContractStatusAr() {
		return lblContractStatusAr;
	}
	public void setLblContractStatusAr(String lblContractStatusAr) {
		this.lblContractStatusAr = lblContractStatusAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblWorkScopeEn() {
		return lblWorkScopeEn;
	}
	public void setLblWorkScopeEn(String lblWorkScopeEn) {
		this.lblWorkScopeEn = lblWorkScopeEn;
	}
	public String getLblWorkScopeAr() {
		return lblWorkScopeAr;
	}
	public void setLblWorkScopeAr(String lblWorkScopeAr) {
		this.lblWorkScopeAr = lblWorkScopeAr;
	}
	
	

}
