package com.avanza.pims.report.dataDef;

public class TenantsByContractsReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyTypeEn;
	private String lblPropertyNameEn;
	private String lblEmirateEn;
	private String lblTenantNameEn;
	private String lblIssueDateEn;
	private String lblExpiryDateEn;
	private String lblContractNumberEn;
	private String lblContractValueEn; 
	private String lblRentalIncomeEn;
	private String lblRentAnnualEn;
	private String lblTotalRentalIncomeEn;
	private String lblAddressEn;
	
	/////////Arabic Label/////////
	private String lblPropertyNumberAr;
	private String lblPropertyTypeAr;
	private String lblPropertyNameAr;
	private String lblEmirateAr;
	private String lblTenantNameAr;
	private String lblIssueDateAr;
	private String lblExpiryDateAr;
	private String lblContractNumberAr;
	private String lblContractValueAr; 
	private String lblRentalIncomeAr;
	private String lblRentAnnualAr;
	private String lblTotalRentalIncomeAr;
	private String lblAddressAr;
	
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	//////////////////////
	public String getLblPropertyNumberEn() {
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblIssueDateEn() {
		return lblIssueDateEn;
	}
	public void setLblIssueDateEn(String lblIssueDateEn) {
		this.lblIssueDateEn = lblIssueDateEn;
	}
	public String getLblExpiryDateEn() {
		return lblExpiryDateEn;
	}
	public void setLblExpiryDateEn(String lblExpiryDateEn) {
		this.lblExpiryDateEn = lblExpiryDateEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractValueEn() {
		return lblContractValueEn;
	}
	public void setLblContractValueEn(String lblContractValueEn) {
		this.lblContractValueEn = lblContractValueEn;
	}
	public String getLblRentalIncomeEn() {
		return lblRentalIncomeEn;
	}
	public void setLblRentalIncomeEn(String lblRentalIncomeEn) {
		this.lblRentalIncomeEn = lblRentalIncomeEn;
	}
	public String getLblRentAnnualEn() {
		return lblRentAnnualEn;
	}
	public void setLblRentAnnualEn(String lblRentAnnualEn) {
		this.lblRentAnnualEn = lblRentAnnualEn;
	}
	public String getLblTotalRentalIncomeEn() {
		return lblTotalRentalIncomeEn;
	}
	public void setLblTotalRentalIncomeEn(String lblTotalRentalIncomeEn) {
		this.lblTotalRentalIncomeEn = lblTotalRentalIncomeEn;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblEmirateAr() {
		return lblEmirateAr;
	}
	public void setLblEmirateAr(String lblEmirateAr) {
		this.lblEmirateAr = lblEmirateAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblIssueDateAr() {
		return lblIssueDateAr;
	}
	public void setLblIssueDateAr(String lblIssueDateAr) {
		this.lblIssueDateAr = lblIssueDateAr;
	}
	public String getLblExpiryDateAr() {
		return lblExpiryDateAr;
	}
	public void setLblExpiryDateAr(String lblExpiryDateAr) {
		this.lblExpiryDateAr = lblExpiryDateAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractValueAr() {
		return lblContractValueAr;
	}
	public void setLblContractValueAr(String lblContractValueAr) {
		this.lblContractValueAr = lblContractValueAr;
	}
	public String getLblRentalIncomeAr() {
		return lblRentalIncomeAr;
	}
	public void setLblRentalIncomeAr(String lblRentalIncomeAr) {
		this.lblRentalIncomeAr = lblRentalIncomeAr;
	}
	public String getLblRentAnnualAr() {
		return lblRentAnnualAr;
	}
	public void setLblRentAnnualAr(String lblRentAnnualAr) {
		this.lblRentAnnualAr = lblRentAnnualAr;
	}
	public String getLblTotalRentalIncomeAr() {
		return lblTotalRentalIncomeAr;
	}
	public void setLblTotalRentalIncomeAr(String lblTotalRentalIncomeAr) {
		this.lblTotalRentalIncomeAr = lblTotalRentalIncomeAr;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	
}
