package com.avanza.pims.report.dataDef;

import java.util.Date;

public class FileDetailsReportDataDef extends AbstractReportDataDef
{
	   private Long fileId; 
	   private String fileNumber; 
	   private Long ownerId; 
	   private Long researcherId; 
	   private Long sponsorId; 
	   private Long fileTypeIs;//this is ID not Is 
	   private Long fileStatus; 
	   private Date missingFromDate; 
	   private String rejectionReason;
	   private String fileDate;
	   private Long createdById;
	   private String fileTypeEn;
	   private String fileTypeAr;
	   private String fileStatusEn;
	   private String fileStatusAr;
	   private String ownerAccNumber;
	   private String researcherNameEn;
	   private String researcherNameAr;
	   private String creatorNameEn;
	   private String creatorNameAr;
	   private String sponsorName;
	   private String ownerName;
	   private String filed1;
	   private String filed2;
	   private String filed3;
	   private String filed4;
	   private String filed5;
	   private String filed6;
	   private String filed7;
	   private String filed8;
	   private String filed9;
	   private String filed10;
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Long getResearcherId() {
		return researcherId;
	}
	public void setResearcherId(Long researcherId) {
		this.researcherId = researcherId;
	}
	public Long getSponsorId() {
		return sponsorId;
	}
	public void setSponsorId(Long sponsorId) {
		this.sponsorId = sponsorId;
	}
	public Long getFileTypeIs() {
		return fileTypeIs;
	}
	public void setFileTypeIs(Long fileTypeIs) {
		this.fileTypeIs = fileTypeIs;
	}
	public Long getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(Long fileStatus) {
		this.fileStatus = fileStatus;
	}
	public Date getMissingFromDate() {
		return missingFromDate;
	}
	public void setMissingFromDate(Date missingFromDate) {
		this.missingFromDate = missingFromDate;
	}
	public String getRejectionReason() {
		return rejectionReason;
	}
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
	public Long getCreatedById() {
		return createdById;
	}
	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}
	public String getFileTypeEn() {
		return fileTypeEn;
	}
	public void setFileTypeEn(String fileTypeEn) {
		this.fileTypeEn = fileTypeEn;
	}
	public String getFileTypeAr() {
		return fileTypeAr;
	}
	public void setFileTypeAr(String fileTypeAr) {
		this.fileTypeAr = fileTypeAr;
	}
	public String getFileStatusEn() {
		return fileStatusEn;
	}
	public void setFileStatusEn(String fileStatusEn) {
		this.fileStatusEn = fileStatusEn;
	}
	public String getFileStatusAr() {
		return fileStatusAr;
	}
	public void setFileStatusAr(String fileStatusAr) {
		this.fileStatusAr = fileStatusAr;
	}
	public String getOwnerAccNumber() {
		return ownerAccNumber;
	}
	public void setOwnerAccNumber(String ownerAccNumber) {
		this.ownerAccNumber = ownerAccNumber;
	}
	public String getResearcherNameEn() {
		return researcherNameEn;
	}
	public void setResearcherNameEn(String researcherNameEn) {
		this.researcherNameEn = researcherNameEn;
	}
	public String getResearcherNameAr() {
		return researcherNameAr;
	}
	public void setResearcherNameAr(String researcherNameAr) {
		this.researcherNameAr = researcherNameAr;
	}
	public String getCreatorNameEn() {
		return creatorNameEn;
	}
	public void setCreatorNameEn(String creatorNameEn) {
		this.creatorNameEn = creatorNameEn;
	}
	public String getCreatorNameAr() {
		return creatorNameAr;
	}
	public void setCreatorNameAr(String creatorNameAr) {
		this.creatorNameAr = creatorNameAr;
	}
	public String getSponsorName() {
		return sponsorName;
	}
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getFiled1() {
		return filed1;
	}
	public void setFiled1(String filed1) {
		this.filed1 = filed1;
	}
	public String getFiled2() {
		return filed2;
	}
	public void setFiled2(String filed2) {
		this.filed2 = filed2;
	}
	public String getFiled3() {
		return filed3;
	}
	public void setFiled3(String filed3) {
		this.filed3 = filed3;
	}
	public String getFiled4() {
		return filed4;
	}
	public void setFiled4(String filed4) {
		this.filed4 = filed4;
	}
	public String getFiled5() {
		return filed5;
	}
	public void setFiled5(String filed5) {
		this.filed5 = filed5;
	}
	public String getFiled6() {
		return filed6;
	}
	public void setFiled6(String filed6) {
		this.filed6 = filed6;
	}
	public String getFiled7() {
		return filed7;
	}
	public void setFiled7(String filed7) {
		this.filed7 = filed7;
	}
	public String getFiled8() {
		return filed8;
	}
	public void setFiled8(String filed8) {
		this.filed8 = filed8;
	}
	public String getFiled9() {
		return filed9;
	}
	public void setFiled9(String filed9) {
		this.filed9 = filed9;
	}
	public String getFiled10() {
		return filed10;
	}
	public void setFiled10(String filed10) {
		this.filed10 = filed10;
	}
	public String getFileDate() {
		return fileDate;
	}
	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}
}
	