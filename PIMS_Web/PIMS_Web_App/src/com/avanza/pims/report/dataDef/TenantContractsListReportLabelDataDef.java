package com.avanza.pims.report.dataDef;

public class TenantContractsListReportLabelDataDef extends AbstractReportDataDef{
	
    /*******************************                           English Labels             *********************************/

	private String lblTenantNumberEn;
	private String lblTenantNameEn;
	private String lblTenantTypeEn;
	private String lblOfficePhoneEn;
	private String lblResidenceNumberEn;
	private String lblCellNumberEn;
	private String lblAddressEn; 
	private String lblPOBoxEn;
	private String lblEmailEn;

	private String lblContractNumberEn;
	private String lblContractStartDateEn;
	private String lblContractEndDateEn;
	private String lblContractRentValueEn;
	private String lblContractStatusEn;
	private String lblContractTypeEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblIndividualEn;
	private String lblIndividualAr;
	private String lblCompanyEn;
	private String lblCompanyAr;
	private String lblTenantIdEn;
	private String lblTenantIdAr;

    /*******************************                           Arabic Labels             *********************************/
	
	private String lblTenantNumberAr;
	private String lblTenantNameAr;
	private String lblTenantTypeAr;
	private String lblOfficePhoneAr;
	private String lblResidenceNumberAr;
	private String lblCellNumberAr;
	private String lblAddressAr; 
	private String lblPOBoxAr;
	private String lblEmailAr;

	private String lblContractNumberAr;
	private String lblContractStartDateAr;
	private String lblContractEndDateAr;
	private String lblContractRentValueAr;
	private String lblContractStatusAr;
	private String lblContractTypeAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;	

	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblTenantNumberEn() {
		return lblTenantNumberEn;
	}
	public void setLblTenantNumberEn(String lblTenantNumberEn) {
		this.lblTenantNumberEn = lblTenantNumberEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblTenantTypeEn() {
		return lblTenantTypeEn;
	}
	public void setLblTenantTypeEn(String lblTenantTypeEn) {
		this.lblTenantTypeEn = lblTenantTypeEn;
	}
	public String getLblOfficePhoneEn() {
		return lblOfficePhoneEn;
	}
	public void setLblOfficePhoneEn(String lblOfficePhoneEn) {
		this.lblOfficePhoneEn = lblOfficePhoneEn;
	}
	public String getLblResidenceNumberEn() {
		return lblResidenceNumberEn;
	}
	public void setLblResidenceNumberEn(String lblResidenceNumberEn) {
		this.lblResidenceNumberEn = lblResidenceNumberEn;
	}
	public String getLblCellNumberEn() {
		return lblCellNumberEn;
	}
	public void setLblCellNumberEn(String lblCellNumberEn) {
		this.lblCellNumberEn = lblCellNumberEn;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblPOBoxEn() {
		return lblPOBoxEn;
	}
	public void setLblPOBoxEn(String lblPOBoxEn) {
		this.lblPOBoxEn = lblPOBoxEn;
	}
	public String getLblEmailEn() {
		return lblEmailEn;
	}
	public void setLblEmailEn(String lblEmailEn) {
		this.lblEmailEn = lblEmailEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblContractStartDateEn() {
		return lblContractStartDateEn;
	}
	public void setLblContractStartDateEn(String lblContractStartDateEn) {
		this.lblContractStartDateEn = lblContractStartDateEn;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblContractRentValueEn() {
		return lblContractRentValueEn;
	}
	public void setLblContractRentValueEn(String lblContractRentValueEn) {
		this.lblContractRentValueEn = lblContractRentValueEn;
	}
	public String getLblContractStatusEn() {
		return lblContractStatusEn;
	}
	public void setLblContractStatusEn(String lblContractStatusEn) {
		this.lblContractStatusEn = lblContractStatusEn;
	}
	public String getLblContractTypeEn() {
		return lblContractTypeEn;
	}
	public void setLblContractTypeEn(String lblContractTypeEn) {
		this.lblContractTypeEn = lblContractTypeEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblTenantNumberAr() {
		return lblTenantNumberAr;
	}
	public void setLblTenantNumberAr(String lblTenantNumberAr) {
		this.lblTenantNumberAr = lblTenantNumberAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblTenantTypeAr() {
		return lblTenantTypeAr;
	}
	public void setLblTenantTypeAr(String lblTenantTypeAr) {
		this.lblTenantTypeAr = lblTenantTypeAr;
	}
	public String getLblOfficePhoneAr() {
		return lblOfficePhoneAr;
	}
	public void setLblOfficePhoneAr(String lblOfficePhoneAr) {
		this.lblOfficePhoneAr = lblOfficePhoneAr;
	}
	public String getLblResidenceNumberAr() {
		return lblResidenceNumberAr;
	}
	public void setLblResidenceNumberAr(String lblResidenceNumberAr) {
		this.lblResidenceNumberAr = lblResidenceNumberAr;
	}
	public String getLblCellNumberAr() {
		return lblCellNumberAr;
	}
	public void setLblCellNumberAr(String lblCellNumberAr) {
		this.lblCellNumberAr = lblCellNumberAr;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblPOBoxAr() {
		return lblPOBoxAr;
	}
	public void setLblPOBoxAr(String lblPOBoxAr) {
		this.lblPOBoxAr = lblPOBoxAr;
	}
	public String getLblEmailAr() {
		return lblEmailAr;
	}
	public void setLblEmailAr(String lblEmailAr) {
		this.lblEmailAr = lblEmailAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblContractStartDateAr() {
		return lblContractStartDateAr;
	}
	public void setLblContractStartDateAr(String lblContractStartDateAr) {
		this.lblContractStartDateAr = lblContractStartDateAr;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblContractRentValueAr() {
		return lblContractRentValueAr;
	}
	public void setLblContractRentValueAr(String lblContractRentValueAr) {
		this.lblContractRentValueAr = lblContractRentValueAr;
	}
	public String getLblContractStatusAr() {
		return lblContractStatusAr;
	}
	public void setLblContractStatusAr(String lblContractStatusAr) {
		this.lblContractStatusAr = lblContractStatusAr;
	}
	public String getLblContractTypeAr() {
		return lblContractTypeAr;
	}
	public void setLblContractTypeAr(String lblContractTypeAr) {
		this.lblContractTypeAr = lblContractTypeAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblIndividualEn() {
		return lblIndividualEn;
	}
	public void setLblIndividualEn(String lblIndividualEn) {
		this.lblIndividualEn = lblIndividualEn;
	}
	public String getLblIndividualAr() {
		return lblIndividualAr;
	}
	public void setLblIndividualAr(String lblIndividualAr) {
		this.lblIndividualAr = lblIndividualAr;
	}
	public String getLblCompanyEn() {
		return lblCompanyEn;
	}
	public void setLblCompanyEn(String lblCompanyEn) {
		this.lblCompanyEn = lblCompanyEn;
	}
	public String getLblCompanyAr() {
		return lblCompanyAr;
	}
	public void setLblCompanyAr(String lblCompanyAr) {
		this.lblCompanyAr = lblCompanyAr;
	}
	public String getLblTenantIdEn() {
		return lblTenantIdEn;
	}
	public void setLblTenantIdEn(String lblTenantIdEn) {
		this.lblTenantIdEn = lblTenantIdEn;
	}
	public String getLblTenantIdAr() {
		return lblTenantIdAr;
	}
	public void setLblTenantIdAr(String lblTenantIdAr) {
		this.lblTenantIdAr = lblTenantIdAr;
	}
	
	
	
}

