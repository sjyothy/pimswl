package com.avanza.pims.report.dataDef;

public class UnitsPerTenantsReportDataDef extends AbstractReportDataDef {
	
	private String propertyNumber;
	private String propertyName;
	private String propertyTypeEn;
	private String propertyTypeAr;
	private String address;
	private String tenantName;
	private String emirates;
	private String contractNumber;
	private String fromDate;
	private String toDate;
	private String contractValue;
	private String rentalIncome;
	private String totalRentalIncome;
	private String tenantId;
	private String unitNumber;
	private String costCenter;
	
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyTypeEn() {
		return propertyTypeEn;
	}
	public void setPropertyTypeEn(String propertyTypeEn) {
		this.propertyTypeEn = propertyTypeEn;
	}
	public String getPropertyTypeAr() {
		return propertyTypeAr;
	}
	public void setPropertyTypeAr(String propertyTypeAr) {
		this.propertyTypeAr = propertyTypeAr;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getContractValue() {
		return contractValue;
	}
	public void setContractValue(String contractValue) {
		this.contractValue = contractValue;
	}
	public String getRentalIncome() {
		return rentalIncome;
	}
	public void setRentalIncome(String rentalIncome) {
		this.rentalIncome = rentalIncome;
	}
	public String getTotalRentalIncome() {
		return totalRentalIncome;
	}
	public void setTotalRentalIncome(String totalRentalIncome) {
		this.totalRentalIncome = totalRentalIncome;
	}
	public String getEmirates() {
		return emirates;
	}
	public void setEmirates(String emirates) {
		this.emirates = emirates;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	} 
	
	
	
	
	
}
