package com.avanza.pims.report.dataDef;


public class InheritanceDetailsTransportationDataDef extends AbstractReportDataDef
{

	   private Long iAssetId;
	   private Long fileId;
	   private Long assetid;
	   private Long isFinal;
	   private Long isConfirm;
	   private String assetNameEn;
	   private String assetNameAr;
	   private String assetDesc;
	   private String assetNumber;
	   private Double expRevenue;
	   private Double amount;
	   private Long isIncomeExp;
	   private Long govtDeptId;
	   private String filedNumber;
	   private Long regionId;
	   private String fieldStatus;
	   private Double devolution;
	   private String fieldType;
	   private String vehicalCat;
	   private String fieldParty;
	   private String company;
	   private String licenceName;
	   private Long  bankId;
	   private String accountName;
	   private Long isAmafAccount;
	   private Long revenueType;
	   private String fromDate;
	   private String dueDate;
	   private String regionName;
	   private String assetTypeEn;
	   private String assetTypeAr;
	   private String revenueTypeEn;
	   private String revenueTypeAr; 
	   private String banknameen;
	   private String banknamear; 
	   private String depttnameen; 
	   private String depttnamear; 
	   private Long assetTypeId;
	   private String contactperson;
	   private String phone1;
	   private String phone2;
	   private String fax;
	   private String email;
	   private String weight;
	   private String isManagerAmaf;
	   private String managerName;
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public Long getIAssetId() {
		return iAssetId;
	}
	public void setIAssetId(Long assetId) {
		iAssetId = assetId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public Long getAssetid() {
		return assetid;
	}
	public void setAssetid(Long assetid) {
		this.assetid = assetid;
	}
	public Long getIsFinal() {
		return isFinal;
	}
	public void setIsFinal(Long isFinal) {
		this.isFinal = isFinal;
	}
	public Long getIsConfirm() {
		return isConfirm;
	}
	public void setIsConfirm(Long isConfirm) {
		this.isConfirm = isConfirm;
	}
	public String getAssetNameEn() {
		return assetNameEn;
	}
	public void setAssetNameEn(String assetNameEn) {
		this.assetNameEn = assetNameEn;
	}
	public String getAssetNameAr() {
		return assetNameAr;
	}
	public void setAssetNameAr(String assetNameAr) {
		this.assetNameAr = assetNameAr;
	}
	public String getAssetDesc() {
		return assetDesc;
	}
	public void setAssetDesc(String assetDesc) {
		this.assetDesc = assetDesc;
	}
	public String getAssetNumber() {
		return assetNumber;
	}
	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}
	public Double getExpRevenue() {
		return expRevenue;
	}
	public void setExpRevenue(Double expRevenue) {
		this.expRevenue = expRevenue;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Long getIsIncomeExp() {
		return isIncomeExp;
	}
	public void setIsIncomeExp(Long isIncomeExp) {
		this.isIncomeExp = isIncomeExp;
	}
	public Long getGovtDeptId() {
		return govtDeptId;
	}
	public void setGovtDeptId(Long govtDeptId) {
		this.govtDeptId = govtDeptId;
	}
	public String getFiledNumber() {
		return filedNumber;
	}
	public void setFiledNumber(String filedNumber) {
		this.filedNumber = filedNumber;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public String getFieldStatus() {
		return fieldStatus;
	}
	public void setFieldStatus(String fieldStatus) {
		this.fieldStatus = fieldStatus;
	}
	public Double getDevolution() {
		return devolution;
	}
	public void setDevolution(Double devolution) {
		this.devolution = devolution;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getVehicalCat() {
		return vehicalCat;
	}
	public void setVehicalCat(String vehicalCat) {
		this.vehicalCat = vehicalCat;
	}
	public String getFieldParty() {
		return fieldParty;
	}
	public void setFieldParty(String fieldParty) {
		this.fieldParty = fieldParty;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLicenceName() {
		return licenceName;
	}
	public void setLicenceName(String licenceName) {
		this.licenceName = licenceName;
	}
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Long getIsAmafAccount() {
		return isAmafAccount;
	}
	public void setIsAmafAccount(Long isAmafAccount) {
		this.isAmafAccount = isAmafAccount;
	}
	public Long getRevenueType() {
		return revenueType;
	}
	public void setRevenueType(Long revenueType) {
		this.revenueType = revenueType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getAssetTypeEn() {
		return assetTypeEn;
	}
	public void setAssetTypeEn(String assetTypeEn) {
		this.assetTypeEn = assetTypeEn;
	}
	public String getAssetTypeAr() {
		return assetTypeAr;
	}
	public void setAssetTypeAr(String assetTypeAr) {
		this.assetTypeAr = assetTypeAr;
	}
	public String getRevenueTypeEn() {
		return revenueTypeEn;
	}
	public void setRevenueTypeEn(String revenueTypeEn) {
		this.revenueTypeEn = revenueTypeEn;
	}
	public String getRevenueTypeAr() {
		return revenueTypeAr;
	}
	public void setRevenueTypeAr(String revenueTypeAr) {
		this.revenueTypeAr = revenueTypeAr;
	}
	public String getBanknameen() {
		return banknameen;
	}
	public void setBanknameen(String banknameen) {
		this.banknameen = banknameen;
	}
	public String getBanknamear() {
		return banknamear;
	}
	public void setBanknamear(String banknamear) {
		this.banknamear = banknamear;
	}
	public String getDepttnameen() {
		return depttnameen;
	}
	public void setDepttnameen(String depttnameen) {
		this.depttnameen = depttnameen;
	}
	public String getDepttnamear() {
		return depttnamear;
	}
	public void setDepttnamear(String depttnamear) {
		this.depttnamear = depttnamear;
	}
	public Long getAssetTypeId() {
		return assetTypeId;
	}
	public void setAssetTypeId(Long assetTypeId) {
		this.assetTypeId = assetTypeId;
	}
	public String getContactperson() {
		return contactperson;
	}
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIsManagerAmaf() {
		return isManagerAmaf;
	}
	public void setIsManagerAmaf(String isManagerAmaf) {
		this.isManagerAmaf = isManagerAmaf;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
}
	