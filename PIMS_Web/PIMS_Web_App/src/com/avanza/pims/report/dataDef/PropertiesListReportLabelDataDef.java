package com.avanza.pims.report.dataDef;

public class PropertiesListReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyNumberAr;
	private String lblPropertyTypeEn;
	private String lblPropertyTypeAr;
	private String	lblPropertyNameEn;
	private String	lblPropertyNameAr;
	private String lblEmirateEn;
	private String lblEmirateAr;
	private String lblAddressEn;
	private String lblAddressAr;
	private String lblTotalUnitsEn;
	private String lblTotalUnitsAr;
	private String lblTotalFloorsEn;
	private String lblTotalFloorsAr;
	private String lblTotalEn;
	private String lblTotalAr;


////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblLoggedInUser;
	public String getLblPropertyNumberEn() {
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblEmirateAr() {
		return lblEmirateAr;
	}
	public void setLblEmirateAr(String lblEmirateAr) {
		this.lblEmirateAr = lblEmirateAr;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblTotalUnitsEn() {
		return lblTotalUnitsEn;
	}
	public void setLblTotalUnitsEn(String lblTotalUnitsEn) {
		this.lblTotalUnitsEn = lblTotalUnitsEn;
	}
	public String getLblTotalUnitsAr() {
		return lblTotalUnitsAr;
	}
	public void setLblTotalUnitsAr(String lblTotalUnitsAr) {
		this.lblTotalUnitsAr = lblTotalUnitsAr;
	}
	public String getLblTotalFloorsEn() {
		return lblTotalFloorsEn;
	}
	public void setLblTotalFloorsEn(String lblTotalFloorsEn) {
		this.lblTotalFloorsEn = lblTotalFloorsEn;
	}
	public String getLblTotalFloorsAr() {
		return lblTotalFloorsAr;
	}
	public void setLblTotalFloorsAr(String lblTotalFloorsAr) {
		this.lblTotalFloorsAr = lblTotalFloorsAr;
	}
	public String getLblTotalEn() {
		return lblTotalEn;
	}
	public void setLblTotalEn(String lblTotalEn) {
		this.lblTotalEn = lblTotalEn;
	}
	public String getLblTotalAr() {
		return lblTotalAr;
	}
	public void setLblTotalAr(String lblTotalAr) {
		this.lblTotalAr = lblTotalAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
}
