package com.avanza.pims.report.dataDef;

public class RentCommitteRecommendationSubReportDataDef extends AbstractReportDataDef {
	
	private String recommendation;
	private String dateTime;
	
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

}
