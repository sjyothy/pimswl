package com.avanza.pims.report.dataDef;

public class PropertyDetailReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyNumberAr;
	private String lblPropertyTypeEn;
	private String lblPropertyTypeAr;
	private String	lblPropertyNameEn;
	private String	lblPropertyNameAr;
	private String lblEmirateEn;
	private String lblEmirateAn;
	private String lblUnitTypeEn;
	private String lblUnitTypeAr;
	private String lblTotalUnitsEn;
	private String lblTotalUnitsAr;
	private String lblGrandTotalEn;
	private String lblGrandTotalAn;
	private String	lblFloorTypeEn;
	private String	lblFloorTypeAr;
	private String	lblFloorNameEn;
	private String	lblFloorNameAr;
	private String	lblFloorNumberEn;
	private String	lblFloorNumberAr;
	private String lblNumberOfUnitsEn;
	private String lblNumberOfUnitsAr;
	
	private String lblAddressEn;
	private String lblAddressAr;
	private String lblTotalSumEn;
	private String lblTotalSumAr;
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblPropertyDetailReportEn;
	private String lblPIMSHeaderAr;
	private String lblPropertyDetailReportAr;
	private String lblLoggedInUser;
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	//////////////////////
	public String getLblPropertyNumberEn() {
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblEmirateAn() {
		return lblEmirateAn;
	}
	public void setLblEmirateAn(String lblEmirateAn) {
		this.lblEmirateAn = lblEmirateAn;
	}
	public String getLblUnitTypeEn() {
		return lblUnitTypeEn;
	}
	public void setLblUnitTypeEn(String lblUnitTypeEn) {
		this.lblUnitTypeEn = lblUnitTypeEn;
	}
	public String getLblUnitTypeAr() {
		return lblUnitTypeAr;
	}
	public void setLblUnitTypeAr(String lblUnitTypeAr) {
		this.lblUnitTypeAr = lblUnitTypeAr;
	}
	public String getLblTotalUnitsEn() {
		return lblTotalUnitsEn;
	}
	public void setLblTotalUnitsEn(String lblTotalUnitsEn) {
		this.lblTotalUnitsEn = lblTotalUnitsEn;
	}
	public String getLblTotalUnitsAr() {
		return lblTotalUnitsAr;
	}
	public void setLblTotalUnitsAr(String lblTotalUnitsAr) {
		this.lblTotalUnitsAr = lblTotalUnitsAr;
	}
	public String getLblGrandTotalEn() {
		return lblGrandTotalEn;
	}
	public void setLblGrandTotalEn(String lblGrandTotalEn) {
		this.lblGrandTotalEn = lblGrandTotalEn;
	}
	public String getLblGrandTotalAn() {
		return lblGrandTotalAn;
	}
	public void setLblGrandTotalAn(String lblGrandTotalAn) {
		this.lblGrandTotalAn = lblGrandTotalAn;
	}
		public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblTotalSumEn() {
		return lblTotalSumEn;
	}
	public void setLblTotalSumEn(String lblTotalSumEn) {
		this.lblTotalSumEn = lblTotalSumEn;
	}
	public String getLblTotalSumAr() {
		return lblTotalSumAr;
	}
	public void setLblTotalSumAr(String lblTotalSumAr) {
		this.lblTotalSumAr = lblTotalSumAr;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblPropertyDetailReportEn() {
		return lblPropertyDetailReportEn;
	}
	public void setLblPropertyDetailReportEn(String lblPropertyDetailReportEn) {
		this.lblPropertyDetailReportEn = lblPropertyDetailReportEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblPropertyDetailReportAr() {
		return lblPropertyDetailReportAr;
	}
	public void setLblPropertyDetailReportAr(String lblPropertyDetailReportAr) {
		this.lblPropertyDetailReportAr = lblPropertyDetailReportAr;
	}
	
	public void setLblFloorNameEn(String lblFloorNameEn) {
		this.lblFloorNameEn = lblFloorNameEn;
	}
	public String getLblFloorNameAr() {
		return lblFloorNameAr;
	}
	public void setLblFloorNameAr(String lblFloorNameAr) {
		this.lblFloorNameAr = lblFloorNameAr;
	}
	
	public String getLblFloorNumberEn() {
		return lblFloorNumberEn;
	}
	public void setLblFloorNumberEn(String lblFloorNumberEn) {
		this.lblFloorNumberEn = lblFloorNumberEn;
	}
	public String getLblFloorNumberAr() {
		return lblFloorNumberAr;
	}
	public void setLblFloorNumberAr(String lblFloorNumberAr) {
		this.lblFloorNumberAr = lblFloorNumberAr;
	}
	public String getLblFloorTypeEn() {
		return lblFloorTypeEn;
	}
	public void setLblFloorTypeEn(String lblFloorTypeEn) {
		this.lblFloorTypeEn = lblFloorTypeEn;
	}
	public String getLblFloorTypeAr() {
		return lblFloorTypeAr;
	}
	public void setLblFloorTypeAr(String lblFloorTypeAr) {
		this.lblFloorTypeAr = lblFloorTypeAr;
	}
	public String getLblFloorNameEn() {
		return lblFloorNameEn;
	}
	public String getLblNumberOfUnitsEn() {
		return lblNumberOfUnitsEn;
	}
	public void setLblNumberOfUnitsEn(String lblNumberOfUnitsEn) {
		this.lblNumberOfUnitsEn = lblNumberOfUnitsEn;
	}
	public String getLblNumberOfUnitsAr() {
		return lblNumberOfUnitsAr;
	}
	public void setLblNumberOfUnitsAr(String lblNumberOfUnitsAr) {
		this.lblNumberOfUnitsAr = lblNumberOfUnitsAr;
	}
	
	
}
