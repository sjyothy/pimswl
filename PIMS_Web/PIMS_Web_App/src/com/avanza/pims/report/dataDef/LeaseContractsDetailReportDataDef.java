package com.avanza.pims.report.dataDef;

public class LeaseContractsDetailReportDataDef extends AbstractReportDataDef {
	
	private String contractNumber;
	private String propertyName;
	private String contractIssueDate;
	private String contractEndDate;
	private String occupantFirstName;
	private String occupantLastName;
	private String statusEn;
	private String statusAr;
	private String tenantFirstName;
	private String tenantMiddleName;
	private String tenantLastName;
	
	private String unitNumber;
	private String unitDesc;
	private String dewaNumber;
	private String landNumber;
	private String unitArea;
	private String floorName;
	private String floorNumber;
	private String rentValue;
	
	private String paymentNo;
	private String paymentDueDate;
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentMethod;
	private String receiptNumber;
	private String chequeNumber;
	private String chequeBankNameEn;
	private String chequeBankNameAr;
	private String paymentAmount;
	private String paymentStatusEn;
	private String paymentStatusAr;
	private String totalSumAmount;
	private String loggedInUser;
	private String contractAmount;
	private String cityName;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getContractIssueDate() {
		return contractIssueDate;
	}
	public void setContractIssueDate(String contractIssueDate) {
		this.contractIssueDate = contractIssueDate;
	}
	public String getOccupantFirstName() {
		return occupantFirstName;
	}
	public void setOccupantFirstName(String occupantFirstName) {
		this.occupantFirstName = occupantFirstName;
	}
	public String getOccupantLastName() {
		return occupantLastName;
	}
	public void setOccupantLastName(String occupantLastName) {
		this.occupantLastName = occupantLastName;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getUnitDesc() {
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public String getUnitArea() {
		return unitArea;
	}
	public void setUnitArea(String unitArea) {
		this.unitArea = unitArea;
	}
	public String getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}
	public String getRentValue() {
		return rentValue;
	}
	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}
	public String getFloorName() {
		return floorName;
	}
	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeBankNameEn() {
		return chequeBankNameEn;
	}
	public void setChequeBankNameEn(String chequeBankNameEn) {
		this.chequeBankNameEn = chequeBankNameEn;
	}
	public String getChequeBankNameAr() {
		return chequeBankNameAr;
	}
	public void setChequeBankNameAr(String chequeBankNameAr) {
		this.chequeBankNameAr = chequeBankNameAr;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentStatusEn() {
		return paymentStatusEn;
	}
	public void setPaymentStatusEn(String paymentStatusEn) {
		this.paymentStatusEn = paymentStatusEn;
	}
	public String getPaymentStatusAr() {
		return paymentStatusAr;
	}
	public void setPaymentStatusAr(String paymentStatusAr) {
		this.paymentStatusAr = paymentStatusAr;
	}
	public String getTotalSumAmount() {
		return totalSumAmount;
	}
	public void setTotalSumAmount(String totalSumAmount) {
		this.totalSumAmount = totalSumAmount;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getDewaNumber() {
		return dewaNumber;
	}
	public void setDewaNumber(String dewaNumber) {
		this.dewaNumber = dewaNumber;
	}
	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	
	
	
}
