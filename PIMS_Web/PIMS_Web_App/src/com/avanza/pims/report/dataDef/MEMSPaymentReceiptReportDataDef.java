package com.avanza.pims.report.dataDef;

public class MEMSPaymentReceiptReportDataDef extends AbstractReportDataDef{
	private String fileNumber;
	private String fileOwnerName;
	private String generalAccountNumber;	
	private String receiptNumber;
	private String receiptDate;	
	private String amount;
	private String relatedToEn;
	private String relatedToAr;
	private String description;	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
    private String bankNameEn;
    private String bankNameAr;
    private String refNumber;
    private String refDate;
    private String accountNumber;
    private String paymentMethodId;
    private String paymentMethodEn;
    private String paymentMethodAr;
    private String requestNo;
    
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public String getFileOwnerName() {
		return fileOwnerName;
	}
	public void setFileOwnerName(String fileOwnerName) {
		this.fileOwnerName = fileOwnerName;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getGeneralAccountNumber() {
		return generalAccountNumber;
	}
	public void setGeneralAccountNumber(String generalAccountNumber) {
		this.generalAccountNumber = generalAccountNumber;
	}
	public String getRelatedToEn() {
		return relatedToEn;
	}
	public void setRelatedToEn(String relatedToEn) {
		this.relatedToEn = relatedToEn;
	}
	public String getRelatedToAr() {
		return relatedToAr;
	}
	public void setRelatedToAr(String relatedToAr) {
		this.relatedToAr = relatedToAr;
	}
	public String getBankNameEn() {
		return bankNameEn;
	}
	public void setBankNameEn(String bankNameEn) {
		this.bankNameEn = bankNameEn;
	}
	public String getBankNameAr() {
		return bankNameAr;
	}
	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}
	public String getPaymentMethodEn() {
		return paymentMethodEn;
	}
	public void setPaymentMethodEn(String paymentMethodEn) {
		this.paymentMethodEn = paymentMethodEn;
	}
	public String getPaymentMethodAr() {
		return paymentMethodAr;
	}
	public void setPaymentMethodAr(String paymentMethodAr) {
		this.paymentMethodAr = paymentMethodAr;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public String getRefDate() {
		return refDate;
	}
	public void setRefDate(String refDate) {
		this.refDate = refDate;
	}
	public String getRequestNo() {
		return requestNo;
	}
	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}
	
}
