package com.avanza.pims.report.dataDef;

public class POJOReportData {
	
	private int intField;
	private long longField;
	private float floatField;
	private double doubleField;	
	private String stringField;
	private String addedField;
	private POJOReportData2 pojoReportData2;
	
	public POJOReportData(int intField, long longField, float floatField, double doubleField, String stringField, String addedField, POJOReportData2 pojoReportData2) 
	{		
		this.intField = intField;
		this.longField = longField;
		this.floatField = floatField;
		this.doubleField = doubleField;
		this.stringField = stringField;
		this.addedField = addedField;
		this.pojoReportData2 = pojoReportData2;
	}

	public int getIntField() {
		return intField;
	}

	public void setIntField(int intField) {
		this.intField = intField;
	}

	public long getLongField() {
		return longField;
	}

	public void setLongField(long longField) {
		this.longField = longField;
	}

	public float getFloatField() {
		return floatField;
	}

	public void setFloatField(float floatField) {
		this.floatField = floatField;
	}

	public double getDoubleField() {
		return doubleField;
	}

	public void setDoubleField(double doubleField) {
		this.doubleField = doubleField;
	}

	public String getStringField() {
		return stringField;
	}

	public void setStringField(String stringField) {
		this.stringField = stringField;
	}

	public String getAddedField() {
		return addedField;
	}

	public void setAddedField(String addedField) {
		this.addedField = addedField;
	}

	public POJOReportData2 getPojoReportData2() {
		return pojoReportData2;
	}

	public void setPojoReportData2(POJOReportData2 pojoReportData2) {
		this.pojoReportData2 = pojoReportData2;
	}
	
}
