package com.avanza.pims.report.dataDef;

public class PropertyUnitListReportDataDef extends AbstractReportDataDef {
	
	private String propertyNumberEn;
	private String propertyTypeEn;
	private String propertyTypeAr;
	
	private String	propertyNameEn;
	private String emirateEn;
	private String unitTypeEn;
	private String unitTypeAr;
	private String grandTotalEn;
	
	private String	floorNumberEn;
	
	private String addressEn;
	
	private String totalUnitsEn;
	
	private String unitNumberEn;
	
	private String unitSideEn;
	private String unitSideAr;
	
	private String numberOfRoomsEn;
	
	private String totalSumEn;
	
	private String grandSumEn;
	private String unitCostCenter;
	private String unitStatusEn;
	private String unitStatusAr;
	private String annualRent;
	private String area;
	private String unitDesc;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8; 
	private String field9;
	private String field10;
	private String usageTypeEn;
	private String usageTypeAr;
	private String usageTypeId;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	
	public String getUnitCostCenter() {
		return unitCostCenter;
	}
	public void setUnitCostCenter(String unitCostCenter) {
		this.unitCostCenter = unitCostCenter;
	}
	public String getUnitStatusEn() {
		return unitStatusEn;
	}
	public void setUnitStatusEn(String unitStatusEn) {
		this.unitStatusEn = unitStatusEn;
	}
	public String getUnitStatusAr() {
		return unitStatusAr;
	}
	public void setUnitStatusAr(String unitStatusAr) {
		this.unitStatusAr = unitStatusAr;
	}
	public String getPropertyNumberEn() {
		return propertyNumberEn;
	}
	public void setPropertyNumberEn(String propertyNumberEn) {
		this.propertyNumberEn = propertyNumberEn;
	}

	public String getPropertyTypeEn() {
		return propertyTypeEn;
	}
	public void setPropertyTypeEn(String propertyTypeEn) {
		this.propertyTypeEn = propertyTypeEn;
	}
	public String getPropertyNameEn() {
		return propertyNameEn;
	}
	public void setPropertyNameEn(String propertyNameEn) {
		this.propertyNameEn = propertyNameEn;
	}
	public String getEmirateEn() {
		return emirateEn;
	}
	public void setEmirateEn(String emirateEn) {
		this.emirateEn = emirateEn;
	}
	public String getUnitTypeEn() {
		return unitTypeEn;
	}
	public void setUnitTypeEn(String unitTypeEn) {
		this.unitTypeEn = unitTypeEn;
	}
	public String getUnitTypeAr() {
		return unitTypeAr;
	}
	public void setUnitTypeAr(String unitTypeAr) {
		this.unitTypeAr = unitTypeAr;
	}
	public String getGrandTotalEn() {
		return grandTotalEn;
	}
	public void setGrandTotalEn(String grandTotalEn) {
		this.grandTotalEn = grandTotalEn;
	}

	public String getFloorNumberEn() {
		return floorNumberEn;
	}
	public void setFloorNumberEn(String floorNumberEn) {
		this.floorNumberEn = floorNumberEn;
	}
	public String getAddressEn() {
		return addressEn;
	}
	public void setAddressEn(String addressEn) {
		this.addressEn = addressEn;
	}
	public String getTotalUnitsEn() {
		return totalUnitsEn;
	}
	public void setTotalUnitsEn(String totalUnitsEn) {
		this.totalUnitsEn = totalUnitsEn;
	}
	public String getUnitNumberEn() {
		return unitNumberEn;
	}
	public void setUnitNumberEn(String unitNumberEn) {
		this.unitNumberEn = unitNumberEn;
	}
	public String getUnitSideEn() {
		return unitSideEn;
	}
	public void setUnitSideEn(String unitSideEn) {
		this.unitSideEn = unitSideEn;
	}
	public String getUnitSideAr() {
		return unitSideAr;
	}
	public void setUnitSideAr(String unitSideAr) {
		this.unitSideAr = unitSideAr;
	}
	public String getNumberOfRoomsEn() {
		return numberOfRoomsEn;
	}
	public void setNumberOfRoomsEn(String numberOfRoomsEn) {
		this.numberOfRoomsEn = numberOfRoomsEn;
	}
	public String getTotalSumEn() {
		return totalSumEn;
	}
	public void setTotalSumEn(String totalSumEn) {
		this.totalSumEn = totalSumEn;
	}
	public String getGrandSumEn() {
		return grandSumEn;
	}
	public void setGrandSumEn(String grandSumEn) {
		this.grandSumEn = grandSumEn;
	}
	public String getPropertyTypeAr() {
		return propertyTypeAr;
	}
	public void setPropertyTypeAr(String propertyTypeAr) {
		this.propertyTypeAr = propertyTypeAr;
	}
	public String getAnnualRent() {
		return annualRent;
	}
	public void setAnnualRent(String annualRent) {
		this.annualRent = annualRent;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getUnitDesc() {
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getUsageTypeEn() {
		return usageTypeEn;
	}
	public void setUsageTypeEn(String usageTypeEn) {
		this.usageTypeEn = usageTypeEn;
	}
	public String getUsageTypeAr() {
		return usageTypeAr;
	}
	public void setUsageTypeAr(String usageTypeAr) {
		this.usageTypeAr = usageTypeAr;
	}
	public String getUsageTypeId() {
		return usageTypeId;
	}
	public void setUsageTypeId(String usageTypeId) {
		this.usageTypeId = usageTypeId;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	
	
}
