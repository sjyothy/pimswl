package com.avanza.pims.report.dataDef;

import java.util.Date;


public class InheritanceDetailsDataDef extends AbstractReportDataDef
{
	   private Long assetTypeId;
	   private Long fileId; 
	   private String fileNumber; 
	   private Long ownerId; 
	   private Long researcherId; 
	   private Long sponsorId; 
	   private Long fileTypeIs;//this is ID not Is 
	   private Long fileStatus; 
	   private Date missingFromDate; 
	   private String rejectionReason;
	   private String fileDate;
	   private Long createdById;
	   private String fileTypeEn;
	   private String fileTypeAr;
	   private String fileStatusEn;
	   private String fileStatusAr;
	   private String ownerAccNumber;
	   private String researcherNameEn;
	   private String researcherNameAr;
	   private String creatorNameEn;
	   private String creatorNameAr;
	   private String sponsorName;
	   private String ownerName;
	   private String field1;
	   private String field2;
	   private String field3;
	   private String field4;
	   private String field5;
	   private String field6;
	   private String field7;
	   private String field8;
	   private String field9;
	   private String field10;
	public Long getAssetTypeId() {
		return assetTypeId;
	}
	public void setAssetTypeId(Long assetTypeId) {
		this.assetTypeId = assetTypeId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Long getResearcherId() {
		return researcherId;
	}
	public void setResearcherId(Long researcherId) {
		this.researcherId = researcherId;
	}
	public Long getSponsorId() {
		return sponsorId;
	}
	public void setSponsorId(Long sponsorId) {
		this.sponsorId = sponsorId;
	}
	public Long getFileTypeIs() {
		return fileTypeIs;
	}
	public void setFileTypeIs(Long fileTypeIs) {
		this.fileTypeIs = fileTypeIs;
	}
	public Long getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(Long fileStatus) {
		this.fileStatus = fileStatus;
	}
	public Date getMissingFromDate() {
		return missingFromDate;
	}
	public void setMissingFromDate(Date missingFromDate) {
		this.missingFromDate = missingFromDate;
	}
	public String getRejectionReason() {
		return rejectionReason;
	}
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
	public String getFileDate() {
		return fileDate;
	}
	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}
	public Long getCreatedById() {
		return createdById;
	}
	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}
	public String getFileTypeEn() {
		return fileTypeEn;
	}
	public void setFileTypeEn(String fileTypeEn) {
		this.fileTypeEn = fileTypeEn;
	}
	public String getFileTypeAr() {
		return fileTypeAr;
	}
	public void setFileTypeAr(String fileTypeAr) {
		this.fileTypeAr = fileTypeAr;
	}
	public String getFileStatusEn() {
		return fileStatusEn;
	}
	public void setFileStatusEn(String fileStatusEn) {
		this.fileStatusEn = fileStatusEn;
	}
	public String getFileStatusAr() {
		return fileStatusAr;
	}
	public void setFileStatusAr(String fileStatusAr) {
		this.fileStatusAr = fileStatusAr;
	}
	public String getOwnerAccNumber() {
		return ownerAccNumber;
	}
	public void setOwnerAccNumber(String ownerAccNumber) {
		this.ownerAccNumber = ownerAccNumber;
	}
	public String getResearcherNameEn() {
		return researcherNameEn;
	}
	public void setResearcherNameEn(String researcherNameEn) {
		this.researcherNameEn = researcherNameEn;
	}
	public String getResearcherNameAr() {
		return researcherNameAr;
	}
	public void setResearcherNameAr(String researcherNameAr) {
		this.researcherNameAr = researcherNameAr;
	}
	public String getCreatorNameEn() {
		return creatorNameEn;
	}
	public void setCreatorNameEn(String creatorNameEn) {
		this.creatorNameEn = creatorNameEn;
	}
	public String getCreatorNameAr() {
		return creatorNameAr;
	}
	public void setCreatorNameAr(String creatorNameAr) {
		this.creatorNameAr = creatorNameAr;
	}
	public String getSponsorName() {
		return sponsorName;
	}
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
}
	