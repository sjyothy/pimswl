package com.avanza.pims.report.dataDef;


public class AuctionReportLabelDataDef extends AbstractReportDataDef
{
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	private String lblAuctionNumber;
	private String lblAuctionNumberAr;
	private String lblAuctionDate;
	private String lblAuctionDesc;
	private String lblAuctionPublishDate;
	private String lblRegistrationStartDate;
	private String lblRegistrationEndDate;
	private String lblBidderName;
	private String lblUnitsRegistered;
	private String lblDepositAmount;
	private String lblPhoneNumber;
	private String lblAuctionDateAr;
	private String lblAuctionDescAr;
	private String lblAuctionPublishDateAr;
	private String lblRegistrationStartDateAr;
	private String lblRegistrationEndDateAr;
	private String lblBidderNameAr;
	private String lblUnitsRegisteredAr;
	private String lblDepositAmountAr;
	private String lblPhoneNumberAr;
    private String lblRequestNumberAr;
    private String lblRequestNumber;
    private String lblBidderStatusEn;
	private String lblBidderStatusAr;
	public String getLblRequestNumberAr() {
	return lblRequestNumberAr;
}
public void setLblRequestNumberAr(String lblRequestNumberAr) {
	this.lblRequestNumberAr = lblRequestNumberAr;
}
public String getLblRequestNumber() {
	return lblRequestNumber;
}
public void setLblRequestNumber(String lblRequestNumber) {
	this.lblRequestNumber = lblRequestNumber;
}
	public String getLblAuctionDateAr() {
		return lblAuctionDateAr;
	}
	public void setLblAuctionDateAr(String lblAuctionDateAr) {
		this.lblAuctionDateAr = lblAuctionDateAr;
	}
	public String getLblAuctionDescAr() {
		return lblAuctionDescAr;
	}
	public void setLblAuctionDescAr(String lblAuctionDescAr) {
		this.lblAuctionDescAr = lblAuctionDescAr;
	}
	public String getLblAuctionPublishDateAr() {
		return lblAuctionPublishDateAr;
	}
	public void setLblAuctionPublishDateAr(String lblAuctionPublishDateAr) {
		this.lblAuctionPublishDateAr = lblAuctionPublishDateAr;
	}
	public String getLblRegistrationStartDateAr() {
		return lblRegistrationStartDateAr;
	}
	public void setLblRegistrationStartDateAr(String lblRegistrationStartDateAr) {
		this.lblRegistrationStartDateAr = lblRegistrationStartDateAr;
	}
	public String getLblRegistrationEndDateAr() {
		return lblRegistrationEndDateAr;
	}
	public void setLblRegistrationEndDateAr(String lblRegistrationEndDateAr) {
		this.lblRegistrationEndDateAr = lblRegistrationEndDateAr;
	}
	public String getLblBidderNameAr() {
		return lblBidderNameAr;
	}
	public void setLblBidderNameAr(String lblBidderNameAr) {
		this.lblBidderNameAr = lblBidderNameAr;
	}
	public String getLblUnitsRegisteredAr() {
		return lblUnitsRegisteredAr;
	}
	public void setLblUnitsRegisteredAr(String lblUnitsRegisteredAr) {
		this.lblUnitsRegisteredAr = lblUnitsRegisteredAr;
	}
	public String getLblDepositAmountAr() {
		return lblDepositAmountAr;
	}
	public void setLblDepositAmountAr(String lblDepositAmountAr) {
		this.lblDepositAmountAr = lblDepositAmountAr;
	}
	public String getLblPhoneNumberAr() {
		return lblPhoneNumberAr;
	}
	public void setLblPhoneNumberAr(String lblPhoneNumberAr) {
		this.lblPhoneNumberAr = lblPhoneNumberAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblAuctionNumber() {
		return lblAuctionNumber;
	}
	public void setLblAuctionNumber(String lblAuctionNumber) {
		this.lblAuctionNumber = lblAuctionNumber;
	}
	public String getLblAuctionDate() {
		return lblAuctionDate;
	}
	public void setLblAuctionDate(String lblAuctionDate) {
		this.lblAuctionDate = lblAuctionDate;
	}
	public String getLblAuctionDesc() {
		return lblAuctionDesc;
	}
	public void setLblAuctionDesc(String lblAuctionDesc) {
		this.lblAuctionDesc = lblAuctionDesc;
	}
	public String getLblAuctionPublishDate() {
		return lblAuctionPublishDate;
	}
	public void setLblAuctionPublishDate(String lblAuctionPublishDate) {
		this.lblAuctionPublishDate = lblAuctionPublishDate;
	}
	public String getLblRegistrationStartDate() {
		return lblRegistrationStartDate;
	}
	public void setLblRegistrationStartDate(String lblRegistrationStartDate) {
		this.lblRegistrationStartDate = lblRegistrationStartDate;
	}
	public String getLblRegistrationEndDate() {
		return lblRegistrationEndDate;
	}
	public void setLblRegistrationEndDate(String lblRegistrationEndDate) {
		this.lblRegistrationEndDate = lblRegistrationEndDate;
	}
	public String getLblBidderName() {
		return lblBidderName;
	}
	public void setLblBidderName(String lblBidderName) {
		this.lblBidderName = lblBidderName;
	}
	public String getLblUnitsRegistered() {
		return lblUnitsRegistered;
	}
	public void setLblUnitsRegistered(String lblUnitsRegistered) {
		this.lblUnitsRegistered = lblUnitsRegistered;
	}
	public String getLblDepositAmount() {
		return lblDepositAmount;
	}
	public void setLblDepositAmount(String lblDepositAmount) {
		this.lblDepositAmount = lblDepositAmount;
	}
	public String getLblPhoneNumber() {
		return lblPhoneNumber;
	}
	public void setLblPhoneNumber(String lblPhoneNumber) {
		this.lblPhoneNumber = lblPhoneNumber;
	}
	public String getLblAuctionNumberAr() {
		return lblAuctionNumberAr;
	}
	public void setLblAuctionNumberAr(String lblAuctionNumberAr) {
		this.lblAuctionNumberAr = lblAuctionNumberAr;
	}
	public String getLblBidderStatusEn() {
		return lblBidderStatusEn;
	}
	public void setLblBidderStatusEn(String lblBidderStatusEn) {
		this.lblBidderStatusEn = lblBidderStatusEn;
	}
	public String getLblBidderStatusAr() {
		return lblBidderStatusAr;
	}
	public void setLblBidderStatusAr(String lblBidderStatusAr) {
		this.lblBidderStatusAr = lblBidderStatusAr;
	}

}
