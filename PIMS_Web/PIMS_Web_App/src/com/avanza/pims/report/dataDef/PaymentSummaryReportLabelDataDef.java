package com.avanza.pims.report.dataDef;

public class PaymentSummaryReportLabelDataDef extends AbstractReportDataDef{

    /*******************************                           English Labels             *********************************/

	private String lblPropertyNameEn;
	private String lblPropertyOwnershipTypeEn;
	private String lblPaymentTypeEn;
	private String lblPaymentAmountEn;  
	private String lblReceiptDateEn;
	private String lblPaymentDueDateEn;
	private String lblPaymentStatusEn;  //receipt Statsus : it is treat it here as a payment Status
	private String lblReceiptNoEn;
	
    /*******************************                           Arabic Labels             *********************************/

	private String lblPropertyNameAr;
	private String lblPropertyOwnershipTypeAr;
	private String lblPaymentTypeAr;
	private String lblPaymentAmountAr;  
	private String lblReceiptDateAr;
	private String lblPaymentDueDateAr;
	private String lblPaymentStatusAr;
	private String lblReceiptNoAr;
	

    /*******************************                           Report Summary Labels             *********************************/
	private String lblTotalAmountEn;
	private String lblTotalAmountAr;
	private String lblTotalPaymentAmountEn;
	private String lblTotalPaymentAmountAr;
	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyOwnershipTypeEn() {
		return lblPropertyOwnershipTypeEn;
	}
	public void setLblPropertyOwnershipTypeEn(String lblPropertyOwnershipTypeEn) {
		this.lblPropertyOwnershipTypeEn = lblPropertyOwnershipTypeEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblPaymentDueDateEn() {
		return lblPaymentDueDateEn;
	}
	public void setLblPaymentDueDateEn(String lblPaymentDueDateEn) {
		this.lblPaymentDueDateEn = lblPaymentDueDateEn;
	}
	
	public String getLblPaymentStatusEn() {
		return lblPaymentStatusEn;
	}
	public void setLblPaymentStatusEn(String lblPaymentStatusEn) {
		this.lblPaymentStatusEn = lblPaymentStatusEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblPropertyOwnershipTypeAr() {
		return lblPropertyOwnershipTypeAr;
	}
	public void setLblPropertyOwnershipTypeAr(String lblPropertyOwnershipTypeAr) {
		this.lblPropertyOwnershipTypeAr = lblPropertyOwnershipTypeAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblPaymentDueDateAr() {
		return lblPaymentDueDateAr;
	}
	public void setLblPaymentDueDateAr(String lblPaymentDueDateAr) {
		this.lblPaymentDueDateAr = lblPaymentDueDateAr;
	}
	
	public String getLblPaymentStatusAr() {
		return lblPaymentStatusAr;
	}
	public void setLblPaymentStatusAr(String lblPaymentStatusAr) {
		this.lblPaymentStatusAr = lblPaymentStatusAr;
	}
	public String getLblTotalAmountEn() {
		return lblTotalAmountEn;
	}
	public void setLblTotalAmountEn(String lblTotalAmountEn) {
		this.lblTotalAmountEn = lblTotalAmountEn;
	}
	public String getLblTotalAmountAr() {
		return lblTotalAmountAr;
	}
	public void setLblTotalAmountAr(String lblTotalAmountAr) {
		this.lblTotalAmountAr = lblTotalAmountAr;
	}
	public String getLblTotalPaymentAmountEn() {
		return lblTotalPaymentAmountEn;
	}
	public void setLblTotalPaymentAmountEn(String lblTotalPaymentAmountEn) {
		this.lblTotalPaymentAmountEn = lblTotalPaymentAmountEn;
	}
	public String getLblTotalPaymentAmountAr() {
		return lblTotalPaymentAmountAr;
	}
	public void setLblTotalPaymentAmountAr(String lblTotalPaymentAmountAr) {
		this.lblTotalPaymentAmountAr = lblTotalPaymentAmountAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblReceiptNoEn() {
		return lblReceiptNoEn;
	}
	public void setLblReceiptNoEn(String lblReceiptNoEn) {
		this.lblReceiptNoEn = lblReceiptNoEn;
	}
	public String getLblReceiptNoAr() {
		return lblReceiptNoAr;
	}
	public void setLblReceiptNoAr(String lblReceiptNoAr) {
		this.lblReceiptNoAr = lblReceiptNoAr;
	}
	
	
	

	

}
