package com.avanza.pims.report.dataDef;

public class PropertyUnitListReportLabelDataDef extends AbstractReportDataDef {
	
	private String lblPropertyNumberEn;
	private String lblPropertyNumberAr;
	private String lblPropertyTypeEn;
	private String lblPropertyTypeAr;
	private String	lblPropertyNameEn;
	private String	lblPropertyNameAr;
	private String lblEmirateEn;
	private String lblEmirateAn;
	private String lblUnitTypeEn;
	private String lblUnitTypeAr;
	private String lblGrandTotalEn;
	private String lblGrandTotalAn;
	private String	lblFloorNumberEn;
	private String	lblFloorNumberAr;
	private String lblAddressEn;
	private String lblAddressAr;
	private String lblTotalUnitsEn;
	private String lblTotalUnitsAr;
	private String lblUnitNumberEn;
	private String lblUnitNumberAr;
	private String lblUnitSideEn;
	private String lblUnitSideAr;
	private String lblNumberOfRoomsEn;
	private String lblNumberOfRoomsAr;
	private String lblTotalSumEn;
	private String lblTotalSumAn;
	private String lblGrandSumEn;
	private String lblGrandSumAn;
	private String lblUnitCountEn;
	private String lblTotalUnitCountEn;
	private String lblUnitCountAr;
	private String lblTotalUnitCountAr;
	private String lblNumberOfUnitsEn;
	private String lblNumberOfUnitsAr;
////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblPropertyUnitListReportEn;
	private String lblPIMSHeaderAr;
	private String lblPropertyUnitListReportAr;
	private String lblLoggedInUser;
	//////////////////////
	private String lblcommercialEn;
	private String lblResidentialEn;
	private String lblMultipurposeEn;
	private String lblcommercialAr;
	private String lblResidentialAr;
	private String lblMultipurposeAr;
	private String lblCommercialUnitsAr;
	private String lblCommercialUnitsEn;
	private String lblResidentialUnitsAr;
	private String lblResidentialUnitsEn;
	
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPropertyNumberEn() {
		return lblPropertyNumberEn;
	}
	public void setLblPropertyNumberEn(String lblPropertyNumberEn) {
		this.lblPropertyNumberEn = lblPropertyNumberEn;
	}
	public String getLblPropertyNumberAr() {
		return lblPropertyNumberAr;
	}
	public void setLblPropertyNumberAr(String lblPropertyNumberAr) {
		this.lblPropertyNumberAr = lblPropertyNumberAr;
	}
	public String getLblPropertyTypeEn() {
		return lblPropertyTypeEn;
	}
	public void setLblPropertyTypeEn(String lblPropertyTypeEn) {
		this.lblPropertyTypeEn = lblPropertyTypeEn;
	}
	public String getLblPropertyTypeAr() {
		return lblPropertyTypeAr;
	}
	public void setLblPropertyTypeAr(String lblPropertyTypeAr) {
		this.lblPropertyTypeAr = lblPropertyTypeAr;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblEmirateEn() {
		return lblEmirateEn;
	}
	public void setLblEmirateEn(String lblEmirateEn) {
		this.lblEmirateEn = lblEmirateEn;
	}
	public String getLblEmirateAn() {
		return lblEmirateAn;
	}
	public void setLblEmirateAn(String lblEmirateAn) {
		this.lblEmirateAn = lblEmirateAn;
	}
	public String getLblUnitTypeEn() {
		return lblUnitTypeEn;
	}
	public void setLblUnitTypeEn(String lblUnitTypeEn) {
		this.lblUnitTypeEn = lblUnitTypeEn;
	}
	public String getLblUnitTypeAr() {
		return lblUnitTypeAr;
	}
	public void setLblUnitTypeAr(String lblUnitTypeAr) {
		this.lblUnitTypeAr = lblUnitTypeAr;
	}
	public String getLblGrandTotalEn() {
		return lblGrandTotalEn;
	}
	public void setLblGrandTotalEn(String lblGrandTotalEn) {
		this.lblGrandTotalEn = lblGrandTotalEn;
	}
	public String getLblGrandTotalAn() {
		return lblGrandTotalAn;
	}
	public void setLblGrandTotalAn(String lblGrandTotalAn) {
		this.lblGrandTotalAn = lblGrandTotalAn;
	}
	public String getLblFloorNumberEn() {
		return lblFloorNumberEn;
	}
	public void setLblFloorNumberEn(String lblFloorNumberEn) {
		this.lblFloorNumberEn = lblFloorNumberEn;
	}
	public String getLblFloorNumberAr() {
		return lblFloorNumberAr;
	}
	public void setLblFloorNumberAr(String lblFloorNumberAr) {
		this.lblFloorNumberAr = lblFloorNumberAr;
	}
	public String getLblAddressEn() {
		return lblAddressEn;
	}
	public void setLblAddressEn(String lblAddressEn) {
		this.lblAddressEn = lblAddressEn;
	}
	public String getLblAddressAr() {
		return lblAddressAr;
	}
	public void setLblAddressAr(String lblAddressAr) {
		this.lblAddressAr = lblAddressAr;
	}
	public String getLblTotalUnitsEn() {
		return lblTotalUnitsEn;
	}
	public void setLblTotalUnitsEn(String lblTotalUnitsEn) {
		this.lblTotalUnitsEn = lblTotalUnitsEn;
	}
	public String getLblTotalUnitsAr() {
		return lblTotalUnitsAr;
	}
	public void setLblTotalUnitsAr(String lblTotalUnitsAr) {
		this.lblTotalUnitsAr = lblTotalUnitsAr;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblUnitSideEn() {
		return lblUnitSideEn;
	}
	public void setLblUnitSideEn(String lblUnitSideEn) {
		this.lblUnitSideEn = lblUnitSideEn;
	}
	public String getLblUnitSideAr() {
		return lblUnitSideAr;
	}
	public void setLblUnitSideAr(String lblUnitSideAr) {
		this.lblUnitSideAr = lblUnitSideAr;
	}
	public String getLblNumberOfRoomsEn() {
		return lblNumberOfRoomsEn;
	}
	public void setLblNumberOfRoomsEn(String lblNumberOfRoomsEn) {
		this.lblNumberOfRoomsEn = lblNumberOfRoomsEn;
	}
	public String getLblNumberOfRoomsAr() {
		return lblNumberOfRoomsAr;
	}
	public void setLblNumberOfRoomsAr(String lblNumberOfRoomsAr) {
		this.lblNumberOfRoomsAr = lblNumberOfRoomsAr;
	}
	public String getLblTotalSumEn() {
		return lblTotalSumEn;
	}
	public void setLblTotalSumEn(String lblTotalSumEn) {
		this.lblTotalSumEn = lblTotalSumEn;
	}
	public String getLblTotalSumAn() {
		return lblTotalSumAn;
	}
	public void setLblTotalSumAn(String lblTotalSumAn) {
		this.lblTotalSumAn = lblTotalSumAn;
	}
	public String getLblGrandSumEn() {
		return lblGrandSumEn;
	}
	public void setLblGrandSumEn(String lblGrandSumEn) {
		this.lblGrandSumEn = lblGrandSumEn;
	}
	public String getLblGrandSumAn() {
		return lblGrandSumAn;
	}
	public void setLblGrandSumAn(String lblGrandSumAn) {
		this.lblGrandSumAn = lblGrandSumAn;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblPropertyUnitListReportEn() {
		return lblPropertyUnitListReportEn;
	}
	public void setLblPropertyUnitListReportEn(String lblPropertyUnitListReportEn) {
		this.lblPropertyUnitListReportEn = lblPropertyUnitListReportEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblPropertyUnitListReportAr() {
		return lblPropertyUnitListReportAr;
	}
	public void setLblPropertyUnitListReportAr(String lblPropertyUnitListReportAr) {
		this.lblPropertyUnitListReportAr = lblPropertyUnitListReportAr;
	}
	public String getLblUnitCountEn() {
		return lblUnitCountEn;
	}
	public void setLblUnitCountEn(String lblUnitCountEn) {
		this.lblUnitCountEn = lblUnitCountEn;
	}
	public String getLblTotalUnitCountEn() {
		return lblTotalUnitCountEn;
	}
	public void setLblTotalUnitCountEn(String lblTotalUnitCountEn) {
		this.lblTotalUnitCountEn = lblTotalUnitCountEn;
	}
	public String getLblUnitCountAr() {
		return lblUnitCountAr;
	}
	public void setLblUnitCountAr(String lblUnitCountAr) {
		this.lblUnitCountAr = lblUnitCountAr;
	}
	public String getLblTotalUnitCountAr() {
		return lblTotalUnitCountAr;
	}
	public void setLblTotalUnitCountAr(String lblTotalUnitCountAr) {
		this.lblTotalUnitCountAr = lblTotalUnitCountAr;
	}
	public String getLblNumberOfUnitsEn() {
		return lblNumberOfUnitsEn;
	}
	public void setLblNumberOfUnitsEn(String lblNumberOfUnitsEn) {
		this.lblNumberOfUnitsEn = lblNumberOfUnitsEn;
	}
	public String getLblNumberOfUnitsAr() {
		return lblNumberOfUnitsAr;
	}
	public void setLblNumberOfUnitsAr(String lblNumberOfUnitsAr) {
		this.lblNumberOfUnitsAr = lblNumberOfUnitsAr;
	}
	public String getLblcommercialEn() {
		return lblcommercialEn;
	}
	public void setLblcommercialEn(String lblcommercialEn) {
		this.lblcommercialEn = lblcommercialEn;
	}
	public String getLblResidentialEn() {
		return lblResidentialEn;
	}
	public void setLblResidentialEn(String lblResidentialEn) {
		this.lblResidentialEn = lblResidentialEn;
	}
	public String getLblMultipurposeEn() {
		return lblMultipurposeEn;
	}
	public void setLblMultipurposeEn(String lblMultipurposeEn) {
		this.lblMultipurposeEn = lblMultipurposeEn;
	}
	public String getLblcommercialAr() {
		return lblcommercialAr;
	}
	public void setLblcommercialAr(String lblcommercialAr) {
		this.lblcommercialAr = lblcommercialAr;
	}
	public String getLblResidentialAr() {
		return lblResidentialAr;
	}
	public void setLblResidentialAr(String lblResidentialAr) {
		this.lblResidentialAr = lblResidentialAr;
	}
	public String getLblMultipurposeAr() {
		return lblMultipurposeAr;
	}
	public void setLblMultipurposeAr(String lblMultipurposeAr) {
		this.lblMultipurposeAr = lblMultipurposeAr;
	}
	public String getLblCommercialUnitsAr() {
		return lblCommercialUnitsAr;
	}
	public void setLblCommercialUnitsAr(String lblCommercialUnitsAr) {
		this.lblCommercialUnitsAr = lblCommercialUnitsAr;
	}
	public String getLblCommercialUnitsEn() {
		return lblCommercialUnitsEn;
	}
	public void setLblCommercialUnitsEn(String lblCommercialUnitsEn) {
		this.lblCommercialUnitsEn = lblCommercialUnitsEn;
	}
	public String getLblResidentialUnitsAr() {
		return lblResidentialUnitsAr;
	}
	public void setLblResidentialUnitsAr(String lblResidentialUnitsAr) {
		this.lblResidentialUnitsAr = lblResidentialUnitsAr;
	}
	public String getLblResidentialUnitsEn() {
		return lblResidentialUnitsEn;
	}
	public void setLblResidentialUnitsEn(String lblResidentialUnitsEn) {
		this.lblResidentialUnitsEn = lblResidentialUnitsEn;
	}
	
}
