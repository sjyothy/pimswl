package com.avanza.pims.report.dataDef;

public class MaintenanceReportDataDef extends AbstractReportDataDef 
{
	private String createdOn;
	
	private String requestId;
	private String maintenanceContractId;
	private String maintenanceContractNumber;
	private String maintenanceTypeId;
	private String maintenanceTypeEn;
	private String maintenanceTypeAr;
	private String  requestNumber;
	private String  requestSourceEn;
	private String  requestSourceAr;
	private String workTypeId;
	private String workTypeEn;
	private String workTypeAr;
	private String leaseContractId;
	private String leaseContractNumber;
	private String leaseContractType;
	private String tenantName;
	private String applicantName;
	private String unitNumber;
	private String unitId;
	private String propertyNumber;
	private String propertyId;
	private String propertyNameEn;
	private String landNumber;
	private String unitTypeAr;
	private String unitTypeEn;
	private String contractorComments;
	private String maintenanceDetails;
	private String requestStatusEn;
	private String requestStatusAr;
	private String priorityEn;
	private String priorityAr;
	private String serviceProvider;
	private String fmComments;
	private String field1;
	
	private String tatSubmitted;
	private String tatApprove;
	private String tatReceivedByContractor;
	private String tatExecutedByContractor;
	private String  tatComplete;
	private String  executedByContractorOn;
	private String  completedOn;
	private String  approvedOn;
	private String  submittedOn;
	private String receivedByContractorOn;
	
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	public String getTatReceivedByContractor() {
		return tatReceivedByContractor;
	}
	public void setTatReceivedByContractor(String tatReceivedByContractor) {
		this.tatReceivedByContractor = tatReceivedByContractor;
	}
	public String getReceivedByContractorOn() {
		return receivedByContractorOn;
	}
	public void setReceivedByContractorOn(String receivedByContractorOn) {
		this.receivedByContractorOn = receivedByContractorOn;
	}
	public String getTatSubmitted() {
		return tatSubmitted;
	}
	public void setTatSubmitted(String tatSubmitted) {
		this.tatSubmitted = tatSubmitted;
	}
	public String getTatApprove() {
		return tatApprove;
	}
	public void setTatApprove(String tatApprove) {
		this.tatApprove = tatApprove;
	}
	public String getTatExecutedByContractor() {
		return tatExecutedByContractor;
	}
	public void setTatExecutedByContractor(String tatExecutedByContractor) {
		this.tatExecutedByContractor = tatExecutedByContractor;
	}
	public String getTatComplete() {
		return tatComplete;
	}
	public void setTatComplete(String tatComplete) {
		this.tatComplete = tatComplete;
	}
	public String getExecutedByContractorOn() {
		return executedByContractorOn;
	}
	public void setExecutedByContractorOn(String executedByContractorOn) {
		this.executedByContractorOn = executedByContractorOn;
	}
	public String getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(String completedOn) {
		this.completedOn = completedOn;
	}
	public String getApprovedOn() {
		return approvedOn;
	}
	public void setApprovedOn(String approvedOn) {
		this.approvedOn = approvedOn;
	}
	public String getSubmittedOn() {
		return submittedOn;
	}
	public void setSubmittedOn(String submittedOn) {
		this.submittedOn = submittedOn;
	}
	public String getFmComments() {
		return fmComments;
	}
	public void setFmComments(String fmComments) {
		this.fmComments = fmComments;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getRequestSourceEn() {
		return requestSourceEn;
	}
	public void setRequestSourceEn(String requestSourceEn) {
		this.requestSourceEn = requestSourceEn;
	}
	public String getRequestSourceAr() {
		return requestSourceAr;
	}
	public void setRequestSourceAr(String requestSourceAr) {
		this.requestSourceAr = requestSourceAr;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getMaintenanceContractId() {
		return maintenanceContractId;
	}
	public void setMaintenanceContractId(String maintenanceContractId) {
		this.maintenanceContractId = maintenanceContractId;
	}
	public String getMaintenanceContractNumber() {
		return maintenanceContractNumber;
	}
	public void setMaintenanceContractNumber(String maintenanceContractNumber) {
		this.maintenanceContractNumber = maintenanceContractNumber;
	}
	public String getMaintenanceTypeId() {
		return maintenanceTypeId;
	}
	public void setMaintenanceTypeId(String maintenanceTypeId) {
		this.maintenanceTypeId = maintenanceTypeId;
	}
	public String getMaintenanceTypeEn() {
		return maintenanceTypeEn;
	}
	public void setMaintenanceTypeEn(String maintenanceTypeEn) {
		this.maintenanceTypeEn = maintenanceTypeEn;
	}
	public String getMaintenanceTypeAr() {
		return maintenanceTypeAr;
	}
	public void setMaintenanceTypeAr(String maintenanceTypeAr) {
		this.maintenanceTypeAr = maintenanceTypeAr;
	}
	public String getWorkTypeId() {
		return workTypeId;
	}
	public void setWorkTypeId(String workTypeId) {
		this.workTypeId = workTypeId;
	}
	public String getWorkTypeEn() {
		return workTypeEn;
	}
	public void setWorkTypeEn(String workTypeEn) {
		this.workTypeEn = workTypeEn;
	}
	public String getWorkTypeAr() {
		return workTypeAr;
	}
	public void setWorkTypeAr(String workTypeAr) {
		this.workTypeAr = workTypeAr;
	}
	public String getLeaseContractId() {
		return leaseContractId;
	}
	public void setLeaseContractId(String leaseContractId) {
		this.leaseContractId = leaseContractId;
	}
	public String getLeaseContractNumber() {
		return leaseContractNumber;
	}
	public void setLeaseContractNumber(String leaseContractNumber) {
		this.leaseContractNumber = leaseContractNumber;
	}
	public String getLeaseContractType() {
		return leaseContractType;
	}
	public void setLeaseContractType(String leaseContractType) {
		this.leaseContractType = leaseContractType;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getPropertyNameEn() {
		return propertyNameEn;
	}
	public void setPropertyNameEn(String propertyNameEn) {
		this.propertyNameEn = propertyNameEn;
	}
	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getUnitTypeAr() {
		return unitTypeAr;
	}
	public void setUnitTypeAr(String unitTypeAr) {
		this.unitTypeAr = unitTypeAr;
	}
	public String getUnitTypeEn() {
		return unitTypeEn;
	}
	public void setUnitTypeEn(String unitTypeEn) {
		this.unitTypeEn = unitTypeEn;
	}
	public String getContractorComments() {
		return contractorComments;
	}
	public void setContractorComments(String contractorComments) {
		this.contractorComments = contractorComments;
	}
	public String getMaintenanceDetails() {
		return maintenanceDetails;
	}
	public void setMaintenanceDetails(String maintenanceDetails) {
		this.maintenanceDetails = maintenanceDetails;
	}
	public String getRequestStatusEn() {
		return requestStatusEn;
	}
	public void setRequestStatusEn(String requestStatusEn) {
		this.requestStatusEn = requestStatusEn;
	}
	public String getRequestStatusAr() {
		return requestStatusAr;
	}
	public void setRequestStatusAr(String requestStatusAr) {
		this.requestStatusAr = requestStatusAr;
	}
	public String getPriorityEn() {
		return priorityEn;
	}
	public void setPriorityEn(String priorityEn) {
		this.priorityEn = priorityEn;
	}
	public String getPriorityAr() {
		return priorityAr;
	}
	public void setPriorityAr(String priorityAr) {
		this.priorityAr = priorityAr;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	
	
	

	
}
