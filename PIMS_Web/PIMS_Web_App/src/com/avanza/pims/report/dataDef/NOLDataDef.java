package com.avanza.pims.report.dataDef;

public class NOLDataDef extends AbstractReportDataDef
{
	
	private String unitDescription;
	private String unitNumber;
	private String propertyName;
	private String regionNameEn;
	private String regionNameAr;
	private String landNumber;
	private String tenantName;
	private String tenantNameEn;
	private String propertyNameEn;
	private String loggedInUser;
	private String propCommercialName;
	private String propertyNumber;
	private String commActivityName;
	private String field1;
	private String field2;
	private String field3;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getTenantNameEn() {
		return tenantNameEn;
	}
	public void setTenantNameEn(String tenantNameEn) {
		this.tenantNameEn = tenantNameEn;
	}
	public String getPropertyNameEn() {
		return propertyNameEn;
	}
	public void setPropertyNameEn(String propertyNameEn) {
		this.propertyNameEn = propertyNameEn;
	}
	public String getPropCommercialName() {
		return propCommercialName;
	}
	public void setPropCommercialName(String propCommercialName) {
		this.propCommercialName = propCommercialName;
	}
	public String getUnitDescription() {
		return unitDescription;
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getRegionNameEn() {
		return regionNameEn;
	}
	public void setRegionNameEn(String regionNameEn) {
		this.regionNameEn = regionNameEn;
	}
	public String getRegionNameAr() {
		return regionNameAr;
	}
	public void setRegionNameAr(String regionNameAr) {
		this.regionNameAr = regionNameAr;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getCommActivityName() {
		return commActivityName;
	}
	public void setCommActivityName(String commActivityName) {
		this.commActivityName = commActivityName;
	}
	
}
	