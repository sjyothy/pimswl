package com.avanza.pims.report.dataDef;

public class WaqifBalanceDetailsDataDef extends AbstractReportDataDef 
{
	private String fileId;
	private String fileNumber;
	private String fileStatusId; 
	private String fileName;
	private String fileStatusEn;
	private String fileStatusAr;
	private String waqifId;
	private String waqifGRPNumber;
	private String waqifName;
	private String endowmentNum;
	private String endowmentName;
	private String endowFor;
	private String assetTypeAr;
	private String assetTypeEn;
	private String endowmentId;
	private String costCenter;
	private Double openingBalance;
	private Double closingBalance;
	private String trxDate;
	private Double credit;
	private Double debit;
	private Double amount;
	private String descriptionAr;
	private String descriptionEn;
	private String trxTypeEn;
	private String trxTypeAr;
	private String transId;
	private String endowmentBenefs;
	private String endowmentProperty;
	private String endowmentCommunity;
	private String endowmentLandNum;
	private String endowmentMainCategory;
	private String endowmentCategory;
	private String endowmentPurpose;
	private String endowmentStatus;
	private String endowmentManager;
	private String assetTypeId;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	public String getEndowmentProperty() {
		return endowmentProperty;
	}
	public void setEndowmentProperty(String endowmentProperty) {
		this.endowmentProperty = endowmentProperty;
	}
	public String getAssetTypeId() {
		return assetTypeId;
	}
	public void setAssetTypeId(String assetTypeId) {
		this.assetTypeId = assetTypeId;
	}
	public String getEndowmentBenefs() {
		return endowmentBenefs;
	}
	public void setEndowmentBenefs(String endowmentBenefs) {
		this.endowmentBenefs = endowmentBenefs;
	}
	public String getEndowmentCommunity() {
		return endowmentCommunity;
	}
	public void setEndowmentCommunity(String endowmentCommunity) {
		this.endowmentCommunity = endowmentCommunity;
	}
	public String getEndowmentLandNum() {
		return endowmentLandNum;
	}
	public void setEndowmentLandNum(String endowmentLandNum) {
		this.endowmentLandNum = endowmentLandNum;
	}
	public String getEndowmentMainCategory() {
		return endowmentMainCategory;
	}
	public void setEndowmentMainCategory(String endowmentMainCategory) {
		this.endowmentMainCategory = endowmentMainCategory;
	}
	public String getEndowmentCategory() {
		return endowmentCategory;
	}
	public void setEndowmentCategory(String endowmentCategory) {
		this.endowmentCategory = endowmentCategory;
	}
	public String getEndowmentPurpose() {
		return endowmentPurpose;
	}
	public void setEndowmentPurpose(String endowmentPurpose) {
		this.endowmentPurpose = endowmentPurpose;
	}
	public String getEndowmentStatus() {
		return endowmentStatus;
	}
	public void setEndowmentStatus(String endowmentStatus) {
		this.endowmentStatus = endowmentStatus;
	}
	public String getEndowmentManager() {
		return endowmentManager;
	}
	public void setEndowmentManager(String endowmentManager) {
		this.endowmentManager = endowmentManager;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public String getFileStatusId() {
		return fileStatusId;
	}
	public void setFileStatusId(String fileStatusId) {
		this.fileStatusId = fileStatusId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileStatusEn() {
		return fileStatusEn;
	}
	public void setFileStatusEn(String fileStatusEn) {
		this.fileStatusEn = fileStatusEn;
	}
	public String getFileStatusAr() {
		return fileStatusAr;
	}
	public void setFileStatusAr(String fileStatusAr) {
		this.fileStatusAr = fileStatusAr;
	}
	public String getWaqifId() {
		return waqifId;
	}
	public void setWaqifId(String waqifId) {
		this.waqifId = waqifId;
	}
	public String getWaqifGRPNumber() {
		return waqifGRPNumber;
	}
	public void setWaqifGRPNumber(String waqifGRPNumber) {
		this.waqifGRPNumber = waqifGRPNumber;
	}
	public String getWaqifName() {
		return waqifName;
	}
	public void setWaqifName(String waqifName) {
		this.waqifName = waqifName;
	}
	public String getEndowmentNum() {
		return endowmentNum;
	}
	public void setEndowmentNum(String endowmentNum) {
		this.endowmentNum = endowmentNum;
	}
	public String getEndowmentName() {
		return endowmentName;
	}
	public void setEndowmentName(String endowmentName) {
		this.endowmentName = endowmentName;
	}
	public String getEndowFor() {
		return endowFor;
	}
	public void setEndowFor(String endowFor) {
		this.endowFor = endowFor;
	}
	public String getAssetTypeAr() {
		return assetTypeAr;
	}
	public void setAssetTypeAr(String assetTypeAr) {
		this.assetTypeAr = assetTypeAr;
	}
	public String getAssetTypeEn() {
		return assetTypeEn;
	}
	public void setAssetTypeEn(String assetTypeEn) {
		this.assetTypeEn = assetTypeEn;
	}
	public String getEndowmentId() {
		return endowmentId;
	}
	public void setEndowmentId(String endowmentId) {
		this.endowmentId = endowmentId;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}
	public String getDescriptionEn() {
		return descriptionEn;
	}
	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}
	public String getTrxTypeEn() {
		return trxTypeEn;
	}
	public void setTrxTypeEn(String trxTypeEn) {
		this.trxTypeEn = trxTypeEn;
	}
	public String getTrxTypeAr() {
		return trxTypeAr;
	}
	public void setTrxTypeAr(String trxTypeAr) {
		this.trxTypeAr = trxTypeAr;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	
	
}