package com.avanza.pims.report.dataDef;

public class SettlementReportPaymentsDataDef extends AbstractReportDataDef {
	
	
	
	private String paymentTypeEn;
	private String paymentTypeAr;
	private String paymentDueDate;
	private String paymentMethod;
	private String paymentAmount;
	private String paymentStatusEn;
	private String paymentStatusAr;
	private String paymentDescriptionEn;
	private String paymentDescriptionAr;
	private String receiptNumber;
	private String category;
	
	public String getPaymentTypeEn() {
		return paymentTypeEn;
	}
	public void setPaymentTypeEn(String paymentTypeEn) {
		this.paymentTypeEn = paymentTypeEn;
	}
	public String getPaymentTypeAr() {
		return paymentTypeAr;
	}
	public void setPaymentTypeAr(String paymentTypeAr) {
		this.paymentTypeAr = paymentTypeAr;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentStatusEn() {
		return paymentStatusEn;
	}
	public void setPaymentStatusEn(String paymentStatusEn) {
		this.paymentStatusEn = paymentStatusEn;
	}
	public String getPaymentStatusAr() {
		return paymentStatusAr;
	}
	public void setPaymentStatusAr(String paymentStatusAr) {
		this.paymentStatusAr = paymentStatusAr;
	}
	public String getPaymentDescriptionEn() {
		return paymentDescriptionEn;
	}
	public void setPaymentDescriptionEn(String paymentDescriptionEn) {
		this.paymentDescriptionEn = paymentDescriptionEn;
	}
	public String getPaymentDescriptionAr() {
		return paymentDescriptionAr;
	}
	public void setPaymentDescriptionAr(String paymentDescriptionAr) {
		this.paymentDescriptionAr = paymentDescriptionAr;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	

	
}
