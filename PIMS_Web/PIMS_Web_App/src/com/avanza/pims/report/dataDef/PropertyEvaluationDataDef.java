package com.avanza.pims.report.dataDef;

public class PropertyEvaluationDataDef extends AbstractReportDataDef {
	
	private String propertyName;
	private String propertyNumber;
	
	private String inspectorReport;
	
	private String hosName;
	private String inspectorName;
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getInspectorReport() {
		return inspectorReport;
	}
	public void setInspectorReport(String inspectorReport) {
		this.inspectorReport = inspectorReport;
	}
	
	public String getHosName() {
		return hosName;
	}
	public void setHostName(String hosName) {
		this.hosName = hosName;
	}
	public String getInspectorName() {
		return inspectorName;
	}
	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}

}
