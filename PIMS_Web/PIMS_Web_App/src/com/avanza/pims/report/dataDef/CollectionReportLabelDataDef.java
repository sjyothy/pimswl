package com.avanza.pims.report.dataDef;

import java.util.Date;

public class CollectionReportLabelDataDef extends AbstractReportDataDef
{
	/*******************************                           English Labels             *********************************/

	private String lblTenantNameEn;
	private String lblPropertyNameEn;
	private String lblUnitNumberEn;
	private String lblContractNumberEn;
	private String lblReceiptNumberEn;
	private String lblReceiptDateEn;
	private String lblReceiptStatusEn;
	private String lblReceiptByEn;
	private String lblReceiptAmountEn;
	private String lblChequeAmountEn;
	private String lblBankNameEn;
	private String lblChequeTypeEn;
	private String lblChequeNumberEn;
	private String lblChequeDateEn;
	private String lblTransactionTypeEn;
	
	private String lblPaymentMethodEn;
	private String lblPMCashEn;
	private String lblPMDraftEn;
	private String lblChequeEn;
	

	/*******************************                           Arabic Labels             *********************************/
    private String lblTenantNameAr;
	private String lblPropertyNameAr;
	private String lblUnitNumberAr;
	private String lblContractNumberAr;
	private String lblReceiptNumberAr;
	private String lblReceiptDateAr;
	private String lblReceiptStatusAr;
	private String lblReceiptByAr;
	private String lblReceiptAmountAr;
	private String lblChequeAmountAr;
	private String lblBankNameAr;
	private String lblChequeTypeAr;
	private String lblChequeNumberAr;
	private String lblChequeDateAr;
	private String lblTransactionTypeAr;
	
	private String lblPaymentMethodAr;
	private String lblPMCashAr;
	private String lblPMDraftAr;
	private String lblChequeAr;
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;

	/***********************************SearchCriteria*******************************/
	private String enteredCriteriaReceivedBy;
	private String enteredCriteriaRemittanceAccountName;
	private String enteredCriteriaReceiptDateFrom;
	private String enteredCriteriaReceiptDateTo;
	
	
	
	
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblReceiptStatusEn() {
		return lblReceiptStatusEn;
	}
	public void setLblReceiptStatusEn(String lblReceiptStatusEn) {
		this.lblReceiptStatusEn = lblReceiptStatusEn;
	}
	public String getLblReceiptByEn() {
		return lblReceiptByEn;
	}
	public void setLblReceiptByEn(String lblReceiptByEn) {
		this.lblReceiptByEn = lblReceiptByEn;
	}
	public String getLblReceiptAmountEn() {
		return lblReceiptAmountEn;
	}
	public void setLblReceiptAmountEn(String lblReceiptAmountEn) {
		this.lblReceiptAmountEn = lblReceiptAmountEn;
	}
	public String getLblChequeAmountEn() {
		return lblChequeAmountEn;
	}
	public void setLblChequeAmountEn(String lblChequeAmountEn) {
		this.lblChequeAmountEn = lblChequeAmountEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblChequeTypeEn() {
		return lblChequeTypeEn;
	}
	public void setLblChequeTypeEn(String lblChequeTypeEn) {
		this.lblChequeTypeEn = lblChequeTypeEn;
	}
	public String getLblChequeNumberEn() {
		return lblChequeNumberEn;
	}
	public void setLblChequeNumberEn(String lblChequeNumberEn) {
		this.lblChequeNumberEn = lblChequeNumberEn;
	}
	public String getLblChequeDateEn() {
		return lblChequeDateEn;
	}
	public void setLblChequeDateEn(String lblChequeDateEn) {
		this.lblChequeDateEn = lblChequeDateEn;
	}
	public String getLblTransactionTypeEn() {
		return lblTransactionTypeEn;
	}
	public void setLblTransactionTypeEn(String lblTransactionTypeEn) {
		this.lblTransactionTypeEn = lblTransactionTypeEn;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblReceiptStatusAr() {
		return lblReceiptStatusAr;
	}
	public void setLblReceiptStatusAr(String lblReceiptStatusAr) {
		this.lblReceiptStatusAr = lblReceiptStatusAr;
	}
	public String getLblReceiptByAr() {
		return lblReceiptByAr;
	}
	public void setLblReceiptByAr(String lblReceiptByAr) {
		this.lblReceiptByAr = lblReceiptByAr;
	}
	public String getLblReceiptAmountAr() {
		return lblReceiptAmountAr;
	}
	public void setLblReceiptAmountAr(String lblReceiptAmountAr) {
		this.lblReceiptAmountAr = lblReceiptAmountAr;
	}
	public String getLblChequeAmountAr() {
		return lblChequeAmountAr;
	}
	public void setLblChequeAmountAr(String lblChequeAmountAr) {
		this.lblChequeAmountAr = lblChequeAmountAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblChequeTypeAr() {
		return lblChequeTypeAr;
	}
	public void setLblChequeTypeAr(String lblChequeTypeAr) {
		this.lblChequeTypeAr = lblChequeTypeAr;
	}
	public String getLblChequeNumberAr() {
		return lblChequeNumberAr;
	}
	public void setLblChequeNumberAr(String lblChequeNumberAr) {
		this.lblChequeNumberAr = lblChequeNumberAr;
	}
	public String getLblChequeDateAr() {
		return lblChequeDateAr;
	}
	public void setLblChequeDateAr(String lblChequeDateAr) {
		this.lblChequeDateAr = lblChequeDateAr;
	}
	public String getLblTransactionTypeAr() {
		return lblTransactionTypeAr;
	}
	public void setLblTransactionTypeAr(String lblTransactionTypeAr) {
		this.lblTransactionTypeAr = lblTransactionTypeAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblPMCashEn() {
		return lblPMCashEn;
	}
	public void setLblPMCashEn(String lblPMCashEn) {
		this.lblPMCashEn = lblPMCashEn;
	}
	public String getLblPMDraftEn() {
		return lblPMDraftEn;
	}
	public void setLblPMDraftEn(String lblPMDraftEn) {
		this.lblPMDraftEn = lblPMDraftEn;
	}
	public String getLblPMCashAr() {
		return lblPMCashAr;
	}
	public void setLblPMCashAr(String lblPMCashAr) {
		this.lblPMCashAr = lblPMCashAr;
	}
	public String getLblPMDraftAr() {
		return lblPMDraftAr;
	}
	public void setLblPMDraftAr(String lblPMDraftAr) {
		this.lblPMDraftAr = lblPMDraftAr;
	}
	public String getLblChequeEn() {
		return lblChequeEn;
	}
	public void setLblChequeEn(String lblChequeEn) {
		this.lblChequeEn = lblChequeEn;
	}
	public String getLblChequeAr() {
		return lblChequeAr;
	}
	public void setLblChequeAr(String lblChequeAr) {
		this.lblChequeAr = lblChequeAr;
	}
	public String getEnteredCriteriaReceivedBy() {
		return enteredCriteriaReceivedBy;
	}
	public void setEnteredCriteriaReceivedBy(String enteredCriteriaReceivedBy) {
		this.enteredCriteriaReceivedBy = enteredCriteriaReceivedBy;
	}
	public String getEnteredCriteriaRemittanceAccountName() {
		return enteredCriteriaRemittanceAccountName;
	}
	public void setEnteredCriteriaRemittanceAccountName(
			String enteredCriteriaRemittanceAccountName) {
		this.enteredCriteriaRemittanceAccountName = enteredCriteriaRemittanceAccountName;
	}
	public String getEnteredCriteriaReceiptDateFrom() {
		return enteredCriteriaReceiptDateFrom;
	}
	public void setEnteredCriteriaReceiptDateFrom(
			String enteredCriteriaReceiptDateFrom) {
		this.enteredCriteriaReceiptDateFrom = enteredCriteriaReceiptDateFrom;
	}
	public String getEnteredCriteriaReceiptDateTo() {
		return enteredCriteriaReceiptDateTo;
	}
	public void setEnteredCriteriaReceiptDateTo(String enteredCriteriaReceiptDateTo) {
		this.enteredCriteriaReceiptDateTo = enteredCriteriaReceiptDateTo;
	}
	
	
}
