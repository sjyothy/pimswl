package com.avanza.pims.report.dataDef;

public class MEMSPaymentReceiptReportLabelDataDef extends AbstractReportDataDef{

	/*******************************                           English Labels             *********************************/
	private String lblFileNumberEn;
	private String lblFileOwnerNameEn;
	private String lblgeneralAccountNumberEn;
	private String lblReceiptNumberEn;
	private String lblReceiptDateEn;	
	private String lblAmountEn;
	private String lblRelatedToEn;
	private String lblDescriptionEn;	
		

	/*******************************                           Arabic Labels             *********************************/

	private String lblFileNumberAr;
	private String lblFileOwnerNameAr;
	private String lblgeneralAccountNumberAr;
	private String lblReceiptNumberAr;
	private String lblReceiptDateAr;	
	private String lblAmountAr;
	private String lblRelatedToAr;
	private String lblDescriptionAr;	

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblMEMSHeaderEn;
	private String lblReportNameEn;
	private String lblMEMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblFileNumberEn() {
		return lblFileNumberEn;
	}
	public void setLblFileNumberEn(String lblFileNumberEn) {
		this.lblFileNumberEn = lblFileNumberEn;
	}
	public String getLblFileOwnerNameEn() {
		return lblFileOwnerNameEn;
	}
	public void setLblFileOwnerNameEn(String lblFileOwnerNameEn) {
		this.lblFileOwnerNameEn = lblFileOwnerNameEn;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptDateEn() {
		return lblReceiptDateEn;
	}
	public void setLblReceiptDateEn(String lblReceiptDateEn) {
		this.lblReceiptDateEn = lblReceiptDateEn;
	}
	public String getLblAmountEn() {
		return lblAmountEn;
	}
	public void setLblAmountEn(String lblAmountEn) {
		this.lblAmountEn = lblAmountEn;
	}
	public String getLblRelatedToEn() {
		return lblRelatedToEn;
	}
	public void setLblRelatedToEn(String lblRelatedToEn) {
		this.lblRelatedToEn = lblRelatedToEn;
	}
	public String getLblDescriptionEn() {
		return lblDescriptionEn;
	}
	public void setLblDescriptionEn(String lblDescriptionEn) {
		this.lblDescriptionEn = lblDescriptionEn;
	}
	public String getLblFileNumberAr() {
		return lblFileNumberAr;
	}
	public void setLblFileNumberAr(String lblFileNumberAr) {
		this.lblFileNumberAr = lblFileNumberAr;
	}
	public String getLblFileOwnerNameAr() {
		return lblFileOwnerNameAr;
	}
	public void setLblFileOwnerNameAr(String lblFileOwnerNameAr) {
		this.lblFileOwnerNameAr = lblFileOwnerNameAr;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}
	public String getLblReceiptDateAr() {
		return lblReceiptDateAr;
	}
	public void setLblReceiptDateAr(String lblReceiptDateAr) {
		this.lblReceiptDateAr = lblReceiptDateAr;
	}
	public String getLblAmountAr() {
		return lblAmountAr;
	}
	public void setLblAmountAr(String lblAmountAr) {
		this.lblAmountAr = lblAmountAr;
	}
	public String getLblRelatedToAr() {
		return lblRelatedToAr;
	}
	public void setLblRelatedToAr(String lblRelatedToAr) {
		this.lblRelatedToAr = lblRelatedToAr;
	}
	public String getLblDescriptionAr() {
		return lblDescriptionAr;
	}
	public void setLblDescriptionAr(String lblDescriptionAr) {
		this.lblDescriptionAr = lblDescriptionAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblMEMSHeaderEn() {
		return lblMEMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblMEMSHeaderEn) {
		this.lblMEMSHeaderEn = lblMEMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblMEMSHeaderAr() {
		return lblMEMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblMEMSHeaderAr) {
		this.lblMEMSHeaderAr = lblMEMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblgeneralAccountNumberEn() {
		return lblgeneralAccountNumberEn;
	}
	public void setLblgeneralAccountNumberEn(String lblgeneralAccountNumberEn) {
		this.lblgeneralAccountNumberEn = lblgeneralAccountNumberEn;
	}
	public String getLblgeneralAccountNumberAr() {
		return lblgeneralAccountNumberAr;
	}
	public void setLblgeneralAccountNumberAr(String lblgeneralAccountNumberAr) {
		this.lblgeneralAccountNumberAr = lblgeneralAccountNumberAr;
	}
	public void setLblMEMSHeaderEn(String lblMEMSHeaderEn) {
		this.lblMEMSHeaderEn = lblMEMSHeaderEn;
	}
	public void setLblMEMSHeaderAr(String lblMEMSHeaderAr) {
		this.lblMEMSHeaderAr = lblMEMSHeaderAr;
	}

}
