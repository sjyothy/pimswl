package com.avanza.pims.report.dataDef;

public class RenewContractsSummaryReportDataDef extends AbstractReportDataDef {
	
	private String contractNumber;
	private String propertyName;
	private String contractDate;
	private String occupantFirstName;
	private String occupantLastName;
	private String statusEn;
	private String statusAr;
	private String tenantFirstName;
	private String tenantMiddleName;
	private String tenantLastName;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getOccupantFirstName() {
		return occupantFirstName;
	}
	public void setOccupantFirstName(String occupantFirstName) {
		this.occupantFirstName = occupantFirstName;
	}
	public String getOccupantLastName() {
		return occupantLastName;
	}
	public void setOccupantLastName(String occupantLastName) {
		this.occupantLastName = occupantLastName;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	
	
}
