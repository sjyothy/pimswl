package com.avanza.pims.report.dataDef;

public class ClearenceLetterDataDef extends AbstractReportDataDef
{
	
	private String unitDescription;
	private String propertyName;
	private String regionNameEn;
	private String regionNameAr;
	private String landNumber;
	private String tenantName;
	private String loggedInUser;
	private String propCommercialName;
	private String rentStartDate;
	private String rentEndDate;
	private String evacuationDate;
	public String getPropCommercialName() {
		return propCommercialName;
	}
	public void setPropCommercialName(String propCommercialName) {
		this.propCommercialName = propCommercialName;
	}
	public String getUnitDescription() {
		return unitDescription;
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getRegionNameEn() {
		return regionNameEn;
	}
	public void setRegionNameEn(String regionNameEn) {
		this.regionNameEn = regionNameEn;
	}
	public String getRegionNameAr() {
		return regionNameAr;
	}
	public void setRegionNameAr(String regionNameAr) {
		this.regionNameAr = regionNameAr;
	}
	public String getRentStartDate() {
		return rentStartDate;
	}
	public void setRentStartDate(String rentStartDate) {
		this.rentStartDate = rentStartDate;
	}
	public String getRentEndDate() {
		return rentEndDate;
	}
	public void setRentEndDate(String rentEndDate) {
		this.rentEndDate = rentEndDate;
	}
	public String getEvacuationDate() {
		return evacuationDate;
	}
	public void setEvacuationDate(String evacuationDate) {
		this.evacuationDate = evacuationDate;
	}
	
}
	