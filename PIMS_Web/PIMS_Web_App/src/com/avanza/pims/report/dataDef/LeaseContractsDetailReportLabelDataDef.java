package com.avanza.pims.report.dataDef;

public class LeaseContractsDetailReportLabelDataDef extends AbstractReportDataDef {
	
    /*******************************                           English Labels             *********************************/
	private String lblContractNumberEn;
	private String lblPropertyNameEn;
	private String lblContractIssueDateEn;
	private String lblContractEndDateEn;
	private String lblOccupantNameEn;
	private String lblStatusEn;
	private String lblTenantNameEn;
	
	private String lblUnitNumberEn;
	private String lblUnitDescEn;
	private String lblUnitAreaEn;
	private String lblFloorNameEn;
	private String lblFloorNumberEn;
	private String lblRentValueEn;
	
	private String lblPaymentNoEn;
	private String lblPaymentDueDateEn;
	private String lblPaymentMethodEn;
	private String lblPaymentTypeEn;
	private String lblChequeNumEn;
	private String lblBankNameEn;
	private String lblPaymentAmountEn;
	private String lblPaymentStatusEn;
	
	private String lblReceiptNumberEn;

    /*******************************                           Arabic Labels             *********************************/

	private String lblContractNumberAr;
	private String lblPropertyNameAr;
	private String lblContractIssueDateAr;
	private String lblContractEndDateAr;
	private String lblOccupantNameAr;
	private String lblStatusAr;
	private String lblTenantNameAr;

	private String lblUnitNumberAr;
	private String lblUnitDescAr;
	private String lblUnitAreaAr;
	private String lblFloorNameAr;
	private String lblFloorNumberAr;
	private String lblRentValueAr;
	
	private String lblPaymentNoAr;
	private String lblPaymentDueDateAr;
	private String lblPaymentMethodAr;
	private String lblPaymentTypeAr;
	private String lblChequeNumAr;
	private String lblBankNameAr;
	private String lblPaymentAmountAr;
	private String lblPaymentStatusAr;
	
	private String lblReceiptNumberAr;

	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	
	public String getLblContractIssueDateEn() {
		return lblContractIssueDateEn;
	}
	public void setLblContractIssueDateEn(String lblContractIssueDateEn) {
		this.lblContractIssueDateEn = lblContractIssueDateEn;
	}
	public String getLblOccupantNameEn() {
		return lblOccupantNameEn;
	}
	public void setLblOccupantNameEn(String lblOccupantNameEn) {
		this.lblOccupantNameEn = lblOccupantNameEn;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblTenantNameEn() {
		return lblTenantNameEn;
	}
	public void setLblTenantNameEn(String lblTenantNameEn) {
		this.lblTenantNameEn = lblTenantNameEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	
	public String getLblContractIssueDateAr() {
		return lblContractIssueDateAr;
	}
	public void setLblContractIssueDateAr(String lblContractIssueDateAr) {
		this.lblContractIssueDateAr = lblContractIssueDateAr;
	}
	public String getLblOccupantNameAr() {
		return lblOccupantNameAr;
	}
	public void setLblOccupantNameAr(String lblOccupantNameAr) {
		this.lblOccupantNameAr = lblOccupantNameAr;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblTenantNameAr() {
		return lblTenantNameAr;
	}
	public void setLblTenantNameAr(String lblTenantNameAr) {
		this.lblTenantNameAr = lblTenantNameAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	public String getLblContractEndDateEn() {
		return lblContractEndDateEn;
	}
	public void setLblContractEndDateEn(String lblContractEndDateEn) {
		this.lblContractEndDateEn = lblContractEndDateEn;
	}
	public String getLblUnitNumberEn() {
		return lblUnitNumberEn;
	}
	public void setLblUnitNumberEn(String lblUnitNumberEn) {
		this.lblUnitNumberEn = lblUnitNumberEn;
	}
	public String getLblUnitDescEn() {
		return lblUnitDescEn;
	}
	public void setLblUnitDescEn(String lblUnitDescEn) {
		this.lblUnitDescEn = lblUnitDescEn;
	}
	public String getLblUnitAreaEn() {
		return lblUnitAreaEn;
	}
	public void setLblUnitAreaEn(String lblUnitAreaEn) {
		this.lblUnitAreaEn = lblUnitAreaEn;
	}
	public String getLblFloorNameEn() {
		return lblFloorNameEn;
	}
	public void setLblFloorNameEn(String lblFloorNameEn) {
		this.lblFloorNameEn = lblFloorNameEn;
	}
	public String getLblFloorNumberEn() {
		return lblFloorNumberEn;
	}
	public void setLblFloorNumberEn(String lblFloorNumberEn) {
		this.lblFloorNumberEn = lblFloorNumberEn;
	}
	public String getLblRentValueEn() {
		return lblRentValueEn;
	}
	public void setLblRentValueEn(String lblRentValueEn) {
		this.lblRentValueEn = lblRentValueEn;
	}
	public String getLblPaymentNoEn() {
		return lblPaymentNoEn;
	}
	public void setLblPaymentNoEn(String lblPaymentNoEn) {
		this.lblPaymentNoEn = lblPaymentNoEn;
	}
	public String getLblPaymentDueDateEn() {
		return lblPaymentDueDateEn;
	}
	public void setLblPaymentDueDateEn(String lblPaymentDueDateEn) {
		this.lblPaymentDueDateEn = lblPaymentDueDateEn;
	}
	public String getLblPaymentMethodEn() {
		return lblPaymentMethodEn;
	}
	public void setLblPaymentMethodEn(String lblPaymentMethodEn) {
		this.lblPaymentMethodEn = lblPaymentMethodEn;
	}
	public String getLblPaymentTypeEn() {
		return lblPaymentTypeEn;
	}
	public void setLblPaymentTypeEn(String lblPaymentTypeEn) {
		this.lblPaymentTypeEn = lblPaymentTypeEn;
	}
	public String getLblChequeNumEn() {
		return lblChequeNumEn;
	}
	public void setLblChequeNumEn(String lblChequeNumEn) {
		this.lblChequeNumEn = lblChequeNumEn;
	}
	public String getLblBankNameEn() {
		return lblBankNameEn;
	}
	public void setLblBankNameEn(String lblBankNameEn) {
		this.lblBankNameEn = lblBankNameEn;
	}
	public String getLblPaymentAmountEn() {
		return lblPaymentAmountEn;
	}
	public void setLblPaymentAmountEn(String lblPaymentAmountEn) {
		this.lblPaymentAmountEn = lblPaymentAmountEn;
	}
	public String getLblPaymentStatusEn() {
		return lblPaymentStatusEn;
	}
	public void setLblPaymentStatusEn(String lblPaymentStatusEn) {
		this.lblPaymentStatusEn = lblPaymentStatusEn;
	}
	public String getLblContractEndDateAr() {
		return lblContractEndDateAr;
	}
	public void setLblContractEndDateAr(String lblContractEndDateAr) {
		this.lblContractEndDateAr = lblContractEndDateAr;
	}
	public String getLblUnitNumberAr() {
		return lblUnitNumberAr;
	}
	public void setLblUnitNumberAr(String lblUnitNumberAr) {
		this.lblUnitNumberAr = lblUnitNumberAr;
	}
	public String getLblUnitDescAr() {
		return lblUnitDescAr;
	}
	public void setLblUnitDescAr(String lblUnitDescAr) {
		this.lblUnitDescAr = lblUnitDescAr;
	}
	public String getLblUnitAreaAr() {
		return lblUnitAreaAr;
	}
	public void setLblUnitAreaAr(String lblUnitAreaAr) {
		this.lblUnitAreaAr = lblUnitAreaAr;
	}
	public String getLblFloorNameAr() {
		return lblFloorNameAr;
	}
	public void setLblFloorNameAr(String lblFloorNameAr) {
		this.lblFloorNameAr = lblFloorNameAr;
	}
	public String getLblFloorNumberAr() {
		return lblFloorNumberAr;
	}
	public void setLblFloorNumberAr(String lblFloorNumberAr) {
		this.lblFloorNumberAr = lblFloorNumberAr;
	}
	public String getLblRentValueAr() {
		return lblRentValueAr;
	}
	public void setLblRentValueAr(String lblRentValueAr) {
		this.lblRentValueAr = lblRentValueAr;
	}
	public String getLblPaymentNoAr() {
		return lblPaymentNoAr;
	}
	public void setLblPaymentNoAr(String lblPaymentNoAr) {
		this.lblPaymentNoAr = lblPaymentNoAr;
	}
	public String getLblPaymentDueDateAr() {
		return lblPaymentDueDateAr;
	}
	public void setLblPaymentDueDateAr(String lblPaymentDueDateAr) {
		this.lblPaymentDueDateAr = lblPaymentDueDateAr;
	}
	public String getLblPaymentMethodAr() {
		return lblPaymentMethodAr;
	}
	public void setLblPaymentMethodAr(String lblPaymentMethodAr) {
		this.lblPaymentMethodAr = lblPaymentMethodAr;
	}
	public String getLblPaymentTypeAr() {
		return lblPaymentTypeAr;
	}
	public void setLblPaymentTypeAr(String lblPaymentTypeAr) {
		this.lblPaymentTypeAr = lblPaymentTypeAr;
	}
	public String getLblChequeNumAr() {
		return lblChequeNumAr;
	}
	public void setLblChequeNumAr(String lblChequeNumAr) {
		this.lblChequeNumAr = lblChequeNumAr;
	}
	public String getLblBankNameAr() {
		return lblBankNameAr;
	}
	public void setLblBankNameAr(String lblBankNameAr) {
		this.lblBankNameAr = lblBankNameAr;
	}
	public String getLblPaymentAmountAr() {
		return lblPaymentAmountAr;
	}
	public void setLblPaymentAmountAr(String lblPaymentAmountAr) {
		this.lblPaymentAmountAr = lblPaymentAmountAr;
	}
	public String getLblPaymentStatusAr() {
		return lblPaymentStatusAr;
	}
	public void setLblPaymentStatusAr(String lblPaymentStatusAr) {
		this.lblPaymentStatusAr = lblPaymentStatusAr;
	}
	public String getLblReceiptNumberEn() {
		return lblReceiptNumberEn;
	}
	public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
		this.lblReceiptNumberEn = lblReceiptNumberEn;
	}
	public String getLblReceiptNumberAr() {
		return lblReceiptNumberAr;
	}
	public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
		this.lblReceiptNumberAr = lblReceiptNumberAr;
	}

	
	
}
