package com.avanza.pims.report.dataDef;

public class PersonDetailsDataDef extends AbstractReportDataDef
{
	   private String name;
	   private String personId;
	   private String fileNumber;
	   private String costCenter;
	   private String oldCostCenter;
	   private String field1;
	   private String field2;
	   private String field3;
	   private String field4;
	   private String field5;
	   private String field6;
	   private String field7;
	   private String field8;
	   private String field9;
	   private String field10;
	   private Double blockingAmount;
	   private Double balance;
	   private Double unUsedUnblock;
	   private Double finalBalance;
	   private Double retainedProfitBalance;
	   
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getOldCostCenter() {
		return oldCostCenter;
	}
	public void setOldCostCenter(String oldCostCenter) {
		this.oldCostCenter = oldCostCenter;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public Double getBlockingAmount() {
		return blockingAmount;
	}
	public void setBlockingAmount(Double blockingAmount) {
		this.blockingAmount = blockingAmount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getUnUsedUnblock() {
		return unUsedUnblock;
	}
	public void setUnUsedUnblock(Double unUsedUnblock) {
		this.unUsedUnblock = unUsedUnblock;
	}
	public Double getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(Double finalBalance) {
		this.finalBalance = finalBalance;
	}
	public Double getRetainedProfitBalance() {
		return retainedProfitBalance;
	}
	public void setRetainedProfitBalance(Double retainedProfitBalance) {
		this.retainedProfitBalance = retainedProfitBalance;
	}
	

}
