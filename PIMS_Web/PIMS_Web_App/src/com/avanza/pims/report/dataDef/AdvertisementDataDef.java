package com.avanza.pims.report.dataDef;

import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

public class AdvertisementDataDef extends AbstractReportDataDef
{
	
	private String unitDescription;
	private String propertyName;
	private String regionNameEn;
	private String regionNameAr;
	private String landNumber;
	private String tenantName;
	private String loggedInUser;
	private String propCommercialName;
	private String unitNumber;
	private String unitArea;
	private String openingPrice;
	private String cityEn;
	private String cityAr;
	private String communityUnitNo;
	private String amafAddressEn;
	private String amafAddressAr;
	private String depositAmount;
	private String depositMax;
	private String registrationEndDate;
	private String incrementalAmount;
	private String confiscationPercentage;
	private String auctionDateTime;
	private String dateFormat;

	
	
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getUnitArea() {
		return unitArea;
	}
	public void setUnitArea(String unitArea) {
		this.unitArea = unitArea;
	}
	public String getOpeningPrice() {
		return openingPrice;
	}
	public void setOpeningPrice(String openingPrice) {
		this.openingPrice = openingPrice;
	}
	public String getCityEn() {
		return cityEn;
	}
	public void setCityEn(String cityEn) {
		this.cityEn = cityEn;
	}
	public String getCityAr() {
		return cityAr;
	}
	public void setCityAr(String cityAr) {
		this.cityAr = cityAr;
	}
	public String getCommunityUnitNo() {
		return communityUnitNo;
	}
	public void setCommunityUnitNo(String communityUnitNo) {
		this.communityUnitNo = communityUnitNo;
	}
	public String getAmafAddressEn()
	{
		return amafAddressEn;
	}
	public void setAmafAddressEn(String amafAddressEn) {
		this.amafAddressEn = amafAddressEn;
	}
	public String getAmafAddressAr() {
		return amafAddressAr;
	}
	public void setAmafAddressAr(String amafAddressAr) {
		this.amafAddressAr = amafAddressAr;
	}
	public String getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getDepositMax() {
		return depositMax;
	}
	public void setDepositMax(String depositMax) {
		this.depositMax = depositMax;
	}
	
	public String getIncrementalAmount() {
		return incrementalAmount;
	}
	public void setIncrementalAmount(String incrementalAmount) {
		this.incrementalAmount = incrementalAmount;
	}
	public String getConfiscationPercentage() {
		return confiscationPercentage;
	}
	public void setConfiscationPercentage(String confiscationPercentage) {
		this.confiscationPercentage = confiscationPercentage;
	}
	public String getPropCommercialName() {
		return propCommercialName;
	}
	public void setPropCommercialName(String propCommercialName) {
		this.propCommercialName = propCommercialName;
	}
	public String getUnitDescription() {
		return unitDescription;
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getRegionNameEn() {
		return regionNameEn;
	}
	public void setRegionNameEn(String regionNameEn) {
		this.regionNameEn = regionNameEn;
	}
	public String getRegionNameAr() {
		return regionNameAr;
	}
	public void setRegionNameAr(String regionNameAr) {
		this.regionNameAr = regionNameAr;
	}

	public String getDateFormat()
	{
		dateFormat=getDateFormat();
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public String getRegistrationEndDate() {
		return registrationEndDate;
	}
	public void setRegistrationEndDate(String registrationEndDate) {
		this.registrationEndDate = registrationEndDate;
	}
	public String getAuctionDateTime() {
		return auctionDateTime;
	}
	public void setAuctionDateTime(String auctionDateTime) {
		this.auctionDateTime = auctionDateTime;
	}
	

	
	
}
	