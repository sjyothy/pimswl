package com.avanza.pims.report.dataDef;

public class ChequeDetailsListReportDataDef extends AbstractReportDataDef
{
	private String tenantId;
	private String tenantTypeEn;
	private String tenantTypeAr;
	private String contractNumber;
	private String contractId;
	private String contractStartDate;
	private String contractEndDate;
	private String unitNumber;
	private String propertyName;
	private String tenantName;
	private String receiptNumber;
	private String receiptDate;
	private String chequeNumber;
	private String chequeDueDate;
	private String chequeOwnerName;
	private String chequeStatusEn;
	private String chequeStatusAr;
	private String chequeTypeEn;
	private String chequeTypeAr;
	private String chequeBankNameEn;
	private String chequeBankNameAr;
	private String chequeAmount;
	private String totalAmount;
	private String paymentType;
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		
		this.contractNumber = contractNumber;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeDueDate() {
		return chequeDueDate;
	}
	public void setChequeDueDate(String chequeDueDate) {
		this.chequeDueDate = chequeDueDate;
	}
	public String getChequeOwnerName() {
		return chequeOwnerName;
	}
	public void setChequeOwnerName(String chequeOwnerName) {
		this.chequeOwnerName = chequeOwnerName;
	}
	
	public String getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getChequeStatusEn() {
		return chequeStatusEn;
	}
	public void setChequeStatusEn(String chequeStatusEn) {
		this.chequeStatusEn = chequeStatusEn;
	}
	public String getChequeStatusAr() {
		return chequeStatusAr;
	}
	public void setChequeStatusAr(String chequeStatusAr) {
		this.chequeStatusAr = chequeStatusAr;
	}
	public String getChequeBankNameEn() {
		return chequeBankNameEn;
	}
	public void setChequeBankNameEn(String chequeBankNameEn) {
		this.chequeBankNameEn = chequeBankNameEn;
	}
	public String getChequeBankNameAr() {
		return chequeBankNameAr;
	}
	public void setChequeBankNameAr(String chequeBankNameAr) {
		this.chequeBankNameAr = chequeBankNameAr;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantTypeEn() {
		return tenantTypeEn;
	}
	public void setTenantTypeEn(String tenantTypeEn) {
		this.tenantTypeEn = tenantTypeEn;
	}
	public String getTenantTypeAr() {
		return tenantTypeAr;
	}
	public void setTenantTypeAr(String tenantTypeAr) {
		this.tenantTypeAr = tenantTypeAr;
	}
	public String getChequeTypeEn() {
		return chequeTypeEn;
	}
	public void setChequeTypeEn(String chequeTypeEn) {
		this.chequeTypeEn = chequeTypeEn;
	}
	public String getChequeTypeAr() {
		return chequeTypeAr;
	}
	public void setChequeTypeAr(String chequeTypeAr) {
		this.chequeTypeAr = chequeTypeAr;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	

}
