package com.avanza.pims.report.dataDef;

public class ApplicationSummaryReportLabelDataDef extends AbstractReportDataDef 
{
	private String lblApplicationNumberEn;
	private String lblApplicationStatusEn;
	private String lblApplicantNameEn;
	private String lblApplicationTypeEn;
	private String lblApplicationDateFromEn;
	private String lblApplicationDescriptionEn;
	
	
	/*******************************                           Arabic Labels             *********************************/
	
	private String lblApplicationNumberAr;
	private String lblApplicationStatusAr;
	private String lblApplicantNameAr;
	private String lblApplicationTypeAr;
	private String lblApplicationDateFromAr;
	private String lblApplicationDescriptionAr;


	////////Report and PIMS Header///////////////
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	
	//////////////////////

	
	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;

	public String getLblApplicationNumberEn() {
		return lblApplicationNumberEn;
	}

	public void setLblApplicationNumberEn(String lblApplicationNumberEn) {
		this.lblApplicationNumberEn = lblApplicationNumberEn;
	}

	public String getLblApplicationStatusEn() {
		return lblApplicationStatusEn;
	}

	public void setLblApplicationStatusEn(String lblApplicationStatusEn) {
		this.lblApplicationStatusEn = lblApplicationStatusEn;
	}

	public String getLblApplicantNameEn() {
		return lblApplicantNameEn;
	}

	public void setLblApplicantNameEn(String lblApplicantNameEn) {
		this.lblApplicantNameEn = lblApplicantNameEn;
	}

	public String getLblApplicationTypeEn() {
		return lblApplicationTypeEn;
	}

	public void setLblApplicationTypeEn(String lblApplicationTypeEn) {
		this.lblApplicationTypeEn = lblApplicationTypeEn;
	}

	public String getLblApplicationDateFromEn() {
		return lblApplicationDateFromEn;
	}

	public void setLblApplicationDateFromEn(String lblApplicationDateFromEn) {
		this.lblApplicationDateFromEn = lblApplicationDateFromEn;
	}

	public String getLblApplicationDescriptionEn() {
		return lblApplicationDescriptionEn;
	}

	public void setLblApplicationDescriptionEn(String lblApplicationDescriptionEn) {
		this.lblApplicationDescriptionEn = lblApplicationDescriptionEn;
	}

	public String getLblApplicationNumberAr() {
		return lblApplicationNumberAr;
	}

	public void setLblApplicationNumberAr(String lblApplicationNumberAr) {
		this.lblApplicationNumberAr = lblApplicationNumberAr;
	}

	public String getLblApplicationStatusAr() {
		return lblApplicationStatusAr;
	}

	public void setLblApplicationStatusAr(String lblApplicationStatusAr) {
		this.lblApplicationStatusAr = lblApplicationStatusAr;
	}

	public String getLblApplicantNameAr() {
		return lblApplicantNameAr;
	}

	public void setLblApplicantNameAr(String lblApplicantNameAr) {
		this.lblApplicantNameAr = lblApplicantNameAr;
	}

	public String getLblApplicationTypeAr() {
		return lblApplicationTypeAr;
	}

	public void setLblApplicationTypeAr(String lblApplicationTypeAr) {
		this.lblApplicationTypeAr = lblApplicationTypeAr;
	}

	public String getLblApplicationDateFromAr() {
		return lblApplicationDateFromAr;
	}

	public void setLblApplicationDateFromAr(String lblApplicationDateFromAr) {
		this.lblApplicationDateFromAr = lblApplicationDateFromAr;
	}

	public String getLblApplicationDescriptionAr() {
		return lblApplicationDescriptionAr;
	}

	public void setLblApplicationDescriptionAr(String lblApplicationDescriptionAr) {
		this.lblApplicationDescriptionAr = lblApplicationDescriptionAr;
	}

	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}

	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}

	public String getLblReportNameEn() {
		return lblReportNameEn;
	}

	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}

	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}

	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}

	public String getLblReportNameAr() {
		return lblReportNameAr;
	}

	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}

	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}

	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
}
	

	