package com.avanza.pims.report.dataDef;

public class LeaseContractsSummaryReportDataDef extends AbstractReportDataDef {
	
	private String contractNumber;
	private String propertyName;
	private String contractDate;
	private String occupantFirstName;
	private String occupantLastName;
	private String statusEn;
	private String statusAr;
	private String tenantFirstName;
	private String tenantMiddleName;
	private String tenantLastName;
	private String companyName;
	private String contractStartDate;
	private String contractEndDate;
	private String contractAmount;
	private String unitNo;
	private String unitDescription;
	private String unitCostCenter;
	private String phoneNumber;
	private String tenantCellNumber;
	private String contactInfoId;
	private String contractId;
	private String personId;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String cityName;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getOccupantFirstName() {
		return occupantFirstName;
	}
	public void setOccupantFirstName(String occupantFirstName) {
		this.occupantFirstName = occupantFirstName;
	}
	public String getOccupantLastName() {
		return occupantLastName;
	}
	public void setOccupantLastName(String occupantLastName) {
		this.occupantLastName = occupantLastName;
	}
	public String getStatusEn() {
		return statusEn;
	}
	public void setStatusEn(String statusEn) {
		this.statusEn = statusEn;
	}
	public String getStatusAr() {
		return statusAr;
	}
	public void setStatusAr(String statusAr) {
		this.statusAr = statusAr;
	}
	public String getTenantFirstName() {
		return tenantFirstName;
	}
	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}
	public String getTenantMiddleName() {
		return tenantMiddleName;
	}
	public void setTenantMiddleName(String tenantMiddleName) {
		this.tenantMiddleName = tenantMiddleName;
	}
	public String getTenantLastName() {
		return tenantLastName;
	}
	public void setTenantLastName(String tenantLastName) {
		this.tenantLastName = tenantLastName;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}
	public String getUnitDescription() {
		return unitDescription;
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}
	public String getUnitCostCenter() {
		return unitCostCenter;
	}
	public void setUnitCostCenter(String unitCostCenter) {
		this.unitCostCenter = unitCostCenter;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTenantCellNumber() {
		return tenantCellNumber;
	}
	public void setTenantCellNumber(String tenantCellNumber) {
		this.tenantCellNumber = tenantCellNumber;
	}
	public String getContactInfoId() {
		return contactInfoId;
	}
	public void setContactInfoId(String contactInfoId) {
		this.contactInfoId = contactInfoId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
}
