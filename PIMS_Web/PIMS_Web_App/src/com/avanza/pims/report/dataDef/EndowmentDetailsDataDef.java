package com.avanza.pims.report.dataDef;


public class EndowmentDetailsDataDef extends AbstractReportDataDef
{
	   private String name;
	   private String fromDate;
	   private String toDate;
	   private String endowmentId;
	   private String endowmentNumber;
	   private String costCenter;
	   private String field1;
	   private String field2;
	   private String field3;
	   private String field4;
	   private String field5;
	   private String field6;
	   private String field7;
	   private String field8;
	   private String field9;
	   private String field10;
	   private Double openingBalance;
	   private Double balance;
	   private Double finalBalance;
	   private Double field11;
	   private Double field12;
	   private Double field13;
	   private Double field14;
	   private Double field15;
	   private Double field16;
	   
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEndowmentId() {
		return endowmentId;
	}
	public void setEndowmentId(String endowmentId) {
		this.endowmentId = endowmentId;
	}
	public String getEndowmentNumber() {
		return endowmentNumber;
	}
	public void setEndowmentNumber(String endowmentNumber) {
		this.endowmentNumber = endowmentNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(Double finalBalance) {
		this.finalBalance = finalBalance;
	}
	public Double getField11() {
		return field11;
	}
	public void setField11(Double field11) {
		this.field11 = field11;
	}
	public Double getField12() {
		return field12;
	}
	public void setField12(Double field12) {
		this.field12 = field12;
	}
	public Double getField13() {
		return field13;
	}
	public void setField13(Double field13) {
		this.field13 = field13;
	}
	public Double getField14() {
		return field14;
	}
	public void setField14(Double field14) {
		this.field14 = field14;
	}
	public Double getField15() {
		return field15;
	}
	public void setField15(Double field15) {
		this.field15 = field15;
	}
	public Double getField16() {
		return field16;
	}
	public void setField16(Double field16) {
		this.field16 = field16;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}

