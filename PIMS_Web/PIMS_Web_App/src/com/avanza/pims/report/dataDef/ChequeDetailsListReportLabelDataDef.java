package com.avanza.pims.report.dataDef;

public class ChequeDetailsListReportLabelDataDef extends AbstractReportDataDef
{
		private String lblTenantTypeEn;
		private String lblTenantIdEn;
 		private String lblContractNumberEn;
		private String lblContractStartDateEn;
		private String lblContractEndDateEn;
		private String lblUnitNumberEn;
		private String lblPropertyNameEn;
		private String lblTenantNameEn;
		private String lblReceiptNumberEn;
		private String lblReceiptDateEn;
		private String lblChequeNumberEn;
		private String lblChequeDueDateEn;
		private String lblChequeOwnerNameEn;
		private String lblStatusEn;
		private String lblChequeTypeEn;
		private String lblChequeBankNameEn;
		private String lblChequeAmountEn;
		private String lblTotalAmountEn;
		////////////////////////////
		private String lblTenantTypeAr;
		private String lblTenantIdAr;
		private String lblContractNumberAr;
		private String lblContractStartDateAr;
		private String lblContractEndDateAr;
		private String lblUnitNumberAr;
		private String lblPropertyNameAr;
		private String lblTenantNameAr;
		private String lblReceiptNumberAr;
		private String lblReceiptDateAr;
		private String lblChequeNumberAr;
		private String lblChequeDueDateAr;
		private String lblChequeOwnerNameAr;
		private String lblStatusAr;
		private String  lblChequeTypeAr;
		private String lblChequeBankNameAr;
		private String lblChequeAmountAr;
		private String lblTotalAmountAr;
		////////Report and PIMS Header///////////////
		private String lblPIMSHeaderEn;
		private String lblChequeDetailsReportEn;
		private String lblPIMSHeaderAr;
		private String lblChequeDetailsReportAr;
		private String lblLoggedInUser;
		public String getLblLoggedInUser() {
			return lblLoggedInUser;
		}
		public void setLblLoggedInUser(String lblLoggedInUser) {
			this.lblLoggedInUser = lblLoggedInUser;
		}
		//////////////////////
		public String getLblContractNumberEn() {
			return lblContractNumberEn;
		}
		public void setLblContractNumberEn(String lblContractNumberEn) {
			this.lblContractNumberEn = lblContractNumberEn;
		}
		public String getLblContractStartDateEn() {
			return lblContractStartDateEn;
		}
		public void setLblContractStartDateEn(String lblContractStartDateEn) {
			this.lblContractStartDateEn = lblContractStartDateEn;
		}
		public String getLblContractEndDateEn() {
			return lblContractEndDateEn;
		}
		public void setLblContractEndDateEn(String lblContractEndDateEn) {
			this.lblContractEndDateEn = lblContractEndDateEn;
		}
		public String getLblUnitNumberEn() {
			return lblUnitNumberEn;
		}
		public void setLblUnitNumberEn(String lblUnitNumberEn) {
			this.lblUnitNumberEn = lblUnitNumberEn;
		}
		public String getLblPropertyNameEn() {
			return lblPropertyNameEn;
		}
		public void setLblPropertyNameEn(String lblPropertyNameEn) {
			this.lblPropertyNameEn = lblPropertyNameEn;
		}
		public String getLblTenantNameEn() {
			return lblTenantNameEn;
		}
		public void setLblTenantNameEn(String lblTenantNameEn) {
			this.lblTenantNameEn = lblTenantNameEn;
		}
		public String getLblReceiptNumberEn() {
			return lblReceiptNumberEn;
		}
		public void setLblReceiptNumberEn(String lblReceiptNumberEn) {
			this.lblReceiptNumberEn = lblReceiptNumberEn;
		}
		public String getLblReceiptDateEn() {
			return lblReceiptDateEn;
		}
		public void setLblReceiptDateEn(String lblReceiptDateEn) {
			this.lblReceiptDateEn = lblReceiptDateEn;
		}
		public String getLblChequeNumberEn() {
			return lblChequeNumberEn;
		}
		public void setLblChequeNumberEn(String lblChequeNumberEn) {
			this.lblChequeNumberEn = lblChequeNumberEn;
		}
		public String getLblChequeDueDateEn() {
			return lblChequeDueDateEn;
		}
		public void setLblChequeDueDateEn(String lblChequeDueDateEn) {
			this.lblChequeDueDateEn = lblChequeDueDateEn;
		}
		public String getLblChequeOwnerNameEn() {
			return lblChequeOwnerNameEn;
		}
		public void setLblChequeOwnerNameEn(String lblChequeOwnerNameEn) {
			this.lblChequeOwnerNameEn = lblChequeOwnerNameEn;
		}
		public String getLblStatusEn() {
			return lblStatusEn;
		}
		public void setLblStatusEn(String lblStatusEn) {
			this.lblStatusEn = lblStatusEn;
		}
		public String getLblChequeTypeEn() {
			return lblChequeTypeEn;
		}
		public void setLblChequeTypeEn(String lblChequeTypeEn) {
			this.lblChequeTypeEn = lblChequeTypeEn;
		}
		public String getLblChequeBankNameEn() {
			return lblChequeBankNameEn;
		}
		public void setLblChequeBankNameEn(String lblChequeBankNameEn) {
			this.lblChequeBankNameEn = lblChequeBankNameEn;
		}
		public String getLblChequeAmountEn() {
			return lblChequeAmountEn;
		}
		public void setLblChequeAmountEn(String lblChequeAmountEn) {
			this.lblChequeAmountEn = lblChequeAmountEn;
		}
		public String getLblTotalAmountEn() {
			return lblTotalAmountEn;
		}
		public void setLblTotalAmountEn(String lblTotalAmountEn) {
			this.lblTotalAmountEn = lblTotalAmountEn;
		}
		public String getLblContractNumberAr() {
			return lblContractNumberAr;
		}
		public void setLblContractNumberAr(String lblContractNumberAr) {
			this.lblContractNumberAr = lblContractNumberAr;
		}
		public String getLblContractStartDateAr() {
			return lblContractStartDateAr;
		}
		public void setLblContractStartDateAr(String lblContractStartDateAr) {
			this.lblContractStartDateAr = lblContractStartDateAr;
		}
		public String getLblContractEndDateAr() {
			return lblContractEndDateAr;
		}
		public void setLblContractEndDateAr(String lblContractEndDateAr) {
			this.lblContractEndDateAr = lblContractEndDateAr;
		}
		public String getLblUnitNumberAr() {
			return lblUnitNumberAr;
		}
		public void setLblUnitNumberAr(String lblUnitNumberAr) {
			this.lblUnitNumberAr = lblUnitNumberAr;
		}
		public String getLblPropertyNameAr() {
			return lblPropertyNameAr;
		}
		public void setLblPropertyNameAr(String lblPropertyNameAr) {
			this.lblPropertyNameAr = lblPropertyNameAr;
		}
		public String getLblTenantNameAr() {
			return lblTenantNameAr;
		}
		public void setLblTenantNameAr(String lblTenantNameAr) {
			this.lblTenantNameAr = lblTenantNameAr;
		}
		public String getLblReceiptNumberAr() {
			return lblReceiptNumberAr;
		}
		public void setLblReceiptNumberAr(String lblReceiptNumberAr) {
			this.lblReceiptNumberAr = lblReceiptNumberAr;
		}
		public String getLblReceiptDateAr() {
			return lblReceiptDateAr;
		}
		public void setLblReceiptDateAr(String lblReceiptDateAr) {
			this.lblReceiptDateAr = lblReceiptDateAr;
		}
		public String getLblChequeNumberAr() {
			return lblChequeNumberAr;
		}
		public void setLblChequeNumberAr(String lblChequeNumberAr) {
			this.lblChequeNumberAr = lblChequeNumberAr;
		}
		public String getLblChequeDueDateAr() {
			return lblChequeDueDateAr;
		}
		public void setLblChequeDueDateAr(String lblChequeDueDateAr) {
			this.lblChequeDueDateAr = lblChequeDueDateAr;
		}
		public String getLblChequeOwnerNameAr() {
			return lblChequeOwnerNameAr;
		}
		public void setLblChequeOwnerNameAr(String lblChequeOwnerNameAr) {
			this.lblChequeOwnerNameAr = lblChequeOwnerNameAr;
		}
		public String getLblStatusAr() {
			return lblStatusAr;
		}
		public void setLblStatusAr(String lblStatusAr) {
			this.lblStatusAr = lblStatusAr;
		}
		public String getLblChequeTypeAr() {
			return lblChequeTypeAr;
		}
		public void setLblChequeTypeAr(String lblChequeTypeAr) {
			this.lblChequeTypeAr = lblChequeTypeAr;
		}
		public String getLblChequeBankNameAr() {
			return lblChequeBankNameAr;
		}
		public void setLblChequeBankNameAr(String lblChequeBankNameAr) {
			this.lblChequeBankNameAr = lblChequeBankNameAr;
		}
		public String getLblChequeAmountAr() {
			return lblChequeAmountAr;
		}
		public void setLblChequeAmountAr(String lblChequeAmountAr) {
			this.lblChequeAmountAr = lblChequeAmountAr;
		}
		public String getLblTotalAmountAr() {
			return lblTotalAmountAr;
		}
		public void setLblTotalAmountAr(String lblTotalAmountAr) {
			this.lblTotalAmountAr = lblTotalAmountAr;
		}
		public String getLblPIMSHeaderEn() {
			return lblPIMSHeaderEn;
		}
		public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
			this.lblPIMSHeaderEn = lblPIMSHeaderEn;
		}
		public String getLblChequeDetailsReportEn() {
			return lblChequeDetailsReportEn;
		}
		public void setLblChequeDetailsReportEn(String lblChequeDetailsReportEn) {
			this.lblChequeDetailsReportEn = lblChequeDetailsReportEn;
		}
		public String getLblPIMSHeaderAr() {
			return lblPIMSHeaderAr;
		}
		public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
			this.lblPIMSHeaderAr = lblPIMSHeaderAr;
		}
		public String getLblChequeDetailsReportAr() {
			return lblChequeDetailsReportAr;
		}
		public void setLblChequeDetailsReportAr(String lblChequeDetailsReportAr) {
			this.lblChequeDetailsReportAr = lblChequeDetailsReportAr;
		}
		public String getLblTenantTypeEn() {
			return lblTenantTypeEn;
		}
		public void setLblTenantTypeEn(String lblTenantTypeEn) {
			this.lblTenantTypeEn = lblTenantTypeEn;
		}
		public String getLblTenantIdEn() {
			return lblTenantIdEn;
		}
		public void setLblTenantIdEn(String lblTenantIdEn) {
			this.lblTenantIdEn = lblTenantIdEn;
		}
		public String getLblTenantTypeAr() {
			return lblTenantTypeAr;
		}
		public void setLblTenantTypeAr(String lblTenantTypeAr) {
			this.lblTenantTypeAr = lblTenantTypeAr;
		}
		public String getLblTenantIdAr() {
			return lblTenantIdAr;
		}
		public void setLblTenantIdAr(String lblTenantIdAr) {
			this.lblTenantIdAr = lblTenantIdAr;
		}

}
