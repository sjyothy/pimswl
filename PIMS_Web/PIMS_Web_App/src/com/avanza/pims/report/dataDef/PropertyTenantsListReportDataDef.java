package com.avanza.pims.report.dataDef;


public class PropertyTenantsListReportDataDef extends AbstractReportDataDef {
	
	private String propertyNumber;
	private String propertyTypeEn;
	private String propertyTypeAr;
	private String propertyName;
	private String address;
	private String emirate;
	private String tenantName;
	private String startDate;
	private String endDate;
	private String contractNumber;
	private String contractValue; 
	private String rentalIncome;
	private String prepaidRent;
	private String rentAnnual;
	private String totalRentalIncome;
	private String totalPrepaidRent;
	private String costCenter;
	private String community;
	private String unitNumber;
	
	public String getPropertyTypeEn() {
		return propertyTypeEn;
	}
	public void setPropertyTypeEn(String propertyTypeEn) {
		this.propertyTypeEn = propertyTypeEn;
	}
	public String getPropertyTypeAr() {
		return propertyTypeAr;
	}
	public void setPropertyTypeAr(String propertyTypeAr) {
		this.propertyTypeAr = propertyTypeAr;
	}
	
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractValue() {
		return contractValue;
	}
	public void setContractValue(String contractValue) {
		this.contractValue = contractValue;
	}
	public String getRentalIncome() {
		return rentalIncome;
	}
	public void setRentalIncome(String rentalIncome) {
		this.rentalIncome = rentalIncome;
	}
	public String getPrepaidRent() {
		return prepaidRent;
	}
	public void setPrepaidRent(String prepaidRent) {
		this.prepaidRent = prepaidRent;
	}
	public String getRentAnnual() {
		return rentAnnual;
	}
	public void setRentAnnual(String rentAnnual) {
		this.rentAnnual = rentAnnual;
	}
	public String getTotalRentalIncome() {
		return totalRentalIncome;
	}
	public void setTotalRentalIncome(String totalRentalIncome) {
		this.totalRentalIncome = totalRentalIncome;
	}
	public String getTotalPrepaidRent() {
		return totalPrepaidRent;
	}
	public void setTotalPrepaidRent(String totalPrepaidRent) {
		this.totalPrepaidRent = totalPrepaidRent;
	}
	public String getPropertyNumber() {
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	
	
}
