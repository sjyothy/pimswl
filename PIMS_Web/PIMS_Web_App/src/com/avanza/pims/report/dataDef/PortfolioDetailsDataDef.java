package com.avanza.pims.report.dataDef;

public class PortfolioDetailsDataDef extends AbstractReportDataDef
{
 private int parameterCurrQuarter;
 private int parameterPrvQuarter;
 private String ACCOUNT_TYPE_EN;
 private String ACCOUNT_TYPE_AR;
 private String ACCOUNT_PERCENTAGE; 
 private String ASSET_CLASS_NAME_EN;
 private String ASSET_CLASS_NAME_AR;
 private String PORTFOLIO_NAME_EN;
 private String PORTFOLIO_NAME_AR;
 private String TOTAL_INVESTMENT_COST;
 private String PORTFOLIO_FEES;
 private String ADDITIONAL_EXPENSES;
 private String UNIT_PRICE;
 private String TOTAL_UNITS;
 private String EVALUATION_TYPE_ID;
 private String EVALUATION_VALUE;
 private String EVALUATION_UNIT_VALUE;
 private String EVALUATION_NUMBER_OF_UNIT;
 private String EVALUATION_QUARTER_ID;
 private String EVALUATION_YEAR;
 private String PROFIT_VALUE;
 private String PROFIT_TO_DATE;
 private String PROFIT_FROM_DATE;
 private String ASSET_CLASS_ID;
 private String PARENT_ASSET_CLASS_ID;
 private String ASSET_CLASS_LEVEL;
 private String ASSET_CLASS_NUMBER;
 private String ASSET_CLASS_DESC;
 private String STATUS_ID;
 private String ASSET_ID;
 private String CURRENCY_ID; 
 private String ASSET_SYMBOL;
 private String ASSET_NAME_EN;
 private String ASSET_NAME_AR;
 private String ASSET_DESC;
 private String ASSET_NUMBER;
 private String ASSET_STATUS;
 private String ASSET_SECTOR_ID;
 private String ASSET_SUB_SECTOR_ID;
 private String EXPIRY_DATE;
 private String ASSET_SUB_CLASS_ID;
 private String PORTFOLIO_ASSET_ID;
 private String PORTFOLIO_ID;
 private String PORTFOLIO_DESC_EN;
 private String PORTFOLIO_DESC_AR;
 private String PORTFOLIO_NUMBER;
 private String PORTFOLIO_TYPE_ID;
 private String MARKET_SECTOR;
 private String ACCOUNT_TYPE_ID;
 private String PRINCIPAL_AMOUNT;
 private String FUNDS_DURATION_START_DATE;;
 private String FUNDS_DURATION_END_DATE;
 private String ACCOUNT_DESC;
private String TOTAL_VALUES_OF_INVESTMENT; 
 private String field1;
 private String field2;
 private String field3;
 private String field4;
 private String field6;
 private String field7;
 private String field8;
 private String field9;
 private String field10;
 private String field11;
 private String field12;
 private String field14;
 private String field13;
 private String field15;
 private String field16;
 private String field17;
 private String field18;
 private String field19;
 private String field20;
 private String PORTFOLIO_PROFIT_ID;
 
 
 
 
 
 
 
public String getPORTFOLIO_PROFIT_ID() {
	return PORTFOLIO_PROFIT_ID;
}
public void setPORTFOLIO_PROFIT_ID(String portfolio_profit_id) {
	PORTFOLIO_PROFIT_ID = portfolio_profit_id;
}
public String getACCOUNT_TYPE_EN() {
	return ACCOUNT_TYPE_EN;
}
public void setACCOUNT_TYPE_EN(String account_type_en) {
	ACCOUNT_TYPE_EN = account_type_en;
}
public String getACCOUNT_TYPE_AR() {
	return ACCOUNT_TYPE_AR;
}
public void setACCOUNT_TYPE_AR(String account_type_ar) {
	ACCOUNT_TYPE_AR = account_type_ar;
}
public String getACCOUNT_PERCENTAGE() {
	return ACCOUNT_PERCENTAGE;
}
public void setACCOUNT_PERCENTAGE(String account_percentage) {
	ACCOUNT_PERCENTAGE = account_percentage;
}
public String getASSET_CLASS_NAME_EN() {
	return ASSET_CLASS_NAME_EN;
}
public void setASSET_CLASS_NAME_EN(String asset_class_name_en) {
	ASSET_CLASS_NAME_EN = asset_class_name_en;
}
public String getASSET_CLASS_NAME_AR() {
	return ASSET_CLASS_NAME_AR;
}
public void setASSET_CLASS_NAME_AR(String asset_class_name_ar) {
	ASSET_CLASS_NAME_AR = asset_class_name_ar;
}
public String getPORTFOLIO_NAME_EN() {
	return PORTFOLIO_NAME_EN;
}
public void setPORTFOLIO_NAME_EN(String portfolio_name_en) {
	PORTFOLIO_NAME_EN = portfolio_name_en;
}
public String getPORTFOLIO_NAME_AR() {
	return PORTFOLIO_NAME_AR;
}
public void setPORTFOLIO_NAME_AR(String portfolio_name_ar) {
	PORTFOLIO_NAME_AR = portfolio_name_ar;
}
public String getTOTAL_INVESTMENT_COST() {
	return TOTAL_INVESTMENT_COST;
}
public void setTOTAL_INVESTMENT_COST(String total_investment_cost) {
	TOTAL_INVESTMENT_COST = total_investment_cost;
}
public String getPORTFOLIO_FEES() {
	return PORTFOLIO_FEES;
}
public void setPORTFOLIO_FEES(String portfolio_fees) {
	PORTFOLIO_FEES = portfolio_fees;
}
public String getADDITIONAL_EXPENSES() {
	return ADDITIONAL_EXPENSES;
}
public void setADDITIONAL_EXPENSES(String additional_expenses) {
	ADDITIONAL_EXPENSES = additional_expenses;
}
public String getUNIT_PRICE() {
	return UNIT_PRICE;
}
public void setUNIT_PRICE(String unit_price) {
	UNIT_PRICE = unit_price;
}
public String getTOTAL_UNITS() {
	return TOTAL_UNITS;
}
public void setTOTAL_UNITS(String total_units) {
	TOTAL_UNITS = total_units;
}
public String getEVALUATION_TYPE_ID() {
	return EVALUATION_TYPE_ID;
}
public void setEVALUATION_TYPE_ID(String evaluation_type_id) {
	EVALUATION_TYPE_ID = evaluation_type_id;
}
public String getEVALUATION_VALUE() {
	return EVALUATION_VALUE;
}
public void setEVALUATION_VALUE(String evaluation_value) {
	EVALUATION_VALUE = evaluation_value;
}
public String getEVALUATION_UNIT_VALUE() {
	return EVALUATION_UNIT_VALUE;
}
public void setEVALUATION_UNIT_VALUE(String evaluation_unit_value) {
	EVALUATION_UNIT_VALUE = evaluation_unit_value;
}
public String getEVALUATION_NUMBER_OF_UNIT() {
	return EVALUATION_NUMBER_OF_UNIT;
}
public void setEVALUATION_NUMBER_OF_UNIT(String evaluation_number_of_unit) {
	EVALUATION_NUMBER_OF_UNIT = evaluation_number_of_unit;
}
public String getEVALUATION_QUARTER_ID() {
	return EVALUATION_QUARTER_ID;
}
public void setEVALUATION_QUARTER_ID(String evaluation_quarter_id) {
	EVALUATION_QUARTER_ID = evaluation_quarter_id;
}
public String getEVALUATION_YEAR() {
	return EVALUATION_YEAR;
}
public void setEVALUATION_YEAR(String evaluation_year) {
	EVALUATION_YEAR = evaluation_year;
}
public String getPROFIT_VALUE() {
	return PROFIT_VALUE;
}
public void setPROFIT_VALUE(String profit_value) {
	PROFIT_VALUE = profit_value;
}
public String getPROFIT_TO_DATE() {
	return PROFIT_TO_DATE;
}
public void setPROFIT_TO_DATE(String profit_to_date) {
	PROFIT_TO_DATE = profit_to_date;
}
public String getPROFIT_FROM_DATE() {
	return PROFIT_FROM_DATE;
}
public void setPROFIT_FROM_DATE(String profit_from_date) {
	PROFIT_FROM_DATE = profit_from_date;
}
public String getASSET_CLASS_ID() {
	return ASSET_CLASS_ID;
}
public void setASSET_CLASS_ID(String asset_class_id) {
	ASSET_CLASS_ID = asset_class_id;
}
public String getPARENT_ASSET_CLASS_ID() {
	return PARENT_ASSET_CLASS_ID;
}
public void setPARENT_ASSET_CLASS_ID(String parent_asset_class_id) {
	PARENT_ASSET_CLASS_ID = parent_asset_class_id;
}
public String getASSET_CLASS_LEVEL() {
	return ASSET_CLASS_LEVEL;
}
public void setASSET_CLASS_LEVEL(String asset_class_level) {
	ASSET_CLASS_LEVEL = asset_class_level;
}
public String getASSET_CLASS_NUMBER() {
	return ASSET_CLASS_NUMBER;
}
public void setASSET_CLASS_NUMBER(String asset_class_number) {
	ASSET_CLASS_NUMBER = asset_class_number;
}
public String getASSET_CLASS_DESC() {
	return ASSET_CLASS_DESC;
}
public void setASSET_CLASS_DESC(String asset_class_desc) {
	ASSET_CLASS_DESC = asset_class_desc;
}
public String getSTATUS_ID() {
	return STATUS_ID;
}
public void setSTATUS_ID(String status_id) {
	STATUS_ID = status_id;
}
public String getASSET_ID() {
	return ASSET_ID;
}
public void setASSET_ID(String asset_id) {
	ASSET_ID = asset_id;
}
public String getCURRENCY_ID() {
	return CURRENCY_ID;
}
public void setCURRENCY_ID(String currency_id) {
	CURRENCY_ID = currency_id;
}
public String getASSET_SYMBOL() {
	return ASSET_SYMBOL;
}
public void setASSET_SYMBOL(String asset_symbol) {
	ASSET_SYMBOL = asset_symbol;
}
public String getASSET_NAME_EN() {
	return ASSET_NAME_EN;
}
public void setASSET_NAME_EN(String asset_name_en) {
	ASSET_NAME_EN = asset_name_en;
}
public String getASSET_NAME_AR() {
	return ASSET_NAME_AR;
}
public void setASSET_NAME_AR(String asset_name_ar) {
	ASSET_NAME_AR = asset_name_ar;
}
public String getASSET_DESC() {
	return ASSET_DESC;
}
public void setASSET_DESC(String asset_desc) {
	ASSET_DESC = asset_desc;
}
public String getASSET_NUMBER() {
	return ASSET_NUMBER;
}
public void setASSET_NUMBER(String asset_number) {
	ASSET_NUMBER = asset_number;
}
public String getASSET_STATUS() {
	return ASSET_STATUS;
}
public void setASSET_STATUS(String asset_status) {
	ASSET_STATUS = asset_status;
}
public String getASSET_SECTOR_ID() {
	return ASSET_SECTOR_ID;
}
public void setASSET_SECTOR_ID(String asset_sector_id) {
	ASSET_SECTOR_ID = asset_sector_id;
}
public String getASSET_SUB_SECTOR_ID() {
	return ASSET_SUB_SECTOR_ID;
}
public void setASSET_SUB_SECTOR_ID(String asset_sub_sector_id) {
	ASSET_SUB_SECTOR_ID = asset_sub_sector_id;
}
public String getEXPIRY_DATE() {
	return EXPIRY_DATE;
}
public void setEXPIRY_DATE(String expiry_date) {
	EXPIRY_DATE = expiry_date;
}
public String getASSET_SUB_CLASS_ID() {
	return ASSET_SUB_CLASS_ID;
}
public void setASSET_SUB_CLASS_ID(String asset_sub_class_id) {
	ASSET_SUB_CLASS_ID = asset_sub_class_id;
}
public String getPORTFOLIO_ASSET_ID() {
	return PORTFOLIO_ASSET_ID;
}
public void setPORTFOLIO_ASSET_ID(String portfolio_asset_id) {
	PORTFOLIO_ASSET_ID = portfolio_asset_id;
}
public String getPORTFOLIO_ID() {
	return PORTFOLIO_ID;
}
public void setPORTFOLIO_ID(String portfolio_id) {
	PORTFOLIO_ID = portfolio_id;
}
public String getPORTFOLIO_DESC_EN() {
	return PORTFOLIO_DESC_EN;
}
public void setPORTFOLIO_DESC_EN(String portfolio_desc_en) {
	PORTFOLIO_DESC_EN = portfolio_desc_en;
}
public String getPORTFOLIO_DESC_AR() {
	return PORTFOLIO_DESC_AR;
}
public void setPORTFOLIO_DESC_AR(String portfolio_desc_ar) {
	PORTFOLIO_DESC_AR = portfolio_desc_ar;
}
public String getPORTFOLIO_NUMBER() {
	return PORTFOLIO_NUMBER;
}
public void setPORTFOLIO_NUMBER(String portfolio_number) {
	PORTFOLIO_NUMBER = portfolio_number;
}
public String getPORTFOLIO_TYPE_ID() {
	return PORTFOLIO_TYPE_ID;
}
public void setPORTFOLIO_TYPE_ID(String portfolio_type_id) {
	PORTFOLIO_TYPE_ID = portfolio_type_id;
}
public String getMARKET_SECTOR() {
	return MARKET_SECTOR;
}
public void setMARKET_SECTOR(String market_sector) {
	MARKET_SECTOR = market_sector;
}
public String getACCOUNT_TYPE_ID() {
	return ACCOUNT_TYPE_ID;
}
public void setACCOUNT_TYPE_ID(String account_type_id) {
	ACCOUNT_TYPE_ID = account_type_id;
}
public String getPRINCIPAL_AMOUNT() {
	return PRINCIPAL_AMOUNT;
}
public void setPRINCIPAL_AMOUNT(String principal_amount) {
	PRINCIPAL_AMOUNT = principal_amount;
}
public String getFUNDS_DURATION_START_DATE() {
	return FUNDS_DURATION_START_DATE;
}
public void setFUNDS_DURATION_START_DATE(String funds_duration_start_date) {
	FUNDS_DURATION_START_DATE = funds_duration_start_date;
}
public String getFUNDS_DURATION_END_DATE() {
	return FUNDS_DURATION_END_DATE;
}
public void setFUNDS_DURATION_END_DATE(String funds_duration_end_date) {
	FUNDS_DURATION_END_DATE = funds_duration_end_date;
}
public String getACCOUNT_DESC() {
	return ACCOUNT_DESC;
}
public void setACCOUNT_DESC(String account_desc) {
	ACCOUNT_DESC = account_desc;
}
public String getField1() {
	return field1;
}
public void setField1(String field1) {
	this.field1 = field1;
}
public String getField2() {
	return field2;
}
public void setField2(String field2) {
	this.field2 = field2;
}
public String getField3() {
	return field3;
}
public void setField3(String field3) {
	this.field3 = field3;
}
public String getField4() {
	return field4;
}
public void setField4(String field4) {
	this.field4 = field4;
}
public String getField6() {
	return field6;
}
public void setField6(String field6) {
	this.field6 = field6;
}
public String getField7() {
	return field7;
}
public void setField7(String field7) {
	this.field7 = field7;
}
public String getField8() {
	return field8;
}
public void setField8(String field8) {
	this.field8 = field8;
}
public String getField9() {
	return field9;
}
public void setField9(String field9) {
	this.field9 = field9;
}
public String getField10() {
	return field10;
}
public void setField10(String field10) {
	this.field10 = field10;
}
public String getField11() {
	return field11;
}
public void setField11(String field11) {
	this.field11 = field11;
}
public String getField12() {
	return field12;
}
public void setField12(String field12) {
	this.field12 = field12;
}
public String getField14() {
	return field14;
}
public void setField14(String field14) {
	this.field14 = field14;
}
public String getField13() {
	return field13;
}
public void setField13(String field13) {
	this.field13 = field13;
}
public String getField15() {
	return field15;
}
public void setField15(String field15) {
	this.field15 = field15;
}
public String getField16() {
	return field16;
}
public void setField16(String field16) {
	this.field16 = field16;
}
public String getField17() {
	return field17;
}
public void setField17(String field17) {
	this.field17 = field17;
}
public String getField18() {
	return field18;
}
public void setField18(String field18) {
	this.field18 = field18;
}
public String getField19() {
	return field19;
}
public void setField19(String field19) {
	this.field19 = field19;
}
public String getField20() {
	return field20;
}
public void setField20(String field20) {
	this.field20 = field20;
}
public int getParameterCurrQuarter() {
	return parameterCurrQuarter;
}
public void setParameterCurrQuarter(int parameterCurrQuarter) {
	this.parameterCurrQuarter = parameterCurrQuarter;
}
public int getParameterPrvQuarter() {
	return parameterPrvQuarter;
}
public void setParameterPrvQuarter(int parameterPrvQuarter) {
	this.parameterPrvQuarter = parameterPrvQuarter;
}
public String getTOTAL_VALUES_OF_INVESTMENT() {
	return TOTAL_VALUES_OF_INVESTMENT;
}
public void setTOTAL_VALUES_OF_INVESTMENT(String total_values_of_investment) {
	TOTAL_VALUES_OF_INVESTMENT = total_values_of_investment;
}

}