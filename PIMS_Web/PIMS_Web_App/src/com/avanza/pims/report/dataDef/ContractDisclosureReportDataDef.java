package com.avanza.pims.report.dataDef;

public class ContractDisclosureReportDataDef extends AbstractReportDataDef {
	
	private String contractId;
	private String contractDate;
	private String contractNumber;
	private String propertyName;
	private String propertyCommercialName;
	private String contractRemarks;
	private String domainDataOwnerNameEn;
	private String domainDataOwnerNameAr;
	private String personFirstName;
	private String personLastName;
	private String personMiddleName;
	private String personCompanyName;
	private String contractStartDate;
	private String contractEndDate;
	private String contractOriginalStartDate;
	private String contractOriginalEndDate;
	private String contractRentAmount;
	private String contractAskingPrice;
	private String contactInfoAddress1;
	private String contactInfoAddress2;
	private String contractType;
	private String unitNumber;
	private String contractPeriod;
	private String tenantDate;
	private String passportNumber;
	private String passportExpiryDate;
	private String visaNumber;
	private String visaExpiryDate;
	private String b_oNumber;
	private String community;
	private String nationality;
	private String officePhone;
	private String street;
	private String designation;
		
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyCommercialName() {
		return propertyCommercialName;
	}
	public void setPropertyCommercialName(String propertyCommercialName) {
		this.propertyCommercialName = propertyCommercialName;
	}
	public String getContractRemarks() {
		return contractRemarks;
	}
	public void setContractRemarks(String contractRemarks) {
		this.contractRemarks = contractRemarks;
	}
	public String getDomainDataOwnerNameEn() {
		return domainDataOwnerNameEn;
	}
	public void setDomainDataOwnerNameEn(String domainDataOwnerNameEn) {
		this.domainDataOwnerNameEn = domainDataOwnerNameEn;
	}
	public String getDomainDataOwnerNameAr() {
		return domainDataOwnerNameAr;
	}
	public void setDomainDataOwnerNameAr(String domainDataOwnerNameAr) {
		this.domainDataOwnerNameAr = domainDataOwnerNameAr;
	}
	public String getPersonFirstName() {
		return personFirstName;
	}
	public void setPersonFirstName(String personFirstName) {
		this.personFirstName = personFirstName;
	}
	public String getPersonLastName() {
		return personLastName;
	}
	public void setPersonLastName(String personLastName) {
		this.personLastName = personLastName;
	}
	public String getPersonMiddleName() {
		return personMiddleName;
	}
	public void setPersonMiddleName(String personMiddleName) {
		this.personMiddleName = personMiddleName;
	}
	public String getPersonCompanyName() {
		return personCompanyName;
	}
	public void setPersonCompanyName(String personCompanyName) {
		this.personCompanyName = personCompanyName;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractOriginalStartDate() {
		return contractOriginalStartDate;
	}
	public void setContractOriginalStartDate(String contractOriginalStartDate) {
		this.contractOriginalStartDate = contractOriginalStartDate;
	}
	public String getContractOriginalEndDate() {
		return contractOriginalEndDate;
	}
	public void setContractOriginalEndDate(String contractOriginalEndDate) {
		this.contractOriginalEndDate = contractOriginalEndDate;
	}
	public String getContractRentAmount() {
		return contractRentAmount;
	}
	public void setContractRentAmount(String contractRentAmount) {
		this.contractRentAmount = contractRentAmount;
	}
	public String getContractAskingPrice() {
		return contractAskingPrice;
	}
	public void setContractAskingPrice(String contractAskingPrice) {
		this.contractAskingPrice = contractAskingPrice;
	}
	public String getContactInfoAddress1() {
		return contactInfoAddress1;
	}
	public void setContactInfoAddress1(String contactInfoAddress1) {
		this.contactInfoAddress1 = contactInfoAddress1;
	}
	public String getContactInfoAddress2() {
		return contactInfoAddress2;
	}
	public void setContactInfoAddress2(String contactInfoAddress2) {
		this.contactInfoAddress2 = contactInfoAddress2;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getContractPeriod() {
		return contractPeriod;
	}
	public void setContractPeriod(String contractPeriod) {
		this.contractPeriod = contractPeriod;
	}
	public String getTenantDate() {
		return tenantDate;
	}
	public void setTenantDate(String tenantDate) {
		this.tenantDate = tenantDate;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}
	public String getVisaExpiryDate() {
		return visaExpiryDate;
	}
	public void setVisaExpiryDate(String visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}
	public String getB_oNumber() {
		return b_oNumber;
	}
	public void setB_oNumber(String number) {
		b_oNumber = number;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}	
}
