package com.avanza.pims.report.dataDef;

public class UnitInquiryReportDataDef extends AbstractReportDataDef 
{
	private String propertyName;
	private String unitNumber;
	private String unitTypeEn;
	private String unitTypeAr;
	private String usageEn;
	private String usageAr;
	private String noOfBedRooms;
	private String noOfBathRooms;
	private String rentValue;
	private String floorNumber;
	private String unitStatusEn;
	private String unitStatusAr;
	private String unitSideEn;
	private String unitSideAr;
	private String unitDescriptionEn;
	private String unitDescriptionAr;
	private String noOfLivingRooms;
	private String ewUtilityNo;
	private String remarks;
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getNoOfBedRooms() {
		return noOfBedRooms;
	}
	public void setNoOfBedRooms(String noOfBedRooms) {
		this.noOfBedRooms = noOfBedRooms;
	}
	public String getNoOfBathRooms() {
		return noOfBathRooms;
	}
	public void setNoOfBathRooms(String noOfBathRooms) {
		this.noOfBathRooms = noOfBathRooms;
	}
	public String getRentValue() {
		return rentValue;
	}
	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}
	public String getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}

	public String getNoOfLivingRooms() {
		return noOfLivingRooms;
	}
	public void setNoOfLivingRooms(String noOfLivingRooms) {
		this.noOfLivingRooms = noOfLivingRooms;
	}
	public String getEwUtilityNo() {
		return ewUtilityNo;
	}
	public void setEwUtilityNo(String ewUtilityNo) {
		this.ewUtilityNo = ewUtilityNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getUnitTypeEn() {
		return unitTypeEn;
	}
	public void setUnitTypeEn(String unitTypeEn) {
		this.unitTypeEn = unitTypeEn;
	}
	public String getUnitTypeAr() {
		return unitTypeAr;
	}
	public void setUnitTypeAr(String unitTypeAr) {
		this.unitTypeAr = unitTypeAr;
	}
	public String getUnitSideEn() {
		return unitSideEn;
	}
	public void setUnitSideEn(String unitSideEn) {
		this.unitSideEn = unitSideEn;
	}
	public String getUnitSideAr() {
		return unitSideAr;
	}
	public void setUnitSideAr(String unitSideAr) {
		this.unitSideAr = unitSideAr;
	}
	public String getUsageEn() {
		return usageEn;
	}
	public void setUsageEn(String usageEn) {
		this.usageEn = usageEn;
	}
	public String getUsageAr() {
		return usageAr;
	}
	public void setUsageAr(String usageAr) {
		this.usageAr = usageAr;
	}
	public String getUnitStatusEn() {
		return unitStatusEn;
	}
	public void setUnitStatusEn(String unitStatusEn) {
		this.unitStatusEn = unitStatusEn;
	}
	public String getUnitStatusAr() {
		return unitStatusAr;
	}
	public void setUnitStatusAr(String unitStatusAr) {
		this.unitStatusAr = unitStatusAr;
	}
	public String getUnitDescriptionEn() {
		return unitDescriptionEn;
	}
	public void setUnitDescriptionEn(String unitDescriptionEn) {
		this.unitDescriptionEn = unitDescriptionEn;
	}
	public String getUnitDescriptionAr() {
		return unitDescriptionAr;
	}
	public void setUnitDescriptionAr(String unitDescriptionAr) {
		this.unitDescriptionAr = unitDescriptionAr;
	}
}
