package com.avanza.pims.report.dataDef;

import java.util.Date;

public class ServiceContractsReportLabelDataDef extends AbstractReportDataDef{

	/*******************************                           English Labels             *********************************/

	private String lblContractNumberEn;
	private String lblStatusEn;
	private String lblContractorTypeEn;
	private String lblContractorNameEn;
	private String lblFromContractDateEn;
	private String lblToContractDateEn;
	private String lblPropertyNameEn;
	private String lblPropertyOwnershipTypeEn;
	
	/*******************************                           Arabic Labels             *********************************/

	private String lblContractNumberAr;
	private String lblStatusAr;
	private String lblContractorTypeAr;
	private String lblContractorNameAr;
	private String lblFromContractDateAr;
	private String lblToContractDateAr;
	private String lblPropertyNameAr;
	private String lblPropertyOwnershipTypeAr;

	/*******************************                           Mandatory Labels             *********************************/
	private String lblLoggedInUser;
	private String lblPIMSHeaderEn;
	private String lblReportNameEn;
	private String lblPIMSHeaderAr;
	private String lblReportNameAr;
	public String getLblContractNumberEn() {
		return lblContractNumberEn;
	}
	public void setLblContractNumberEn(String lblContractNumberEn) {
		this.lblContractNumberEn = lblContractNumberEn;
	}
	public String getLblStatusEn() {
		return lblStatusEn;
	}
	public void setLblStatusEn(String lblStatusEn) {
		this.lblStatusEn = lblStatusEn;
	}
	public String getLblContractorTypeEn() {
		return lblContractorTypeEn;
	}
	public void setLblContractorTypeEn(String lblContractorTypeEn) {
		this.lblContractorTypeEn = lblContractorTypeEn;
	}
	public String getLblContractorNameEn() {
		return lblContractorNameEn;
	}
	public void setLblContractorNameEn(String lblContractorNameEn) {
		this.lblContractorNameEn = lblContractorNameEn;
	}
	public String getLblFromContractDateEn() {
		return lblFromContractDateEn;
	}
	public void setLblFromContractDateEn(String lblFromContractDateEn) {
		this.lblFromContractDateEn = lblFromContractDateEn;
	}
	public String getLblToContractDateEn() {
		return lblToContractDateEn;
	}
	public void setLblToContractDateEn(String lblToContractDateEn) {
		this.lblToContractDateEn = lblToContractDateEn;
	}
	public String getLblPropertyNameEn() {
		return lblPropertyNameEn;
	}
	public void setLblPropertyNameEn(String lblPropertyNameEn) {
		this.lblPropertyNameEn = lblPropertyNameEn;
	}
	public String getLblPropertyOwnershipTypeEn() {
		return lblPropertyOwnershipTypeEn;
	}
	public void setLblPropertyOwnershipTypeEn(String lblPropertyOwnershipTypeEn) {
		this.lblPropertyOwnershipTypeEn = lblPropertyOwnershipTypeEn;
	}
	public String getLblContractNumberAr() {
		return lblContractNumberAr;
	}
	public void setLblContractNumberAr(String lblContractNumberAr) {
		this.lblContractNumberAr = lblContractNumberAr;
	}
	public String getLblStatusAr() {
		return lblStatusAr;
	}
	public void setLblStatusAr(String lblStatusAr) {
		this.lblStatusAr = lblStatusAr;
	}
	public String getLblContractorTypeAr() {
		return lblContractorTypeAr;
	}
	public void setLblContractorTypeAr(String lblContractorTypeAr) {
		this.lblContractorTypeAr = lblContractorTypeAr;
	}
	public String getLblContractorNameAr() {
		return lblContractorNameAr;
	}
	public void setLblContractorNameAr(String lblContractorNameAr) {
		this.lblContractorNameAr = lblContractorNameAr;
	}
	public String getLblFromContractDateAr() {
		return lblFromContractDateAr;
	}
	public void setLblFromContractDateAr(String lblFromContractDateAr) {
		this.lblFromContractDateAr = lblFromContractDateAr;
	}
	public String getLblToContractDateAr() {
		return lblToContractDateAr;
	}
	public void setLblToContractDateAr(String lblToContractDateAr) {
		this.lblToContractDateAr = lblToContractDateAr;
	}
	public String getLblPropertyNameAr() {
		return lblPropertyNameAr;
	}
	public void setLblPropertyNameAr(String lblPropertyNameAr) {
		this.lblPropertyNameAr = lblPropertyNameAr;
	}
	public String getLblPropertyOwnershipTypeAr() {
		return lblPropertyOwnershipTypeAr;
	}
	public void setLblPropertyOwnershipTypeAr(String lblPropertyOwnershipTypeAr) {
		this.lblPropertyOwnershipTypeAr = lblPropertyOwnershipTypeAr;
	}
	public String getLblLoggedInUser() {
		return lblLoggedInUser;
	}
	public void setLblLoggedInUser(String lblLoggedInUser) {
		this.lblLoggedInUser = lblLoggedInUser;
	}
	public String getLblPIMSHeaderEn() {
		return lblPIMSHeaderEn;
	}
	public void setLblPIMSHeaderEn(String lblPIMSHeaderEn) {
		this.lblPIMSHeaderEn = lblPIMSHeaderEn;
	}
	public String getLblReportNameEn() {
		return lblReportNameEn;
	}
	public void setLblReportNameEn(String lblReportNameEn) {
		this.lblReportNameEn = lblReportNameEn;
	}
	public String getLblPIMSHeaderAr() {
		return lblPIMSHeaderAr;
	}
	public void setLblPIMSHeaderAr(String lblPIMSHeaderAr) {
		this.lblPIMSHeaderAr = lblPIMSHeaderAr;
	}
	public String getLblReportNameAr() {
		return lblReportNameAr;
	}
	public void setLblReportNameAr(String lblReportNameAr) {
		this.lblReportNameAr = lblReportNameAr;
	}
	
	
	
}
