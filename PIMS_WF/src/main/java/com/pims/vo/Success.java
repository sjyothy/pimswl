package com.pims.vo;

public class Success {
	
	private String successCode;
	private String successMsg;
	
	public String getSuccessCode(){
		return successCode;
	}
	public void setSuccessCode(String successCode){
		this.successCode = successCode;
	}
	
	public String getSuccessMsg(){
		return successMsg;
	}
	public void setSuccessMsg(String successMsg){
		this.successMsg = successMsg;
	}

}
