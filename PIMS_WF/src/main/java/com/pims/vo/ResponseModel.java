package com.pims.vo;

import java.util.List;

public class ResponseModel {
	
	private List<Errors> errors;
	private List<Success> success;
	
	public List<Errors> getErrors(){
		return errors;
	}
	public void setErrors(List<Errors> errors){
		this.errors = errors;
	}
	
	
	public List<Success> getSuccess(){
		return success;
	}
	public void setSuccess(List<Success> success){
		this.success = success;
	}
	
	
	
	

}
