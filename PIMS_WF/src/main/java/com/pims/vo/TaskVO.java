package com.pims.vo;

public class TaskVO {

	
	private String requestId;
	private String auctionId;
	private String procedureTypeId;
	private String userId;
	private String assignedGroups;
	private String assignUser;
	private String taskId;
	private String taskAction;
	private String procedureTaskId;
	
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the auctionId
	 */
	public String getAuctionId() {
		return auctionId;
	}
	/**
	 * @param auctionId the auctionId to set
	 */
	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the assignGroup
	 */
	public String getAssignedGroups() {
		return assignedGroups;
	}
	/**
	 * @param assignGroup the assignGroup to set
	 */
	public void setAssignedGroups(String assignGroup) {
		this.assignedGroups = assignGroup;
	}
	/**
	 * @return the assignUser
	 */
	public String getAssignUser() {
		return assignUser;
	}
	/**
	 * @param assignUser the assignUser to set
	 */
	public void setAssignUser(String assignUser) {
		this.assignUser = assignUser;
	}
	/**
	 * @return the procedureTypeId
	 */
	public String getProcedureTypeId() {
		return procedureTypeId;
	}
	/**
	 * @param procedureTypeId the procedureTypeId to set
	 */
	public void setProcedureTypeId(String procedureTypeId) {
		this.procedureTypeId = procedureTypeId;
	}
	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	 * @return the taskAction
	 */
	public String getTaskAction() {
		return taskAction;
	}
	/**
	 * @param taskAction the taskAction to set
	 */
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	
	public String getProcedureTaskId(){
		return procedureTaskId;
	}
	
	public void setProcedureTaskId(String procedureTaskId){
		this.procedureTaskId = procedureTaskId;
	}
	
	
	
}
