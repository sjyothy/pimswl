package com.pims.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

public class StringHelper {
	
	 public static String clobToString(Clob data) {
		 
		 if(data == null)
			 return null;
	        StringBuilder sb = new StringBuilder();
	        try {
	            Reader reader = data.getCharacterStream();
	            BufferedReader br = new BufferedReader(reader);

	            String line;
	            while(null != (line = br.readLine())) {
	                sb.append(line);
	            }
	            br.close();
	        } catch (SQLException e) {
	            // handle this exception
	        } catch (IOException e) {
	            // handle this exception
	        }
	        return sb.toString();
	    }

}
