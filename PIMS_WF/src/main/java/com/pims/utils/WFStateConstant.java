package com.pims.utils;

public enum  WFStateConstant {

		CLAIM("CLAIM"),
		UNCLAIM("UNCLAIM"),
		COMPLETE("COMPLETE"),
		REINITIATE("REINITIATE"),
		SENDBACK("SENDBACK")
		
		;

	    private final String text;

	    /**
	     * @param text
	     */
	    private WFStateConstant(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
}
