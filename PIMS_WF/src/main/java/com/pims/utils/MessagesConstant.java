package com.pims.utils;

public class MessagesConstant {
	
	public static final String MSG_FILE_MISSING= "No file selected for uploading";
	
	
	public static final String MSG_SUCCESS_FILE="File uploaded successfully";
	public static final String MSG_SUCCESS_FILE_CODE="100";
	public static final String MSG_FAIL_UPLOAD = "An error has occurred while upload file";
	public static final String MSG_FAIL_UPLOAD_CODE = "101";
	public static final String MSG_PROCESS_INITIATE = "Process has been initiated successfully";
	public static final String MSG_PROCESS_INITIATE_CODE = "102";
	public static final String MSG_TASK_CLAIM = "Task has been claimed successfully";
	public static final String MSG_TASK_CLAIM_CODE = "103";
	public static final String MSG_TASK_RELEASE = "Task has been released successfully";
	public static final String MSG_TASK_RELEASE_CODE = "104";
	public static final String MSG_UNAUTHORIZE_TASK = "You are not authorize for this task";
	public static final String MSG_UNAUTHORIZE_TASK_CODE = "105";
	public static final String MSG_INVALID_TASK = "Invalid Task Action";
	public static final String MSG_INVALID_TASK_CODE = "106";
	public static final String MSG_TASK_NOTCLAIM = "Task is not claim yet";
	public static final String MSG_TASK_NOTCLAIM_CODE = "107";
	public static final String MSG_TASK_ACTION = "Task has been %s successfully";
	public static final String MSG_TASK_ACTION_CODE = "108";
	public static final String MSG_ALREADY_CLAIM = "Task is already claimed by another user";
	public static final String MSG_ALREADY_CLAIM_CODE = "109";

}
