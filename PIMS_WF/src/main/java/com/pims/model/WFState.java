package com.pims.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WF_STATE")
public class WFState implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8037610029418556063L;

	@Id
	private String state;
	
	@Column(name="NAME_EN")
	private String nameEn;
	
	@Column(name="NAME_AR")
	private String nameAr;

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the nameEn
	 */
	public String getNameEn() {
		return nameEn;
	}

	/**
	 * @param nameEn the nameEn to set
	 */
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	/**
	 * @return the nameAr
	 */
	public String getNameAr() {
		return nameAr;
	}

	/**
	 * @param nameAr the nameAr to set
	 */
	public void setNameAr(String nameAr) {
		this.nameAr = nameAr;
	}

	
}
