package com.pims.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="WF_PROCESS_DATA")
public class WFProcessData extends DbObject implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6071867987796832564L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WF_PROCESS_DATA")
    @SequenceGenerator(sequenceName = "SEQ_WF_PROCESS_DATA", allocationSize = 1, name = "SEQ_WF_PROCESS_DATA")
	private Long id;
	
	@Column(name="REQUEST_ID")
	private Long requestId;
	
	@Column(name="AUCTION_ID")
	private Long auctionId;
	
	@OneToOne(mappedBy="wfProcessData", cascade = {CascadeType.ALL},fetch = FetchType.LAZY)
	@JsonIgnore
	private WFProcessInstance processInstance;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the requestId
	 */
	public Long getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the auctionId
	 */
	public Long getAuctionId() {
		return auctionId;
	}

	/**
	 * @param auctionId the auctionId to set
	 */
	public void setAuctionId(Long auctionId) {
		this.auctionId = auctionId;
	}

	/**
	 * @return the processInstance
	 */
	public WFProcessInstance getProcessInstance() {
		return processInstance;
	}

	/**
	 * @param processInstance the processInstance to set
	 */
	public void setProcessInstance(WFProcessInstance processInstance) {
		this.processInstance = processInstance;
	}
	
	
	

}
