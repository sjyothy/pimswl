package com.pims.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WF_TASK_INSTANCE")
public class WFProcessTaskInstance extends DbObject implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7132039894584721526L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WF_PROCESS_TASK_INSTANCE")
    @SequenceGenerator(sequenceName = "SEQ_WF_PROCESS_TASK_INSTANCE", allocationSize = 1, name = "SEQ_WF_PROCESS_TASK_INSTANCE")
	private Long id;
	
//	@Column(name="WF_PROCESS_INSTANCE_ID")
//	private Long processInstanceId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "WF_PROCESS_INSTANCE_ID", nullable = false)
	private WFProcessInstance wfProcessInstance;
	
	@Column(name="WF_STATE")
	private String state;
	
	@Column(name="TASK_SUBJECT_EN")
	private String taskSubjectEn;
	
	@Column(name="TASK_SUBJECT_AR")
	private String taskSubjectAr;
	
	@Column(name="ASSIGN_USER")
	private String assignUser;
	
	@Column(name="INITIATED_TASK_INSTANCE_ID")
	private Long initiatedTaskId;
	
	@Column(name = "PROCEDURE_TASK_ID")
	private Long procedureTaskId;
	
	@Column(name = "CLAIM_BY")
	private String claimBy; 

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

//	/**
//	 * @return the processInstanceId
//	 */
//	public Long getProcessInstanceId() {
//		return processInstanceId;
//	}
//
//	/**
//	 * @param processInstanceId the processInstanceId to set
//	 */
//	public void setProcessInstanceId(Long processInstanceId) {
//		this.processInstanceId = processInstanceId;
//	}


	/**
	 * @return the taskSubjectEn
	 */
	public String getTaskSubjectEn() {
		return taskSubjectEn;
	}

	/**
	 * @param taskSubjectEn the taskSubjectEn to set
	 */
	public void setTaskSubjectEn(String taskSubjectEn) {
		this.taskSubjectEn = taskSubjectEn;
	}

	/**
	 * @return the taskSubjectAr
	 */
	public String getTaskSubjectAr() {
		return taskSubjectAr;
	}

	/**
	 * @param taskSubjectAr the taskSubjectAr to set
	 */
	public void setTaskSubjectAr(String taskSubjectAr) {
		this.taskSubjectAr = taskSubjectAr;
	}

	/**
	 * @return the assignUser
	 */
	public String getAssignUser() {
		return assignUser;
	}

	/**
	 * @param assignUser the assignUser to set
	 */
	public void setAssignUser(String assignUser) {
		this.assignUser = assignUser;
	}

	/**
	 * @return the initiatedTaskId
	 */
	public Long getInitiatedTaskId() {
		return initiatedTaskId;
	}

	/**
	 * @param initiatedTaskId the initiatedTaskId to set
	 */
	public void setInitiatedTaskId(Long initiatedTaskId) {
		this.initiatedTaskId = initiatedTaskId;
	}

	/**
	 * @return the procedureTaskId
	 */
	public Long getProcedureTaskId() {
		return procedureTaskId;
	}

	/**
	 * @param procedureTaskId the procedureTaskId to set
	 */
	public void setProcedureTaskId(Long procedureTaskId) {
		this.procedureTaskId = procedureTaskId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the claimBy
	 */
	public String getClaimBy() {
		return claimBy;
	}

	/**
	 * @param claimBy the claimBy to set
	 */
	public void setClaimBy(String claimBy) {
		this.claimBy = claimBy;
	}

	/**
	 * @return the wfProcessInstance
	 */
	public WFProcessInstance getWfProcessInstance() {
		return wfProcessInstance;
	}

	/**
	 * @param wfProcessInstance the wfProcessInstance to set
	 */
	public void setWfProcessInstance(WFProcessInstance wfProcessInstance) {
		this.wfProcessInstance = wfProcessInstance;
	}

	
	
}
