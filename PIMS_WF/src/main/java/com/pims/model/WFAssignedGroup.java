package com.pims.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="WF_ASSIGNED_GROUP")
public class WFAssignedGroup {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WF_ASSIGNED_GROUP")
    @SequenceGenerator(sequenceName = "SEQ_WF_ASSIGNED_GROUP", allocationSize = 1, name = "SEQ_WF_ASSIGNED_GROUP")
	private Long id;
	
	@Column(name = "USER_GROUP_ID")
	private String userGroup;
	
	
	
	@OneToOne
	@JsonIgnore
	@JoinColumn(name = "TASK_INSTANCE_ID")
	private WFProcessTaskInstance wfProcessTaskInstances;
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	
	/**
	 * @return the wfProcessTaskInstances
	 */
	public WFProcessTaskInstance getWfProcessTaskInstances() {
		return wfProcessTaskInstances;
	}

	/**
	 * @param wfProcessTaskInstances the wfProcessTaskInstances to set
	 */
	public void setWfProcessTaskInstances(WFProcessTaskInstance wfProcessTaskInstances) {
		this.wfProcessTaskInstances = wfProcessTaskInstances;
	}
	

	/**
	 * @return the userGroup
	 */
	
	public String getUserGroup() {
		return userGroup;
	}

	/**
	 * @param userGroup the userGroup  to set
	 */
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	

}
