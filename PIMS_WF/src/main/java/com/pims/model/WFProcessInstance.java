package com.pims.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="WF_PROCESS_INSTANCE")
public class WFProcessInstance extends DbObject implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4227532671524801779L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WF_PROCESS_INSTANCE")
    @SequenceGenerator(sequenceName = "SEQ_WF_PROCESS_INSTANCE", allocationSize = 1, name = "SEQ_WF_PROCESS_INSTANCE")
    private Long id;
	
	@Column(name = "PROCESS_START_TIME")
	private Date processStartTime;
	
	@Column(name = "PROCESS_END_TIME")
	private Date processEndTime;
	
//	@Column(name = "PROCESS_DATA_ID")
//	private Long processDataId; 
	
	@OneToOne(cascade = {CascadeType.ALL},fetch = FetchType.LAZY)
	@JoinColumn(name = "PROCESS_DATA_ID")
	private WFProcessData wfProcessData;

	
	@Column(name = "PROCEDURE_TYPE_ID")
	private Long procedureTypeId;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "wfProcessInstance")
	@JsonIgnore
	private Set<WFProcessTaskInstance> wfProcessTaskInstances = new HashSet<WFProcessTaskInstance>(0);
	
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the processStartTime
	 */
	public Date getProcessStartTime() {
		return processStartTime;
	}

	/**
	 * @param processStartTime the processStartTime to set
	 */
	public void setProcessStartTime(Date processStartTime) {
		this.processStartTime = processStartTime;
	}

	/**
	 * @return the processEndTime
	 */
	public Date getProcessEndTime() {
		return processEndTime;
	}

	/**
	 * @param processEndTime the processEndTime to set
	 */
	public void setProcessEndTime(Date processEndTime) {
		this.processEndTime = processEndTime;
	}

	/**
	 * @return the procedureTypeId
	 */
	public Long getProcedureTypeId() {
		return procedureTypeId;
	}

	/**
	 * @param procedureTypeId the procedureTypeId to set
	 */
	public void setProcedureTypeId(Long procedureTypeId) {
		this.procedureTypeId = procedureTypeId;
	}

	/**
	 * @return the wfProcessData
	 */
	public WFProcessData getWfProcessData() {
		return wfProcessData;
	}

	/**
	 * @param wfProcessData the wfProcessData to set
	 */
	public void setWfProcessData(WFProcessData wfProcessData) {
		this.wfProcessData = wfProcessData;
	}

	/**
	 * @return the wfProcessTaskInstances
	 */
	public Set<WFProcessTaskInstance> getWfProcessTaskInstances() {
		return wfProcessTaskInstances;
	}

	/**
	 * @param wfProcessTaskInstances the wfProcessTaskInstances to set
	 */
	public void setWfProcessTaskInstances(Set<WFProcessTaskInstance> wfProcessTaskInstances) {
		this.wfProcessTaskInstances = wfProcessTaskInstances;
	}

//	/**
//	 * @return the wfProcessTaskInstances
//	 */
//	public Set<WFProcessTaskInstance> getWfProcessTaskInstances() {
//		return wfProcessTaskInstances;
//	}
//
//	/**
//	 * @param wfProcessTaskInstances the wfProcessTaskInstances to set
//	 */
//	public void setWfProcessTaskInstances(Set<WFProcessTaskInstance> wfProcessTaskInstances) {
//		this.wfProcessTaskInstances = wfProcessTaskInstances;
//	}


}
