package com.pims.model;

import java.util.Date;
import javax.persistence.*;


@Entity
public class ProcedureTask {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROCEDURE_TASK")
    @SequenceGenerator(sequenceName = "SEQ_PROCEDURE_TASK", allocationSize = 1, name = "SEQ_PROCEDURE_TASK")
    private Long procedureTaskId;
	
	@Column(name = "PROCEDURE_TYPE_ID")
    private Long procedureTypeId;
	
	@Column(name = "HUMAN_TASK_ID")
    private Long humanTaskId;
	
	@Column(name = "CREATED_BY")
    private String createdBy;
	
	@Column(name = "CREATED_ON")
    private Date createdOn;
	
	@Column(name = "UPDATED_BY")
    private String updatedBy;
	
	@Column(name = "UPDATED_ON")
    private Date updatedOn;
	
	@Column(name = "IS_DELETED")
    private Long isDeleted;
	
	@Column(name = "RECORD_STATUS")
    private Long recordStatus;
	
	@Column(name = "PREVIOUS_TASKS_ID")
	private Long previousTask;
	
	@Column(name = "TASK_ACTION")
	private String taskAction;
	
	
	/**
	 * @return the procedureTaskId
	 */
	public Long getProcedureTaskId() {
		return procedureTaskId;
	}
	/**
	 * @param procedureTaskId the procedureTaskId to set
	 */
	public void setProcedureTaskId(Long procedureTaskId) {
		this.procedureTaskId = procedureTaskId;
	}
	/**
	 * @return the procedureTypeId
	 */
	public Long getProcedureTypeId() {
		return procedureTypeId;
	}
	/**
	 * @param procedureTypeId the procedureTypeId to set
	 */
	public void setProcedureTypeId(Long procedureTypeId) {
		this.procedureTypeId = procedureTypeId;
	}
	/**
	 * @return the humanTaskId
	 */
	public Long getHumanTaskId() {
		return humanTaskId;
	}
	/**
	 * @param humanTaskId the humanTaskId to set
	 */
	public void setHumanTaskId(Long humanTaskId) {
		this.humanTaskId = humanTaskId;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the isDeleted
	 */
	public Long getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the recordStatus
	 */
	public Long getRecordStatus() {
		return recordStatus;
	}
	/**
	 * @param recordStatus the recordStatus to set
	 */
	public void setRecordStatus(Long recordStatus) {
		this.recordStatus = recordStatus;
	}
	/**
	 * @return the taskAction
	 */
	public String getTaskAction() {
		return taskAction;
	}
	/**
	 * @param taskAction the taskAction to set
	 */
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	/**
	 * @return the previousTask
	 */
	public Long getPreviousTask() {
		return previousTask;
	}
	/**
	 * @param previousTask the previousTask to set
	 */
	public void setPreviousTask(Long previousTask) {
		this.previousTask = previousTask;
	}
 
    
    
}
