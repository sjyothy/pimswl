package com.pims.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DbObject  implements Serializable{

    // persisted fields
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5892642288262716267L;

	private Date createdOn;
	
    private String createdBy;
	
    private Date updatedOn;
	
    private String updatedBy;
	
    private Boolean isDeleted;
	
    private Integer recordStatus;

    protected DbObject() {
    }

    public void setCreation(String userId) {

        this.createdOn = new Date();
        this.createdBy = userId;
        this.updatedOn = this.createdOn;
        this.updatedBy = userId;
    }

    public void setUpdation(String userId) {

        this.updatedOn = new Date();
        this.updatedBy = userId;
    }

    public String getCreatedBy() {

        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {

        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {

        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {

        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {

        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {

        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {

        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {

        this.updatedOn = updatedOn;
    }

    public boolean isNew() {

        return ((this.createdOn == null) && (this.createdBy == null));
    }
    

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void copyValues(DbObject copyFrom) {
    	
        this.createdBy = copyFrom.createdBy;
        this.createdOn = copyFrom.createdOn;
        this.updatedBy = copyFrom.updatedBy;
        this.updatedOn = copyFrom.updatedOn;
        this.isDeleted = copyFrom.isDeleted;
        this.recordStatus = copyFrom.recordStatus;
    }

    public boolean isModified() {
        return true;
    }

	/**
	 * @return the isDeleted
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
    
    
}