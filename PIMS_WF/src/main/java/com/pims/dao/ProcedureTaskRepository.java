package com.pims.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pims.model.ProcedureTask;

@Transactional
public interface ProcedureTaskRepository extends JpaRepository<ProcedureTask, Long> {
	
	List<ProcedureTask> findByProcedureTaskId(Long proceduretaskid);
	
	List<ProcedureTask> findByProcedureTypeIdAndPreviousTaskIsNull(Long proceduretaskid);
	
	List<ProcedureTask> findByPreviousTaskAndTaskAction(Long procedureTaskId, String taskAction);
	
	List<ProcedureTask> findByCreatedBy(String createdby);
	
	@Query(value = "select WF_GetGroupByProcedureTaskId(:taskId) from dual", nativeQuery = true)
	Object findGroupByProcedureTaskId(@Param("taskId") Long procedureTaskId);


//	@Query(value = "select count(*) from sec_user_user_group where  "
//				+ "login_id =:loginId and user_group_id in  "
//				+ "(select regexp_substr(assign_group ,'[^,]+', 1, level) from wf_task_instance "
//				+ "where id=:taskId connect by regexp_substr(assign_group , '[^,]+', 1, level) is not null )", nativeQuery = true )
//	int getUserGroupCountByTaskId(@Param("taskId") Long taskId, @Param("loginId") String loginId);
	
	@Query(value = "select count(*)FROM wf_task_instance task "
			+ "left join wf_assigned_group assignGrp on assignGrp.task_instance_id=task.id "
			+ "left join sec_user_user_group userGrp on userGrp.USER_GROUP_ID = assignGrp.USER_GROUP_ID "
			+ "where 1=1  and task.id =:procedureTaskId and task.wf_state in ('UNCLAIM','REINITIATE') "
			+ "and   ( (task.assign_user =:loginId) or ( userGrp.login_id =:loginId "
			+ "and userGrp.user_group_id is not null))", nativeQuery = true)
	int getUserGroupCountByTaskId(@Param("procedureTaskId") Long procedureTaskId,@Param("loginId") String loginId);
	
}
