package com.pims.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pims.model.WFProcessInstance;
import java.lang.Long;

@Transactional
@Repository
public interface WFProcessInstanceRepository extends JpaRepository<WFProcessInstance, Long> {
	
	List<WFProcessInstance> findById(Long id);

}
