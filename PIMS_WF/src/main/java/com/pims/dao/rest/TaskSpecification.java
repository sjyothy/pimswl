package com.pims.dao.rest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.pims.model.WFProcessTaskInstance;

public class TaskSpecification implements Specification<WFProcessTaskInstance> {
	 
    private SearchCriteria criteria;
 
    public TaskSpecification(SearchCriteria param) {
    	criteria = param;
	}

	@Override
    public Predicate toPredicate
      (Root<WFProcessTaskInstance> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		 
//		Class<?> someClass = WFProcessTaskInstance.class;
////		someClass.getField()
//		if(true){
//		Join<WFProcessTaskInstance,WFProcessInstance> userProd = root.join("wfProcessInstance");
////        Join<FollowingRelationship,Product> prodRelation = userProd.join("ownedRelationships");
//		builder.equal(root.get(criteria.getKey()), criteria.getValue());
//        return builder.equal(userProd.get("procedureTypeId"), "143");
//		}
//		
        if (criteria.getOperation().equalsIgnoreCase(">")) {
            return builder.greaterThanOrEqualTo(
              root.<String> get(criteria.getKey()), criteria.getValue().toString());
        } 
        else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return builder.lessThanOrEqualTo(
              root.<String> get(criteria.getKey()), criteria.getValue().toString());
        } 
        else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.like(
                  root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }
        return null;
    }
}