package com.pims.dao.rest;

public class SearchCriteria {
    public SearchCriteria(String key2, String operation2, Object value2) {
    	this.key = key2;
    	this.operation = operation2;
    	this.value = value2;
    	
	}
	private String key;
    private String operation;
    private Object value;
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
}