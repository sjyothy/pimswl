package com.pims.dao.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.pims.model.WFProcessTaskInstance;

public class TaskSpecificationsBuilder {

	private final List<SearchCriteria> params;

	public TaskSpecificationsBuilder() {
		params = new ArrayList<SearchCriteria>();
	}

	public TaskSpecificationsBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
//		params.add(new SearchCriteria("wfProcessInstance.procedureTypeId", ":", "143"));
//		params.add(new SearchCriteria("id", ":", "58"));
		return this;
	}

	public Specification<WFProcessTaskInstance> build() {
		if (params.size() == 0) {
			return null;
		}

		List<Specification<WFProcessTaskInstance>> specs = new ArrayList<Specification<WFProcessTaskInstance>>();

		for (SearchCriteria param : params) {
			specs.add(new TaskSpecification(param));
		}
		
		Specification<WFProcessTaskInstance> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}

		return result;
	}
}