package com.pims.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pims.model.ProcedureTask;
import com.pims.model.WFProcessTaskInstance;
import java.lang.Long;
import java.util.List;

public interface WFProcessTaskRepository extends JpaRepository<WFProcessTaskInstance, Long>, JpaSpecificationExecutor<WFProcessTaskInstance>{
	
	List<WFProcessTaskInstance> findByProcedureTaskId(Long proceduretaskid);
	
	List<WFProcessTaskInstance> findById(Long id);
	
//	@Query(value = "select wftask from WFProcessTaskInstance wftask " +
//			", ProcedureTask pt where pt.procedureTaskId = wftask.procedureTaskId and " + 
//			"pt.procedureTypeId =:typeId and pt.previousTask =:previousTaskId and pt.taskAction =:nextTaskAction "+
//			"and wftask.processInstanceId =:processInstanceId and wftask.state =:previousTaskAction ")
//	List<WFProcessTaskInstance> findTasks(@Param("typeId") Long procedureTypeId, @Param("previousTaskId") Long previousTaskId,
//												 @Param("nextTaskAction") String nextTaskAction, @Param("processInstanceId") Long processInstanceId,
//												 @Param("previousTaskAction") String previousTaskAction
//			);

}
