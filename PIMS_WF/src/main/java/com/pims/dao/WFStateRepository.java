package com.pims.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pims.model.WFState;
import java.lang.String;
import java.util.List;

@Transactional
@Repository
public interface WFStateRepository extends JpaRepository<WFState, String> {
	
	List<WFState> findByState(String state);
	
	List<WFState> findByNameEn(String nameen);
	
	List<WFState> findByNameAr(String namear);

}
