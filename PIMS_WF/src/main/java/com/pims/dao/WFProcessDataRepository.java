package com.pims.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pims.model.WFProcessData;

@Transactional
@Repository
public interface WFProcessDataRepository extends JpaRepository<WFProcessData, Long> {

}
