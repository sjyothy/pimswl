package com.pims;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pims.dao.ProcedureTaskRepository;
import com.pims.dao.WFProcessInstanceRepository;
import com.pims.dao.WFProcessTaskRepository;
import com.pims.dao.WFStateRepository;
import com.pims.dao.rest.TaskSpecificationsBuilder;
import com.pims.model.WFProcessTaskInstance;
import com.pims.service.ProcessService;
import com.pims.vo.ResponseModel;
import com.pims.vo.TaskVO;



@RestController
public class MainController {

    @Autowired
    ProcedureTaskRepository procedureTaskRepository;
    
    @Autowired
    WFProcessInstanceRepository instanceRepository; 
    
    @Autowired
    ProcessService procedureTaskService;
    
    @Autowired 
    WFStateRepository stateRepository;
    
    @Autowired
    WFProcessTaskRepository taskRepository; 
    
//    @Autowired
//	MessageSource messageSource;
    

    /*@RequestMapping("/getTask")
    public List<ProcedureTask> getTask(@RequestParam(value="name", defaultValue="World") String name) {
//    	return null;
    	return IteratorUtils.toList(procedureTaskRepository.findAll().iterator());
    }*/
    
    
  /*  @RequestMapping("/saveTask")
    public WFProcessInstance saveTask() {
//    	return null;
    	
    	WFProcessInstance processInstance = new WFProcessInstance();
//    	processInstance.setCurrentTaskId(2L);
    	processInstance.setId(34L);
    	processInstance.setProcessEndTime(new Date());
    	processInstance.setProcessEndTime(new Date());
    	
    	ProcedureTask procedureTask = new ProcedureTask();
    	procedureTask.setHumanTaskId(45010L);
    	procedureTask.setProcedureTypeId(45010L);
    	procedureTask.setUpdatedBy("admin");
    	procedureTask.setRecordStatus(1L);
    	procedureTask.setIsDeleted(0L);
    	procedureTask.setUpdatedOn(new Date());
    	procedureTask.setCreatedOn(new Date());
    	procedureTask.setCreatedBy("admin");
    	
    	try{
    		procedureTaskService.saveProcess(procedureTask, processInstance);
//    	instanceRepository.save(instance);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return processInstance;
    }*/
    
    @RequestMapping(value = "/initiateProcess",method=RequestMethod.POST)
    public ResponseModel initiateProcess(
    							@RequestBody TaskVO taskVO
    							){
    	
    	
    	return procedureTaskService.initiateProcess(taskVO);
    	
    }
    
    
    @RequestMapping(value = "/completeTask",method=RequestMethod.POST)
    public ResponseModel completeTask(
    							@RequestBody TaskVO taskVO
    							){
    	
    	
    	return procedureTaskService.completeTask(taskVO);
    	
    	
    }
    
    @RequestMapping(value = "/claimTask",method=RequestMethod.POST)
    public ResponseModel claimTask(
    							@RequestBody TaskVO taskVO
    							){
    	System.out.println("before msg resource");
//    	System.out.println("message resource:"+messageSource.getMessage("msg.unauthorize.task",null, Locale.getDefault()));
    	return procedureTaskService.claimTask(taskVO);
    	
    	
    }
    
    
    @RequestMapping(value = "/releaseTask",method=RequestMethod.POST)
    public ResponseModel releaseTask(
    							@RequestBody TaskVO taskVO
    							){
    	
    	
    	return procedureTaskService.releaseTask(taskVO);
    	
    	
    }
    
    
    @RequestMapping(value = "/getTask",method=RequestMethod.POST)
    public List<WFProcessTaskInstance> getTask(
    							@RequestBody TaskVO taskVO
    							){
    	procedureTaskRepository.findAll();
    	
    	return null;
    	
    	
    }
    
    
    @RequestMapping("/mock")
    public void mock(
    							
    							){
    	try{
//    		for (Iterator<WFProcessTaskInstance> iterator = taskRepository.findTasks(45050L, 142L, "APPROVED", 23L, "SENDBACK").iterator(); iterator.hasNext();) {
//				WFProcessTaskInstance wfProcessTaskInstance = iterator.next();
//				System.out.println(wfProcessTaskInstance.getState());
				
//			}
    		
    	}catch (Exception e) {
    		e.printStackTrace();
		}
    }
    
    
    @RequestMapping(method = RequestMethod.GET, value = "/task")
    @ResponseBody
    public List<WFProcessTaskInstance> search(@RequestParam(value = "search") String search) {
    	TaskSpecificationsBuilder builder = new TaskSpecificationsBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }
         
        Specification<WFProcessTaskInstance> spec = builder.build();
        return taskRepository.findAll(spec);
    }
    
    
}
