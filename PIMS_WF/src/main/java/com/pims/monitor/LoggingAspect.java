package com.pims.monitor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

	
	@AfterReturning("execution(* com..*Service.*(..))")
	public void afterlogServiceAccess(JoinPoint joinPoint) {
		LOGGER.info("Completed: " + joinPoint);
	}
	
	@Before("execution(* com..*Service.*(..))")
	public void beforelogServiceAccess(JoinPoint joinPoint) {
		LOGGER.info("Completed: " + joinPoint);
	}
	
	@AfterReturning("execution(* com..*Controller.*(..))")
	public void afterLogControllerAccess(JoinPoint joinPoint) {
		LOGGER.info("Starting: " + joinPoint);
	}
	
	@Before("execution(* com..*Controller.*(..))")
	public void beforeLogControllerAccess(JoinPoint joinPoint) {
		LOGGER.info("Starting: " + joinPoint);
	}

	
	@AfterThrowing(pointcut = "execution(* com..*Controller.*(..))", throwing= "error")
		    public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {

			LOGGER.error("logAfterThrowing() is running!");
			LOGGER.error("Method Name : " + joinPoint.getSignature().getName());
			LOGGER.error("Exception : " + error);

		    }
	
	
}
