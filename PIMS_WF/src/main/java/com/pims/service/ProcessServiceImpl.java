package com.pims.service;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.EnumUtils;
import org.hibernate.validator.internal.util.logging.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pims.dao.ProcedureTaskRepository;
import com.pims.dao.WFAssignGroupRepository;
import com.pims.dao.WFProcessDataRepository;
import com.pims.dao.WFProcessInstanceRepository;
import com.pims.dao.WFProcessTaskRepository;
import com.pims.dao.WFStateRepository;
import com.pims.model.ProcedureTask;
import com.pims.model.WFAssignedGroup;
import com.pims.model.WFProcessData;
import com.pims.model.WFProcessInstance;
import com.pims.model.WFProcessTaskInstance;
import com.pims.utils.MessagesConstant;
import com.pims.utils.StringHelper;
import com.pims.utils.WFStateConstant;
import com.pims.utils.WFTaskAction;
import com.pims.vo.Errors;
import com.pims.vo.ResponseModel;
import com.pims.vo.Success;
import com.pims.vo.TaskVO;

@Service("processService")
@Transactional
public class ProcessServiceImpl implements ProcessService{

	 @Autowired
	 private ProcedureTaskRepository procedureTaskRepository;
	 
	 @Autowired
	 private WFProcessInstanceRepository instanceRepository;
	 
	 @Autowired
	 private WFProcessDataRepository dataRepository;
	 
	 @Autowired
	 private WFStateRepository stateRepository;
	 
	 @Autowired
	 private WFProcessTaskRepository processTaskRepository;
	 
	 @Autowired
	 private WFAssignGroupRepository assignedGroupRepository;
	 
	 
	 
//	 @Autowired
//	 MessageSource messageSource;
	 
	@Override
	public ResponseModel saveTask(ProcedureTask procedureTask) {
		procedureTaskRepository.save(procedureTask);
		return null;
		
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=Exception.class)
	public ResponseModel saveProcess(final ProcedureTask procedureTask,
	final WFProcessInstance processInstance) throws Exception {
		
		try{
			
			WFProcessData data = new WFProcessData();
			
			
//			dataRepository.save(data);
			processInstance.setProcessStartTime(new Date());
			
//			processInstance.setProcessDataId(data.getId());
			
			processInstance.setWfProcessData(data);
			instanceRepository.save(processInstance);
			procedureTaskRepository.save(procedureTask);
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return null;
	
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=Exception.class)
	public ResponseModel saveProcess(String requestId, String userId, String nolType) {
		
		ResponseModel response = new ResponseModel();
		List<Success> succesMsgs = new ArrayList<Success>();
		
		WFProcessData processData = new WFProcessData();
		processData.setRequestId(Long.parseLong(requestId));
		processData.setCreatedBy(userId);
		processData.setCreatedOn(new Date());
		
//		dataRepository.save(processData);
		
		WFProcessInstance processInstance = new WFProcessInstance();
		
		processInstance.setProcessStartTime(new Date());
		
//		processInstance.setProcessDataId(processData.getId());
		
		processInstance.setWfProcessData(processData);
		processInstance.setCreatedBy(userId);
		processInstance.setCreatedOn(new Date());
		
		
		instanceRepository.save(processInstance);
		
		WFProcessTaskInstance taskInstance = new WFProcessTaskInstance();
		
//		taskInstance.setProcessInstanceId(processInstance.getId());
		taskInstance.setWfProcessInstance(processInstance);
		
		taskInstance.setCreatedBy(userId);
		taskInstance.setCreatedOn(new Date());
		
		processTaskRepository.save(taskInstance);
		
		Success success = new Success();
		success.setSuccessMsg(MessagesConstant.MSG_PROCESS_INITIATE);
		success.setSuccessCode(MessagesConstant.MSG_PROCESS_INITIATE_CODE);
		succesMsgs.add(success);
		response.setSuccess(succesMsgs);
		
		return response;
		
		
	}

	@Override
	public ResponseModel initiateProcess(TaskVO taskVO) {
		
		Long procedureTypeId = Long.parseLong(taskVO.getProcedureTypeId());
		String groups = null;
		Long procedureTaskId = null;
		ResponseModel response = new ResponseModel();
		List<Success> succesMsgs = new ArrayList<Success>();
		
		for (Iterator<ProcedureTask> iterator = procedureTaskRepository.findByProcedureTypeIdAndPreviousTaskIsNull(procedureTypeId).iterator(); iterator.hasNext();) {
			ProcedureTask procedureTask = iterator.next();
			procedureTaskId = procedureTask.getProcedureTaskId();
			
			
		}
		
		//adding process data
		WFProcessData processData = new WFProcessData();
		processData.setRequestId(Long.parseLong(taskVO.getRequestId()));
		processData.setCreatedBy(taskVO.getUserId());
		processData.setCreatedOn(new Date());
		
//		dataRepository.save(processData);
		
		//adding process instance
		WFProcessInstance processInstance = new WFProcessInstance();
		
		processInstance.setProcessStartTime(new Date());
		
//		processInstance.setProcessDataId(processData.getId());
		processInstance.setWfProcessData(processData);
		
		processInstance.setCreatedBy(taskVO.getUserId());
		processInstance.setCreatedOn(new Date());
		
		processInstance.setProcedureTypeId(procedureTypeId);
		
		instanceRepository.save(processInstance);
		
		//adding task
		
		
		
		
		WFProcessTaskInstance taskInstance = new WFProcessTaskInstance();
		
//		taskInstance.setProcessInstanceId(processInstance.getId());
		taskInstance.setWfProcessInstance(processInstance);

		
		taskInstance.setCreatedBy(taskVO.getUserId());
		taskInstance.setCreatedOn(new Date());
		
		taskInstance.setProcedureTaskId(procedureTaskId);
		taskInstance.setState(WFStateConstant.UNCLAIM.toString());
		
		
		if(taskVO.getAssignUser() != null && taskVO.getAssignUser().length() > 0 && taskVO.getAssignUser() != "-1")
			taskInstance.setAssignUser(taskVO.getAssignUser());
		else
			saveAssignGroup(taskVO.getAssignedGroups(),taskInstance,procedureTaskId);
		
		processTaskRepository.save(taskInstance);
		
		Success success = new Success();
		success.setSuccessMsg(MessagesConstant.MSG_PROCESS_INITIATE);
		success.setSuccessCode(MessagesConstant.MSG_PROCESS_INITIATE_CODE);
		succesMsgs.add(success);
		response.setSuccess(succesMsgs);
		
		return response;
		
		
	}
	
	
	private void saveAssignGroup(String assignedGroups, WFProcessTaskInstance taskInstance, Long procedureTaskId) {

		if(assignedGroups != null){
			for(String group :assignedGroups.split(",")){
				WFAssignedGroup assignedGroup = new WFAssignedGroup();
				assignedGroup.setWfProcessTaskInstances(taskInstance);
				assignedGroup.setUserGroup(group);
				assignedGroupRepository.save(assignedGroup);
			}
		}
		else{
			Object result = procedureTaskRepository.findGroupByProcedureTaskId(procedureTaskId);
			if(result != null){
				String resultStr = StringHelper.clobToString(((Clob)result));
				if(null != resultStr && resultStr.length() > 0){
					for(String group :resultStr.split(",")){
						WFAssignedGroup assignedGroup = new WFAssignedGroup();
						assignedGroup.setWfProcessTaskInstances(taskInstance);
						assignedGroup.setUserGroup(group);
						assignedGroupRepository.save(assignedGroup);
					}
				}
			}
		}
	}

	@Override
	public ResponseModel completeTask(TaskVO taskVO) {
		
		WFProcessTaskInstance wfProcessTaskInstance = null;
		WFProcessInstance processInstance = null;
		ResponseModel response = new ResponseModel();
		List<Success> succesMsgs = new ArrayList<Success>();
		List<Errors> errorMsgs = new ArrayList<Errors>();
		
		Long taskId = Long.parseLong(taskVO.getTaskId());
		for (Iterator<WFProcessTaskInstance> iterator = processTaskRepository.findById(taskId).iterator(); iterator.hasNext();) {
			wfProcessTaskInstance = iterator.next();
			processInstance = wfProcessTaskInstance.getWfProcessInstance();
			
			if(wfProcessTaskInstance.getState().equals(WFStateConstant.REINITIATE.toString())){
				
			}
			else if(wfProcessTaskInstance.getClaimBy() == null || wfProcessTaskInstance.getState().equals(WFStateConstant.UNCLAIM.toString())){
//					return messageSource.getMessage("msg.task.notClaim",null, Locale.getDefault());
				Errors error = new Errors();
				error.setErrorMsg(MessagesConstant.MSG_TASK_NOTCLAIM);
				error.setErrorCode(MessagesConstant.MSG_TASK_NOTCLAIM_CODE);
				errorMsgs.add(error);
				response.setErrors(errorMsgs);
				return response;
				}
				else if(!wfProcessTaskInstance.getClaimBy().equals(taskVO.getUserId())){
//					return messageSource.getMessage("msg.unauthorize.task",null, Locale.getDefault());
					Errors error = new Errors();
					error.setErrorMsg(MessagesConstant.MSG_UNAUTHORIZE_TASK);
					error.setErrorCode(MessagesConstant.MSG_UNAUTHORIZE_TASK_CODE);
					errorMsgs.add(error);
					response.setErrors(errorMsgs);
					return response;
				}
				else if(!EnumUtils.isValidEnum(WFTaskAction.class, taskVO.getTaskAction())){
//					return messageSource.getMessage("msg.invalid.taskAction",null, Locale.getDefault());
					Errors error = new Errors();
					error.setErrorMsg(MessagesConstant.MSG_INVALID_TASK);
					error.setErrorCode(MessagesConstant.MSG_INVALID_TASK_CODE);
					errorMsgs.add(error);
					response.setErrors(errorMsgs);
					return response;
				}
			
			if(!wfProcessTaskInstance.getState().equals(WFStateConstant.REINITIATE.toString())){
							
							wfProcessTaskInstance.setUpdatedBy(taskVO.getUserId());
							wfProcessTaskInstance.setUpdatedOn(new Date());
							wfProcessTaskInstance.setState(WFStateConstant.COMPLETE.toString());
			}
			
		}
		
//		check if it's re-initiated  
		if(wfProcessTaskInstance.getState().equals(WFStateConstant.REINITIATE.toString())){

			WFProcessTaskInstance taskInstance = new WFProcessTaskInstance();
			
			completeTask(taskVO,taskInstance,wfProcessTaskInstance,processInstance);
			
			for (Iterator<WFProcessTaskInstance> iterator = processTaskRepository.findById(wfProcessTaskInstance.getInitiatedTaskId()).iterator(); iterator.hasNext();) {
				WFProcessTaskInstance wfProcessTaskInstance2 = iterator.next();
				taskInstance.setProcedureTaskId(wfProcessTaskInstance2.getProcedureTaskId());
				
			}
			
			if(taskInstance.getProcedureTaskId() != null)
				processTaskRepository.save(taskInstance);
			
			wfProcessTaskInstance.setUpdatedBy(taskVO.getUserId());
			wfProcessTaskInstance.setUpdatedOn(new Date());
			wfProcessTaskInstance.setState(WFStateConstant.COMPLETE.toString());
			
			
		}
		else
			
		for (Iterator<ProcedureTask> iterator = procedureTaskRepository.findByPreviousTaskAndTaskAction((wfProcessTaskInstance.getProcedureTaskId()), taskVO.getTaskAction()).iterator(); iterator.hasNext();) 
		{
			
			ProcedureTask procedureTask = iterator.next();
			WFProcessTaskInstance taskInstance = new WFProcessTaskInstance();
			
			completeTask(taskVO,taskInstance,wfProcessTaskInstance,processInstance);
			
			taskInstance.setProcedureTaskId(procedureTask.getProcedureTaskId());
			
//			taskInstance.setProcessInstanceId(wfProcessTaskInstance.getProcessInstanceId());
//			taskInstance.setInitiatedTaskId(wfProcessTaskInstance.getId());
//			taskInstance.setCreatedBy(taskVO.getUserId());
//			taskInstance.setCreatedOn(new Date());
			
//			if(taskVO.getAssignGroup() != null)
//				taskInstance.setAssignGroup(taskVO.getAssignGroup());
//			else{
//				taskInstance.setAssignGroup(StringHelper.clobToString(((Clob)procedureTaskRepository.findGroupByProcedureTaskId(Long.parseLong(taskVO.getTaskId())))));
//			}
				
//			taskInstance.setAssignUser(taskVO.getAssignUser());
//			taskInstance.setState(WFStateConstant.UNCLAIM.toString());
			
			
			
			if(taskInstance.getProcedureTaskId() != null)
				processTaskRepository.save(taskInstance);
//			taskInstanceList.add(taskInstance);
		}	
		
		//in-case of review or post back
		if(taskVO.getTaskAction().equals(WFTaskAction.POSTBACK.toString()) 
//				|| taskVO.getTaskAction().equals(WFTaskAction.REVIEW.toString())
				|| taskVO.getTaskAction().equals(WFTaskAction.POSTBACK_RESUBMIT.toString())
				){
			for (Iterator<ProcedureTask> iterator2 = procedureTaskRepository.findByProcedureTaskId(wfProcessTaskInstance.getProcedureTaskId()).iterator(); iterator2.hasNext();) {
				
				ProcedureTask procedureTask = iterator2.next();
				
				WFProcessTaskInstance newTaskInstance = new WFProcessTaskInstance();
				
				completeTask(taskVO,newTaskInstance,wfProcessTaskInstance,processInstance);
				
				newTaskInstance.setProcedureTaskId(procedureTask.getProcedureTaskId());
				
//				taskInstance.setProcessInstanceId(wfProcessTaskInstance.getProcessInstanceId());
//				taskInstance.setInitiatedTaskId(wfProcessTaskInstance.getId());
//				taskInstance.setCreatedBy(taskVO.getUserId());
//				taskInstance.setCreatedOn(new Date());
				
//				if(taskVO.getAssignGroup() != null)
//					taskInstance.setAssignGroup(taskVO.getAssignGroup());
//				else{
//					taskInstance.setAssignGroup(StringHelper.clobToString(((Clob)procedureTaskRepository.findGroupByProcedureTaskId(Long.parseLong(taskVO.getTaskId())))));
//				}
//					
//				taskInstance.setAssignUser(taskVO.getAssignUser());
//				taskInstance.setState(WFStateConstant.UNCLAIM.toString());
				
				
				if(
//						taskVO.getTaskAction().equals(WFTaskAction.REVIEW.toString()) || 
						taskVO.getTaskAction().equals(WFTaskAction.POSTBACK_RESUBMIT.toString()) ){
	//				nextTask = procedureTask2.getPreviousTask();
					newTaskInstance.setProcedureTaskId(procedureTask.getPreviousTask());
				}
				else if(taskVO.getTaskAction().equals(WFTaskAction.POSTBACK.toString())){
					newTaskInstance.setProcedureTaskId(procedureTask.getPreviousTask());
					newTaskInstance.setState(WFStateConstant.REINITIATE.toString());
					wfProcessTaskInstance.setState(WFStateConstant.SENDBACK.toString());
				}
				
				if(newTaskInstance.getProcedureTaskId() != null)
					processTaskRepository.save(newTaskInstance);
				
			}
		}
		
		
		
		
		
		
		
		
		
		
//		in case of post back
//		if(taskVO.getTaskAction().equals(WFTaskAction.POSTBACK.toString())){
//			
//			taskInstance.setProcedureTaskId(wfProcessTaskInstance.getPreviousTask());
//			taskInstance.setState(WFStateConstant.REINITIATE.toString());
//			wfProcessTaskInstance.setState(WFStateConstant.SENDBACK.toString());
//		}
//		else if(taskVO.getTaskAction().equals(WFTaskAction.REVIEW.toString())){
//			
//		}
		
//		if(taskInstance.getProcedureTaskId() == null){
//			
//		}
		
		
		processTaskRepository.save(wfProcessTaskInstance);
		
		
		Success success = new Success();
		success.setSuccessMsg(String.format(MessagesConstant.MSG_TASK_ACTION,taskVO.getTaskAction()));
		success.setSuccessCode(MessagesConstant.MSG_TASK_ACTION_CODE);
		succesMsgs.add(success);
		response.setSuccess(succesMsgs);
		
		return response;
	}

	@Override
	public ResponseModel claimTask(TaskVO taskVO) {
		
		ResponseModel response = new ResponseModel();
		List<Success> succesMsgs = new ArrayList<Success>();
		List<Errors> errorMsgs = new ArrayList<Errors>();
		
		Long taskId = Long.parseLong(taskVO.getTaskId());
		for (Iterator<WFProcessTaskInstance> iterator = processTaskRepository.findById(taskId).iterator(); iterator.hasNext();) {
			WFProcessTaskInstance wfProcessTaskInstance = iterator.next();
			
			if(wfProcessTaskInstance.getClaimBy() != null){
//				return messageSource.getMessage("msg.alreadyClaim",null, Locale.getDefault());
				Errors error = new Errors();
				error.setErrorMsg(MessagesConstant.MSG_ALREADY_CLAIM);
				error.setErrorCode(MessagesConstant.MSG_ALREADY_CLAIM_CODE);
				errorMsgs.add(error);
				response.setErrors(errorMsgs);
				return response;
			}
			
			else if(procedureTaskRepository.getUserGroupCountByTaskId(taskId, taskVO.getUserId()) > 0){
				wfProcessTaskInstance.setClaimBy(taskVO.getUserId());
				wfProcessTaskInstance.setState(WFStateConstant.CLAIM.toString());
				processTaskRepository.save(wfProcessTaskInstance);
			}
			else{
//				return messageSource.getMessage("msg.unauthorize.task",null, Locale.getDefault());
				Errors error = new Errors();
				error.setErrorMsg(MessagesConstant.MSG_UNAUTHORIZE_TASK);
				error.setErrorCode(MessagesConstant.MSG_UNAUTHORIZE_TASK_CODE);
				errorMsgs.add(error);
				response.setErrors(errorMsgs);
				return response;
			}
		}
		
//		return messageSource.getMessage("msg.task.claim",null, Locale.getDefault());
		Success success = new Success();
		success.setSuccessMsg(String.format(MessagesConstant.MSG_TASK_CLAIM,taskVO.getTaskAction()));
		success.setSuccessCode(MessagesConstant.MSG_TASK_CLAIM_CODE);
		succesMsgs.add(success);
		response.setSuccess(succesMsgs);
		
		return response;
	}

	@Override
	public ResponseModel releaseTask(TaskVO taskVO) {
		
		ResponseModel response = new ResponseModel();
		List<Success> succesMsgs = new ArrayList<Success>();
		List<Errors> errorMsgs = new ArrayList<Errors>();
		
		Long taskId = Long.parseLong(taskVO.getTaskId());
		for (Iterator<WFProcessTaskInstance> iterator = processTaskRepository.findById(taskId).iterator(); iterator.hasNext();) {
			WFProcessTaskInstance wfProcessTaskInstance = iterator.next();
			
//			if(procedureTaskRepository.getUserGroupCountByTaskId(taskId, taskVO.getUserId()) <= 0){
			if(wfProcessTaskInstance.getAssignUser() == null && !wfProcessTaskInstance.getAssignUser().equals(taskVO.getUserId())){
				
//				return messageSource.getMessage("msg.unauthorize.task",null, Locale.getDefault());
				Errors error = new Errors();
				error.setErrorMsg(MessagesConstant.MSG_UNAUTHORIZE_TASK);
				error.setErrorCode(MessagesConstant.MSG_UNAUTHORIZE_TASK_CODE);
				errorMsgs.add(error);
				response.setErrors(errorMsgs);
				return response;
				
			}
			else if(wfProcessTaskInstance.getState().equals(WFStateConstant.UNCLAIM.toString())){
//				return messageSource.getMessage("msg.task.notClaim",null, Locale.getDefault());
				Errors error = new Errors();
				error.setErrorMsg(MessagesConstant.MSG_TASK_NOTCLAIM);
				error.setErrorCode(MessagesConstant.MSG_TASK_NOTCLAIM_CODE);
				errorMsgs.add(error);
				response.setErrors(errorMsgs);
				return response;
			}
			else{
				wfProcessTaskInstance.setClaimBy(null);
				wfProcessTaskInstance.setState(WFStateConstant.UNCLAIM.toString());
				processTaskRepository.save(wfProcessTaskInstance);
			}
		}
		
//		return messageSource.getMessage("msg.task.release",null, Locale.getDefault());
		Success success = new Success();
		success.setSuccessMsg(String.format(MessagesConstant.MSG_TASK_RELEASE,taskVO.getTaskAction()));
		success.setSuccessCode(MessagesConstant.MSG_TASK_RELEASE);
		succesMsgs.add(success);
		response.setSuccess(succesMsgs);
		
		return response;
	
	}
	
	void completeTask(TaskVO taskVO, WFProcessTaskInstance taskInstance, WFProcessTaskInstance wfProcessTaskInstance, WFProcessInstance processInstance){
		
		taskInstance.setWfProcessInstance(processInstance);
//		taskInstance.setProcessInstanceId(wfProcessTaskInstance.getProcessInstanceId());
		taskInstance.setCreatedBy(taskVO.getUserId());
		taskInstance.setCreatedOn(new Date());
		taskInstance.setInitiatedTaskId(wfProcessTaskInstance.getId());
		
//		if(taskVO.getAssignedGroups() != null)
//			taskInstance.setAssignGroup(taskVO.getAssignedGroups());
//		else{
//			taskInstance.setAssignGroup(StringHelper.clobToString(((Clob)procedureTaskRepository.findGroupByProcedureTaskId(Long.parseLong(taskVO.getProcedureTaskId())))));
//		}
		
		if(taskVO.getAssignUser() != null && taskVO.getAssignUser().length() > 0 && taskVO.getAssignUser() != "-1")
			taskInstance.setAssignUser(taskVO.getAssignUser());
		else
			saveAssignGroup(taskVO.getAssignedGroups(), taskInstance, Long.parseLong(taskVO.getProcedureTaskId().toString()));
		
		taskInstance.setState(WFStateConstant.UNCLAIM.toString());
	}
	
	void setTaskAssignGroup(){
		
	}

}
