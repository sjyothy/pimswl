package com.pims.service;

import com.pims.model.ProcedureTask;
import com.pims.model.WFProcessInstance;
import com.pims.vo.ResponseModel;
import com.pims.vo.TaskVO;

public interface ProcessService {
	
	ResponseModel saveTask(ProcedureTask procedureTask);
	
	ResponseModel saveProcess(ProcedureTask procedureTask,
			WFProcessInstance processInstance) throws Exception;

	ResponseModel saveProcess(String requestId, String userId, String nolType);

	ResponseModel initiateProcess(TaskVO taskVO);

	ResponseModel completeTask(TaskVO taskVO);

	ResponseModel claimTask(TaskVO taskVO);
	
	ResponseModel releaseTask(TaskVO taskVO);

}
