package com.avanza.pims.soa.bpm.worklist;

public class ExceptionCodes
{
  public static final int TASK_ALREADY_ACQUIRED = 101;
  public static final int TASK_IS_NOT_ACQUIRED = 102;
  public static final int FAILED_TO_ACQUIRE_TASK = 103;
  public static final int FAILED_TO_RELEASE_TASK = 104;
  public static final int FAILED_TO_COMPLETE_TASK = 105;
  public static final int FAILED_TO_GET_USER_TASKS = 106;
  public static final int FAILED_TO_ADD_COMMENT = 107;
  public static final int TASK_ALREADY_COMPLETED = 108;
  public static final int USER_NOT_AUTHENTIC = 109;
  public static final int USER_NOT_AUTHORIZED_FOR_TASK = 110;
  public static final int FAILED_TO_DELEGATE_TASK = 111;
}


/* Location:           C:\Projects\Java\PIMS\_PIMS_SOA_CLIENT\
 * Qualified Name:     com.avanza.pims.soa.bpm.worklist.ExceptionCodes
 * JD-Core Version:    0.7.0.1
 */