  package com.avanza.pims.soa.bpm.worklist;
  
  public class PIMSWorkListException
    extends Exception
  {
    private int exceptionCode;
    private Exception baseException;
    private String userIdAlreadyPerformedAction;
    
    public PIMSWorkListException(int exceptionCode)
    {
      this.exceptionCode = exceptionCode;
    }
    
    public PIMSWorkListException(int exceptionCode, Exception baseException)
    {
      this.exceptionCode = exceptionCode;
      this.baseException = baseException;
    }
    
    public PIMSWorkListException(int exceptionCode, String userIdAlreadyPerformedAction)
    {
      this.exceptionCode = exceptionCode;
      this.userIdAlreadyPerformedAction = userIdAlreadyPerformedAction;
    }
    
    public void setExceptionCode(int exceptionCode)
    {
      this.exceptionCode = exceptionCode;
    }
    
    public int getExceptionCode()
    {
      return this.exceptionCode;
    }
    
    public void setBaseException(Exception baseException)
    {
      this.baseException = baseException;
    }
    
    public Exception getBaseException()
    {
      return this.baseException;
    }
    
    public void setUserIdAlreadyPerformedAction(String userIdAlreadyPerformedAction)
    {
      this.userIdAlreadyPerformedAction = userIdAlreadyPerformedAction;
    }
    
    public String getUserIdAlreadyPerformedAction()
    {
      return this.userIdAlreadyPerformedAction;
    }
  }


/* Location:           C:\Projects\Java\PIMS\_PIMS_SOA_CLIENT\
 * Qualified Name:     com.avanza.pims.soa.bpm.worklist.PIMSWorkListException
 * JD-Core Version:    0.7.0.1
 */