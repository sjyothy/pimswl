package com.avanza.pims.soa.bpm.worklist;

public enum TaskOutcome
{
	
	SEND_FOR_CLOSING_VIOLATIONS(12),	OK(2),	SEND_FOR_RECOMMENDATIONS ( 11),	REPLY ( 10 ),	CLOSE ( 9),	SEND_FOR_SITE_VISIT (  8),
	LEGAL_DEPT_REVIEW (  7),	COLLECT ( 6 ),	CANCEL (  5),	COMPLETE ( 4),	FORWARD ( 3),	REJECT ( 1),	APPROVE (  0);
	
	private int value;

    private TaskOutcome(int value) {
            this.value = value;
    }


}
