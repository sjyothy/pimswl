package com.avanza.pims.soa.bpm.worklist;
 
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.bpel.services.workflow.WorkflowException;
import oracle.bpel.services.workflow.client.IWorkflowServiceClient;
import oracle.bpel.services.workflow.client.WorkflowServiceClientFactory;
import oracle.bpel.services.workflow.query.ITaskQueryService;
import oracle.bpel.services.workflow.repos.Ordering;
import oracle.bpel.services.workflow.repos.Predicate;
import oracle.bpel.services.workflow.repos.TableConstants;
import oracle.bpel.services.workflow.task.ITaskService;
import oracle.bpel.services.workflow.task.impl.TaskAssignee;
import oracle.bpel.services.workflow.task.model.CommentType;
import oracle.bpel.services.workflow.task.model.Task;
import oracle.bpel.services.workflow.verification.IWorkflowContext;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.avanza.core.util.Logger;
import com.avanza.util.ResourceWrapper;
import com.avanza.util.SystemParameters;




public class BPMWorklistClient
  {
    private static Logger logger = Logger.getLogger(BPMWorklistClient.class);
    private String configFilePath;
    private static String WF_MANANAGER_UN;
    private static String WF_MANANAGER_PW;
    private static String LDAP_DOMAIN;
    private SystemParameters parameters;
    
    private void $init$()
    {
      this.configFilePath = "";
      this.parameters = null;
    }
    
    public BPMWorklistClient(String configFilePath,String resourceFileEn, String resourceFileAr)
      throws IOException
    {
      $init$();
      logger.logInfo("Stepped into the BPMWorklistClient constructor with config file path:%s" , configFilePath);
      this.configFilePath = configFilePath;
      Properties properties = new Properties();
      try
      {
        logger.logInfo("About to load the BPMWorklistClient properties");
        
        properties.load(new FileInputStream(this.configFilePath));
        
        WF_MANANAGER_UN = properties.getProperty("UserName");
        WF_MANANAGER_PW = properties.getProperty("Password");
        LDAP_DOMAIN = properties.getProperty("Domain");
        
        logger.logInfo("About to get SystemParameters instance for loading resources");
          ResourceWrapper.getInstance();
//        if ((resourceFileEn != null) && (resourceFileEn != null)) {
//          this.parameters = SystemParameters.getInstance(resourceFileEn, resourceFileAr);
//        }
        logger.logInfo("PMWorklistClient properties and SystemParameters loaded successfully");
      }
      catch (IOException exp)
      {
        logger.LogException("BPMWorklistClient constructor crashed due to:", exp);
        throw exp;
      }
    }
    public BPMWorklistClient(String configFilePath)
      throws IOException
    {
      $init$();
      
      logger.logInfo("Stepped into the BPMWorklistClient constructor with config file path:" + configFilePath);
      
      this.configFilePath = configFilePath;
      
      Properties properties = new Properties();
      try
      {
        logger.logInfo("About to load the BPMWorklistClient properties");
        
        properties.load(new FileInputStream(this.configFilePath));
        
            String resourceFileEn = null;
            String resourceFileAr = null;
            if (properties.containsKey("RESOURCE_EN")) {
              resourceFileEn = properties.getProperty("RESOURCE_EN");
            }
            if (properties.containsKey("RESOURCE_AR")) {
              resourceFileAr = properties.getProperty("RESOURCE_AR");
            }
        WF_MANANAGER_UN = properties.getProperty("UserName");
        WF_MANANAGER_PW = properties.getProperty("Password");
        LDAP_DOMAIN = properties.getProperty("Domain");
        
        logger.logInfo("About to get SystemParameters instance for loading resources");
//        if ((resourceFileEn != null) && (resourceFileEn != null)) {
//          this.parameters = SystemParameters.getInstance(resourceFileEn, resourceFileAr);
//        }
        logger.logInfo("PMWorklistClient properties and SystemParameters loaded successfully");
      }
      catch (IOException exp)
      {
        logger.LogException("BPMWorklistClient constructor crashed due to:", exp);
        throw exp;
      }
    }
    
    public static void main(String[] args)
    {
      try
      {
        BPMWorklistClient bPMWorklistClient = new BPMWorklistClient("config.properties");
//        bPMWorklistClient.getTasksForUser("malijtabi");
        TaskSearchCriteria searchCriteria  = new TaskSearchCriteria();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        searchCriteria.setAssignedDateFrom(df.parse("07/12/2017"));
        searchCriteria.setAssignedDateTo(df.parse("07/12/2017"));
        
        Set<UserTask>  userTasks = bPMWorklistClient.getFilteredUserTasks("pims_admin",searchCriteria);
          if(userTasks != null)
          logger.logInfo("TotalTasks found:%s",userTasks.size());
          else
          logger.logInfo("TotalTasks found:null");
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
      catch (Exception e) {e.printStackTrace();}
    }
    
    public String retrieveTaskData(Task t)
    {
      return t.getSystemAttributes().getTaskId();
    }
    
    private ITaskService getTaskService()
    {
      ITaskService taskService = getWorkflowServiceClient().getTaskService();
      
      return taskService;
    }
    
    public Set<UserTask> getTasksForUser(String User)
      throws PIMSWorkListException
    {
      return getFilteredUserTasks(User, null);
    }
    
    public int getBPMTasksCount(String User, TaskSearchCriteria searchCriteria)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped into the getBPMTasksCount with parameter User:" + User);
      int count = 0;
      try
      {
        IWorkflowContext workflowContext  =getWorkflowContext(User);  
        List tasks = getBPMTasks(User, searchCriteria,workflowContext);
        if ((tasks != null) && (tasks.size() > 0)) {
          count = tasks.size();
        }
      }
      
      catch (Exception exp)
      {
        logger.LogException("Exception exception occured due to:", exp);
        exp.printStackTrace();
        throw new PIMSWorkListException(106, exp);
      }
      logger.logInfo("Stepped out of the method getBPMTasksCount");
      
      return count;
    }
    
    public List getBPMTasks(String User, TaskSearchCriteria searchCriteria,IWorkflowContext workflowContext)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped into the getBPMTasks with parameter User:" + User);
      List tasks = null;
      String worklistUser = User;
      System.out.println("Finding tasks for user " + worklistUser);
      try
      {
        Predicate whereclause = new Predicate(TableConstants.WFTASK_STATE_COLUMN, 0, "ASSIGNED");
        Predicate datePredicateAssignedFrom = null;
        Predicate datePredicateAssignedTo = null;
        Predicate assigneeGroupsPredicate = null;
        Predicate acquiredBy =null;
        if (searchCriteria != null)
        {
        	
          List<String> assigneeGroups = searchCriteria.getAssigneeGroups();
          Date assignedDateFrom = searchCriteria.getAssignedDateFrom();
          Date assignedDateTo = searchCriteria.getAssignedDateTo();
          if( searchCriteria.getAcquiredBy() != null && searchCriteria.getAcquiredBy().length()> 0 )
          {
        	  acquiredBy = new Predicate(TableConstants.WFTASK_ACQUIREDBY_COLUMN, Predicate.OP_EQ, searchCriteria.getAcquiredBy());
        	  
        	  worklistUser= searchCriteria.getAcquiredBy();
              whereclause  = new Predicate(whereclause,7 ,acquiredBy);
          }
          if ((assigneeGroups != null) && (assigneeGroups.size() > 0)) {
            assigneeGroupsPredicate = new Predicate(TableConstants.WFTASK_ASSIGNEEGROUPS_COLUMN, 9, assigneeGroups);
          }
          if (assignedDateFrom != null) {
            datePredicateAssignedFrom = new Predicate(TableConstants.WFTASK_ASSIGNEDDATE_COLUMN, 2, assignedDateFrom);
          }
          if (assignedDateTo != null)
          {
            Calendar date = Calendar.getInstance();
            date.setTime(assignedDateTo);
            date.add(6, 1);
            
            datePredicateAssignedTo = new Predicate(TableConstants.WFTASK_ASSIGNEDDATE_COLUMN, 4, date.getTime());
          }
        }
        if (datePredicateAssignedFrom != null) {
          whereclause = new Predicate(whereclause, 7, datePredicateAssignedFrom);
        }
        if (datePredicateAssignedTo != null) {
          whereclause = new Predicate(whereclause, 7, datePredicateAssignedTo);
        }
        if (assigneeGroupsPredicate != null) {
          whereclause = new Predicate(whereclause, 7, assigneeGroupsPredicate);
        }
        Ordering ordering = new Ordering(TableConstants.WFTASK_ASSIGNEDDATE_COLUMN, true, true);
        
  
        List displayColumns = new ArrayList();
        displayColumns.add("CREATEDDATE");
        displayColumns.add("TITLE");
        displayColumns.add("IDENTIFICATIONKEY");
        List optionalInfo = new ArrayList();
        
  
        logger.logInfo("About to fetch the task list");
        
        tasks = getTaskQueryService().queryTasks(
//        											getWorkflowContext(worklistUser),
                                                                                                workflowContext,
        											displayColumns, 
        											optionalInfo, 
        											"My+Group", 
        											null, 
        											whereclause, 
        											ordering, 
        											0, 
        											0
        										);
        
  
  
  
  
        logger.logInfo("Task List fetched sucessfully");
      }

      catch (WorkflowException exp)
      {
        logger.LogException("Workflow exception occured due to:", exp);
        if (exp.getErrorCode() == 30513) {
          throw new PIMSWorkListException(110, exp);
        }
        if (exp.getErrorCode() == 30501) {
          throw new PIMSWorkListException(109, exp);
        }
        throw new PIMSWorkListException(106, exp);
      }
      catch (Exception exp)
      {
        logger.LogException("Exception exception occured due to:", exp);
        exp.printStackTrace();
        throw new PIMSWorkListException(106, exp);
      }
      logger.logInfo("Stepped out of the method getTasksForUser");
      
      return tasks;
    }
    
    public Set<UserTask> getFilteredUserTasks(TaskSearchCriteria searchCriteria)
      throws PIMSWorkListException
    {
      return getFilteredUserTasks(WF_MANANAGER_UN, searchCriteria);
    }
    
    public Set<UserTask> getFilteredUserTasks(String User, TaskSearchCriteria searchCriteria)
      throws PIMSWorkListException
    {
      
      Set<UserTask> userTaskCollection = new HashSet(0);
      String worklistUser = User;
      logger.logInfo("Finding tasks for usersssssssss :%s" , worklistUser);
      try
      {
          IWorkflowContext workflowContext  =getWorkflowContext(User);  
        List tasks = getBPMTasks(User, searchCriteria,workflowContext);
        
        if ((tasks != null) && (tasks.size() > 0))
        {
          logger.logInfo("Number of tasks found:" + tasks.size());
          for (int i = 0; i < tasks.size(); i++)
          {
            logger.logInfo("Task Count:%s" ,i);
            UserTask userTask = null;
            Task bpmTask = (Task)tasks.get(i);
            String taskId = bpmTask.getSystemAttributes().getTaskId();
//            int taskNumber = bpmTask.getSystemAttributes().getTaskNumber();
            try
            {
              userTask = getTaskForUser(taskId, User,workflowContext);
               bpmTask=null; 
            }
            catch (PIMSWorkListException e) {
                    logger.LogException("getFilteredUserTasks:Error:" ,e);
                }
            if (userTask != null) {
              userTaskCollection.add(userTask);
            }
          }
        }
      }
      catch (PIMSWorkListException exp)
      {
        throw exp;
      }
      catch (Exception exp)
      {
        logger.LogException("Exception exception occured due to:", exp);
        exp.printStackTrace();
        throw new PIMSWorkListException(106, exp);
      }
      logger.logInfo("Stepped out of the method getTasksForUser");
      
      return userTaskCollection;
    }
    
    public UserTask getTaskForWFAdmin(String taskId)
      throws PIMSWorkListException
    {
      IWorkflowContext workflowContext  =getWorkflowContext(WF_MANANAGER_UN);
      return getTaskForUser(taskId, WF_MANANAGER_UN,workflowContext);
    }
    public UserTask getTaskForUser(String taskId, String User)
      throws PIMSWorkListException
    {
        logger.logInfo("Stepped into the getTaskForUser with parameter User:%s  and taskId:%s " , User , taskId);
        IWorkflowContext workflowContext  =getWorkflowContext(User);  
       return getTaskForUser(taskId,  User,workflowContext  );
    }
    public UserTask getTaskForUser(String taskId, String User,IWorkflowContext workflowContext  )
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped into the getTaskForUser with parameter User:%s  and taskId:%s " , User , taskId);
      
      UserTask userTask = null;
//      String worklistUser = User;
      try
      {
          
//        logger.logInfo("About to fetch the bpm task details");
        Task task = getTaskQueryService().getTaskDetailsById(workflowContext, taskId.trim());
//        logger.logInfo("BPM task details found successfully");
        int taskNumber = task.getSystemAttributes().getTaskNumber();
        Calendar assignedDate = task.getSystemAttributes().getAssignedDate();
        Calendar expiryDate = task.getSystemAttributes().getExpirationDate();
        Calendar updatedDate = task.getSystemAttributes().getUpdatedDate();
//        List<String> taskComments = new ArrayList();
        Element payload = task.getPayloadAsElement();
        Map<String, String> taskAttributes = new HashMap(0);
        String bpelName = null;
        String requestTitle = null;
        String taskTitle = null;
        String taskType = null;
        String taskSubject = null;
        String accquiredBy = null;
        String sentByUser = "";
        String sentToUser = "";
        userTask = new UserTask();
//        try
//        {
//          List userComments = task.getUserComment();
//          if ((userComments != null) && (userComments.size() > 0)) {
//            for (int index = 0; index < userComments.size(); index++)
//            {
//              CommentType comment = (CommentType)userComments.get(index);
//              taskComments.add(comment.getComment());
//            }
//          }
//        }
//        catch (Exception ex) {}
        logger.logInfo("About to manupulate task payload");
        try
        {
          if (payload.hasChildNodes())
          {
//            logger.logInfo("Task payload has attributes with them");
            
            NodeList attributeNodes = payload.getChildNodes();
            
//            logger.logInfo("Number of attributes with task payload are:%s, Now Starting iterating the payload" , attributeNodes.getLength());
            for (int index = 0; index < attributeNodes.getLength(); index++)
            {
              Node node = attributeNodes.item(index);
              try
              {
                String nodeName = node.getNodeName();
                String nodeValue = node.getFirstChild().getNodeValue();
                taskAttributes.put(nodeName, nodeValue);
                if ((nodeName != null) && ((nodeName.toLowerCase().contains("group")) || (nodeName.toLowerCase().contains("assign")))) {
                  userTask.setTaskAssigneeGroups(nodeValue);
                }
              }
              catch (Exception e)
              {
                logger.LogException("Failed to add an attributes into the taskAttributes collection due to:", e);
              }
            }
          }
        }
        catch (Exception exp)
        {
          logger.LogException("Failed to manupulate task payload due to:", exp);
        }
        if (taskAttributes.containsKey("BPEL_NAME")) {
          bpelName = (String)taskAttributes.get("BPEL_NAME");
        }
        if (taskAttributes.containsKey("TASK_TITLE")) {
//            logger.logInfo("TaskTitle:%s",(String)taskAttributes.get("TASK_TITLE"));  
          taskTitle = (String)taskAttributes.get("TASK_TITLE");
          
        }
        if (taskAttributes.containsKey("SUBJECT")) {
          taskSubject = (String)taskAttributes.get("SUBJECT");
        }
        if (taskAttributes.containsKey("TASK_TYPE")) {
          taskType = (String)taskAttributes.get("TASK_TYPE");
        }
        if (taskAttributes.containsKey("REQUEST_TITLE")) {
//            logger.logDebug("REQUEST_TITLE:%s",(String)taskAttributes.get("REQUEST_TITLE"));  
          requestTitle = (String)taskAttributes.get("REQUEST_TITLE");
        }
        if (taskAttributes.containsKey("SENT_BY_USER")) {
//            logger.logDebug("SENT_BY_USER:%s",(String)taskAttributes.get("SENT_BY_USER"));  
          sentByUser = (String)taskAttributes.get("SENT_BY_USER");
        }
        if (taskAttributes.containsKey("SENT_TO_USER")) {
          sentToUser = (String)taskAttributes.get("SENT_TO_USER");
        } else if (taskAttributes.containsKey("TASK_ASSIGNEE_USER")) {
          sentToUser = (String)taskAttributes.get("TASK_ASSIGNEE_USER");
        }
        if (taskAttributes.containsKey("CONTRACT_NUMBER")) {
//            logger.logDebug("CONTRACT_NUMBER:%s",(String)taskAttributes.get("CONTRACT_NUMBER"));  
          userTask.setContractNumber(((String)taskAttributes.get("CONTRACT_NUMBER")).toString());
        }
        if (taskAttributes.containsKey("REQUEST_NUMBER")) {
//            logger.logDebug("REQUEST_NUMBER:%s",(String)taskAttributes.get("REQUEST_NUMBER"));  
          userTask.setRequestNumber(((String)taskAttributes.get("REQUEST_NUMBER")).toString());
        }
        if (taskAttributes.containsKey("CONTRACT_UNIT_NUMBER")) {
          userTask.setContractUnitNumber(((String)taskAttributes.get("CONTRACT_UNIT_NUMBER")).toString());
        }
        if (taskAttributes.containsKey("CONTRACT_TENANT_NAME_EN")) {
          userTask.setContractTenantNameEn(((String)taskAttributes.get("CONTRACT_TENANT_NAME_EN")).toString());
        }
        if (taskAttributes.containsKey("CONTRACT_TENANT_NAME_AR")) {
          userTask.setContractTenantNameAr(((String)taskAttributes.get("CONTRACT_TENANT_NAME_AR")).toString());
        }
        if (taskAttributes.containsKey("REQUEST_CREATED_BY_EN")) {
          userTask.setRequestCreatedByEn(((String)taskAttributes.get("REQUEST_CREATED_BY_EN")).toString());
        }
        if (taskAttributes.containsKey("REQUEST_CREATED_BY_AR")) {
          userTask.setRequestCreatedByAr(((String)taskAttributes.get("REQUEST_CREATED_BY_AR")).toString());
        }
        if (taskAttributes.containsKey("REQUEST_APPLICANT_NAME_EN")) {
//            logger.logDebug("REQUEST_APPLICANT_NAME_EN:%s",(String)taskAttributes.get("REQUEST_APPLICANT_NAME_EN"));  
          userTask.setRequestApplicantNameEn(((String)taskAttributes.get("REQUEST_APPLICANT_NAME_EN")).toString());
        }
        if (taskAttributes.containsKey("REQUEST_APPLICANT_NAME_AR")) {
          userTask.setRequestApplicantNameAr(((String)taskAttributes.get("REQUEST_APPLICANT_NAME_AR")).toString());
        }
        accquiredBy = task.getSystemAttributes().getAcquiredBy();
        if ((accquiredBy != null) && (accquiredBy != ""))
        {
//            logger.logDebug("accquiredBy:%s",(String)taskAttributes.get("accquiredBy"));  
          userTask.setAccquiredby(accquiredBy);
          userTask.setIsAccquiredByUser(Boolean.valueOf(true));
        }
        else
        {
          userTask.setIsAccquiredByUser(Boolean.valueOf(false));
        }
        if (bpelName != null)
        {
          userTask.setBpelName_en(GetEnglishTranslation(bpelName));
          userTask.setBpelName_ar(GetArabicTranslation(bpelName));
        }
        else
        {
          userTask.setBpelName_en("");
          userTask.setBpelName_ar("");
        }
        userTask.setTaskTitle_en(GetEnglishTranslation(taskTitle));
//          logger.logDebug("userTask.getTaskTitle_en():%s",userTask.getTaskTitle_en());  
        userTask.setTaskTitle_ar(GetArabicTranslation(taskTitle));
//          logger.logDebug("userTask.getTaskTitle_ar():%s",userTask.getTaskTitle_ar());  
        
        userTask.setTaskType(taskType);
//        logger.logDebug("userTask.getTaskType():%s",userTask.getTaskType());  
        userTask.setSentByUser(sentByUser);
        userTask.setSentToUser(sentToUser);
        if (taskSubject != null)
        {
          userTask.setTaskSubject_en(GetEnglishTranslation(taskSubject));
//            logger.logDebug("userTask.getTaskSubject_en():%s",userTask.getTaskSubject_en());  
          userTask.setTaskSubject_ar(GetArabicTranslation(taskSubject));
//            logger.logDebug("userTask.getTaskSubject_ar():%s",userTask.getTaskSubject_ar());  
        }
        else
        {
          userTask.setTaskSubject_en("");
          userTask.setTaskSubject_ar("");
        }
        if (requestTitle != null)
        {
          userTask.setRequestTitle_en(GetEnglishTranslation(requestTitle));
          userTask.setRequestTitle_ar(GetArabicTranslation(requestTitle));
        }
        else
        {
          userTask.setRequestTitle_en("");
          userTask.setRequestTitle_ar("");
        }
//          logger.logDebug("userTask.getRequestTitle_ar():%s",userTask.getRequestTitle_ar());  
//          logger.logDebug("userTask.getRequestTitle_en():%s",userTask.getRequestTitle_en());  
        userTask.setTaskAttributes(taskAttributes);
        
  
        userTask.setTaskId(taskId);
        
  
        userTask.setTaskNumber(taskNumber);
        if (assignedDate != null) {
          userTask.setAssignedDate(assignedDate.getTime());
        }
        if (expiryDate != null) {
          userTask.setExpiryDate(expiryDate.getTime());
        }
        if (updatedDate != null) {
          userTask.setUpdateDate(updatedDate.getTime());
        }
        if (task.getProcessInfo() != null)
        {
          userTask.setProcessId(task.getProcessInfo().getProcessId());
          userTask.setProcessName(task.getProcessInfo().getProcessName());
//          userTask.setProcessVersion(task.getProcessInfo().getProcessVersion());
          userTask.setInstanceId(task.getProcessInfo().getInstanceId());
        }
//        userTask.setTaskComments(taskComments);
      }
      
      catch (WorkflowException exp)
      {
        userTask = null;
        logger.LogException("Failed to fetch task details for task id:" + taskId + ". Workflow exception occured due to:", exp);
        exp.printStackTrace();
        if (exp.getErrorCode() == 30513) {
          throw new PIMSWorkListException(110, exp);
        }
        if (exp.getErrorCode() == 30501) {
          throw new PIMSWorkListException(109, exp);
        }
        throw new PIMSWorkListException(106, exp);
      }
      catch (Exception exp)
      {
        userTask = null;
        logger.LogException("Failed to fetch task details for task id:" + taskId + " Exception exception occured due to:", exp);
        exp.printStackTrace();
        throw new PIMSWorkListException(106, exp);
      }
      logger.logInfo("Stepped out of the method getTaskForUser");
      
      return userTask;
    }
    
    public boolean accquireTask(UserTask userTask, String user)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped in the method accquireTask with user:" + user);
      boolean isTaskAcquired = false;
      try
      {
        String worklistUser = user;
        String taskId = userTask.getTaskId();
        
        logger.logInfo("About to fetch task details using task id");
        
        Task task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(worklistUser), taskId);
        
  
  
        logger.logInfo("Task details fetched successfully");
        
        logger.logInfo("About to accquire task");
        
        String accquiredBy = task.getSystemAttributes().getAcquiredBy();
        if ((accquiredBy != null) && (!accquiredBy.equals("")))
        {
          logger.logInfo("Task is already acquired by user " + accquiredBy);
          throw new PIMSWorkListException(101, accquiredBy);
        }
        getTaskService().acquireTask(getWorkflowContext(worklistUser), task);
        isTaskAcquired = true;
      }
      catch (PIMSWorkListException pe)
      {
        throw pe;
      }
      catch (WorkflowException we)
      {
        logger.LogException("accquireTask crashed due to:", we);
        throw new PIMSWorkListException(103, we);
      }
      catch (Exception e)
      {
        logger.LogException("accquireTask crashed due to:", e);
        throw new PIMSWorkListException(103, e);
      }
      logger.logInfo("Stepped out of the method accquireTask");
      
      return isTaskAcquired;
    }
    
    public boolean releaseTask(UserTask userTask, String user)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped in the method releaseTask with user:" + user);
      boolean isTaskReleased = false;
      try
      {
        String worklistUser = user;
        String taskId = userTask.getTaskId();
        
        logger.logInfo("About to fetch task details using task id");
        
        Task task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(worklistUser), taskId);
        
  
  
        logger.logInfo("Task details fetched successfully");
        
        String accquiredBy = task.getSystemAttributes().getAcquiredBy();
        if ((accquiredBy == null) || (accquiredBy.equals("")))
        {
          logger.logInfo("Task is not acquired and hence can not be released");
          throw new PIMSWorkListException(102);
        }
        logger.logInfo("About to release task");
        if (accquiredBy.equals(user)) {
          getTaskService().releaseTask(getWorkflowContext(worklistUser), task);
        }
        isTaskReleased = true;
      }
      catch (PIMSWorkListException pe)
      {
        throw pe;
      }
      catch (WorkflowException we)
      {
        logger.LogException("releaseTask crashed due to:", we);
        throw new PIMSWorkListException(104, we);
      }
      catch (Exception e)
      {
        logger.LogException("releaseTask crashed due to:", e);
        throw new PIMSWorkListException(104, e);
      }
      logger.logInfo("Stepped out of the method releaseTask");
      
      return isTaskReleased;
    }
    
    public boolean completeTask(UserTask userTask, String user, TaskOutcome outcome)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped in the method completeTask with user:" + user + "and outcome:" + outcome);
      boolean isTaskCompleted = false;
      try
      {
        String taskId = userTask.getTaskId();
        String completed = "COMPLETED";
        boolean loadTaskAgain = false;
        
  
        logger.logInfo("About to fetch task details using task id");
        
        Task task = null;
        task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
        
        logger.logInfo("Task details fetched successfully");
        
        logger.logInfo("Querying if the task is already completed");
        if (task.getSystemAttributes().getState().equals(completed))
        {
          String completedBy = task.getSystemAttributes().getUpdatedBy().getId();
          logger.logInfo("Task was already completed by " + completedBy);
          throw new PIMSWorkListException(105, completedBy);
        }
        String accquiredBy = task.getSystemAttributes().getAcquiredBy();
        if ((userTask.getSentToUser() != null) && (userTask.getSentToUser().trim().length() > 0))
        {
          logger.logInfo("Task is assigned to particular user with id:" + userTask.getSentToUser().trim());
          accquiredBy = userTask.getSentToUser();
        }
        if ((accquiredBy != null) && (!accquiredBy.equals("")))
        {
          logger.logInfo("Task has been already acquired by " + accquiredBy);
          if (!accquiredBy.equals(user))
          {
            logger.logInfo("About to fetch task details using user id:" + accquiredBy);
            task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(accquiredBy), taskId);
            logger.logInfo("Task details fetched successfully");
            
            logger.logInfo("Trying to release task from user:" + accquiredBy);
            getTaskService().releaseTask(getWorkflowContext(accquiredBy), task);
            logger.logInfo("Task release successfully");
            
            logger.logInfo("About to fetch task details using user id:" + user);
            task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
            logger.logInfo("Task details fetched successfully");
            
            logger.logInfo("Trying to acquire task for user:" + user);
            getTaskService().acquireTask(getWorkflowContext(user), task);
            logger.logInfo("Task acquired successfully");
            loadTaskAgain = true;
          }
        }
        else
        {
          logger.logInfo("Task is not acquired by any one");
          logger.logInfo("Trying to acquire task for user:" + user);
          getTaskService().acquireTask(getWorkflowContext(user), task);
          logger.logInfo("Task acquired");
          loadTaskAgain = true;
        }
        if (loadTaskAgain)
        {
          logger.logInfo("About to fetch task details using user id:" + user);
          task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
          logger.logInfo("Task details fetched successfully");
        }
        logger.logInfo("Trying to complete te task");
        getTaskService().updateTaskOutcome(getWorkflowContext(user), task, outcome.toString());
        logger.logInfo("Task completed successfully");
        isTaskCompleted = true;
      }
      catch (PIMSWorkListException pe)
      {
        throw pe;
      }
      catch (WorkflowException we)
      {
        logger.LogException("completeTask crashed due to:", we);
        throw new PIMSWorkListException(105, we);
      }
      catch (Exception e)
      {
        logger.LogException("completeTask crashed due to:", e);
        throw new PIMSWorkListException(105, e);
      }
      logger.logInfo("Stepped out of the method completeTask");
      
      return isTaskCompleted;
    }
    
    @SuppressWarnings("unchecked")
    public boolean delegateTask(UserTask userTask, String user, List<TaskAssignee> assignees)throws PIMSWorkListException
    {
      boolean isTaskDelegated = false;
      try
      {
        String taskId = userTask.getTaskId();
        String completed = "COMPLETED";
        logger.logInfo("About to fetch task details using task id:" +taskId);
        
        Task task = null;
        task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
        logger.logInfo("Task details fetched successfully for task#:"+task.getSystemAttributes().getTaskNumber());
        logger.logInfo("Querying if the task is already completed");
        if (task.getSystemAttributes().getState().equals(completed))
        {
          String completedBy = task.getSystemAttributes().getUpdatedBy().getId();
          logger.logInfo("Task was already completed by " + completedBy);
          throw new PIMSWorkListException(111, completedBy);
        }
        String accquiredBy = task.getSystemAttributes().getAcquiredBy();
        
        if (accquiredBy == null )
        {
        	this.accquireTask(userTask, user);
        }
        logger.logInfo("Trying to delegate te task");
        
        getTaskService().reassignTask(getWorkflowContext("oc4jadmin"), task.getSystemAttributes().getTaskId(), assignees);
//        getTaskService().delegateTask(getWorkflowContext(), task.getSystemAttributes().getTaskId(), assignees);
        logger.logInfo("Task delegate successfully");
        isTaskDelegated = true;
      }
      catch (PIMSWorkListException pe)
      {
        throw pe;
      }
      catch (WorkflowException we)
      {
        logger.LogException("delegateTask crashed due to:", we);
        throw new PIMSWorkListException(111, we);
      }
      catch (Exception e)
      {
        logger.LogException("delegateTask crashed due to:", e);
        throw new PIMSWorkListException(111, e);
      }
      logger.logInfo("Stepped out of the method delegateTask");
      
      return isTaskDelegated;
    }
    
    public boolean addTaskComment(UserTask userTask, String user, String comment)
      throws PIMSWorkListException
    {
      logger.logInfo("Stepped in the method addTaskComment with user:" + user + "and comment:" + comment);
      boolean isCommentAdded = false;
      try
      {
        String taskId = userTask.getTaskId();
        String completed = "COMPLETED";
        boolean loadTaskAgain = false;
        
  
        logger.logInfo("About to fetch task details using task id");
        
        Task task = null;
        task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
        
        logger.logInfo("Task details fetched successfully");
        
        logger.logInfo("Querying if the task is already completed");
        if (task.getSystemAttributes().getState().equals(completed))
        {
          String completedBy = task.getSystemAttributes().getUpdatedBy().getId();
          logger.logInfo("Task was already completed by " + completedBy);
          throw new PIMSWorkListException(107, completedBy);
        }
        String accquiredBy = task.getSystemAttributes().getAcquiredBy();
        if ((accquiredBy != null) && (!accquiredBy.equals("")))
        {
          logger.logInfo("Task has been already acquired by " + accquiredBy);
          if (!accquiredBy.equals(user))
          {
            logger.logInfo("About to fetch task details using user id:" + accquiredBy);
            task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(accquiredBy), taskId);
            logger.logInfo("Task details fetched successfully");
            
            logger.logInfo("Trying to release task from user:" + accquiredBy);
            getTaskService().releaseTask(getWorkflowContext(accquiredBy), task);
            logger.logInfo("Task release successfully");
            
            logger.logInfo("About to fetch task details using user id:" + user);
            task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
            logger.logInfo("Task details fetched successfully");
            
            logger.logInfo("Trying to acquire task for user:" + user);
            getTaskService().acquireTask(getWorkflowContext(user), task);
            logger.logInfo("Task acquired successfully");
            
            loadTaskAgain = true;
          }
        }
        else
        {
          logger.logInfo("Task is not acquired by any one");
          logger.logInfo("Trying to acquire task for user:" + user);
          getTaskService().acquireTask(getWorkflowContext(user), task);
          logger.logInfo("Task acquired");
          loadTaskAgain = true;
        }
        if (loadTaskAgain)
        {
          logger.logInfo("About to fetch task details using user id:" + user);
          task = getTaskQueryService().getTaskDetailsById(getWorkflowContext(user), taskId);
          logger.logInfo("Task details fetched successfully");
        }
        logger.logInfo("Trying to add task comment");
        getTaskService().addComment(getWorkflowContext(user), task, comment);
        logger.logInfo("Task comment added successfully");
        isCommentAdded = true;
      }
      catch (PIMSWorkListException pe)
      {
        throw pe;
      }
      catch (WorkflowException we)
      {
        logger.LogException("addTaskComment crashed due to:", we);
        throw new PIMSWorkListException(105, we);
      }
      catch (Exception e)
      {
        logger.LogException("addTaskComment crashed due to:", e);
        throw new PIMSWorkListException(105, e);
      }
      logger.logInfo("Stepped out of the method addTaskComment");
      
      return isCommentAdded;
    }
    
    public void PrintXML(Element payload)
    {
      try
      {
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = null;
        try
        {
          trans = transfac.newTransformer();
        }
        catch (TransformerConfigurationException e) {}
        trans.setOutputProperty("omit-xml-declaration", "yes");
        trans.setOutputProperty("indent", "yes");
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(payload);
        try
        {
          trans.transform(source, result);
        }
        catch (TransformerException e) {}
        String xmlString = sw.toString();
        
        System.out.println(xmlString);
      }
      catch (Exception e) {}
    }
    
    private String GetEnglishTranslation(String val)
    {
      String returnVal = null;
      try
      {
          returnVal = ResourceWrapper.getInstance().getProperty(val,"en");
//            returnVal = "";

      }
      catch (Exception ex)
      {
        returnVal = val;
        logger.LogException("Error Occured:",ex);
      }
      return returnVal;
    }
    
    private String GetArabicTranslation(String val)
    {
      String returnVal = null;
      try
      {
//        if (this.parameters != null) {
////          returnVal = this.parameters.getParameter(val, Boolean.valueOf(false));
//        }
            returnVal = ResourceWrapper.getInstance().getProperty(val,"ar");
//          returnVal = "";
//          logger.logDebug("GetArabicTranslation|key:%s|value:%s", val,returnVal);
//        }
      }
      catch (Exception ex)
      {
        returnVal = val;
        logger.LogException("Error Occured:",ex);
      }
      return returnVal;
    }
    
    private IWorkflowServiceClient getWorkflowServiceClient()
    {
      System.out.println("Attempting to create WorkflowServiceClient");
      IWorkflowServiceClient client = WorkflowServiceClientFactory.getWorkflowServiceClient("REMOTE");
      
      System.out.println("Created WorkflowServiceClient");
      
      return client;
    }
    
    private ITaskQueryService getTaskQueryService()
    {
      System.out.println("Obtaining TaskQueryService");
      ITaskQueryService taskQueryService = getWorkflowServiceClient().getTaskQueryService();
      
      System.out.println("Obtained TaskQueryService:");
      
      return taskQueryService;
    }
    
    private IWorkflowContext getWorkflowContext(String worklistUser)      throws PIMSWorkListException
    {
      System.out.println("Creating WorkflowContext");
      IWorkflowContext workflowContext = null;
      try
      {
        workflowContext = getTaskQueryService().authenticate(WF_MANANAGER_UN, WF_MANANAGER_PW, LDAP_DOMAIN, worklistUser);
        
  
  
        System.out.println("Created WorkflowContext");
      }
      catch (WorkflowException exp)
      {
        throw new PIMSWorkListException(109, exp);
      }
      catch (Exception exp)
      {
        throw new PIMSWorkListException(109, exp);
      }
      if (workflowContext == null) {
        throw new PIMSWorkListException(109, new Exception());
      }
      return workflowContext;
    }
    
    public void setConfigFilePath(String configFilePath)
    {
      this.configFilePath = configFilePath;
    }
    
    public String getConfigFilePath()
{
return this.configFilePath;
}
}
