 package com.avanza.pims.soa.bpm.worklist;
 
 import java.util.Date;
 import java.util.List;
 
 public class TaskSearchCriteria
 {
   private String requestTitle;
   private String taskTitle;
   private String acquiredBy;
   private Date assignedDateFrom;
   private Date assignedDateTo;
   private Date expiryDateFrom;
   private Date expiryDateTo;
   private List<String> assigneeGroups;
   
   public void setRequestTitle(String requestTitle)
   {
     this.requestTitle = requestTitle;
   }
   
   public String getRequestTitle()
   {
     return this.requestTitle;
   }
   
   public void setTaskTitle(String taskTitle)
   {
     this.taskTitle = taskTitle;
   }
   
   public String getTaskTitle()
   {
     return this.taskTitle;
   }
   
   public void setAssignedDateFrom(Date assignedDateFrom)
   {
     this.assignedDateFrom = assignedDateFrom;
   }
   
   public Date getAssignedDateFrom()
   {
     return this.assignedDateFrom;
   }
   
   public void setAssignedDateTo(Date assignedDateTo)
   {
     this.assignedDateTo = assignedDateTo;
   }
   
   public Date getAssignedDateTo()
   {
     return this.assignedDateTo;
   }
   
   public void setExpiryDateFrom(Date expiryDateFrom)
   {
     this.expiryDateFrom = expiryDateFrom;
   }
   
   public Date getExpiryDateFrom()
   {
     return this.expiryDateFrom;
   }
   
   public void setExpiryDateTo(Date expiryDateTo)
   {
     this.expiryDateTo = expiryDateTo;
   }
   
   public Date getExpiryDateTo()
   {
     return this.expiryDateTo;
   }
   
   public void setAssigneeGroups(List<String> assigneeGroups)
   {
     this.assigneeGroups = assigneeGroups;
   }
   
   public List<String> getAssigneeGroups()
   {
     return this.assigneeGroups;
   }

public String getAcquiredBy() {
	return acquiredBy;
}

public void setAcquiredBy(String acquiredBy) {
	this.acquiredBy = acquiredBy;
}
 }


/* Location:           C:\Projects\Java\PIMS\_PIMS_SOA_CLIENT\
 * Qualified Name:     com.avanza.pims.soa.bpm.worklist.TaskSearchCriteria
 * JD-Core Version:    0.7.0.1
 */