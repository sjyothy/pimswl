 package com.avanza.pims.soa.bpm.worklist;
 
 import java.io.Serializable;
 import java.util.Date;
 import java.util.List;
 import java.util.Map;
 
 public class UserTask
   implements Serializable
 {
   private String taskId;
   private int taskNumber;
   private Date assignedDate;
   private Date expiryDate;
   private Date updateDate;
   private String bpelName_en;
   private String bpelName_ar;
   private String requestTitle_en;
   private String requestTitle_ar;
   private String taskTitle_en;
   private String taskTitle_ar;
   private String taskType;
   private String taskSubject_en;
   private String taskSubject_ar;
   private Map<String, String> taskAttributes;
   private Boolean isAccquiredByUser;
   private String accquiredby;
   List<String> taskComments;
   private String sentByUser;
   private String sentToUser;
   private String contractNumber;
   private String contractTenantNameEn;
   private String contractTenantNameAr;
   private String contractUnitNumber;
   private String requestNumber;
   private String requestCreatedByEn;
   private String requestCreatedByAr;
   private String requestApplicantNameEn;
   private String requestApplicantNameAr;
   private String processId;
   private String processName;
   private String processVersion;
   private String instanceId;
   private String taskAssigneeGroups;
   
   public void setTaskId(String taskId)
   {
     this.taskId = taskId;
   }
   
   public String getTaskId()
   {
     return this.taskId;
   }
   
   public void setTaskNumber(int taskNumber)
   {
     this.taskNumber = taskNumber;
   }
   
   public int getTaskNumber()
   {
     return this.taskNumber;
   }
   
   public void setAssignedDate(Date assignedDate)
   {
     this.assignedDate = assignedDate;
   }
   
   public Date getAssignedDate()
   {
     return this.assignedDate;
   }
   
   public void setExpiryDate(Date expiryDate)
   {
     this.expiryDate = expiryDate;
   }
   
   public Date getExpiryDate()
   {
     return this.expiryDate;
   }
   
   public void setUpdateDate(Date updateDate)
   {
     this.updateDate = updateDate;
   }
   
   public Date getUpdateDate()
   {
     return this.updateDate;
   }
   
   public void setBpelName_en(String bpelName_en)
   {
     this.bpelName_en = bpelName_en;
   }
   
   public String getBpelName_en()
   {
     return this.bpelName_en;
   }
   
   public void setBpelName_ar(String bpelName_ar)
   {
     this.bpelName_ar = bpelName_ar;
   }
   
   public String getBpelName_ar()
   {
     return this.bpelName_ar;
   }
   
   public void setTaskTitle_en(String taskTitle_en)
   {
     this.taskTitle_en = taskTitle_en;
   }
   
   public String getTaskTitle_en()
   {
     return this.taskTitle_en;
   }
   
   public void setTaskTitle_ar(String taskTitle_ar)
   {
     this.taskTitle_ar = taskTitle_ar;
   }
   
   public String getTaskTitle_ar()
   {
     return this.taskTitle_ar;
   }
   
   public void setTaskSubject_en(String taskSubject_en)
   {
     this.taskSubject_en = taskSubject_en;
   }
   
   public String getTaskSubject_en()
   {
     return this.taskSubject_en;
   }
   
   public void setTaskSubject_ar(String taskSubject_ar)
   {
     this.taskSubject_ar = taskSubject_ar;
   }
   
   public String getTaskSubject_ar()
   {
     return this.taskSubject_ar;
   }
   
   public void setTaskAttributes(Map<String, String> taskAttributes)
   {
     this.taskAttributes = taskAttributes;
   }
   
   public Map<String, String> getTaskAttributes()
   {
     return this.taskAttributes;
   }
   
   public void setIsAccquiredByUser(Boolean isAccquiredByUser)
   {
     this.isAccquiredByUser = isAccquiredByUser;
   }
   
   public Boolean getIsAccquiredByUser()
   {
     return this.isAccquiredByUser;
   }
   
   public void setAccquiredby(String accquiredby)
   {
     this.accquiredby = accquiredby;
   }
   
   public String getAccquiredby()
   {
     return this.accquiredby;
   }
   
   public void setTaskType(String taskType)
   {
     this.taskType = taskType;
   }
   
   public String getTaskType()
   {
     return this.taskType;
   }
   
   public void setTaskComments(List<String> taskComments)
   {
     this.taskComments = taskComments;
   }
   
   public List<String> getTaskComments()
   {
     return this.taskComments;
   }
   
   public void setRequestTitle_en(String requestTitle_en)
   {
     this.requestTitle_en = requestTitle_en;
   }
   
   public String getRequestTitle_en()
   {
     return this.requestTitle_en;
   }
   
   public void setRequestTitle_ar(String requestTitle_ar)
   {
     this.requestTitle_ar = requestTitle_ar;
   }
   
   public String getRequestTitle_ar()
   {
     return this.requestTitle_ar;
   }
   
   public void setSentByUser(String sentByUser)
   {
     this.sentByUser = sentByUser;
   }
   
   public String getSentByUser()
   {
     return this.sentByUser;
   }
   
   public void setContractNumber(String contractNumber)
   {
     this.contractNumber = contractNumber;
   }
   
   public String getContractNumber()
   {
     return this.contractNumber;
   }
   
   public void setContractUnitNumber(String contractUnitNumber)
   {
     this.contractUnitNumber = contractUnitNumber;
   }
   
   public String getContractUnitNumber()
   {
     return this.contractUnitNumber;
   }
   
   public void setRequestNumber(String requestNumber)
   {
     this.requestNumber = requestNumber;
   }
   
   public String getRequestNumber()
   {
     return this.requestNumber;
   }
   
   public void setContractTenantNameEn(String contractTenantNameEn)
   {
     this.contractTenantNameEn = contractTenantNameEn;
   }
   
   public String getContractTenantNameEn()
   {
     return this.contractTenantNameEn;
   }
   
   public void setContractTenantNameAr(String contractTenantNameAr)
   {
     this.contractTenantNameAr = contractTenantNameAr;
   }
   
   public String getContractTenantNameAr()
   {
     return this.contractTenantNameAr;
   }
   
   public void setRequestCreatedByEn(String requestCreatedByEn)
   {
     this.requestCreatedByEn = requestCreatedByEn;
   }
   
   public String getRequestCreatedByEn()
   {
     return this.requestCreatedByEn;
   }
   
   public void setRequestCreatedByAr(String requestCreatedByAr)
   {
     this.requestCreatedByAr = requestCreatedByAr;
   }
   
   public String getRequestCreatedByAr()
   {
     return this.requestCreatedByAr;
   }
   
   public void setRequestApplicantNameEn(String requestApplicantNameEn)
   {
     this.requestApplicantNameEn = requestApplicantNameEn;
   }
   
   public String getRequestApplicantNameEn()
   {
     return this.requestApplicantNameEn;
   }
   
   public void setRequestApplicantNameAr(String requestApplicantNameAr)
   {
     this.requestApplicantNameAr = requestApplicantNameAr;
   }
   
   public String getRequestApplicantNameAr()
   {
     return this.requestApplicantNameAr;
   }
   
   public void setProcessId(String processId)
   {
     this.processId = processId;
   }
   
   public String getProcessId()
   {
     return this.processId;
   }
   
   public void setProcessName(String processName)
   {
     this.processName = processName;
   }
   
   public String getProcessName()
   {
     return this.processName;
   }
   
   public void setProcessVersion(String processVersion)
   {
     this.processVersion = processVersion;
   }
   
   public String getProcessVersion()
   {
     return this.processVersion;
   }
   
   public void setInstanceId(String instanceId)
   {
     this.instanceId = instanceId;
   }
   
   public String getInstanceId()
   {
     return this.instanceId;
   }
   
   public String getSentToUser()
   {
     return this.sentToUser;
   }
   
   public void setSentToUser(String sentToUser)
   {
     this.sentToUser = sentToUser;
   }
   
   public void setTaskAssigneeGroups(String taskAssigneeGroups)
   {
     this.taskAssigneeGroups = taskAssigneeGroups;
   }
   
   public String getTaskAssigneeGroups()
   {
     return this.taskAssigneeGroups;
   }
 }


/* Location:           C:\Projects\Java\PIMS\_PIMS_SOA_CLIENT\
 * Qualified Name:     com.avanza.pims.soa.bpm.worklist.UserTask
 * JD-Core Version:    0.7.0.1
 */