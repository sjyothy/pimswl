package com.avanza.util;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.ui.util.ResourceUtil;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceWrapper {
    
    private static Logger logger = Logger.getLogger(ResourceWrapper.class);
    private static ResourceWrapper _instance;
    private  Map<String, ResourceBundle> resourceBundles;
    
    public static String property = "com.avanza.pims.web.messageresource.Messages";
    public static String resource ="";    
    
    protected ResourceWrapper() {
        resourceBundles = new HashMap<String, ResourceBundle>(); 
    }
    
    
    
    public static ResourceWrapper getInstance() {
        if (_instance == null) {
            _instance = new ResourceWrapper();
        }
        return _instance;
    }

    
    
    public String getProperty(String propertyName,String localeString,  Object... params) {
        
        String retValue = "";
        
        Locale locale =  null;
        try 
        {
//            logger.logDebug("getProperty:|propertyName:%s|localeString:%s",propertyName,localeString);
                locale = new Locale(localeString);    
                retValue = this.getResourceBundle(property, locale).getString(propertyName);
        
        } catch(Exception e){
            
            retValue = "key " + propertyName + " not found";
            logger.LogException("Error Occured:",e);
        }
        
        if(params != null){
            
            MessageFormat mf = new MessageFormat(retValue, locale);
            retValue = mf.format(params, new StringBuffer(), null).toString();
        }

        return retValue;
    }
    
    

    private ResourceBundle getResourceBundle(String bundleName, Locale locale) {
        ResourceBundle bundle = this.resourceBundles.get(bundleName + locale.toString());
        
        if (bundle == null) {
            bundle = ResourceBundle.getBundle(bundleName, locale);
            this.resourceBundles.put((bundleName + locale.toString()), bundle);
        }
        return bundle;
    }
}
