 package com.avanza.util;
 
 import com.avanza.core.util.Logger;
 import java.io.IOException;
 import org.apache.commons.configuration.ConfigurationException;
 import org.apache.commons.configuration.FileConfiguration;
 import org.apache.commons.configuration.PropertiesConfiguration;
 
 public class SystemParameters
 {
   private static Logger logger = Logger.getLogger(SystemParameters.class);
   private static String PARAMETERS_FILE_EN = "";
   private static String PARAMETERS_FILE_AR = "";
   private static FileConfiguration config_en;
   private static FileConfiguration config_ar;
   private static SystemParameters me;
   
   private SystemParameters(String paramFileEn, String paramFileAr)
     throws IOException
   {
     PARAMETERS_FILE_EN = paramFileEn;
     PARAMETERS_FILE_AR = paramFileAr;
     try
     {
       loadParametersFromFile_EN();
       loadParametersFromFile_AR();
     }
     catch (Exception e)
     {
       logger.logError("Failed to load system resource files:" + e.getStackTrace());
     }
   }
   
   public static SystemParameters getInstance(String paramFileEn, String paramFileAr)
     throws IOException
   {
     if (me == null) {
       me = new SystemParameters(paramFileEn, paramFileAr);
     }
     return me;
   }
   
   private static void loadParametersFromFile_EN()
     throws Exception
   {
     logger.logInfo(" About to load parameters from File: " + PARAMETERS_FILE_EN);
     try
     {
       config_en = new PropertiesConfiguration(PARAMETERS_FILE_EN);
       config_en.setEncoding("UTF-8");
       config_en.setAutoSave(true);
     }
     catch (ConfigurationException e)
     {
       throw new Exception(e);
     }
     logger.logInfo(" Parameters loaded from File successfully. ");
   }
   
   private static void loadParametersFromFile_AR()
     throws Exception
   {
     logger.logInfo(" About to load parameters from File: " + PARAMETERS_FILE_AR);
     try
     {
       config_ar = new PropertiesConfiguration(PARAMETERS_FILE_AR);
       config_ar.setEncoding("UTF-8");
       config_ar.setAutoSave(true);
     }
     catch (ConfigurationException e)
     {
       throw new Exception(e);
     }
     logger.logInfo(" Parameters loaded from File successfully. ");
   }
   
   public String getParameter(String aParamName, Boolean isEnglish)
   {
     if (isEnglish.booleanValue()) {
       return config_en.getString(aParamName);
     }
     return config_ar.getString(aParamName);
   }
   
   public boolean getBoolean(String parameter, Boolean isEnglish)
   {
     if (isEnglish.booleanValue()) {
       return config_en.getBoolean(parameter);
     }
     return config_ar.getBoolean(parameter);
   }
   
   public int getInteger(String parameter, Boolean isEnglish)
   {
     if (isEnglish.booleanValue()) {
       return config_en.getInt(parameter);
     }
     return config_ar.getInt(parameter);
   }
   
   public void setParameter(String aParamName, Object value, Boolean isEnglish)
   {
     if (isEnglish.booleanValue()) {
       config_en.setProperty(aParamName, value);
     } else {
       config_ar.setProperty(aParamName, value);
     }
   }
   
   public String[] getParamterValues(String parameter, Boolean isEnglish)
   {
     if (isEnglish.booleanValue()) {
       return config_en.getStringArray(parameter);
     }
     return config_ar.getStringArray(parameter);
   }
 }


/* Location:           C:\Projects\Java\PIMS\_PIMS_SOA_CLIENT\
 * Qualified Name:     com.avanza.util.SystemParameters
 * JD-Core Version:    0.7.0.1
 */